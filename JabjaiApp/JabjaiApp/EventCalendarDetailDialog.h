//
//  EventCalendarDetailDialog.h
//  JabjaiApp
//
//  Created by mac on 5/16/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EventCalendarDetailDialogDelegate <NSObject>

@optional
-(void)eventDialogClose;

@end

@interface EventCalendarDetailDialog : UIViewController

@property (nonatomic, retain) id<EventCalendarDetailDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *message2Label;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;


- (IBAction)closeDialog:(id)sender;

- (void)showDialogInView:(UIView *)targetView message:(NSString *)message holiday:(NSString *)holiday messageDate:(NSDate *)messageDate;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
