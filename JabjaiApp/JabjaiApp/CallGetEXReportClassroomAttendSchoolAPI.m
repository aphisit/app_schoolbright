//
//  CallGetEXReportClassroomAttendSchoolAPI.m
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetEXReportClassroomAttendSchoolAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetEXReportClassroomAttendSchoolAPI () {
    NSInteger connectCounter;
    NSMutableArray<EXReportClassStatusModel *> *classStatusArray;
}

@end

@implementation CallGetEXReportClassroomAttendSchoolAPI

- (instancetype)init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(long long)schoolId classLevelId:(long long)classLevelId date:(NSDate *)date {
    [self getDataFromServer:schoolId classLevelId:classLevelId date:date];
}

- (void)getDataFromServer:(long long)schoolId classLevelId:(long long)classLevelId date:(NSDate *)date {
    
    NSString *urlString = [APIURL getEXReportClassroomAttendSchoolWithSchoolId:schoolId classLevelId:classLevelId date:date];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataFromServer:schoolId classLevelId:classLevelId date:date];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId classLevelId:classLevelId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId classLevelId:classLevelId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(classStatusArray != nil) {
                    [classStatusArray removeAllObjects];
                    classStatusArray = nil;
                }
                
                classStatusArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long classroomId = [[dataDict objectForKey:@"sublevelid"] longLongValue];
                    NSString *classroomName = [dataDict objectForKey:@"sublevelname"];
                    NSInteger numberOfOnTimeStatus = [[dataDict objectForKey:@"status_0"] integerValue];
                    NSInteger numberOfLateStatus = [[dataDict objectForKey:@"status_1"] integerValue];
                    NSInteger numberOfAbsenceStatus = [[dataDict objectForKey:@"status_2"] integerValue];
                    NSInteger numberOfEventStatus = [[dataDict objectForKey:@"status_5"] integerValue];
                    NSInteger numberOfSickLeaveStatus = [[dataDict objectForKey:@"status_4"] integerValue];
                    NSInteger numberOfPersonalStatus = [[dataDict objectForKey:@"status_3"] integerValue];
                    
                    EXReportClassStatusModel *model = [[EXReportClassStatusModel alloc] init];
                    [model setIId:classroomId];
                    [model setTitle:classroomName];
                    [model setNumberOfOnTimeStatus:numberOfOnTimeStatus];
                    [model setNumberOfLateStatus:numberOfLateStatus];
                    [model setNumberOfAbsenceStatus:numberOfAbsenceStatus];
                    [model setNumberOfEventStatus:numberOfEventStatus];
                    [model setNumberOfSickLeaveStatus:numberOfSickLeaveStatus];
                    [model setNumberOfPersonalStatus:numberOfPersonalStatus];
                    
                    [classStatusArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetEXReportClassroomAttendSchoolAPI:data:success:)]) {
                    [self.delegate callGetEXReportClassroomAttendSchoolAPI:self data:classStatusArray success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetEXReportClassroomAttendSchoolAPI:data:success:)]) {
                [self.delegate callGetEXReportClassroomAttendSchoolAPI:self data:nil success:NO];
            }
        }
    }];
}
@end
