//
//  ReportLeaveMessageListViewController.h
//  JabjaiApp
//
//  Created by mac on 4/10/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoticeReportDateMessageModel.h"
#import "SlideMenuController.h"

@protocol ReportLeaveMessageListDelegate <NSObject>
@optional
- (void)showReportLeaveDetailLeave:(long long)letterID page:(int)page;
@end

@interface ReportLeaveMessageListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate>
@property (retain, nonatomic) id<ReportLeaveMessageListDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *headerLeaveTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDayLabel;

@property (nonatomic) long long letterID;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *dateCount;

@end
