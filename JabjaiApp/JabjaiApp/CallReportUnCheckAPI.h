//
//  CallReportUnCheckAPI.h
//  JabjaiApp
//
//  Created by toffee on 21/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReportUnCheckModel.h"
NS_ASSUME_NONNULL_BEGIN

@class CallReportUnCheckAPI;

@protocol CallReportUnCheckAPIDelegate <NSObject>
@optional
- (void)callReportUnCheckAPI:(CallReportUnCheckAPI *)classObj dateTimeArray:(NSMutableArray<ReportUnCheckModel *>*)dateArray success:(BOOL)success;
@end

@interface CallReportUnCheckAPI : NSObject
@property (nonatomic, weak) id<CallReportUnCheckAPIDelegate> delegate;
- (void)call:(long long)schoolId date:(NSString*)date;
@end


NS_ASSUME_NONNULL_END
