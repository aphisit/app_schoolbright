//
//  TASelectClassLevelViewController.m
//  JabjaiApp
//
//  Created by mac on 7/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TASelectClassLevelViewController.h"
#import "UserData.h"
#import "TAStudentListViewController.h"
#import "TAStudentStatusModel.h"
#import "Utils.h"
#import "UserInfoViewController.h"
@interface TASelectClassLevelViewController () {
    
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    BOOL notArrangeTimeTable;
    // The dialog
    TableListDialog *tableListDialog;
    SubjectTableListDialog *subjectTableListDialog;
    AlertDialog *alertDialog;
    UnAuthorizeMenuDialog *unAuthorizeMenuDialog;
    NSBundle *myLangBundle;
    NSMutableArray *authorizeSubMenuArray;
    NSDate *dateCalendar;
    
}
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;
@property (strong, nonatomic) CallGetSubjectAPI *callGetSubjectAPI;
@property (strong, nonatomic) CallGetStudentInClassAPI *callGetStudentInClassAPI;
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@property (nonatomic, strong) SelectOneDayCalendarViewController *calendarDialog;

@end

static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";
static NSString *subjectRequestCode = @"subjectRequestCode";

@implementation TASelectClassLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    if ([UserData getUserType] != STUDENT) { [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//call to authorize menu
    }
     myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.classLevelTextField.delegate = self;
    self.classroomTextField.delegate = self;
    self.subjectTextField.delegate = self;
    
    dateCalendar = [NSDate date];
    self.dateLabel.text = [Utils getThaiDateFormatWithDate:[NSDate date]];
    
    if(_classLevelArray != nil && _classroomArray != nil && _subjectArray != nil) {
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classLevelArray.count; i++) {
            SchoolLevelModel *model = [_classLevelArray objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classroomArray.count; i++) {
            SchoolClassroomModel *model = [_classroomArray objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:_selectedClassLevelIndex] getClassLevelName];
        self.classroomTextField.text = [[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomName];
        self.subjectTextField.text = [_subjectArray objectAtIndex:_selectedSubjectIndex].subjectName;
        
        notArrangeTimeTable = NO;
        
    }
    else {
        _selectedClassLevelIndex = -1;
        _selectedClassroomIndex = -1;
        _selectedSubjectIndex = -1;
        [self showIndicator];
        notArrangeTimeTable = YES;
        
        [self getSchoolClassLevel];
    }
   
    if(_mode == TA_MODE_CHECK) {
        //self.headerTitleLabel.text = @"เช็คชื่อรายวิชา";
        self.calandaBtn.hidden = YES;
    }
    else {
        //self.headerTitleLabel.text = @"แก้ไขเช็คชื่อรายวิชา";
        self.calandaBtn.hidden = NO;
    }
    
    [self setLanguage];
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section2.layer setShadowOpacity:0.3];
    [self.section2.layer setShadowRadius:3.0];
    [self.section2.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section3.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section3.layer setShadowOpacity:0.3];
    [self.section3.layer setShadowRadius:3.0];
    [self.section3.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.headerNextbtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.headerNextbtn.layer setShadowOpacity:0.3];
    [self.headerNextbtn.layer setShadowRadius:3.0];
    [self.headerNextbtn.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLanguage{
   
    if(self.mode == TA_MODE_CHECK){
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN",nil,myLangBundle,nil);
    }else if(self.mode == TA_MODE_EDIT){
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_EDIT",nil,myLangBundle,nil);
    }
    self.headerClassLevelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_CLASSLEVEL",nil,myLangBundle,nil);
    self.headerClassLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_CLASSROOM",nil,myLangBundle,nil);
    self.headerSubjectLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_SUBJECT",nil,myLangBundle,nil);
    self.classLevelTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_TAKE_ATTEN_CLASSLAVEL",nil,myLangBundle,nil);
    self.classroomTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_TAKE_ATTEN_CLASSROOM",nil,myLangBundle,nil);
    self.subjectTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_TAKE_ATTEN_SUBJECT",nil,myLangBundle,nil);
    [_headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_JOBHOME_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
}

///check authorize menu
- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
    [self showIndicator];
    // [self popUpLoaddin];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
    authorizeSubMenuArray = [[NSMutableArray alloc] init];
    [self stopIndicator];
    
        if ((resCode == 300 || resCode == 200) && success ) {
            if (data != NULL || data != nil) {
                for (int i = 0;i < data.count; i++) {
                    NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
                    for (int j = 0; j < subMenuArray.count; j++) {
                        NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
                        [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
                    }
                }
            }
            NSString* notPermissionText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_UNAUTHORRIZE",nil,myLangBundle,nil);
            NSString* pleaseContactText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_PLEASECONTACT",nil,myLangBundle,nil);
            NSString *alertMessage = [NSString stringWithFormat:@"%@\n%@",notPermissionText,pleaseContactText];
            if(self.mode == 1){
                if (![authorizeSubMenuArray containsObject: @"9"]) {
                    [self showAlertDialogAuthorizeMenu:alertMessage];
                    NSLog(@"ไม่มีสิทธิ์");
                }
            }else{
                if (![authorizeSubMenuArray containsObject: @"11"]) {
                    [self showAlertDialogAuthorizeMenu:alertMessage];
                    NSLog(@"ไม่มีสิทธิ์");
                }
            }
        }else{
            
            self.dbHelper = [[UserInfoDBHelper alloc] init];
            [self.dbHelper deleteUserAllWithMasterId:[UserData getMasterUserID]];
            
            // Logout
            [UserData setUserLogin:NO];
            [UserData saveMasterUserID:0];
            [UserData saveUserID:0];
            [UserData setUserType:USERTYPENULL];
            [UserData setAcademyType:ACADEMYTYPENULL];
            [UserData setFirstName:@""];
            [UserData setLastName:@""];
            [UserData setUserImage:@""];
            [UserData setGender:MALE];
            [UserData setClientToken:@""];
            LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
            [self.slideMenuController changeMainViewController:viewController close:YES];
            
        }
}

- (void)onAlertDialogClose {
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
     [self stopIndicator];
    if(textField.tag == 1) { // when user select class level textfield
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        return NO;
    }
    else if(textField.tag == 2) { // select classroom textfield
         [self stopIndicator];
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_TAKE_ATTEN_CLASSLEVEL",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }else{
            NSString *alertMessage = @"ไม่พบชั้นเรียน";
            [self showAlertDialogWithMessage:alertMessage];
        }
        return NO;
    }
    else if(textField.tag == 3) {
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_TAKE_ATTEN_CLASSLEVEL",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(self.classroomTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_TAKE_ATTEN_CLASSROOM",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(notArrangeTimeTable) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_TAKE_ATTEN_NOT_TIME_TABLE",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(_subjectArray != nil && _subjectArray.count > 0) {
            
            // show dialog
            [self showSubjectTableListDialogWithRequestCode:subjectRequestCode data:_subjectArray];
           
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - API Caller

- (void)getSchoolClassLevel {
    [self showIndicator];
    if(self.callGetSchoolLevelAPI != nil) {
        self.callGetSchoolLevelAPI = nil;
    }
    
    self.callGetSchoolLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolLevelAPI.delegate = self;
    [self.callGetSchoolLevelAPI call:[UserData getSchoolId]];
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    [self showIndicator];
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}

- (void)getSubjectDataWithClassroomId:(long long)classroomId date:(NSString*)date{
    [self showIndicator];
    if(self.callGetSubjectAPI != nil) {
        self.callGetSubjectAPI = nil;
    }
    
    self.callGetSubjectAPI = [[CallGetSubjectAPI alloc] init];
    self.callGetSubjectAPI.delegate = self;
    [self.callGetSubjectAPI call:[UserData getSchoolId] classroomId:classroomId date:date];
}

- (void)getStudentInClassData {
    
    if(self.callGetStudentInClassAPI != nil) {
        self.callGetStudentInClassAPI = nil;
    }
    
    self.callGetStudentInClassAPI = [[CallGetStudentInClassAPI alloc] init];
    self.callGetStudentInClassAPI.delegate = self;
    [self.callGetStudentInClassAPI call:[UserData getSchoolId] classroomId:[[[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomID] longLongValue] subjectId:[_subjectArray objectAtIndex:_selectedSubjectIndex].subjectID teacherId:[UserData getUserID] date:[Utils dateToServerDateFormat:dateCalendar]];
}

#pragma mark - CallGetSchoolClassLevelAPIDelegate

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    [self stopIndicator];
    if(success && data != nil) {
        
        _classLevelArray = data;
        
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
    }
}

#pragma mark - CallGetSchoolClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
     [self stopIndicator];
    if(success && data != nil) {
       
        _classroomArray = data;
        
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}

#pragma mark - CallGetSubjectAPIDelegate

- (void)callGetSubjectAPI:(CallGetSubjectAPI *)classObj data:(NSArray<SSubjectModel *> *)data success:(BOOL)success {
    [self stopIndicator];
    if((success && data != nil) || data.count != 0) {
        
        _subjectArray = data;
        notArrangeTimeTable = NO;
    }else{
      //  _subjectArray = nil;
        notArrangeTimeTable = YES;

    }
}

- (void)notArrangeTimeTable:(CallGetSubjectAPI *)classObj {
    notArrangeTimeTable = YES;
}

#pragma mark - CallGetStudentInClassAPIDelegate

- (void)callGetStudentInClassAPI:(CallGetStudentInClassAPI *)classObj data:(NSArray<TAStudentStatusModel *> *)data success:(BOOL)success {
    if(success && data != nil && data.count > 0) {
        BOOL alreadyCheck = NO;
        for(TAStudentStatusModel *model in data) {
            if([model getScanStatus] == 0 || [model getScanStatus] == 1 || [model getScanStatus] == 3 || [model getScanStatus] == 6) {
                alreadyCheck = YES;
                break;
            }
        }
        
        if(_mode == TA_MODE_CHECK && alreadyCheck) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_TAKE_ATTEN_CHECK_SUCESS",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else {
            [self gotoTAStudentListViewController];
        }
    }
    else {
        [self gotoTAStudentListViewController];
    }
}

#pragma mark - Action functions
- (IBAction)openDrawer:(id)sender {
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
}

- (IBAction)selecDayAction:(id)sender {
    [self showCalendarSelectDay];
}

- (IBAction)actionNext:(id)sender {
    if([self validateData]) {
        [self getStudentInClassData];
    }
}

#pragma mark - Utility
- (BOOL)validateData {
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_TAKE_ATTEN_CLASSLEVEL",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    else if(self.classroomTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_TAKE_ATTEN_CLASSROOM",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    else if(self.subjectTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_TAKE_ATTEN_SUBJECT",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    return YES;
}

- (void)gotoTAStudentListViewController {
    TAStudentListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TAStudentListStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.subjectArray = _subjectArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.selectedSubjectIndex = _selectedSubjectIndex;
    viewController.mode = _mode;
    viewController.dateCalendar = dateCalendar;
    
    viewController.classroomId = [[[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomID] longLongValue];
    viewController.subjectId = [_subjectArray objectAtIndex:_selectedSubjectIndex].subjectID;
    viewController.subjectName = [_subjectArray objectAtIndex:_selectedSubjectIndex].subjectName;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    [self stopIndicator];
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
         [self stopIndicator];
        if(index == _selectedClassLevelIndex) {
            return;
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classroomTextField.text = @"";
        self.subjectTextField.text = @"";
        
        _selectedClassroomIndex = -1;
        _selectedSubjectIndex = -1;
        
        // get classroom data with class level
        long long classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:classLevelId];
        
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
         [self stopIndicator];
        if(index == _selectedClassroomIndex) {
            return;
        }
        
        self.classroomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        
        // Clear dependency text field values;
        self.subjectTextField.text = @"";
        _selectedSubjectIndex = -1;
        
        // get subject data with classroom id
        long long classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        [self getSubjectDataWithClassroomId:classroomId date:[Utils dateToServerDateFormat:dateCalendar]];
        
    }
    

}

#pragma mark - SubjectTableListDialogDelegate
- (void)subjectTableListDialog:(SubjectTableListDialog *)dialog onItemSelectWithRequestCode:(NSString *)requestCode atIndex:(NSInteger)index {
 
    if([requestCode isEqualToString:subjectRequestCode]) {
        
        if(index == _selectedSubjectIndex) {
            return;
        }
        self.subjectTextField.text = [_subjectArray objectAtIndex:index].subjectName;
        _selectedSubjectIndex = index;
    }
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showSubjectTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray<SSubjectModel *> *)data {
    
    if(subjectTableListDialog != nil && [subjectTableListDialog isDialogShowing]) {
        [subjectTableListDialog dismissDialog];
        subjectTableListDialog = nil;
    }
    
    subjectTableListDialog = [[SubjectTableListDialog alloc] initWithRequestCode:requestCode];
    subjectTableListDialog.delegate = self;
    [subjectTableListDialog showDialogInView:self.view subjectArray:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showAlertDialogAuthorizeMenu:(NSString *)message {
    if(unAuthorizeMenuDialog != nil && [unAuthorizeMenuDialog isDialogShowing]) {
        [unAuthorizeMenuDialog dismissDialog];
    }
    else {
        unAuthorizeMenuDialog = [[UnAuthorizeMenuDialog alloc] init];
        unAuthorizeMenuDialog.delegate = self;
    }
    [unAuthorizeMenuDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - SelectOneDayCalendarViewController
- (void)showCalendarSelectDay{
    if(self.calendarDialog != nil && [self.calendarDialog isDialogShowing]) {
           [self.calendarDialog dismissDialog];
           self.calendarDialog = nil;
    }
    
    self.calendarDialog = [[SelectOneDayCalendarViewController alloc] init];
    self.calendarDialog.delegate = self;
    [self.calendarDialog showDialogInView:self.view];
}
- (void)calendarDialogPressOK:(NSArray<NSDate *> *)selectedDates{
    
    if (selectedDates.count > 0) {
        dateCalendar = [selectedDates objectAtIndex:0];
        self.dateLabel.text = [Utils getThaiDateFormatWithDate:[selectedDates objectAtIndex:0]];
    }else{
        dateCalendar = [NSDate date];
        self.dateLabel.text = [Utils getThaiDateFormatWithDate:[NSDate date]];
    }
    
   

    self.classLevelTextField.text = @"";
    self.classroomTextField.text = @"";
    self.subjectTextField.text = @"";
     _selectedClassLevelIndex = -1;
    _selectedClassroomIndex = -1;
    _selectedSubjectIndex = -1;
    [self showIndicator];
    //notArrangeTimeTable = YES;
           
    [self getSchoolClassLevel];
    [self getSchoolClassLevel];
    NSLog(@"xxxx");
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

@end
