//
//  BSReportCalendarViewController.m
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSReportCalendarViewController.h"
#import "Utils.h"
#import "DateUtility.h"
#import "BSReportDetailViewController.h"
#import "BSReportARSPContainerController.h"
#import "BSReportSelectClassLevelViewController.h"

#import "BSCalendarModel.h"

#import "BSNameBehaviorScoreTableViewCell.h"

#import "UserData.h"

#import "APIURL.h"

@interface BSReportCalendarViewController () {
    UIColor *grayColor;
    UIColor *headerTitleColor;
    UIColor *todayColor;
    UIColor *selectionColor;
    
    NSMutableDictionary *fillDefaultColors;
    NSMutableArray *weekendDays;
    
    NSMutableArray *holidayArray;
    
    NSInteger connectCounter;
    
    NSCalendar *gregorian;
    
    UIColor *greenColor;
    UIColor *redColor;
    
    NSDate *consideredDate;
    NSDateFormatter *dateFormatter;
    
    NSMutableDictionary<NSString *, NSMutableArray<BSHistoryModel *> *> *bsHistoryModelDict;
    NSMutableArray<BSHistoryModel *> *bsHistoryArray;
    
    NSMutableArray<BSCalendarModel *> *eventArray;
}
//
//@property (strong, nonatomic) BSReportARSPContainerController *panelControllerContainer;
//@property (strong, nonatomic) BSReportScoreListViewController *bsReportScoreListViewController;

@property (strong, nonatomic) UIColor *eventGreenColor;

@property (nonatomic) CGFloat shadowRadius;
@property (nonatomic) CGFloat shadowOpacity;
@property (nonatomic) CGFloat animationDuration;

@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;

@property (strong, nonatomic) CallGetBSHistoryInDateRangeAPI *callGetBSHistoryInDateRangeAPI;

@end

static NSString *cellIdentifier = @"Cell";

@implementation BSReportCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    selectionColor = [UIColor colorWithRed:212/255.0 green:44/255.0 blue:31/255.0 alpha:1.0];
    
    self.eventGreenColor = [UIColor colorWithRed:62/255.0 green:194/255.0 blue:45/255.0 alpha:1.0];
    
    greenColor = [UIColor colorWithRed:12/255.0 green:190/255.0 blue:64/255.0 alpha:1.0];
    redColor = [UIColor colorWithRed:255/255.0 green:57/255.0 blue:56/255.0 alpha:1.0];
    
    gregorian = [Utils getGregorianCalendar];
    
    self.minimumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
    self.maximumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:1 toDate:[NSDate date] options:0];
    
    // Sliding up panel
    //    self.panelControllerContainer = (BSReportARSPContainerController *)self.parentViewController;
    //    self.panelControllerContainer.dragDelegate = self;
    //    self.panelControllerContainer.visibilityStateDelegate = self;
    
    self.shadowRadius = 5.0f;
    self.shadowOpacity = 0.5f;
    self.animationDuration = 0.55f;
    
    //    self.panelControllerContainer.dropShadow = YES;
    //    self.panelControllerContainer.shadowRadius = self.shadowRadius;
    //    self.panelControllerContainer.shadowOpacity = self.shadowOpacity;
    //    self.panelControllerContainer.animationDuration = self.animationDuration;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.statusType = self.statusId;
    self.classroomId = self.classroomId;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.backgroundColor = grayColor;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSNameBehaviorScoreTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_up"] forState:UIControlStateNormal];
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_down"] forState:UIControlStateSelected];
    
    dateFormatter = [Utils getDateFormatter];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    consideredDate = [NSDate date];
    self.dateTitleLabel.text = [Utils getThaiDateFormatWithDate:consideredDate];
    
    NSDate *startDate = [DateUtility firstDayOfMonth:consideredDate];
    NSDate *endDate = [DateUtility lastDayOfMonth:consideredDate];
    
    [self getBSHistoryInDateRangeWithStartDate:startDate endDate:endDate];
    
    [self getCalendarListWithDate:consideredDate classid:self.classroomId];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createCalendar {
    
    // Remove all subviews from container
    NSArray *viewsToRemove = self.containerView.subviews;
    for(UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    self.containerView.backgroundColor = grayColor;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    //    CGFloat width = CGRectGetWidth(self.containerView.bounds);
    //    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, height)];
    
    calendar.dataSource = self;
    calendar.delegate = self;
    //calendar.swipeToChooseGesture.enabled = YES;
    calendar.backgroundColor = grayColor;//[UIColor whiteColor];
    //    calendar.appearance.titleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.subtitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.weekdayFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.headerTitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:32.0f];
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
    calendar.appearance.selectionColor = selectionColor;
    calendar.appearance.todayColor = todayColor;
    [self.containerView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 95, 34);
    previousButton.backgroundColor = grayColor;//[UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    nextButton.frame = CGRectMake(CGRectGetWidth(self.containerView.frame) - 95, 5, 95, 34);
    
    nextButton.frame = CGRectMake(self.view.frame.size.width - nextButton.frame.size.width - 95, 5, 95, 34);
    nextButton.backgroundColor = grayColor; //[UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:nextButton];
    self.nextButton = nextButton;
    
    //    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    //
    //    self.panelControllerContainer.visibleZoneHeight = screenHeight - (self.calendar.frame.size.height + self.calendar.frame.origin.y) - (self.headerView.frame.origin.y + self.headerView.frame.size.height);
    //    self.panelControllerContainer.swipableZoneHeight = 0;
    //    self.panelControllerContainer.draggingEnabled = YES;
    //    self.panelControllerContainer.shouldOverlapMainViewController = YES;
    //    self.panelControllerContainer.shouldShiftMainViewController = NO;
    
    [self getCalendarListWithDate:calendar.currentPage classid:self.classroomId];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    //    self.bsReportScoreListViewController = (BSReportScoreListViewController *)self.panelControllerContainer.panelViewController;
    //    self.bsReportScoreListViewController.delegate = self;
    
    [self createCalendar];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//#pragma mark - Drag delegate
//
//- (void)panelControllerWasDragged:(CGFloat)panelControllerVisibility {
//
//}
//
//- (void)panelControllerChangedVisibilityState:(ARSPVisibilityState)state {
//
//}

#pragma mark - CFSCalendarDataSource

- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

#pragma mark - CFSCalendarDelegate

- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
    if([DateUtility sameMonthOfYear:date date2:calendar.currentPage]) {
        return YES;
    }
    
    return NO;
}

- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
    //    if(self.delegate && [self.delegate respondsToSelector:@selector(onSelectDate:)]) {
    //        [self.delegate onSelectDate:date];
    //    }
    
    self.dateTitleLabel.text = [Utils getThaiDateFormatWithDate: date];
    
    if([DateUtility sameDate:date date2:consideredDate]) {
        consideredDate = date;
        
        [self updateBSHistoryArray];
    }
    else {
        consideredDate = date;
        
        NSDate *startDate = [DateUtility firstDayOfMonth:consideredDate];
        NSDate *endDate = [DateUtility lastDayOfMonth:consideredDate];
        
        [self getBSHistoryInDateRangeWithStartDate:startDate endDate:endDate];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd/MM/yyyy";
    NSLog(@"%@", [formatter stringFromDate:date]);
}

#pragma mark - Class buttons event

- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
    
    [self getCalendarListWithDate:previousMonth classid:self.classroomId];
}

- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
    
    [self getCalendarListWithDate:nextMonth classid:self.classroomId];
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date {
    
    if(fillDefaultColors != nil) {
        NSString *key = [dateFormatter stringFromDate:date];
        
        if([fillDefaultColors objectForKey:key] != nil) {
            return [fillDefaultColors objectForKey:key];
        }
    }
    
    return nil;
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(bsHistoryArray == nil) {
        return 0;
    }
    else {
        return bsHistoryArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BSNameBehaviorScoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    BSHistoryModel *model = [bsHistoryArray objectAtIndex:indexPath.row];
    
    if([model getBehaviorType] == 0) { // type add
        cell.scoreLabel.backgroundColor = greenColor;
        cell.scoreLabel.text = [NSString stringWithFormat:@"+%ld", [model getBehaviorScore]];
    } else {
        cell.scoreLabel.backgroundColor = redColor;
        cell.scoreLabel.text = [NSString stringWithFormat:@"-%ld", [model getBehaviorScore]];
    }
    
    cell.nameLabel.text = [model getStudentName];
    cell.behaviorNameLabel.text = [model getBehaviorName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onSelectItem:date:)]) {
    //
    //        BSHistoryModel *model = [bsHistoryArray objectAtIndex:indexPath.row];
    //
    //        [self.delegate onSelectItem:model date:consideredDate];
    //    }
    
    BSHistoryModel *model = [bsHistoryArray objectAtIndex:indexPath.row];
    
    BSReportDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSReportDetailStoryboard"];
    
    //    viewController.classLevelArray = self.classLevelArray;
    //    viewController.classroomArray = self.classroomArray;
    //    viewController.statusArray = self.statusArray;
    //    viewController.selectedClassLevelIndex = self.selectedClassLevelIndex;
    //    viewController.selectedClassroomIndex = self.selectedClassroomIndex;
    //    viewController.selectedStatusIndex = self.selectedStatusIndex;
    //    viewController.classLevelId = self.classLevelId;
    //    viewController.classroomId = self.classroomId;
    //    viewController.statusId = self.statusId;
    //
    //    viewController.selectedHistoryModel = data;
    
    viewController.classLevelArray = self.classLevelArray;
    viewController.classroomArray = self.classroomArray;
    viewController.statusArray = self.statusArray;
    viewController.selectedClassLevelIndex = self.selectedClassLevelIndex;
    viewController.selectedClassroomIndex = self.selectedClassroomIndex;
    viewController.selectedStatusIndex = self.selectedStatusIndex;
    viewController.classLevelId = self.classLevelId;
    viewController.classroomId = self.classroomId;
    viewController.statusId = self.statusId;
    
    viewController.selectedHistoryModel = model;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

#pragma mark - API Caller

- (void)getBSHistoryInDateRangeWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    
    if(self.callGetBSHistoryInDateRangeAPI != nil) {
        self.callGetBSHistoryInDateRangeAPI = nil;
    }
    
    self.callGetBSHistoryInDateRangeAPI = [[CallGetBSHistoryInDateRangeAPI alloc] init];
    self.callGetBSHistoryInDateRangeAPI.delegate = self;
    [self.callGetBSHistoryInDateRangeAPI call:[UserData getSchoolId] classroomId:self.classroomId startDate:startDate endDate:endDate type:self.statusType];
    
    [self showIndicator];
}

#pragma mark - API Delegate

- (void)callGetBSHistoryInDateRangeAPI:(CallGetBSHistoryInDateRangeAPI *)classObj data:(NSArray<BSHistoryModel *> *)data success:(BOOL)success {
    
    [self stopIndicator];
    
    if(success && data != nil) {
        
        if(bsHistoryModelDict != nil) {
            [bsHistoryModelDict removeAllObjects];
            bsHistoryModelDict = nil;
        }
        
        bsHistoryModelDict = [[NSMutableDictionary alloc] init];
        
        for(BSHistoryModel *model in data) {
            
            NSDate *date = [model getDateTime];
            NSString *dateStr = [dateFormatter stringFromDate:date];
            
            if([bsHistoryModelDict objectForKey:dateStr] == nil) {
                NSMutableArray<BSHistoryModel *> *dataArray = [[NSMutableArray alloc] init];
                [dataArray addObject:model];
                
                [bsHistoryModelDict setObject:dataArray forKey:dateStr];
            }
            else {
                NSMutableArray<BSHistoryModel *> *dataArray = [bsHistoryModelDict objectForKey:dateStr];
                
                [dataArray addObject:model];
            }
        }
        
        [self updateBSHistoryArray];
    }
}

#pragma mark - Utility

- (void)updateBSHistoryArray {
    
    if(consideredDate != nil && bsHistoryModelDict != nil) {
        NSString *dateStr = [dateFormatter stringFromDate:consideredDate];
        
        if([bsHistoryModelDict objectForKey:dateStr] != nil) {
            bsHistoryArray = [bsHistoryModelDict objectForKey:dateStr];
        }
        else {
            bsHistoryArray = nil;
        }
        
        [self.tableView reloadData];
    }
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Drawer
- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)moveBack:(id)sender {
    
    BSReportSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSReportSelectClassLevelStoryboard"];
    
    //    viewController.classLevelArray = self.panelControllerContainer.classLevelArray;
    //    viewController.classroomArray = self.panelControllerContainer.classroomArray;
    //    viewController.statusArray = self.panelControllerContainer.statusArray;
    //    viewController.selectedClassLevelIndex = self.panelControllerContainer.selectedClassLevelIndex;
    //    viewController.selectedClassroomIndex = self.panelControllerContainer.selectedClassroomIndex;
    //    viewController.selectedStatusIndex = self.panelControllerContainer.selectedStatusIndex;
    //    viewController.classLevelId = self.panelControllerContainer.classLevelId;
    //    viewController.classroomId = self.panelControllerContainer.classroomId;
    //    viewController.statusId = self.panelControllerContainer.statusId;
    
    viewController.classLevelArray = self.classLevelArray;
    viewController.classroomArray = self.classroomArray;
    viewController.statusArray = self.statusArray;
    viewController.selectedClassLevelIndex = self.selectedClassLevelIndex;
    viewController.selectedClassroomIndex = self.selectedClassroomIndex;
    viewController.selectedStatusIndex = self.selectedStatusIndex;
    viewController.classLevelId = self.classLevelId;
    viewController.classroomId = self.classroomId;
    viewController.statusId = self.statusId;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}




- (IBAction)openDetail:(id)sender {
    
    self.arrowStatement.selected = !self.arrowStatement.selected;
    
    if (self.arrowStatement.isSelected == YES) {
        //        self.topDetailConstraint.constant = -300.0f;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailConstraint.constant = -450.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailConstraint.constant = 0.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = -300.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
        
        
        
    }else{
        //        self.topDetailConstraint.constant = 0.0f;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = -450.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailConstraint.constant = 0.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailConstraint.constant = -300.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
    }
    
}

#pragma mark - BSReportScoreListViewControllerDelegate
- (void)onSelectItem:(BSHistoryModel *)data date:(NSDate *)date {
    
    BSReportDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSReportDetailStoryboard"];
    
    //    viewController.classLevelArray = self.panelControllerContainer.classLevelArray;
    //    viewController.classroomArray = self.panelControllerContainer.classroomArray;
    //    viewController.statusArray = self.panelControllerContainer.statusArray;
    //    viewController.selectedClassLevelIndex = self.panelControllerContainer.selectedClassLevelIndex;
    //    viewController.selectedClassroomIndex = self.panelControllerContainer.selectedClassroomIndex;
    //    viewController.selectedStatusIndex = self.panelControllerContainer.selectedStatusIndex;
    //    viewController.classLevelId = self.panelControllerContainer.classLevelId;
    //    viewController.classroomId = self.panelControllerContainer.classroomId;
    //    viewController.statusId = self.panelControllerContainer.statusId;
    
    viewController.classLevelArray = self.classLevelArray;
    viewController.classroomArray = self.classroomArray;
    viewController.statusArray = self.statusArray;
    viewController.selectedClassLevelIndex = self.selectedClassLevelIndex;
    viewController.selectedClassroomIndex = self.selectedClassroomIndex;
    viewController.selectedStatusIndex = self.selectedStatusIndex;
    viewController.classLevelId = self.classLevelId;
    viewController.classroomId = self.classroomId;
    viewController.statusId = self.statusId;
    
    viewController.selectedHistoryModel = data;
    
//    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}

- (void)getCalendarListWithDate:(NSDate *)date classid:(long long)classid{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getCalendarReportBehaviorWithhUserID:userID date:date classid:classid schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if (error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if (data == nil){
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if ((connectCounter < TRY_CONNECT_MAX)) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getCalendarListWithDate:date classid:classid];
            }
            else{
                connectCounter = 0;
            }
        }
        
        else{
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getCalendarListWithDate:date classid:classid];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getCalendarListWithDate:date classid:classid];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                holidayArray = [[NSMutableArray alloc] init];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *datadict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *holidayStr = [[NSMutableString alloc] initWithFormat:@"%@", [datadict objectForKey:@"daybuy"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)holidayStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *holiday = [formatter dateFromString:holidayStr];
                    
                    BSCalendarModel *model = [[BSCalendarModel alloc] init];
                    
                    [model setBehaviorCalendarList:holiday];
                    
                    [holidayArray addObject:model];
                }
                
                [self updateEventHoliday];
                
            }
            
            
        }
    }];
    
    
}

- (void)updateEventHoliday{
    
    
    if (holidayArray == nil) {
        return;
    }
    
    if (fillDefaultColors != nil) {
        [fillDefaultColors removeAllObjects];
    }
    
    fillDefaultColors = [[NSMutableDictionary alloc] init];
    
    for (BSCalendarModel *model in holidayArray) {
        
        NSString *dateKey = [dateFormatter stringFromDate:model.behaviorCalendarList];
        
        [fillDefaultColors setObject:self.eventGreenColor forKey:dateKey];
        
        
    }
    
    [self.calendar reloadData];
    
}

@end

