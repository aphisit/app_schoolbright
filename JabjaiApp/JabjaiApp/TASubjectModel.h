//
//  TASubjectModel.h
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TASubjectModel : NSObject

@property (nonatomic) NSString *subjectId;
@property (nonatomic) NSString *subjectName;

@end
