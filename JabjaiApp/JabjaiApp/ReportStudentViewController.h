//
//  ReportStudentViewController.h
//  JabjaiApp
//
//  Created by mac on 4/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"
#import "CallGetMenuListAPI.h"

@class ReportStudentViewController;
@protocol ReportStudentViewControllerDelegate <NSObject>

- (void)changePageReportStudentViewController:(ReportStudentViewController *)classObj numberPage:(int)numberPage;
@end
@interface ReportStudentViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate, CallGetMenuListAPIDelegate>

@property (nonatomic, weak) id<ReportStudentViewControllerDelegate> delegate;
@property (strong, nonatomic) NSMutableArray* authorizeMenuArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@end
