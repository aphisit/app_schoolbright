//
//  TFProductModel.h
//  JabjaiApp
//
//  Created by Mac on 5/10/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TFProductModel : NSObject
@property (strong ,nonatomic) NSString *productName;
@property (assign, nonatomic) double productPrice;
@end

NS_ASSUME_NONNULL_END
