//
//  LoginListAllSchoolNameViewController.m
//  JabjaiApp
//
//  Created by toffee on 25/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "LoginListAllSchoolNameDialog.h"

@interface LoginListAllSchoolNameDialog (){
    //Search Bar
    BOOL searchActive;
    UIGestureRecognizer *searchBarCancelGesture;
    NSArray<LoginListAllSchoolNameModel*> *schoolArray;
    NSArray<LoginListAllSchoolNameModel *> *searchStudentArray;
}
@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic,strong) CallLoginListAllSchoolNameAPI *callLoginListAllSchoolNameAPI;
@end
static NSString *cellIdentifier = @"SchoolNameCell";
@implementation LoginListAllSchoolNameDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([LoginListAllSchoolNameTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    self.searchBar.delegate = self;
    [self.searchBar layoutIfNeeded];
    searchActive = NO;
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - CallLoginListAllSchoolNameAPI
- (void)getListAllSchoolName {
    [self showIndicator];
    if(self.callLoginListAllSchoolNameAPI != nil) {
        self.callLoginListAllSchoolNameAPI = nil;
    }
    
    self.callLoginListAllSchoolNameAPI = [[CallLoginListAllSchoolNameAPI alloc] init];
    self.callLoginListAllSchoolNameAPI.delegate = self;
    [self.callLoginListAllSchoolNameAPI call];
}

-(void)callLoginListAllSchoolNameAPI:(CallLoginListAllSchoolNameAPI *)classObj data:(NSArray<LoginListAllSchoolNameModel *> *)data success:(BOOL)success{
    [self stopIndicator];
    if (data != nil && success) {
        schoolArray = data;
    }
    [self.tableView reloadData];
}

-(void)showDialogInView:(UIView *)targetView {
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
    [self getListAllSchoolName];
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        if(self.delegate && [self.delegate respondsToSelector:@selector(touchesBeganClose)]) {
            [self.delegate touchesBeganClose];
        }
        [self dismissDialog];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if(searchActive) {
        if(searchStudentArray != nil) {
            return searchStudentArray.count;
        }
        else {
            return 0;
        }
    }else{
        if(schoolArray != nil) {
            return schoolArray.count;
        }
        else {
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LoginListAllSchoolNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    LoginListAllSchoolNameModel *model;
    if(searchActive) {
        model = [searchStudentArray objectAtIndex:indexPath.row];
    }
    else {
        model = [schoolArray objectAtIndex:indexPath.row];
    }
    cell.schoolNameLabel.text = [model getSchoolName];
    cell.numberLabel.text = [@(indexPath.row+1)stringValue];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(searchActive) {
        if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogClose:)]) {
            [self.delegate onAlertDialogClose:[searchStudentArray objectAtIndex:indexPath.row]];
        }
    }else{
        if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogClose:)]) {
            [self.delegate onAlertDialogClose:[schoolArray objectAtIndex:indexPath.row]];
        }
    }
    [self dismissDialog];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//    {
//        return 50;
//    }else{
//        return 70;
//    }
//}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBarCancelGesture = [UITapGestureRecognizer new];
    [searchBarCancelGesture addTarget:self action:@selector(backgroundTouched:)];
    [self.view addGestureRecognizer:searchBarCancelGesture];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if(searchBarCancelGesture != nil) {
        [self.view removeGestureRecognizer:searchBarCancelGesture];
        searchBarCancelGesture = nil;
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // called when text changes (including clear)
    searchActive = YES;
    if(searchText.length != 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"getSchoolName contains[c] %@", searchText];
        searchStudentArray = [schoolArray filteredArrayUsingPredicate:predicate];
    }
    else {
        searchStudentArray = schoolArray;
    }
    [self.tableView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // called when keyboard search button pressed
    [self.searchBar resignFirstResponder];
}
#pragma mark - Utils
- (void)backgroundTouched:(id)sender {
    [self.view endEditing:YES];
    [self.searchBar resignFirstResponder];

    if(searchActive && self.searchBar.text.length == 0) {
        searchActive = NO;
        [self.tableView reloadData];
    }
}
@end
