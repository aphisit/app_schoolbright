//
//  CallAddPinAPI.m
//  JabjaiApp
//
//  Created by toffee on 6/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallAddPinAPI.h"
#import "APIURL.h"
#import "Utils.h"

@implementation CallAddPinAPI{
    NSInteger connectCounter;
    NSString *responseString;
}

-(void)call:(long long)userId pinCode:(NSString*)pinCode schoolid:(long long)schoolid{
    connectCounter = 0;
    [self doAddPinCode:userId pinCode:pinCode schoolid:schoolid];
}

- (void)doAddPinCode:(long long)userId pinCode:(NSString*)pinCode schoolid:(long long)schoolid {
    NSString *URLString = [APIURL getAddPinCode:userId pinCode:pinCode schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self doAddPinCode:userId pinCode:pinCode schoolid:schoolid];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
                connectCounter = 0;
               // responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSString *status = [returnedData objectForKey:@"resMessage"];
                if(_delegate != nil && [self.delegate respondsToSelector:@selector(callAddPinAPI:status:success:)]) {
                    [self.delegate callAddPinAPI:self status:status success:YES];
                }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGMMenuAPI:data:success:)]) {
                [self.delegate callAddPinAPI:self status:nil success:NO];
            }
        }
        
        
        
        
    }
     ];
    
}

@end
