//
//  RPCheckNameSubjectDateTimeModel.h
//  JabjaiApp
//
//  Created by toffee on 24/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RPCheckNameSubjectDateTimeModel : NSObject

@property (nonatomic) NSString *date;
@property (nonatomic) NSString *time;
@property (nonatomic) NSString *status;

- (void)setDate:(NSString *)date;
- (void)setTime:(NSString *)time;
- (void)setStatus:(NSString *)status;

- (NSString *)getDate;
- (NSString *)getTime;
- (NSString *)getStatus;

@end

NS_ASSUME_NONNULL_END
