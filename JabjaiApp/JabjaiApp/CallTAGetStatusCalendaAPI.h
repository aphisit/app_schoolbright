//
//  CallTAGetStatusCalendaAPI.h
//  JabjaiApp
//
//  Created by Mac on 18/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAStatusCalendaModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallTAGetStatusCalendaAPI;
@protocol CallTAGetStatusCalendaAPIDelegate <NSObject>

- (void)callTAGetStatusCalendaAPI:(CallTAGetStatusCalendaAPI*)classObj data:(NSArray<TAStatusCalendaModel *>*)data success:(BOOL)success;

@end

@interface CallTAGetStatusCalendaAPI : NSObject

@property (nonatomic, weak) id<CallTAGetStatusCalendaAPIDelegate> delegate;
- (void) call:(long long)userID day:(NSDate*)day subjectID:(long long)subjectID schoolID:(long long) schoolID classroomID:(long long)classroomID;
@end

NS_ASSUME_NONNULL_END
