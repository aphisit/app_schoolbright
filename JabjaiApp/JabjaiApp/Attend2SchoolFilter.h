//
//  Attend2SchoolFilter.h
//  JabjaiApp
//
//  Created by mac on 12/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    ALL, INTIME, LATE, ABSENCE, ONLEAVE, HOLIDAY, SICK, PERSONAL, UNCHECK
} ATTENDANCESTATUS;

@interface Attend2SchoolFilter : NSObject

@property (strong, nonatomic) NSNumber *schoolYear;
@property (strong, nonatomic) NSString *semester;
@property (nonatomic) ATTENDANCESTATUS status;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;

@end
