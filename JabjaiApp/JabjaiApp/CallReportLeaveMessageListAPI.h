//
//  CallReportLeaveMessageListAPI.h
//  JabjaiApp
//
//  Created by toffee on 29/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NoticeReportDateMessageModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallReportLeaveMessageListAPI;
@protocol CallReportLeaveMessageListAPIDelegate <NSObject>
- (void)callReportLeaveMessageListAPI:(CallReportLeaveMessageListAPI *)classObj data:(NSArray<NoticeReportDateMessageModel *> *)data  countLeave:(NSString*)countLeave success:(BOOL)success;
@end
@interface CallReportLeaveMessageListAPI : NSObject
@property (nonatomic, weak) id<CallReportLeaveMessageListAPIDelegate> delegate;
-(void)call:(long long)schoolId userId:(long long)userId;
@end

NS_ASSUME_NONNULL_END
