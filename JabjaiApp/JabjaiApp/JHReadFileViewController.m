//
//  JHReadFileViewController.m
//  JabjaiApp
//
//  Created by toffee on 15/7/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "JHReadFileViewController.h"
#import "Utils.h"
#import "AlertDialogConfirm.h"
@interface JHReadFileViewController (){
    NSString *pathURL;
    NSMutableData *mdata;
    AlertDialogConfirm *alertDialog;
    NSTimer *timer;
}

@end

@implementation JHReadFileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.delegate = self;
    // Do any additional setup after loading the view from its nib.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}

#pragma mark - Dialog Functions

-(void)showPagesReadFileInView:(UIView *)targetView partFile:(NSString *)partFile{
    NSLog(@"System Version is %@",[[UIDevice currentDevice] systemVersion]);
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height)];
    }
    [targetView addSubview:self.view];
    pathURL = partFile;
    NSString *html = [NSString stringWithContentsOfFile:partFile encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:html baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]bundlePath]]];
    [self loadRemoteFile:partFile];
}

- (void) loadRemoteFile:(NSString*)partFile
{
    NSString *url = partFile;
    self.webView.autoresizesSubviews = YES;
    self.webView.scalesPageToFit = YES;
    self.webView.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
    [_webView loadHTMLString:[NSString stringWithFormat:@"<script>window.location=%@;</script>",[[NSString alloc]initWithData:[NSJSONSerialization dataWithJSONObject:url options:NSJSONReadingAllowFragments error:NULL]encoding:NSUTF8StringEncoding]]baseURL:nil];
//    NSString* encodedUrl = [partFile stringByAddingPercentEscapesUsingEncoding:
//                            NSUTF8StringEncoding];
//    self.webView.autoresizesSubviews = YES;
//    self.webView.scalesPageToFit = YES;
//    self.webView.autoresizingMask=(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
//    NSURL *myUrl = [NSURL URLWithString:encodedUrl];
//    NSURLRequest *myRequest = [NSURLRequest requestWithURL:myUrl];
//    [self.webView loadRequest:myRequest];
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    self.hideLoadView.hidden = NO;
    [self showIndicator];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    self.hideLoadView.hidden = YES;
    [self stopIndicator];
}

-(void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}
- (IBAction)closeAction:(id)sender {
    [self removeDialogFromView];
}

- (IBAction)saveFileAction:(id)sender {
    self.hideLoadView.hidden = NO;
    [self showIndicator];
    double delayInSeconds = 3.0;
    timer = [NSTimer scheduledTimerWithTimeInterval:delayInSeconds
                                                          target:self
                                                        selector:@selector(delaySendFile)
                                                        userInfo:nil
                                                        repeats:YES];
}

- (void)delaySendFile{
    NSString* encodedUrl = [pathURL stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    NSURL *URL = [NSURL URLWithString:encodedUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request
                                                            completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error){
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSURL *documentsDirectoryURL = [NSURL fileURLWithPath:documentsPath];
    NSURL *documentURL = [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    [[NSFileManager defaultManager] moveItemAtURL:location toURL:documentURL error:nil];
    }];
    [downloadTask resume];
    
    [timer invalidate];
    timer = nil;
    self.hideLoadView.hidden = YES;
    [self stopIndicator];
    //NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_COMPLETED",nil,myLangBundle,nil);
    NSString *alertMessage = @"บันทึกเสร็จสิ้น";
    [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    if(alertDialog == nil) {
        alertDialog = [[AlertDialogConfirm alloc] init];
    }
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *mainView = window.rootViewController.view;
    [alertDialog showDialogInView:mainView title:title message:message];
}


@end
