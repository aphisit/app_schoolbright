//
//  DateUtility.h
//  DateUtilityApp
//
//  Created by mac on 11/6/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtility : NSObject

+ (NSDate *)minDate:(NSArray *)dates;
+ (NSDate *)maxDate:(NSArray *)dates;
+ (BOOL)sameDate:(NSDate *)date1 date2:(NSDate *)date2;
+ (BOOL)sameMonthOfYear:(NSDate *)date1 date2:(NSDate *)date2;
+ (BOOL)betweenDate:(NSDate *)date minDate:(NSDate *)minDate maxDate:(NSDate *)maxDate;
+ (NSDate *)startDateOfMonthAndYearInRangeWithMonth:(NSInteger)month year:(NSInteger)year minDate:(NSDate *)minDate maxDate:(NSDate *)maxDate;
+ (NSDate *)endDateOfMonthAndYearInRangeWithMonth:(NSInteger)month year:(NSInteger)year minDate:(NSDate *)minDate maxDate:(NSDate *)maxDate;
+ (NSInteger)numberOfDaysInMonthCount:(NSDate *)date;
+ (NSDate *)dateWithOutTime:(NSDate *)date;
+ (NSDate *)firstDayOfMonth:(NSDate *)date;
+ (NSDate *)lastDayOfMonth:(NSDate *)date;
@end
