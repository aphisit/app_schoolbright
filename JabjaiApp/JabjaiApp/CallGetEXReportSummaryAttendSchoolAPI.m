  //
//  CallGetEXReportSummaryAttendSchoolAPI.m
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetEXReportSummaryAttendSchoolAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetEXReportSummaryAttendSchoolAPI () {
    NSInteger connectCounter;
    NSMutableArray<EXReportClassStatusModel *> *classStatusArray;
}

@end
@implementation CallGetEXReportSummaryAttendSchoolAPI

- (instancetype)init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(long long)schoolId date:(NSDate *)date {
    [self getDataFromServer:schoolId date:date];
}

#pragma mark - Get API Data

- (void)getDataFromServer:(long long)schoolId date:(NSDate *)date {
    
    NSString *urlString = [APIURL getEXReportSummaryAttendSchoolWithSchoolId:schoolId date:date];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataFromServer:schoolId date:date];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(classStatusArray != nil) {
                    [classStatusArray removeAllObjects];
                    classStatusArray = nil;
                }
                
                classStatusArray = [[NSMutableArray alloc] init];
                
                if(returnedArray.count > 0) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    if(![[dataDict objectForKey:@"takeschoolattendancerepos2level"] isKindOfClass:[NSNull class]] &&
                       ![[dataDict objectForKey:@"takeschoolattendancerepos2theacher"] isKindOfClass:[NSNull class]] &&
                       ![[dataDict objectForKey:@"takeschoolattendancerepos2emplyees"] isKindOfClass:[NSNull class]]) {
                        
                        NSArray *teacherArray = [dataDict objectForKey:@"takeschoolattendancerepos2theacher"];
                        
                        if(teacherArray.count > 0) {
                            NSDictionary *dict = [teacherArray objectAtIndex:0];
                            
                            long long iid = -1; // -1 to indicated whether it's the teacher type
                            NSString *title = @"อาจารย์";
                            NSInteger numberOfOnTimeStatus = [[dict objectForKey:@"status_0"] integerValue];
                            NSInteger numberOfLateStatus = [[dict objectForKey:@"status_1"] integerValue];
                            NSInteger numberOfAbsenceStatus = [[dict objectForKey:@"status_2"] integerValue];
                            NSInteger numberOfEventStatus = [[dict objectForKey:@"status_3"] integerValue];
                            NSInteger numberOfSickLeaveStatus = [[dict objectForKey:@"status_4"] integerValue];
                            NSInteger numberOfPersonalStatus = [[dict objectForKey:@"status_5"] integerValue];
                            
                            EXReportClassStatusModel *model = [[EXReportClassStatusModel alloc] init];
                            [model setIId:iid];
                            [model setTitle:title];
                            [model setNumberOfOnTimeStatus:numberOfOnTimeStatus];
                            [model setNumberOfLateStatus:numberOfLateStatus];
                            [model setNumberOfAbsenceStatus:numberOfAbsenceStatus];
                            [model setNumberOfEventStatus:numberOfEventStatus];
                            [model setNumberOfSickLeaveStatus:numberOfSickLeaveStatus];
                            [model setNumberOfPersonalStatus:numberOfPersonalStatus];
                            
                            [classStatusArray addObject:model];
                        }
                        
                        NSArray *employeeArray = [dataDict objectForKey:@"takeschoolattendancerepos2emplyees"];
                        
                        if(employeeArray.count > 0) {
                            NSDictionary *dict = [employeeArray objectAtIndex:0];
                            
                            long long iid = -2; // -2 to indicated whether it's the employee type
                            NSString *title = @"พนักงาน";
                            NSInteger numberOfOnTimeStatus = [[dict objectForKey:@"status_0"] integerValue];
                            NSInteger numberOfLateStatus = [[dict objectForKey:@"status_1"] integerValue];
                            NSInteger numberOfAbsenceStatus = [[dict objectForKey:@"status_2"] integerValue];
                            NSInteger numberOfEventStatus = [[dict objectForKey:@"status_3"] integerValue];
                            NSInteger numberOfSickLeaveStatus = [[dict objectForKey:@"status_4"] integerValue];
                            NSInteger numberOfPersonalStatus = [[dict objectForKey:@"status_5"] integerValue];
                            
                            EXReportClassStatusModel *model = [[EXReportClassStatusModel alloc] init];
                            [model setIId:iid];
                            [model setTitle:title];
                            [model setNumberOfOnTimeStatus:numberOfOnTimeStatus];
                            [model setNumberOfLateStatus:numberOfLateStatus];
                            [model setNumberOfAbsenceStatus:numberOfAbsenceStatus];
                            [model setNumberOfEventStatus:numberOfEventStatus];
                            [model setNumberOfSickLeaveStatus:numberOfSickLeaveStatus];
                            [model setNumberOfPersonalStatus:numberOfPersonalStatus];
                            
                            [classStatusArray addObject:model];
                        }
                        
                        NSArray *classLevelArray = [dataDict objectForKey:@"takeschoolattendancerepos2level"];
                        
                        for(int i=0; i<classLevelArray.count; i++) {
                            NSDictionary *dict = [classLevelArray objectAtIndex:i];
                            
                            long long iid = [[dict objectForKey:@"levelid"] longLongValue];
                            NSString *title = [dict objectForKey:@"levelname"];
                            NSInteger numberOfOnTimeStatus = [[dict objectForKey:@"status_0"] integerValue];
                            NSInteger numberOfLateStatus = [[dict objectForKey:@"status_1"] integerValue];
                            NSInteger numberOfAbsenceStatus = [[dict objectForKey:@"status_2"] integerValue];
                            NSInteger numberOfEventStatus = [[dict objectForKey:@"status_5"] integerValue];
                            NSInteger numberOfSickLeaveStatus = [[dict objectForKey:@"status_4"] integerValue];
                            NSInteger numberOfPersonalStatus = [[dict objectForKey:@"status_3"] integerValue];
                            
                            EXReportClassStatusModel *model = [[EXReportClassStatusModel alloc] init];
                            [model setIId:iid];
                            [model setTitle:title];
                            [model setNumberOfOnTimeStatus:numberOfOnTimeStatus];
                            [model setNumberOfLateStatus:numberOfLateStatus];
                            [model setNumberOfAbsenceStatus:numberOfAbsenceStatus];
                            [model setNumberOfEventStatus:numberOfEventStatus];
                            [model setNumberOfSickLeaveStatus:numberOfSickLeaveStatus];
                            [model setNumberOfPersonalStatus:numberOfPersonalStatus];
                            
                            [classStatusArray addObject:model];

                        }
                        
                        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetEXReportSummaryAttendSchoolAPI:data:success:)]) {
                            [self.delegate callGetEXReportSummaryAttendSchoolAPI:self data:classStatusArray success:YES];
                        }
                    }
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetEXReportSummaryAttendSchoolAPI:data:success:)]) {
                [self.delegate callGetEXReportSummaryAttendSchoolAPI:self data:nil success:NO];
            }
        }
    }];
}

@end
