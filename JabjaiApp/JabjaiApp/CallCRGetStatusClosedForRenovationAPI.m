//
//  CallCRGetStatusClosedForRenovationAPI.m
//  JabjaiApp
//
//  Created by Mac on 9/2/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "APIURL.h"
#import "Utils.h"
@implementation CallCRGetStatusClosedForRenovationAPI{
    NSInteger connectCounter,statusCode;
    NSString *messageStr;
    
}

-(void) callStatusServer{
    connectCounter = 0;
    [self getStatusServer];
    
}

-(void)getStatusServer{
    NSString *URLString = [APIURL getStatusServer];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }

            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;

                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getStatusServer];
;

            }
            else {
                isFail = YES;
                self->connectCounter = 0;
            }

        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getStatusServer];
;
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
                
            }
            else {
                NSDictionary *dic = returnedData;
                
                self->statusCode = [[dic objectForKey:@"StatusCode"] integerValue];
                if (![[dic objectForKey:@"Message"] isKindOfClass:[NSNull class]]) {
                    self->messageStr = [dic objectForKey:@"Message"] ;
            
                }else{
                    self->messageStr = @"";
                }
                NSLog(@"xxxx");
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callCRGetStatusClosedForRenovationAPI:statusCode:message:success:)]) {
                    [self.delegate callCRGetStatusClosedForRenovationAPI:self statusCode:self->statusCode message:self->messageStr success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callCRGetStatusClosedForRenovationAPI:statusCode:message:success:)]) {
                [self.delegate callCRGetStatusClosedForRenovationAPI:self statusCode:self->statusCode message:self->messageStr success:NO];
            }
        }
     
        
    }
     ];
}



@end
