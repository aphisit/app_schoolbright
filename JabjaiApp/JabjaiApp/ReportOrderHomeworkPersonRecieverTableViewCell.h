//
//  ReportOrderHomeworkPersonRecieverTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 5/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportOrderHomeworkPersonRecieverTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imagePerson;
@property (weak, nonatomic) IBOutlet UILabel *namePerson;
@property (weak, nonatomic) IBOutlet UILabel *statusPerson;


@end
