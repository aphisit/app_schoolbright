//
//  EXReportClassHeaderTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EXReportClassHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;
@end
