//
//  CallReportCheckSubjectDateTimeStudentAPI.h
//  JabjaiApp
//
//  Created by toffee on 23/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPCheckNameSubjectDateTimeModel.h"

NS_ASSUME_NONNULL_BEGIN

@class CallReportCheckSubjectDateTimeStudentAPI;

@protocol CallReportCheckSubjectDateTimeStudentAPIDelegate <NSObject>
@optional

- (void)callReportCheckSubjectDateTimeStudentAPI:(CallReportCheckSubjectDateTimeStudentAPI *)classObj dateTimeArray:(NSMutableArray<RPCheckNameSubjectDateTimeModel *>*)dateTimeArray success:(BOOL)success;
@end

@interface CallReportCheckSubjectDateTimeStudentAPI : NSObject

@property (nonatomic, weak) id<CallReportCheckSubjectDateTimeStudentAPIDelegate> delegate;
- (void)call:(NSString*)studentId date:(NSString*)date subjectId:(NSString*)subjectId schoolId:(long long)schoolId;
@end

NS_ASSUME_NONNULL_END
