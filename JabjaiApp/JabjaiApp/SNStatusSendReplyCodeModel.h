//
//  SNStatusSendReplyCodeModel.h
//  JabjaiApp
//
//  Created by Mac on 30/4/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SNStatusSendReplyCodeModel : NSObject
@property (nonatomic, assign) NSInteger statusCode;
@property (nonatomic, strong) NSString *message;
@end

NS_ASSUME_NONNULL_END
