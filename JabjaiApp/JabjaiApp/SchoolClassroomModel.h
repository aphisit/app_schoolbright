//
//  SchoolClassroomModel.h
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SchoolClassroomModel : NSObject

@property (nonatomic, strong) NSNumber *classroomID;
@property (nonatomic, strong) NSString *classroomName;

-(NSNumber *)getClassroomID;
-(NSString *)getClassroomName;

@end
