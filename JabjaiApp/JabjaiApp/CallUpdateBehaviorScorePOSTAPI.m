//
//  CallUpdateBehaviorScorePOSTAPI.m
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallUpdateBehaviorScorePOSTAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"
#import <AFNetworking/AFNetworking.h>

@implementation CallUpdateBehaviorScorePOSTAPI

- (void)call:(long long)schoolId teacherId:(long long)teacherId jsonString:(NSString *)jsonString {
    [self updateStudentBehaviorScore:schoolId teacherId:teacherId jsonString:jsonString];
}

- (void)updateStudentBehaviorScore:(long long)schoolId teacherId:(long long)teacherId jsonString:(NSString *)jsonString {
    
    NSString *URLString = [APIURL getUpdateStudentBehaviorScorePOSTWithSchoolId:schoolId teacherId:teacherId];
   
    NSError *error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&error];
    NSLog(@"Json = %@", json);
    NSLog(@"ObjectData = %@", objectData);
    NSLog(@"jsonString = %@", jsonString);

    if(error != nil) {
        NSLog(@"%@", @"Parse JSON data failed");
    }
    else {
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"%@", responseObject);
            
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callUpdateBehaviorScorePOSTAPI:response:success:)]) {
                NSString *response = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"status"]];
                [self.delegate callUpdateBehaviorScorePOSTAPI:self response:response success:YES];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callUpdateBehaviorScorePOSTAPI:response:success:)]) {
                [self.delegate callUpdateBehaviorScorePOSTAPI:self response:nil success:NO];
            }
            
        }];
        
    }
}

@end
