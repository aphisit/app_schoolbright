//
//  JHSubjectModel.h
//  JabjaiApp
//
//  Created by toffee on 11/22/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JHSubjectModel : NSObject

@property (assign, nonatomic) NSString *subjectID;
@property (strong, nonatomic) NSString *subjectName;

-(NSString *)getSubjectID;
-(NSString *)getSubjectName;

@end
