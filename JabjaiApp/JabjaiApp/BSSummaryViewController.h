//
//  BSSummaryViewController.h
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "BSSelectedStudent.h"
#import "BehaviorScoreModel.h"
#import "AlertDialog.h"
#import "AlertDialogConfirm.h"
#import "CallUpdateBehaviorScorePOSTAPI.h"
#import "BSListNameUserSummaryTableViewCell.h"
#import "SlideMenuController.h"
#import "BSSummaryImageCollectionViewCell.h"

@interface BSSummaryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate,UICollectionViewDataSource,AlertDialogConfirmDelegate, CallUpdateBehaviorScorePOSTAPIDelegate, SlideMenuControllerDelegate>

// Global variables
// BSSelectClassLevelViewController
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger mode;
@property (strong, nonatomic) NSMutableArray *imageArray;
@property (nonatomic) int status;

// BSSelectStudentsViewController
@property (strong, nonatomic) NSArray<BSSelectedStudent *> *selectedStudentArray;

// BSSelectBehaviorScoreViewController
@property (strong, nonatomic) NSArray<BehaviorScoreModel *> *behaviorScoreArray;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *listNameView;
@property (weak, nonatomic) IBOutlet UIView *behaviorScoreView;
@property (weak, nonatomic) IBOutlet UIView *imageBehaviorScoreView;


@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerListNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerBehaviorLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerImageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *IconImage;


@property (weak, nonatomic) IBOutlet UITableView *tableViewListOfNames;
@property (weak, nonatomic) IBOutlet UITableView *tableViewBehaviorScore;
@property (weak, nonatomic) IBOutlet UICollectionView *imageCollectionView;


@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *totalScoreLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightUserConstraintView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightScoreConstraintView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightImgCollectionViewConstraintView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTotalScoreConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTableViewScoreConstraint;


- (IBAction)actionSubmit:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
