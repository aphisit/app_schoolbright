//
//  JHClassLevelViewController.h
//  JabjaiApp
//
//  Created by toffee on 11/21/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "TableListDialog.h"
#import "JHSelectRoomsTableListDialog.h"
#import "JHSubjectTableListDialog.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "CallGetSchoolClassroomJForobHomeAPI.h"
#import "CallJHGetSubjectAPI.h"
#import "CallJHGetStudentInClassroomAPI.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "JHSelectRoomRadioModel.h"
#import "SlideMenuController.h"
#import "UnAuthorizeMenuDialog.h"
#import "CallGetMenuListAPI.h"
#import "LoginViewController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
static NSInteger JH_MODE_PERSON = 0;
static NSInteger JH_MODE_ROOM = 1;
@interface JHClassLevelViewController : UIViewController<UITextFieldDelegate, TableListDialogDelegate, JHSubjectTableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomJForobHomeAPIDelegate, CallJHGetSubjectAPIDelegate, CallJHGetStudentInClassroomAPIDelegate,JHSelectRoomTableListDialogDelegate, SlideMenuControllerDelegate,UnAuthorizeMenuDialogDelegate,CallGetMenuListAPIDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>

//Global variables
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<JHSelectRoomRadioModel *> *classroomGroupArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<JHSubjectModel *> *subjectArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
@property (nonatomic) NSInteger mode;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *showheader;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSubjectLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIView *section2;
@property (weak, nonatomic) IBOutlet UIView *section3;

@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classroomTextField;
@property (weak, nonatomic) IBOutlet UITextField *subjectTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)openDrawer:(id)sender;
- (IBAction)actionNext:(id)sender;

@end
