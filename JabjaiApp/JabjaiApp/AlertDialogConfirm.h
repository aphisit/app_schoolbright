//
//  AlertDialogConfirm.h
//  JabjaiApp
//
//  Created by toffee on 1/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AlertDialogConfirmDelegate <NSObject>
@optional
- (void)onAlertDialogClose;

@end

@interface AlertDialogConfirm : UIViewController
@property (retain, nonatomic) id<AlertDialogConfirmDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dialogTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dialogBottomConstraint;
- (IBAction)closeDialog:(id)sender;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end
