//
//  RPOriginPinCodeViewController.h
//  JabjaiApp
//
//  Created by toffee on 6/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallConfirmPinAPI.h"
#import "ResetPinCodeViewController.h"
#import "UserInfoViewController.h"
#import "SlideMenuController.h"
//#import "SettingViewController.m"


@interface RPOriginPinCodeViewController : UIViewController <CallConfirmPinAPIDelegate, ResetPinCodeViewControllerDelegate, SlideMenuControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPinLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerIncorrectPinLabel;


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UILabel *confirmPinUnfair;
@property (weak, nonatomic) IBOutlet UIView *passView1;
@property (weak, nonatomic) IBOutlet UIView *passView2;
@property (weak, nonatomic) IBOutlet UIView *passView3;
@property (weak, nonatomic) IBOutlet UIView *passView4;
@property (weak, nonatomic) IBOutlet UIView *passView5;
@property (weak, nonatomic) IBOutlet UIView *passView6;

- (IBAction)actionButtonNumber:(UIControl*)sender;
- (IBAction)moveBack:(id)sender;


@end
