//
//  CallLoginListAllSchoolNameAPI.m
//  JabjaiApp
//
//  Created by toffee on 26/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallLoginListAllSchoolNameAPI.h"
#import "APIURL.h"
#import "Utils.h"
@implementation CallLoginListAllSchoolNameAPI{
     NSInteger connectCounter;
    NSMutableArray<LoginListAllSchoolNameModel *> *schoolArray;

}
-(void)call{
    [self getAllSchool];
}
-(void)getAllSchool{
    NSString *URLString = [APIURL getListAllSchool];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            BOOL isFail = NO;
            
            if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

                if(error != nil) {
                    NSLog(@"An error occured : %@" , [error localizedDescription]);
                }
                else if(data == nil) {
                    NSLog(@"Data is null");
                }
                else {
                    NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                    NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
                }

                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;

                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getAllSchool];

                }
                else {

                    isFail = YES;
                    self->connectCounter = 0;
                }

            }
            else {
                id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if(error != nil) {
                    NSLog(@"An error occured : %@", [error localizedDescription]);
                    
                    if(self->connectCounter < TRY_CONNECT_MAX) {
                        self->connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                        [self getAllSchool];
                    }
                    else {
                        isFail = YES;
                        self->connectCounter = 0;
                    }
                    
                }
                else if(![returnedData isKindOfClass:[NSArray class]]) {
                    
                    if(self->connectCounter < TRY_CONNECT_MAX) {
                        self->connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                        [self getAllSchool];
                    }
                    else {
                        isFail = YES;
                        self->connectCounter = 0;
                    }
                }
                else {
                    
                    NSArray *returnedArray = returnedData;
                    self->connectCounter = 0;
                    
                    if(self->schoolArray != nil) {
                        [self->schoolArray removeAllObjects];
                        self->schoolArray = nil;
                    }
                    self->schoolArray = [[NSMutableArray alloc] init];
                    for(int i=0; i<returnedArray.count; i++) {
                        NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                        NSString *schoolName;
                        long long schoolID = [[dataDict objectForKey:@"SchoolID"] longLongValue];
                        
                        if ([[UserData getChangLanguage] isEqualToString:@"th"]) {
                            if(![[dataDict objectForKey:@"SchoolName"] isKindOfClass:[NSNull class]]) {
                                schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolName"]];
                                if ([schoolName isEqual:@""]) {
                                    schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolNameEN"]];
                                }
                            }
                            else {
                                schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolNameEN"]];
                            }
                        }else{
                            if(![[dataDict objectForKey:@"SchoolNameEN"] isKindOfClass:[NSNull class]]) {
                                schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolNameEN"]];
                                if ([schoolName isEqual:@""]) {
                                    schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolName"]];
                                }
                            }
                            else {
                                schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolName"]];
                            }
                        }
                       
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) schoolName);
                        LoginListAllSchoolNameModel *model = [[LoginListAllSchoolNameModel alloc] init];
                        [model setSchoolID:schoolID];
                        [model setSchoolName:schoolName];
                        [self->schoolArray addObject:model];
                    }

                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callLoginListAllSchoolNameAPI:data:success:)]) {
                        [self.delegate callLoginListAllSchoolNameAPI:self data:schoolArray success:YES];
                    }
                }
                
            }
            
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callLoginListAllSchoolNameAPI:data:success:)]) {
                    [self.delegate callLoginListAllSchoolNameAPI:self data:schoolArray success:NO];
                }
            }
         
            
        }
         ];
}
@end
