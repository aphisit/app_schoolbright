//
//  MGPasscodeViewController.h
//  JabjaiApp
//
//  Created by toffee on 6/7/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "CallAddPinAPI.h"
#import "MGListMargetViewController.h"
@class MGPasscodeViewController;
@protocol MGPasscodeViewControllerDelegate <NSObject>


@end
static NSString *ADD_PINCODE = @"ADD_PINCODE";
static NSString *CONFIRM_PINCODE = @"CONFIRM_PINCODE";
@interface MGPasscodeViewController : UIViewController <SlideMenuControllerDelegate, CallAddPinAPIDelegate>

@property (nonatomic, weak) id<MGPasscodeViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UILabel *headerPinLabel;
@property (strong, nonatomic) IBOutlet UILabel *confirmPinUnfair;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;



@property (weak, nonatomic) IBOutlet UIView *passView1;
@property (weak, nonatomic) IBOutlet UIView *passView2;
@property (weak, nonatomic) IBOutlet UIView *passView3;
@property (weak, nonatomic) IBOutlet UIView *passView4;
@property (weak, nonatomic) IBOutlet UIView *passView5;
@property (weak, nonatomic) IBOutlet UIView *passView6;

@property (weak, nonatomic) IBOutlet UIButton *numberButton;

//- (void)showDialogInView:(UIView *)targetView data:(NSDictionary *)data;

- (IBAction)actionButtonNumber:(UIControl*)sender;
- (IBAction)openDarwer:(id)sender;


@end
