//
//  BSReportCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import ARSlidingPanel;

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CFSCalendar.h"
#import "BSReportScoreListViewController.h"
#import "BSReportScoreListViewControllerDelegate.h"
#import "BSReportCalendarViewControllerDelegate.h"

#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "BSStatusModel.h"

#import "BSHistoryModel.h"
#import "CallGetBSHistoryInDateRangeAPI.h"

#import "SlideMenuController.h"


//@interface BSReportCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, ARSPDragDelegate, ARSPVisibilityStateDelegate, BSReportScoreListViewControllerDelegate>

@interface BSReportCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, CFSCalendarDelegateAppearance, UITableViewDataSource, UITableViewDelegate ,CallGetBSHistoryInDateRangeAPIDelegate, SlideMenuControllerDelegate>

//@property (weak, nonatomic) id<BSReportCalendarViewControllerDelegate> delegate;

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<BSStatusModel *> *statusArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedStatusIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger statusId;

@property (nonatomic) NSInteger statusType;

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;

@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)openDrawer:(id)sender;
- (IBAction)moveBack:(id)sender;
- (IBAction)openDetail:(id)sender;

@end
