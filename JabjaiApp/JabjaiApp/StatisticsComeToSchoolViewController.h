//
//  StatisticsComeToSchoolViewController.h
//  JabjaiApp
//
//  Created by Mac on 17/7/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Charts/Charts.h>
#import <MKDropdownMenu/MKDropdownMenu.h>
#import "CallStatisticsComeToSchoolAPI.h"
#import "TableListWithHeaderDialog.h"
#import "TableListDialog.h"
#import "specifyDateDialog.h"
//#import "CreditBureauGraphDialog.h"
NS_ASSUME_NONNULL_BEGIN
@class StatisticsComeToSchoolViewController;
@protocol StatisticsComeToSchoolDelegate
-(void) removeMKDropdownMenu:(MKDropdownMenu*)mKDropdownMenu;
//-(void) xx:(NSString *)title status:(enum ATTENDSTATUS)status times:(NSInteger)times totalTimes:(NSInteger)totalTimes;
@end
@interface StatisticsComeToSchoolViewController : UIViewController <CallStatisticsComeToSchoolAPIDelegate,MKDropdownMenuDelegate,MKDropdownMenuDataSource,specifyDateDialogDelegate>
@property (nonatomic, retain) id<StatisticsComeToSchoolDelegate> delegate;
@property (weak, nonatomic) IBOutlet PieChartView *pieChart;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *dateTimeDropdown;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UIView *onTimeColorView;
@property (weak, nonatomic) IBOutlet UIView *lateColorView;
@property (weak, nonatomic) IBOutlet UIView *absenceColorView;
@property (weak, nonatomic) IBOutlet UIView *eventColorView;
@property (weak, nonatomic) IBOutlet UIView *sickColorView;
@property (weak, nonatomic) IBOutlet UIView *personalColorView;
@property (weak, nonatomic) IBOutlet UIView *uncheckColorView;
@property (weak, nonatomic) IBOutlet UIView *otherColorView;

@property (weak, nonatomic) IBOutlet UILabel *onTimeAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *lateAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *absenceAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sickAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *uncheckAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *otherAmountLabel;


@property (weak, nonatomic) IBOutlet UILabel *onTimePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *latePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *absencePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *sickPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *unCheckPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *otherPercentLabel;


@property (weak, nonatomic) IBOutlet UILabel *onTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *lateLabel;
@property (weak, nonatomic) IBOutlet UILabel *absenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventLabel;
@property (weak, nonatomic) IBOutlet UILabel *sickLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalLabel;
@property (weak, nonatomic) IBOutlet UILabel *uncheckLabel;
@property (weak, nonatomic) IBOutlet UILabel *otherLabel;

@property (weak, nonatomic) IBOutlet UIView *onTimeDialogView;
@property (weak, nonatomic) IBOutlet UIView *lateDialogView;
@property (weak, nonatomic) IBOutlet UIView *absenceDialogView;
@property (weak, nonatomic) IBOutlet UIView *eventDialogView;
@property (weak, nonatomic) IBOutlet UIView *sickDialogView;
@property (weak, nonatomic) IBOutlet UIView *personalDialogView;
@property (weak, nonatomic) IBOutlet UIView *uncheckDialogView;
@property (weak, nonatomic) IBOutlet UIView *otherDialogView;




@end

NS_ASSUME_NONNULL_END
