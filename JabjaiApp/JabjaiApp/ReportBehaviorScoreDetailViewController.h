//
//  ReportBehaviorScoreDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 4/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BSHistoryModel.h"
#import "SlideMenuController.h"

@interface ReportBehaviorScoreDetailViewController : UIViewController <SlideMenuControllerDelegate>

// Global variables
@property (strong, nonatomic) NSArray<BSHistoryModel *> *bsHistoryArray;
@property (nonatomic) double remainingScore;
@property (nonatomic) BSHistoryModel *selectedHistoryModel;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRecorderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerNoteLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRemainingScoreLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *behaviorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *datetimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *recorderLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingScoreLabel;
@property (weak, nonatomic) IBOutlet UIView *remainingScoreView;

- (IBAction)moveBack:(id)sender;

@end
