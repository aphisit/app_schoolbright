//
//  CallGetAddBehaviorScoreListAPI.m
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetAddBehaviorScoreListAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetAddBehaviorScoreListAPI () {
    NSInteger connectCounter;
    NSMutableArray<BehaviorScoreModel *> *behaviorScoreArray;
}

@end

@implementation CallGetAddBehaviorScoreListAPI

- (id)init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(long long)schoolId {
    [self getAddBehaviorScoreList:schoolId];
}

#pragma mark - Get API Data

- (void)getAddBehaviorScoreList:(long long)schoolId {
    
    NSString *URLString = [APIURL getAddBehaviorScoreListWithSchoolId:schoolId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getAddBehaviorScoreList:schoolId];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getAddBehaviorScoreList:schoolId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getAddBehaviorScoreList:schoolId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {

                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(behaviorScoreArray != nil) {
                    [behaviorScoreArray removeAllObjects];
                    behaviorScoreArray = nil;
                }
                
                behaviorScoreArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long behaviorId = [[dataDict objectForKey:@"BehaviorsId"] longLongValue];
                    NSInteger score = [[dataDict objectForKey:@"Score"] integerValue];
                    
                    NSMutableString *behaviorScoreName;
                    
                    if(![[dataDict objectForKey:@"BehaviorsName"] isKindOfClass:[NSNull class]]) {
                        behaviorScoreName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"BehaviorsName"]];
                    }
                    else {
                        behaviorScoreName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) behaviorScoreName);
                    
                    BehaviorScoreModel *model = [[BehaviorScoreModel alloc] init];
                    [model setBehaviorScoreType:0]; // type 0: add
                    [model setBehaviorScoreId:behaviorId];
                    [model setBehaviorScoreName:behaviorScoreName];
                    [model setScore:score];
                    [model setSelected:NO];
                    
                    [behaviorScoreArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetAddBehaviorScoreListAPI:data:success:)]) {
                    [self.delegate callGetAddBehaviorScoreListAPI:self data:behaviorScoreArray success:YES];
                }
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetAddBehaviorScoreListAPI:data:success:)]) {
                [self.delegate callGetAddBehaviorScoreListAPI:self data:nil success:NO];
            }
        }
        
    }];
}

@end
