//
//  MargetListTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 4/10/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MargetListTableViewCellDelegate <NSObject>
@end
@interface MargetListTableViewCell : UITableViewCell

@property (weak, nonatomic) id<MargetListTableViewCellDelegate> delegate;
@property (nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UIImageView *margetImage;
@property (weak, nonatomic) IBOutlet UILabel *margetName;
@property (weak, nonatomic) IBOutlet UIView *sectionView;
    

@end
