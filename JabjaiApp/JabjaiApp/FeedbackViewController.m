//
//  FeedbackViewController.m
//  JabjaiApp
//
//  Created by mac on 5/18/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import UITextView_Placeholder;

#import "FeedbackViewController.h"
#import "SettingViewController.h"
#import "SWRevealViewController.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"
#import "UserData.h"
#import "AlertDialogConfirm.h"

#import "UserInfoViewController.h"

static NSString *placeHolderText = @"โปรดระบุปัญหา หรือข้อแนะนำของคุณ";

@interface FeedbackViewController () {
    
    UIAlertController *actionSheetAlertController;
    UIImage *chosenImage;
    NSURL *chosenImageURL;
    
    AlertDialog *alertDialog;
    AlertDialogConfirm *alertDialogSuccess;
    
    NSInteger connectCounter;
    BOOL backtoSetting;
}

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    [self.feedbackTextView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:NULL];
    self.feedbackTextView.placeholder = placeHolderText;
    
    // Add gesture recognizer for hide keyboard when tap outside keyboard
    UITapGestureRecognizer *hideKeyboardRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:hideKeyboardRecognizer];
    
    [self setupSelectPhotoSourceUsingActionSheet];
    
    connectCounter = 0;
    backtoSetting = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    
    if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Device has not camera" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:cancelButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    //Align textview content center vertically
    UITextView *tv = object;
    CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])/2.0;
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
    [tv setContentInset:UIEdgeInsetsMake(topCorrect,0,0,0)];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Keyboard
- (void)hideKeyboard {
    [self.feedbackTextView resignFirstResponder];
    [self.phoneNumberTextField resignFirstResponder];
}

#pragma mark - <UIImagePickerControllerDelegate>
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    chosenImageURL = info[UIImagePickerControllerReferenceURL];
    
    self.uploadPictureButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.uploadPictureButton setImage:chosenImage forState:UIControlStateNormal];
    [self.uploadPictureButton setBackgroundImage:nil forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private manage photo

- (void)takePhotoFromCamera {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)takePhotoFromExistingSource {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
    
}

- (void)setupSelectPhotoSourceUsingActionSheet {
    
    actionSheetAlertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *takePhotoFromCameraAction = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self takePhotoFromCamera];
    }];
    
    UIAlertAction *choosePhotoFromGalleryAction = [UIAlertAction actionWithTitle:@"Choose Existing" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self takePhotoFromExistingSource];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheetAlertController addAction:takePhotoFromCameraAction];
    [actionSheetAlertController addAction:choosePhotoFromGalleryAction];
    [actionSheetAlertController addAction:cancelAction];
}

#pragma mark - Utility

- (BOOL)validateData {
    
    NSString *feedBack = self.feedbackTextView.text;
    feedBack = [feedBack stringByReplacingOccurrencesOfString:@"โปรดระบุปัญหา หรือข้อแนะนำของคุณ" withString:@""];
    NSMutableString *feedBackMutableStr = [[NSMutableString alloc] initWithFormat:@"%@", feedBack];
    CFStringTrimWhitespace((__bridge CFMutableStringRef) feedBackMutableStr);
    
    if(feedBackMutableStr.length == 0) {
        
        backtoSetting = NO;
        
        NSString *alertMessage = @"กรุณาระบุข้อเสนอแนะ";
        [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
        
        return NO;
    }
    else if(self.phoneNumberTextField.text.length != 0 && self.phoneNumberTextField.text.length > 15) {
        
        backtoSetting = NO;
        
        NSString *alertMessage = @"ขออภัยระบบไม่รองรับหมายเลขโทรศัพท์ความยาวมากกว่า 15 ตัวอักษร";
        [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
        
        return NO;
    }
    else {
        
        backtoSetting = YES;
        
        return YES;
    }
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

#pragma mark - Dialog
- (void)showAlertDialogSuccess:(NSString *)title message:(NSString *)message {
    
    if(alertDialogSuccess == nil) {
        alertDialogSuccess = [[AlertDialogConfirm alloc] init];
        alertDialogSuccess.delegate = self;
    }
    if(alertDialogSuccess != nil && [alertDialogSuccess isDialogShowing]) {
        [alertDialogSuccess dismissDialog];
    }
    [alertDialogSuccess showDialogInView:self.view title:title message:message];
}

#pragma mark - AlertDialogDelegate
- (void)onAlertDialogClose {
    
//    if(backtoSetting) {
//        [self moveBack:self.submitButton];
//    }
    
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (void)enableSubmitButton {
    [self.submitButton setUserInteractionEnabled:YES];
}

- (void)disableSubmitButton {
    [self.submitButton setUserInteractionEnabled:NO];
}

#pragma mark - Upload data via API
- (void)uploadFeedbackToServer {
    
    static NSString *KEY_IMAGE = @"image";
    static NSString *KEY_MESSAGE = @"message";
    static NSString *KEY_PHONE = @"phone";
    static NSString *KEY_USERID = @"userid";
    
    NSString *urlString = [APIURL getFeedBackPOSTURL];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *userID = [[NSString alloc] initWithFormat:@"%lld", (long long)[UserData getUserID]];
    NSData *imageData = nil;
    NSString *imageFileName = nil;
    
    if(chosenImage != nil) {
        imageData = UIImageJPEGRepresentation(chosenImage, 1.0f);
    }
    
    if(chosenImageURL != nil) {
        imageFileName = [chosenImageURL lastPathComponent];
    }
    else {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HHmmss";
        
        imageFileName = [[NSString alloc] initWithFormat:@"%@_%@.jpg", userID, [dateFormatter stringFromDate:[NSDate date]]];
    }
    
    NSString *feedBack = self.feedbackTextView.text;
    feedBack = [feedBack stringByReplacingOccurrencesOfString:@"โปรดระบุปัญหา หรือข้อแนะนำของคุณ" withString:@""];
    NSMutableString *feedBackStr = [[NSMutableString alloc] initWithFormat:@"%@", feedBack];
    CFStringTrimWhitespace((__bridge CFMutableStringRef) feedBackStr);
    
    NSMutableString *phoneNumberStr;
    
    if(self.phoneNumberTextField.text.length > 0) {
        phoneNumberStr = [[NSMutableString alloc] initWithFormat:@"%@", self.phoneNumberTextField.text];
        CFStringTrimWhitespace((__bridge CFMutableStringRef) phoneNumberStr);
    }
    
    
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:KEY_USERID];
    [parameters setObject:feedBackStr forKey:KEY_MESSAGE];
    
    if(phoneNumberStr != nil) {
        [parameters setObject:phoneNumberStr forKey:KEY_PHONE];
    }
    
    [Utils uploadImageFromURL:url data:imageData imageparameterName:KEY_IMAGE imageFileName:imageFileName mimeType:JPEG parameters:parameters withCompletionHandler:^(id responseObject, NSError *error) {
        
        BOOL isUploadFailed = NO;
        if(error != nil) {
            
            NSLog(@"%@", [error localizedDescription]);
            
            if(connectCounter < TRY_CONNECT_MAX) {
                
                connectCounter++;
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                
                [self uploadFeedbackToServer];
            }
            else {
                isUploadFailed = YES;
            }
        }
        else {
            
            connectCounter = 0;
            backtoSetting = YES;
            
            [self stopIndicator];
            [self enableSubmitButton];
            
            NSLog(@"responseObject = %@", responseObject);
            NSString *alertMessage = @"ส่งรายการข้อเสนอแนะเสร็จเรียบร้อย";
            [self showAlertDialogSuccess:@"แจ้งเตือน" message:alertMessage];
            
            return;
        }
        
        if(isUploadFailed) {
            
            connectCounter = 0;
            backtoSetting = NO;
            
            [self stopIndicator];
            [self enableSubmitButton];
            
            NSString *alertMessage = @"ขออภัยเกิดข้อผิดพลาดในการทำรายการ โปรดลองใหม่";
            [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
        }
        
    }];
}

#pragma mark - Actions

- (IBAction)moveBack:(id)sender {
    
    SettingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}

- (IBAction)uploadPicture:(id)sender {
    
    [self presentViewController:actionSheetAlertController animated:YES completion:nil];
    
}

- (IBAction)submitFeedback:(id)sender {
    
    if([self validateData]) {
        [self disableSubmitButton];
        [self showIndicator];
        [self uploadFeedbackToServer];
    }
    
}

- (IBAction)dismissKeyboard:(id)sender {
    [self resignFirstResponder];
}

- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}
@end
