//
//  BSAlertStatusDialog.h
//  JabjaiApp
//
//  Created by toffee on 1/31/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AlertDialogStatusDelegate <NSObject>
@optional
- (void)onAlertDialogClose;

@end

@interface BSAlertStatusDialog : UIViewController
@property (retain, nonatomic) id<AlertDialogStatusDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconStatus;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dialogTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dialogBottomConstraint;
- (IBAction)closeDialog:(id)sender;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message mode:(NSInteger)mode;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
