//
//  ARSubjectScoreModel.m
//  JabjaiApp
//
//  Created by toffee on 19/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "ARSubjectScoreModel.h"

@implementation ARSubjectScoreModel

@synthesize courseCode = _courseCode;
@synthesize courseName = _courseName;
@synthesize status = _status;
@synthesize score = _score;
@synthesize grade = _grade;

-(void)setCourseCode:(NSString *)courseCode{
    _courseCode = courseCode;
}
-(void)setCourseName:(NSString *)courseName{
    _courseName = courseName;
}

-(void)setStatus:(NSString *)status{
    _status = status;
}

-(void)setScore:(NSString*)score{
    _score = score;
}
-(void)setGrade:(NSString*)grade{
    _grade = grade;
}

-(NSString*)getCourseCode{
    return _courseCode;
}
-(NSString*)getCourseName{
    return _courseName;
}
-(NSString*)getStatus{
    return _status;
}
-(NSString*)getScore{
    return _score;
}
-(NSString*)getGrade{
    return _grade;
}

@end
