//
//  ReportInOutStatusDepartmentTableViewCell.m
//  JabjaiApp
//
//  Created by toffee on 7/3/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutStatusDepartmentTableViewCell.h"
#import "UserData.h"
#import "Utils.h"
@interface ReportInOutStatusDepartmentTableViewCell () {
    UIColor *greenColor;
    UIColor *yellowColor;
    UIColor *redColor;
    UIColor *pinkColor;
    UIColor *lightBluecolor;
    UIColor *blueColor;
    UIColor *grayColor;
    CAGradientLayer *gradient;
    NSBundle *myLangBundle;
}
@end
@implementation ReportInOutStatusDepartmentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat max = MAX(self.statusButton.frame.size.width, self.statusButton.frame.size.height);
    self.statusButton.layer.cornerRadius = max/2.0;
    self.statusButton.layer.masksToBounds = YES;
    self.statusButton.titleLabel.numberOfLines = 0;
    
    self.picDepartmentImage.layer.cornerRadius = self.picDepartmentImage.frame.size.height /2;
    self.picDepartmentImage.layer.masksToBounds = YES;
    self.picDepartmentImage.layer.borderWidth = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateStatus:(int)status {
    gradient = [CAGradientLayer layer];
    UIImage * backgroundColorImage;
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource: [UserData getChangLanguage] ofType:@"lproj"]];
    switch (status) {
        case 0:
            
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"green"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            
            break;
        case 1:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_LATE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"yellow"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 3:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ABSENCE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"red"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 4:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_PERSONAL_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 5:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SICK_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"pink"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
            
        case 10:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_PERSONAL_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 11:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SICK_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"pink"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 21:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_MATERNITY",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purplesoft"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 22:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_VACATION",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purplesoft"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 23:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_ORDINATION",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purplesoft"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 24:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_MILITARY",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purplesoft"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 25:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_STUDY",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purplesoft"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 26:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_GOVERNMENT",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purplesoft"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 9:case 8:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_HOLIDAY",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"orange"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        default:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_AUTHER",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"gray"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
    }
    
}

@end
