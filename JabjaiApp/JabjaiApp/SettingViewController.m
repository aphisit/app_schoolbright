//
//  SettingViewController.m
//  JabjaiApp
//
//  Created by mac on 12/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SettingViewController.h"
#import "SWRevealViewController.h"
#import "ProfileViewController.h"
#import "FeedbackViewController.h"
#import "SettingTableViewCell.h"
#import "APIURL.h"
#import "Utils.h"
#import "Constant.h"
#import "AddAccountMainViewController.h"
#import "RPOriginPinCodeViewController.h"
#import "AboutViewController.h"
#import "ReportInOutAllDataViewController.h"
#import "SettingShowIDCardSomeTableViewCell.h"
#import "TFTuitionDetailViewController.h"
@interface SettingViewController () {
    NSInteger connectCounter;
    //NSBundle *myLangBundle;
}

@property (strong, nonatomic) ChangeFingerprintAlertDialog *changeFingerprintAlertDialog;
@property (strong, nonatomic) ChangeFingerprintConfirmDialog *changeFingerprintConfirmDialog;
@property (strong, nonatomic) ChangLanguageDialog *changLanguageDialog;
@property (strong, nonatomic) PhotoAlbumLibary *photoAlbumLibary;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@property (nonatomic, strong) CallSTSendTokenAndLanguageAPI *callSTSendTokenAndLanguageAPI;
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.tblView.dataSource = self;
    self.tblView.delegate = self;
    
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([SettingTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([SettingShowIDCardSomeTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"CellSwitch"];
    
    
    self.tblView.layoutMargins = UIEdgeInsetsZero;
    self.tblView.separatorInset = UIEdgeInsetsZero;
    self.tblView.preservesSuperviewLayoutMargins = NO;
    self.tblView.tableFooterView = [[UIView alloc] init];
    
    self.changeFingerprintAlertDialog = [[ChangeFingerprintAlertDialog alloc] init];
    self.changeFingerprintConfirmDialog = [[ChangeFingerprintConfirmDialog alloc] init];
    self.changeFingerprintConfirmDialog.delegate = self;
    [self setLanguage];
    connectCounter = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews{
    
    [self doDesignLayout];
}


//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CallSTSendTokenAndLanguageAPI
- (void) doSendTokenAndLanguage:(NSString*)token language:(NSString*)language{
    if ( self.callSTSendTokenAndLanguageAPI != nil) {
        self.callSTSendTokenAndLanguageAPI = nil;
    }
    self.callSTSendTokenAndLanguageAPI = [[CallSTSendTokenAndLanguageAPI alloc] init];
    self.callSTSendTokenAndLanguageAPI.delegate = self;
    [self.callSTSendTokenAndLanguageAPI call:token language:language];
}
- (void)callSTSendTokenAndLanguageAPI:(CallSTSendTokenAndLanguageAPI *)classObj statusCode:(NSInteger)statusCode success:(BOOL)success{
    NSLog(@"%d",statusCode);
    
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer  *gradientHeaderView;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeaderView = [Utils getGradientColorHeader];
    gradientHeaderView.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeaderView atIndex:0];
    
}

-(void)setLanguage{
    //myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SETTING",nil,[Utils getLanguage],nil);
}

- (IBAction)openDrawer:(id)sender {
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 4 || indexPath.row == 3|| indexPath.row == 5) {
         SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        [cell.sectionView.layer setShadowColor:[UIColor blackColor].CGColor];
        [cell.sectionView.layer setShadowOpacity:0.3];
        [cell.sectionView.layer setShadowRadius:3.0];
        [cell.sectionView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        
        if(indexPath.row == 0) {
            cell.label.text = NSLocalizedStringFromTableInBundle(@"LABEL_SETTING_MANAGES_ACCOUNT",nil,[Utils getLanguage],nil);
            cell.iconImg.image = [UIImage imageNamed:@"ic_setting_add_user"];
        }
        else if(indexPath.row == 4) {
            cell.label.text = NSLocalizedStringFromTableInBundle(@"LABEL_SETTING_ABOUT",nil,[Utils getLanguage],nil);
            cell.iconImg.image = [UIImage imageNamed:@"st_icon_about"];
        }
        else if(indexPath.row == 3) {
            cell.label.text = NSLocalizedStringFromTableInBundle(@"LABEL_SETTING_CHANG_PIN",nil,[Utils getLanguage],nil);
            cell.iconImg.image = [UIImage imageNamed:@"ic_setting_about_us"];
        }
        else if(indexPath.row == 1) {
            cell.label.text = NSLocalizedStringFromTableInBundle(@"LABEL_SETTING_CHANG_LANGUAGE",nil,[Utils getLanguage],nil);
            cell.iconImg.image = [UIImage imageNamed:@"ic_setting_translate"];
        }
        return cell;
    }
    else{
        SettingShowIDCardSomeTableViewCell *cellSwitch = [tableView dequeueReusableCellWithIdentifier:@"CellSwitch"forIndexPath:indexPath];
        cellSwitch.switchOnOff.onTintColor = [UIColor colorWithRed: 1.00 green: 0.40 blue: 0.40 alpha: 1.00];
        cellSwitch.iconImg.image = [UIImage imageNamed:@"ic_setting_edit_key"];
        cellSwitch.label.text = NSLocalizedStringFromTableInBundle(@"LABEL_SETTING_SOME_IDCARD",nil,[Utils getLanguage],nil);
        if ([[UserData getShowSomeId] isEqualToString:@"on"]) {
                [cellSwitch.switchOnOff setOn:YES animated:YES];
        }
        else{
               [cellSwitch.switchOnOff setOn:NO animated:YES];
        }
        return cellSwitch;
    }
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            return 120; /* Device is iPad */
        }else{
            return 60; /* Device is iPhone */
        }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if(indexPath.row == 0) {
//        [self showChangeFingerprintConfirmDialogWithTitle:@"แจ้งเตือน" message:@"ยืนยันการขอรับรหัส\nเพื่อแก้ไขลายนิ้วมือ"];
//    }
    
    if( (indexPath.row == 0)) {
        AddAccountMainViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAccountMainStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (indexPath.row == 4) {
        
        // Go to profile view controller
        AboutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
    }
    else if (indexPath.row == 3) {
        
        RPOriginPinCodeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RPOriginPinCodeStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];

    }
    else if (indexPath.row == 1){
        if(self.changLanguageDialog != nil && [self.changLanguageDialog isDialogShowing]) {
            [self.changLanguageDialog dismissDialog];
        }
        else {
            self.changLanguageDialog = [[ChangLanguageDialog alloc] init];
            self.changLanguageDialog.delegate = self;
        }
        [self.changLanguageDialog showDialogInView:self.view];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - ChangeFingerprintConfirmDialogDelegate

- (void)changeFingerpringDialogConfirmOK {
    [self sendChangeFingerprintReqeust];
}

#pragma mark - GetAPIData

- (void) sendChangeFingerprintReqeust {
    NSString *URLString = [APIURL getChangeFingerprintRequestURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self sendChangeFingerprintReqeust];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            connectCounter = 0;
            
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"%@", [NSString stringWithFormat:@"Change Fingerprint Request Response : %@", result]);
            
            result = [result stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            
            if([Utils stringIsNumeric:result]) {
                
                int resultCode = [result intValue];
                
                if(resultCode > 0) {
                    [self showChangeFingerprintAlertDialogWithTitle:@"แจ้งเตือน" message:@"ระบบได้ทำการส่งรหัสยืนยัน\nให้ท่านทางอีเมลแล้ว\nกรุณาตรวจสอบอีเมลของท่าน"];
                }
                else if(resultCode == 0) {
                    [self showChangeFingerprintAlertDialogWithTitle:@"แจ้งเตือน" message:@"ท่านทำการขอรหัสยืนยันเรียบร้อยแล้ว กรุณาตรวจสอบอีเมล"];
                }
                else {
                    NSLog(@"%@", [NSString stringWithFormat:@"Change finger print request error : %@", result]);
                    [self showChangeFingerprintAlertDialogWithTitle:@"แจ้งเตือน" message:@"ขอภัยเนื่องจาก\nเกิดข้อผิดพลาดในการทำรายการ"];
                }
            }
            else {
                NSLog(@"%@", [NSString stringWithFormat:@"Change finger print request error : %@", result]);
                [self showChangeFingerprintAlertDialogWithTitle:@"แจ้งเตือน" message:@"ขอภัยเนื่องจาก\nเกิดข้อผิดพลาดในการทำรายการ"];
            }
        }

    }];
    
}

#pragma mark - Dialog

- (void)showChangeFingerprintConfirmDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(self.changeFingerprintConfirmDialog != nil && [self.changeFingerprintConfirmDialog isDialogShowing]) {
        [self.changeFingerprintConfirmDialog dismissDialog];
    }
    
    [self.changeFingerprintConfirmDialog showDialogInView:self.view title:title message:message];
}

- (void)showChangeFingerprintAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(self.changeFingerprintAlertDialog != nil && [self.changeFingerprintAlertDialog isDialogShowing]) {
        [self.changeFingerprintAlertDialog dismissDialog];
    }
    
    [self.changeFingerprintAlertDialog showDialogInView:self.view title:title message:message];
}

#pragma mark - ChangLanguageDialog
- (void) onAlertDialogClose{
    [self doSendTokenAndLanguage:tokenStr language:[UserData getChangLanguage]];
    //[self setLanguage];
    [self.tblView reloadData];
    
}

@end
