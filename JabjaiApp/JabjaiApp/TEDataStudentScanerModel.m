//
//  TEDataStudentScanerModel.m
//  JabjaiApp
//
//  Created by toffee on 2/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "TEDataStudentScanerModel.h"

@implementation TEDataStudentScanerModel
@synthesize studentFirstName = _studentFirstName;
@synthesize studentLastName = _studentLastName;
@synthesize studentPicture = _studentPicture;
@synthesize studentClass = _studentClass;

- (void)setStudentFirstName:(NSString *)studentFirstName{
    _studentFirstName = studentFirstName;
}
- (void)setStudentLastName:(NSString *)studentLastName{
    _studentLastName = studentLastName;
}
- (void)setStudentPicture:(NSString *)studentPicture{
    _studentPicture = studentPicture;
}
- (void)setStudentClass:(NSString *)studentClass{
    _studentClass = studentClass;
}

- (NSString*)getStudentFirstName{
    return _studentFirstName;
}
- (NSString*)getStudentLastName{
    return _studentLastName;
}
- (NSString*)getStudentPicture{
    return _studentPicture;
}
- (NSString*)getStudentClass{
    return _studentClass;
}

@end
