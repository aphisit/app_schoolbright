//
//  CallCheckFlagDateTimeOfStudentAPI.m
//  JabjaiApp
//
//  Created by toffee on 18/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallCheckFlagDateTimeOfStudentAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallCheckFlagDateTimeOfStudentAPI () {
    NSInteger connectCounter;
    NSMutableArray<RPCheckFlagDateTimeModel *> *dateTimeArray;
    
}
@end
@implementation CallCheckFlagDateTimeOfStudentAPI

- (void)call:(long long)shcoolId studentId:(NSString *)studentId date:(NSString *)date{
     connectCounter = 0;
    [self getDateStudent:shcoolId studentId:studentId date:date];
}

- (void) getDateStudent:(long long)shcoolId studentId:(NSString *)studentId date:(NSString *)date{
    
    NSString *URLString = [APIURL getReportCheckFlagDateTime:shcoolId studentId:studentId date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDateStudent:shcoolId studentId:studentId date:date];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                     [self getDateStudent:shcoolId studentId:studentId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
           
            else {
                
                NSDictionary *returnedDict = returnedData;
                NSArray *listDateTime ,*dateArray1 ,*dateArray2 ;
                NSString *date ,*time, *status;
               
                if(dateTimeArray != nil) {
                    [dateTimeArray removeAllObjects];
                    dateTimeArray = nil;
                }
                dateTimeArray = [[NSMutableArray alloc] init];
                if(![[returnedDict objectForKey:@"scanLists"] isKindOfClass:[NSNull class]]) {
                    listDateTime = [returnedDict objectForKey:@"scanLists"];
                }
                connectCounter = 0;
            
                for(int i=0; i<listDateTime.count; i++) {
                    NSDictionary *dataDict = [listDateTime objectAtIndex:i];
                    
                    date = [dataDict objectForKey:@"LogDate"];
                    time = [dataDict objectForKey:@"LogTime"];
                    status = [dataDict objectForKey:@"LogStatus"];
                    
                    dateArray1 = [date componentsSeparatedByString:@"T"];
                    dateArray2 = [dateArray1[0] componentsSeparatedByString:@"-"];
                    date = [NSString stringWithFormat:@"%@/%@/%@",dateArray2[2],dateArray2[1],dateArray2[0]];
                
                    RPCheckFlagDateTimeModel *model = [[RPCheckFlagDateTimeModel alloc] init];
                    [model setDate:date];
                    [model setTime:time];
                    [model setStatus:status];

                    [dateTimeArray addObject:model];
                }
                
                
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callCheckFlagDateTimeOfStudentAPI:data:success:)]) {
                    [self.delegate callCheckFlagDateTimeOfStudentAPI:self data:dateTimeArray success:YES];
                }
                
                
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callCheckFlagDateTimeOfStudentAPI:data:success:)]) {
                    [self.delegate callCheckFlagDateTimeOfStudentAPI:self data:nil success:NO];
                }
            }
            
        }
        
    }];
    
}

@end
