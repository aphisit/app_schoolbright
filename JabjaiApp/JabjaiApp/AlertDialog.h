//
//  AlertDialog.h
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AlertDialogDelegate <NSObject>

@optional
- (void)onAlertDialogClose;

@end

@interface AlertDialog : UIViewController

@property (retain, nonatomic) id<AlertDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

- (IBAction)closeDialog:(id)sender;

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
