//
//  SNClassLevelGroupViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "TableListDialog.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "SWRevealViewController.h"

#import "SlideMenuController.h"

@interface SNClassLevelGroupViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate, SlideMenuControllerDelegate>{
}
// The dialog
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger mode;


@property (nonatomic) NSMutableArray* level2Id;
@property (nonatomic) NSInteger sendtype;
@property (nonatomic) NSInteger newstype;
@property (nonatomic) NSInteger sendgroup;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerSendNewsLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClasslevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIView *section2;


@property (weak, nonatomic) IBOutlet UITextField *classLavelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classRoomTextField;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)actionNext:(id)sender;
- (IBAction)backToMenu:(id)sender;


@end
