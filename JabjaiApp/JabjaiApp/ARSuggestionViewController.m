//
//  ARSuggestionViewController.m
//  JabjaiApp
//
//  Created by toffee on 4/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//
#import "ARSuggestionViewController.h"
#import "UserData.h"
#import "Utils.h"
@interface ARSuggestionViewController ()
@end
@implementation ARSuggestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showIndicator];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_RESULT",nil,[Utils getLanguage],nil);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopIndicator];
        if ([[UserData getChangLanguage] isEqualToString:@"th"]) {
               UIImage *img =  [UIImage imageNamed:@"ar_icon_asuggestionTH"];
               self.suggestionImage.image =  [self imageWithImage:img scaledToSize:CGSizeMake(self.suggestionImage.frame.size.width+50, self.suggestionImage.frame.size.height+50)];
           }
           else{
               UIImage *img =  [UIImage imageNamed:@"ar_icon_asuggestionEN"];
               self.suggestionImage.image =  [self imageWithImage:img scaledToSize:CGSizeMake(self.suggestionImage.frame.size.width+50, self.suggestionImage.frame.size.height+50)];
           }
    });
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout {
    //set color header
    CAGradientLayer *gradientOrange;
    [self.headerView layoutIfNeeded];
    gradientOrange = [Utils getGradientColorHeader];
    gradientOrange.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientOrange atIndex:0];
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    // Here pass new size you need
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)moveBackAction:(id)sender {
    ARDetailAcademicResultsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ARDetailAcademicResultsStoryboard"];
    viewController.termStr = self.termStr;
    viewController.yearId = self.yearId;
    viewController.yearStr = self.yearStr;
    viewController.termArray = self.termArray;
    viewController.yearIdArray = self.yearIdArray;
    viewController.yearNumberArray = self.yearNumberArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
