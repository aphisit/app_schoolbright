//
//  CallGetImageNewsAPI.h
//  JabjaiApp
//
//  Created by toffee on 10/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CallGetImageNewsAPI ;
@protocol CallUpdateSendNewPOSTAPIDelegate <NSObject>
@optional
- (void)callImageNewsPOSTAPI:(CallGetImageNewsAPI *)classObj imageArray:(NSArray*)imageArray;
@end
@interface CallGetImageNewsAPI : NSObject

@property (nonatomic, weak) id<CallUpdateSendNewPOSTAPIDelegate> delegate;

- (void)call:(long long)massageID;
@end
