//
//  CallConfirmPinAPI.h
//  JabjaiApp
//
//  Created by toffee on 6/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CallConfirmPinAPI;
@protocol CallConfirmPinAPIDelegate <NSObject>

- (void)callConfirmPinAPI:(CallConfirmPinAPI *)classObj status:(NSString*)status success:(BOOL)success;
@end
@interface CallConfirmPinAPI : NSObject
@property (nonatomic, weak) id<CallConfirmPinAPIDelegate> delegate;
- (void)call:(NSString*)jsonString;
@end
