//
//  BSScanerStudentDataDialog.m
//  JabjaiApp
//
//  Created by toffee on 21/11/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "BSScanerStudentDataDialog.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Utils.h"
@interface BSScanerStudentDataDialog (){
    BSDataStudentScanerModel *bSDataStudentScanerModel;
}
@property (nonatomic, assign) BOOL isShowing;
@end

@implementation BSScanerStudentDataDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isShowing = NO;
}

- (void)viewDidLayoutSubviews{
    
    [self doDesignLayout];
}

- (void) doDesignLayout{
    
    //set color header
    [self.cancelBnt layoutIfNeeded];
   CAGradientLayer *gradientCancel = [Utils getGradientColorStatus:@"red"];
    gradientCancel.frame = self.cancelBnt.bounds;
    [self.cancelBnt.layer insertSublayer:gradientCancel atIndex:0];
    
    //set color button next
    [self.confirmBnt layoutIfNeeded];
     CAGradientLayer *gradientConfirm = [Utils getGradientColorNextAtion];
    gradientConfirm.frame = self.confirmBnt.bounds;
    [self.confirmBnt.layer insertSublayer:gradientConfirm atIndex:0];
}

- (void)showDialogInView:(UIView *)targetView data:(BSDataStudentScanerModel *)data mode:(int)mode{

    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    bSDataStudentScanerModel = data;
    
    if(mode == 1){// add behavior
          self.headerStatusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_ADD",nil,[Utils getLanguage],nil);
    }
    else{// cut behavior
        self.headerStatusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REDUCE",nil,[Utils getLanguage],nil);
    }
    self.headerNameLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_SCANER_NAME",nil,[Utils getLanguage],nil);
    self.headerLevelLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_SCANER_CLASSROOM",nil,[Utils getLanguage],nil);
    [self.confirmBnt setTitle:NSLocalizedStringFromTableInBundle(@"BTN_BEHAVIOR_OK",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
     [self.cancelBnt setTitle:NSLocalizedStringFromTableInBundle(@"BTN_BEHAVIOR_CANCEL",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    
    
    self.studentNameLabel.text = [NSString stringWithFormat:@"%@ %@",[data getStudentFirstName],[data getStudentLastName]];
    self.classRoomLabel.text = [data getStudentClass];
    
    if ([data getStudentPicture] == NULL || [[data getStudentPicture] isEqual:@""]) {
        self.studentPictureImg.image = [UIImage imageNamed:@"ic_user_info"];
    }
    else{
        [self.studentPictureImg sd_setImageWithURL:[NSURL URLWithString:[data getStudentPicture]]];
    }
//    self.studentPicture.layer.cornerRadius = self.studentPicture.frame.size.height /2;
    self.studentPictureImg.layer.masksToBounds = YES;
    self.studentPictureImg.layer.borderWidth = 0;
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
//    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogScanerClose)]) {
//        [self.delegate onAlertDialogScanerClose];
//    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}


- (IBAction)confirmAction:(id)sender {
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(confirmResponse:)]) {
           [self.delegate confirmResponse:bSDataStudentScanerModel];
       }
}

- (IBAction)cancelAction:(id)sender {
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(cancelResponse)]) {
           [self.delegate cancelResponse];
       }
}
@end
