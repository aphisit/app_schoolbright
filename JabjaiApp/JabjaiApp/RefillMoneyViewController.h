//
//  RefillMoneyViewController.h
//  JabjaiApp
//
//  Created by mac on 11/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfoDBHelper.h"
#import "SWRevealViewController.h"
#import "RFSelectMonyTableViewCell.h"
#import "SlideMenuController.h"
#import "RefillMoneyWebsiteViewController.h"
#import "RFSpecifyNumberMoneyViewController.h"
#import "UnAuthorizeMenuDialog.h"
#import "CallGetMenuListAPI.h"
#import "LoginViewController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface RefillMoneyViewController : UIViewController <SlideMenuControllerDelegate, UITableViewDataSource, UITableViewDelegate,RFSpecifyNumberMoneyViewControllerDelegate,UnAuthorizeMenuDialogDelegate,CallGetMenuListAPIDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>

@property (weak, nonatomic) IBOutlet UILabel *headLable;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *summary;
@property (weak, nonatomic) IBOutlet UILabel *userNameLable;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;

- (IBAction)nextPage:(id)sender;
- (IBAction)openDrawer:(id)sender;

@end
