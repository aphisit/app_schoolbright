//
//  ASAcceptStudentsMapsViewController.m
//  JabjaiApp
//
//  Created by Mac on 11/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "ASAcceptStudentsMapsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "UserData.h"
#import "Utils.h"

@interface ASAcceptStudentsMapsViewController (){
    GMSMapView *mapView;
    NSString *sourceLocationLatitude,*sourceLocationLongitutude;
    NSArray *arrLocation;
    AlertDialogConfirm *alertDialogConfirm;
    UIAlertController *alert;
    AlertDialog *defectiveLocationDialog;
    CAGradientLayer  *submitGradient;
//    CLLocationCoordinate2D coordinates;
}

@property (nonatomic,strong) CallASGetLocationOfSchoolAPI *callASGetLocationOfSchoolAPI;
@property (nonatomic,strong) CallASGetPathStartAndEndOfGoogleMapsAPI *callASGetPathStartAndEndOfGoogleMapsAPI;
@property (nonatomic,strong) CallASSubmitRequestGetStudentAPI *callASSubmitRequestGetStudentAPI;
@property (nonatomic,strong) ASCloseFuturePickupDialog *aSCloseFuturePickupDialog;
@property (nonatomic, strong) CLLocationManager *locationManager;
//@property (nonatomic, strong)  CLLocationCoordinate2D coordinates;
@end

@implementation ASAcceptStudentsMapsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //set color header
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACCEPT_STU",nil,[Utils getLanguage],nil);
    [self.confirmBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_ACCEPT_STU",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    [self.headerView layoutIfNeeded];
    [self.confirmBtn layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    submitGradient = [Utils getGradientColorStatus:@"green"];
    submitGradient.frame = self.confirmBtn.bounds;
    self.confirmBtn.enabled = NO;
    self.refreshBtn.enabled = NO;
    [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
    
    if( self.locationManager == nil ){
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [self.locationManager startUpdatingLocation];
    }
    [self checkLocationAccess];
}

- (void)checkLocationAccess {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    switch (status) {

    // custom methods after each case
        case kCLAuthorizationStatusDenied:
            NSLog(@"");
            break;
        case kCLAuthorizationStatusRestricted:
            NSLog(@"");
            break;
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"");
            [self doRefreshGetLocationOfStudent];
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
            NSLog(@"");
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            NSLog(@"");
            [self doRefreshGetLocationOfStudent];
            break;
    }
}
#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self getLocationSchool:[UserData getSchoolId] studentID:[UserData getUserID]];
    }
    else if (status == kCLAuthorizationStatusDenied) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"App Permission Denied"
                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
            CLLocationManager *locationManager = [[CLLocationManager alloc] init];
            
            if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        NSLog(@"denied"); // denied
    }
}

- (void)doRefreshGetLocationOfStudent{
    
    //int distance = [[self.locationManager location] distanceFromLocation:oldLocation];
    
    CLLocation *location = [self.locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[latitude doubleValue]
                                                          longitude:[longitude doubleValue]
                                                               zoom:15];
    [self.mapContentView layoutIfNeeded];
    mapView = [GMSMapView mapWithFrame:CGRectMake(0,
                                                  0,
                                                  self.mapContentView.bounds.size.width,
                                                  self.mapContentView.bounds.size.height) camera:camera];
    mapView.myLocationEnabled = YES;
    mapView.settings.myLocationButton = YES;
    [self.mapContentView addSubview:mapView];
}


#pragma mark - CallASGetLocationOfSchoolAPI
- (void) getLocationSchool:(long long)schoolID studentID:(long long)studentID{
    [self showIndicator];
    if(self.callASGetLocationOfSchoolAPI != nil) {
        self.callASGetLocationOfSchoolAPI = nil;
    }
    self.callASGetLocationOfSchoolAPI = [[CallASGetLocationOfSchoolAPI alloc] init];
    self.callASGetLocationOfSchoolAPI.delegate = self;
    [self.callASGetLocationOfSchoolAPI call:schoolID studentID:studentID];
}
- (void)callASGetLocationOfSchoolAPI:(CallASGetLocationOfSchoolAPI *)classObj data:(ACLocationModel *)dada success:(BOOL)success{
    [self stopIndicator];
//    CAGradientLayer *layerToRemove;
//    for (CALayer *layer in self.confirmBtn.layer.sublayers) {//Remove CAGradientLayer
//        if ([layer isKindOfClass:[CAGradientLayer class]]) {
//            layerToRemove = (CAGradientLayer *)layer;
//        }
//    }
//    [layerToRemove removeFromSuperlayer];
    CLLocation *locationSchool = [[CLLocation alloc] initWithLatitude:[dada latitude] longitude:[dada longitude]];
    int radius = [[self.locationManager location] distanceFromLocation:locationSchool];
    float kilomate = radius / 1000.0;
    NSLog(@"%.2f",kilomate);

    if (dada != nil && success) {
        [self.confirmBtn layoutIfNeeded];
        if (dada.statusIsActive == 1) {// permission
            //self.confirmBtn.enabled = YES;
            self.refreshBtn.enabled = YES;
            if (dada.statusLacation == 1) {// location have = 1 , not have = 0
                if (dada.statusAccept == 0) {// submit yes = 1 , no = 0
                    CLLocation *location = [self.locationManager location];
                    CLLocationCoordinate2D coordinate = [location coordinate];
                    if (coordinate.latitude == 0 || coordinate.longitude == 0) {
                        self.confirmBtn.enabled = NO;
                        self.refreshBtn.enabled = NO;
                        [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
                    }else{
                        NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
                        NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
                        NSString *sourceLocation = [NSString stringWithFormat:@"%f,%f",[latitude doubleValue],[longitude doubleValue]];
                        NSString *destinationLocation = [NSString stringWithFormat:@"%f,%f",dada.latitude,dada.longitude];
                        [self getDistanceStartPointToEndPoint:sourceLocation destinationLocation:destinationLocation];
                    }
                   
                }else{
                    self.confirmBtn.enabled = NO;
                    self.refreshBtn.enabled = NO;
                    [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
                    [self.confirmBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_PICKUP_SUBMIT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
                    [self showAlertDialogWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_PICKUP_SUBMIT",nil,[Utils getLanguage],nil)];
                }
            }else{
                self.confirmBtn.enabled = NO;
                self.refreshBtn.enabled = NO;
                [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
                [self showAlertDefectiveLocationDialog:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_LOCATION_HAS_NOT",nil,[Utils getLanguage],nil)];
            }
        }else{

            self.confirmBtn.enabled = NO;
            self.refreshBtn.enabled = NO;
            [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
            [self showAlertASCloseFuturePickupDialog:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_NOT_SUBPORT",nil,[Utils getLanguage],nil)];
        }
        
    }
}

#pragma mark - CallASGetPathStartAndEndOfGoogleMapsAPI
- (void) getDistanceStartPointToEndPoint:(NSString*)sourceLocation destinationLocation:(NSString*)destinationLocation{
    [self showIndicator];
    if(self.callASGetPathStartAndEndOfGoogleMapsAPI != nil) {
        self.callASGetPathStartAndEndOfGoogleMapsAPI = nil;
    }
    self.callASGetPathStartAndEndOfGoogleMapsAPI = [[CallASGetPathStartAndEndOfGoogleMapsAPI alloc] init];
    self.callASGetPathStartAndEndOfGoogleMapsAPI.delegate = self;
    [self.callASGetPathStartAndEndOfGoogleMapsAPI call:sourceLocation destinationLocation:destinationLocation];
}

- (void)CallASGetPathStartAndEndOfGoogleMapsAPI:(CallASGetPathStartAndEndOfGoogleMapsAPI *)classObj data:(ASDistanceModel *)data success:(BOOL)success{
    [self stopIndicator];
    CAGradientLayer *layerToRemove;
    for (CALayer *layer in self.confirmBtn.layer.sublayers) {//Remove CAGradientLayer
        if ([layer isKindOfClass:[CAGradientLayer class]]) {
            layerToRemove = (CAGradientLayer *)layer;
        }
    }
    [layerToRemove removeFromSuperlayer];
    if (data != nil && success) {
        //set color button submit
        submitGradient = [Utils getGradientColorStatus:@"green"];
        submitGradient.frame = self.confirmBtn.bounds;
        NSArray *kilomaterArray = [data.kilomater componentsSeparatedByString:@" "];
        //data.kilomater = @"m";
        if ([data.kilomater containsString:@"km"] || [data.kilomater containsString:@"กม."]) {

            if ([kilomaterArray[0] floatValue ] <= 1.0) {
                self.confirmBtn.enabled = YES;
                [self.confirmBtn.layer insertSublayer:submitGradient atIndex:0];
            }else{
                self.confirmBtn.enabled = NO;
                [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
                [self showAlertDialogWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_LIMIT_DISTANCE",nil,[Utils getLanguage],nil)];
            }
        }
        else{
            if ([kilomaterArray[0] floatValue ] > 1000.0) {
                self.confirmBtn.enabled = NO;
                [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
                [self showAlertDialogWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_LIMIT_DISTANCE",nil,[Utils getLanguage],nil)];
                
            }else{
                self.confirmBtn.enabled = YES;
                [self.confirmBtn.layer insertSublayer:submitGradient atIndex:0];
            }
        }
        

        
        
//        if ([data.kilomater containsString:@"km"] && [kilomaterArray[0] floatValue ] <= 1.0) {
//            self.confirmBtn.enabled = YES;
//            [self.confirmBtn.layer insertSublayer:submitGradient atIndex:0];
//        }else{
//            self.confirmBtn.enabled = NO;
//            [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
//        }
//
//        if ([data.kilomater containsString:@"m"] && [kilomaterArray[0] floatValue ] > 1000.0) {
//            self.confirmBtn.enabled = NO;
//            [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
//        }else{
//            self.confirmBtn.enabled = YES;
//            [self.confirmBtn.layer insertSublayer:submitGradient atIndex:0];
//        }
        

        //set text
        arrLocation = data.distance;
        self.timeLabel.text = data.timeLeft;
        self.kilometerLabel.text = data.kilomater;
        [self setMapView];
    }else{
        self.timeLabel.text = @"-";
        self.kilometerLabel.text = @"-";
        self.confirmBtn.enabled = NO;
        self.refreshBtn.enabled = NO;
        [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
    }
}

-(void)getStatusSubmitGetStudent:(long long)schoolID studentID:(long long)studentID{
    [self showIndicator];
    if(self.callASSubmitRequestGetStudentAPI != nil) {
        self.callASSubmitRequestGetStudentAPI = nil;
    }
    self.callASSubmitRequestGetStudentAPI = [[CallASSubmitRequestGetStudentAPI alloc] init];
    self.callASSubmitRequestGetStudentAPI.delegate = self;
    [self.callASSubmitRequestGetStudentAPI call:schoolID studentID:studentID];
}

- (void)callASSubmitRequestGetStudentAPI:(CallASSubmitRequestGetStudentAPI *)classObj status:(NSString *)status success:(BOOL)success{
    [self stopIndicator];
    CAGradientLayer *layerToRemove;
    for (CALayer *layer in self.confirmBtn.layer.sublayers) {
        if ([layer isKindOfClass:[CAGradientLayer class]]) {
            layerToRemove = (CAGradientLayer *)layer;
        }
    }
    [layerToRemove removeFromSuperlayer];
    if (status != nil && status) {
        if ([[status lowercaseString]isEqual:@"ok"]) {
            self.confirmBtn.enabled = NO;
            self.refreshBtn.enabled = NO;
            [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
            [self.confirmBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_PICKUP_SUBMIT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
            [self.confirmBtn layoutIfNeeded];
            [self showAlertDialogWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_PICKUP_SUCCESS",nil,[Utils getLanguage],nil)];
        }
    }else{
        self.confirmBtn.enabled = NO;
        [self.confirmBtn setBackgroundColor:[UIColor colorWithRed: 0.76 green: 0.88 blue: 0.77 alpha: 1.00]];
    }
}

//MapView
-(void)setMapView
{

    CLLocationCoordinate2D sourcePosition,destinationPosition;
    if (arrLocation.count > 0) {
        sourcePosition.latitude = [[[[arrLocation objectAtIndex:0] valueForKey:@"start_location"] valueForKey:@"lat"] floatValue];
        sourcePosition.longitude = [[[[arrLocation objectAtIndex:0] valueForKey:@"start_location"] valueForKey:@"lng"] floatValue];

        destinationPosition.latitude = [[[[arrLocation objectAtIndex:arrLocation.count-1] valueForKey:@"end_location"] valueForKey:@"lat"] floatValue];
        destinationPosition.longitude = [[[[arrLocation objectAtIndex:arrLocation.count-1] valueForKey:@"end_location"] valueForKey:@"lng"] floatValue];
    }

    [self setAnnotationInMapview:sourcePosition.latitude longitude:sourcePosition.longitude pinImgName:@"IMG_0924"];
    [self setAnnotationInMapview:destinationPosition.latitude longitude:destinationPosition.longitude pinImgName:@"IMG_0924"];
    
    GMSMutablePath *path = [GMSMutablePath path];
    [path addCoordinate:CLLocationCoordinate2DMake(sourcePosition.latitude,sourcePosition.longitude)];
    [path addCoordinate:CLLocationCoordinate2DMake(destinationPosition.latitude,destinationPosition.longitude)];
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc]initWithPath:path];
    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    
    if (arrLocation.count == 0)
    {
        GMSMutablePath *path = [GMSMutablePath path];
        [path addCoordinate:CLLocationCoordinate2DMake(sourcePosition.latitude,sourcePosition.longitude)];
        [path addCoordinate:CLLocationCoordinate2DMake(destinationPosition.latitude,destinationPosition.longitude)];
        GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
        //        rectangle.strokeWidth = 2.f;
        rectangle.map = mapView;
    }
    else
    {
        for (int i = 0; i<arrLocation.count; i++)
        {
            NSString *strPolyline = arrLocation[i][@"polyline"][@"points"];
            GMSPath *path = [GMSPath pathFromEncodedPath:strPolyline];
            GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
             //Add the polyline to the map.
            polyline.strokeColor = [UIColor redColor];
            polyline.strokeWidth = 7.f;
            polyline.map = mapView;
        }
    }
}

-(void)setAnnotationInMapview:(float)newLatitude longitude:(float)newLongitude pinImgName:(NSString *)pinImgName
{
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLatitude, newLongitude);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    [marker setDraggable: YES];
    marker.map = mapView;
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    if(alertDialogConfirm != nil) {
        if([alertDialogConfirm isDialogShowing]) {
            [alertDialogConfirm dismissDialog];
        }
    } 
    else {
        alertDialogConfirm = [[AlertDialogConfirm alloc] init];
    }
    [alertDialogConfirm showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showAlertDefectiveLocationDialog:(NSString *)message {
    
    if(defectiveLocationDialog != nil && [defectiveLocationDialog isDialogShowing]) {
        [defectiveLocationDialog dismissDialog];
    }
    else {
        defectiveLocationDialog = [[AlertDialog alloc] init];
    }
    [defectiveLocationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showAlertASCloseFuturePickupDialog:(NSString *)message {
    
    if(self.aSCloseFuturePickupDialog != nil && [self.aSCloseFuturePickupDialog isDialogShowing]) {
        [self.aSCloseFuturePickupDialog dismissDialog];
    }
    else {
        self.aSCloseFuturePickupDialog = [[ASCloseFuturePickupDialog alloc] init];
        self.aSCloseFuturePickupDialog.delegate = self;
    }
    [self.aSCloseFuturePickupDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)onAlertPickupDialogClose{
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}





- (void)showIndicator {
   
  
    // Show the indicator
    if(![self.indicator isAnimating]) {
        self.confirmBtn.enabled = NO;
        [self.indicator startAnimating];
    }
}
- (void)stopIndicator {
    self.confirmBtn.enabled = YES;
    [self.indicator stopAnimating];
}



- (IBAction)refreshAction:(id)sender {
    [self doRefreshGetLocationOfStudent];
    [self getLocationSchool:[UserData getSchoolId] studentID:[UserData getUserID]];
    //[self doRefreshGetLocationOfStudent];
    //[self getLocationSchool:[UserData getSchoolId]];
}

- (IBAction)confirmAction:(id)sender {
        [self getStatusSubmitGetStudent:[UserData getSchoolId] studentID:[UserData getUserID]];
}

- (IBAction)openDrawer:(id)sender {
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
}

//test core data

@end

