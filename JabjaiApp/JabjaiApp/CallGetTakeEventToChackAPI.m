//
//  CallGetTakeEventToChackAPI.m
//  JabjaiApp
//
//  Created by toffee on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetTakeEventToChackAPI.h"
#import "Utils.h"
#import "APIURL.h"



@implementation CallGetTakeEventToChackAPI{
     NSInteger connectCounter;
}

- (void)call:(long long)schoolId  {
    connectCounter = 0;
    [self getDataFromServer:schoolId ];
}
- (void)getDataFromServer:(long long)schoolId  {
    NSString *urlString = [APIURL getStatusTakeEvent:schoolId ];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataFromServer:schoolId ];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSArray *returnedArray = returnedData;
            //NSDictionary *dataDict = [returnedArray objectAtIndex:0];
            BOOL statusShowManuTakeEvent = [[returnedData objectForKey:@"come2school"] boolValue];
            NSLog(@"status = %d",statusShowManuTakeEvent);
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTakeEventToChackAPI:data:success:)]) {
                [self.delegate callGetTakeEventToChackAPI:self data:statusShowManuTakeEvent success:YES];
            }
        }
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTakeEventToChackAPI:data:success:)]) {
                [self.delegate callGetTakeEventToChackAPI:self data:nil success:NO];
            }
        }
        
       
    }];
}

@end
