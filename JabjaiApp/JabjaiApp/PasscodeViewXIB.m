//
//  PasscodeViewXIB.m
//  JabjaiApp
//
//  Created by toffee on 6/7/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "PasscodeViewXIB.h"
#import "NumberButtonCollectionViewCell.h"
#import <CommonCrypto/CommonHMAC.h>
#import "UserData.h"
#import "UIView+Shake.h"


@interface PasscodeViewXIB (){
    NSString *mode;
    NSString *numberAddPin;
    NSString *numberConfirmPin;
    
}

@property (strong, nonatomic) CallAddPinAPI *callAddPinAPI;
@property (strong, nonatomic) MGPasscodeViewController *mGPasscodeViewController;
//@property (strong, nonatomic) MGListMargetViewController *MGListMargetViewController;
@end
static NSMutableArray  *numberArray;
static NSMutableString *numberCode;


@implementation PasscodeViewXIB

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        //Ini
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}
-(void)customInit{
    [[NSBundle mainBundle] loadNibNamed:@"PasscodeViewXIB" owner:self options:nil];

    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    mode = ADD_PINCODE;
    numberArray = [[NSMutableArray alloc] init];
    numberCode = [[NSMutableString alloc] init];
    _confirmPinUnfair.hidden = YES;
    
    _passView1.layer.cornerRadius = _passView1.bounds.size.width/2;
    _passView1.layer.masksToBounds = YES;
    _passView1.layer.borderWidth = 1;
    
    _passView2.layer.cornerRadius = _passView2.bounds.size.width/2;
    _passView2.layer.masksToBounds = YES;
     _passView2.layer.borderWidth = 1;
    
    _passView3.layer.cornerRadius = _passView3.bounds.size.width/2;
    _passView3.layer.masksToBounds = YES;
     _passView3.layer.borderWidth = 1;
    
    _passView4.layer.cornerRadius = _passView4.bounds.size.width/2;
    _passView4.layer.masksToBounds = YES;
     _passView4.layer.borderWidth = 1;
    
    _passView5.layer.cornerRadius = _passView5.bounds.size.width/2;
    _passView5.layer.masksToBounds = YES;
     _passView5.layer.borderWidth = 1;
    
    _passView6.layer.cornerRadius = _passView6.bounds.size.width/2;
    _passView6.layer.masksToBounds = YES;
     _passView6.layer.borderWidth = 1;
    
}

-(void)clearPin{
    mode = CONFIRM_PINCODE;
    [_passView1 setBackgroundColor:[UIColor clearColor]];
    [_passView2 setBackgroundColor:[UIColor clearColor]];
    [_passView3 setBackgroundColor:[UIColor clearColor]];
    [_passView4 setBackgroundColor:[UIColor clearColor]];
    [_passView5 setBackgroundColor:[UIColor clearColor]];
    [_passView6 setBackgroundColor:[UIColor clearColor]];
    numberAddPin = numberCode;
    numberArray = [[NSMutableArray alloc] init];
    numberCode = [[NSMutableString alloc] init];
   
    [_headerPinLabel setText:@"กรอกรหัส Pin ใหม่อีกครั้ง"];
    
}

- (void)doAddPinCode {
     NSLog(@"xxx = %@",_delegate);
    if(self.callAddPinAPI != nil) {
        self.callAddPinAPI = nil;
    }
    self.callAddPinAPI = [[CallAddPinAPI alloc] init];
    self.callAddPinAPI.delegate = self;
    [self.callAddPinAPI call:[UserData getUserID] pinCode:[numberConfirmPin longLongValue]] ;
}
-(void)callAddPinAPI:(CallAddPinAPI *)classObj status:(NSString *)status success:(BOOL)success{
    if ([status isEqualToString:@"Success"]) {
       
        
        
    

    }
}

//- (void)doConfirmPinCode:(NSString*)jsonCode {
//
//    if(self.callConfirmPinAPI != nil) {
//        self.callConfirmPinAPI = nil;
//    }
//    self.callConfirmPinAPI = [[CallConfirmPinAPI alloc] init];
//    self.callConfirmPinAPI.delegate = self;
//    [self.callConfirmPinAPI call:jsonCode] ;
//}
//- (void)callConfirmPinAPI:(CallConfirmPinAPI *)classObj status:(NSString *)status success:(BOOL)success{
//    if ([status isEqualToString:@"Success"]) {
//        NSLog(@"%@",status);
//    }else{
//
//        [self.passView1 shake:5   // 10 times
//               withDelta:5    // 5 points wide
//         ];
//
//    }
//
//}



-(NSString *)createJsonString:(NSString*)pinCode{
    NSString  *jsonString = [[NSString alloc] initWithFormat:@"{ \"Pin\" : \"%@\", \"User_Id\" : %d }", pinCode, [UserData getUserID] ];
    return jsonString;
}


-(void)checkAddAndConfirm{
    numberConfirmPin = numberCode;
    if ([numberAddPin isEqualToString:numberConfirmPin]) {
        [self doAddPinCode];
    }else{
        _confirmPinUnfair.hidden = NO;
        [self.passView1 shake:5   // 10 times
            withDelta:5    // 5 points wide
        ];
        [self.passView2 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView3 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView4 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView5 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView6 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
    }
}

- (IBAction)actionButtonNumber:(UIControl*)sender {
    
    if (sender.tag != 10) {
        if (numberArray.count < 6) {
            [numberCode appendString:[NSString stringWithFormat:@"%d",sender.tag]];
            [numberArray addObject:[NSString stringWithFormat:@"%d",sender.tag]];
           
            if (numberArray.count == 1) {
                
                [_passView1 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 2) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 3) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 4) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 5) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 6) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
                [_passView6 setBackgroundColor:[UIColor grayColor]];
              
                if (mode == ADD_PINCODE) {
                    [self clearPin];
                }else{
                    [self checkAddAndConfirm];
                }
//                if (mode == ADD_PINCODE) {
//                    [self doAddPinCode];
//                }else{
//                    NSString *key = @"ra7$K:L.]%";
//                    NSMutableString *hmac1 = [self hmacForKey1:key andStringData:numberCode];
//                    NSString *jsonString = [self createJsonString:hmac1];
                   // [self doConfirmPinCode:jsonString];
               // }
        
            }
           
        }
    }else{
        _confirmPinUnfair.hidden = YES;
        if (numberArray.count > 0) {
            [numberCode deleteCharactersInRange:NSMakeRange(numberArray.count-1, 1)];
            [numberArray removeObjectAtIndex:numberArray.count-1];
            [_passView1 setBackgroundColor:[UIColor clearColor]];
            [_passView2 setBackgroundColor:[UIColor clearColor]];
            [_passView3 setBackgroundColor:[UIColor clearColor]];
            [_passView4 setBackgroundColor:[UIColor clearColor]];
            [_passView5 setBackgroundColor:[UIColor clearColor]];
            [_passView6 setBackgroundColor:[UIColor clearColor]];
            if (numberArray.count == 1) {
                
                [_passView1 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 2) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 3) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 4) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 5) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 6) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
                [_passView6 setBackgroundColor:[UIColor grayColor]];
            }

        }
    }
    


}
//- (NSMutableString *)hmacForKey1:(NSString *)key andStringData:(NSString *)data
//{
//
//    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
//    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
//    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
//    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
//    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
//
//    NSMutableString *stringOut = [NSMutableString stringWithCapacity:HMAC.length];
//    const unsigned char *macOutBytes = HMAC.bytes;
//
//    for (NSInteger i=0; i<HMAC.length; ++i) {
//        [stringOut appendFormat:@"%02x", macOutBytes[i]];
//    }
//    return stringOut;
//
//}


@end
