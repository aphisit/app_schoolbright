//
//  ReportPersonViewController.h
//  JabjaiApp
//
//  Created by mac on 4/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"


@class ReportPersonViewController;
@protocol ReportPersonViewControllerDelegate <NSObject>

- (void)changePageReportPersonViewController:(ReportPersonViewController *)classObj numberPage:(int)numberPage;
@end

@interface ReportPersonViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate>
@property (nonatomic, weak) id<ReportPersonViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
