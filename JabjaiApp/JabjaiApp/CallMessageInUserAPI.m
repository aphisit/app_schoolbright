//
//  CallMessageInUserAPI.m
//  JabjaiApp
//
//  Created by toffee on 3/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallMessageInUserAPI.h"
#import "Utils.h"
#import "APIURL.h"
#import "UserData.h"

@interface CallMessageInUserAPI (){
    
    
    NSInteger userId;
    NSArray *returnedArray;
    
}

@end
@implementation CallMessageInUserAPI


-(void)call:(NSString*)userIdMember messageId:(long long)messageId {
    [self getUseridinMessge:userIdMember messageId:messageId schoolId:[UserData getSchoolId] ];
}
- (void)getUseridinMessge:(NSString*)userIdMember messageId:(long long)messageId schoolId:(long long)schoolId {
    NSString *URLString = [APIURL getUserIdOfMessage:schoolId messageId:messageId userId:userIdMember];
     NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
        }else{
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            returnedArray = returnedData;
            
            if (returnedData != nil || returnedData != NULL) {
                //userId = [[returnedData objectForKey:@"userid"] integerValue];
                for (int i=0; i<returnedArray.count; i++) {
                    userId = [[returnedArray[i] objectForKey:@"userid"] integerValue];
                }
            }
            if(self.delegate && [self.delegate respondsToSelector:@selector(callMessageInUserAPI:userId:)]) {
                [self.delegate callMessageInUserAPI:self userId:userId];
            }
        }
        
        
        
    }];
}

@end

