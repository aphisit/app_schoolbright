//
//  CallGMGetUserInfoDataAPI.m
//  JabjaiApp
//
//  Created by toffee on 4/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallGMGetUserInfoDataAPI.h"
#import "APIURL.h"
#import "Utils.h"

@implementation CallGMGetUserInfoDataAPI{
    NSInteger connectCounter;
    UserInfoModel *model;
}

- (void)getUserInfoData {
    NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
    connectCounter = 0;
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUserInfoData];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(returnedArray.count != 0) {
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    NSMutableString *firstName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sName"]];
                    NSMutableString *lastName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sLastname"]];
                    NSInteger sex = [[dataDict objectForKey:@"cSex"] integerValue];
                    NSMutableString *birthdayStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dBirth"]];
                    NSMutableString *phoneNumber = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sPhone"]];
                    NSMutableString *email = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sEmail"]];
                    float money = [[dataDict objectForKey:@"nMoney"] floatValue];
                    float creditLimits = [[dataDict objectForKey:@"nMax"] floatValue];
                    NSNumber *score = [NSNumber numberWithDouble:[[dataDict objectForKey:@"Score"] doubleValue]];
                    NSMutableString *department = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"department"]];
                    NSMutableString *position = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"job_name"]];
                    NSMutableString *identification = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sIdentification"]];
                    NSMutableString *studentId = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"StudentId"]];
                    
                    NSString *imageUrl;
                    if(![[dataDict objectForKey:@"image"] isKindOfClass:[NSNull class]]) {
                        imageUrl = [dataDict objectForKey:@"image"];
                    }
                    else {
                        imageUrl = @"";
                    }
                    
                    NSMutableString *studentCode, *studentClass, *schoolName;
                    if(![[dataDict objectForKey:@"StudentId"] isKindOfClass:[NSNull class]]) {
                        studentCode = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"StudentId"]];
                    }
                    else {
                        studentCode = [[NSMutableString alloc] init];
                    }
                    if(![[dataDict objectForKey:@"StudentClass"] isKindOfClass:[NSNull class]]) {
                        studentClass = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"StudentClass"]];
                    }
                    else {
                        studentClass = [[NSMutableString alloc] init];
                    }
                    if(![[dataDict objectForKey:@"SchoolName"] isKindOfClass:[NSNull class]]) {
                        schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolName"]];
                    }
                    else {
                        schoolName = [[NSMutableString alloc] init];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) firstName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lastName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) birthdayStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) phoneNumber);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) email);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentCode);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentClass);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) schoolName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) department);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) position);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) identification);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentId);
                    
                    NSDate *birtdayDate = [Utils parseServerDateStringToDate:birthdayStr];
                    model = [[UserInfoModel alloc] init];
                    model.firstName = firstName;
                    model.lastName = lastName;
                    model.sex = sex;
                    model.birthday = birtdayDate;
                    model.studentCode = studentCode;
                    model.studentClass = studentClass;
                    model.schoolName = schoolName;
                    model.email = email;
                    model.phoneNumber = phoneNumber;
                    model.money = money;
                    model.creditLimits = creditLimits;
                    model.score = score;
                    model.imageUrl = imageUrl;
                    model.department = department;
                    model.position = position;
                    model.sIdentification = identification;
                    model.studentId = studentId;
                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGMGetUserInfoDataAPI:data:success:)]) {
                        [self.delegate callGMGetUserInfoDataAPI:self data:model success:YES];
                    }
                    
                }
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGMGetUserInfoDataAPI:data:success:)]) {
                    [self.delegate callGMGetUserInfoDataAPI:self data:model success:NO];
                }
            }
            
        }
        
    }];
    
}


@end
