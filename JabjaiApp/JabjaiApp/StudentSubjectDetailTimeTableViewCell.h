//
//  StudentSubjectDetailTimeTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 16/5/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StudentSubjectDetailTimeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *subjectHeaderLable;
@property (weak, nonatomic) IBOutlet UILabel *codeHeaderLable;
@property (weak, nonatomic) IBOutlet UILabel *classroomHeaderLable;
@property (weak, nonatomic) IBOutlet UILabel *instructorHeaderLable;
@property (weak, nonatomic) IBOutlet UILabel *timeInOutHeaderLable;
@property (weak, nonatomic) IBOutlet UILabel *saveTimeHeaderLable;
@property (weak, nonatomic) IBOutlet UILabel *classHeaderLable;


@property (weak, nonatomic) IBOutlet UILabel *subjectNameLable;
@property (weak, nonatomic) IBOutlet UILabel *codeSubjectLable;
@property (weak, nonatomic) IBOutlet UILabel *classRoomLable;
@property (weak, nonatomic) IBOutlet UILabel *nameTeacherLable;
@property (weak, nonatomic) IBOutlet UILabel *timeInOutLable;
@property (weak, nonatomic) IBOutlet UILabel *saveTimeLable;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UILabel *classLable;

@end

NS_ASSUME_NONNULL_END
