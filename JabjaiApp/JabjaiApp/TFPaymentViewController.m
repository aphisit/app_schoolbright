//
//  TFPaymentViewController.m
//  JabjaiApp
//
//  Created by Mac on 12/10/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "TFPaymentViewController.h"
#import "APIURL.h"
#import "Utils.h"
@interface TFPaymentViewController ()

@end

@implementation TFPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE",nil,[Utils getLanguage],nil);
    
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    self.webView.delegate = self;
    NSURLRequest *request;
    if (self.paymentType == 0) {
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:[APIURL getPaymentPrompayWebviewAPI:self.studentID amount:self.amount invoiceID:self.invoiceID]]];
    }else{
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:[APIURL getPaymentCardWebviewAPI:self.studentID amount:self.amount invoiceID:self.invoiceID]]];
    }
    [self.webView loadRequest:request];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self.indicator startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
        [self.indicator stopAnimating];
        self.indicator.hidesWhenStopped = YES;
    if ([self.webView.request.URL.description isEqualToString:[APIURL getMoveBackTopUp]]) {
        TFTuitionDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TFTuitionDetailStoryboard"];
        viewController.invoiceID = self.invoiceID;
        viewController.studentID = self.studentID;
        viewController.schoolID = self.schoolID;
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
}

- (IBAction)moveBack:(id)sender {
    TFTuitionDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TFTuitionDetailStoryboard"];
    viewController.invoiceID = self.invoiceID;
    viewController.studentID = self.studentID;
    viewController.schoolID = self.schoolID;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
