//
//  TopupTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 5/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TopupTableViewCell.h"

@implementation TopupTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    self.titleLabel.text = @"";
    self.timeLabel.text = @"";
    self.messageLabel.text = @"";
}

@end
