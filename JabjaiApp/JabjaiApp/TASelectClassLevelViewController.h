//
//  TASelectClassLevelViewController.h
//  JabjaiApp
//
//  Created by mac on 7/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfoDBHelper.h"
#import "SWRevealViewController.h"
#import "AlertDialog.h"
#import "TableListDialog.h"
#import "SubjectTableListDialog.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "CallGetSubjectAPI.h"
#import "CallGetStudentInClassAPI.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "SSubjectModel.h"
#import "SlideMenuController.h"
#import "CallGetMenuListAPI.h"//check authorize menu
#import "UnAuthorizeMenuDialog.h"
#import "SelectOneDayCalendarViewController.h"
#import "CRClosedForRenovationDialog.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"

static NSInteger TA_MODE_CHECK = 1;
static NSInteger TA_MODE_EDIT = 2;
//static NSInteger TE_MODE_CHECK = 1;
//static NSInteger TE_MODE_EDIT = 2;

@interface TASelectClassLevelViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, SubjectTableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate, CallGetSubjectAPIDelegate, CallGetStudentInClassAPIDelegate, SlideMenuControllerDelegate,CallGetMenuListAPIDelegate,UnAuthorizeMenuDialogDelegate,SelectOneDayCalendarDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

//Global variables
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SSubjectModel *> *subjectArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
@property (nonatomic) NSInteger mode;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSubjectLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;

@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classroomTextField;
@property (weak, nonatomic) IBOutlet UITextField *subjectTextField;
@property (weak, nonatomic) IBOutlet UIButton *calandaBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIView *section2;
@property (weak, nonatomic) IBOutlet UIView *section3;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)selecDayAction:(id)sender;


- (IBAction)openDrawer:(id)sender;
- (IBAction)actionNext:(id)sender;

@end
