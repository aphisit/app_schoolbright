//
//  ReportInOutAllDataPersonalListViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"

@interface ReportInOutAllDataPersonalListViewController : UIViewController <SlideMenuControllerDelegate>


- (IBAction)moveBack:(id)sender;

@end
