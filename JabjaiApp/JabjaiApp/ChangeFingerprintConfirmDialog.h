//
//  ChangeFingerprintConfirmDialog.h
//  JabjaiApp
//
//  Created by mac on 12/21/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangeFingerprintConfirmDialogDelegate

-(void)changeFingerpringDialogConfirmOK;

@end

@interface ChangeFingerprintConfirmDialog : UIViewController

@property (nonatomic, retain) id<ChangeFingerprintConfirmDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *OKButton;

- (IBAction)onPressCancelButton:(id)sender;
- (IBAction)onPressOKButton:(id)sender;

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
