//
//  SNNewsDetailViewControllerXIB.h
//  JabjaiApp
//
//  Created by Mac on 17/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportSendNewsImageWithFileCollectionViewCell.h"
#import "saveImageXIBViewController.h"
#import "ReportSendNewsOpenFileViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface SNNewsDetailViewControllerXIB : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>
@property (assign, nonatomic) long long newsID;

@property (weak, nonatomic) IBOutlet UILabel *headerSendTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAmountReciptientLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerReadLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerUnReadLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDateSendLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTimeSendLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachFileLabel;

@property (weak, nonatomic) IBOutlet UILabel *sendTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *allCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *unreadCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
@property (weak, nonatomic) IBOutlet UICollectionView *imgCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *fileCollectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highImaCollectionConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highFileCollectionConstraint;

@end

NS_ASSUME_NONNULL_END
