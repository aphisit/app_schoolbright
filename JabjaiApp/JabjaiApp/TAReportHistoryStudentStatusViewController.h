//
//  TAReportHistoryStudentStatusViewController.h
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import ARSlidingPanel;

#import <UIKit/UIKit.h>
#import "TAReportCalendarViewController.h"
#import "CallGetTAHistoryStudentStatusListAPI.h"

@interface TAReportHistoryStudentStatusViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CallGetTAHistoryStudentStatusListAPIDelegate>

@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
