//
//  SNReciverViewControllerXIB.h
//  JabjaiApp
//
//  Created by Mac on 17/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SNReciverViewControllerXIB : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (assign, nonatomic) long long newsID;

@property (nonatomic, strong) NSString *sortData;
@property (nonatomic, strong) NSString *sortChangeData;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (void)getRecieverListWithnewsID:(long long)newsId page:(NSInteger)page readStatus:(NSString *)readStatus;
@end

NS_ASSUME_NONNULL_END
