//
//  ReportOrderHomeWorkDetailViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportOrderHomeWorkDetailViewController.h"

#import <QBImagePickerController/QBImagePickerController.h>
#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"

#import "AppDelegate.h"
#import "ZMImageSliderViewController.h"

#import "ReportOrderHomeworkDetailModel.h"

@interface ReportOrderHomeWorkDetailViewController (){
    
    ReportOrderHomeworkDetailModel *orderHomeworkDetailModel;
    NSMutableArray<ReportOrderHomeworkDetailModel *> *orderArray;
    NSInteger connectCounter;
    
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;

@end

@implementation ReportOrderHomeWorkDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    // set up formatter
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    [self getReportOrderHomeworkDetailWithHomeworkID:self.homeworkID];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews{
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getReportOrderHomeworkDetailWithHomeworkID:(long long)homeworkId{
    
    [self showIndicator];
    
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getReportOrderHomeworkDetailWithSchoolID:schoolID homeworkID:homeworkId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getReportOrderHomeworkDetailWithHomeworkID:homeworkId];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportOrderHomeworkDetailWithHomeworkID:homeworkId];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportOrderHomeworkDetailWithHomeworkID:homeworkId];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if (orderArray != nil) {
                    [orderArray removeAllObjects];
                    orderArray = nil;
                }
                
                orderArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"message"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"message"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *newsDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"daysend"]];
                    
                    int all = [[dataDict objectForKey:@"user_all"] intValue];
                    int read = [[dataDict objectForKey:@"user_read"] intValue];
                    int unread = [[dataDict objectForKey:@"user_unread"] intValue];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) newsDateStr);
                    
                    NSRange dotRange = [newsDateStr rangeOfString:@"."];
                    
                    NSDate *newsDate;
                    
                    if(dotRange.length != 0) {
                        newsDate = [formatter dateFromString:newsDateStr];
                    }
                    else {
                        newsDate = [formatter2 dateFromString:newsDateStr];
                    }
                    
                    ReportOrderHomeworkDetailModel *model = [[ReportOrderHomeworkDetailModel alloc] init];
                    model.message = message;
                    model.all = all;
                    model.read = read;
                    model.unread = unread;
                    model.dateStart = newsDate;
                    orderHomeworkDetailModel = model;
                    [self performData];
                    
                    [orderArray addObject:model];
                    
                }
                
            }
        }
        
    }];
    
}

- (void)performData {
    
    if (orderHomeworkDetailModel != nil) {
        self.sendTypeLabel.text = @"";
        self.recieverLabel.text = @"";
        self.allCountLabel.text = [[NSString alloc] initWithFormat:@"%i คน", orderHomeworkDetailModel.all];
        self.readCountLabel.text = [[NSString alloc] initWithFormat:@"%i คน", orderHomeworkDetailModel.read];
        self.unreadCountLabel.text = [[NSString alloc] initWithFormat:@"%i คน", orderHomeworkDetailModel.unread];
        self.dateTimeLabel.text = [[NSString alloc] initWithFormat:@"%@ เวลา %@",[self.dateFormatter stringFromDate:orderHomeworkDetailModel.dateStart], [self.timeFormatter stringFromDate:orderHomeworkDetailModel.dateStart]];
        self.detailLabel.text = [[NSString alloc] initWithFormat:@"%@", orderHomeworkDetailModel.message];
        
    }
    else{
        
        [self clearDisplay];
        
    }
    
    
}

- (void)clearDisplay {
    
    self.sendTypeLabel.text = @"";
    self.recieverLabel.text = @"";
    self.allCountLabel.text = @"";
    self.readCountLabel.text = @"";
    self.unreadCountLabel.text = @"";
    self.dateTimeLabel.text = @"";
    self.detailLabel.text = @"";
    
}

- (void)showIndicator{
    if (![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator{
    [self.indicator stopAnimating];
    
}

@end
