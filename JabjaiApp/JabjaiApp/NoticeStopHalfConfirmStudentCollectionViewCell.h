//
//  NoticeStopHalfConfirmStudentCollectionViewCell.h
//  JabjaiApp
//
//  Created by mac on 3/7/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeStopHalfConfirmStudentCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageNotice;

@end
