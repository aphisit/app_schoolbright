//
//  ReportInOutAllDataViewController.h
//  JabjaiApp
//
//  Created by mac on 4/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "SlideMenuController.h"
#import "ReportInOutAllDataStudentViewController.h"
#import "ReportInOutAllDataPersonalViewController.h"
#import "SelectCalendaDayFormerDialog.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface ReportInOutAllDataViewController : UIViewController <SlideMenuControllerDelegate, CAPSPageMenuDelegate, SelectCalendaDayFormerDialogDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

@property (nonatomic) CAPSPageMenu *pageMenu;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic, strong) ReportInOutAllDataStudentViewController *studentController;
@property (nonatomic, strong) ReportInOutAllDataPersonalViewController *personController;
- (IBAction)calendarAction:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
