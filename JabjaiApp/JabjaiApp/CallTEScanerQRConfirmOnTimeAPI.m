//
//  CallTEScanerQRConfirmOnTimeAPI.m
//  JabjaiApp
//
//  Created by toffee on 31/7/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallTEScanerQRConfirmOnTimeAPI.h"
#import "Utils.h"
#import "APIURL.h"
@interface CallTEScanerQRConfirmOnTimeAPI () {
    NSInteger connectCounter;
    NSInteger resCode;
}
@end
@implementation CallTEScanerQRConfirmOnTimeAPI

- (void)call:(long long)schoolId userId:(long long)userId idCode:(NSString *)idCode{
    [self doCallTEConfirmScanerBarcodeAPI:schoolId userId:userId idCode:idCode];
}

- (void)doCallTEConfirmScanerBarcodeAPI:(long long)schoolId userId:(long long)userId idCode:(NSString *)idCode{
    NSString *URLString = [APIURL getTEScanerQROnTimeConfirm:schoolId userId:userId idCode:idCode];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self doCallTEConfirmScanerBarcodeAPI:schoolId userId:userId idCode:idCode];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self doCallTEConfirmScanerBarcodeAPI:schoolId userId:userId idCode:idCode];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            
            else {
                
                NSDictionary *dict = returnedData;
                resCode = [[dict objectForKey:@"resCode"] integerValue];
                connectCounter = 0;
                
                
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTEScanerQROnTimeConfirm:resCode:success:)]) {
                    [self.delegate callTEScanerQROnTimeConfirm:self resCode:resCode success:YES];
                }
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTEScanerQROnTimeConfirm:resCode:success:)]) {
                [self.delegate callTEScanerQROnTimeConfirm:self resCode:resCode success:NO];
            }
        }
        
    }];
}


@end
