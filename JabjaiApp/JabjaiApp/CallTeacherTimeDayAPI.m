//
//  CallTeacherTimeDayAPI.m
//  JabjaiApp
//
//  Created by toffee on 22/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallTeacherTimeDayAPI.h"
#import "UserData.h"
#import "Utils.h"
#import "APIURL.h"
@implementation CallTeacherTimeDayAPI{
    NSInteger connectCounter;
    NSMutableArray<TeacherTimeTableModel *> *subjectArray;
}
- (void)call:(long long)userId date:(NSDate *)date schoolid:(long long)schoolid{
    connectCounter = 0;
    [self getSubjectOfda:userId date:date schoolid:(long long)schoolid];
}

- (void)getSubjectOfda:(long long)userId date:(NSDate*)date schoolid:(long long)schoolid{
    
    NSString *URLString = [APIURL getTeacherScheduleListWithUserID:userId date:date schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                 [self getSubjectOfda:userId date:date schoolid:(long long)schoolid];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if (returnedData)
            {
                NSLog(@"yyyy1");
                if(error != nil) {
                    NSLog(@"An error occured : %@", [error localizedDescription]);
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                         [self getSubjectOfda:userId date:date schoolid:(long long)schoolid];
                    }
                    else {
                        connectCounter = 0;
                    }
                }
                else if(![returnedData isKindOfClass:[NSArray class]]) {
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                         [self getSubjectOfda:userId date:date schoolid:(long long)schoolid];
                    }
                    else {
                        connectCounter = 0;
                    }
                    
                }
                else {
                    NSArray *returnedArray = returnedData;
                    connectCounter = 0;
                    long long subjectID;
                    if(subjectArray != nil) {
                        [subjectArray removeAllObjects];
                        subjectArray = nil;
                    }

                    subjectArray = [[NSMutableArray alloc] init];

                    for(int i=0; i<returnedArray.count; i++) {
                        NSDictionary *dataDict = [returnedArray objectAtIndex:i];

                        subjectID = [[dataDict objectForKey:@"ScheduleId"] longLongValue];

                        NSMutableString *subjectName, *startTime, *endTime, *codeSubject;

                        if(![[dataDict objectForKey:@"ScheduleName"] isKindOfClass:[NSNull class]]) {
                            subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScheduleName"]];
                        }
                        else {
                            subjectName = [[NSMutableString alloc] initWithString:@""];
                        }

                        if(![[dataDict objectForKey:@"TimeIn"] isKindOfClass:[NSNull class]]) {
                            startTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeIn"]];
                        }
                        else {
                            startTime = [[NSMutableString alloc] initWithString:@""];
                        }

                        if(![[dataDict objectForKey:@"TimeOut"] isKindOfClass:[NSNull class]]) {
                            endTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeOut"]];
                        }
                        else {
                            endTime = [[NSMutableString alloc] initWithString:@""];
                        }
                        
                        if(![[dataDict objectForKey:@"CourseCode"] isKindOfClass:[NSNull class]]) {
                            codeSubject = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"CourseCode"]];
                        }
                        else {
                            codeSubject = [[NSMutableString alloc] initWithString:@""];
                        }

                        CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) startTime);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) endTime);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) codeSubject);

                        TeacherTimeTableModel *model = [[TeacherTimeTableModel alloc] init];
                        model.subjectID = subjectID;
                        model.subjectName = subjectName;
                        model.startTime = startTime;
                        model.endTime = endTime;
                        model.codeSubject = codeSubject;

                        [self->subjectArray addObject:model];
                    
                    }
                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTeacherTimeDayAPI:data:success:)]) {
                        [self.delegate callTeacherTimeDayAPI:self data:self->subjectArray success:YES];
                    }
                }
                
            }else{
                NSLog(@"yyyy2");
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTeacherTimeDayAPI:data:success:)]) {
                    [self.delegate callTeacherTimeDayAPI:self data:self->subjectArray success:NO];
                }
            }
            
           
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTeacherTimeDayAPI:data:success:)]) {
                    [self.delegate callTeacherTimeDayAPI:self data:self->subjectArray success:NO];
                }
            }
        }
        
    }];
}

@end
