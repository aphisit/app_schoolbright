//
//  MGBillSummaryViewController.h
//  JabjaiApp
//
//  Created by toffee on 5/16/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallUpdateMargetPOSTAPI.h"
#import "MGListMenuViewController.h"
#import "MGListMargetViewController.h"
#import "SlideMenuController.h"
#import "MargetBillSummaryTableViewCell.h"
#import "MGListMenuDialog.h"
#import "ConfirmPinCodeViewController.h"
#import "MGPasscodeViewController.h"

@interface MGBillSummaryViewController : UIViewController<CallUpdateMargetPOSTAPIDelegate,MargetBillSummaryTableViewCellDelegate, MGListMenuDialogDelegate, ConfirmPinCodeViewControllerDelegate>
@property (nonatomic) NSMutableArray* orderArray;
@property (nonatomic) NSMutableArray* amountOrderArray;
@property (nonatomic) NSMutableArray* totalPriceOrderArray;
@property (nonatomic) NSInteger totalPrice;
@property (nonatomic) NSString* orderStringJson;
@property (nonatomic) long long shopId;
@property (nonatomic) NSString* nameShop;



@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerShopLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPayMentLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerCustomerLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSellerLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerPaymentbtn;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *senderImage;
@property (weak, nonatomic) IBOutlet UIImageView *recipientImage;

@property (weak, nonatomic) IBOutlet UILabel *firstName;
@property (weak, nonatomic) IBOutlet UILabel *lastName;
@property (weak, nonatomic) IBOutlet UILabel *nameShopLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalMonny;

@property (weak, nonatomic) IBOutlet UIButton *summitBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)actionSubmit:(id)sender;
- (IBAction)moveBack:(id)sender;


@end
