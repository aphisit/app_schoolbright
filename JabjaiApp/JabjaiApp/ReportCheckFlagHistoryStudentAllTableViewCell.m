//
//  ReportCheckFlagHistoryStudentAllTableViewCell.m
//  JabjaiApp
//
//  Created by toffee on 7/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportCheckFlagHistoryStudentAllTableViewCell.h"
#import "UserData.h"
#import "Utils.h"
@interface ReportCheckFlagHistoryStudentAllTableViewCell () {
    UIColor *greenColor;
    UIColor *yellowColor;
    UIColor *redColor;
    UIColor *pinkColor;
    UIColor *lightBluecolor;
    UIColor *blueColor;
    UIColor *grayColor;
    CAGradientLayer *gradient;
    NSBundle *myLangBundle;
}
@end
@implementation ReportCheckFlagHistoryStudentAllTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    CGFloat max = MAX(self.statusBtn.frame.size.width, self.statusBtn.frame.size.height);
    self.statusBtn.layer.cornerRadius = max/2.0;
    self.statusBtn.layer.masksToBounds = YES;
    self.statusBtn.titleLabel.numberOfLines = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateStatus:(int)status {
    gradient = [CAGradientLayer layer];
    UIImage * backgroundColorImage;
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource: [UserData getChangLanguage] ofType:@"lproj"]];
    switch (status) {
        case 0:
        
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ONTIME",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"green"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            
            break;
        case 1:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_LATE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"yellow"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 3:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ABSENCE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"red"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
    
        case 6:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_EVENT_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"blue"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 4:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_PERSONAL_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 5:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SICK_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"pink"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
            
        case 10:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_PERSONAL_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 11:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SICK_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"pink"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 12:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_EVENT_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"blue"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 21:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_DISTRIBUTE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 22:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_RESING",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 23:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SUSPENDED",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 24:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_GRADUATE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 25:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_LOST_CONTRACT",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 26:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_RETIRE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        default:
            [self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_UNDEFINED",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"gray"];
            gradient.frame = self.statusBtn.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusBtn setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
    }
    
}

@end
