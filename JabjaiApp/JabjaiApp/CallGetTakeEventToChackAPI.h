//
//  CallGetTakeEventToChackAPI.h
//  JabjaiApp
//
//  Created by toffee on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CallGetTakeEventToChackAPI;

@protocol CallGetTakeEventToChackAPIDelegate <NSObject>

@optional
- (void)callGetTakeEventToChackAPI:(CallGetTakeEventToChackAPI *)classObj data:(BOOL)data success:(BOOL)success;

@end

@interface CallGetTakeEventToChackAPI : NSObject
@property (nonatomic, weak) id<CallGetTakeEventToChackAPIDelegate> delegate;

- (void)call:(long long)schoolId ;

@end
