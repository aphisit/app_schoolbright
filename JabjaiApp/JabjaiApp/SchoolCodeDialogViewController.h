//
//  SchoolCodeAlertViewController.h
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SchoolCodeDialogViewControllerDelegate <NSObject>

@optional
- (void)onCancelButtonPress;
- (void)onOkButtonPress;

@end

@interface SchoolCodeDialogViewController : UIViewController

@property (nonatomic, retain) id<SchoolCodeDialogViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldDailogTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldDailogBottomConstraint;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hintLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;

- (IBAction)cancelAction:(id)sender;
- (IBAction)okAction:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;

- (void)showDialogInView:(UIView *)targetView;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title hint:(NSString *)hint;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
