//
//  ReportInOutStatusDepartmantViewController.h
//  JabjaiApp
//
//  Created by toffee on 6/3/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportInOutStatusDepartmantCollectionViewCell.h"
#import "CallReportInOutStatusDepartmantAPI.h"
#import "ReportInOutStatusDepartmentModel.h"
#import "ReportInOutStatusDepartmentTableViewCell.h"
#import "ReportInOutAllDataSelectDepartmentViewController.h"
#import "SlideMenuController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutStatusDepartmantViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, CallReportInOutStatusDepartmantAPIDelegat, UITableViewDelegate, UITableViewDataSource,SlideMenuControllerDelegate>
@property (strong,nonatomic) NSDate *dayReports;
@property (nonatomic) long long departmentId;
@property (strong,nonatomic) NSString *departmentName;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *departmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountAllLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *showNoStudent;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *headerView;
- (IBAction)moveBack:(id)sender;

@end

NS_ASSUME_NONNULL_END
