//
//  ChangLanguageDialog.m
//  JabjaiApp
//
//  Created by toffee on 7/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ChangLanguageDialog.h"
#import "UserData.h"

@interface ChangLanguageDialog ()
@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic, strong) CallSTSendTokenAndLanguageAPI *callSTSendTokenAndLanguageAPI;
@end

@implementation ChangLanguageDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dialogView.layer.masksToBounds = YES;
    
   
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)languageThaiAction:(id)sender {
    [UserData setChangLanguage:@"th"];
    [self dismissDialog];
}

- (IBAction)languageEnglishAction:(id)sender {
    [UserData setChangLanguage:@"en"];
    [self dismissDialog];
}

-(void)showDialogInView:(UIView *)targetView {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self doSendTokenAndLanguage:tokenStr language:[UserData getChangLanguage]];
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogClose)]) {
        [self.delegate onAlertDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

#pragma mark - CallSTSendTokenAndLanguageAPI
- (void) doSendTokenAndLanguage:(NSString*)token language:(NSString*)language{
    if ( self.callSTSendTokenAndLanguageAPI != nil) {
        self.callSTSendTokenAndLanguageAPI = nil;
    }
    self.callSTSendTokenAndLanguageAPI = [[CallSTSendTokenAndLanguageAPI alloc] init];
    self.callSTSendTokenAndLanguageAPI.delegate = self;
    [self.callSTSendTokenAndLanguageAPI call:token language:language];
}
- (void)callSTSendTokenAndLanguageAPI:(CallSTSendTokenAndLanguageAPI *)classObj statusCode:(NSInteger)statusCode success:(BOOL)success{
    NSLog(@"%d",statusCode);
}

@end
