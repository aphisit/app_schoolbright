//
//  NoticeStopHalfViewController.h
//  JabjaiApp
//
//  Created by mac on 3/7/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfoDBHelper.h"
#import "SlideMenuController.h"
#import "TableListDialog.h"
#import "CalendarDialog.h"
#import "SWRevealViewController.h"
#import "SelectOneDayCalendarViewController.h"
#import "UnAuthorizeMenuDialog.h"
#import "CallGetMenuListAPI.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface NoticeStopHalfViewController : UIViewController <UITextFieldDelegate, CalendarDialogDelegate, SlideMenuControllerDelegate, TableListDialogDelegate, SelectOneDayCalendarDelegate,UnAuthorizeMenuDialogDelegate,CallGetMenuListAPIDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>

@property (nonatomic) NSString *leaveCause;
@property (nonatomic) NSString *leaveFirstDate;
@property (nonatomic) NSString *leavePeriod;
@property (nonatomic) NSDate *leaveStartdate;


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerReasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDatefromLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSeasonLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIView *section2;
@property (weak, nonatomic) IBOutlet UIView *section3;
@property (weak, nonatomic) IBOutlet UITextField *cause;
@property (weak, nonatomic) IBOutlet UITextField *leaveDate;
@property (weak, nonatomic) IBOutlet UITextField *period;

- (IBAction)openDrawer:(id)sender;
- (IBAction)nextPage:(id)sender;


@end
