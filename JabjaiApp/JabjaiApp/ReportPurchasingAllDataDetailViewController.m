//
//  ReportPurchasingAllDataDetailViewController.m
//  JabjaiApp
//
//  Created by mac on 4/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportPurchasingAllDataDetailViewController.h"
#import "ReportPurchasingAllDataShopListViewController.h"

@interface ReportPurchasingAllDataDetailViewController ()

@end

@implementation ReportPurchasingAllDataDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)moveBack:(id)sender {
    
    ReportPurchasingAllDataShopListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportPurchasingAllDataShopListStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
