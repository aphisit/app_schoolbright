//
//  CallImageBarCodeAPI.h
//  JabjaiApp
//
//  Created by toffee on 7/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CallImageBarCodeAPI;

@protocol CallImageBarCodeAPIDelegate <NSObject>

- (void)callImageBarCodeAPI:(CallImageBarCodeAPI *)classObj data:(NSString *)data generateCode:(NSString*)generateCode success:(BOOL)success;

- (void)callImageQRCodeAPI:(CallImageBarCodeAPI *)classObj data:(NSString *)data success:(BOOL)success;

@end

@interface CallImageBarCodeAPI : NSObject
@property (nonatomic, weak) id<CallImageBarCodeAPIDelegate> delegate;
- (void)call:(long long)high width:(long long)width userId:(long long)userId generateCode:(NSString*)generateCode schoolid:(long long)schoolid;
//- (void)callQRCode:(long long)high width:(long long)width formatTime:(NSString*)formatTime schoolid:(long long)schoolid;


@end
