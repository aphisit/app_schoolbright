//
//  ReportBehaviorAllDataDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 4/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "BSStatusModel.h"
#import "BSHistoryModel.h"

#import "SlideMenuController.h"

@interface ReportBehaviorAllDataDetailViewController : UIViewController <SlideMenuControllerDelegate>

// Global variables

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<BSStatusModel *> *statusArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedStatusIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger statusId;

@property (nonatomic) BSHistoryModel *selectedHistoryModel;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSummaryLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerStudentLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRecorderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRemarkLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *datetimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *recorderLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (weak, nonatomic) IBOutlet UILabel *behaviorNameLabel;

- (IBAction)moveBack:(id)sender;


@end
