//
//  MGListMenuDialog.m
//  JabjaiApp
//
//  Created by toffee on 5/17/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "MGListMenuDialog.h"
#import "MargetBillSummaryTableViewCell.h"
#import "Utils.h"

@interface MGListMenuDialog ()
@property (nonatomic, strong) NSString *requestCode;
@property (nonatomic) BOOL isShowing;
@end
static NSString *cellIdentifier = @"MargetSummaryCell";
static CGFloat cellHeight = 40;

@implementation MGListMenuDialog
-(id)initWithRequestCode:(NSString *)requestCode {
    
    self = [[MGListMenuDialog alloc] init];
    
    if(self) {
        self.requestCode = requestCode;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([MargetBillSummaryTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    self.tblView.delegate = self;
    self.tblView.dataSource = self;
    self.tblView.layer.masksToBounds = YES;
    self.tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.isShowing = NO;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm;
    //set color header
    [self.btnNext layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.btnNext.bounds;
    [self.btnNext.layer insertSublayer:gradientConfirm atIndex:0];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.tblView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(_menuArr == nil) {
        return 0;
    }
    else {
        return _menuArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MargetBillSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    NSLog(@"index = %d",indexPath.row);
    cell.userInteractionEnabled = false;
    cell.listNameLabel.text = [_menuArr objectAtIndex:indexPath.row];
    cell.priceLabel.text = [_priceArr objectAtIndex:indexPath.row];
    cell.amountLabel.text = [_amountArr objectAtIndex:indexPath.row];

    return cell;
}

#pragma UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return cellHeight;
}
#pragma mark - Dialog Functions

-(void)showDialogInView:(UIView *)targetView listMenu:(NSArray *)listMenu lisiPrice:(NSArray *)listPrice listAmount:(NSArray *)listAmount{
    
    _menuArr = listMenu;
    _priceArr = listPrice;
    _amountArr = listAmount;
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.tblView setAlpha:0.0];
    [self.btnNext setAlpha:0.0];
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.tblView setAlpha:1.0];
    [self.btnNext setAlpha:1.0];
    [UIView commitAnimations];
    
    // Adjust table view frame size;
    
    int cellNumber = 9;
    if(_menuArr.count < 9) {
        if (_menuArr.count<=4) {
            cellNumber = 4;
        }else{
             cellNumber = (int)_menuArr.count;
        }
    }
    
    // Calcalate position of new table view dialog
    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat viewHeight = self.view.frame.size.height;
    CGFloat tableViewWidth = self.view.frame.size.width - 32; //self.tblView.frame.size.width;
    CGFloat tableViewHeight = cellHeight * cellNumber;
    CGFloat posX = (viewWidth - tableViewWidth)/2.0;
    CGFloat posY = (viewHeight - tableViewHeight)/2.0;
    
    self.tblView.frame = CGRectMake(posX, posY, tableViewWidth, tableViewHeight);
    self.content.frame = CGRectMake(posX, posY, tableViewWidth, tableViewHeight + 40);
    self.btnNext.frame = CGRectMake(posX-16, tableViewHeight, tableViewWidth, 40);
    
    [self.tblView reloadData];
    self.isShowing = YES;;
    
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.tblView setAlpha:0.0];
    [self.btnNext setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

-(BOOL)isDialogShowing {
    return self.isShowing;
}
- (IBAction)actionNext:(id)sender {
//    [self.delegate onItemSelectClassrooms:self.requestCode dataClassroomName:nameRoom dataClassroomID:radio];
    [self dismissDialog];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
