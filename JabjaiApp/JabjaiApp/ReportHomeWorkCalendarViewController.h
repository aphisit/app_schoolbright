//
//  ReportHomeWorkCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 1/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

@import ARSlidingPanel;
#import "JHDetailHomeWorkViewController.h"
@protocol ReportHomeworkControllerDelegate <NSObject>

@optional
- (void)onSelectDate:(NSDate *)date;

@end

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CFSCalendar.h"
#import "ReportHomeWorkViewControllerDelegate.h"

@interface ReportHomeWorkCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, CFSCalendarDelegateAppearance, ARSPDragDelegate, ARSPVisibilityStateDelegate, ReportHomeWorkViewControllerDelegate>

@property (retain, nonatomic) id<ReportHomeworkControllerDelegate> delegate;

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) NSCalendar *gregorian;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topindicatorConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomindicatorConstraint;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)openDrawer:(id)sender;
- (IBAction)showNotifications:(id)sender;



@end
