//
//  TEStatusDialog.h
//  JabjaiApp
//
//  Created by toffee on 11/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
@class TEStatusDialog;

@protocol TEStatusDialogDelegate <NSObject>
    - (void)taStatusDialog:(TEStatusDialog *)dialog onSelectStatus:(TA_SCAN_STATUS)scanStatus;
@end


@interface TEStatusDialog : UIViewController
@property (nonatomic, retain) id<TEStatusDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIStackView *dialogStackView;
@property (weak, nonatomic) IBOutlet UIButton *statusOnTimeButton;
@property (weak, nonatomic) IBOutlet UIButton *statusLateButton;
@property (weak, nonatomic) IBOutlet UIButton *statusAbsenceButton;
@property (weak, nonatomic) IBOutlet UIButton *statusEventButton;
@property (weak, nonatomic) IBOutlet UIButton *statusPersonalLeaveButton;
@property (weak, nonatomic) IBOutlet UIButton *statusSickLeaveButton;
- (IBAction)selectStatus:(id)sender;

- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end
