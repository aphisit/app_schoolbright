//
//  NavUserTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 8/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUIImageView.h"

@interface NavUserTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet CustomUIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
