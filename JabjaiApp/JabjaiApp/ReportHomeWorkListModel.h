//
//  ReportHomeWorkListModel.h
//  JabjaiApp
//
//  Created by mac on 1/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportHomeWorkListModel : NSObject

@property (assign, nonatomic) long long messageID;
@property (assign, nonatomic) long long homeworkID;
@property (strong, nonatomic) NSString *subjectName;
@property (strong, nonatomic) NSString *detail;
@property (strong, nonatomic) NSDate *sendTime;


@end
