//
//  ASAttendSchoolAgendaTableViewCell.m
//  JabjaiApp
//
//  Created by Mac on 14/9/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "ASAttendSchoolAgendaTableViewCell.h"
#import "Utils.h"
@implementation ASAttendSchoolAgendaTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setColorStatusInView:(int)number{
    //CAGradientLayer *gradient;
    NSLog(@"NUMBER = %d",number);
    [self.inTimeView layoutIfNeeded];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    UIImage * backgroundColorImage;
    switch (number) {
        case 0:
            gradient  = [Utils getGradientColorStatus:@"green"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 1:
            gradient = [Utils getGradientColorStatus:@"yellow"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 3:
            gradient = [Utils getGradientColorStatus:@"red"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 7:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 8:
            gradient = [Utils getGradientColorStatus:@"orange"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 9:
            gradient = [Utils getGradientColorStatus:@"orange"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 10:
            gradient = [Utils getGradientColorStatus:@"pink"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 11:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 12:
            gradient = [Utils getGradientColorStatus:@"blue"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 21:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 22:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 23:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 24:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 25:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 26:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 99:
            gradient = [Utils getGradientColorStatus:@"gray"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        default:
            gradient = [Utils getGradientColorStatus:@"gray"];
            gradient.frame = self.inTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.inTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
    }
    
    // circle
    [self.inTimeView layoutIfNeeded];
    CGFloat maxIn = MAX(self.inTimeView.frame.size.width, self.inTimeView.frame.size.height);
    CGFloat cornerRadiusIn = maxIn / 2.0;
    self.inTimeView.layer.cornerRadius = cornerRadiusIn;
    self.inTimeView.layer.masksToBounds = true;
    
}

- (void) setColorStatusOutView:(int)number{
    [self.outTimeView layoutIfNeeded];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    UIImage * backgroundColorImage;
    switch (number) {
        case 0:
            gradient  = [Utils getGradientColorStatus:@"green"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 1:
            gradient = [Utils getGradientColorStatus:@"yellow"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 2:
            gradient = [Utils getGradientColorStatus:@"red"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 3:
            gradient = [Utils getGradientColorStatus:@"green"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 7:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 8:
            gradient = [Utils getGradientColorStatus:@"orange"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 9:
            gradient = [Utils getGradientColorStatus:@"orange"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 10:
            gradient = [Utils getGradientColorStatus:@"pink"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 11:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 12:
            gradient = [Utils getGradientColorStatus:@"blue"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 99:
            gradient = [Utils getGradientColorStatus:@"gray"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 21:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 22:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 23:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 24:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 25:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 26:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        default:
            gradient = [Utils getGradientColorStatus:@"gray"];
            gradient.frame = self.outTimeView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.outTimeView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
    }
    
    // circle
    [self.outTimeView layoutIfNeeded];
    CGFloat maxOut = MAX(self.outTimeView.frame.size.width, self.outTimeView.frame.size.height);
    CGFloat cornerRadiusOut = maxOut / 2.0;
    self.outTimeView.layer.cornerRadius = cornerRadiusOut;
    self.outTimeView.layer.masksToBounds = true;
}

@end
