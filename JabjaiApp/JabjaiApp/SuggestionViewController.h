//
//  SuggestionViewController.h
//  JabjaiApp
//
//  Created by mac on 4/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"

@interface SuggestionViewController : UIViewController <SlideMenuControllerDelegate ,UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *changeIconMenu;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;


- (IBAction)openDrawer:(id)sender;

- (IBAction)moveBack:(id)sender;



@end
