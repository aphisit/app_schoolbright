//
//  SettingShowIDCardSomeTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 11/2/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingShowIDCardSomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UISwitch *switchOnOff;

- (IBAction)switchAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
