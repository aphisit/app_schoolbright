//
//  SNClassLevelViewController.m
//  JabjaiApp
//
//  Created by toffee on 8/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNClassLevelViewController.h"
#import "EXReportClassHeaderTableViewCell.h"
#import "EXReportClassChildTableViewCell.h"
#import "SNLevelStudentViewController.h"
#import "UserData.h"
#import "Constant.h"
#import "Utils.h"
#import "EXReportStudyClassModel.h"
#import "BSStudentNameRadioTableViewCell.h"
#import "SNTimeOutViewController.h"
#import "AlertDialog.h"
#import "SNClassRoomview.h"

@interface SNClassLevelViewController (){
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classLevelIdArray;
    NSMutableArray *expandedSections;
    NSMutableArray *isSelected;
    NSMutableDictionary<NSNumber *, EXReportStudyClassModel *> *studyClassDict;
    AlertDialog *alertDialog;
    NSMutableArray *sectionKeys;
    NSMutableArray *classroomStringArrray;
    NSMutableArray *classroomIdStringArrray;
    NSMutableArray *userAcceptArray;
    NSString *classLevelId;
    NSNumber *numberindex;
    NSNumber *number;
    NSBundle *myLangBundle;
}

@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;

@end
static NSString *headerCellIdentifier = @"HeaderCell";
static NSString *childCellIdentifier = @"Cell";
static NSString *buttonCellIdentifier = @"ButtonCell";
static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";

@implementation SNClassLevelViewController
@synthesize classLevelId = _classLevelId;
@synthesize classroomId = _classroomId;

- (void)viewDidLoad {
    [super viewDidLoad];

    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    classroomStringArrray = [[NSMutableArray alloc] init];
    
    isSelected = [[NSMutableArray alloc] init];
    self.dict = [[NSMutableDictionary alloc] init];
    self.dicSction = [[NSMutableDictionary alloc] init];
    sectionKeys = [[NSMutableArray alloc] init];
    expandedSections = [[NSMutableArray alloc] init];
    studyClassDict = [[NSMutableDictionary alloc] init];
    self.level2Id = [[NSMutableArray alloc] init];
    userAcceptArray = [[NSMutableArray alloc]init];
    number = 0;
    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([EXReportClassHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:headerCellIdentifier];
    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([SNClassRoomview class]) bundle:nil] forCellReuseIdentifier:childCellIdentifier];
   
    
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    self.tableview.tableFooterView = [[UIView alloc] init];
    self.tableview.separatorInset = UIEdgeInsetsZero;
    [self setLanguage];
    [self getSchoolClassLevel];
   
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
}

-(void)setLanguage{
    
    _headertitelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_SELECT_CLASSLEVEL",nil,myLangBundle,nil);
    [_headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - API Caller

- (void)getSchoolClassLevel {
    
   
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
    
    
}

#pragma mark - CallGetSchoolClassLevelAPIDelegate

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        classLevelIdArray = [[NSMutableArray alloc] init];
        EXReportStudyClassModel *studyClassModel = [[EXReportStudyClassModel alloc] init];
        for(int i=0; i<data.count; i++) {
            
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
            [classLevelIdArray addObject:@([model getClassLevelId])];
            [sectionKeys addObject:@([model getClassLevelId])];
            [studyClassDict setObject:studyClassModel forKey:@([model getClassLevelId])];
            //[self getSchoolClassroomWithClassLevelId:[sectionKeys[i] integerValue]];
        }
      
         [self.tableview reloadData];
        
    }
    
}


- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId ];
    
}

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _classLevelArray = data;
        classroomIdStringArrray = [[NSMutableArray alloc] init];
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
            
        }
       
        classroomStringArrray = [[NSMutableArray alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
            [classroomIdStringArrray addObject:[model getClassroomID]];
            [isSelected addObject: @"NO"];
        }
        [self.dict setObject:data forKey: [NSString stringWithFormat:@"%d",[numberindex integerValue]]];
        [self.dicSction setObject:classroomIdStringArrray forKey: [NSString stringWithFormat:@"%d",[numberindex integerValue]]];
        NSString *actorName = [[self.dict allKeys] objectAtIndex:0];
        NSLog(@"actionName = %@",actorName);
        [self.tableview reloadSections:[NSIndexSet indexSetWithIndex:[numberindex integerValue]] withRowAnimation:UITableViewRowAnimationNone];
    }
    
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(classLevelStringArray != nil) {
        
        return classLevelStringArray.count;
    }
    else {
        return 0;
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([expandedSections containsObject:@(section)]) {
        if([self.dict objectForKey: [NSString stringWithFormat:@"%ld",section]] != nil){
            NSLog(@"count = %d",[[self.dict objectForKey: [NSString stringWithFormat:@"%ld",section]] count]+1);
            
            return [[self.dict objectForKey: [NSString stringWithFormat:@"%ld",  section]] count]+1;

        }else{
            return 1;
        }
    }
    else {
        
        NSLog(@"xx");
        return 1; // show only header
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Row = %d",indexPath.row);
    NSLog(@"section = %d",indexPath.section);
    NSNumber *key = [sectionKeys objectAtIndex:indexPath.section];
    if(indexPath.row == 0 ) { // For the first row of each section we'll show header indexPath.row == 0
        EXReportClassHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier forIndexPath:indexPath];
        BOOL isExpanded = [expandedSections containsObject:@(indexPath.section)];
        if(isExpanded) {
            cell.imgArrow.image = [UIImage imageNamed:@"ic_arrow_down"];
        }
        else {
            cell.imgArrow.image = [UIImage imageNamed:@"ic_arrow_left"];
            
        }
        cell.titleLabel.text = [classLevelStringArray objectAtIndex:indexPath.section];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.userInteractionEnabled = YES;
        return cell;
       
    }
    else {

        SNClassRoomview *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
        SchoolClassroomModel *statusClassroomModel = [[self.dict objectForKey: [NSString stringWithFormat:@"%ld",indexPath.section]] objectAtIndex:indexPath.row-1];
        NSInteger indexOfTheObject = [[statusClassroomModel getClassroomID] integerValue];
        cell.delegate = self;
        cell.index = indexPath.row;
        
        cell.titleLabel.text = [statusClassroomModel getClassroomName];
        
        if([self.level2Id containsObject: @(indexOfTheObject)]) {
            [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
           cell.radioButton.tag = indexPath.section;
        }
        else {
            [cell setRadioButtonClearSelected];
            cell.radioButton.tag = indexPath.section;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = YES;
        return cell;
        
    }
   
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Row = %d",indexPath.row);
    NSLog(@"section = %d",indexPath.section);
    if([expandedSections containsObject:@(indexPath.section)]) {
        if(indexPath.row == 0) { // If select header of each section, then toggle up and down
            [expandedSections removeObject:@(indexPath.section)];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    else {
        
        classLevelId = [classLevelIdArray objectAtIndex:indexPath.section];
        numberindex = @(indexPath.section);
        NSLog(@"ID = %d",[classLevelId integerValue]);
        classroomStringArrray = nil;
        [expandedSections addObject:@(indexPath.section)];
        [self getSchoolClassroomWithClassLevelId:[classLevelId integerValue]];
        
    }
}
- (void)onPressRadioButton:(SNClassRoomview *)tableViewCell atIndex:(NSInteger)index roomID:(NSInteger)roomID tag:(NSInteger)tag roomString:(NSString*)roomString{
    if([self.level2Id containsObject:@(roomID)]){
        [self.level2Id removeObject:@(roomID)];
        [userAcceptArray removeObject:roomString];
        
    }else{
        [self.level2Id addObject:@(roomID)];
        [userAcceptArray addObject:roomString];
    }
    [self.tableview reloadSections:[NSIndexSet indexSetWithIndex:tag] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Utility

- (IBAction)moveBack:(id)sender {
    
    SNLevelStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SendNewsStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (IBAction)actionNext:(id)sender {
    NSLog(@"level2Id = %@",self.level2Id);
    if (self.level2Id.count > 0) {
        SNTimeOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        
        viewController.sendType = self.sendType;
        viewController.sendGroup = self.sendGroup;
        viewController.newsType = self.newsType;
        viewController.level2id = self.level2Id;
        viewController.userAcceptArray = userAcceptArray;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
    }
    else{
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_LEAST_LIST",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
    }
}
@end

