//
//  BSReportForStudentDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 7/27/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "BSHistoryModel.h"

#import "SlideMenuController.h"

@interface BSReportForStudentDetailViewController : UIViewController <SlideMenuControllerDelegate>

// Global variables
@property (strong, nonatomic) NSArray<BSHistoryModel *> *bsHistoryArray;
@property (nonatomic) NSInteger remainingScore;
@property (nonatomic) BSHistoryModel *selectedHistoryModel;

@property (weak, nonatomic) IBOutlet UILabel *behaviorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *datetimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *recorderLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingScoreLabel;

- (IBAction)moveBack:(id)sender;
@end
