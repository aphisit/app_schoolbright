//
//  TeacherTimeDetailViewController.h
//  JabjaiApp
//
//  Created by toffee on 5/15/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallTeacherTimeDetailAPI.h"

@interface TeacherTimeDetailViewController : UIViewController<CallTeacherTimeDetailAPIDelegate>

@property (nonatomic) NSInteger ScheduleId;
@property (nonatomic) NSDate* date;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *subjectName;
@property (weak, nonatomic) IBOutlet UILabel *subjectID;
@property (weak, nonatomic) IBOutlet UILabel *classRoom;
@property (weak, nonatomic) IBOutlet UILabel *timeIn;
@property (weak, nonatomic) IBOutlet UILabel *timeOut;
- (IBAction)moveBack:(id)sender;



@end
