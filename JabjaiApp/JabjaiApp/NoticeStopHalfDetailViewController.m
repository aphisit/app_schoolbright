//
//  NoticeStopHalfDetailViewController.m
//  JabjaiApp
//
//  Created by mac on 3/7/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "NoticeStopHalfDetailViewController.h"
#import "UserData.h"
#import "Utils.h"
#import "AlertDialog.h"
#import "Utils.h"
#import "APIURL.h"
#import "IDValueModel.h"
#import "Constant.h"
#import <Photos/Photos.h>
@interface NoticeStopHalfDetailViewController (){
    
    AlertDialog *alertDialog;
    UIImage *imager;
    PHImageRequestOptions *requestOptions;
    NSMutableArray *addImage;
    int exitRownumber;
    NSString* dateString ,*road ,*phoneNumber;
    NSMutableArray *provinceArr;
    NSMutableArray *districtArr;
    NSMutableArray *subDistrictArr;
    NSString *headSubDistrict, *headDistrict, *headProvince, *headPhoneNumber;
    TableListDialog *tableListDialog;
    NSInteger connectCounter;
    NSBundle *myLangBundle;
    NSArray *imagesArray;
}

@property (assign, nonatomic) NSInteger provinceID;
@property (assign, nonatomic) NSInteger districtID;
@property (assign, nonatomic) NSInteger subDistrictID;
@property (nonatomic, assign) BOOL shouldCapitalizeNextChar;
@property (strong , nonatomic) PhotoAlbumLibary *photoAlbumLibary;
@end

static NSString *cellIdentifier = @"Cell";
static NSString *provinceDialogRequestCode = @"provinceDialogRequestCode";
static NSString *districtDialogRequestCode = @"districtDialogRequestCode";
static NSString *subDistrictDialogRequestCode = @"subDistrictDialogRequestCode";

@implementation NoticeStopHalfDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    alertDialog = [[AlertDialog alloc] init];
    tableListDialog = [[TableListDialog alloc] init];
    self.fillProvince.delegate = self;
    self.fillDistrict.delegate = self;
    self.fillSubDistrict.delegate = self;
    self.fillAddress.delegate = self;
    self.fillPhone.delegate = self;
    self.shouldCapitalizeNextChar = YES;
    road = @"";
    headSubDistrict = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_DISTRICT",nil,myLangBundle,nil);
    headDistrict = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_AMPHUR",nil,myLangBundle,nil);
    headProvince = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PROVINCE",nil,myLangBundle,nil);
    headPhoneNumber = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PHONE_NUMBER",nil,myLangBundle,nil);

    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(hideKeyboard)];
    keyboardToolbar.items = @[doneBarButton];
    self.fillDetail.inputAccessoryView = keyboardToolbar;
    
    connectCounter = 0;
    [self setLanguage];
    [self doInit];
    [self addDoneButtonOnkeyboard];
    
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
    
    [self.shadowDetailView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.shadowDetailView.layer setShadowOpacity:0.3];
    [self.shadowDetailView.layer setShadowRadius:3.0];
    [self.shadowDetailView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.shadowImageView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.shadowImageView.layer setShadowOpacity:0.3];
    [self.shadowImageView.layer setShadowRadius:3.0];
    [self.shadowImageView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.shadowAddress.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.shadowAddress.layer setShadowOpacity:0.3];
    [self.shadowAddress.layer setShadowRadius:3.0];
    [self.shadowAddress.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

-(void)setLanguage{
    
    self.headerLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE",nil,myLangBundle,nil);
    self.headerDescriptionLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_DESCRIPTION",nil,myLangBundle,nil);
    self.headerAddressLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_ADDRESS",nil,myLangBundle,nil);
    self.headerHouseNoLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_HOUSE_NO",nil,myLangBundle,nil);
    self.headerProvinceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PROVINCE",nil,myLangBundle,nil);
    self.headerAmphurLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_AMPHUR",nil,myLangBundle,nil);
    self.headerDistrictLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_DISTRICT",nil,myLangBundle,nil);
    self.headerPhoneNumberLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PHONE_NUMBER",nil,myLangBundle,nil);
    self.fillAddress.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEAVE_HOMEADDRESS",nil,myLangBundle,nil);
    self.fillProvince.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEAVE_PROVINCE",nil,myLangBundle,nil);
    self.fillDistrict.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEAVE_DISTRICT",nil,myLangBundle,nil);
    self.fillSubDistrict.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEAVE_SUBDISTRICT",nil,myLangBundle,nil);
    self.fillPhone.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEAVE_PHONENUMBER",nil,myLangBundle,nil);
    [self.headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_LEAVE_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

#pragma mark - UITextViewDelegate
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    if (textView.tag == 0) {
        self.fillDetail.text = @"";
    }
    self.fillDetail.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView {
    if(self.fillDetail.text.length == 0) {
        self.fillDetail.tag = 0;
        self.fillDetail.textColor = [UIColor lightGrayColor];
        self.fillDetail.text = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEAVE_DETAIL",nil,myLangBundle,nil);;
        [self.fillDetail resignFirstResponder];
    }else{
        self.fillDetail.tag = 1;
    }
}

-(void) textViewShouldEndEditing:(UITextView *)textView {
    if(self.fillDetail.text.length == 0) {
                self.fillDetail.textColor = [UIColor lightGrayColor];
        self.fillDetail.text = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEAVE_DETAIL",nil,myLangBundle,nil);
        [self.fillDetail resignFirstResponder];
    }
}

#pragma mark - Keyboard
- (void)hideKeyboard
{
    [self.fillDetail resignFirstResponder];
}
- (void)addDoneButtonOnkeyboard{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 20)];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.fillPhone.inputAccessoryView = numberToolbar;
}

- (void)cancelNumberPad{
    [self.fillPhone resignFirstResponder];
    self.fillPhone.text = @"";
}

- (void)doneWithNumberPad{
    [self.fillPhone resignFirstResponder];
}

-(void)viewDidLayoutSubviews{
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
    [self doDesignLayout];
}

- (void)doInit{
    self.fillDetail.delegate = self;
    self.fillDetail.tag = self.fillDetailTag;
    if (self.fillDetail.tag == 0) {
        self.fillDetail.text = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEAVE_DETAIL",nil,myLangBundle,nil);
        self.fillDetail.textColor = [UIColor lightGrayColor];
        self.fillDetail.tag = 0;
    }else{
        self.fillDetail.tag = 1;
        self.fillDetail.text = self.noticeDetail;
        self.fillAddress.text = self.address;
        self.fillProvince.text = self.province;
        self.fillDistrict.text = self.district;
        self.fillSubDistrict.text = self.subDistrict;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [imagesArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //    static NSString *cellIdentifier = @"Cell";
    NoticeStopCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.imageNotice.tag = 200;
    cell.imageNotice.image = [imagesArray objectAtIndex:indexPath.row];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:   (UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(60, 100);
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    NSString *dialogTitle = @"แจ้งเตือน";
    
    if(textField.tag == 1) { //province text field
        
        if(tableListDialog == nil) {
            [self getProvinceInfo];

        }
        else if(tableListDialog != nil && ![tableListDialog isDialogShowing]) {
            [self getProvinceInfo];
        }
        
        return NO;
    }
    else if(textField.tag == 2) {
        
        if([self.fillProvince.text length] == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_LEAVE_FIL_PROVINCE",nil,myLangBundle,nil);
            [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        }
        else {
            
            if(tableListDialog == nil) {
                [self getDistrictInfo];
            }
            else if(tableListDialog != nil && ![tableListDialog isDialogShowing]) {
                [self getDistrictInfo];
            }
            
        }
        
        return NO;
    }
    else if(textField.tag == 3) {
        
        if([self.fillProvince.text length] == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_LEAVE_FIL_PROVINCE",nil,myLangBundle,nil);
            [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        }
        else if([self.fillDistrict.text length] == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_LEAVE_FIL_AMPHUR",nil,myLangBundle,nil);
            [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        }
        else {
            
            if(tableListDialog == nil) {
                [self getSubDistrictInfo];
            }
            else if(tableListDialog != nil && ![tableListDialog isDialogShowing]) {
                [self getSubDistrictInfo];
        
            }
        }
        
        return NO;
    }
    return YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@" "]) {
        self.shouldCapitalizeNextChar = YES;
        return NO;
    }
    if (self.shouldCapitalizeNextChar) {
        NSString *capitalizedChar = [string capitalizedString];
        textField.text = [textField.text stringByAppendingString:capitalizedChar];
        self.shouldCapitalizeNextChar = NO;
        return NO;
    }
    return YES;
}


- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index{
    
    if([requestCode isEqualToString:provinceDialogRequestCode]) {
        
        self.fillProvince.text = [[provinceArr objectAtIndex:index] getVal];
        self.provinceID = [[[provinceArr objectAtIndex:index] getID] integerValue];
        
        // Clear dependency text field value
        self.fillDistrict.text = @"";
        self.fillSubDistrict.text = @"";
        
    }
    else if([requestCode isEqualToString:districtDialogRequestCode]) {
        
        self.fillDistrict.text = [[districtArr objectAtIndex:index] getVal];
        self.districtID = [[[districtArr objectAtIndex:index] getID] integerValue];
        
        // Clear dependency text field value
        self.fillSubDistrict.text = @"";
        
    }
    else if([requestCode isEqualToString:subDistrictDialogRequestCode]) {
        
        self.fillSubDistrict.text = [[subDistrictArr objectAtIndex:index] getVal];
        self.subDistrictID = [[[subDistrictArr objectAtIndex:index] getID] integerValue];
    }
}

#pragma mark - GetAPIData

- (void)getProvinceInfo {
    NSString *URLString = [APIURL getProvinceURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getProvinceInfo];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getProvinceInfo];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getProvinceInfo];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                provinceArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSNumber *ID = [[NSNumber alloc] initWithInt:(int)[[dataDict objectForKey:@"PROVINCE_ID"] integerValue]];
                    NSMutableString *Value = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"PROVINCE_NAME"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) Value);
                    
                    IDValueModel *model = [[IDValueModel alloc] init];
                    model.ID = ID;
                    model.Value = Value;
                    
                    [provinceArr addObject:model];
                    [dataArr addObject:Value];
                }
                
                // Create NSArray data to show in dialog
                [self showTableListDialogWithRequestCode:provinceDialogRequestCode data:dataArr];
                
            }
        }
        
    }];
    
}

- (void)getDistrictInfo {
    NSString *URLString = [APIURL getDistrictURL:self.provinceID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDistrictInfo];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDistrictInfo];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDistrictInfo];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                districtArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSNumber *ID = [[NSNumber alloc] initWithInt:(int)[[dataDict objectForKey:@"AMPHUR_ID"] integerValue]];
                    NSMutableString *Value = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"AMPHUR_NAME"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) Value);
                    
                    IDValueModel *model = [[IDValueModel alloc] init];
                    model.ID = ID;
                    model.Value = Value;
                    
                    [districtArr addObject:model];
                    [dataArr addObject:Value];
                }
                
                // Create NSArray data to show in dialog
                [self showTableListDialogWithRequestCode:districtDialogRequestCode data:dataArr];
                
            }
            
        }
    }];
    
}

- (void)getSubDistrictInfo {
    NSString *URLString = [APIURL getSubDistrictURL:self.districtID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSubDistrictInfo];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubDistrictInfo];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubDistrictInfo];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                subDistrictArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSNumber *ID = [[NSNumber alloc] initWithInt:(int)[[dataDict objectForKey:@"DISTRICT_ID"] integerValue]];
                    NSMutableString *Value = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"DISTRICT_NAME"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) Value);
                    
                    IDValueModel *model = [[IDValueModel alloc] init];
                    model.ID = ID;
                    model.Value = Value;
                    
                    [subDistrictArr addObject:model];
                    [dataArr addObject:Value];
                }
                
                // Create NSArray data to show in dialog
                [self showTableListDialogWithRequestCode:subDistrictDialogRequestCode data:dataArr];
                
            }
            
        }
    }];
    
}

#pragma mark - PhotoAlbumDelegate
- (void) replyPhotoAlbum:(PhotoAlbumLibary *)classObj imageArray:(NSMutableArray *)imageArray{
    if (imageArray != nil) {
        self.headerNextbtn.enabled = YES;
        imagesArray = imageArray;
        [self.collectionView reloadData];
    }
}

- (IBAction)selectImage:(id)sender {
    
    self.headerNextbtn.enabled = NO;
    self.photoAlbumLibary = [[PhotoAlbumLibary alloc] init];
    self.photoAlbumLibary.delegate  = self;
    [self.photoAlbumLibary showPhotoAlbumView:self.view];
}

- (IBAction)moveBack:(id)sender {
    
    NoticeStopHalfViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeStopHalfDayStoryboard"];
    
    viewController.leaveCause = self.leaveCause;
    viewController.leaveFirstDate = self.leaveFirstDate;
    viewController.leavePeriod = self.leavePeriod;
    viewController.leaveStartdate = self.leaveStartdate;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
}


- (IBAction)dismissKeyboard:(id)sender {
    
    [self resignFirstResponder];
    
}

- (IBAction)nextPage:(id)sender {
    self.noticeDetail = self.fillDetail.text;
    if ([self validateData]) {
        
        if ([UserData getUserType] != STUDENT) {
            NoticeStopHalfConfimTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeStopHalfConfirmTeacherStoryboard"];
            viewController.leaveCause = self.leaveCause;
            viewController.leaveFirstDate = self.leaveFirstDate;
            viewController.leavePeriod = self.leavePeriod;
            viewController.imageNotice = imagesArray;
            viewController.noticeDetail = self.fillDetail.text;
            // Date not format
            viewController.leaveStartdate = self.leaveStartdate;
            viewController.leaveEndDate = self.leaveEndDate;
            viewController.fillDetailTag  = (int)self.fillDetail.tag;
            viewController.headTitleAddress = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_ADDRESS",nil,myLangBundle,nil);
                
            if ([self.fillProvince.text isEqual:@"กรุงเทพมหานคร"]) {
                headSubDistrict = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_DISTRICT",nil,myLangBundle,nil);
                headDistrict = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_AREA",nil,myLangBundle,nil);
                viewController.headSubDistrict = headSubDistrict;
                viewController.headDistrict = headDistrict;
                viewController.headProvince = headProvince;
                viewController.headPhoneNumber = headPhoneNumber;
                viewController.address = self.fillAddress.text;
                viewController.province = self.fillProvince.text;
                viewController.district = self.fillDistrict.text;
                viewController.subDistrict = self.fillSubDistrict.text;
                viewController.phoneNumber = self.fillPhone.text;
                viewController.road = road;
                viewController.leaveStartdate = self.leaveStartdate;
                viewController.leaveEndDate = self.leaveEndDate;
            }
            else{
                viewController.headSubDistrict = headSubDistrict;
                viewController.headDistrict = headDistrict;
                viewController.headProvince = headProvince;
                viewController.headPhoneNumber = headPhoneNumber;
                viewController.address = self.fillAddress.text;
                viewController.province = self.fillProvince.text;
                viewController.district = self.fillDistrict.text;
                viewController.subDistrict = self.fillSubDistrict.text;
                viewController.phoneNumber = self.fillPhone.text;
                viewController.road = road;
                viewController.leaveStartdate = self.leaveStartdate;
                viewController.leaveEndDate = self.leaveEndDate;
            }
            
            [self.slideMenuController changeMainViewController:viewController close:YES];
        }
        else{
            
            NoticeStopHalfConfirmStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeStopHalfConfirmStudentStoryboard"];
            
            viewController.leaveCause = self.leaveCause;
            viewController.leaveFirstDate = self.leaveFirstDate;
            viewController.leavePeriod = self.leavePeriod;
            viewController.imageNotice = imagesArray;
            viewController.noticeDetail = self.fillDetail.text;
            // Date not format
            viewController.leaveStartdate = self.leaveStartdate;
            viewController.fillDetailTag  = (int)self.fillDetail.tag;
            //if (self.checkButton.isSelected == YES) {
                viewController.headTitleAddress = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_ADDRESS",nil,myLangBundle,nil);
                if ([self.fillProvince.text isEqual:@"กรุงเทพมหานคร"]) {
                    headSubDistrict = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_DISTRICT",nil,myLangBundle,nil);
                    headDistrict = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_AREA",nil,myLangBundle,nil);
                    viewController.headSubDistrict = headSubDistrict;
                    viewController.headDistrict = headDistrict;
                    viewController.headProvince = headProvince;
                    viewController.headPhoneNumber = headPhoneNumber;
                    viewController.address = self.fillAddress.text;
                    viewController.province = self.fillProvince.text;
                    viewController.district = self.fillDistrict.text;
                    viewController.subDistrict = self.fillSubDistrict.text;
                    viewController.phoneNumber = self.fillPhone.text;
                    viewController.road = road;
                    viewController.leaveStartdate = self.leaveStartdate;
                }
                else{
                    viewController.headSubDistrict = headSubDistrict;
                    viewController.headDistrict = headDistrict;
                    viewController.headProvince = headProvince;
                    viewController.headPhoneNumber = headPhoneNumber;
                    viewController.address = self.fillAddress.text;
                    viewController.province = self.fillProvince.text;
                    viewController.district = self.fillDistrict.text;
                    viewController.subDistrict = self.fillSubDistrict.text;
                    viewController.phoneNumber = self.fillPhone.text;
                    viewController.road = road;
                    viewController.leaveStartdate = self.leaveStartdate;
                }
            [self.slideMenuController changeMainViewController:viewController close:YES];

        }

    }
}

-(BOOL)validateData{
    
    NSString *dialogTitle = @"แจ้งเตือน";
    
    if (self.fillDetail.tag == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_LEAVE_FIL_DETAILS",nil,myLangBundle,nil);
        
        [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        
        return NO;
    }
    
    else{
        
        if ([self.fillAddress.text length] == 0 && [self.fillProvince.text length] == 0 && [self.fillDistrict.text length] == 0 && [self.fillSubDistrict.text length] == 0 && [self.fillPhone.text length] == 0 && [self.fillPhone.text length] == 0){
            
            headSubDistrict = @"";
            headDistrict = @"";
            headProvince = @"";
            headPhoneNumber = @"";
            
            return YES;
            
        }else if ([self.fillAddress.text length] != 0 && [self.fillProvince.text length] != 0 && [self.fillDistrict.text length] != 0 && [self.fillSubDistrict.text length] != 0 && [self.fillPhone.text length] != 0 && [self.fillPhone.text length] != 0){
            return YES;
        }else{
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_LEAVE_PLEASE_DETAILS",nil,myLangBundle,nil);
            [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
            return NO;
        }
    }
    

    return YES;
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
    
    [self.view endEditing:YES];
}

- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
    [self.view endEditing:YES];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

@end
