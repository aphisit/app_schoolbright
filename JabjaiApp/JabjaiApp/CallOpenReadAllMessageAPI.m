//
//  CallOpenReadAllMessageAPI.m
//  JabjaiApp
//
//  Created by toffee on 20/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallOpenReadAllMessageAPI.h"
#import "Utils.h"
#import "APIURL.h"
#import "UserData.h"

@implementation CallOpenReadAllMessageAPI{
    NSInteger connectCounter;
    NSString *str;
}

-(void) call:(long long)userId schoolid:(long long)schoolid{
    [self openMessageDataAll:userId schoolid:schoolid];
}

- (void)openMessageDataAll:(long long)userId  schoolid:(long long)schoolid{
    NSString *URLString = [APIURL getOpenMessageDataAll:userId schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
            
        }else{
            str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
           
            NSLog(@"xxxx = %@",str);
            
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callOpenReadAllMessageAPI:data:sucess:)]) {
                [self.delegate callOpenReadAllMessageAPI:self data:str sucess:YES];
            }

        }
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callOpenReadAllMessageAPI:data:sucess:)]) {
                [self.delegate callOpenReadAllMessageAPI:self data:str sucess:NO];
            }
        }
        
        
        
    }];
}

@end
