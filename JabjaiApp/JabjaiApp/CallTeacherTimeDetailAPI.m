//
//  CallTeacherTimeDetailAPI.m
//  JabjaiApp
//
//  Created by toffee on 5/15/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallTeacherTimeDetailAPI.h"
#import "APIURL.h"
#import "UserData.h"
#import "Utils.h"

@implementation CallTeacherTimeDetailAPI{
    NSInteger connectCounter;
}
- (void)call:(long long)ScheduleId date:(NSDate *)date schoolid:(long long)schoolid{
    connectCounter = 0;
    [self getTeacherDetail:ScheduleId date:date schoolid:schoolid];
}
#pragma mark - Get API Data
- (void)getTeacherDetail:(long long)ScheduleId date:(NSDate*)date schoolid:(long long)schoolid{
    
    NSString *URLString = [APIURL getTeacherTimeDetail:[UserData getUserID] date:date ScheduleId:ScheduleId schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getTeacherDetail:ScheduleId date:date schoolid:schoolid];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSArray *returnedArray = returnedData;
            connectCounter = 0;
            
            NSDictionary *dataDict = [returnedArray objectAtIndex:0];
            NSString *subjectID = [dataDict objectForKey:@"ScheduleCode"] ;
            NSString *subjectName = [dataDict objectForKey:@"ScheduleName"] ;
            NSString *classRoom = [dataDict objectForKey:@"classname"] ;
            NSString *timeIn = [dataDict objectForKey:@"TimeIn"] ;
            NSString *timeOut = [dataDict objectForKey:@"TimeOut"] ;
            
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTeacherTimeDetailAPI:subjectName:subjectID:classRoom:timeIn:timeOut:success:)]) {
                    [self.delegate callTeacherTimeDetailAPI:self subjectName:subjectName subjectID:subjectID classRoom:classRoom timeIn:timeIn timeOut:timeOut success:YES];
            }
            
            return ;
        }
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTeacherTimeDetailAPI:subjectName:subjectID:classRoom:timeIn:timeOut:success:)]) {
                [self.delegate callTeacherTimeDetailAPI:self subjectName:nil subjectID:nil classRoom:nil timeIn:nil timeOut:nil success:NO];
           }
        }
        
    }];
    
}


@end

