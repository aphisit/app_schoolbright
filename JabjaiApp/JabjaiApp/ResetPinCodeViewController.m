//
//  ResetPinCodeViewController.m
//  JabjaiApp
//
//  Created by toffee on 6/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ResetPinCodeViewController.h"
#import <CommonCrypto/CommonHMAC.h>
#import "UserData.h"
#import "UIView+Shake.h"
#import "AlertDialogConfirm.h"
#import "SettingViewController.h"
#import "Utils.h"
@interface ResetPinCodeViewController (){
    NSString *mode;
    NSString *numberPinOld;
    NSString *numberAddPin;
    NSString *numberConfirmPin;
    NSMutableArray  *numberArray;
    NSMutableString *numberCode;
    
    AlertDialogConfirm *alertSuccessDialog;
}
@property (nonatomic, assign) BOOL isShowing;
@property (strong, nonatomic) CallRPUpdateNewPinPOSTAPI *callRPUpdateNewPinPOSTAPI;
@end

@implementation ResetPinCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     mode = ADD_PINCODE;
    _confirmPinUnfair.hidden = YES;
    self.contentView.layer.masksToBounds = YES;
    self.isShowing = NO;
    numberArray = [[NSMutableArray alloc] init];
    numberCode = [[NSMutableString alloc] init];
    
    _passView1.layer.cornerRadius = _passView1.bounds.size.width/2;
    _passView1.layer.masksToBounds = YES;
    _passView1.layer.borderWidth = 1;
    
    _passView2.layer.cornerRadius = _passView2.bounds.size.width/2;
    _passView2.layer.masksToBounds = YES;
    _passView2.layer.borderWidth = 1;
    
    _passView3.layer.cornerRadius = _passView3.bounds.size.width/2;
    _passView3.layer.masksToBounds = YES;
    _passView3.layer.borderWidth = 1;
    
    _passView4.layer.cornerRadius = _passView4.bounds.size.width/2;
    _passView4.layer.masksToBounds = YES;
    _passView4.layer.borderWidth = 1;
    
    _passView5.layer.cornerRadius = _passView5.bounds.size.width/2;
    _passView5.layer.masksToBounds = YES;
    _passView5.layer.borderWidth = 1;
    
    _passView6.layer.cornerRadius = _passView6.bounds.size.width/2;
    _passView6.layer.masksToBounds = YES;
    _passView6.layer.borderWidth = 1;
    // Do any additional setup after loading the view from its nib.
    self.confirmPinUnfair.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHANGE_PIN_INCORRECT",nil,[Utils getLanguage],nil);
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHANGE_PIN",nil,[Utils getLanguage],nil);
    [_headerPinLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_CHANGE_PIN_NEW_PIN",nil,[Utils getLanguage],nil)];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

-  (void)doUpdatePinCode:(NSString*)jsonCode  {
    
    if(self.callRPUpdateNewPinPOSTAPI != nil) {
        self.callRPUpdateNewPinPOSTAPI = nil;
    }
    
    self.callRPUpdateNewPinPOSTAPI = [[CallRPUpdateNewPinPOSTAPI alloc] init];
    self.callRPUpdateNewPinPOSTAPI.delegate = self;
    [self.callRPUpdateNewPinPOSTAPI call:jsonCode] ;
    
}
-(void)callRPUpdateNewPinPOSTAPI:(CallRPUpdateNewPinPOSTAPI *)classObj response:(NSString *)response success:(BOOL)success{
    NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_CHANGE_PIN_SUCCESS",nil,[Utils getLanguage],nil);
    [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
}

-(void)showDialogInView:(UIView *)targetView pinOld:(NSString *)pinOld{
   
    numberPinOld = pinOld;
    _confirmPinUnfair.hidden = YES;
    mode = ADD_PINCODE;
    [self clearPin];
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.contentView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentView setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
}
-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(successConfirmPin)]) {
        [self.delegate successConfirmPin];
    }
    
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}
//Close Dialog
-(void)onAlertDialogClose{
    [self dismissDialog];
}

//CreateJson
-(NSString *)createJsonString:(NSString*)pinCode newPin:(NSString*)newPin{
    NSString  *jsonString = [[NSString alloc] initWithFormat:@"{ \"PinOld\" : \"%@\", \"User_Id\" : %d , \"PinNew\" : \"%@\", \"schoolid\" : %lld}", pinCode, [UserData getUserID], newPin, [UserData getSchoolId] ];
    
    return jsonString;
}
//hash Pin sha256
- (NSMutableString *)hmacForKey1:(NSString *)key andStringData:(NSString *)data
{
    
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    NSMutableString *stringOut = [NSMutableString stringWithCapacity:HMAC.length];
    const unsigned char *macOutBytes = HMAC.bytes;
    
    for (NSInteger i=0; i<HMAC.length; ++i) {
        [stringOut appendFormat:@"%02x", macOutBytes[i]];
    }
    return stringOut;
    
}

-(void)clearPin{
   
    [_passView1 setBackgroundColor:[UIColor clearColor]];
    [_passView2 setBackgroundColor:[UIColor clearColor]];
    [_passView3 setBackgroundColor:[UIColor clearColor]];
    [_passView4 setBackgroundColor:[UIColor clearColor]];
    [_passView5 setBackgroundColor:[UIColor clearColor]];
    [_passView6 setBackgroundColor:[UIColor clearColor]];
    
    numberArray = [[NSMutableArray alloc] init];
    numberCode = [[NSMutableString alloc] init];
    
}

//check pin page AddPin with ConfirmPin
-(void)checksamePin{
    numberConfirmPin = numberCode;
    if ([numberAddPin isEqualToString:numberConfirmPin]) {
        
        NSString *key = @"ra7$K:L.]%";
        NSMutableString *hmac1 = [self hmacForKey1:key andStringData:numberPinOld];
        NSString *jsonString = [self createJsonString:hmac1 newPin:numberCode];
        [self doUpdatePinCode:jsonString];
        
    }else{
        
        _confirmPinUnfair.hidden = NO;
        [self.passView1 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView2 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView3 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView4 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView5 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView6 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
    }
}


- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    if(alertSuccessDialog != nil && [alertSuccessDialog isDialogShowing]) {
        [alertSuccessDialog dismissDialog];
    }
    else {
        alertSuccessDialog = [[AlertDialogConfirm alloc] init];
        alertSuccessDialog.delegate = self;
    }
     [alertSuccessDialog showDialogInView:self.view title:title message:message];
    
}
-(void)onTick:(NSTimer *)timer {
    [self clearPin];
}


- (IBAction)actionButtonNumber:(UIControl*)sender {
    
    if (sender.tag != 10) {
        if (numberArray.count < 6) {
            [numberCode appendString:[NSString stringWithFormat:@"%d",sender.tag]];
            [numberArray addObject:[NSString stringWithFormat:@"%d",sender.tag]];
            
            if (numberArray.count == 1) {
                
                [_passView1 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 2) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 3) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 4) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 5) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 6) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
                [_passView6 setBackgroundColor:[UIColor grayColor]];
                
                
                if (mode == ADD_PINCODE) {
                    mode = CONFIRM_PINCODE;
                    numberAddPin = numberCode;
                     [_headerPinLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_CHANGE_PIN_NEW_PIN_AGAIN",nil,[Utils getLanguage],nil)];
                    [NSTimer scheduledTimerWithTimeInterval:0.1
                                                     target:self
                                                   selector:@selector(onTick:)
                                                   userInfo:nil
                                                    repeats:NO];
                    
                   
                }else{
                    [self checksamePin];

                }

            }
            
        }
    }else{
        _confirmPinUnfair.hidden = YES;
        if (numberArray.count > 0) {
            [numberCode deleteCharactersInRange:NSMakeRange(numberArray.count-1, 1)];
            [numberArray removeObjectAtIndex:numberArray.count-1];
            [_passView1 setBackgroundColor:[UIColor clearColor]];
            [_passView2 setBackgroundColor:[UIColor clearColor]];
            [_passView3 setBackgroundColor:[UIColor clearColor]];
            [_passView4 setBackgroundColor:[UIColor clearColor]];
            [_passView5 setBackgroundColor:[UIColor clearColor]];
            [_passView6 setBackgroundColor:[UIColor clearColor]];
            if (numberArray.count == 1) {
                
                [_passView1 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 2) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 3) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 4) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 5) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 6) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
                [_passView6 setBackgroundColor:[UIColor grayColor]];
            }
            
        }
    }
    
}
- (IBAction)moveBack:(id)sender {
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(onMoveBack)]) {
        [self.delegate onMoveBack];
    }
    
}

@end
