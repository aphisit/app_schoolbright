//
//  CallTFGetTuitionDetailAPI.m
//  JabjaiApp
//
//  Created by Mac on 2/10/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallTFGetTuitionDetailAPI.h"
#import "Utils.h"
#import "APIURL.h"

@implementation CallTFGetTuitionDetailAPI{
    NSInteger connectCounter;
    NSMutableArray<TFProductModel*> *productListModelArray;
    NSMutableArray<TFInvoicesModel*> *invoicesModelArray;
    TFitionFeesModel *invoiceModel;
}
- (void)call:(long long)userID invoiceID:(long long)invoiceID schoolId:(long long)schoolId{
    connectCounter = 0;
    [self getGetTuitionDetail:userID invoiceID:invoiceID schoolId:schoolId];
}

- (void) getGetTuitionDetail:(long long)userID invoiceID:(long long)invoiceID schoolId:(long long)schoolId{
    NSString *URLString = [APIURL getSchoolFeeDetailWithUserID:userID InvoiceID:invoiceID schoolid:schoolId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }

            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;

                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getGetTuitionDetail:userID invoiceID:invoiceID schoolId:schoolId];

            }
            else {
                isFail = YES;
                self->connectCounter = 0;
            }

        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getGetTuitionDetail:userID invoiceID:invoiceID schoolId:schoolId];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
                
            }
//            else if(![returnedData isKindOfClass:[NSArray class]]) {
//
//                if(self->connectCounter < TRY_CONNECT_MAX) {
//                    self->connectCounter++;
//
//                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
//                    [self getGetTuitionDetail:userID invoiceID:invoiceID schoolId:schoolId];
//                }
//                else {
//                    isFail = YES;
//
//                    self->connectCounter = 0;
//                }
//            }
            else {
                    id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    NSArray *productListArray, *invoicesArray;
                    NSString *productName, *term, *year, *studentName, *studentCode, *level, *className, *imageSchoolIconUrl, *schoolEngName, *schoolThaiName, *paymentDate;
                    int status = 0, paymentGroupId = 0;
                    double productPrice = 0, totalPrice = 0, amount = 0;
                    NSString *dueDate, *issueDate;
                
                    if(self->productListModelArray != nil) {
                        [self->productListModelArray removeAllObjects];
                        self->productListModelArray = nil;
                    }
                    self->productListModelArray = [[NSMutableArray alloc] init];
                    if(self->invoicesModelArray != nil) {
                        [self->invoicesModelArray removeAllObjects];
                        self->invoicesModelArray = nil;
                    }
                    self->invoicesModelArray = [[NSMutableArray alloc] init];
                
                    productListArray = [returnedData objectForKey:@"product"];
                    for (int i = 0; i<productListArray.count; i++) {
                        NSDictionary  *dataDict = [productListArray objectAtIndex:i];
                        if(![[dataDict objectForKey:@"product_name"] isKindOfClass:[NSNull class]]) {
                        productName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"product_name"]];
                        }
                        else {
                            productName = [[NSMutableString alloc] initWithString:@""];
                        }
                        productPrice = [[dataDict objectForKey:@"price"] doubleValue];
                        TFProductModel *model = [[TFProductModel alloc] init];
                        model.productName = productName;
                        model.productPrice = productPrice;
                        [self->productListModelArray addObject:model];
                    }
                
                    invoicesArray = [returnedData objectForKey:@"invoicesPayments"];
                    for (int i = 0; i<invoicesArray.count; i++) {
                        NSDictionary  *dataDict = [invoicesArray objectAtIndex:i];
                        amount = [[dataDict objectForKey:@"amount"] doubleValue];
                        paymentGroupId = [[dataDict objectForKey:@"paymentGroupId"] intValue];
                        if(![[dataDict objectForKey:@"paymentDate"] isKindOfClass:[NSNull class]]){
                            paymentDate = [dataDict objectForKey:@"paymentDate"];
                        }
                        else{
                            paymentDate = @"";
                        }
                        TFInvoicesModel *model = [[TFInvoicesModel alloc] init];
                        model.amount = amount;
                        model.paymentDate = paymentDate;
                        model.paymentGroupId = paymentGroupId;
                        
                        [self->invoicesModelArray addObject:model];
                    }
                
                
                    totalPrice = [[returnedData objectForKey:@"totalprice"] doubleValue];
                    status = [[returnedData objectForKey:@"status"] intValue];
                
                    if (![[returnedData objectForKey:@"trem"] isKindOfClass:[NSNull class]]) {
                        term = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"trem"]];
                    }
                    else{
                        term = [[NSMutableString alloc] initWithString:@""];
                    }

                    if (![[returnedData objectForKey:@"yesr"] isKindOfClass:[NSNull class]]) {
                        year = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"yesr"]];
                    }
                    else{
                        year = [[NSMutableString alloc] initWithString:@""];
                    }

                    if (![[returnedData objectForKey:@"student_name"] isKindOfClass:[NSNull class]]) {
                        studentName = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"student_name"]];
                    }
                    else{
                        studentName = [[NSMutableString alloc] initWithString:@""];
                    }

                    if (![[returnedData objectForKey:@"student_id"] isKindOfClass:[NSNull class]]) {
                        studentCode = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"student_id"]];
                    }
                    else{
                        studentCode = [[NSMutableString alloc] initWithString:@""];
                    }

                    if (![[returnedData objectForKey:@"level_name"] isKindOfClass:[NSNull class]]) {
                        level = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"level_name"]];
                    }
                    else{
                        level = [[NSMutableString alloc] initWithString:@""];
                    }

                    if (![[returnedData objectForKey:@"class_name"] isKindOfClass:[NSNull class]]) {
                        className = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"class_name"]];
                    }
                    else{
                        className = [[NSMutableString alloc] initWithString:@""];
                    }
                
                    if(![[returnedData objectForKey:@"school_pic"] isKindOfClass:[NSNull class]]){
                        imageSchoolIconUrl = [returnedData objectForKey:@"school_pic"];
                    }
                    else{
                        imageSchoolIconUrl = @"";
                    }
        
                    if(![[returnedData objectForKey:@"school_nameEN"] isKindOfClass:[NSNull class]]){
                        schoolEngName = [returnedData objectForKey:@"school_nameEN"];
                    }
                    else{
                        schoolEngName = @"";
                    }
        
                    if(![[returnedData objectForKey:@"school_name"] isKindOfClass:[NSNull class]]){
                        schoolThaiName = [returnedData objectForKey:@"school_name"];
                    }
                    else{
                        schoolThaiName = @"";
                    }
                
                    if(![[returnedData objectForKey:@"dueDate"] isKindOfClass:[NSNull class]]){
                        dueDate = [returnedData objectForKey:@"dueDate"];
                    }
                    else{
                        dueDate = @"";
                    }
                
                    if(![[returnedData objectForKey:@"issuedDate"] isKindOfClass:[NSNull class]]){
                        issueDate = [returnedData objectForKey:@"issuedDate"];
                    }
                    else{
                        issueDate = @"";
                    }
                    
                    self->invoiceModel = [[TFitionFeesModel alloc] init];
                    self->invoiceModel.schoolEngName = schoolEngName;
                    self->invoiceModel.schoolThaiName = schoolThaiName;
                    self->invoiceModel.yearName = year;
                    self->invoiceModel.termName = term;
                    self->invoiceModel.studentName = studentName;
                    self->invoiceModel.studentCode = studentCode;
                    self->invoiceModel.className = className;
                    self->invoiceModel.levelName = level;
                    self->invoiceModel.imageUrl = imageSchoolIconUrl;
                    self->invoiceModel.status = status;
                    self->invoiceModel.price = totalPrice;
                    self->invoiceModel.dueDate = dueDate;
                    self->invoiceModel.issueDate = issueDate;
                    self->invoiceModel.productListArray = self->productListModelArray;
                    self->invoiceModel.invoicesArray = self->invoicesModelArray;
                
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTFGetTuitionDetailAPI:data:success:)]) {
                        [self.delegate callTFGetTuitionDetailAPI:self data:self->invoiceModel success:YES];
                    }
                
                }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTFGetTuitionDetailAPI:data:success:)]) {
                [self.delegate callTFGetTuitionDetailAPI:self data:self->invoiceModel success:NO];
            }
        }
     
        
    }
     ];
}
@end
