//
//  BSReportScoreListViewController.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//
@import ARSlidingPanel;

#import <UIKit/UIKit.h>
#import "BSHistoryModel.h"
#import "BSReportScoreListViewControllerDelegate.h"
#import "BSReportCalendarViewController.h"
#import "BSReportCalendarViewControllerDelegate.h"
#import "CallGetBSHistoryInDateRangeAPI.h"

@interface BSReportScoreListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CallGetBSHistoryInDateRangeAPIDelegate>

// Global variables
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger statusType;

@property (weak, nonatomic) id<BSReportScoreListViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@end
