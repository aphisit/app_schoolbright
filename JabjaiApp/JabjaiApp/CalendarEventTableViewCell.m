//
//  CalendarEventTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 11/20/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CalendarEventTableViewCell.h"

@implementation CalendarEventTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.colorStatusView.layer.cornerRadius = self.colorStatusView.frame.size.height /2;
//    self.colorStatusView.layer.masksToBounds = YES;
//   self.colorStatusView.layer.borderWidth = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse{
    
    self.eventLabel.text = @"";
    
}

@end
