//
//  ReportInOutSelectClassRoomCollectionViewCell.m
//  JabjaiApp
//
//  Created by toffee on 27/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutSelectClassRoomCollectionViewCell.h"

@implementation ReportInOutSelectClassRoomCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) setBackgroundClick:(int)numberCell numberClick:(int)numberClick{
    if (numberClick == numberCell) {
        self.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
        self.classroomLabel.textColor = [UIColor whiteColor];
       
        
    }else{
       
        self.backgroundColor.backgroundColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.0];
        self.classroomLabel.textColor = [UIColor blackColor];
        
    }
}

@end
