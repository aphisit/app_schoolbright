//
//  MenuReportTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 4/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuReportTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *BackgroundView;



@property (weak, nonatomic) IBOutlet UIImageView *iconReport;
@property (weak, nonatomic) IBOutlet UILabel *nameReport;

@end
