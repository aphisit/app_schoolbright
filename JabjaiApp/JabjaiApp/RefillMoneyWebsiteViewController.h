//
//  RefillMoneyWebsiteViewController.h
//  JabjaiApp
//
//  Created by mac on 11/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"
#import "RFSaveBillTopUpDialog.h"

@interface RefillMoneyWebsiteViewController : UIViewController <UIWebViewDelegate, SlideMenuControllerDelegate, RFSaveBillTopUpDialogDelegate>
@property (weak, nonatomic) IBOutlet UILabel *headLable;
@property (strong,nonatomic) NSString *numberMoney;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *headerView;
- (IBAction)moveBack:(id)sender;

@end
