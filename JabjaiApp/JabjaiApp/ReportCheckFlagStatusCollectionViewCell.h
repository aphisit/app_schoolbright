//
//  ReportCheckFlagStatusCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 30/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportCheckFlagStatusCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *clockImage;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundColor;











@end

NS_ASSUME_NONNULL_END
