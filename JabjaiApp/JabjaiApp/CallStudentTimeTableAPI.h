//
//  CallStudentTimeTableAPI.h
//  JabjaiApp
//
//  Created by toffee on 16/5/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSubjectModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallStudentTimeTableAPI;

@protocol CallStudentTimeTableAPIDelegate <NSObject>

@optional
- (void)callStudentTimeTableAPI:(CallStudentTimeTableAPI *)classObj data:(NSMutableArray<SSubjectModel *> *)data success:(BOOL)success;
@end
@interface CallStudentTimeTableAPI : NSObject
@property (nonatomic, weak) id<CallStudentTimeTableAPIDelegate> delegate;
- (void)call:(long long)userId date:(NSDate*)date schoolid:(long long)schoolid;
@end

NS_ASSUME_NONNULL_END
