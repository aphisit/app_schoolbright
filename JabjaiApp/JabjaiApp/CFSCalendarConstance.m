//
//  CFSCalendarConstane.m
//  CFSCalendar
//
//  Created by dingwenchao on 8/28/15.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//
//  https://github.com/WenchaoD
//

#import "CFSCalendarConstance.h"

CGFloat const CFSCalendarStandardHeaderHeight = 40;
CGFloat const CFSCalendarStandardWeekdayHeight = 25;
CGFloat const CFSCalendarStandardMonthlyPageHeight = 300.0;
CGFloat const CFSCalendarStandardWeeklyPageHeight = 108+1/3.0;
CGFloat const CFSCalendarStandardCellDiameter = 100/3.0;
CGFloat const CFSCalendarAutomaticDimension = -1;
CGFloat const CFSCalendarDefaultBounceAnimationDuration = 0.15;
CGFloat const CFSCalendarStandardRowHeight = 38+1.0/3;
CGFloat const CFSCalendarStandardTitleTextSize = 13.5;
CGFloat const CFSCalendarStandardSubtitleTextSize = 10;
CGFloat const CFSCalendarStandardWeekdayTextSize = 14;
CGFloat const CFSCalendarStandardHeaderTextSize = 16.5;
CGFloat const CFSCalendarMaximumEventDotDiameter = 4.8;
CGFloat const CFSCalendarStandardScopeHandleHeight = 26;

NSInteger const CFSCalendarDefaultHourComponent = 0;
