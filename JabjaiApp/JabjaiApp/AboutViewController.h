//
//  AboutViewController.h
//  JabjaiApp
//
//  Created by mac on 12/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"

@interface AboutViewController : UIViewController <SlideMenuControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerCompanyLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerWebsiteLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerVersionLabel;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextView *websiteTextView;

@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UITextView *phoneTextView;

@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
- (IBAction)moveBack:(id)sender;

@end
