//
//  CallGetSubjectAPI.m
//  JabjaiApp
//
//  Created by mac on 7/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetSubjectAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetSubjectAPI () {
    NSInteger connectCounter;
    NSMutableArray<SSubjectModel *> *subjectArray;
}
@end
@implementation CallGetSubjectAPI

- (void)call:(long long)schoolId classroomId:(long long)classroomId date:(NSString *)date{
    connectCounter = 0;
    [self getSubjectWithSchoolId:schoolId classroomId:classroomId date:date];
}

#pragma mark - Get API Data
- (void)getSubjectWithSchoolId:(long long)schoolId classroomId:(long long)classroomId date:(NSString *)date{
    NSString *URLString = [APIURL getSubjectWithSchoolId:schoolId classroomId:classroomId date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSubjectWithSchoolId:schoolId classroomId:classroomId date:date];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubjectWithSchoolId:schoolId classroomId:classroomId date:date];
                }
                else {
                    
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if([returnedData isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDict = returnedData;
                
                if([dataDict objectForKey:@"Status"] != nil && [[dataDict objectForKey:@"Status"] integerValue] == -1) {
                    
//                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(notArrangeTimeTable:)]) {
//                        [self.delegate notArrangeTimeTable:self];
//                    }
                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetSubjectAPI:data:success:)]) {
                        [self.delegate callGetSubjectAPI:self data:nil success:NO];
                    }
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubjectWithSchoolId:schoolId classroomId:classroomId date:date];
                }
                else {
                    
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(subjectArray != nil) {
                    [subjectArray removeAllObjects];
                    subjectArray = nil;
                }
                
                subjectArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long subjectID = [[dataDict objectForKey:@"scheduleId"] longLongValue];
                    
                    NSMutableString *subjectName, *startTime, *endTime, *planeID;
                    
                    if(![[dataDict objectForKey:@"scheduleName"] isKindOfClass:[NSNull class]]) {
                        subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"scheduleName"]];
                    }
                    else {
                        subjectName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"timestart"] isKindOfClass:[NSNull class]]) {
                        startTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"timestart"]];
                    }
                    else {
                        startTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"timeend"] isKindOfClass:[NSNull class]]) {
                        endTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"timeend"]];
                    }
                    else {
                        endTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"plane_id"] isKindOfClass:[NSNull class]]) {
                        planeID = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"plane_id"]];
                    }
                    else {
                        planeID = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) planeID);
                    
                    SSubjectModel *model = [[SSubjectModel alloc] init];
                    model.subjectID = subjectID;
                    model.subjectName = subjectName;
                    model.startTime = startTime;
                    model.endTime = endTime;
                    model.planeID = planeID;
                    
                    [subjectArray addObject:model];
                    
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetSubjectAPI:data:success:)]) {
                    [self.delegate callGetSubjectAPI:self data:subjectArray success:YES];
                }
                
                return ;
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetSubjectAPI:data:success:)]) {
                [self.delegate callGetSubjectAPI:self data:nil success:NO];
            }
        }
        
    }];

}

@end
