//
//  BSAlertStatusDialog.m
//  JabjaiApp
//
//  Created by toffee on 1/31/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "BSAlertStatusDialog.h"
#import "UserData.h"
#import "Utils.h"

@interface BSAlertStatusDialog ()
@property (nonatomic, assign) BOOL isShowing;
@end

@implementation BSAlertStatusDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dialogView.layer.masksToBounds = YES;
    self.cancelBtn.layer.masksToBounds = YES;
    self.isShowing = NO;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (screenSize.height == 812.0f) {
            self.dialogTopConstraint.constant = 298.5;
            self.dialogBottomConstraint.constant = 298.5;
        }
        
        else if (screenSize.height == 736.0f){
            self.dialogTopConstraint.constant = 260.5;
            self.dialogBottomConstraint.constant = 260.5;
        }
        
        else if (screenSize.height == 667.0f){
            self.dialogTopConstraint.constant = 226.0;
            self.dialogBottomConstraint.constant = 226.0;
        }
        else if (screenSize.height == 568.0f){
            self.dialogTopConstraint.constant = 176.5;
            self.dialogBottomConstraint.constant = 176.5;
        }
        
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientCancel;
    //set color header
    [self.cancelBtn layoutIfNeeded];
    gradientCancel = [Utils getGradientColorStatus:@"green"];
    gradientCancel.frame = self.cancelBtn.bounds;
    [self.cancelBtn.layer insertSublayer:gradientCancel atIndex:0];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

-(void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message mode:(NSInteger)mode {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    
    // [self.titleLabel setText:title];
    [self.messageLabel setText:message];
    if(mode == 1){
        [self.iconStatus setImage:[UIImage imageNamed:@"emoji_happy"]];
        [self.cancelBtn setBackgroundColor:[UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0]];
        //emoji_cry
    }else{
        [self.iconStatus setImage:[UIImage imageNamed:@"emoji_cry"]];
        [self.cancelBtn setBackgroundColor:[UIColor colorWithRed:220/255.0 green:43/255.0 blue:69/255.0 alpha:1.0]];
    }
    NSBundle *myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    [_cancelBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_BTN_FLAG_CLOSE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogClose)]) {
        [self.delegate onAlertDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

@end
