//
//  SNReciverViewControllerXIB.m
//  JabjaiApp
//
//  Created by Mac on 17/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "SNReciverViewControllerXIB.h"
#import "ReportSendNewsPersonRecieveTableViewCell.h"
#import "Utils.h"
#import "UserData.h"
#import "Constant.h"
#import "APIURL.h"
#import "ReportSendNewsSortStatus.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppDelegate.h"
@interface SNReciverViewControllerXIB (){
    NSMutableDictionary<NSNumber *, NSArray<ReportSendNewsSortStatus *> *> *reportSections;
    NSMutableArray<ReportSendNewsSortStatus *> *messages;
    UIColor *grayColor;
    NSManagedObject *personalImage;
    NSInteger connectCounter;
    NSString *changePageStatus;
}
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@end

@implementation SNReciverViewControllerXIB

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    reportSections = [[NSMutableDictionary alloc] init];
    messages = [[NSMutableArray alloc] init];
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportSendNewsPersonRecieveTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    
    // set up formatter
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    self.sortData = @"";
    [self getRecieverListWithnewsID:self.newsID page:1 readStatus:self.sortData];
}

- (void) viewWillLayoutSubviews{
    [self getRecieverListWithnewsID:self.newsID page:1 readStatus:@""];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return reportSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger page = section +1;
    
    return [[reportSections objectForKey:@(page)] count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ReportSendNewsPersonRecieveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    NSInteger page = indexPath.section + 1;
    ReportSendNewsSortStatus *model = [[reportSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
//    cell.personImage.image = [UIImage imageNamed:@"ic_user_info"];
    
    cell.personImage.layer.cornerRadius = cell.personImage.frame.size.height /2;
    cell.personImage.layer.masksToBounds = YES;
    
    if ([model.image isEqual:@""] || model.image == nil) {
        cell.personImage.image = [UIImage imageNamed:@"ic_user_info"];
    }
    else{
        [cell.personImage sd_setImageWithURL:[NSURL URLWithString:model.image]];
    }
    
    cell.personName.text = model.name;
    
    if (model.status == YES) {
        cell.statusRead.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_READ",nil,[Utils getLanguage],nil);
    }
    else if (model.status == NO){
        cell.statusRead.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_UNREAD",nil,[Utils getLanguage],nil);
    }
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    return cell;
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
    
}


- (void)getRecieverListWithnewsID:(long long)newsId page:(NSInteger)page readStatus:(NSString *)readStatus{
    
    [self showIndicator];
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getReportSendNewsRecieverWithSchoolID:schoolID newsID:newsId page:page status:readStatus];
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getRecieverListWithnewsID:newsId page:page readStatus:readStatus];
            }
            else {
                self->connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getRecieverListWithnewsID:newsId page:page readStatus:readStatus];
                }
                else {
                    self->connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getRecieverListWithnewsID:newsId page:page readStatus:readStatus];
                }
                else {
                    self->connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                self->connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                if (page == 1) {
                    self->reportSections = [[NSMutableDictionary alloc] init];
                }
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *recievedname;
                    
                    if(![[dataDict objectForKey:@"user_name"] isKindOfClass:[NSNull class]]) {
                        recievedname = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"user_name"]];
                    }
                    else {
                        recievedname = [[NSMutableString alloc] initWithString:@""];
                    }

                    NSMutableString *recievedimage;
                    
                    if(![[dataDict objectForKey:@"picture"] isKindOfClass:[NSNull class]]) {
                        recievedimage = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"picture"]];
                    }
                    else {
                        recievedimage = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    
                    BOOL status = [[dataDict objectForKey:@"read_status"] boolValue];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) recievedname);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) recievedimage);
                    
                    ReportSendNewsSortStatus *model = [[ReportSendNewsSortStatus alloc] init];
                    
                    model.name = recievedname;
                    model.status = status;
                    model.image = recievedimage;
                    
                    [newDataArr addObject:model];
                    
                }
                
                [self->messages removeAllObjects];
                self->messages = [[NSMutableArray alloc] initWithArray:newDataArr];
                [self->reportSections setObject:newDataArr forKey:@(page)];
                
                [self.tableView reloadData];
            }
            
        }
    }];
    
    
}

- (void)fetchMoreData{
    
    if (messages.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *lastRowIndexPath = [visiblePaths lastObject];
        
        NSInteger numberOfSections = [self.tableView numberOfSections];
        NSInteger lastRowSection = [lastRowIndexPath section];
        NSInteger lastRowRow = [lastRowIndexPath row];
        NSInteger numberOfRowInsection = [self.tableView numberOfRowsInSection:lastRowSection];
        
        if (lastRowSection == numberOfSections -1 && lastRowRow == numberOfRowInsection - 1) {
            if (messages.count % 20 == 0) {
                NSInteger nextpage = lastRowSection + 2;
                
                changePageStatus = self.sortChangeData;
                
                if ([changePageStatus isEqual:@"read"]) {
                    [self getRecieverListWithnewsID:self.newsID page:nextpage readStatus:changePageStatus];
                }
                else if([changePageStatus isEqual:@"unread"]) {
                    [self getRecieverListWithnewsID:self.newsID page:nextpage readStatus:changePageStatus];
                }
                else {
                    [self getRecieverListWithnewsID:self.newsID page:nextpage readStatus:changePageStatus];
                }

            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self fetchMoreData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self fetchMoreData];
}

- (void)showIndicator{
    if (![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator{
    [self.indicator stopAnimating];
    
}

@end
