//
//  ReportOrderHomeWorkRecieverViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportOrderHomeWorkRecieverViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (assign, nonatomic) long long homeworkID;

@property (nonatomic, strong) NSString *sortData;
@property (nonatomic, strong) NSString *sortChangeData;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (void)getRecieverListWithhomeworkID:(long long)homeworkId page:(NSInteger)page readStatus:(NSString *)readStatus;

@end
