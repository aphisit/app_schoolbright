//
//  CallRPUpdateNewPinPOSTAPI.m
//  JabjaiApp
//
//  Created by toffee on 6/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.


#import "CallRPUpdateNewPinPOSTAPI.h"
//#import "Constant.h"
//#import "Utils.h"
#import "APIURL.h"
#import <AFNetworking/AFNetworking.h>

@implementation CallRPUpdateNewPinPOSTAPI
- (void)call:(NSString*)jsonString{
    
    [self updateNewPint:(NSString*)jsonString];
}
- (void)updateNewPint:(NSString*)jsonString {
    
    NSString *URLString = [APIURL getUpdatePinCode];
    
    NSError *error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&error];
    NSLog(@"Json = %@", json);
    NSLog(@"ObjectData = %@", objectData);
    NSLog(@"jsonString = %@", jsonString);
    
    if(error != nil) {
        NSLog(@"%@", @"Parse JSON data failed");
    }
    else {
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        
        [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
           
             NSString *response = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"resMessage"]];
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callRPUpdateNewPinPOSTAPI:response:success:)]) {
               
                [self.delegate callRPUpdateNewPinPOSTAPI:self response:response success:YES];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callRPUpdateNewPinPOSTAPI:response:success:)]) {
                [self.delegate callRPUpdateNewPinPOSTAPI:self response:nil success:NO];
            }
            
        }];
        
    }
}
@end
