//
//  CallFileMessageAPI.m
//  JabjaiApp
//
//  Created by toffee on 2/8/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallFileMessageAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"
#import "UserData.h"

@interface CallFileMessageAPI () {
    NSInteger connectCounter;
    NSMutableArray *fileArray;
    NSArray *returnedArray;
}
@end

@implementation CallFileMessageAPI
- (void) call:(long long)userID messageID:(long long)messageID schoolid:(long long)schoolid{
    [self getFileFromMessage:userID messageID:messageID schoolid:schoolid];
}

- (void) getFileFromMessage:(long long)userID messageID:(long long)messageID schoolid:(long long)schoolid{
    
    //add image to array
    //long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getImageNews:userID idMessage:messageID schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
  
//    fileArray = [[NSMutableArray alloc]init];
//
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getFileFromMessage:userID messageID:messageID schoolid:schoolid];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }

        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getFileFromMessage:userID messageID:messageID schoolid:schoolid];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getFileFromMessage:userID messageID:messageID schoolid:schoolid];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                returnedArray = returnedData;
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callFileMessageAPI:data:success:)]) {
                    [self.delegate callFileMessageAPI:self data:returnedArray success:YES];
                }
            }
        }
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callFileMessageAPI:data:success:)]) {
                [self.delegate callFileMessageAPI:self data:returnedArray success:NO];
            }
        }

    }];
}

@end
