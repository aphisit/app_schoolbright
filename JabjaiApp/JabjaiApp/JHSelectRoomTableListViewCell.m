//
//  JHSelectRoomTableListViewCell.m
//  JabjaiApp
//
//  Created by toffee on 1/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "JHSelectRoomTableListViewCell.h"

@implementation JHSelectRoomTableListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    
}
- (void)setRadioButtonSelectedWithType:(int) type {
    
    if(type == RADIO_TYPE_RED) {
        [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_red"] ];
        //[self.radioButton setImage:[UIImage imageNamed:@"anyImageName"]];
    }
    else {
        [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_green"] ];
    }
}
- (void)setRadioButtonClearSelected {
    [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_clear"] ];
}


//- (IBAction)actionPressRadioButton:(id)sender {
//    
//    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onPressRadioButton:atIndex:)]) {
//        [self.delegate onPressRadioButton:self atIndex:_index];
//    }
//    
//}

@end
