//
//  UnAuthorizeMenuDialog.h
//  JabjaiApp
//
//  Created by toffee on 22/5/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol UnAuthorizeMenuDialogDelegate <NSObject>

@optional
- (void)onAlertDialogClose;

@end
@interface UnAuthorizeMenuDialog : UIViewController
@property (retain, nonatomic) id<UnAuthorizeMenuDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dialogTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dialogBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContstraint;

- (IBAction)closeDialog:(id)sender;

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end

NS_ASSUME_NONNULL_END
