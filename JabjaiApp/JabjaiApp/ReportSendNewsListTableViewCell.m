//
//  ReportSendNewsListTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 4/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportSendNewsListTableViewCell.h"

@implementation ReportSendNewsListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imageIconSendNews.layer.cornerRadius = self.imageIconSendNews.frame.size.width / 2;
    self.imageIconSendNews.clipsToBounds = YES;
    self.imageIconSendNews.layer.shouldRasterize = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
