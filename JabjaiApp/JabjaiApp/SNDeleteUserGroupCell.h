//
//  SNDeleteUserGroupCell.h
//  JabjaiApp
//
//  Created by toffee on 2/15/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNDeleteUserGroupCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageMembers;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteMember;

@property (weak, nonatomic) IBOutlet UILabel *nameMembers;

@end
