//
//  JHSelectStudenClassViewController.m
//  JabjaiApp
//
//  Created by toffee on 11/21/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "JHSelectStudenClassViewController.h"
#import "BSSelectStudentsViewController.h"
#import "SNClassLevelGroupViewController.h"
#import "BSSelectBehaviorScoreViewController.h"
#import "JHTimeOutAndDetailViewController.h"
#import "JHClassLevelViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "Constant.h"
#import "Utils.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface JHSelectStudenClassViewController (){
    UIColor *greenBGColor;
    UIColor *redBGColor;
    NSInteger selectedStudentIndex;
    BOOL optionStatusSelectAll;
    NSMutableArray *student;
    NSMutableArray *isSelected;
    NSMutableArray *userAcceptArray;
    NSMutableArray *selectArrayClassroom;
    BOOL searchActive;
    UIColor *unauthorizedBG;
    UIColor *searchBarBG;
    NSArray<TAStudentStatusModel *> *studentStatusHistoryForCopyArray;
    // The dialog
    AlertDialog *alertDialog;
    NSArray<JHSelectedStudentModel *> *searchStudentStatusArray;
     UIGestureRecognizer *searchBarCancelGesture;
    NSBundle *myLangBundle;
}
//@property (strong, nonatomic) CallGetStudentInClassroomAPI *callGetStudentInClassroomAPI;
@property (strong, nonatomic) CallJHGetStudentInClassroomAPI *callJHGetStudentInClassrommAPI;
@property (strong, nonatomic) CallGetPreviousAttendClassStatusAPI *callGetPreviousAttendClassStatusAPI;
@end
static NSString *cellIdentifier = @"Cell";
@implementation JHSelectStudenClassViewController
@synthesize studentRadioArray = _studentRadioArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    _studentArray = [[NSMutableArray alloc] init];
    selectArrayClassroom = [[NSMutableArray alloc] init];
    student = [[NSMutableArray alloc] init];
    userAcceptArray = [[NSMutableArray alloc] init];
    NSLog(@"classroomID = %@",self.subjectId);
    self.titleLabel.text = self.subjectName;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([JHStudentNameRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    selectedStudentIndex = -1;
    searchActive = NO;
    unauthorizedBG = [UIColor colorWithRed:213/255.0 green:213/255.0 blue:213/255.0 alpha:1.0];
    searchBarBG = [UIColor colorWithRed:224/255.0 green:93/255.0 blue:37/255.0 alpha:1];
    self.searchBar.delegate = self;
    self.searchBar.translucent = NO;
    self.searchBar.opaque = NO;
    self.searchBar.barTintColor = searchBarBG;
    self.searchBar.backgroundImage = [[UIImage alloc] init];

    UIView *view = [self.searchBar.subviews objectAtIndex:0];
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *cancelButton = (UIButton *)subView;
            [cancelButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        }
    }
    [self setLanguage];
    [self getStudentInClassData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.nextButton layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.nextButton.bounds;
    [self.nextButton.layer insertSublayer:gradientNext atIndex:0];
}

-(void)setLanguage{
    [self.nextButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_JOBHOME_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

- (void)getStudentInClassData {
    if(self.callJHGetStudentInClassrommAPI != nil) {
        self.callJHGetStudentInClassrommAPI = nil;
    }
    selectArrayClassroom = _classroomId;
    NSString *classroomIdString = [NSString stringWithFormat:@"%@", [_classroomId objectAtIndex:0] ];
    NSLog(@"classRoomID = %lld",[classroomIdString longLongValue]  );
    self.callJHGetStudentInClassrommAPI = [[CallJHGetStudentInClassroomAPI alloc] init];
    self.callJHGetStudentInClassrommAPI.delegate = self;
    [self.callJHGetStudentInClassrommAPI call:[UserData getSchoolId] classroomId:[classroomIdString longLongValue]];
}

- (void)getPreviousAttendClassStatus {
    if(self.callGetPreviousAttendClassStatusAPI != nil) {
        self.callGetPreviousAttendClassStatusAPI = nil;
    }
    NSLog(@"getSchoolId = %d",[UserData getSchoolId]);
    NSLog(@"teacherId = %d", [UserData getUserID]);
    self.callGetPreviousAttendClassStatusAPI = [[CallGetPreviousAttendClassStatusAPI alloc] init];
    self.callGetPreviousAttendClassStatusAPI.delegate = self;
    [self.callGetPreviousAttendClassStatusAPI call:[UserData getSchoolId] classroomId:self.classroomId subjectId:self.subjectId teacherId:[UserData getUserID]];
}

#pragma mark - CallGetStudentInClassAPIDelegate

- (void)callJHGetStudentInClassrommAPI:(CallJHGetStudentInClassroomAPI *)classObj data:(NSArray<JHSelectedStudentModel *> *)data success:(BOOL)success {
    if (data != nil || success == nil) {
        _studentArray = data;
        [self.tableView reloadData];
    }
}
- (void)callGetPreviousAttendClassStatusAPI:(CallGetPreviousAttendClassStatusAPI *)classObj data:(NSArray<TAStudentStatusModel *> *)data success:(BOOL)success {
    
    if(success) {
        studentStatusHistoryForCopyArray = data;
    }
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(searchActive) {
        if(searchStudentStatusArray != nil) {
            return searchStudentStatusArray.count;
        }
        else {
            return 0;
        }
    }
    else if(_studentArray != nil) {
        
            return _studentArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    JHStudentNameRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    JHSelectedStudentModel *model;
    
    if(searchActive) {
        model = [searchStudentStatusArray objectAtIndex:indexPath.row];
    }
    else {
        model = [_studentArray objectAtIndex:indexPath.row];
    }
    
    cell.index = indexPath.row;
    NSArray *array = [[NSString stringWithFormat:@"%@", [model getStudentName]] componentsSeparatedByString:@" "];
    NSMutableString *lastName = [[NSMutableString alloc] init];
    if (array.count>1) {
        for (int i = 0; i < array.count; i++) {
            if (i>0) {
                [lastName appendFormat:@"%@ ",array[i]];
            }
        }
        cell.titleLabel.text = array[0];
        cell.titleLastname.text = lastName;
    }else{
        cell.titleLabel.text = array[0];
    }
    
    cell.runNumber.text = [NSString stringWithFormat:@"%d", indexPath.row + 1];
    [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@", [model getStudentPic]]]] placeholderImage:nil options:SDWebImageRefreshCached
        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        if (image == nil || error) {
                                          cell.userImageView.image = [UIImage imageNamed:@"ic_user_info"];
                                        }
        }];
    cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height /2;
    cell.userImageView.layer.masksToBounds = YES;
    cell.userImageView.layer.borderWidth = 0;
    cell.delegate = self;
    NSLog(@"index = %d",indexPath.row);

    if([model isSelected]) {
        
        if (![student containsObject:@([model getStudentId])]) {
            [student addObject: @([model getStudentId])];
            [userAcceptArray addObject:[model getStudentName]];
        }
        [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
    }
    else {
        [student removeObject: @([model getStudentId])];
        [userAcceptArray removeObject: [model getStudentName]];
        [cell setRadioButtonClearSelected];
    }
    [cell.shadowViewCell.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.shadowViewCell.layer setShadowOpacity:0.3];
    [cell.shadowViewCell.layer setShadowRadius:3.0];
    [cell.shadowViewCell.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    BOOL  isSelected;
    if(searchActive) {
        isSelected = [[searchStudentStatusArray objectAtIndex:indexPath.row] isSelected];
        [[searchStudentStatusArray objectAtIndex:indexPath.row] setSelected:!isSelected]; //
    }
    else {
        isSelected = [[self.studentArray objectAtIndex:indexPath.row] isSelected];
        [[self.studentArray objectAtIndex:indexPath.row] setSelected:!isSelected];
    }
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)onPressRadioButton:(JHStudentNameRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index{
    BOOL  isSelected;
    if(searchActive) {
       isSelected = [[searchStudentStatusArray objectAtIndex:index] isSelected];
        [[searchStudentStatusArray objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
        
    }
    else {
        isSelected = [[self.studentArray objectAtIndex:index] isSelected];
        [[self.studentArray objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    NSLog(@"indexpath = %@",indexPath);
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}
#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBarCancelGesture = [UITapGestureRecognizer new];
    [searchBarCancelGesture addTarget:self action:@selector(backgroundTouched:)];
    [self.view addGestureRecognizer:searchBarCancelGesture];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if(searchBarCancelGesture != nil) {
        [self.view removeGestureRecognizer:searchBarCancelGesture];
        searchBarCancelGesture = nil;
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // called when text changes (including clear)
    searchActive = YES;
    if(searchText.length != 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"studentName contains[c] %@", searchText];
        searchStudentStatusArray = [_studentArray filteredArrayUsingPredicate:predicate];
    }
    else {
        searchStudentStatusArray = _studentArray;
    }
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}
#pragma mark - Utils

- (void)backgroundTouched:(id)sender {
    [self.view endEditing:YES];
    [self.searchBar resignFirstResponder];
    
    if(searchActive && self.searchBar.text.length == 0) {
        searchActive = NO;
        [self.tableView reloadData];
    }
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (BOOL)validateData {
    if (student.count == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_SELECT_INPUT_USER",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
     return YES;
}
- (IBAction)actionNext:(id)sender {
    if ([self validateData]) {
        JHTimeOutAndDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHTimeOutAndDetailStoryboard"];
        NSLog(@"studen = %@",student);
        viewController.levelId = selectArrayClassroom;
        viewController.studentId = student;
        viewController.studentName = userAcceptArray;
        viewController.sendType = self.sendType;
        viewController.subjectId = self.subjectId;
        viewController.subjectName = self.subjectName;
        viewController.classLevelArray = _classLevelArray;
        viewController.classRoomArray = _classRoomArray;
        viewController.sujectNameArrray = _sujectNameArrray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.selectedSubjectIndex = _selectedSubjectIndex;
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
}
- (IBAction)moveBack:(id)sender {
    JHClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHClassLevelStoryboard"];
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classRoomArray;
    viewController.subjectArray = _sujectNameArrray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.selectedSubjectIndex = _selectedSubjectIndex;
    viewController.mode = 0;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

@end
