//
//  ChangLanguageDialog.h
//  JabjaiApp
//
//  Created by toffee on 7/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallSTSendTokenAndLanguageAPI.h"
@protocol ChangLanguageDialogDelegate <NSObject>
@optional
- (void)onAlertDialogClose;

@end
extern NSString *tokenStr;

@interface ChangLanguageDialog : UIViewController <CallSTSendTokenAndLanguageAPIDelegate>
@property (retain, nonatomic) id<ChangLanguageDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIButton *thaiLanguagebtn;

- (IBAction)languageThaiAction:(id)sender;
- (IBAction)languageEnglishAction:(id)sender;


- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end
