//
//  JHConfirmJobHomeViewController.h
//  JabjaiApp
//
//  Created by toffee on 12/1/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "JHSubjectModel.h"
#import "SlideMenuController.h"

@interface JHConfirmJobHomeViewController : UIViewController <SlideMenuControllerDelegate>
@property (nonatomic) long long schoolId;
@property (nonatomic) NSMutableArray* levelId;
@property (nonatomic) NSInteger sendtype;
@property (nonatomic) NSDate* dayend;
@property (nonatomic) NSDate* daynotification;
@property (nonatomic) NSString* timenotification;
@property (nonatomic) NSDate* daystart;
@property (nonatomic) NSString* homeworkdetail;
@property (nonatomic) NSString* teacherid;
@property (nonatomic) NSString* planeId;

@property (nonatomic) NSString* subJectName;
@property (nonatomic) NSMutableArray* roomName;

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classRoomArray;
@property (strong, nonatomic) NSArray<JHSubjectModel *> *sujectNameArrray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;

@property (nonatomic) NSMutableArray* arrayuserId;
@property (nonatomic) NSMutableArray* arrayuserName;

@property (nonatomic) NSMutableArray* fileArray;
@property (nonatomic) NSMutableArray* imageHomework;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerSummarizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSubjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDiliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRecipientLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerStartDateLabel;
//@property (weak, nonatomic) IBOutlet UILabel *headerEndDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAlertDateLabel;
//@property (weak, nonatomic) IBOutlet UILabel *headerAlertTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDescription;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachImageLabel;

@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;

@property (weak, nonatomic) IBOutlet UITextView *detail;
//@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet UILabel *subjectName;
@property (weak, nonatomic) IBOutlet UILabel *student;
@property (weak, nonatomic) IBOutlet UILabel *daySendHomeWork;
@property (weak, nonatomic) IBOutlet UILabel *dayNotification;
@property (weak, nonatomic) IBOutlet UILabel *sendType;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionview;
@property (weak, nonatomic) IBOutlet UICollectionView *fileCollectionView;

- (IBAction)confirmAction:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
