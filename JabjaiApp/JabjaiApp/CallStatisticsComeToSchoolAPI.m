//
//  CallStatisticsComeToSchoolAPI.m
//  JabjaiApp
//
//  Created by Mac on 17/7/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallStatisticsComeToSchoolAPI.h"
#import "APIURL.h"
#import "Utils.h"
#import "UserData.h"
@implementation CallStatisticsComeToSchoolAPI{
    NSInteger connectCounter;
    NSMutableArray<AttendToSchoolModel*>  *attendToSchoolArray;
}
-(void)call:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status{
    connectCounter = 0;
    [self getStatisticsComeToSchool:startDate endDate:endDate status:status];
}

-(void)getStatisticsComeToSchool:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status{
    long long schoolid = [UserData getSchoolId];
        NSString *URLString = [APIURL getAttendSchoolDataURLWithStartDate:startDate endDate:endDate status:status schoolid:schoolid];
        NSURL *URL = [NSURL URLWithString:URLString];
        
        [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            BOOL isFail = NO;
            if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
                
                if(error != nil) {
                    NSLog(@"An error occured : %@" , [error localizedDescription]);
                }
                else if(data == nil) {
                    NSLog(@"Data is null");
                }
                else {
                    NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                    NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
                }
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStatisticsComeToSchool:startDate endDate:endDate status:status];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if(error != nil) {
                    NSLog(@"An error occured : %@", [error localizedDescription]);
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self getStatisticsComeToSchool:startDate endDate:endDate status:status];
                    }
                    else {
                        isFail = YES;
                        connectCounter = 0;
                    }
                    
                }
                else if(![returnedData isKindOfClass:[NSArray class]]) {
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self getStatisticsComeToSchool:startDate endDate:endDate status:status];
                    }
                    else {
                        isFail = YES;
                        connectCounter = 0;
                    }
                    
                }
                else {
                    NSArray *returnedArray = returnedData;
                    NSMutableArray *yData;
                    connectCounter = 0;
                    
                    attendToSchoolArray = [[NSMutableArray alloc] init];
                    
                    for(int i=0; i<returnedArray.count; i++) {
                        NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                        
                        NSString *statusStr = [dataDict objectForKey:@"StatusIN"];
                        NSMutableString *scanDateTimeStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dScan"]];
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) scanDateTimeStr);
                        
                        NSDateFormatter *formatter = [Utils getDateFormatter];
                        [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                        
                        NSDate *scanDate = [formatter dateFromString:scanDateTimeStr];
                        
                        NSInteger status;
                        
                        if([statusStr isKindOfClass:[NSNull class]] || [statusStr length] == 0) {
                            status = 3; //Absence
                        }
                        else {
                            status = [statusStr integerValue];
                        }
                        
                        AttendToSchoolModel *model = [[AttendToSchoolModel alloc] init];
                        [model setStatus:[[NSNumber alloc] initWithInteger:status]];
                        [model setScanDate:scanDate];
                        [self->attendToSchoolArray addObject:model];
                    }
//
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callStatisticsComeToSchoolAPI:data:success:)]) {
                        [self.delegate callStatisticsComeToSchoolAPI:self data:attendToSchoolArray success:YES];
                    }
                
//                    // prepare data for pie chart
//                    [yData removeAllObjects];
//                    NSInteger totalStatus,statusOnTime,statusLate,statusAbsence,statusSick,statusPersonal,statusEvent,statusNoCheckPercent;
//                    
//                    statusOnTime = 0;
//                    statusLate = 0;
//                    statusAbsence = 0;
//                    statusSick = 0;
//                    statusPersonal = 0;
//                    statusEvent = 0;
//                    statusNoCheckPercent = 0;
//                    
//                    
//                    for(AttendToSchoolModel *model in attendToSchoolArray) {
//                        
//                        if([model.status integerValue] == 0 || [model.status integerValue] == 7) {
//                            statusOnTime++;
//                        }
//                        else if([model.status integerValue] == 1) {
//                            statusLate++;
//                        }
//                        else if([model.status integerValue] == 3) {
//                            statusAbsence++;
//                        }
//                        else if([model.status integerValue] == 4 || [model.status integerValue] == 10) {
//                            statusPersonal++;
//                        }
//                        else if([model.status integerValue] == 5 || [model.status integerValue] == 11) {
//                            statusSick++;
//                        }
//                        else if([model.status integerValue] == 6 || [model.status integerValue] == 12) {
//                            statusEvent++;
//                        }
//                        else if([model.status integerValue] == 99 ) {
//                            statusNoCheckPercent++;
//                        }
//                    }
//                    
//                    totalStatus = statusOnTime + statusLate + statusAbsence + statusPersonal + statusSick + statusEvent + statusNoCheckPercent;
//                    
//                    [yData addObject:[[NSNumber alloc] initWithInteger:statusOnTime]];
//                    [yData addObject:[[NSNumber alloc] initWithInteger:statusLate]];
//                    [yData addObject:[[NSNumber alloc] initWithInteger:statusAbsence]];
//                    [yData addObject:[[NSNumber alloc] initWithInteger:statusSick]];
//                    [yData addObject:[[NSNumber alloc] initWithInteger:statusPersonal]];
//                    [yData addObject:[[NSNumber alloc] initWithInteger:statusEvent]];
//                    [yData addObject:[[NSNumber alloc] initWithInteger:statusNoCheckPercent]];
//                    
//                    onTimePercent = (float)statusOnTime/totalStatus * 100;
//                    latePercent = (float)statusLate/totalStatus * 100;
//                    absencePercent = (float)statusAbsence/totalStatus * 100;
//                    personalPercent = (float)statusPersonal/totalStatus * 100;
//                    sickPercent = (float)statusSick/totalStatus * 100;
//                    eventPercent = (float)statusEvent/totalStatus * 100;
//                    noCheckPercent = (float)statusNoCheckPercent/totalStatus * 100;
//   
//                    [self setupGraphData];
//                    [self.tableView reloadData];
                }
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callStatisticsComeToSchoolAPI:data:success:)]) {
                    [self.delegate callStatisticsComeToSchoolAPI:self data:attendToSchoolArray success:YES];
                }
            }
        }];
}
@end
