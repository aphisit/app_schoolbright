//
//  TAReportARSPContainerController.h
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import ARSlidingPanel;

#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "TASubjectModel.h"

@interface TAReportARSPContainerController : ARSPContainerController

//Global variables
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<TASubjectModel *> *subjectArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
@property (nonatomic) long long selectedClassLevelId;
@property (nonatomic) long long selectedClassroomId;
@property (nonatomic) NSString *selectedSubjectId;

@end
