//
//  HWTDetailOrderHomeworkViewControllerXIB.m
//  JabjaiApp
//
//  Created by Mac on 9/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "HWTDetailOrderHomeworkViewControllerXIB.h"

@interface HWTDetailOrderHomeworkViewControllerXIB (){
    CAPSPageMenu *pageMenu;
    UIColor *OrangeColor;
    UIColor *GrayColor;
    NSString *sortData;
//    long long homeworkID;
    
}
@property (nonatomic, assign) BOOL isShowing;
@end

@implementation HWTDetailOrderHomeworkViewControllerXIB

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
    
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_HOME",nil,[Utils getLanguage],nil);
}

- (void)showDetailHomeworkController:(UIView *)targetView homeworkID:(long long)homeworkID sortData:(NSString *)sortData{
    self.homeworkID = homeworkID;
    self.sortData = sortData;
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
        [targetView addSubview:self.view];
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height-20)];
        [targetView addSubview:self.view];
    }

    // Initialize dialog view hide it
    [self.contentView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentView setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
    
    OrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1.0];
    GrayColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
    
    [self setupPagemenu];
    self.sortButton.hidden = YES;
    self.sortButton.enabled = false;

    [self.sortButton addTarget:self action:@selector(showRightMenu:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)removeDetailNewsViewController {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDetailNewsViewController {
    
    [self removeDetailNewsViewController];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(closePage)]) {
        [self.delegate closePage];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

- (void)setupPagemenu{

    self.detailController = [[HWTDetailPersonViewControllerXIB alloc] initWithNibName:@"HWTDetailPersonViewControllerXIB" bundle:nil];
    self.detailController.title = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_DETAIL",nil,[Utils getLanguage],nil);
    self.detailController.homeworkID = self.homeworkID;
    
    self.recieverController  = [[HWTReciverViewControllerXIB alloc] initWithNibName:@"HWTReciverViewControllerXIB" bundle:nil];
    self.recieverController.title = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_RECIPIENT",nil,[Utils getLanguage],nil);
    self.recieverController .homeworkID = self.homeworkID;
    self.recieverController .sortData = self.sortData;
    NSArray *controllerArray = @[self.detailController, self.recieverController];
    NSDictionary *parameters;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(40),
                       CAPSPageMenuOptionMenuHeight: @(50),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:32.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    else{
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(20),
                       CAPSPageMenuOptionMenuHeight: @(40),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:22.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    [self.contentView layoutIfNeeded];
    pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    // For the first time set first tab background to orange and label color to white
    NSArray *menuItems = pageMenu.menuItems;
    MenuItemView *menuItemView = [menuItems objectAtIndex:pageMenu.currentPageIndex];
    menuItemView.backgroundColor = OrangeColor;
    menuItemView.titleLabel.textColor = [UIColor whiteColor];
    
    //Optional delegate
    pageMenu.delegate = self;
    
    [self.contentView addSubview:pageMenu.view];
    
}

- (void)showRightMenu:(UIButton *)sender{
    
    UIImage *allImage = [UIImage imageNamed:@"sort_with_all"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [allImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *allImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *readImage = [UIImage imageNamed:@"sort_with_read"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [readImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *readImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *unreadImage = [UIImage imageNamed:@"sort_with_unread"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [unreadImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *unreadImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    NSArray *menuItems =
    @[
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_ALL",nil,[Utils getLanguage],nil)
                     image:allImage2
                    target:self
                    action:@selector(sortAll:)],
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_READ",nil,[Utils getLanguage],nil)
                     image:readImage2
                    target:self
                    action:@selector(sortRead:)],
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_UNREND",nil,[Utils getLanguage],nil)
                     image:unreadImage2
                    target:self
                    action:@selector(sortUnRead:)],
      ];
    
    //    KxMenuItem *first = menuItems[0];
    //    first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
    //    first.alignment = NSTextAlignmentLeft;
    
    const CGFloat W = self.view.bounds.size.width;
    const CGFloat H = self.view.bounds.size.height;
    
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(W - 16, 18, 40, 40) menuItems:menuItems];
    
}

#pragma CAPSPageMenuDelegate

- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index{
    
    NSArray *menuItems = pageMenu.menuItems;
    
    int count = 0;
    
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = OrangeColor;
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        
        count++;
    }
    
    if (index == 0) {
        self.sortButton.hidden = YES;
        self.sortButton.enabled = false;
    }
    else{
        self.sortButton.hidden = NO;
        self.sortButton.enabled = true;
    }
    
}


- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
    NSArray *menuItems = pageMenu.menuItems;
    
    int count = 0;
    
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = OrangeColor;
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        
        count++;
    }
    
    if (index == 0) {
        self.sortButton.hidden = YES;
        self.sortButton.enabled = false;
    }
    else{
        self.sortButton.hidden = NO;
        self.sortButton.enabled = true;
        
    }
    
}

- (void) sortAll:(id)sender
{
    sortData = nil;
    self.recieverController.sortChangeData = sortData;
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        [self.recieverController getRecieverListWithhomeworkID:self.homeworkID page:1 readStatus:self->sortData];
    });
    NSLog(@"%@", sortData);
}

- (void) sortRead:(id)sender
{
    sortData = @"read";
    self.recieverController.sortChangeData = sortData;
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        [self.recieverController getRecieverListWithhomeworkID:self.homeworkID page:1 readStatus:self->sortData];
    });
}

- (void) sortUnRead:(id)sender
{
    sortData = @"unread";
    self.recieverController.sortChangeData = sortData;
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        [self.recieverController getRecieverListWithhomeworkID:self.homeworkID page:1 readStatus:self->sortData];
    });
    NSLog(@"%@", sortData);
}


- (IBAction)moveBack:(id)sender {
    [self dismissDetailNewsViewController];
}
@end
