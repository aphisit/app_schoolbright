//
//  RPListPurchasingViewController.h
//  JabjaiApp
//
//  Created by Mac on 11/8/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PurchasingProductModel.h"
#import "RPListPurchasingTableViewCell.h"
#import "PurchasingListMenuModel.h"
#import "ReportPurchasingViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface RPListPurchasingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) PurchasingProductModel *dataListProduct;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTotalLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *totalView;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;

- (IBAction)moveBackAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
