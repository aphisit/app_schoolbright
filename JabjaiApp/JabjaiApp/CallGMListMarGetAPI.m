//
//  CallGMListMarGetAPI.m
//  JabjaiApp
//
//  Created by toffee on 4/10/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallGMListMarGetAPI.h"
#import "APIURL.h"
#import "Utils.h"


@implementation CallGMListMarGetAPI{
     NSInteger connectCounter;
    NSMutableArray<MargetListModel *> *selectedMargetArray;
}

- (void)call:(long long)schoolId  {
    connectCounter = 0;
    [self getListMarget:schoolId ];
}
- (void)getListMarget:(long long)schoolId  {
    NSString *URLString = [APIURL getMargetListName:schoolId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }

            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;

                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getListMarget:schoolId ];

            }
            else {

                isFail = YES;
                connectCounter = 0;
            }

        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getListMarget:schoolId ];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getListMarget:schoolId ];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(selectedMargetArray != nil) {
                    [selectedMargetArray removeAllObjects];
                    selectedMargetArray = nil;
                }
//
                selectedMargetArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
//
                    long long shopId = [[dataDict objectForKey:@"shop_id"] longLongValue];
                   
                    NSMutableString *shopName;
                    NSMutableString *shopPic;

                    if(![[dataDict objectForKey:@"shop_name"] isKindOfClass:[NSNull class]]) {
                        shopName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"shop_name"]];
                    }
                    else {
                        shopName = [[NSMutableString alloc] initWithString:@""];
                    }
//
                    if(![[dataDict objectForKey:@"picture"] isKindOfClass:[NSNull class]]) {
                        shopPic = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"picture"]];
                    }
                    else {
                        shopPic = [[NSMutableString alloc] initWithString:@""];
                    }


                    CFStringTrimWhitespace((__bridge CFMutableStringRef) shopName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) shopPic);

                    MargetListModel *model = [[MargetListModel alloc] init];
                    model.shopId = shopId;
                    model.shopName = shopName;
                    model.picture = shopPic;
                    

                    [selectedMargetArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGMListMarGetAPI:data:success:)]) {
                    [self.delegate callGMListMarGetAPI:self data:selectedMargetArray success:YES];
                }
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGMListMarGetAPI:data:success:)]) {
                [self.delegate callGMListMarGetAPI:self data:nil success:NO];
            }
        }
     
        
    }
     ];
    
}

@end
