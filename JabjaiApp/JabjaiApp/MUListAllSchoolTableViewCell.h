//
//  MUListAllSchoolTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 28/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MUListAllSchoolTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *schoolNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@end

NS_ASSUME_NONNULL_END
