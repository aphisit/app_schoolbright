//
//  ReportBehaviorScoreViewController.h
//  JabjaiApp
//
//  Created by mac on 4/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "BSHistoryModel.h"
#import "CallGetBSHistoryForStudentReportAPI.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface ReportBehaviorScoreViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CallGetBSHistoryForStudentReportAPIDelegate, SlideMenuControllerDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

// Global variables
@property (strong, nonatomic) NSArray<BSHistoryModel *> *bsHistoryArray;
@property (nonatomic) double remainingScore;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *remainingScoreView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerListLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRemainingScoreLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *remainingScoreLabel;

- (IBAction)moveBack:(id)sender;

@end
