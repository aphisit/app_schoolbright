//
//  ReportInOutSelectClassRoomCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 27/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutSelectClassRoomCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *backgroundColor;
@property (weak, nonatomic) IBOutlet UILabel *classroomLabel;
- (void) setBackgroundClick:(int)number numberClick:(int)numberClick;
@end

NS_ASSUME_NONNULL_END
