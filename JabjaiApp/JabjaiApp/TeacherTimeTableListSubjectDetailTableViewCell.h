//
//  TeacherTimeTableListSubjectDetailTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 23/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TeacherTimeTableListSubjectDetailTableViewCell : UITableViewCell
    
    @property (weak, nonatomic) IBOutlet UILabel *subjectHeaderLable;
    @property (weak, nonatomic) IBOutlet UILabel *codeHeaderLable;
    @property (weak, nonatomic) IBOutlet UILabel *classroomHeaderLable;
    @property (weak, nonatomic) IBOutlet UILabel *instructorHeaderLable;
    @property (weak, nonatomic) IBOutlet UILabel *timeInOutHeaderLable;
    @property (weak, nonatomic) IBOutlet UILabel *saveTimeHeaderLable;
@property (weak, nonatomic) IBOutlet UILabel *classRoomHeaderLable;

    
    
    

@property (weak, nonatomic) IBOutlet UILabel *subjectNameLable;
@property (weak, nonatomic) IBOutlet UILabel *codeSubjectLable;
@property (weak, nonatomic) IBOutlet UILabel *classRoomLable;
@property (weak, nonatomic) IBOutlet UILabel *nameTeacherLable;
@property (weak, nonatomic) IBOutlet UILabel *timeInOutLable;
@property (weak, nonatomic) IBOutlet UILabel *saveTimeLable;
@property (weak, nonatomic) IBOutlet UILabel *classLable;


@property (weak, nonatomic) IBOutlet UIView *section1;



@end

NS_ASSUME_NONNULL_END
