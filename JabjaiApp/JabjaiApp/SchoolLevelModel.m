//
//  SchoolLevelModel.m
//  JabjaiApp
//
//  Created by mac on 7/4/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SchoolLevelModel.h"

@implementation SchoolLevelModel

- (long long)getClassLevelId {
    return self.classLevelId;
}

- (NSString *)getClassLevelName {
    return self.classLevelName;
}

@end
