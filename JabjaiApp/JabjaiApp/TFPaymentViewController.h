//
//  TFPaymentViewController.h
//  JabjaiApp
//
//  Created by Mac on 12/10/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFTuitionDetailViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface TFPaymentViewController : UIViewController <UIWebViewDelegate>
@property (assign, nonatomic) long long invoiceID;
@property (assign, nonatomic) long long studentID;
@property (assign, nonatomic) double  amount;
@property (assign, nonatomic) long long schoolID;
@property (assign, nonatomic) int paymentType;


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)moveBack:(id)sender;


@end

NS_ASSUME_NONNULL_END
