//
//  SNAddUserInGroupCell.m
//  JabjaiApp
//
//  Created by toffee on 2/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "SNAddUserInGroupCell.h"

@implementation SNAddUserInGroupCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)actionAddMembersButton:(id)sender {
    
    NSLog(@"delegate = %@",_delegate);
    if(_delegate != nil && [self.delegate respondsToSelector:@selector(onPressAddButton:atIndex:)]) {
        [self.delegate onPressAddButton:self atIndex:_index];
    }
}
@end
