//
//  CallJHGetSubjectModeGroupAPI.h
//  JabjaiApp
//
//  Created by toffee on 12/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JHSubjectModel.h"
@class CallJHGetSubjectAPI;

@protocol CallJHGetSubjectModeGroupAPIDelegate <NSObject>

@optional
- (void)callJHGetSubjectModeGroupAPI:(CallJHGetSubjectAPI *)classObj data:(NSArray<JHSubjectModel *> *)data success:(BOOL)success;
- (void)notArrangeTimeTable:(CallJHGetSubjectAPI *)classObj;

@end

@interface CallJHGetSubjectModeGroupAPI : NSObject
@property (nonatomic, weak) id<CallJHGetSubjectModeGroupAPIDelegate> delegate;

- (void)call:(long long)schoolId classLevelId:(long long)classLevelId;
@end
