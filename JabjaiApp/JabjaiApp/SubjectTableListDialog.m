//
//  SubjectTableListDialog.m
//  JabjaiApp
//
//  Created by mac on 7/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SubjectTableListDialog.h"

@interface SubjectTableListDialog () {
    BOOL isShowing;
}

@property (nonatomic) NSString *requestCode;
@property (nonatomic) NSArray<SSubjectModel *> *subjectArray;

@end

static CGFloat cellHeight = 70;

@implementation SubjectTableListDialog

- (IBAction)cancel:(id)sender {
    [self removeDialogFromView];
}

- (instancetype)initWithRequestCode:(NSString *)requestCode {
    
    self = [super init];
    
    if(self) {
        self.requestCode = requestCode;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TableSubjectListViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    //self.tableView.layer.cornerRadius = 10.0;
    self.tableView.layer.masksToBounds = YES;
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    isShowing = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
     [self dismissDialog];
//    if(!CGRectContainsPoint(self.tableView.frame, touchLocation)) {
//        [self dismissDialog];
//    }
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.subjectArray == nil) {
        return 0;
    }
    else {
        return self.subjectArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TableSubjectListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    SSubjectModel *model = [self.subjectArray objectAtIndex:indexPath.row];
    
    NSMutableString *timeStr = [[NSMutableString alloc] init];
    
    NSRange range1 = [model.startTime rangeOfString:@":" options:NSBackwardsSearch];
    NSRange range2 = [model.endTime rangeOfString:@":" options:NSBackwardsSearch];
    
    if(range1.length != 0) {
        NSString *startTime = [model.startTime substringToIndex:range1.location];
        [timeStr appendString:startTime];
    }
    else {
        [timeStr appendString:model.startTime];
    }
    
    if(range2.length != 0) {
        NSString *endTime = [model.endTime substringToIndex:range2.location];
        [timeStr appendString:[[NSString alloc] initWithFormat:@" - %@", endTime]];
    }
    else {
        [timeStr appendString:[[NSString alloc] initWithFormat:@" - %@", model.endTime]];
    }
    
    cell.titleLabel.text = model.subjectName;
    cell.timeLabel.text = timeStr;
    cell.numberSubject.text = model.planeID;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.delegate subjectTableListDialog:self onItemSelectWithRequestCode:self.requestCode atIndex:indexPath.row];
    [self dismissDialog];
}

#pragma mark - Dialog functions
- (void)showDialogInView:(UIView *)targetView subjectArray:(NSArray<SSubjectModel *> *)subjectArray {
    
    self.subjectArray = subjectArray;
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.tableView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.tableView setAlpha:1.0];
    [UIView commitAnimations];
    
    // Adjust table view frame size
    
    int cellNumber = 5; // maximum row show in dialog
    if(self.subjectArray.count < 6) {
        cellNumber = (int)self.subjectArray.count;
    }
    
    // Calculate position of new table view dialog
    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat viewHeight = self.view.frame.size.height;
    
    CGFloat tableViewWidth = self.view.frame.size.width - 32;
    CGFloat tableViewHeight = cellHeight * cellNumber;
    
    CGFloat posX = (viewWidth - tableViewWidth) / 2.0;
    CGFloat posY = (viewHeight - tableViewHeight) / 2.0;
    
    self.tableView.frame = CGRectMake(posX, posY, tableViewWidth, tableViewHeight);
    
    [self.tableView reloadData];
    
    isShowing = YES;
}

#pragma mark - Dialog functions
- (void)showJHDialogInView:(UIView *)targetView subjectArray:(NSArray<JHSubjectModel *> *)subjectArray {
    
    self.subjectArray = subjectArray;
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.tableView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.tableView setAlpha:1.0];
    [UIView commitAnimations];
    
    // Adjust table view frame size
    
    int cellNumber = 5; // maximum row show in dialog
    if(self.subjectArray.count < 6) {
        cellNumber = (int)self.subjectArray.count;
    }
    
    // Calculate position of new table view dialog
    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat viewHeight = self.view.frame.size.height;
    
    CGFloat tableViewWidth = self.view.frame.size.width - 32;
    CGFloat tableViewHeight = cellHeight * cellNumber;
    
    CGFloat posX = (viewWidth - tableViewWidth) / 2.0;
    CGFloat posY = (viewHeight - tableViewHeight) / 2.0;
    
    self.tableView.frame = CGRectMake(posX, posY, tableViewWidth, tableViewHeight);
    
    [self.tableView reloadData];
    
    isShowing = YES;
}


- (void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.tableView setAlpha:0.0];
    [self.btnCel setAlpha:0.0];
    
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return isShowing;
}

@end
