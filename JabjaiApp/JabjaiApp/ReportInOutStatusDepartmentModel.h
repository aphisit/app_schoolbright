//
//  ReportInOutStatusDepartmentModel.h
//  JabjaiApp
//
//  Created by toffee on 6/3/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutStatusDepartmentModel : NSObject
@property (nonatomic) NSString *nameDepartment;
@property (nonatomic) long long departmentId;
@property (nonatomic) NSString *status;
@property (nonatomic) NSString *pictuer;

- (void) setNameDepartment:(NSString * _Nonnull)nameDepartment;
- (void) setDepartmentId:(long long)departmentId;
- (void) setStatus:(NSString * _Nonnull)status;
- (void) setPictuer:(NSString * _Nonnull)pictuer;

- (NSString *)getNameDepartment;
- (long long) getDepartmentId;
- (NSString *)getStatus;
- (NSString *)getPictuer;
@end

NS_ASSUME_NONNULL_END
