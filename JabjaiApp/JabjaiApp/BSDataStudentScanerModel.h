//
//  BSDataStudentScanerModel.h
//  JabjaiApp
//
//  Created by toffee on 24/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BSDataStudentScanerModel : NSObject

@property (nonatomic) NSString *studentFirstName;
@property (nonatomic) NSString *studentLastName;
@property (nonatomic) NSString *studentPicture;
@property (nonatomic) NSString *studentClass;
@property (nonatomic) NSString *studentCode;
@property (nonatomic) long long studentID;

- (void)setStudentFirstName:(NSString *)studentFirstName;
- (void)setStudentLastName:(NSString *)studentLastName;
- (void)setStudentPicture:(NSString *)studentPicture;
- (void)setStudentClass:(NSString *)studentClass;
- (void)setStudentCode:(NSString *)studentCode;
- (void)setStudentID:(long long)studentID;

- (NSString*)getStudentFirstName;
- (NSString*)getStudentLastName;
- (NSString*)getStudentPicture;
- (NSString*)getStudentClass;
- (NSString*)getStudentCode;
- (long long)getStudentID;

@end

NS_ASSUME_NONNULL_END
