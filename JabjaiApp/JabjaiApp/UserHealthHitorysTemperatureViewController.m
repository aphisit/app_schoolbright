//
//  UserHealthHitorysTemperatureViewController.m
//  JabjaiApp
//
//  Created by Mac on 23/6/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "UserHealthHitorysTemperatureViewController.h"
#import "Utils.h"
#import "UserData.h"
#import "AppDelegate.h"
@interface UserHealthHitorysTemperatureViewController (){
    NSArray<UserHealthHistoryModel *> *historyTemperatureArray;
}
@property (nonatomic,strong) CallUHGetHistoryHealthAPI *callUHGetHistoryHealthAPI;
@property (nonatomic,strong) SelectOneDayCalendarViewController *selectOneDayCalendarViewController;
@end

@implementation UserHealthHitorysTemperatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([UserHealthTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    CAGradientLayer *gradient;
    [self.headerView layoutIfNeeded];
    NSLog(@"Height = %f",self.headerView.frame.size.width);
    gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    self.headerLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_HEALTH_TEMPERATURE",nil,[Utils getLanguage],nil);
    self.noDataLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_DATA_NOTFOUND",nil,[Utils getLanguage],nil);
    //[self.headerView layoutIfNeeded];
}

#pragma mark - CallUHGetHistoryHealthAPI
-(void)doCallUHGetHistoryHealthAPI:(long long)userId schoolId:(long long)schoolId date:(NSString*)date{
    [self showIndicator];
    self.callUHGetHistoryHealthAPI = [[CallUHGetHistoryHealthAPI alloc] init];
    self.callUHGetHistoryHealthAPI.delegate = self;
    [self.callUHGetHistoryHealthAPI call:userId schoolId:schoolId date:date];
}
-(void)callUHGetHistoryHealthAPI:(NSArray<UserHealthHistoryModel *> *)temperatureArray sucess:(BOOL)sucess{
    [self stopIndicator];
    if (sucess) {
        
        historyTemperatureArray = temperatureArray;
        NSLog(@"xx");
    }
    if (temperatureArray.count > 0) {
        self.noDataLabel.hidden = YES;
    }else{
        self.noDataLabel.hidden = NO;
    }
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return historyTemperatureArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserHealthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    UserHealthHistoryModel *model = [historyTemperatureArray objectAtIndex: indexPath.row];
    cell.timeLabel.text = [model getTime];
    cell.dateLabel.text = [model getDate];
    cell.temperatureLabel.text =[NSString stringWithFormat:@"%.@°c", [model getTemperature]] ;
    
    NSInteger temperature = [[model getTemperature] integerValue];
    if (temperature >= 37) {
        cell.heartImage.image = [UIImage imageNamed:@"health_icom_red_heart"];
    }else if (temperature == 36){
        cell.heartImage.image = [UIImage imageNamed:@"health_icon_orange_heart"];
    }else{
        cell.heartImage.image = [UIImage imageNamed:@"health_icon_green_heart"];
    }
    return cell;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            return 110; /* Device is iPad */
        }else{
            return 80; /* Device is iPhone */
        }
    
}

#pragma mark - SelectOneDayCalendarViewController
- (IBAction)chooseDateAction:(id)sender {
    self.selectOneDayCalendarViewController = [[SelectOneDayCalendarViewController alloc] init];
    self.selectOneDayCalendarViewController.delegate = self;
    UserHealthHitorysTemperatureViewController *rootController = (UserHealthHitorysTemperatureViewController*)[[(AppDelegate*)
    [[UIApplication sharedApplication]delegate] window] rootViewController];
    [self.selectOneDayCalendarViewController showDialogInView:rootController.view];
}
-(void)calendarDialogPressOK:(NSArray<NSDate *> *)selectedDates{
    NSLog(@"Date = %@",[Utils dateToServerDateFormat:[selectedDates objectAtIndex:0]]);
    [self doCallUHGetHistoryHealthAPI:[UserData getUserID] schoolId:[UserData getSchoolId] date:[Utils dateToServerDateFormat:[selectedDates objectAtIndex:0]]];
}

-(void)showPageHitorysTemperatuer:(UIView *)targetView{
 
    CGSize result = [[UIScreen mainScreen] bounds].size;
    NSLog(@"screen = %@",@(result.height));
    if(result.height == 568)
    {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20
        , targetView.frame.size.width, targetView.frame.size.height)];
        // iPhone 5,SE
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y
        , targetView.frame.size.width, targetView.frame.size.height)];
    }
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    self.noDataLabel.hidden = YES;
    [self doCallUHGetHistoryHealthAPI:[UserData getUserID] schoolId:[UserData getSchoolId] date:[Utils dateToServerDateFormat:[NSDate date]]];
    //[self doCallUHGetHistoryHealthAPI:111198 schoolId:161 date:@"06/30/2020"];
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)moveBackAction:(id)sender {
    //[self.contentView setAlpha:0.0];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
}

@end
