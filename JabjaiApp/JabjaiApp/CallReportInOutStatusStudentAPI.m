//
//  CallReportInOutStatusStudentAPI.m
//  JabjaiApp
//
//  Created by toffee on 28/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallReportInOutStatusStudentAPI.h"
#import "APIURL.h"
#import "Utils.h"

@interface CallReportInOutStatusStudentAPI () {
    NSInteger connectCounter;
    NSMutableArray<ReportInOutStatusStudentModel *> *dataStatuArray;
    NSMutableArray<ReportInOutStatusClassroomModel*> *dataClassroomArray;
}
@end
@implementation CallReportInOutStatusStudentAPI

- (void) call:(long long)schoolId dayReports:(NSString *)dayReports levelId:(long long)levelId{
    connectCounter = 0;
    [self getReportInOutStatus:schoolId dayReports:dayReports levelId:levelId];
}

- (void) getReportInOutStatus :(long long)schoolId dayReports:(NSString *)dayReports levelId:(long long)levelId{
    NSString *URLString = [APIURL getReportInOutStatus:schoolId dayReports:dayReports levelId:levelId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getReportInOutStatus:schoolId dayReports:dayReports levelId:levelId];;
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportInOutStatus:schoolId dayReports:dayReports levelId:levelId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportInOutStatus:schoolId dayReports:dayReports levelId:levelId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                NSMutableString *nameStudent, *status, *picture, *subLevelName ;
                long long studentId, subLevelId;
                NSArray *studentArray, *logData;
                if(dataStatuArray != nil) {
                    [dataStatuArray removeAllObjects];
                    dataStatuArray = nil;
                }
                if(dataClassroomArray != nil) {
                    [dataClassroomArray removeAllObjects];
                    dataClassroomArray = nil;
                }
                dataClassroomArray = [[NSMutableArray alloc] init];
                
               
                for (int i = 0; i<returnedArray.count; i++) {
                    
                    dataStatuArray = [[NSMutableArray alloc] init];
                    NSDictionary* dataDict = [returnedArray objectAtIndex:i];
                    studentArray = [dataDict objectForKey:@"logDatas"];
                    subLevelId = [[dataDict objectForKey:@"sublevel_Id"] longLongValue];
                    if(![[dataDict objectForKey:@"sbulevel_Name"] isKindOfClass:[NSNull class]]) {
                        subLevelName = [[NSMutableString alloc]initWithFormat:@"%@", [dataDict objectForKey:@"sbulevel_Name"]];
                    }
                    else {
                        subLevelName = [[NSMutableString alloc] initWithString:@""];
                    }
                    for (int j = 0; j<studentArray.count; j++) {
                        NSDictionary *studentDict = [studentArray objectAtIndex:j];
                        NSInteger studentState = 0;
                        studentId = [[studentDict objectForKey:@"id"] longLongValue];
                        if(![[studentDict objectForKey:@"name"] isKindOfClass:[NSNull class]]) {
                            nameStudent = [[NSMutableString alloc]
                                                           initWithFormat:@"%@", [studentDict objectForKey:@"name"]];
                        }
                        else {
                            nameStudent = [[NSMutableString alloc] initWithString:@""];
                        }
                        if(![[studentDict objectForKey:@"status"] isKindOfClass:[NSNull class]]) {
                            status = [[NSMutableString alloc]
                                                           initWithFormat:@"%@", [studentDict objectForKey:@"status"]];
                        }
                        else {
                            status = [[NSMutableString alloc] initWithString:@""];
                        }
                    
                        if(![[studentDict objectForKey:@"pictuer"] isKindOfClass:[NSNull class]]) {
                            picture = [[NSMutableString alloc]
                                                      initWithFormat:@"%@", [studentDict objectForKey:@"pictuer"]];
                        }
                        else {
                            picture = [[NSMutableString alloc] initWithString:@""];
                        }
                        
                        if(![[studentDict objectForKey:@"Student_State"] isKindOfClass:[NSNull class]]) {
                            studentState = [ [studentDict objectForKey:@"Student_State"]integerValue];
                        }
                        
                    
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) nameStudent);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) status);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) picture);
                    
                        ReportInOutStatusStudentModel *model = [[ReportInOutStatusStudentModel alloc] init];
                        [model setNameStudent:nameStudent];
                        [model setStudentId:studentId];
                        [model setStatus:status];
                        [model setPictuer:picture];
                        [model setStudentState:studentState];
                        [dataStatuArray addObject:model];
                    }
                     CFStringTrimWhitespace((__bridge CFMutableStringRef) subLevelName);
                    
                    ReportInOutStatusClassroomModel *model = [[ReportInOutStatusClassroomModel alloc] init];
                    [model setSubLevelName:subLevelName];
                    [model setSubLevelId:subLevelId];
                    [model setLogData:dataStatuArray];
                    
                    [dataClassroomArray addObject:model];
                    
                }
                


                connectCounter = 0;
                
                
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportInOutStatusStudentAPI:dataClassroom:success:)]) {
                    [self.delegate callReportInOutStatusStudentAPI:self dataClassroom:dataClassroomArray success:YES];
                }
                
                
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportInOutStatusStudentAPI:dataClassroom:success:)]) {
                    [self.delegate callReportInOutStatusStudentAPI:self dataClassroom:dataClassroomArray success:NO];
                }
            }
            
        }
        
    }];
}


@end
