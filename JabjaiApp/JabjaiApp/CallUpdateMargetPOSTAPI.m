//
//  CallUpdateMargetPOSTAPI.m
//  JabjaiApp
//
//  Created by toffee on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallUpdateMargetPOSTAPI.h"
#import "APIURL.h"

#import "UserData.h"
#import <AFNetworking/AFNetworking.h>

@implementation CallUpdateMargetPOSTAPI

-(void)call:(long long)schoolId shopId:(long long)shopId userId:(long long)userId jsonString:(NSString *)jsonString{
    [self updateOrder:schoolId shopId:shopId userId:userId jsonString:jsonString];
}

- (void)updateOrder:(long long)schoolId shopId:(long long)shopId userId:(long long)userId jsonString:(NSString *)jsonString {
    NSString *URLString = [APIURL getUpdateOrder:schoolId shopId:shopId userId:userId];
    
    NSError* error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableLeaves error:&error];
    
   
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
    [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject = %@", responseObject);
        NSString *statusMonny = [responseObject objectForKey:@"status"];
        NSDictionary *dic = [responseObject objectForKey:@"data"];
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(callUpdateMargetPOSTAPI:response:chackMonny:success:)]) {
            NSString *response = responseObject;
            [self.delegate callUpdateMargetPOSTAPI:self response:dic chackMonny:statusMonny success:YES];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(callUpdateMargetPOSTAPI:response:chackMonny:success:)]) {
             [self.delegate callUpdateMargetPOSTAPI:self response:nil chackMonny:nil success:NO];
        }
        
    }];
}
@end
