//
//  ReportPurchasingViewController.h
//  JabjaiApp
//
//  Created by mac on 4/20/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "CFSCalendar.h"
#import "RPPurchasingTableViewDialog.h"
#import "RPListPurchasingViewController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@protocol PurchasingReportViewControllerDelegate <NSObject>

@optional
- (void)onSelectDate:(NSDate *)date;

@end

@interface ReportPurchasingViewController : UIViewController <SlideMenuControllerDelegate, CFSCalendarDelegate, CFSCalendarDelegateAppearance, CFSCalendarDataSource , UITableViewDataSource, UITableViewDelegate, RPPurchasingTableViewDialogDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

@property (retain, nonatomic) id<PurchasingReportViewControllerDelegate> delegate;

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerBathLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *containerTableView;
@property (weak, nonatomic) IBOutlet UIView *totalView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *summaryLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailContraint;
- (IBAction)openDetail:(id)sender;
@property (weak, nonatomic) IBOutlet UIStackView *summaryStack;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightStackConstraint;
- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
