//
//  SNDetailNewsViewController.h
//  JabjaiApp
//
//  Created by Mac on 2/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageInboxDataModel.h"
#import "SNReadFileViewController.h"
#import "SNReadImageViewController.h"
#import "SNDetailNewsCollectionViewCell.h"
#import "ConfirmDialog.h"
#import "CallFileMessageAPI.h"
#import "CellDetailMessageBoxAPI.h"
#import "CallSNSendReplyCodeMessageBoxAPI.h"
#import "AlertDialog.h"
NS_ASSUME_NONNULL_BEGIN
@protocol SNDetailNewsViewControllerDelegate <NSObject>

- (void) closeShowDetailNews;
@end

@interface SNDetailNewsViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, CallFileMessageAPIDelegate,CellDetailMessageBoxAPIDelegate, CallSNSendReplyCodeMessageBoxAPIDelegate,ConfirmDialogDelegate>
@property (nonatomic,retain) id<SNDetailNewsViewControllerDelegate> delegate;
//@property (strong, nonatomic) MessageInboxDataModel *model;
//@property (nonatomic) NSInteger typySend;//message = 0, notication = 1
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *notAllowedView;
@property (weak, nonatomic) IBOutlet UIView *allowView;
@property (weak, nonatomic) IBOutlet UIView *acknowledgeView;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UIStackView *allowStack;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;


@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerNewsLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttactImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttactFileLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highImaCollectionConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highFileCollectionConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *imageCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *fileCollectionView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorIma;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorFile;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *acknowledgeConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allowHeightConstraint;


- (IBAction)backAction:(id)sender;
- (void)dismissDetailNewsViewController;
- (BOOL)isDialogShowing;
- (void) showDetailNewsViewController:(UIView*)targetView model:(MessageInboxDataModel*)model typeSend:(NSInteger)typeSend;
@end

NS_ASSUME_NONNULL_END
