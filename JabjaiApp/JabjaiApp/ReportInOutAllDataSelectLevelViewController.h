//
//  ReportInOutAllDataSelectLevelViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"
#import "ReportInOutAllDataSelectLevelTableViewCell.h"
#import "CallReportInOutAllDataSelectLevelAPI.h"
#import "ReportInOutAllDataSelectLevelModel.h"
#import "ReportInOutStatusStudentViewController.h"

@interface ReportInOutAllDataSelectLevelViewController : UIViewController <SlideMenuControllerDelegate, UITableViewDelegate, UITableViewDataSource,CallReportInOutAllDataSelectLevelAPIDelegate>
@property (nonatomic) NSDate *date;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)moveBack:(id)sender;
//- (IBAction)next:(id)sender;

@end
