//
//  ReportPurchasingAllDataViewController.h
//  JabjaiApp
//
//  Created by mac on 4/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"
#import <MKDropdownMenu/MKDropdownMenu.h>
#import <Charts/Charts.h>
#import "CallReportPurchasingDayAPI.h"
#import "CallReportPurchasingWeekAPI.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface ReportPurchasingAllDataViewController : UIViewController <SlideMenuControllerDelegate,CallReportPurchasingDayAPIDelegate,CallReportPurchasingWeekAPIDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *detailRefillMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *dateTimeDropdown;
@property (weak, nonatomic) IBOutlet BarChartView *barCharView;
- (IBAction)moveBack:(id)sender;

@end
