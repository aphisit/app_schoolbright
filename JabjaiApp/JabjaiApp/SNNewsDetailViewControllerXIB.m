//
//  SNNewsDetailViewControllerXIB.m
//  JabjaiApp
//
//  Created by Mac on 17/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "SNNewsDetailViewControllerXIB.h"
#import "Utils.h"
#import "APIURL.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface SNNewsDetailViewControllerXIB (){
    NSInteger connectCounter;
    NSMutableArray *imageArray,*fileArray,*typeFileArray;
    NSInteger imgConstant,fileConstant;
}
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property (strong, nonatomic) saveImageXIBViewController *saveImageXIB;
@property (strong, nonatomic) ReportSendNewsOpenFileViewController *reportSendNewsOpenFileViewController;
@end
static NSString *cellIdentifier = @"Cell";
@implementation SNNewsDetailViewControllerXIB

- (void)viewDidLoad {
    [super viewDidLoad];
    
    imgConstant = self.highImaCollectionConstraint.constant;
    fileConstant = self.highFileCollectionConstraint.constant;
    NSLog(@"high = %f",self.highImaCollectionConstraint.constant);
    
    // set up formatter
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.timeFormatter = [Utils getDateFormatter];
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [self.timeFormatter setDateFormat:@"HH:mm"];
    }
    
    imageArray = [[NSMutableArray alloc] init];
    fileArray = [[NSMutableArray alloc] init];
    typeFileArray = [[NSMutableArray alloc] init];
    [self.imgCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportSendNewsImageWithFileCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.imgCollectionView.delegate = self;
    self.imgCollectionView.dataSource = self;
    
    [self.fileCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportSendNewsImageWithFileCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.fileCollectionView.delegate = self;
    self.fileCollectionView.dataSource = self;
    [self getReportSendNewsDetailWithNewsID:self.newsID];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    self.headerSendTypeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_FORMAT_SEND",nil,[Utils getLanguage],nil);
    self.headerAmountReciptientLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_AMOUNT",nil,[Utils getLanguage],nil);
    self.headerReadLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_READ",nil,[Utils getLanguage],nil);
    self.headerUnReadLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_UNREAD",nil,[Utils getLanguage],nil);
    self.headerDateSendLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_DATE_SEND",nil,[Utils getLanguage],nil);
    self.headerTimeSendLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_TIME_SEND",nil,[Utils getLanguage],nil);
    self.headerDetailLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_LIST",nil,[Utils getLanguage],nil);
    self.headerAttachImageLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_ATTACH_IMAGE",nil,[Utils getLanguage],nil);
    self.headerAttachFileLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_ATTACH_FILE",nil,[Utils getLanguage],nil);
}

- (void)getReportSendNewsDetailWithNewsID:(long long)newsId{
    
   // [self showIndicator];
    
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getReportSendNewsDetailWithSchoolID:schoolID newsID:newsId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        //[self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getReportSendNewsDetailWithNewsID:newsId];
            }
            else {
                self->connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            self->connectCounter = 0;
             NSMutableString *message;
            NSDateFormatter *formatter = [Utils getDateFormatter];
            [formatter setDateFormat:[Utils getXMLDateFormat]];
            NSDateFormatter *formatter2 = [Utils getDateFormatter];
            [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
            int sendType = [[returnedData objectForKey:@"send_type"] intValue];
            if(![[returnedData objectForKey:@"message"] isKindOfClass:[NSNull class]]) {
                message = [[NSMutableString alloc] initWithString:[returnedData objectForKey:@"message"]];
            }
            else {
                message = [[NSMutableString alloc] initWithString:@""];
            }

            NSMutableString *newsDateStr = [[NSMutableString alloc] initWithString:[returnedData objectForKey:@"daysend"]];
            int all = [[returnedData objectForKey:@"user_all"] intValue];
            int read = [[returnedData objectForKey:@"user_read"] intValue];
            int unread = [[returnedData objectForKey:@"user_unread"] intValue];
            NSRange dotRange = [newsDateStr rangeOfString:@"."];
            NSDate *newsDate;
            if(dotRange.length != 0) {
                newsDate = [formatter dateFromString:newsDateStr];
            }
            else {
                newsDate = [formatter2 dateFromString:newsDateStr];
            }
            
            NSArray *imageFileArray;
            if(![[returnedData objectForKey:@"news_files"] isKindOfClass:[NSNull class]]) {
                imageFileArray = [returnedData objectForKey:@"news_files"];
            }
            
            for (int i=0; i<imageFileArray.count; i++) {

                if ([[[imageFileArray objectAtIndex:i]objectForKey:@"file_type"] isEqualToString:@"image/jpeg"]) {
                    [self->imageArray addObject:[[imageFileArray objectAtIndex:i]objectForKey:@"file_name"] ];
                }else{
                    NSString *fileType = [[imageFileArray objectAtIndex:i]objectForKey:@"file_type"];
                    NSString *fileName = [[imageFileArray objectAtIndex:i]objectForKey:@"file_name"];
                    if (fileType != nil || fileName != nil) {
                        [typeFileArray addObject:fileType];
                        [self->fileArray addObject:fileName];
                    }
                }
            }
            
            //InputData
            if (sendType == 0) {
                self.sendTypeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_SENDNOW",nil,[Utils getLanguage],nil);
            }
            else{
                self.sendTypeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_SETTIME",nil,[Utils getLanguage],nil);;
            }
           
            self.allCountLabel.text = [[NSString alloc] initWithFormat:@"%i %@", all,NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_PERSON",nil,[Utils getLanguage],nil)];
            self.readCountLabel.text = [[NSString alloc] initWithFormat:@"%i %@", read,NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_PERSON",nil,[Utils getLanguage],nil)];
            self.unreadCountLabel.text = [[NSString alloc] initWithFormat:@"%i %@", unread,NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_PERSON",nil,[Utils getLanguage],nil)];
            self.dateTimeLabel.text = [self.dateFormatter stringFromDate:newsDate];
            self.timeLabel.text = [self.timeFormatter stringFromDate:newsDate];
            self.detailTextView.text = [[NSString alloc] initWithFormat:@"%@", message];
            
            
            float wCollection = self.imgCollectionView.frame.size.width / ((self.imgCollectionView.frame.size.width/4)-10);
            float amountRow = self->imageArray.count / (int)wCollection;
            NSLog(@"num = %d",(int)wCollection);
            NSInteger modImg = self->imageArray.count % (int)wCollection ;
            if (modImg > 0) {
                amountRow ++;
            }
            self.highImaCollectionConstraint.constant = self.highImaCollectionConstraint.constant * amountRow+((amountRow*5)*2);//set hight image collectionview
            
            float wFileCollection = self.fileCollectionView.frame.size.width / ((self.fileCollectionView.frame.size.width/4)-10);
            float amountFileRow = self->fileArray.count / (int)wFileCollection;
            NSInteger modFile = self->fileArray.count % (int)wFileCollection ;
            if (modFile > 0) {
                amountFileRow ++;
            }
            self.highFileCollectionConstraint.constant = self.highFileCollectionConstraint.constant * amountFileRow + ((amountFileRow*5)*2);//set hight file colectionview
            
            if (self->imageArray.count < 1) {
                self.imgCollectionView.hidden = YES;
            }else{
                self.imgCollectionView.hidden = NO;
               [self.imgCollectionView reloadData];
            }
            
            if (self->fileArray.count < 1) {
                self.fileCollectionView.hidden = YES;
            }else{
                self.fileCollectionView.hidden = NO;
                [self.fileCollectionView reloadData];
            }
//            float wCollection = self.imageNewsCollectionView.frame.size.width / ((self.imageNewsCollectionView.frame.size.width/4)-10);
//            float amountRow = imageArray.count / (int)wCollection;
//            NSLog(@"num = %d",(int)wCollection);
//            NSInteger modImg = imageArray.count % (int)wCollection ;
//
//            if (modImg > 0) {
//                amountRow ++;
//            }
//            self.highImaCollectionConstraint.constant = self.highImaCollectionConstraint.constant * amountRow+((amountRow*5)*2);
            
//            if (imageArray.count < 1) {
//                self.imageNewsCollectionView.hidden = YES;
//            }else{
//                self.imageNewsCollectionView.hidden = NO;
//                [self.imageNewsCollectionView reloadData];
//            }
//
//            if (fileArray.count < 1) {
//                self.fileNewsCollectionView.hidden = YES;
//            }else{
//                self.fileNewsCollectionView.hidden = NO;
//                [self.fileNewsCollectionView reloadData];
//            }
        }
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.imgCollectionView) {
        return imageArray.count;
    }else{
        return fileArray.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ReportSendNewsImageWithFileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    if (collectionView == self.imgCollectionView) {
        cell.fileBtn.tag = indexPath.row;
        [cell.fileimage sd_setImageWithURL:[NSURL URLWithString: [imageArray objectAtIndex:indexPath.row]]];
        [cell.fileBtn addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else{
        cell.fileBtn.tag = indexPath.row;
        if ([[typeFileArray objectAtIndex:indexPath.row] isEqualToString:@"PDF"]) {
            [cell.fileimage setImage:[UIImage imageNamed:@"ic_pdf"]];
        }else if ([[typeFileArray objectAtIndex:indexPath.row] isEqualToString:@"MS Word"]){
            [cell.fileimage setImage:[UIImage imageNamed:@"ic_word"]];
        }else{
            [cell.fileimage setImage:[UIImage imageNamed:@"ic_excel"]];
        }
        [cell.fileBtn addTarget:self action:@selector(openFileTapped:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    [self.imgCollectionView layoutIfNeeded];
    CGSize defaultSize;
    if (collectionView == self.imgCollectionView) {
        defaultSize = CGSizeMake((self.imgCollectionView.frame.size.width/4)-10, (imgConstant));
    }else{
        defaultSize = CGSizeMake((self.fileCollectionView.frame.size.width/4)-10, (fileConstant));
    }
    return defaultSize;
}

- (void)okButtonTapped:(UIButton *)sender {
    if(self.saveImageXIB != nil && [self.saveImageXIB isDialogShowing]) {
        [self.saveImageXIB dismissDialog];
        self.saveImageXIB = nil;
    }
    self.saveImageXIB = [[saveImageXIBViewController  alloc] init];
    self.saveImageXIB.delegate = self;
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;
    [self.saveImageXIB showDialogInView:topView urlImageArray:imageArray];
}

- (void)openFileTapped:(UIButton *)sender{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;

    self.reportSendNewsOpenFileViewController = [[ReportSendNewsOpenFileViewController alloc]init];
    [self.reportSendNewsOpenFileViewController showPagesReadFileInView:topView partFile:[fileArray objectAtIndex:sender.tag]];
}

//- (void)showIndicator{
//    if (![self.indicator isAnimating]) {
//        [self.indicator startAnimating];
//    }
//}
//
//- (void)stopIndicator{
//    [self.indicator stopAnimating];
//
//}

@end
