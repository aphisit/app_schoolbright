//
//  EventCalendarDetailDialog.m
//  JabjaiApp
//
//  Created by mac on 5/16/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "EventCalendarDetailDialog.h"
#import "Utils.h"

@interface EventCalendarDetailDialog (){
    
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic) BOOL isShowing;


@end

@implementation EventCalendarDetailDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.dialogView.layer.masksToBounds = YES;
    self.closeBtn.layer.masksToBounds = YES;
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.isShowing = NO;
    
    [self.closeBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_CALENDA_SCHOOL_CLOSE",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm;
    //set color header
    [self.closeBtn layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.closeBtn.bounds;
    [self.closeBtn.layer insertSublayer:gradientConfirm atIndex:0];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

- (void)dismissDialog {
    
    [self removeDialogFromView];
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(eventDialogClose)]) {
        [self.delegate eventDialogClose];
    }
}

-(void)showDialogInView:(UIView *)targetView message:(NSString *)message holiday:(NSString *)holiday messageDate:(NSDate *)messageDate{
    
   
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    [self.messageLabel setText:message];
    [self.message2Label setText:holiday];
    
    NSString *dateEvent = [NSString stringWithFormat:@"%@", [self.dateFormatter stringFromDate:messageDate]];
    [self.dateLabel setText:dateEvent];
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

-(BOOL)isDialogShowing {
    return self.isShowing;
}

- (IBAction)closeDialog:(id)sender {
    
    [self dismissDialog];
    
}
@end
