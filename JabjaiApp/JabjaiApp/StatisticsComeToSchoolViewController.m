//
//  StatisticsComeToSchoolViewController.m
//  JabjaiApp
//
//  Created by Mac on 17/7/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "StatisticsComeToSchoolViewController.h"
#import "UserData.h"
#import "Utils.h"
#import "CreditBureauGraphDialog.h"
#import "AppDelegate.h"
#import "UserInfoViewController.h"
@interface StatisticsComeToSchoolViewController (){
    NSInteger totalStatus,statusOnTime,statusLate,statusAbsence,statusSick,statusPersonal,statusEvent,statusNoCheckPercent,statusOther;
    UIColor *onTimeColor,*lateColor,*absenceColor,*sickColor,*personalColor,*eventColor,*unCheckColor,*otherColor;
    NSMutableArray *DateList;
    NSString *selected_calendar;
    MKDropdownMenu* mKDropdownMenu;
    
    // The dialog
    TableListWithHeaderDialog *dialogSelectMonth;
    TableListDialog *dialogSelectYear;
}
@property (strong, nonatomic) specifyDateDialog *dialogSpecifyDate;
@property (strong, nonatomic) CreditBureauGraphDialog *creditBureauGraphDialog;
@property (strong, nonatomic) CallStatisticsComeToSchoolAPI *callStatisticsComeToSchoolAPI;
@end

@implementation StatisticsComeToSchoolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupPieChartView];
    
    onTimeColor = [UIColor colorWithRed:0.19 green:0.74 blue:0.31 alpha:1.0];
    lateColor = [UIColor colorWithRed:1.00 green:0.76 blue:0.33 alpha:1.0];
    absenceColor = [UIColor colorWithRed:0.94 green:0.43 blue:0.43 alpha:1.0];
    sickColor = [UIColor colorWithRed:0.87 green:0.43 blue:0.98 alpha:1.0];
    personalColor = [UIColor colorWithRed:0.58 green:0.11 blue:0.70 alpha:1.0];
    eventColor = [UIColor colorWithRed:0.11 green:0.45 blue:0.77 alpha:1.0];
    unCheckColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    otherColor = [UIColor colorWithRed: 0.00 green: 0.30 blue: 0.81 alpha: 1.00];
    if ([UserData getUserType] == STUDENT) {
        self.otherDialogView.hidden = YES;
    }
    // Do any additional setup after loading the view.
    [self setLanguage];
    [self setDropdowe];
    [self declaredDialog];
    [self doSelectDayCalendar:@"week"];
}

//- (void)viewDidAppear:(BOOL)animated {
//
//    if(self.pieChart != nil) {
//        [self.pieChart animateWithYAxisDuration:2.0 easingOption:ChartEasingOptionEaseOutCirc];
//    }
//}

- (void)viewWillAppear:(BOOL)animated{
    [mKDropdownMenu closeAllComponentsAnimated:YES];
    NSLog(@"xxxx");
}

-(void)viewWillDisappear:(BOOL)animated{
    [mKDropdownMenu closeAllComponentsAnimated:YES];
    NSLog(@"xxxx");
}

- (void)setLanguage{
    self.onTimeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME", nil, [Utils getLanguage], nil);
    self.lateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LATE", nil, [Utils getLanguage], nil);
    self.absenceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ABSENCE", nil, [Utils getLanguage], nil);
    self.eventLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_EVENT", nil, [Utils getLanguage], nil);
    self.sickLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SICK", nil, [Utils getLanguage], nil);
    self.personalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_PERSONAL", nil, [Utils getLanguage], nil);
    self.uncheckLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NOCHECK", nil, [Utils getLanguage], nil);
    self.otherLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_OTHER", nil, [Utils getLanguage], nil);
}

- (void)declaredDialog{
    // Attach tap event to each legend view
    UITapGestureRecognizer *onTimeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleOnTimeViewTap:)];
    [self.onTimeDialogView addGestureRecognizer:onTimeTap];

    UITapGestureRecognizer *lateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLateViewTap:)];
    [self.lateDialogView addGestureRecognizer:lateTap];

    UITapGestureRecognizer *absenceTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleAbsenceViewTap:)];
    [self.absenceDialogView addGestureRecognizer:absenceTap];

    UITapGestureRecognizer *sickTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSickViewTap:)];
    [self.sickDialogView addGestureRecognizer:sickTap];

    UITapGestureRecognizer *personalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePersonalViewTap:)];
    [self.personalDialogView addGestureRecognizer:personalTap];

    UITapGestureRecognizer *eventTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleEventViewTap:)];
    [self.eventDialogView addGestureRecognizer:eventTap];
    
    UITapGestureRecognizer *unCheckTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleUnCheckViewTap:)];
    [self.uncheckDialogView addGestureRecognizer:unCheckTap];
    
    UITapGestureRecognizer *otherTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleOtherViewTap:)];
    [self.otherDialogView addGestureRecognizer:otherTap];
}

-(void)handleOnTimeViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:ONTIME times:statusOnTime totalTimes:totalStatus];
}
-(void)handleLateViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:LATE times:statusLate totalTimes:totalStatus];
}
-(void)handleAbsenceViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:ABSENCE times:statusAbsence totalTimes:totalStatus];
}
-(void)handleSickViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:SICK times:statusSick totalTimes:totalStatus];
}
-(void)handlePersonalViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:PERSONAL times:statusPersonal totalTimes:totalStatus];
}
-(void)handleEventViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:EVENT times:statusEvent totalTimes:totalStatus];
}
-(void)handleUnCheckViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:UNCHECK times:statusNoCheckPercent totalTimes:totalStatus];
}
-(void)handleOtherViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:OTHER times:statusOther totalTimes:totalStatus];
}

-(void)doSelectDayCalendar:(NSString*)selectMode{
    
    NSDate *dateNow = [NSDate date];
    //day
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    [gregorian setFirstWeekday:2]; // Sunday == 1, Saturday == 7
    NSUInteger adjustedWeekdayOrdinal = [gregorian ordinalityOfUnit:NSWeekdayCalendarUnit inUnit:NSWeekCalendarUnit forDate:dateNow];
    NSLog(@"Adjusted weekday ordinal: %d", adjustedWeekdayOrdinal);
    //month
    NSCalendar *gregorian1 = [Utils getGregorianCalendar];
    NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:dateNow];
    [comp setDay:1];
    NSDate *firstDayOfMonthDate = [gregorian dateFromComponents:comp];
    //year
   
    NSDateComponents *components= [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:dateNow];
    components.month = 1;
    components.day = 1;
    NSDate *firstDayOfYear = [[NSCalendar currentCalendar] dateFromComponents:components];
    NSString *daysNow = [Utils getThaiDateFormatWithDate:dateNow]; // 20 /กันยายน /2561
    //day
    NSDate *daysAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-(adjustedWeekdayOrdinal-1) toDate:[NSDate date] options:0];
    NSString *daysStr = [Utils getThaiDateFormatWithDate:daysAgo];
    //month
    NSDate *monthAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfMonthDate options:0];
    NSString *monthStr = [Utils getThaiDateFormatWithDate:monthAgo];
    //year
    NSDate *yearAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfYear options:0];
    NSString *yearStr = [Utils getThaiDateFormatWithDate:yearAgo];
    
    DateList = [[NSMutableArray alloc] init];
    [DateList addObject:[NSString stringWithFormat:@"%@ %@ - %@",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_WEEKLY",nil,[Utils getLanguage],nil),daysStr,daysNow]];
    [DateList addObject:[NSString stringWithFormat:@"%@ %@ - %@",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_MONTHLY",nil,[Utils getLanguage],nil),monthStr,daysNow]];
    [DateList addObject:[NSString stringWithFormat:@"%@ %@ - %@",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_YEARLY",nil,[Utils getLanguage],nil),yearStr,daysNow]];
    [DateList addObject:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SELECTDATE",nil,[Utils getLanguage],nil)];
    
    
    if ([selectMode isEqualToString:@"week"]) {
        selected_calendar = [DateList objectAtIndex:0];
        NSDate *days = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-(adjustedWeekdayOrdinal-1) toDate:[NSDate date] options:0];
         [self updatePieChartDataWithStartDate:days endDate:dateNow];
    }else if([selectMode isEqualToString:@"month"]){
        NSDate *month = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfMonthDate options:0];
        [self updatePieChartDataWithStartDate:month endDate:dateNow];
    }else if([selectMode isEqualToString:@"year"]){
        NSDate *year = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfYear options:0];
        [self updatePieChartDataWithStartDate:year endDate:dateNow];
    }
   
    NSLog(@"xxx");
}
- (void)updatePieChartDataWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    [self doCallStatisticsComeToSchoolAPI:startDate endDate:endDate status:-1];
}

#pragma mark - CallStatisticsComeToSchoolAPI
-(void)doCallStatisticsComeToSchoolAPI:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status{
    [self showIndicator];
    if(self.callStatisticsComeToSchoolAPI != nil) {
           self.callStatisticsComeToSchoolAPI = nil;
    }
    self.callStatisticsComeToSchoolAPI = [[CallStatisticsComeToSchoolAPI alloc] init];
    self.callStatisticsComeToSchoolAPI.delegate = self;
    [self.callStatisticsComeToSchoolAPI call:startDate endDate:endDate status:(status)] ;
}
-(void)callStatisticsComeToSchoolAPI:(CallStatisticsComeToSchoolAPI *)classObj data:(NSArray<AttendToSchoolModel *> *)data success:(BOOL)success{
    if (data != nil && success) {
        [self stopIndicator];
            // prepare data for pie chart
        NSMutableArray  *yData = [[NSMutableArray alloc] init];
        //[yData removeAllObjects];
        double onTimePercent = 0.0, latePercent = 0.0, absencePercent = 0.0, personalPercent = 0.0, sickPercent = 0.0, eventPercent = 0.0, noCheckPercent = 0.0, otherPercent = 0.0;
        statusOnTime = 0;
        statusLate = 0;
        statusAbsence = 0;
        statusSick = 0;
        statusPersonal = 0;
        statusEvent = 0;
        statusNoCheckPercent = 0;
        statusOther = 0;
        
        for(AttendToSchoolModel *model in data) {
            if([model.status integerValue] == 0 || [model.status integerValue] == 7) {
                statusOnTime++;
            }
            else if([model.status integerValue] == 1) {
                statusLate++;
            }
            else if([model.status integerValue] == 3) {
                statusAbsence++;
            }
            else if([model.status integerValue] == 4 || [model.status integerValue] == 10) {
                statusPersonal++;
            }
            else if([model.status integerValue] == 5 || [model.status integerValue] == 11) {
                statusSick++;
            }
            else if([model.status integerValue] == 6 || [model.status integerValue] == 12) {
                statusEvent++;
            }
            else if([model.status integerValue] == 99 ) {
                statusNoCheckPercent++;
            }else if([model.status integerValue] == 21 || [model.status integerValue] == 22 || [model.status integerValue] == 23 || [model.status integerValue] == 24 || [model.status integerValue] == 25 || [model.status integerValue] == 26 ){
                statusOther++;
            }
        }
        totalStatus = statusOnTime + statusLate + statusAbsence + statusPersonal + statusSick + statusEvent + statusNoCheckPercent + statusOther;
        [yData addObject:[[NSNumber alloc] initWithInteger:statusOnTime]];
        [yData addObject:[[NSNumber alloc] initWithInteger:statusLate]];
        [yData addObject:[[NSNumber alloc] initWithInteger:statusAbsence]];
        [yData addObject:[[NSNumber alloc] initWithInteger:statusSick]];
        [yData addObject:[[NSNumber alloc] initWithInteger:statusPersonal]];
        [yData addObject:[[NSNumber alloc] initWithInteger:statusEvent]];
        [yData addObject:[[NSNumber alloc] initWithInteger:statusNoCheckPercent]];
        [yData addObject:[[NSNumber alloc] initWithInteger:statusOther]];
        
        onTimePercent = ((double)statusOnTime/totalStatus) * 100;
        latePercent = ((double)statusLate/totalStatus) * 100;
        absencePercent = ((double)statusAbsence/totalStatus) * 100;
        personalPercent = ((double)statusPersonal/totalStatus) * 100;
        sickPercent = ((double)statusSick/totalStatus) * 100;
        eventPercent = ((double)statusEvent/totalStatus) * 100;
        noCheckPercent = ((double)statusNoCheckPercent/totalStatus) * 100;
        otherPercent = ((double)statusOther/totalStatus) * 100;
        
        
        if ( isnan(onTimePercent)) {
            onTimePercent = 0;
        }
        if (isnan(latePercent)) {
            latePercent = 0;
        }
        if (isnan(absencePercent)) {
            absencePercent = 0;
        }
        if (isnan(personalPercent)) {
            personalPercent = 0;
        }
        if (isnan(sickPercent)) {
            sickPercent = 0;
        }
        if (isnan(eventPercent)) {
            eventPercent = 0;
        }
        if (isnan(noCheckPercent)) {
            noCheckPercent = 0;
        }
        if (isnan(otherPercent)) {
            otherPercent = 0;
        }
        self.onTimeAmountLabel.text = [NSString stringWithFormat:@"%d %@",statusOnTime,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        self.lateAmountLabel.text = [NSString stringWithFormat:@"%d %@",statusLate,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        self.absenceAmountLabel.text = [NSString stringWithFormat:@"%d %@",statusAbsence,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        self.eventAmountLabel.text = [NSString stringWithFormat:@"%d %@",statusEvent,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        self.sickAmountLabel.text = [NSString stringWithFormat:@"%d %@",statusSick,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        self.personalAmountLabel.text = [NSString stringWithFormat:@"%d %@",statusPersonal,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        self.uncheckAmountLabel.text = [NSString stringWithFormat:@"%d %@",statusNoCheckPercent,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        self.otherAmountLabel.text = [NSString stringWithFormat:@"%d %@",statusOther,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        
        
//        self.onTimePercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",onTimePercent];
//        self.latePercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",latePercent];
//        self.absencePercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",absencePercent];
//        self.eventPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",eventPercent];
//        self.sickPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",sickPercent];
//        self.personalPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",personalPercent];
//        self.unCheckPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",noCheckPercent];
//        self.otherPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",otherPercent];
        
        self.onTimePercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",onTimePercent];
        self.latePercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",latePercent];
        self.absencePercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",absencePercent];
        self.eventPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",eventPercent];
        self.sickPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",sickPercent];
        self.personalPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",personalPercent];
        self.unCheckPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",noCheckPercent];
        self.otherPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",otherPercent];
        
        CAGradientLayer *onTimeGradient = [Utils getGradientColorStatus:@"green"];
        onTimeGradient.frame = self.onTimeColorView.bounds;
        UIGraphicsBeginImageContext(onTimeGradient.bounds.size);
        [onTimeGradient renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *backgroundColorImageOnTime = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.onTimeColorView.layer.cornerRadius = MAX(self.onTimeColorView.frame.size.width, self.onTimeColorView.frame.size.height)/2.0;
        self.onTimeColorView.layer.masksToBounds = YES;
        [self.onTimeColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImageOnTime]];
        
        CAGradientLayer *lateGradient = [Utils getGradientColorStatus:@"yellow"];
        lateGradient.frame = self.lateColorView.bounds;
        UIGraphicsBeginImageContext(lateGradient.bounds.size);
        [lateGradient renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *backgroundColorImageLate = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.lateColorView.layer.cornerRadius = MAX(self.lateColorView.frame.size.width, self.lateColorView.frame.size.height)/2.0;
        self.lateColorView.layer.masksToBounds = YES;
        [self.lateColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImageLate]];
        
        CAGradientLayer *absenceGradient = [Utils getGradientColorStatus:@"red"];
        absenceGradient.frame = self.absenceColorView.bounds;
        UIGraphicsBeginImageContext(absenceGradient.bounds.size);
        [absenceGradient renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *backgroundColorImageAbsence = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.absenceColorView.layer.cornerRadius = MAX(self.absenceColorView.frame.size.width, self.absenceColorView.frame.size.height)/2.0;
        self.absenceColorView.layer.masksToBounds = YES;
        [self.absenceColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImageAbsence]];
        
        CAGradientLayer *eventGradient = [Utils getGradientColorStatus:@"blue"];
        eventGradient.frame = self.eventColorView.bounds;
        UIGraphicsBeginImageContext(eventGradient.bounds.size);
        [eventGradient renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *backgroundColorImageEvent = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.eventColorView.layer.cornerRadius = MAX(self.eventColorView.frame.size.width, self.eventColorView.frame.size.height)/2.0;
        self.eventColorView.layer.masksToBounds = YES;
        [self.eventColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImageEvent]];
        
        CAGradientLayer *sickGradient = [Utils getGradientColorStatus:@"pink"];
        sickGradient.frame = self.sickColorView.bounds;
        UIGraphicsBeginImageContext(sickGradient.bounds.size);
        [sickGradient renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *backgroundColorImageSick = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.sickColorView.layer.cornerRadius = MAX(self.sickColorView.frame.size.width, self.sickColorView.frame.size.height)/2.0;
        self.sickColorView.layer.masksToBounds = YES;
        [self.sickColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImageSick]];
        
        CAGradientLayer *personalGradient = [Utils getGradientColorStatus:@"purple"];
        personalGradient.frame = self.personalColorView.bounds;
        UIGraphicsBeginImageContext(personalGradient.bounds.size);
        [personalGradient renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *backgroundColorImagePersonal = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.personalColorView.layer.cornerRadius = MAX(self.personalColorView.frame.size.width, self.personalColorView.frame.size.height)/2.0;
        self.personalColorView.layer.masksToBounds = YES;
        [self.personalColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImagePersonal]];
        
        CAGradientLayer *unCheckGradient = [Utils getGradientColorStatus:@"gray"];
        unCheckGradient.frame = self.uncheckColorView.bounds;
        UIGraphicsBeginImageContext(unCheckGradient.bounds.size);
        [unCheckGradient renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *backgroundColorImageUnCheck = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.uncheckColorView.layer.cornerRadius = MAX(self.uncheckColorView.frame.size.width, self.uncheckColorView.frame.size.height)/2.0;
        self.uncheckColorView.layer.masksToBounds = YES;
        [self.uncheckColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImageUnCheck]];
        
        CAGradientLayer *otherGradient = [Utils getGradientColorStatus:@"PurpleNeedle"];
        otherGradient.frame = self.otherColorView.bounds;
        UIGraphicsBeginImageContext(otherGradient.bounds.size);
        [otherGradient renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *backgroundColorImageOther = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.otherColorView.layer.cornerRadius = MAX(self.otherColorView.frame.size.width, self.otherColorView.frame.size.height)/2.0;
        self.otherColorView.layer.masksToBounds = YES;
        [self.otherColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImageOther]];
        
        [self setupGraphData:yData];
    }
}

#pragma mark - Pie Chart
- (void)setupPieChartView {
    self.pieChart.centerText = @"xxxx";
    self.pieChart.delegate = self;
    self.pieChart.usePercentValuesEnabled = YES;
    self.pieChart.drawSlicesUnderHoleEnabled = NO;
    //self.pieChart.drawSliceTextEnabled = NO;
    self.pieChart.holeRadiusPercent = 0.7;
    self.pieChart.transparentCircleRadiusPercent = 0.0;
    self.pieChart.chartDescription.enabled = NO;
    [self.pieChart setExtraOffsetsWithLeft:5.f top:10.f right:5.f bottom:5.f];
    self.pieChart.drawCenterTextEnabled = YES;
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    self.pieChart.drawHoleEnabled = YES;
    self.pieChart.rotationAngle = 0.0;
    self.pieChart.rotationEnabled = YES;
    self.pieChart.highlightPerTapEnabled = NO;
    self.pieChart.entryLabelColor = [UIColor whiteColor];
    ChartLegend *l = self.pieChart.legend;
    [l setEnabled:NO];
    
        if ([UserData getUserType] == STUDENT) {
            NSMutableAttributedString *centerText = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_REPORT_STUDENT",nil,[Utils getLanguage],nil)];
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
                [centerText setAttributes:@{
                                            NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:28.f],
                                            NSParagraphStyleAttributeName: paragraphStyle
                                            } range:NSMakeRange(0, centerText.length)];
            }else{
                [centerText setAttributes:@{
                                            NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:48.f],
                                            NSParagraphStyleAttributeName: paragraphStyle
                                            } range:NSMakeRange(0, centerText.length)];
            }
            self.pieChart.centerAttributedText = centerText;
        }else{
            NSMutableAttributedString *centerText = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_REPORT_ATTENDANCE",nil,[Utils getLanguage],nil)];
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
                [centerText setAttributes:@{
                NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:28.f],
                NSParagraphStyleAttributeName: paragraphStyle
                } range:NSMakeRange(0, centerText.length)];
            }else{
                [centerText setAttributes:@{
                NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:48.f],
                NSParagraphStyleAttributeName: paragraphStyle
                } range:NSMakeRange(0, centerText.length)];
            }
            self.pieChart.centerAttributedText = centerText;
        }
}

- (void)setupGraphData:(NSArray*)yData {
    if(yData.count == 0) {
        [self.pieChart setAlpha:0.0];
        return;
    }
    else {
        [self.pieChart setAlpha:1.0];
    }
    
    NSMutableArray *values = [[NSMutableArray alloc] init];
    
    // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
    for (int i = 0; i < yData.count; i++)
    {
        [values addObject:[[PieChartDataEntry alloc] initWithValue:([[yData objectAtIndex:i] integerValue]) label:@""]];//Show title in piechart
    }
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithEntries:values label:@""];
    dataSet.sliceSpace = 2.0;
    
    // add a lot of colors
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    [colors addObject:onTimeColor];
    [colors addObject:lateColor];
    [colors addObject:absenceColor];
    [colors addObject:sickColor];
    [colors addObject:personalColor];
    [colors addObject:eventColor];
    [colors addObject:unCheckColor];
    [colors addObject:otherColor];
    [colors addObjectsFromArray:ChartColorTemplates.vordiplom];
    [colors addObjectsFromArray:ChartColorTemplates.joyful];
    [colors addObjectsFromArray:ChartColorTemplates.colorful];
    [colors addObjectsFromArray:ChartColorTemplates.liberty];
    [colors addObjectsFromArray:ChartColorTemplates.pastel];
    [colors addObject:[UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f]];
    dataSet.colors = colors;
    PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
    [data setValueFont:[UIFont fontWithName:@"TH Sarabun New" size:0.0f]];
    [data setValueTextColor:UIColor.whiteColor];
    self.pieChart.data = data;
    [self.pieChart highlightValue:nil];
    [self.pieChart animateWithYAxisDuration:2.0 easingOption:ChartEasingOptionEaseOutCirc];
    
}

#pragma mark - Dialog
- (void)showDialogSelectMonthWithMonthDict:(NSDictionary *)monthsDict  monthsTermDataKeys:(NSArray *)monthsTermDataKeys {
    if(dialogSelectMonth != nil && [dialogSelectMonth isDialogShowing]) {
        [dialogSelectMonth dismissDialog];
    }
    [dialogSelectMonth showDialogInView:self.parentViewController.view stringDataDict:monthsDict dataDictKeys:monthsTermDataKeys];
}

- (void)showDialogSelectYearWithYearArray:(NSArray *)yearArray {
    if(dialogSelectYear != nil && [dialogSelectYear isDialogShowing]) {
        [dialogSelectYear dismissDialog];
    }
    [dialogSelectYear showDialogInView:self.parentViewController.view dataArr:yearArray];
}

- (void)showCreditBureauGraphDialogWithTitle:(NSString *)title status:(ATTENDSTATUS)status times:(NSInteger)times totalTimes:(NSInteger)totalTimes {
    if(self.creditBureauGraphDialog != nil && [self.creditBureauGraphDialog isDialogShowing]) {
        [self.creditBureauGraphDialog dismissDialog];
    }
    self.creditBureauGraphDialog = [[CreditBureauGraphDialog alloc] init];
//    UserViewController *rootController = (UserViewController*)[[(AppDelegate*)
//    [[UIApplication sharedApplication]delegate] window] rootViewController];
    [self.creditBureauGraphDialog showDialogInView:self.parentViewController.view title:title status:status times:times totalTimes:totalTimes];
}

#pragma mark - MKDropdownMenuDataSource
-(NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu {
    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component {
    return DateList.count;
}

#pragma mark - MKDropdownMenuDelegate
- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
  // [self declaredDialog];
    mKDropdownMenu = dropdownMenu;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        return 45;
    }else{
        return 70;
    }
   
    
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {
    NSString *textString;
    textString = selected_calendar;
    NSAttributedString *attributes;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:28], NSForegroundColorAttributeName: [UIColor blackColor] }];
    }else{
        attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:48], NSForegroundColorAttributeName: [UIColor blackColor] }];
    }
    return attributes;
}

-(NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {

    NSString *textString;
    textString = [DateList objectAtIndex:row];
    NSAttributedString *attributes;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:28], NSForegroundColorAttributeName: [UIColor blackColor] }];
    }else{
        attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:48], NSForegroundColorAttributeName: [UIColor blackColor] }];
    }
    
    return attributes;
}

- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
    [self.delegate removeMKDropdownMenu:dropdownMenu];
    return NO;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        selected_calendar = [DateList objectAtIndex:row];
        if (row == 0) {
            [self doSelectDayCalendar:@"week"];
        }else if (row == 1){
            [self doSelectDayCalendar:@"month"];
        }else if(row == 2){
            [self doSelectDayCalendar:@"year"];
        }else if (row == 3){
            [self doShowDialogSpecifyDate];
        }
        [dropdownMenu reloadAllComponents];
    }
    delay(0.0, ^{
        [dropdownMenu closeAllComponentsAnimated:YES];
    });
}
- (void) setDropdowe{
    // Set up dropdown
    self.dateTimeDropdown.dataSource = self;
    self.dateTimeDropdown.delegate = self;
    self.dateTimeDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.dateTimeDropdown.backgroundDimmingOpacity = 0;
    self.dateTimeDropdown.dropdownShowsTopRowSeparator = YES;
    self.dateTimeDropdown.dropdownCornerRadius = 5.0;
    self.dateTimeDropdown.tintColor = [UIColor blackColor];
   // self.dateTimeDropdown.layer.cornerRadius = 5;
    self.dateTimeDropdown.layer.masksToBounds = YES;
    
}

- (void) doShowDialogSpecifyDate{
    if(self.dialogSpecifyDate != nil && [self.dialogSpecifyDate isDialogShowing]) {
        [self.dialogSpecifyDate dismissDialog];
    }
    else {
        self.dialogSpecifyDate = [[specifyDateDialog alloc] init];
        self.dialogSpecifyDate.delegate = self;
    }
    [self.dialogSpecifyDate showDialogInView:self.parentViewController.view];
}

- (void) onAlertDialogConfirm:(NSDate *)startDate endDate:(NSDate *)endDate{
    [self updatePieChartDataWithStartDate:startDate endDate:endDate];
   
}


#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}


@end
