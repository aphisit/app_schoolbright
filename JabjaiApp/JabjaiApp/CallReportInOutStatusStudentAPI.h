//
//  CallReportInOutStatusStudentAPI.h
//  JabjaiApp
//
//  Created by toffee on 28/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReportInOutStatusStudentModel.h"
#import "ReportInOutStatusClassroomModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallReportInOutStatusStudentAPI;

@protocol CallReportInOutStatusStudentAPIDelegate <NSObject>
@optional

- (void)callReportInOutStatusStudentAPI:(CallReportInOutStatusStudentAPI *)classObj dataClassroom:(NSMutableArray<ReportInOutStatusClassroomModel *>*)dataClassroom success:(BOOL)success;
@end
@interface CallReportInOutStatusStudentAPI : NSObject
@property (nonatomic, weak) id<CallReportInOutStatusStudentAPIDelegate> delegate;

- (void)call:(long long)schoolId dayReports:(NSString*)dayReports levelId:(long long)levelId;
@end

NS_ASSUME_NONNULL_END
