//
//  CallMessageInUserAPI.h
//  JabjaiApp
//
//  Created by toffee on 3/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CallMessageInUserAPI;
@protocol CallMessageInUserAPIDelegate <NSObject>
@optional
- (void)callMessageInUserAPI:(CallMessageInUserAPI *)classObj userId:(NSInteger*)userId;
@end

@interface CallMessageInUserAPI : NSObject
@property (nonatomic, weak) id<CallMessageInUserAPIDelegate> delegate;

- (void)call:(NSString*)userIdMember messageId:(long long)messageId;
@end
