//
//  CallGetMenuListAPI.m
//  JabjaiApp
//
//  Created by toffee on 8/17/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallGetMenuListAPI.h"
#import "APIURL.h"
#import "Utils.h"

@implementation CallGetMenuListAPI{
    NSInteger connectCounter;
}
- (void)call:(long long)userId clientToken:(NSString *)clientToken schoolid:(long long)schoolid {
    connectCounter = 0;
    [self getAuthorizeMenu:userId clientToken:clientToken schoolid:schoolid];
}

- (void)getAuthorizeMenu:(long long)userId clientToken:(NSString *)clientToken schoolid:(long long)schoolid{
    NSString *urlString = [APIURL getAuthorizeMenu:userId clientToken:clientToken schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getAuthorizeMenu:userId clientToken:clientToken schoolid:schoolid];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSDictionary *returnedArray = returnedData;
            NSMutableArray *auThorizeArray = [[NSMutableArray alloc] init];
            //NSArray *dataDict = [returnedArray objectForKey:@"permission"];
            NSInteger resCode = [[returnedArray objectForKey:@"resCode"] integerValue];
           
            if(![[returnedArray objectForKey:@"permission"] isKindOfClass:[NSNull class]]) {
                 auThorizeArray = [returnedArray objectForKey:@"permission"];
            }


            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetMenuListAPI:data:resCode:success:)]) {
                [self.delegate callGetMenuListAPI:self data:auThorizeArray resCode:resCode success:YES];
            }
        }
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetMenuListAPI:data:resCode:success:)]) {
                [self.delegate callGetMenuListAPI:self data:nil resCode:nil success:NO];
            }
        }
    }];
}
     

@end
