//
//  JHTimeOutAndDetailViewController.h
//  JabjaiApp
//  Created by toffee on 11/23/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectOneDayCalendarViewController.h"
#import "YearTermModel.h"
#import "SWRevealViewController.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "JHSubjectModel.h"
#import "SlideMenuController.h"
#import "JHIconFileCollectionViewCell.h"
#import "PhotoAlbumLibary.h"
@interface JHTimeOutAndDetailViewController : UIViewController <  UITextFieldDelegate,UITextViewDelegate ,SelectOneDayCalendarDelegate, SlideMenuControllerDelegate, JHIconFileCollectionViewCellDelegate, PhotoAlbumLibaryDelegate>{
    UIDatePicker *datePicker;
}
//รับค่าจาก JHSelectStudenClassViewController
@property (nonatomic) NSMutableArray* levelId;
@property (nonatomic) NSMutableArray* studentId;
@property (nonatomic) NSMutableArray* studentName;
@property (nonatomic) NSInteger sendType;
@property (nonatomic) NSString* subjectId;
@property (nonatomic) NSString* subjectName;
@property (nonatomic) NSMutableArray* roomName;
@property (assign, nonatomic) long long classroomId;
@property (nonatomic) NSString* detail;
@property (nonatomic, retain) NSDate *startDay;
@property (nonatomic, retain) NSDate *endDay;
@property (nonatomic, retain) NSDate *alertDay;
@property (nonatomic,strong) NSString *alertTime;
@property (strong, nonatomic) NSMutableArray * imagesArray;
@property (strong, nonatomic) NSMutableArray *attachmentArray;

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classRoomArray;
@property (strong, nonatomic) NSArray<JHSubjectModel *> *sujectNameArrray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
- (id)initWithYearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict;
- (void)YearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerInformationLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerStartTime;
@property (weak, nonatomic) IBOutlet UILabel *headderEndTime;
@property (weak, nonatomic) IBOutlet UILabel *headerAlertDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAlertTimeLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerDescriptionLabel;


@property (weak, nonatomic) IBOutlet UILabel *headerFileAttachLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerAttachLabel;
@property (weak, nonatomic) IBOutlet UITextField *startTime;
@property (weak, nonatomic) IBOutlet UITextField *stopTime;
@property (weak, nonatomic) IBOutlet UITextField *alertDateHomeWork;
@property (weak, nonatomic) IBOutlet UITextField *alertTimeHomeWork;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *fileCollectionView;
@property (weak, nonatomic) IBOutlet UIView *shadowChooseImage;
@property (weak, nonatomic) IBOutlet UIView *shadowChooseFile;
@property (weak, nonatomic) IBOutlet UIView *shadowChooseDetail;

@property (weak, nonatomic) IBOutlet UIButton *selectImageBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectFileBtn;


- (IBAction)selectFile:(id)sender;
- (IBAction)selectImage:(id)sender;
- (IBAction)actionNext:(id)sender;
- (IBAction)moveBack:(id)sender;


@end
