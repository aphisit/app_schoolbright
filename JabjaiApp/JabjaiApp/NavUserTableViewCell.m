//
//  NavUserTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 8/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NavUserTableViewCell.h"

@implementation NavUserTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat max = MAX(self.imgUser.frame.size.width, self.imgUser.frame.size.height);
    self.imgUser.layer.cornerRadius = max / 2.0;
    self.imgUser.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
