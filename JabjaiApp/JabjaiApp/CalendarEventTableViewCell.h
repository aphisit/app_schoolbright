//
//  CalendarEventTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 11/20/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarEventTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *eventLabel;
@property (weak, nonatomic) IBOutlet UIView *colorStatusView;





@end
