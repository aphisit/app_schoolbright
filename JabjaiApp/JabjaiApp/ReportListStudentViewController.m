//
//  ReportListStudentViewController.m
//  JabjaiApp
//
//  Created by mac on 4/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportListStudentViewController.h"

#import "MenuReportTableViewCell.h"

#import "ReportInOutViewController.h"
#import "ReportLeaveViewController.h"
#import "ReportRefillMoneyViewController.h"
#import "ReportPurchasingViewController.h"
#import "ReportBehaviorScoreViewController.h"
#import "NavigationViewController.h"
#import "Utils.h"

@interface ReportListStudentViewController (){
    
    UIColor *grayColor;
    
}

@end

@implementation ReportListStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MenuReportTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    self.tableView.scrollEnabled = false;
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STUDENT_REPORT",nil,[Utils getLanguage],nil);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    //set color header
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if(indexPath.row == 0) {
        UIImage *myImage =[UIImage imageNamed:@"in_out"];
        cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_STUDENT_REPORT_STUDYING",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 1) {
        UIImage *myImage =[UIImage imageNamed:@"leave_report"];
        cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_STUDENT_LEAVE_REPORT",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 2) {
        UIImage *myImage =[UIImage imageNamed:@"coin"];
        cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_STUDENT_TOPUP_REPORT",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 3) {
        UIImage *myImage =[UIImage imageNamed:@"purchase"];
        cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_STUDENT_PURCHASING_REPORT",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 4) {
        UIImage *myImage =[UIImage imageNamed:@"people"];
        cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_STUDENT_BEHAVIORSCORE_REPORT",nil,[Utils getLanguage],nil);
    }
    [cell.BackgroundView layoutIfNeeded];
    [cell.BackgroundView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.BackgroundView.layer setShadowOpacity:0.3];
    [cell.BackgroundView.layer setShadowRadius:3.0];
    [cell.BackgroundView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0) {
        ReportInOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];

    }
    else if(indexPath.row == 1) {
        
        ReportLeaveViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportLeaveStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
     
    }
    else if(indexPath.row == 2) {
        
        ReportRefillMoneyViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportRefillMoneyStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
  
    }
    else if(indexPath.row == 3) {
        
        ReportPurchasingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportPurchasingStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
   
    }
    else if(indexPath.row == 4) {
        
        ReportBehaviorScoreViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportBehaviorScoreStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
    }
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 90;
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    // Here pass new size you need
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}
@end
