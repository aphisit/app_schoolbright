//
//  CallReportPurchasingWeekAPI.m
//  JabjaiApp
//
//  Created by toffee on 10/18/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallReportPurchasingWeekAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallReportPurchasingWeekAPI () {
    NSInteger connectCounter;
    
}
@end
@implementation CallReportPurchasingWeekAPI

- (void) call:(long long)schoolId dayStart:(NSString *)dateStart dayEnd:(NSString *)dayEnd{
    connectCounter = 0;
    [self getDataGraphPurchasing:schoolId dayStart:dateStart dayEnd:dayEnd];
}

#pragma mark - Get API Data
- (void)getDataGraphPurchasing:(long long)schoolId dayStart:(NSString *)dateStart dayEnd:(NSString *)dayEnd {
    
    NSString *URLString = [APIURL getDataPurchasingWeek:schoolId dayStart:dateStart dayEnd:dayEnd];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataGraphPurchasing:schoolId dayStart:dateStart dayEnd:dayEnd];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataGraphPurchasing:schoolId dayStart:dateStart dayEnd:dayEnd];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataGraphPurchasing:schoolId dayStart:dateStart dayEnd:dayEnd];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                NSMutableArray *dayArray;
                NSMutableArray *totalMoneyArray;
                
                if(returnedArray.count != 0) {
                    dayArray  = [[NSMutableArray alloc] init];
                    totalMoneyArray  = [[NSMutableArray alloc] init];
                    NSString *dateTime ;
                    for (int i = 0; i < returnedArray.count; i++) {
                        NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                        
                        if(![[dataDict objectForKey:@"Date"] isKindOfClass:[NSNull class]]) {
                            
                            dateTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Date"]];
                            NSArray *cutDateTime = [dateTime componentsSeparatedByString:@"T"];
                            [dayArray addObject:[cutDateTime objectAtIndex:0]];
                            
                        }
                        else {
                            dateTime = [[NSMutableString alloc] initWithString:@""];
                        }
                        
                        NSInteger totalMoney = [[dataDict objectForKey:@"TotatlMoney"] integerValue];
                        [totalMoneyArray addObject:[NSString stringWithFormat:@"%d",totalMoney]];
                        NSLog(@"xxxx");
                    }
                    
                    
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportPurchasingWeekAPI:dayArray:totalMoneyArray:success:)]) {
                    [self.delegate callReportPurchasingWeekAPI:self dayArray:dayArray totalMoneyArray:totalMoneyArray success:YES];
                }
                
                
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportPurchasingWeekAPI:dayArray:totalMoneyArray:success:)]) {
                    [self.delegate callReportPurchasingWeekAPI:self dayArray:nil totalMoneyArray:nil success:NO];
                }
            }
            
        }
        
    }];
    
}

@end
