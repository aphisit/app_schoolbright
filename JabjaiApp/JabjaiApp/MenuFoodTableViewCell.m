//
//  MenuFoodTableViewCell.m
//  JabjaiApp
//
//  Created by toffee on 4/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "MenuFoodTableViewCell.h"

@implementation MenuFoodTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (IBAction)actionAddProductButton:(id)sender {
    if(_delegate != nil && [self.delegate respondsToSelector:@selector(processNumberProduct:atIndex:status:)]) {
        [self.delegate processNumberProduct:self atIndex:_index status:@"ADD"];
    }
}
- (IBAction)actionDeleteProductButton:(id)sender {
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(processNumberProduct:atIndex:status:)]) {
        [self.delegate processNumberProduct:self atIndex:_index status:@"DELETE"];
    }
}
@end
