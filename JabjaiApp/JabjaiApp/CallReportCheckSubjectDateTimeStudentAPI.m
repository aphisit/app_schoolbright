//
//  CallReportCheckSubjectDateTimeStudentAPI.m
//  JabjaiApp
//
//  Created by toffee on 23/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallReportCheckSubjectDateTimeStudentAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallReportCheckSubjectDateTimeStudentAPI () {
    NSInteger connectCounter;
    NSMutableArray<RPCheckNameSubjectDateTimeModel *> *dateTimeArray;
}
@end
@implementation CallReportCheckSubjectDateTimeStudentAPI



-(void)call:(NSString*)studentId date:(NSString *)date subjectId:(NSString*)subjectId schoolId:(long long)schoolId{
    connectCounter = 0;
    [self getDataCheckSubjectDateTime:studentId date:date subjectId:subjectId schoolId:schoolId];
}

-(void)getDataCheckSubjectDateTime:(NSString*)studentId date:(NSString *)date subjectId:(NSString*)subjectId schoolId:(long long)schoolId{
    
    NSString *URLString = [APIURL getDataReportCheckSubjectDateTime:studentId date:date subjectId:subjectId schoolId:schoolId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataCheckSubjectDateTime:studentId date:date subjectId:subjectId schoolId:schoolId];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataCheckSubjectDateTime:studentId date:date subjectId:subjectId schoolId:schoolId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataCheckSubjectDateTime:studentId date:date subjectId:subjectId schoolId:schoolId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                NSMutableString *date, *time, *status ;
                if(dateTimeArray != nil) {
                    [dateTimeArray removeAllObjects];
                    dateTimeArray = nil;
                }
                dateTimeArray = [[NSMutableArray alloc] init];
                for (int i = 0; i<returnedArray.count; i++) {
                    NSDictionary* dataDict = [returnedArray objectAtIndex:i];
                    
                    if(![[dataDict objectForKey:@"Date"] isKindOfClass:[NSNull class]]) {
                        
                        date = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"LogDate"]];    
                    }
                    else {
                        date = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    
                    if(![[dataDict objectForKey:@"LogTime"] isKindOfClass:[NSNull class]]) {
                        time =  [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"LogTime"]];
                    }
                    else {
                        time = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"LogStatus"] isKindOfClass:[NSNull class]]) {
                        status = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"LogStatus"]];
                    }
                    else {
                        status = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) date);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) time);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) status);
                    
                   
                    
                    RPCheckNameSubjectDateTimeModel *model = [[RPCheckNameSubjectDateTimeModel alloc]init];
                    [model setDate:date];
                    [model setTime:time];
                    [model setStatus:status];
                    [dateTimeArray addObject:model];
                }
                connectCounter = 0;
               
               
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportCheckSubjectDateTimeStudentAPI:dateTimeArray:success:)]) {
                    [self.delegate callReportCheckSubjectDateTimeStudentAPI:self dateTimeArray:dateTimeArray success:YES];
                }
                
                
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportCheckSubjectDateTimeStudentAPI:dateTimeArray:success:)]) {
                    [self.delegate callReportCheckSubjectDateTimeStudentAPI:self dateTimeArray:dateTimeArray success:NO];
                }
            }
            
        }
        
    }];
    
    
}

@end
