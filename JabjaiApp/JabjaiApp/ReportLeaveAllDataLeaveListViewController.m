//
//  ReportLeaveAllDataLeaveListViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportLeaveAllDataLeaveListViewController.h"
#import "ReportLeaveAllDataPersonListViewController.h"

@interface ReportLeaveAllDataLeaveListViewController ()

@end

@implementation ReportLeaveAllDataLeaveListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)moveBack:(id)sender {
    
    ReportLeaveAllDataPersonListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportLeaveAllDataPersonListStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
