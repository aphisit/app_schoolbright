//
//  ReportOrderHomeworkDetailModel.h
//  JabjaiApp
//
//  Created by mac on 5/2/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportOrderHomeworkDetailModel : NSObject

@property (nonatomic) long long homeworkID;
@property (nonatomic, strong) NSString *subjectTitle;
@property (nonatomic, strong) NSString *sender;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSDate *dateSend;
@property (nonatomic, strong) NSDate *dateStart;
@property (nonatomic, strong) NSDate *dateNoti;
@property (nonatomic, strong) NSDate *dateEnd;

@property (nonatomic) int all;
@property (nonatomic) int read;
@property (nonatomic) int unread;

@end
