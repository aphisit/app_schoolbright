//
//  RFSpecifyNumberMoneyViewController.h
//  JabjaiApp
//
//  Created by toffee on 17/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class RFSpecifyNumberMoneyViewController;
@protocol RFSpecifyNumberMoneyViewControllerDelegate <NSObject>
@optional
- (void)confirmNumberMoney:(RFSpecifyNumberMoneyViewController *)SpecifyNumberMoneyDialog amountMoney:(NSInteger)amountMoney;
@end
@interface RFSpecifyNumberMoneyViewController : UIViewController <UITextFieldDelegate>
@property (nonatomic, retain) id<RFSpecifyNumberMoneyViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *specifyAmountLable;
@property (weak, nonatomic) IBOutlet UILabel *showUnNumberLable;

@property (weak, nonatomic) IBOutlet UITextField *amountMoneyTextField;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewCenterY;

- (IBAction)confirmAction:(id)sender;
- (IBAction)cancelAction:(id)sender;


- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end

NS_ASSUME_NONNULL_END
