//
//  ReportCheckNameSubjectDateTimeTableViewCell.m
//  JabjaiApp
//
//  Created by toffee on 24/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportCheckNameSubjectDateTimeTableViewCell.h"
#import "Utils.h"
@interface ReportCheckNameSubjectDateTimeTableViewCell () {
   
    
    CAGradientLayer *gradient;
    
}
@end
@implementation ReportCheckNameSubjectDateTimeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat max = MAX(self.colorStatus.frame.size.width, self.colorStatus.frame.size.height);
    self.colorStatus.layer.cornerRadius = max/2.0;
    self.colorStatus.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateStatus:(int)status {
    gradient = [CAGradientLayer layer];
    NSLog(@"numberStatusButton = %d",status);
    UIImage * backgroundColorImage;
   
    switch (status) {
        case 0:
            
            gradient = [Utils getGradientColorStatus:@"green"];
            gradient.frame = self.colorStatus.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorStatus setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            
            break;
        case 1:
            gradient = [Utils getGradientColorStatus:@"yellow"];
            gradient.frame = self.colorStatus.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorStatus setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 3:
            gradient = [Utils getGradientColorStatus:@"red"];
            gradient.frame = self.colorStatus.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorStatus setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 10:
            gradient = [Utils getGradientColorStatus:@"pink"];
            gradient.frame = self.colorStatus.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorStatus setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 11:
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.colorStatus.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorStatus setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 12:
            gradient = [Utils getGradientColorStatus:@"gray"];
            gradient.frame = self.colorStatus.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorStatus setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
     
        default:
            gradient = [Utils getGradientColorStatus:@"gray"];
            gradient.frame = self.colorStatus.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorStatus setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
    }
    
}

- (void)updateDateTimeLabel:(NSString *)dateTime status:(NSString *)status{
    
        NSDateFormatter *dateFormatter = [Utils getDateFormatter];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:dateTime];
    
        NSString *dateTH = [Utils getThaiDateFormatWithDate:date];
    
    switch ([status intValue]) {
        case 0:
            
            self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@",dateTH,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ONTIME",nil,[Utils getLanguage],nil)];
            
            break;
        case 1:
            self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@",dateTH,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_LATE",nil,[Utils getLanguage],nil)];
            break;
        case 3:
            self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@",dateTH,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ABSENCE",nil,[Utils getLanguage],nil)];
            break;
        case 10:
            self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@",dateTH,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PERSONAL",nil,[Utils getLanguage],nil)];
            break;
        case 11:
            self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@",dateTH,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_SICK",nil,[Utils getLanguage],nil)];
            break;
        case 12:
            self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@",dateTH,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_UNDEFINED",nil,[Utils getLanguage],nil)];
            break;
            
        default:
             self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@",dateTH,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_UNDEFINED",nil,[Utils getLanguage],nil)];
            break;
    
    }
}



@end
