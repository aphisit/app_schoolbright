//
//  CallPayMonnyPOSTAPI.m
//  JabjaiApp
//
//  Created by toffee on 7/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallPayMonnyPOSTAPI.h"
#import "APIURL.h"
#import "UserData.h"
#import <AFNetworking/AFNetworking.h>

@implementation CallPayMonnyPOSTAPI
-(void)call:(NSString *)jsonString shopId:(long long)shopId {
    [self doConfirmPinCode:jsonString shopId:shopId ];
    
}
- (void)doConfirmPinCode:(NSString*)jsonString shopId:(long long)shopId{
    //NSString *URLString = [APIURL getConfirmPinCodePOST];
    NSString *URLString = [APIURL getConfirmPinProductPOST:[UserData getSchoolId] shopId:shopId];
    NSError* error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableLeaves error:&error];
    if(error != nil) {
        NSLog(@"%@", @"Parse JSON data failed");
    }
    else {
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"responseObject = %@", responseObject);
            NSString  *status = [responseObject objectForKey:@"status"] ;
            if(self.delegate && [self.delegate respondsToSelector:@selector(callPayMonnyPOSTAPI:status:success:)]) {
                [self.delegate callPayMonnyPOSTAPI:self status:status success:YES];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callPayMonnyPOSTAPI:status:success:)]) {
                [self.delegate callPayMonnyPOSTAPI:self status:nil success:NO];
            }
            
        }];
        
    }

}

@end
