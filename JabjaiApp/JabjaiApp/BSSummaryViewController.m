//
//  BSSummaryViewController.m
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.


#import "BSSummaryViewController.h"
#import "BSSelectClassLevelViewController.h"
#import "BSSelectBehaviorScoreViewController.h"
#import "BSLeftRightLabelTableViewCell.h"
#import "UserData.h"
#import "Constant.h"
#import "Utils.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface BSSummaryViewController () {
    
    NSMutableArray<BSSelectedStudent *> *allSelectedStudentsArray;
    NSMutableArray<BehaviorScoreModel *> *allSelectedBehaviorScoreArray;
    UIColor *greenBGColor;
    UIColor *redBGColor;
    BOOL updateSuccess;
    
    // The dialog
    AlertDialogConfirm *alertDialog;
    AlertDialog *mistakeAlertDialog;
    
    NSBundle *myLangBundle;
    CAGradientLayer *gradient;
    
    double heightListUserView,heightBehaviorScoreView,heightImgView;
}
@property (strong, nonatomic) CallUpdateBehaviorScorePOSTAPI *callUpdateBehaviorScorePOSTAPI;
@end

static NSString *cellIdentifier = @"LeftRightCell";
static NSString *cellListImageUser = @"Cell";
static NSString *cellImgIdentifier = @"Cell";

@implementation BSSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //if Used give delete
    self.imageBehaviorScoreView.hidden = YES;
    self.imageCollectionView.hidden = YES;
    self.headerImageLabel.hidden = YES;
    self.IconImage.hidden = YES;
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    [self.tableViewBehaviorScore layoutIfNeeded];
    heightListUserView = self.heightUserConstraintView.constant;//get height constraint view of list user
    //heightBehaviorScoreView = self.tableViewBehaviorScore.frame.size.height;//get height tableview of list Behavior Score
    heightBehaviorScoreView =  self.heightTableViewScoreConstraint.constant;
    [self.imageBehaviorScoreView layoutIfNeeded];
    heightImgView = self.imageBehaviorScoreView.frame.size.height; //get height imagecollectionview
    
    self.tableViewListOfNames.dataSource = self;
    self.tableViewListOfNames.delegate = self;
    self.tableViewListOfNames.tableFooterView = [[UIView alloc] init];
    self.tableViewListOfNames.separatorInset = UIEdgeInsetsZero;
    
    self.tableViewBehaviorScore.dataSource = self;
    self.tableViewBehaviorScore.delegate = self;
    self.tableViewBehaviorScore.tableFooterView = [[UIView alloc] init];
    self.tableViewBehaviorScore.separatorInset = UIEdgeInsetsZero;
    
    [self.tableViewListOfNames registerNib:[UINib nibWithNibName:NSStringFromClass([BSLeftRightLabelTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self.tableViewListOfNames registerNib:[UINib nibWithNibName:NSStringFromClass([BSListNameUserSummaryTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellListImageUser];
    
    [self.tableViewBehaviorScore registerNib:[UINib nibWithNibName:NSStringFromClass([BSLeftRightLabelTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    
    [self.imageCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([BSSummaryImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellImgIdentifier];
    self.imageCollectionView.delegate = self;
    self.imageCollectionView.dataSource = self;
  
    
    greenBGColor = [UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0];
    redBGColor = [UIColor colorWithRed:220/255.0 green:43/255.0 blue:69/255.0 alpha:1.0];
    
    updateSuccess = NO;
    [self createSelectedData];
}
    
- (void) viewDidLayoutSubviews{
    [self setLanguage ];
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];

    self.heightUserConstraintView.constant =  heightListUserView * allSelectedStudentsArray.count;
    //[self.tableViewListOfNames setFrame:CGRectMake(0, 0, self.tableViewListOfNames.frame.size.width, heightListUserView * allSelectedStudentsArray.count)];
    
    self.heightTableViewScoreConstraint.constant = (heightBehaviorScoreView * allSelectedBehaviorScoreArray.count);
    //[self.tableViewBehaviorScore setFrame:CGRectMake(0, 0, self.tableViewBehaviorScore.frame.size.width, heightBehaviorScoreView * allSelectedBehaviorScoreArray.count)];
    
    
    int roundedUp = 1;
    if (self.imageArray.count>3) {
        float a = (float)self.imageArray.count/3;
         roundedUp = ceil(a);
    }
    
    self.heightImgCollectionViewConstraintView.constant = (heightImgView * roundedUp)-heightImgView;
    
    [self.listNameView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.listNameView.layer setShadowOpacity:0.3];
    [self.listNameView.layer setShadowRadius:3.0];
    [self.listNameView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.behaviorScoreView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.behaviorScoreView.layer setShadowOpacity:0.3];
    [self.behaviorScoreView.layer setShadowRadius:3.0];
    [self.behaviorScoreView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.imageBehaviorScoreView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.imageBehaviorScoreView.layer setShadowOpacity:0.3];
    [self.imageBehaviorScoreView.layer setShadowRadius:3.0];
    [self.imageBehaviorScoreView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setLanguage{
    
    [self.submitButton layoutIfNeeded];
    if(_mode == BS_MODE_ADD) {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_ADD",nil,myLangBundle,nil);
        
        gradient = [Utils getGradientColorStatus:@"green"];
        gradient.frame = self.submitButton.bounds;
        [self.submitButton.layer insertSublayer:gradient atIndex:0];
       // [self.submitButton setBackgroundColor:greenBGColor];
    }
    else {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REDUCE",nil,myLangBundle,nil);
        
        gradient = [Utils getGradientColorStatus:@"red"];
        gradient.frame = self.submitButton.bounds;
        [self.submitButton.layer insertSublayer:gradient atIndex:0];
       // [self.submitButton setBackgroundColor:redBGColor];
    }
    
    self.headerListNameLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_LISTNAME",nil,myLangBundle,nil);
    self.headerBehaviorLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR",nil,myLangBundle,nil);
    self.headerTotalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_TOTAL",nil,myLangBundle,nil);
    
    [self.submitButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_BEHAVIOR_SUBMIT",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
}
#pragma mark - UICollectionDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
    BSSummaryImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellImgIdentifier forIndexPath:indexPath];
    cell.behaviorScoreImage.image = [self.imageArray objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize defaultSize = CGSizeMake((self.imageCollectionView.frame.size.width/3)-10, (heightImgView));
    return defaultSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView.tag == 1) { // tableViewListOfNames
        
        if(allSelectedStudentsArray != nil) {
            return allSelectedStudentsArray.count;
        }
    }
    else if(tableView.tag == 2) { // tableViewBehaviorScore
        
        if(allSelectedBehaviorScoreArray != nil) {
            return allSelectedBehaviorScoreArray.count;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView.tag == 1) {
         BSListNameUserSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellListImageUser forIndexPath:indexPath];
        
        BSSelectedStudent *model = [allSelectedStudentsArray objectAtIndex:indexPath.row];
        cell.rankLabel.text =[NSString stringWithFormat:@"%d.",indexPath.row+1];
        cell.usernameLabel.text = [model getStudentName];
        
        if ([model getStudentPic] == NULL || [[model getStudentPic] isEqual:@""]) {
            cell.imageUser.image = [UIImage imageNamed:@"ic_user_info"];
        }
        else{
            [cell.imageUser sd_setImageWithURL:[NSURL URLWithString:[model getStudentPic]]];
        }
        [cell.imageUser layoutIfNeeded];
        cell.imageUser.layer.cornerRadius = cell.imageUser.frame.size.height /2;
        cell.imageUser.layer.masksToBounds = YES;
        cell.imageUser.layer.borderWidth = 0;

        
        return cell;
    }
    else if(tableView.tag == 2) {
         BSLeftRightLabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        BehaviorScoreModel *model = [allSelectedBehaviorScoreArray objectAtIndex:indexPath.row];
        
        cell.LeftLabel.text = [[NSString alloc] initWithFormat:@"%d. %@", indexPath.row + 1, [model getBehaviorScoreName]];
        [cell.RightLabel setAlpha:1.0f];
        
        if([model getBehaviorScoreType] == 0) {
            cell.RightLabel.text = [[NSString alloc] initWithFormat:@"+%i", (int)[model getScore]];
        }
        else {
            cell.RightLabel.text = [[NSString alloc] initWithFormat:@"-%i", (int)[model getScore]];
        }
        
        return cell;
    }
    
    return [UITableViewCell new];
}

//#pragma mark - UITableViewDelegate
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
//    {
//
//        return 100; /* Device is iPad */
//    }else{
//
//        return 55;
//    }
//}



#pragma mark - API Caller

- (void)updateStudentBehaviorScoreWithJSONString:(NSString *)jsonString {
    
    if(self.callUpdateBehaviorScorePOSTAPI != nil) {
        self.callUpdateBehaviorScorePOSTAPI = nil;
    }
    self.callUpdateBehaviorScorePOSTAPI = [[CallUpdateBehaviorScorePOSTAPI alloc] init];
    self.callUpdateBehaviorScorePOSTAPI.delegate = self;
    [self.callUpdateBehaviorScorePOSTAPI call:[UserData getSchoolId] teacherId:[UserData getUserID] jsonString:jsonString];
    NSLog(@"JSON = %@",jsonString);
    [self showIndicator];
}

#pragma mark - API Delegate
- (void)callUpdateBehaviorScorePOSTAPI:(CallUpdateBehaviorScorePOSTAPI *)classObj response:(NSString *)response success:(BOOL)success {
    [self stopIndicator];
    updateSuccess = success;
    if(success) {
        if ([response isEqualToString:@"1"]) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_COMPLETED",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }else{
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_SCHOOL_HOLIDAYS",nil,myLangBundle,nil);
            [self showAlertMistakeDialogWithMessage:alertMessage];
        }
    }
    else {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_NO_COMPLETED",nil,myLangBundle,nil);
        [self showAlertMistakeDialogWithMessage:alertMessage];
    }
}

#pragma mark - Dialog
- (void)showAlertDialogWithMessage:(NSString *)message {
    if(alertDialog != nil) {
        if([alertDialog isDialogShowing]) {
            [alertDialog dismissDialog];
        }
    }
    else {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}
- (void)showAlertMistakeDialogWithMessage:(NSString *)message {
    
    if(mistakeAlertDialog != nil) {
        if([mistakeAlertDialog isDialogShowing]) {
            [mistakeAlertDialog dismissDialog];
        }
    }
    else {
        mistakeAlertDialog = [[AlertDialog alloc] init];
        mistakeAlertDialog.delegate = self;
    }
    [mistakeAlertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)onAlertDialogClose {
    if(updateSuccess) {
        [self gotoBSSelectClassLevelViewController];
    }
}

#pragma mark - Utility
- (void)createSelectedData {
    
    allSelectedStudentsArray = [[NSMutableArray alloc] init];
    allSelectedBehaviorScoreArray = [[NSMutableArray alloc] init];
    
    if(self.selectedStudentArray != nil) {
        for(BSSelectedStudent *model in self.selectedStudentArray) {
            if([model isSelected]) {
                [allSelectedStudentsArray addObject:model];
            }
        }
    }
    NSInteger totalBehaviorScore = 0;
    if(self.behaviorScoreArray != nil) {
        for(BehaviorScoreModel *model in self.behaviorScoreArray) {
            if([model isSelected]) {
                [allSelectedBehaviorScoreArray addObject:model];
                
                if([model getBehaviorScoreType] == 0) { // type add
                    totalBehaviorScore += [model getScore];
                }
                else {
                    totalBehaviorScore -= [model getScore];
                }
            }
        }
    }
    
    if(totalBehaviorScore > 0) {
        self.totalScoreLabel.text = [[NSString alloc] initWithFormat:@"+%ld", totalBehaviorScore];
    }
    else {
        self.totalScoreLabel.text = [[NSString alloc] initWithFormat:@"%ld", totalBehaviorScore];
    }
    
    [self.tableViewListOfNames reloadData];
    [self.tableViewBehaviorScore reloadData];
}

- (void)gotoBSSelectClassLevelViewController {
    BSSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectClassLevelStoryboard"];
    
    viewController.mode = _mode;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Action functions
- (IBAction)actionSubmit:(id)sender {
    _submitBtn.enabled = NO;
    if(allSelectedStudentsArray != nil && allSelectedBehaviorScoreArray != nil) {
        NSString *jsonString = [Utils getStudentsBehaviorsScoreJSONWithSelectedStudentArray:allSelectedStudentsArray selectedBehaviorsScoreArray:allSelectedBehaviorScoreArray];
        NSLog(@"%@", jsonString);
        [self updateStudentBehaviorScoreWithJSONString:jsonString];
    }
}

- (IBAction)moveBack:(id)sender {
    BSSelectBehaviorScoreViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectBehaviorScoreStoryboard"];
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    viewController.status = self.status;
    viewController.selectedStudentArray = _selectedStudentArray;
    viewController.behaviorScoreArray = _behaviorScoreArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}



@end
