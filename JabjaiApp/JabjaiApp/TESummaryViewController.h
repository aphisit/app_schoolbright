//
//  TESummaryViewController.h
//  JabjaiApp
//
//  Created by toffee on 11/14/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallTEUpdateStudentStatusPOSTAPI.h"
#import "AlertDialog.h"
#import "TAStudentStatusModel.h"
#import "SWRevealViewController.h"

#import "SlideMenuController.h"

@interface TESummaryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CallTEUpdateStudentStatusPOSTAPIDelegate, AlertDialogDelegate, SlideMenuControllerDelegate>

// TAStudentListViewController
@property (strong, nonatomic) NSArray<TAStudentStatusModel *> *studentStatusArray;
@property (nonatomic) NSInteger mode;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;



@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *SummaryDetailBTN;

- (IBAction)moveBack:(id)sender;
- (IBAction)actionSubmit:(id)sender;

@end
