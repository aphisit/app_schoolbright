//
//  MessageBoxScrollableTextDialog.h
//  JabjaiApp
//
//  Created by mac on 11/24/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageInboxScrollableTextDialogCollectionViewCell.h"
#import "JHReadFileViewController.h"

@protocol MessageInBoxScrollableTextDialogDelegate <NSObject>

@optional
-(void)messageInBoxScrollableTextClose;

@end

@interface MessageInBoxScrollableTextDialog : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, retain) id<MessageInBoxScrollableTextDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageTextView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

- (IBAction)closeDialog:(id)sender;

-(void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message messageDate:(NSDate *)messageDate messageID:(long long)messageID;
-(void)dismissDialog;
-(BOOL)isDialogShowing;

@end
