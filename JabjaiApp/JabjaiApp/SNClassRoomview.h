//
//  SNClassRoomview.h
//  JabjaiApp
//
//  Created by toffee on 9/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
@class SNClassRoomview;
@protocol SNClassRoomviewRadioTableViewCellDelegate <NSObject>
- (void)onPressRadioButton:(SNClassRoomview *)tableViewCell atIndex:(NSInteger)index roomID:(NSInteger)roomID tag:(NSInteger)tag roomString:(NSString*)roomString;
@end
@interface SNClassRoomview : UITableViewCell
@property (weak, nonatomic) id<SNClassRoomviewRadioTableViewCellDelegate> delegate;
@property (nonatomic) NSInteger index;
- (void)setRadioButtonSelected;
- (void)setRadioButtonSelectedWithType:(int) type;
- (void)setRadioButtonClearSelected;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *radioButton;
- (IBAction)actionPressRadioButton:(id)sender;


@end
