//
//  CallStudentTimeTableAPI.m
//  JabjaiApp
//
//  Created by toffee on 16/5/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallStudentTimeTableAPI.h"
#import "UserData.h"
#import "Utils.h"
#import "APIURL.h"
@implementation CallStudentTimeTableAPI{
    NSInteger connectCounter;
    NSMutableArray<SSubjectModel *> *subjectArray;
}

- (void)call:(long long)userId date:(NSDate *)date schoolid:(long long)schoolid{
    connectCounter = 0;
    [self getSubjectOfdate:userId date:date schoolid:schoolid];
}

- (void)getSubjectOfdate:(long long)userId date:(NSDate*)date schoolid:(long long)schoolid{
    NSString *URLString = [APIURL getStudentScheduleListURLWithUserID:userId date:date schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSubjectOfdate:userId date:date schoolid:schoolid];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubjectOfdate:userId date:date schoolid:schoolid];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubjectOfdate:userId date:date schoolid:schoolid];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(subjectArray != nil) {
                    [subjectArray removeAllObjects];
                    subjectArray = nil;
                }
                
                subjectArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long subjectID = [[dataDict objectForKey:@"ScheduleId"] longLongValue];
                    
                    NSMutableString *subjectName, *startTime, *endTime;
                    
                    if(![[dataDict objectForKey:@"ScheduleName"] isKindOfClass:[NSNull class]]) {
                        subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScheduleName"]];
                    }
                    else {
                        subjectName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TimeIn"] isKindOfClass:[NSNull class]]) {
                        startTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeIn"]];
                    }
                    else {
                        startTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TimeOut"] isKindOfClass:[NSNull class]]) {
                        endTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeOut"]];
                    }
                    else {
                        endTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endTime);
                    
                    SSubjectModel *model = [[SSubjectModel alloc] init];
                    model.subjectID = subjectID;
                    model.subjectName = subjectName;
                    model.startTime = startTime;
                    model.endTime = endTime;
                    
                    [subjectArray addObject:model];
                    
                }
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callStudentTimeTableAPI:data:success:)]) {
                    [self.delegate callStudentTimeTableAPI:self data:subjectArray success:YES];
                }
                
                
            }
             if(isFail) {
                 if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callStudentTimeTableAPI:data:success:)]) {
                     [self.delegate callStudentTimeTableAPI:self data:subjectArray success:NO];
                 }
             }
        }
        
    }];
}

@end
