//
//  TEReportSelectClassLevelViewController.h
//  JabjaiApp
//
//  Created by toffee on 11/15/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAStudentStatusModel.h"
#import "SchoolClassroomModel.h"
#import "SchoolLevelModel.h"
#import "TableListDialog.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "SWRevealViewController.h"
#import "TAStudentStatusModel.h"
#import "TAStudentStatusTableViewCell.h"

#import "SlideMenuController.h"

@interface TEReportSelectClassLevelViewController : UIViewController<UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate,TAStudentStatusTableViewCellDelegate, SlideMenuControllerDelegate>{}
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (strong, nonatomic) NSArray<TAStudentStatusModel *> *selectedStudentArray;
@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classRoomTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *headerView;


@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
- (IBAction)actionNext:(id)sender;
- (IBAction)openDrawer:(id)sender;


@end
