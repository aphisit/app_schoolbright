//
//  BSPadNumberBehavioViewController.m
//  JabjaiApp
//
//  Created by toffee on 24/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "BSPadNumberBehavioViewController.h"
#import "Utils.h"
#import "UserData.h"
@interface BSPadNumberBehavioViewController (){
    NSMutableString *idStudentStr ;
    TEDataStudentScanerModel *userData;
    NSArray *listStudentArray;
    AlertDialog *alertDialog;
}
@property (strong, nonatomic) CallBSGetDataStudentScanBehaviorAPI *callBSGetDataStudentScanBehaviorAPI;
@property (strong, nonatomic) CallTEConfirmScanerBarcode *callTEConfirmScanerBarcode;
@property (strong, nonatomic) BSScanerStudentDataDialog *dialogConfirmScaner;
@end

@implementation BSPadNumberBehavioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    idStudentStr = [NSMutableString string];
    // Do any additional setup after loading the view.
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    [self.nextBtn layoutIfNeeded];
    CAGradientLayer *gradient, *gradientHeader;
    
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    if(_mode == BS_MODE_ADD) {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_ADD",nil,[Utils getLanguage],nil);
        self.headerTypeBehavioLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_ADD_SCORE_USERID",nil,[Utils getLanguage],nil);
        gradient = [Utils getGradientColorStatus:@"green"];
        gradient.frame = self.nextBtn.bounds;
        [self.nextBtn.layer insertSublayer:gradient atIndex:0];
    }
    else {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REDUCE",nil,[Utils getLanguage],nil);
        self.headerTypeBehavioLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REDUCE_SCORE_USERID",nil,[Utils getLanguage],nil);
        gradient = [Utils getGradientColorStatus:@"red"];
        gradient.frame = self.nextBtn.bounds;
        [self.nextBtn.layer insertSublayer:gradient atIndex:0];
    }
    [self.headerClearBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_CLEAR",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    self.idStudentTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_BEHAVIOR_STUDENTID",nil,[Utils getLanguage],nil);
    [self.nextBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_BEHAVIOR_NEXT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

#pragma mark - CallBSGetDataStudentScanBehaviorAPI
//call get data student
- (void)doCallBSGetDataStudentScanBehaviorAPI:(long long)schoolId idStudent:(NSString *)idStudent {
    [self showIndicator];
    if(self.callBSGetDataStudentScanBehaviorAPI != nil) {
        self.callBSGetDataStudentScanBehaviorAPI = nil;
    }
    self.callBSGetDataStudentScanBehaviorAPI = [[CallBSGetDataStudentScanBehaviorAPI alloc] init];
    self.callBSGetDataStudentScanBehaviorAPI.delegate = self;
    [self.callBSGetDataStudentScanBehaviorAPI call:schoolId idStudent:idStudent];
}

- (void)callBSGetDataStudentScanBarcodeAPI:(CallBSGetDataStudentScanBehaviorAPI *)classObj data:(BSDataStudentScanerModel *)data resCode:(NSInteger)resCode success:(BOOL)success{
    [self stopIndicator];
    if (success && resCode == 200) {
        [self showAlertDialogConfirmScaner:data];
    }else{
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_USER_NOTFOUND",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
    }
}
#pragma mark - BSScanerStudentDataDialog
//show dialog confirmScaner
- (void)showAlertDialogConfirmScaner:(BSDataStudentScanerModel *)data {
    if(self.dialogConfirmScaner != nil && [self.dialogConfirmScaner isDialogShowing]) {
        [self.dialogConfirmScaner dismissDialog];
    }
    else {
        self.dialogConfirmScaner = [[BSScanerStudentDataDialog alloc] init];
        self.dialogConfirmScaner.delegate = self;
    }
        [self.dialogConfirmScaner showDialogInView:self.view data:data mode:self.mode];// 1 Add score /  2 Cut score
}
- (void) confirmResponse:(BSDataStudentScanerModel *)userData{//revert Object
    BSSelectedStudent *model = [[BSSelectedStudent alloc] init];
    NSString *studentName = [NSString stringWithFormat:@"%@ %@",[userData getStudentFirstName], [userData getStudentLastName]];
    [model setStudentName:studentName];
    [model setPic:[userData getStudentPicture]];
    [model setStudentId:[userData getStudentID]];
    [model setSelected:YES];
    NSArray<BSSelectedStudent*> *shoppingList = @[model];
    BSSelectBehaviorScoreViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectBehaviorScoreStoryboard"];
        viewController.status  = self.status;
        viewController.mode = self.mode;
        viewController.selectedStudentArray = shoppingList;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

#pragma action when press Clear text
- (void)onAlertDialogClose{
    idStudentStr = [NSMutableString string];
    self.idStudentTextField.text = idStudentStr;
}

- (IBAction)numberButton:(UIButton *)sender {
    if (sender.tag < 10) {
        [idStudentStr appendFormat:@"%@",[@(sender.tag) stringValue]];
    }else if (sender.tag>10){
        [self onAlertDialogClose];
    }
    else{
        NSUInteger characterCount = [idStudentStr length];
        if (characterCount > 0) {
              [idStudentStr deleteCharactersInRange:NSMakeRange(characterCount-1, 1)];
        }
    }
    self.idStudentTextField.text = idStudentStr;
    self.idStudentTextField.text = idStudentStr;
}

// Show the indicator
- (void)showIndicator {
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}
#pragma Dialog
- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)nextAction:(id)sender{
    if (idStudentStr.length > 0) {
         [self doCallBSGetDataStudentScanBehaviorAPI:[UserData getSchoolId] idStudent:idStudentStr];
    }else{
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_ENTER_ID",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
    }
}
- (void)moveBack:(id)sender{
    BSSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectClassLevelStoryboard"];
    viewController.mode = self.mode;
    viewController.status = self.status;
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.selectedStudentArray = _selectedStudentArray;
    viewController.behaviorScoreArray = _behaviorScoreArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
