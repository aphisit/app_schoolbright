//
//  SNAddUserGroupViewController.h
//  JabjaiApp
//
//  Created by toffee on 2/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNAddUserInGroupCell.h"
#import "SWRevealViewController.h"

#import "SlideMenuController.h"

@interface SNAddUserGroupViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, SNAddUserInGroupCellDelegate, SlideMenuControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *groupNameTextField;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
