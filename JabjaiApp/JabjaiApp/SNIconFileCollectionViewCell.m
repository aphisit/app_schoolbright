//
//  SNIconFileCollectionViewCell.m
//  JabjaiApp
//
//  Created by toffee on 27/6/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "SNIconFileCollectionViewCell.h"

@implementation SNIconFileCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)removeFileAction:(id)sender {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(removeIndexFile:collectionView:)]) {
        [self.delegate removeIndexFile:self.fileBtn.tag collectionView:self.typeCollectionView];
    }
}
@end
