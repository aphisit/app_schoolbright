//
//  ReportPurchasingViewController.m
//  JabjaiApp
//
//  Created by mac on 4/20/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportPurchasingViewController.h"
#import "PurchasingDataModel.h"
#import "PurchasingTableViewCell.h"
#import "PurchasingProductModel.h"
#import "PurchasingListMenuModel.h"
#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"
#import "DateUtility.h"
#import "ReportListStudentViewController.h"
#import "ReportListTeacherViewController.h"


@interface ReportPurchasingViewController (){
    
    UIColor *grayColor;
    UIColor *headerTitleColor;
    UIColor *todayColor;
    UIColor *selectionColor;
    
    NSMutableDictionary *fillDefaultColors;
    NSMutableDictionary *fillSelectionColors;
    NSMutableDictionary *borderSelectionColors;
    NSMutableDictionary *titleDefaultColors;
    NSMutableDictionary *titleSelectionColors;
    NSMutableArray *weekendDays;
    NSMutableArray *holidayArray;
    NSInteger connectCounter;
    NSMutableArray<PurchasingProductModel *> *productArray;
    NSMutableArray<PurchasingListMenuModel *> *listProductArray;
    RPPurchasingTableViewDialog *rpPurchasingTableViewDialog;
    UIColor *dodgerBlue, *aqua, *monaLisa, *bitterSweet, *ripTide, *mountainMeadow, *perfume, *heliotRope, *alto, *dustyGray, *peachOrange, *texasRose;
    
}

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (nonatomic) NSCalendar *gregorian;
@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;

@property (strong, nonatomic) UIColor *eventGreenColor;
@property (strong, nonatomic) UIColor *todayColor;
@property (strong, nonatomic) UIColor *customTextSelectedColor;
@property (strong, nonatomic) UIColor *customTextDeselectedColor;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation ReportPurchasingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    [self setLanguage];
    self.heightStackConstraint.constant = 0.0f;
    self.summaryStack.hidden = YES;
    NSLog(@"%f",self.heightStackConstraint.constant);
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    selectionColor = [UIColor colorWithRed:212/255.0 green:44/255.0 blue:31/255.0 alpha:1.0];
    
    //04/05/2017
    
    dodgerBlue = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0]; // Now Full
    aqua = [UIColor colorWithRed:0.21 green:0.89 blue:1.00 alpha:1.0]; // Now Select
    monaLisa = [UIColor colorWithRed:1.00 green:0.68 blue:0.62 alpha:1.0]; // Color status 0 Before Select
    bitterSweet = [UIColor colorWithRed:1.00 green:0.48 blue:0.38 alpha:1.0]; // Color status 0 After Select
    ripTide = [UIColor colorWithRed:0.47 green:0.93 blue:0.71 alpha:1.0]; // Color status 1 Before Select
    mountainMeadow = [UIColor colorWithRed:0.13 green:0.76 blue:0.46 alpha:1.0]; // Color status 1 After Select
    perfume = [UIColor colorWithRed:0.80 green:0.62 blue:0.98 alpha:1.0]; // Color status 2 Before Select
    heliotRope = [UIColor colorWithRed:0.75 green:0.51 blue:1.00 alpha:1.0]; // Color status 2 After Select
    alto = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1.0]; // Color status 3 Before Select
    dustyGray = [UIColor colorWithRed:0.60 green:0.60 blue:0.60 alpha:1.0]; // Color status 3 After Select
    peachOrange = [UIColor colorWithRed:1.00 green:0.85 blue:0.59 alpha:1.0]; // Color status 4 Before Select
    texasRose = [UIColor colorWithRed:1.00 green:0.75 blue:0.31 alpha:1.0]; // Color status 4 After Select
    
    self.eventGreenColor = [UIColor colorWithRed:62/255.0 green:194/255.0 blue:45/255.0 alpha:1.0];
    self.todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    self.customTextSelectedColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.customTextDeselectedColor = [UIColor colorWithRed:190/255.0 green:194/255.0 blue:197/255.0 alpha:1.0];
    
    self.gregorian = [Utils getGregorianCalendar];
    
    self.minimumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
    self.maximumDate = [NSDate date];
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height >= 1024.0f) {
            self.heightCalendarConstraint.constant = 450.0f;
        }
    }
    else{
        self.heightCalendarConstraint.constant = 300.0f;
    }
    
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_up"] forState:UIControlStateNormal];
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_down"] forState:UIControlStateSelected];
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = grayColor;
    
    connectCounter = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PurchasingTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.dateLabel.text = [Utils getThaiDateFormatWithDate:[NSDate date]];
    [self getPurchasingItemListWithDate:[NSDate date]];
    [self createCalendar];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(upSwipe:)];
       [swipeLeft setDirection: UISwipeGestureRecognizerDirectionUp ];
       [self.containerTableView addGestureRecognizer:swipeLeft];
       swipeLeft=nil;

       UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(downSwipe:)];
       [swipeRight setDirection:UISwipeGestureRecognizerDirectionDown];
       [self.containerTableView addGestureRecognizer:swipeRight];
}

- (void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PURCHASING_PRI_REP_LIST", nil, [Utils getLanguage], nil);
    self.headerTotalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PURCHASING_PRI_REP_TATAL", nil, [Utils getLanguage], nil);
    self.headerBathLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PURCHASING_PRI_REP_B", nil, [Utils getLanguage], nil);
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    [self.containerTableView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.containerTableView.layer setShadowOpacity:0.3];
    [self.containerTableView.layer setShadowRadius:10.0];
    [self.containerTableView.layer setShadowOffset:CGSizeMake(0.0, 0.0)];

}

- (void)viewWillAppear:(BOOL)animated {
    [self createCalendar];
}

- (void)createCalendar {
  
    self.containerView.backgroundColor = grayColor;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, height)];
    
    calendar.dataSource = self;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor whiteColor];
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
    // 04/05/2018
    calendar.appearance.todayColor = dodgerBlue;
    calendar.appearance.titleTodayColor = [UIColor whiteColor];
    calendar.appearance.todaySelectionColor = dodgerBlue;
    calendar.appearance.selectionColor = [UIColor clearColor];
    calendar.appearance.titleSelectionColor = [UIColor blackColor];
    calendar.appearance.borderSelectionColor = dodgerBlue;
    
    [self.containerView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 95, 34);
    previousButton.backgroundColor = [UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(self.view.frame.size.width - nextButton.frame.size.width - 95, 5, 95, 34);
    nextButton.backgroundColor = [UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:nextButton];
    self.nextButton = nextButton;
    [self getPurchaseMonthly:calendar.currentPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CFSCalendarDataSource
- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}
- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

#pragma mark - CFSCalendarDelegate
- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
    if([DateUtility sameMonthOfYear:date date2:calendar.currentPage]) {
        return YES;
    }
    return NO;
}

- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
 
    self.dateLabel.text = [Utils getThaiDateFormatWithDate:date];
    [self getPurchasingItemListWithDate:date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd/MM/yyyy";
    NSLog(@"%@", [formatter stringFromDate:date]);
}

#pragma mark - upSwipe, downSwipe
-(void)upSwipe:(UISwipeGestureRecognizer *)gesture
{
    self.arrowStatement.selected = !self.arrowStatement.selected;
      if (self.arrowStatement.isSelected == YES) {
           if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
               CGSize screenSize = [[UIScreen mainScreen] bounds].size;
               if (screenSize.height >= 1024.0f) {
                   [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                       self.topDetailContraint.constant = 0.0f;
                       self.heightStackConstraint.constant = 0.0f;
                       self.summaryStack.hidden = YES;
                       [self.view layoutIfNeeded];
                   } completion:^(BOOL finished) {
                       [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                           
                           self.topDetailContraint.constant = -450.0f;
                           self.heightStackConstraint.constant = 70.0f;
                           self.summaryStack.hidden = NO;
                           [self.view layoutIfNeeded];
                       } completion:^(BOOL finished) {
                       }];
                   }];
               }
           }
           else{
               [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                   self.topDetailContraint.constant = 0.0f;
                   self.heightStackConstraint.constant = 0.0f;
                   self.summaryStack.hidden = YES;
                   [self.view layoutIfNeeded];
               } completion:^(BOOL finished) {
                   [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                   
                       self.topDetailContraint.constant = -300.0f;
                       self.heightStackConstraint.constant = 40.0f;
                       self.summaryStack.hidden = NO;
                       [self.view layoutIfNeeded];
                   } completion:^(BOOL finished) {
                       
                   }];
               }];
           }
           
           
           
       }else{
           //        self.topDetailConstraint.constant = 0.0f;
           if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
               CGSize screenSize = [[UIScreen mainScreen] bounds].size;
               if (screenSize.height >= 1024.0f) {
                   [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                       
                       self.topDetailContraint.constant = -450.0f;
                       self.heightStackConstraint.constant = 70.0f;
                       self.summaryStack.hidden = NO;
                       
                       [self.view layoutIfNeeded];
                   } completion:^(BOOL finished) {
                       [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                           self.topDetailContraint.constant = 0.0f;
                           self.heightStackConstraint.constant = 0.0f;
                           self.summaryStack.hidden = YES;
                           [self.view layoutIfNeeded];
                       } completion:^(BOOL finished) {
                       }];
                   }];
               }
           }
           else{
               [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                   
                   self.topDetailContraint.constant = -300.0f;
                   self.heightStackConstraint.constant = 40.0f;
                   self.summaryStack.hidden = NO;
                   [self.view layoutIfNeeded];
                   
               } completion:^(BOOL finished) {
                   [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                       
                       self.topDetailContraint.constant = 0.0f;
                       self.heightStackConstraint.constant = 0.0f;
                       self.summaryStack.hidden = YES;
                       
                       [self.view layoutIfNeeded];
                   } completion:^(BOOL finished) {
                       
                   }];
               }];
           }
       }

       
}

-(void)downSwipe:(UISwipeGestureRecognizer *)gesture
{
   self.arrowStatement.selected = !self.arrowStatement.selected;
     if (self.arrowStatement.isSelected == YES) {
          if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
              CGSize screenSize = [[UIScreen mainScreen] bounds].size;
              if (screenSize.height >= 1024.0f) {
                  [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                      self.topDetailContraint.constant = 0.0f;
                      self.heightStackConstraint.constant = 0.0f;
                      self.summaryStack.hidden = YES;
                      [self.view layoutIfNeeded];
                  } completion:^(BOOL finished) {
                      [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                          
                          self.topDetailContraint.constant = -450.0f;
                          self.heightStackConstraint.constant = 70.0f;
                          self.summaryStack.hidden = NO;
                          [self.view layoutIfNeeded];
                      } completion:^(BOOL finished) {
                      }];
                  }];
              }
          }
          else{
              [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                  self.topDetailContraint.constant = 0.0f;
                  self.heightStackConstraint.constant = 0.0f;
                  self.summaryStack.hidden = YES;
                  [self.view layoutIfNeeded];
              } completion:^(BOOL finished) {
                  [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                  
                      self.topDetailContraint.constant = -300.0f;
                      self.heightStackConstraint.constant = 40.0f;
                      self.summaryStack.hidden = NO;
                      [self.view layoutIfNeeded];
                  } completion:^(BOOL finished) {
                      
                  }];
              }];
          }
          
          
          
      }else{
          //        self.topDetailConstraint.constant = 0.0f;
          if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
              CGSize screenSize = [[UIScreen mainScreen] bounds].size;
              if (screenSize.height >= 1024.0f) {
                  [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                      
                      self.topDetailContraint.constant = -450.0f;
                      self.heightStackConstraint.constant = 70.0f;
                      self.summaryStack.hidden = NO;
                      
                      [self.view layoutIfNeeded];
                  } completion:^(BOOL finished) {
                      [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                          self.topDetailContraint.constant = 0.0f;
                          self.heightStackConstraint.constant = 0.0f;
                          self.summaryStack.hidden = YES;
                          [self.view layoutIfNeeded];
                      } completion:^(BOOL finished) {
                          
                      }];
                  }];
              }
          }
          else{
              [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                  
                  self.topDetailContraint.constant = -300.0f;
                  self.heightStackConstraint.constant = 40.0f;
                  self.summaryStack.hidden = NO;
                  [self.view layoutIfNeeded];
                  
              } completion:^(BOOL finished) {
                  [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                      
                      self.topDetailContraint.constant = 0.0f;
                      self.heightStackConstraint.constant = 0.0f;
                      self.summaryStack.hidden = YES;
                      
                      [self.view layoutIfNeeded];
                  } completion:^(BOOL finished) {
                      
                  }];
              }];
          }
      }

      
}


#pragma mark - Class buttons event
- (IBAction)openDetail:(id)sender {
    
    self.arrowStatement.selected = !self.arrowStatement.selected;
    if (self.arrowStatement.isSelected == YES) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailContraint.constant = 0.0f;
                    self.heightStackConstraint.constant = 0.0f;
                    self.summaryStack.hidden = YES;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        
                        self.topDetailContraint.constant = -450.0f;
                        self.heightStackConstraint.constant = 70.0f;
                        self.summaryStack.hidden = NO;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        CAGradientLayer *gradientTotal;
                        //set color header
                        [self.totalView layoutIfNeeded];
                        gradientTotal = [Utils getGradientColorStatus:@"green"];
                        gradientTotal.frame = self.totalView.bounds;
                        [self.totalView.layer insertSublayer:gradientTotal atIndex:0];
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailContraint.constant = 0.0f;
                self.heightStackConstraint.constant = 0.0f;
                self.summaryStack.hidden = YES;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                
                    self.topDetailContraint.constant = -300.0f;
                    self.heightStackConstraint.constant = 40.0f;
                    self.summaryStack.hidden = NO;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    CAGradientLayer *gradientTotal;
                    //set color header
                    [self.totalView layoutIfNeeded];
                    gradientTotal = [Utils getGradientColorStatus:@"green"];
                    gradientTotal.frame = self.totalView.bounds;
                    [self.totalView.layer insertSublayer:gradientTotal atIndex:0];
                }];
            }];
        }
        
        
        
    }else{
        [self.totalView layoutIfNeeded];
        CAGradientLayer *layerToRemovePickupButton;
        for (CALayer *layer in self.totalView.layer.sublayers) {
                if ([layer isKindOfClass:[CAGradientLayer class]]) {
                    layerToRemovePickupButton = (CAGradientLayer *)layer;
                }
        }
        [layerToRemovePickupButton removeFromSuperlayer];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    
                    self.topDetailContraint.constant = -450.0f;
                    self.heightStackConstraint.constant = 70.0f;
                    self.summaryStack.hidden = NO;
                    
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailContraint.constant = 0.0f;
                        self.heightStackConstraint.constant = 0.0f;
                        self.summaryStack.hidden = YES;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                
                self.topDetailContraint.constant = -300.0f;
                self.heightStackConstraint.constant = 40.0f;
                self.summaryStack.hidden = NO;
                [self.view layoutIfNeeded];
                
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    
                    self.topDetailContraint.constant = 0.0f;
                    self.heightStackConstraint.constant = 0.0f;
                    self.summaryStack.hidden = YES;
                    
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
    }
    
}

- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
    
    [self getPurchaseMonthly:previousMonth];
}

- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
    
    [self getPurchaseMonthly:nextMonth];
}

- (void)calendarCurrentPageDidChange:(CFSCalendar *)calendar {
    
    NSString *dateAsString = [self.dateFormatter stringFromDate:calendar.currentPage];
    NSLog(@"%@", [NSString stringWithFormat:@"Did change page %@", dateAsString]);
    
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date {
    
    if(fillDefaultColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if([fillDefaultColors objectForKey:key] != nil) {
            return [fillDefaultColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date{
    
    if (fillSelectionColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([fillSelectionColors objectForKey:key] != nil) {
            return [fillSelectionColors objectForKey:key];
        }
    }
    
    return nil;
    
}
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance borderSelectionColorForDate:(NSDate *)date{
    
    if (borderSelectionColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([borderSelectionColors objectForKey:key] != nil) {
            return [borderSelectionColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date{
    
    if (titleDefaultColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([titleDefaultColors objectForKey:key] != nil) {
            return [titleDefaultColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleSelectionColorForDate:(NSDate *)date{
    
    if (titleSelectionColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([titleSelectionColors objectForKey:key] != nil) {
            return [titleSelectionColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (void)getPurchaseMonthly:(NSDate *)date{
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getBoughtItemMonthWithUserID:userID date:date schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            if (error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if (data == nil){
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if ((self->connectCounter < TRY_CONNECT_MAX)) {
                self->connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getPurchaseMonthly:date];
            }
            else{
                self->connectCounter = 0;
            }
        }
        
        else{
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getPurchaseMonthly:date];
                }
                else {
                    self->connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getPurchaseMonthly:date];
                }
                else {
                    self->connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                self->connectCounter = 0;
                self->holidayArray = [[NSMutableArray alloc] init];
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *datadict = [returnedArray objectAtIndex:i];
                    NSMutableString *holidayStr = [[NSMutableString alloc] initWithFormat:@"%@", [datadict objectForKey:@"daybuy"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)holidayStr);
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    NSDate *holiday = [formatter dateFromString:holidayStr];
                    PurchasingDataModel *model = [[PurchasingDataModel alloc] init];
                    [model setPurchasingList:holiday];
                    [self->holidayArray addObject:model];
                }
                [self updateEventHoliday];
            }
            
            
        }
    }];
    
}

- (void)updateEventHoliday{
    
    
    if (holidayArray == nil) {
        return;
    }
    
    if (fillDefaultColors != nil) {
        [fillDefaultColors removeAllObjects];
    }
    
    if (fillSelectionColors != nil) {
        [fillSelectionColors removeAllObjects];
    }
    
    if (borderSelectionColors != nil) {
        [borderSelectionColors removeAllObjects];
    }
    
    if (titleDefaultColors != nil) {
        [titleDefaultColors removeAllObjects];
    }
    
    if (titleSelectionColors != nil) {
        [titleSelectionColors removeAllObjects];
    }
    
    fillDefaultColors = [[NSMutableDictionary alloc] init];
    fillSelectionColors = [[NSMutableDictionary alloc] init];
    borderSelectionColors = [[NSMutableDictionary alloc] init];
    titleDefaultColors = [[NSMutableDictionary alloc] init];
    titleSelectionColors = [[NSMutableDictionary alloc] init];
    
    for (PurchasingDataModel *model in holidayArray) {
    NSString *dateKey = [self.dateFormatter stringFromDate:model.purchasingList];
        NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:model.purchasingList];
        NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
        if([today day] == [otherDay day] &&
           [today month] == [otherDay month] &&
           [today year] == [otherDay year] &&
           [today era] == [otherDay era]) {
            
            [fillDefaultColors setObject:dodgerBlue forKey:dateKey];
            [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
            [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
        }
        else{
            [fillDefaultColors setObject:ripTide forKey:dateKey];
            [fillSelectionColors setObject:mountainMeadow forKey:dateKey];
            [borderSelectionColors setObject:[UIColor clearColor] forKey:dateKey];
            [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
            [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
        }
        
    }
    
    [self.calendar reloadData];
    
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (void)getPurchasingItemListWithDate:(NSDate *)date {
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getBoughtItemWithUserID:userID date:date schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    [self showIndicator];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self stopIndicator];
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;
                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getPurchasingItemListWithDate:date];
            }
            else {
                self->connectCounter = 0;
            }
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getPurchasingItemListWithDate:date];
                }
                else {
                    self->connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getPurchasingItemListWithDate:date];
                }
                else {
                    self->connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                self->connectCounter = 0;
                
                if(self->productArray != nil) {
                    [self->productArray removeAllObjects];
                    self->productArray = nil;
                }
                
                self->productArray = [[NSMutableArray alloc] init];
                
                NSString *paymantDate,*userType,*shopName;
                NSArray *listMenuArray;
                NSInteger totalSum = 0;
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    long long paymentId = [[dataDict objectForKey:@"payment_Id"] longLongValue];
                    long long userId = [[dataDict objectForKey:@"user_Id"] longLongValue];
                    
                    if(![[dataDict objectForKey:@"paymentDate"] isKindOfClass:[NSNull class]]) {
                        paymantDate = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"paymentDate"]];
                    }
                    else {
                        paymantDate = [[NSMutableString alloc] initWithString:@""];
                    }
                    if(![[dataDict objectForKey:@"user_Type"] isKindOfClass:[NSNull class]]) {
                        userType = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"user_Type"]];
                    }
                    else {
                        userType = [[NSMutableString alloc] initWithString:@""];
                    }
                    if(![[dataDict objectForKey:@"Shop_Name"] isKindOfClass:[NSNull class]]) {
                        shopName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Shop_Name"]];
                    }
                    else {
                        shopName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    listMenuArray = [dataDict objectForKey:@"items"];
                    NSString *productName;
                    self->listProductArray = [[NSMutableArray alloc] init];
                    NSInteger sum = 0;
                    for (int j = 0; j < listMenuArray.count; j++) {
                        NSDictionary *productDict = [listMenuArray objectAtIndex:j];
                        long long productId = [[productDict objectForKey:@"Id"] longLongValue];
                        NSInteger productPrice = [[productDict objectForKey:@"Price"] integerValue];
                        NSInteger productAmount = [[productDict objectForKey:@"Amount"] integerValue];
                        if(![[productDict objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
                            productName = [[NSMutableString alloc] initWithFormat:@"%@", [productDict objectForKey:@"Name"]];
                        }
                        else {
                            productName = [[NSMutableString alloc] initWithString:@""];
                        }
                        sum = sum + (productPrice * productAmount);
                        totalSum = totalSum + (productPrice * productAmount);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) productName);
                        PurchasingListMenuModel *model = [[PurchasingListMenuModel alloc] init];
                        model.productId = productId;
                        model.productPrice = productPrice;
                        model.productAmount = productAmount;
                        model.productName = productName;
                    [self->listProductArray addObject:model];
                    }
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) paymantDate);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) userType);
                    PurchasingProductModel *pmodel = [[PurchasingProductModel alloc] init];
                    pmodel.paymentDate = paymantDate;
                    pmodel.paymentId = paymentId;
                    pmodel.userId = userId;
                    pmodel.userType = userType;
                    pmodel.shopName = shopName;
                    pmodel.totalPrice = sum;
                    pmodel.listProduct = self->listProductArray;
                [self->productArray addObject:pmodel];
                }
                self.summaryLabel.text = [[NSString alloc] initWithFormat:@"%d", totalSum];
                [self.tableView reloadData];
            }
        }
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(productArray == nil) {
        return 0;
    }
    else {
        return productArray.count;
    }
}
 
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    PurchasingProductModel *model = [productArray objectAtIndex:indexPath.row];
    NSDate *date = [Utils parseServerDateStringToDate:model.paymentDate];
    NSDateFormatter *timeFormatter = [Utils getDateFormatter];
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [timeFormatter setDateFormat:@"HH:mm"];
    }
    
    //[timeFormatter setDateFormat:@"HH:mm:ss 'น.'"];
    PurchasingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.paymentDateLabel.text = [timeFormatter stringFromDate:date];
    cell.priceLable.text = [NSString stringWithFormat:@"%@ %ld %@",NSLocalizedStringFromTableInBundle(@"LABEL_PURCHASING_PRI_REP_BUY", nil, [Utils getLanguage], nil),(long)model.totalPrice,NSLocalizedStringFromTableInBundle(@"LABEL_PURCHASING_PRI_REP_B", nil, [Utils getLanguage], nil)];
    cell.shopNameLabel.text = [model shopName];
    [cell.colorView layoutIfNeeded];
    cell.colorView.layer.cornerRadius = cell.colorView.frame.size.height /2;
    cell.colorView.layer.masksToBounds = YES;
    cell.colorView.layer.borderWidth = 0;
    
    [cell.shadowView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.shadowView.layer setShadowOpacity:0.3];
    [cell.shadowView.layer setShadowRadius:3.0];
    [cell.shadowView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self showTableListProduct:[productArray objectAtIndex:indexPath.row]];
}

#pragma mark - RPListPurchasingViewController
- (void)showTableListProduct:(PurchasingProductModel *)data {
    RPListPurchasingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RPListPurchasingStoryboard"];
    viewController.dataListProduct = data;
    [self.slideMenuController changeMainViewController:viewController close:YES];
//    if(rpPurchasingTableViewDialog != nil && [rpPurchasingTableViewDialog isDialogShowing]) {
//        [rpPurchasingTableViewDialog dismissDialog];
//        rpPurchasingTableViewDialog = nil;
//    }
//    rpPurchasingTableViewDialog = [[RPPurchasingTableViewDialog alloc] init];
//    rpPurchasingTableViewDialog.delegate = self;
//    [rpPurchasingTableViewDialog showDialogInView:self.view dataArr:data];
    //[self stopIndicator];
}

- (IBAction)moveBack:(id)sender {
    if ([UserData getUserType] == STUDENT) {
        ReportListStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListStudentStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else{
        ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    
}
@end
