//
//  ReportCheckNameSubjectDateTimeTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 24/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportCheckNameSubjectDateTimeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *colorStatus;


- (void)updateStatus:(int)status;
- (void)updateDateTimeLabel:(NSString*)dateTime status:(NSString*)status;

@end

NS_ASSUME_NONNULL_END
