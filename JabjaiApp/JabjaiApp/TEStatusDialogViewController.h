//
//  TEStatusDialogViewController.h
//  JabjaiApp
//
//  Created by toffee on 1/16/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@class TEStatusDialogViewController;

@protocol TEStatusDialogViewControllerDelegate <NSObject>

- (void)taStatusDialog:(TEStatusDialogViewController *)dialog onSelectStatus:(TA_SCAN_STATUS)scanStatus;

@end

@interface TEStatusDialogViewController : UIViewController

@property (nonatomic, retain) id<TEStatusDialogViewControllerDelegate> delegate;


@property (weak, nonatomic) IBOutlet UIButton *statusOnTimeButton;//เข้าเรียน
@property (weak, nonatomic) IBOutlet UIButton *statusLateButton;//สาย
@property (weak, nonatomic) IBOutlet UIButton *statusAbsenceButton;//ขาด
@property (weak, nonatomic) IBOutlet UIButton *noStatus;//ไม่ระบ

@property (weak, nonatomic) IBOutlet UIButton *statusEventButton;//กิจกรรม
@property (weak, nonatomic) IBOutlet UIButton *statusPersonalLeaveButton;//ลากิจ
@property (weak, nonatomic) IBOutlet UIButton *statusSickLeaveButton;//ลาป่วย

- (IBAction)selectStatus:(id)sender;

- (void)showDialogInView:(UIView *)targetView mode:(NSInteger)mode;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
