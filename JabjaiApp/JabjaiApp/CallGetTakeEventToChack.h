//
//  CallGetTakeEventToChack.h
//  JabjaiApp
//
//  Created by toffee on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CallGetTakeEventToChack;

@protocol CallGetTEHistoryStudentStatusListAPIDelegate <NSObject>

@optional
- (void)callGetTEHistoryStudentStatusListAPI:(CallGetTEHistoryStudentStatusList *)classObj data:(NSArray<TAReportStudentStatusModel *> *)data success:(BOOL)success;

@end
@interface CallGetTakeEventToChack : NSObject

@end
