//
//  ReportInOutAllDataSelectDepartmantModel.h
//  JabjaiApp
//
//  Created by toffee on 15/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutAllDataSelectDepartmantModel : NSObject
@property (nonatomic) NSString *departmantName;
@property (nonatomic) long long departmantId;
@property (nonatomic) NSInteger amountDepartmant;
@property (nonatomic) NSDictionary *status;

- (void)setDepartmantName:(NSString *)departmantName;
- (void)setDepartmantId:(long long )departmantId;
- (void)setAmountDepartmant:(NSInteger )amountDepartmant;
- (void)setStatus:(NSDictionary *)status;

- (NSString*)getDepartmantName;
- (long long)getDepartmantId;
- (NSInteger)getAmountDepartmant;
- (NSDictionary*)getStatus;


@end

NS_ASSUME_NONNULL_END
