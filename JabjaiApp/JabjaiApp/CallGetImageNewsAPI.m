//
//  CallGetImageNewsAPI.m
//  JabjaiApp
//
//  Created by toffee on 10/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetImageNewsAPI.h"
#import "Utils.h"
#import "APIURL.h"
#import "UserData.h"
@interface CallGetImageNewsAPI (){
   
    NSMutableArray *imageArray;
    NSArray *returnedArray;
}

@end
@implementation CallGetImageNewsAPI

-(void)call:(long long)massageID{
     [self getImageNews:massageID ];
}
- (void)getImageNews:(long long)massageID {
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getImageNews:userID idMessage:massageID schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    imageArray = [[NSMutableArray alloc] init];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        returnedArray = returnedData;
        if(returnedArray.count == 0){
            returnedArray = NULL;
            
        }
        if (returnedArray != nil || returnedArray != NULL) {
            for (int i=0; i<returnedArray.count; i++) {
                [imageArray addObject:[[returnedArray objectAtIndex:i]objectForKey:@"filename"] ];
            }
        }
        if(self.delegate && [self.delegate respondsToSelector:@selector(callImageNewsPOSTAPI:response:success:)]) {
            [self.delegate callImageNewsPOSTAPI:self imageArray:imageArray];
        }
        
    }];
    
}
@end
