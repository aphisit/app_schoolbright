//
//  CellDetailMessageBoxAPI.m
//  JabjaiApp
//
//  Created by Mac on 27/4/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import "CellDetailMessageBoxAPI.h"
#import "APIURL.h"
#import "Utils.h"
@implementation CellDetailMessageBoxAPI{
    NSInteger connectCounter;
    SNDetailMessageBoxModel *detailModel;
}

- (void)call:(long long)userID messageID:(long long)messageID schoolid:(long long)schoolid{
    [self getDetailMessageBox:userID messageID:messageID schoolid:schoolid];
}
- (void)getDetailMessageBox:(long long)userID messageID:(long long)messageID schoolid:(long long)schoolid{

    NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:messageID userID:userID schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;
                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getDetailMessageBox:userID messageID:messageID schoolid:schoolid];
            }
            else {
                isFail = YES;
                self->connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getDetailMessageBox:userID messageID:messageID schoolid:schoolid];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
            }
            else {
                if (returnedData != nil) {
                    NSDictionary *dataDict = returnedData;
                    NSInteger messageID;
                    NSString *messageStr, *dateStr, *replyResult;
                    NSArray *replyButtons;
                    
                    if (![[dataDict objectForKey:@"nMessageID"] isKindOfClass:[NSNull class]]) {
                        messageID = [[dataDict objectForKey:@"nMessageID"] integerValue];
                    }else{
                        messageID = 0;
                    }
                    
                    if (![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        messageStr = [dataDict objectForKey:@"sMessage"];
                    }else{
                        messageStr = @"";
                    }
                    
                    if (![[dataDict objectForKey:@"dSend"] isKindOfClass:[NSNull class]]) {
                        dateStr = [dataDict objectForKey:@"dSend"];
                    }else{
                        dateStr = @"";
                    }
                    
                    
                    if (![[dataDict objectForKey:@"replyButtons"] isKindOfClass:[NSNull class]]) {
                        replyButtons = [dataDict objectForKey:@"replyButtons"];
                    }
                    
                    if (![[dataDict objectForKey:@"replyResult"] isKindOfClass:[NSNull class]]) {
                        replyResult = [dataDict objectForKey:@"replyResult"];
                    }else{
                        replyResult = @"";
                    }
                    
//                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageStr);
//                    CFStringTrimWhitespace((__bridge CFMutableStringRef) dateStr);
                    
                    self->detailModel = [[SNDetailMessageBoxModel alloc] init];
                    self->detailModel.message = messageStr;
                    self->detailModel.date = dateStr;
                    self->detailModel.replyButton = replyButtons;
                    self->detailModel.messageID = messageID;
                    self->detailModel.replyResult = replyResult;
                    
                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callDetailMessageBoxAPI:data:success:)]) {
                        [self.delegate callDetailMessageBoxAPI:self data:self->detailModel success:YES];
                    }
                   
        
                }
                
            
            }
        }
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callDetailMessageBoxAPI:data:success:)]) {
                [self.delegate callDetailMessageBoxAPI:self data:self->detailModel success:NO];
            }
        }
        
    }];
}
@end
