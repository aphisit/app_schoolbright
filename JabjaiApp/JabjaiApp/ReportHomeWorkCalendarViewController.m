//
//  ReportHomeWorkCalendarViewController.m
//  JabjaiApp
//
//  Created by mac on 1/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportHomeWorkCalendarViewController.h"
#import "ReportHomeWorkListViewController.h"
#import "ReportHomeWorkMessageViewController.h"
#import "ReportHomeWorkMonthlyModel.h"
#import "APIURL.h"
#import "UserData.h"
#import "Utils.h"
#import "DateUtility.h"

@interface ReportHomeWorkCalendarViewController (){
    UIColor *grayColor;
    UIColor *headerTitleColor;
    UIColor *todayColor;
    UIColor *selectionColor;
    
    NSMutableDictionary *fillDefaultColors;
    NSMutableArray *weekendDays;
    
    NSMutableArray *holidayArray;
    
    NSInteger connectCounter;
    
}

@property (strong, nonatomic) ARSPContainerController *panelControllerContainer;
@property (strong, nonatomic) ReportHomeWorkListViewController *reportHomeWorkListViewController;
@property (strong, nonatomic) JHDetailHomeWorkViewController *jHDetailHomeWorkViewController;
@property (nonatomic) CGFloat shadowRadius;
@property (nonatomic) CGFloat shadowOpacity;
@property (nonatomic) CGFloat animationDuration;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;

@property (strong, nonatomic) UIColor *eventGreenColor;
@property (strong, nonatomic) UIColor *todayColor;
@property (strong, nonatomic) UIColor *customTextSelectedColor;
@property (strong, nonatomic) UIColor *customTextDeselectedColor;

@end

@implementation ReportHomeWorkCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    selectionColor = [UIColor colorWithRed:212/255.0 green:44/255.0 blue:31/255.0 alpha:1.0];
    
    // 26/10/2017
    self.eventGreenColor = [UIColor colorWithRed:62/255.0 green:194/255.0 blue:45/255.0 alpha:1.0];
    self.todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    self.customTextSelectedColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.customTextDeselectedColor = [UIColor colorWithRed:190/255.0 green:194/255.0 blue:197/255.0 alpha:1.0];
    
    // 26/10/2017
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.gregorian = [Utils getGregorianCalendar];
    
    self.minimumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
    self.maximumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:1 toDate:[NSDate date] options:0];
    
    // Sliding up panel
    self.panelControllerContainer = (ARSPContainerController *)self.parentViewController;
    self.panelControllerContainer.dragDelegate = self;
    self.panelControllerContainer.visibilityStateDelegate = self;
    
    
    self.shadowRadius = 5.0f;
    self.shadowOpacity = 0.5f;
    self.animationDuration = 0.55f;
    
    self.panelControllerContainer.dropShadow = YES;
    self.panelControllerContainer.shadowRadius = self.shadowRadius;
    self.panelControllerContainer.shadowOpacity = self.shadowOpacity;
    self.panelControllerContainer.animationDuration = self.animationDuration;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    
}

- (void)creatCalendar{
    
    // Remove all subviews from container
//    NSArray *viewsToRemove = self.containerView.subviews;
//    for(UIView *v in viewsToRemove) {
//        [v removeFromSuperview];
//    }
    
    self.containerView.backgroundColor = grayColor;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    CGFloat width = CGRectGetWidth(self.containerView.bounds);
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    
    calendar.dataSource = self;
    calendar.delegate = self;
    //calendar.swipeToChooseGesture.enabled = YES;
    calendar.backgroundColor = grayColor;//[UIColor whiteColor];
    //    calendar.appearance.titleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.subtitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.weekdayFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.headerTitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:32.0f];
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
    calendar.appearance.selectionColor = selectionColor;
    calendar.appearance.todayColor = todayColor;
    [self.containerView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 95, 34);
    previousButton.backgroundColor = grayColor;//[UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(CGRectGetWidth(self.containerView.frame) - 95, 5, 95, 34);
    nextButton.backgroundColor = grayColor; //[UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:nextButton];
    self.nextButton = nextButton;
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    self.panelControllerContainer.visibleZoneHeight = screenHeight - (self.calendar.frame.size.height + self.calendar.frame.origin.y) - (self.headerView.frame.origin.y + self.headerView.frame.size.height);
    self.panelControllerContainer.swipableZoneHeight = 0;
    self.panelControllerContainer.draggingEnabled = YES;
    self.panelControllerContainer.shouldOverlapMainViewController = YES;
    self.panelControllerContainer.shouldShiftMainViewController = NO;
    
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
            if (screenSize.height == 812.0f) {
                self.topindicatorConstraint.constant = 176.5;
                self.bottomindicatorConstraint.constant = 298.5;
            }
    
            else if (screenSize.height == 736.0f){
                self.topindicatorConstraint.constant = 157.5;
                self.bottomindicatorConstraint.constant = 471;
            }
    
            else if (screenSize.height == 667.0f){
                self.topindicatorConstraint.constant = 140;
                self.bottomindicatorConstraint.constant = 420;
            }
            else if (screenSize.height == 568.0f){
                self.topindicatorConstraint.constant = 115.5;
                self.bottomindicatorConstraint.constant = 345;
            }
    
        }

    [self getStudentHomeworkCalendar:calendar.currentPage];
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    self.reportHomeWorkListViewController = (ReportHomeWorkListViewController *)self.panelControllerContainer.panelViewController;
    self.reportHomeWorkListViewController.delegate = self;
    
    [self creatCalendar];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Drag delegate

- (void)panelControllerWasDragged:(CGFloat)panelControllerVisibility {
    
}

- (void)panelControllerChangedVisibilityState:(ARSPVisibilityState)state {
    
}

#pragma mark - CFSCalendarDataSource

- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

#pragma mark - CFSCalendarDelegate

- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
    if([DateUtility sameMonthOfYear:date date2:calendar.currentPage]) {
        
//        [self getStudentHomeworkCalendar:calendar.currentPage];
        
        return YES;
    }
    
    return NO;
}

- (void)onSelectDate:(NSDate *)date {
    [self getStudentHomeworkCalendar:date];
}

- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
    if(self.delegate && [self.delegate respondsToSelector:@selector(onSelectDate:)]) {
        [self.delegate onSelectDate:date];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd/MM/yyyy";
    NSLog(@"%@", [formatter stringFromDate:date]);
}

#pragma mark - Class buttons event

- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
    [self getStudentHomeworkCalendar:previousMonth];

    
}

- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];

    [self getStudentHomeworkCalendar:nextMonth];
    
    
}

- (void)calendarCurrentPageDidChange:(CFSCalendar *)calendar {
    
    NSString *dateAsString = [self.dateFormatter stringFromDate:calendar.currentPage];
    NSLog(@"%@", [NSString stringWithFormat:@"Did change page %@", dateAsString]);
    
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date {

    if(fillDefaultColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];

        if([fillDefaultColors objectForKey:key] != nil) {
            return [fillDefaultColors objectForKey:key];
        }
    }

    return nil;

}

//- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date {
//
//    if(fillDefaultColors != nil) {
//        NSString *key = [self.dateFormatter stringFromDate:date];
//
//        if([fillDefaultColors objectForKey:key] != nil) {
//
//            if([weekendDays containsObject:key]) {
//                return self.customTextDeselectedColor;
//            }
//            else {
//                return self.customTextDeselectedColor;
//            }
//
//        }
//    }
//
//    return nil;
//}

- (void)getStudentHomeworkCalendar:(NSDate *)date{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getHomeworkCalendarWithUserID:userID date:date schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            [self stopIndicator];
            
            if (error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if (data == nil){
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if ((connectCounter < TRY_CONNECT_MAX)) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getStudentHomeworkCalendar:date];
            }
            else{
                connectCounter = 0;
            }
        }
        
        else{
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentHomeworkCalendar:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentHomeworkCalendar:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                holidayArray = [[NSMutableArray alloc] init];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *holidayStr = [[NSMutableString alloc] initWithFormat:@"%@" , [dataDict objectForKey:@"daybuy"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)holidayStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *holiday = [formatter dateFromString:holidayStr];
                    
                    ReportHomeWorkMonthlyModel *model = [[ReportHomeWorkMonthlyModel alloc] init];
                    
                    [model setHomeworkList:holiday];
                    
                    [holidayArray addObject:model];
                    
                }
                
                [self updateHomeworkDay];
            }
            
        }
        
        
    }];
    
}

- (void)updateHomeworkDay{

    if (holidayArray == nil) {
        return;
    }

    if (fillDefaultColors != nil) {
        [fillDefaultColors removeAllObjects];
    }

    fillDefaultColors  = [[NSMutableDictionary alloc] init];

    for (ReportHomeWorkMonthlyModel *model in holidayArray) {
        NSString *dateKey = [self.dateFormatter stringFromDate:model.homeworkList];
        
        [fillDefaultColors setObject:self.eventGreenColor forKey:dateKey];
    }

    [self.calendar reloadData];

}

- (IBAction)openDrawer:(id)sender {
    
    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)showNotifications:(id)sender {
    
    ReportHomeWorkMessageViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportHomeWorkMessageStoryboard"];
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

//- (void)onSelectSubject:(long long)subjectID subjectName:(NSString *)subjectName date:(NSDate *)date{
//
//    ReportHomeWorkDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportHomeWorkDetailStoryboard"];
//    viewController.subjectID = subjectID;
//    viewController.subjectName = subjectName;
//    viewController.consideredDate = date;
//
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
//
//}

- (void)onSelectMessage:(long long)messageID{
    if(self.jHDetailHomeWorkViewController == nil) {
        self.jHDetailHomeWorkViewController = [[JHDetailHomeWorkViewController alloc] init];
    }
    if(self.jHDetailHomeWorkViewController != nil && [self.jHDetailHomeWorkViewController isDialogShowing]) {
        [self.jHDetailHomeWorkViewController dismissDetailHomeWorkViewController];
    }
    [self.jHDetailHomeWorkViewController showDetailHomeWorkViewController:self.view homeworkID:messageID];
//    ReportHomeWorkDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportHomeWorkDetailStoryboard"];
//    viewController.messageID = messageID;
//
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

@end
