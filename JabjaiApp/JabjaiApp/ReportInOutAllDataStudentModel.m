//
//  ReportInOutAllDataStudentModel.m
//  JabjaiApp
//
//  Created by toffee on 8/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutAllDataStudentModel.h"

@implementation ReportInOutAllDataStudentModel

@synthesize status_0 = _status_0;
@synthesize status_1 = _status_1;
@synthesize status_2 = _status_2;
@synthesize status_3 = _status_3;
@synthesize status_4 = _status_4;
@synthesize status_5 = _status_5;
@synthesize status_6 = _status_6;


- (void)setStatus_0:(NSInteger)status_0{
    _status_0 = status_0;
}
- (void)setStatus_1:(NSInteger)status_1{
    _status_1 = status_1;
}
- (void)setStatus_2:(NSInteger)status_2{
    _status_2 = status_2;
}
- (void)setStatus_3:(NSInteger)status_3{
    _status_3 = status_3;
}
- (void)setStatus_4:(NSInteger)status_4{
    _status_4 = status_4;
}
- (void)setStatus_5:(NSInteger)status_5{
    _status_5 = status_5;
}
- (void)setStatus_6:(NSInteger)status_6{
    _status_6 = status_6;
}

- (NSInteger)getStatus_0{
    return _status_0;
}
- (NSInteger)getStatus_1{
    return _status_1;
}
- (NSInteger)getStatus_2{
    return _status_2;
}
- (NSInteger)getStatus_3{
    return _status_3;
}
- (NSInteger)getStatus_4{
    return _status_4;
}
- (NSInteger)getStatus_5{
    return _status_5;
}
- (NSInteger)getStatus_6{
    return _status_6;
}



@end
