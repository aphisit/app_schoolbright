//
//  ReportCheckFlagDateTimeOfStudentViewController.h
//  JabjaiApp
//
//  Created by toffee on 3/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportCheckFlagStatusCollectionViewCell.h"
#import "CallCheckFlagDateTimeOfStudentAPI.h"
#import "ReportCheckFlagDateTimeOfStudentTableViewCell.h"
#import "RPCheckFlagDateTimeModel.h"
#import "ReportCheckFlagHistoryStudentAllViewController.h"


NS_ASSUME_NONNULL_BEGIN

@interface ReportCheckFlagDateTimeOfStudentViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate ,UITableViewDataSource ,CallCheckFlagDateTimeOfStudentAPIDelegate>
@property (nonatomic) NSArray *studentStatusAbsenArray;
@property (nonatomic) NSArray *studentStatusOnTimeArray;
@property (nonatomic) NSArray *studentStatusLateArray;
@property (nonatomic) NSArray *studentStatusPersonalArray;
@property (nonatomic) NSArray *studentStatusSickArray;
@property (nonatomic) NSArray *studentStatusEventArray;
@property (nonatomic) NSArray *studentStatusAllArray;
@property (nonatomic) NSArray *studentStatusUndefinedArray;
@property (nonatomic) NSArray *studentAllArray;
@property (nonatomic) NSString *nameClassRoom;
@property (nonatomic) long long selectedClassroomId;
@property (nonatomic) NSString *studentId;
@property (nonatomic) NSDate *consideredDate;
@property (nonatomic) NSString *studentName;
@property (nonatomic) NSString *studentPic;

//date
@property (nonatomic) NSArray *dateTimeAllArray;
@property (nonatomic) NSArray *dateTimeOnTimeArray;
@property (nonatomic) NSArray *dateTimeLateArray;
@property (nonatomic) NSArray *dateTimeAbsenArray;
@property (nonatomic) NSArray *dateTimeSickArray;
@property (nonatomic) NSArray *dateTimePersonalArray;
@property (nonatomic) NSArray *dateTimeEventArray;
@property (nonatomic) NSArray *dateTimeUndefinedArray;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ImageStudent;
@property (weak, nonatomic) IBOutlet UILabel *showNoStudent;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

- (IBAction)moveBack:(id)sender;


@end

NS_ASSUME_NONNULL_END
