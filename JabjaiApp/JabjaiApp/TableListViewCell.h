//
//  TableListViewCell.h
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableListViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
