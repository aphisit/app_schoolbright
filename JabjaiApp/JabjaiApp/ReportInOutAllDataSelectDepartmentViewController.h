//
//  ReportInOutAllDataSelectDepartmentViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "ReportInOutAllDataSelectDepartmantTableViewCell.h"
#import "CallReportInOutAllDataSelectDepartmantAPI.h"
//#import "ReportInOutAllDataViewController.h"

@interface ReportInOutAllDataSelectDepartmentViewController : UIViewController <SlideMenuControllerDelegate, UITableViewDelegate, UITableViewDataSource, CallReportInOutAllDataSelectDepartmantAPIDelegate>

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;


@property (nonatomic) NSDate *date;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)moveBack:(id)sender;

@end
