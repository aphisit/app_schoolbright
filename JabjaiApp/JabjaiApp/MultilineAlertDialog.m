//
//  MultilineAlertDialog.m
//  JabjaiApp
//
//  Created by mac on 10/28/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "MultilineAlertDialog.h"
#import "Utils.h"
@interface MultilineAlertDialog ()

@property (nonatomic, assign) BOOL isShowing;

@end

@implementation MultilineAlertDialog

- (void)viewDidLoad {
    [super viewDidLoad];

    // Initialize variables
    self.isShowing = NO;
  
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (screenSize.height == 812.0f) {
            self.multilineDialogTopConstraint.constant = 265.5;
            self.multilineDialogBottomConstraint.constant = 265.5;
        }
        
        else if (screenSize.height == 736.0f){
            self.multilineDialogTopConstraint.constant = 227.5;
            self.multilineDialogBottomConstraint.constant = 227.5;
        }
        
        else if (screenSize.height == 667.0f){
            self.multilineDialogTopConstraint.constant = 193;
            self.multilineDialogBottomConstraint.constant = 193;
        }
        else if (screenSize.height == 568.0f){
            self.multilineDialogTopConstraint.constant = 143.5;
            self.multilineDialogBottomConstraint.constant = 143.5;
        }
        
    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientCancel;
   
    //set color button next
    [self.cancelBtn layoutIfNeeded];
    gradientCancel = [Utils getGradientColorStatus:@"red"];
    gradientCancel.frame = self.cancelBtn.bounds;
    [self.cancelBtn.layer insertSublayer:gradientCancel atIndex:0];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}



- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

#pragma mark - Dialog Functions

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
   
    [self.messageLabel setText:message];
    [self doDesignLayout];
    self.isShowing = YES;
}

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title attribute:(NSAttributedString *)attribute {
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    [self.messageLabel setAttributedText:attribute];
    self.isShowing = YES;
}

- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
    
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onMultilineAlertDialogClose)]) {
        [self.delegate onMultilineAlertDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

@end
