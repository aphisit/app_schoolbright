//
//  CallTeacherTimeDayAPI.h
//  JabjaiApp
//
//  Created by toffee on 22/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TeacherTimeTableModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallTeacherTimeDayAPI;

@protocol CallTeacherTimeDayAPIDelegate <NSObject>

@optional
    - (void)callTeacherTimeDayAPI:(CallTeacherTimeDayAPI *)classObj data:(NSMutableArray<TeacherTimeTableModel *> *)data success:(BOOL)success;
@end
@interface CallTeacherTimeDayAPI : NSObject
@property (nonatomic, weak) id<CallTeacherTimeDayAPIDelegate> delegate;
- (void)call:(long long)userId date:(NSDate*)date schoolid:(long long)schoolid;
@end

NS_ASSUME_NONNULL_END
