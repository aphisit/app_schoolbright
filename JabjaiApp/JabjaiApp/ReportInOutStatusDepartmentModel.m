//
//  ReportInOutStatusDepartmentModel.m
//  JabjaiApp
//
//  Created by toffee on 6/3/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutStatusDepartmentModel.h"

@implementation ReportInOutStatusDepartmentModel
@synthesize nameDepartment = _nameDepartment;
@synthesize departmentId = _departmentId;
@synthesize status = _status;
@synthesize pictuer = _pictuer;

- (void) setNameDepartment:(NSString *)nameDepartment{
    _nameDepartment = nameDepartment;
}
- (void) setDepartmentId:(long long)departmentId{
    _departmentId = departmentId;
}
- (void) setStatus:(NSString *)status{
    _status = status;
}
- (void) setPictuer:(NSString *)pictuer{
    _pictuer = pictuer;
}

- (NSString *)getNameDepartment{
    return _nameDepartment;
}
- (long long)getDepartmentId{
    return _departmentId;
}
- (NSString *)getStatus{
    return _status;
}
- (NSString *)getPictuer{
    return _pictuer;
}
@end
