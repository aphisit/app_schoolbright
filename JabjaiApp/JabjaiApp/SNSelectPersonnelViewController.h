//
//  SNSelectPersonnelViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNSelectTeacherModel.h"
#import "BSStudentNameRadioTableViewCell.h"
#import "SNLevelStudentViewController.h"
#import "SWRevealViewController.h"
#import "SNTimeOutViewController.h"
#import <CoreData/CoreData.h>

#import "SlideMenuController.h"

@interface SNSelectPersonnelViewController : UIViewController <SlideMenuControllerDelegate>
@property (nonatomic) NSInteger sendType;
@property (nonatomic) NSInteger sendGroup;
@property (nonatomic) NSInteger newsType;
@property (nonatomic) NSMutableArray* personnelImageArray;


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerSendNewsLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPersonnelLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)moveBack:(id)sender;
- (IBAction)actionNext:(id)sender;

@property (strong, nonatomic) NSArray<SNSelectTeacherModel *> *addPersonnelArrray;
@end
