//
//  TAStudentStatusTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 7/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>


@class TAStudentStatusTableViewCell;

@protocol TAStudentStatusTableViewCellDelegate <NSObject>

- (void)onSelectStatusButton:(TAStudentStatusTableViewCell *)tableViewCell atIndex:(NSInteger)index;

@end

@interface TAStudentStatusTableViewCell : UITableViewCell

@property (nonatomic, weak) id<TAStudentStatusTableViewCellDelegate> delegate;

@property (assign, nonatomic) NSInteger index;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageStuden;
@property (weak, nonatomic) IBOutlet UILabel *runNumber;
@property (weak, nonatomic) IBOutlet UILabel *lastName;
@property (weak, nonatomic) IBOutlet UIView *backgroundColorView;

- (void)updateStatus:(int)status;

- (IBAction)selectStatus:(id)sender;

@end
