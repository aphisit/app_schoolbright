//
//  UserData.m
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "UserData.h"

@interface UserData()

@end

@implementation UserData

+ (void)saveMasterUserID:(NSInteger)masterId {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:masterId forKey:MASTER_USERID];
    
    [defaults synchronize];
}

+ (NSInteger)getMasterUserID {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:MASTER_USERID] != nil) {
        NSInteger masterId = [defaults integerForKey:MASTER_USERID];
        return masterId;
    }
    else {
        return -1;
    }
}

+(void)saveUserID:(NSInteger)userid {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:userid forKey:USERID];
    
    [defaults synchronize];
}

+(NSInteger)getUserID {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:USERID] != nil) {
        NSInteger user_id = [defaults integerForKey:USERID];
        return user_id;
    }
    else {
        return -1;
    }
}

+ (void)saveSchoolId:(long long)schoolId {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@(schoolId) forKey:SCHOOLID];
    
    [defaults synchronize];
}

+ (long long)getSchoolId {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    long long schoolId = [[defaults objectForKey:SCHOOLID] longLongValue];
    
    return schoolId;
}

+(void)setUserLogin:(BOOL)login {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:login forKey:USERLOGIN];
    
    [defaults synchronize];
}

+(BOOL)isUserLogin {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:USERLOGIN] != nil) {
        BOOL login = [defaults boolForKey:USERLOGIN];
        return login;
    }
    else {
        return NO;
    }
}

+ (void)setUserImage:(NSString *)imageUrl {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:imageUrl forKey:USER_IMAGE];
    
    [defaults synchronize];
}

+ (NSString *)getUserImage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userImage = [defaults objectForKey:USER_IMAGE];
    
    return userImage;
}

+ (void)setFirstName:(NSString *)firstName {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:firstName forKey:FIRSTNAME];
    
    [defaults synchronize];
}

+ (NSString *)getFirstName {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *firstName = [defaults objectForKey:FIRSTNAME];
    
    return firstName;
}

+ (void)setLastName:(NSString *)lastName {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:lastName forKey:LASTNAME];
    
    [defaults synchronize];
}

+ (NSString *)getLastName {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *lastName = [defaults objectForKey:LASTNAME];
    
    return lastName;
}

+ (void)setAppVersion:(NSString *)appVersion {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:appVersion forKey:APPVERSION];
    
    [defaults synchronize];
}

+ (NSString *)getAppVersion {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *appVersion = [defaults objectForKey:APPVERSION];
    return appVersion;
}

+ (void)setSchoolImage:(NSData *)imageUrl {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:imageUrl forKey:SCHOOL_IMAGE];
    
    [defaults synchronize];
}

+ (NSData *)getSchoolImage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *schoolImage = [defaults objectForKey:SCHOOL_IMAGE];
    
    return schoolImage;
}

+ (void)setSchoolPic:(NSString *)schoolPic {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:schoolPic forKey:SCHOOL_PIC];
    
    [defaults synchronize];
}

+ (NSString *)getSchoolPic {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *schoolpic = [defaults objectForKey:SCHOOL_PIC];
    return schoolpic;
}
    
+ (void)setChangLanguage:(NSString *)changLanguage{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:changLanguage forKey:LANGUAGE];
        
        [defaults synchronize];
}
    
+ (NSString *)getChangLanguage{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *changLanguage = [defaults objectForKey:LANGUAGE];
    
    return changLanguage;
}

+ (void) setClientToken:(NSString *)clientToken{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:clientToken forKey:TOKEN];
    [defaults synchronize];
}

+ (NSString *)getClientToken{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *clientToken = [defaults objectForKey:TOKEN];
    return clientToken;
}

+ (void)setShowSomeId:(NSString *)switchSomeId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:switchSomeId forKey:SHOWSOMEID];
    [defaults synchronize];
}
+(NSString*)getShowSomeId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *clientToken = [defaults objectForKey:SHOWSOMEID];
    return clientToken;
}

+ (void)setUsername:(NSString *)username{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:username forKey:USERNAME];
    [defaults synchronize];
}
+ (NSString*)getUsername{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:USERNAME];
    return username;
}

+ (void)setPassword:(NSString *)password{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:password forKey:PASSWORD];
    [defaults synchronize];
}
+ (NSString*)getPassword{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *password = [defaults objectForKey:PASSWORD];
    return password;
}



+(void)setUserType:(USERTYPE)userType {
    
    int userTypeNumber = 0;
    
    if(userType == STUDENT) {
        userTypeNumber = 0;
    }
    else {
        userTypeNumber = 1;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:userTypeNumber forKey:USER_TYPE];
    
    [defaults synchronize];
}

+(USERTYPE)getUserType {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:USER_TYPE] != nil) {
        NSInteger userTypeNumber = [defaults integerForKey:USER_TYPE];
        
        if(userTypeNumber == 0) {
            return STUDENT;
        }
        else {
            return TEACHER;
        }
    }
    else {
        return USERTYPENULL;
    }
    
}

+(void)setAcademyType:(ACADEMYTYPE)academyType {
    
    int academyTypeNumber = 1;
    
    if(academyType == SCHOOL) {
        academyTypeNumber = 1;
    }
    else {
        academyTypeNumber = 2;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:academyTypeNumber forKey:ACADEMY_TYPE];
    
    [defaults synchronize];
}

+(ACADEMYTYPE)getAcademyType {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:ACADEMY_TYPE] != nil) {
        NSInteger academyTypeNumber = [defaults integerForKey:ACADEMY_TYPE];
        
        if(academyTypeNumber == 1) {
            return SCHOOL;
        }
        else {
            return UNIVERSITY;
        }
    }
    else {
        return ACADEMYTYPENULL;
    }
    
}

+ (void)setGender:(GENDERTYPE)genderType {
    
    int genderTypeNumber = 0;
    
    if(genderType == MALE) {
        genderTypeNumber = 0;
    }
    else {
        genderTypeNumber = 1;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:genderTypeNumber forKey:GENDER];
    
    [defaults synchronize];
}

+ (GENDERTYPE)getGender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:GENDER] != nil) {
        NSInteger genderTypeNumber = [defaults integerForKey:GENDER];
        
        if(genderTypeNumber == 0) {
            return MALE;
        }
        else {
            return FEMALE;
        }
    }
    else {
        return MALE;
    }
    
}


@end
