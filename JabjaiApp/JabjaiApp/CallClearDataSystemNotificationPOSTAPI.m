//
//  CallClearDataSystemNotificationPOSTAPI.m
//  JabjaiApp
//
//  Created by Mac on 5/3/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import "CallClearDataSystemNotificationPOSTAPI.h"
#import "APIURL.h"
#import <AFNetworking/AFNetworking.h>
#import "Utils.h"
@interface CallClearDataSystemNotificationPOSTAPI () {
    NSInteger connectCounter;
}

@end

@implementation CallClearDataSystemNotificationPOSTAPI
- (void)call:(NSString *)token{
    connectCounter = 0;
    [self clearData:token];
}
- (void)clearData:(NSString *)token{
    NSString *URLString = [APIURL getStatusClearDataOfNotification:token];
    
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self clearData:token];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callClearDataSystemNotificationPOSTAPI:)]) {
                    [self.delegate callClearDataSystemNotificationPOSTAPI:YES];
            }
        }
            
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callClearDataSystemNotificationPOSTAPI:)]) {
                [self.delegate callClearDataSystemNotificationPOSTAPI:NO];
            }
        }
    }];
}
@end
