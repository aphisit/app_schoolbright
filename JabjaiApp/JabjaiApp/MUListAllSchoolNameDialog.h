//
//  MUListAllSchoolNameDialog.h
//  JabjaiApp
//
//  Created by toffee on 27/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallMUListAllSchoolAPI.h"
#import "MUListAllSchoolTableViewCell.h"
#import "MUAllSchoolModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol MUListAllSchoolNameDialogDelegate <NSObject>
@optional
- (void)onAlertDialogClose:(MUAllSchoolModel*)listSchoolNameModel;
- (void)touchesBeganClose;
@end
@interface MUListAllSchoolNameDialog : UIViewController<UITableViewDataSource, UITableViewDelegate, CallMUListAllSchoolAPIDelegate, UISearchBarDelegate>
@property (retain, nonatomic) id<MUListAllSchoolNameDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end

NS_ASSUME_NONNULL_END
