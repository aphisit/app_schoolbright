//
//  specifyDateDialog.m
//  JabjaiApp
//
//  Created by toffee on 9/21/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "specifyDateDialog.h"
#import "Utils.h"
#import "UserData.h"
@interface specifyDateDialog ()
@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic, strong) NSDate *showDate;
@end

@implementation specifyDateDialog
- (IBAction)confirmAction:(id)sender {
    NSDateFormatter *dateFormatterStart = [Utils getDateFormatter];
    [dateFormatterStart setDateFormat:[Utils getSeverDateTimeFormat]];
    NSString *startDateStr = [dateFormatterStart stringFromDate:self.startDatePicker.date];
    NSDate *startDate = [dateFormatterStart dateFromString:startDateStr];
    NSDateFormatter *dateFormatterEnd = [Utils getDateFormatter];
    [dateFormatterEnd setDateFormat:[Utils getSeverDateTimeFormat]];
    NSString *endDateStr = [dateFormatterEnd stringFromDate:self.endDatePicker.date];
    NSDate *endDate = [dateFormatterEnd dateFromString:endDateStr];
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogConfirm:endDate:)]) {
        [self.delegate onAlertDialogConfirm:startDate endDate:endDate];
    }
}

- (IBAction)cancelAction:(id)sender {
     [self dismissDialog];
}

- (void)viewDidLoad {
    [super viewDidLoad];
     self.isShowing = NO;
    [self setLanguage];
    [self.startDatePicker setLocale: [NSLocale localeWithLocaleIdentifier:[UserData getChangLanguage]]];
    [self.endDatePicker setLocale: [NSLocale localeWithLocaleIdentifier:[UserData getChangLanguage]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm,*gradientCancel;
    //set color header
    [self.confirmBtn layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.confirmBtn.bounds;
    [self.confirmBtn.layer insertSublayer:gradientConfirm atIndex:0];
    
    //set color button next
    [self.cancelBtn layoutIfNeeded];
    gradientCancel = [Utils getGradientColorStatus:@"red"];
    gradientCancel.frame = self.cancelBtn.bounds;
    [self.cancelBtn.layer insertSublayer:gradientCancel atIndex:0];
}

- (void) setLanguage{
   self.headerSpacifyTimeLabel.text =  NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SPACIFY_TIME", nil, [Utils getLanguage], nil);
    self.headerToLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TO", nil, [Utils getLanguage], nil);
    [self.confirmBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_CONFIRME", nil, [Utils getLanguage], nil) forState:UIControlStateNormal];
    [self.cancelBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_CANCEL", nil, [Utils getLanguage], nil) forState:UIControlStateNormal];
}

-(void)showDialogInView:(UIView *)targetView {
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}


@end
