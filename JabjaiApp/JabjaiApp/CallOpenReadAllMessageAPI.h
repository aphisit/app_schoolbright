//
//  CallOpenReadAllMessageAPI.h
//  JabjaiApp
//
//  Created by toffee on 20/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CallOpenReadAllMessageAPI;
@protocol CallOpenReadAllMessageAPIDelegate <NSObject>
@optional
- (void)callOpenReadAllMessageAPI:(CallOpenReadAllMessageAPI *)classObj data:(NSString*)data sucess:(BOOL)sucess;
@end
@interface CallOpenReadAllMessageAPI : NSObject
@property (nonatomic, weak) id<CallOpenReadAllMessageAPIDelegate> delegate;
- (void)call:(long long)userId schoolid:(long long)schoolid;
@end

NS_ASSUME_NONNULL_END
