//
//  CallSNSendReplyCodeMessageBoxAPI.m
//  JabjaiApp
//
//  Created by Mac on 30/4/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import "CallSNSendReplyCodeMessageBoxAPI.h"
#import "Utils.h"
#import "APIURL.h"
@implementation CallSNSendReplyCodeMessageBoxAPI{
    NSInteger connectCounter;
    SNStatusSendReplyCodeModel *replyCodeModel;
}

- (void)call:(long long)userID messageID:(long long)messageID replyCode:(int)replyCode schoolID:(long long)schoolID{
    [self getStatusSendReplyCode:userID messageID:messageID replyCode:replyCode schoolID:schoolID];
}

- (void)getStatusSendReplyCode:(long long)userID messageID:(long long)messageID replyCode:(int)replyCode schoolID:(long long)schoolID{
    NSString *URLString = [APIURL getStatusReplyCodeMessageBox:userID messageID:messageID replyCode:replyCode schoolID:schoolID];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;
                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getStatusSendReplyCode:userID messageID:messageID replyCode:replyCode schoolID:schoolID];
            }
            else {
                isFail = YES;
                self->connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getStatusSendReplyCode:userID messageID:messageID replyCode:replyCode schoolID:schoolID];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
            }
            else {
                if (returnedData != nil) {
                    NSDictionary *dataDict = returnedData;
                    NSInteger statusCode;
                    NSString *messageStr;

                    //statusCode = [[dataDict objectForKey:@"statusCode"] integerValue] ;
                    if (![[dataDict objectForKey:@"message"] isKindOfClass:[NSNull class]]) {
                        messageStr = [[dataDict objectForKey:@"message"] stringValue];
                    }else{
                        messageStr = @"";
                    }
                    
                    if (![[dataDict objectForKey:@"statusCode"] isKindOfClass:[NSNull class]]) {
                        statusCode = [[dataDict objectForKey:@"statusCode"] integerValue];
                    }else{
                        statusCode = 0;
                    }

                    
                    self->replyCodeModel = [[SNStatusSendReplyCodeModel alloc] init];
                    self->replyCodeModel.message = messageStr;
                    self->replyCodeModel.statusCode = statusCode;

                    
                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callSNSendReplyCodeMessageBoxAPI:data:success:)]) {
                        [self.delegate callSNSendReplyCodeMessageBoxAPI:self data:self->replyCodeModel success:YES];
                        //[self.delegate callDetailMessageBoxAPI:self data:self->detailModel success:YES];
                    }
                   
        
                }
                
            
            }
        }
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callSNSendReplyCodeMessageBoxAPI:data:success:)]) {
                [self.delegate callSNSendReplyCodeMessageBoxAPI:self data:self->replyCodeModel success:NO];
                
            }
        }
        
    }];
}
@end
