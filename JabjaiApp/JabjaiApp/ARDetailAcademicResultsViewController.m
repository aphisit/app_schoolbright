//
//  ARDetailAcademicResultsViewController.m
//  JabjaiApp
//
//  Created by toffee on 21/2/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "ARDetailAcademicResultsViewController.h"
#import "Utils.h"
#import "UserData.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface ARDetailAcademicResultsViewController (){
    NSArray *basicCourseArray,*additionalCourseArray,*eventCourseArray,*noCountCreditArray;
}
@property (strong, nonatomic) CallGetDetailOfAcademicResultsAPI *callGetDetailOfAcademicResultsAPI;

@end
static NSString *cellIdentifier = @"SubjectCell";
static NSString *eventCellIdentifier = @"EventCell";
@implementation ARDetailAcademicResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_RESULT",nil,[Utils getLanguage],nil);
    self.headerSubjectLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_SUBJECT",nil,[Utils getLanguage],nil);
    self.headerGradeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_GRADE",nil,[Utils getLanguage],nil);
    self.headerBasicCourseLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_BASICCOURSE",nil,[Utils getLanguage],nil);
    self.headerAdditionalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_ADDITIONALCOURSE",nil,[Utils getLanguage],nil);
    self.headerNotCountCreditLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_NOT_COUNT_CREDIT",nil,[Utils getLanguage],nil);
    self.headerEventLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_EVENT",nil,[Utils getLanguage],nil);
    self.headerGPALabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_GPA",nil,[Utils getLanguage],nil);
    
    self.basicSubjectsTableView.delegate = self;
    self.basicSubjectsTableView.dataSource = self;
    [self.basicSubjectsTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ARGradeTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    self.additionalSubjectsTableView.delegate = self;
    self.additionalSubjectsTableView.dataSource = self;
    [self.additionalSubjectsTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ARGradeTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    self.noCountCreditTableView.delegate = self;
    self.noCountCreditTableView.dataSource = self;
    [self.noCountCreditTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ARGradeTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    self.eventTableView.delegate = self;
    self.eventTableView.dataSource = self;
    [self.eventTableView registerNib:[UINib nibWithNibName:NSStringFromClass([AREventCourseTableViewCell class]) bundle:nil] forCellReuseIdentifier:eventCellIdentifier];
    
    [self callDetailOfAcademicResultsAPI:[UserData getSchoolId] studentId:[UserData getUserID] year:self.yearId term:self.termStr];

}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout {
    //set color header
    CAGradientLayer *gradientOrange;
    [self.headerView layoutIfNeeded];
    gradientOrange = [Utils getGradientColorHeader];
    gradientOrange.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientOrange atIndex:0];
}

//CallGetDetailOfAcademicResultsAPI
-(void)callDetailOfAcademicResultsAPI:(long long)schoolId studentId:(long long)studentId year:(NSString *)year term:(NSString*)term{
    [self showIndicator];
    if(self.callGetDetailOfAcademicResultsAPI != nil) {
          self.callGetDetailOfAcademicResultsAPI = nil;
    }
      self.callGetDetailOfAcademicResultsAPI = [[CallGetDetailOfAcademicResultsAPI alloc] init];
      self.callGetDetailOfAcademicResultsAPI.delegate = self;
      [self.callGetDetailOfAcademicResultsAPI call:schoolId studentId:studentId year:year term:term];
}
-(void)CallGetDetailOfAcademicResultsAPI:(CallGetDetailOfAcademicResultsAPI *)classObj data:(ARDetailModel *)data success:(BOOL)success{
    [self stopIndicator];
    if (data != nil && success) {
        basicCourseArray = [data getBasicCoursesArray];
        additionalCourseArray = [data getAdditionalCoursesArray];
        eventCourseArray = [data getEventCoursesArray];
        noCountCreditArray = [data getNoCountCreditArray];
        
        if (![[data getSchoolImg] isEqualToString:@""]) {
            [self.imageSchool sd_setImageWithURL:[NSURL URLWithString:[data getSchoolImg]]];
        }else{
            [self.imageSchool setImage:[UIImage imageNamed:@"school"]];
        }
        self.imageSchool.layer.cornerRadius = self.imageSchool.frame.size.width / 2;
        self.imageSchool.clipsToBounds = YES;
       
        self.schoolNameLabel.text = [data getSchoolNameTH];
        self.schoolNameENLabel.text = [data getSchoolNameEN];
        
        self.yearLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_SCHOOL_YEAR",nil,[Utils getLanguage],nil),[@([data getYear])stringValue]];
        self.nameLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_NAME",nil,[Utils getLanguage],nil),[data getName]];
        self.studentIDLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_STUDENTID",nil,[Utils getLanguage],nil),[data getStudentId]];
       // self.branchLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_MAJOR",nil,[Utils getLanguage],nil),[data getBranch]];
        self.classRoomLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_CLASS",nil,[Utils getLanguage],nil),[data getSubLevelName]];
        if ([[data getBranch] isEqualToString:@""]||[[data getBranch] isEqualToString:@"-"]) {
            self.branchLabel.text = @"";
        }else{
            self.branchLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_MAJOR",nil,[Utils getLanguage],nil),[data getBranch]];
        }
        if ([[data getTerm] isEqualToString:@"All"]||[[data getTerm] isEqualToString:@""]) {
            self.termLabel.text = @"";
        }else{
            self.termLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_SEMESTERS",nil,[Utils getLanguage],nil),[data getTerm]];
        }
        self.gradeAverageLabel.text = [data getGradeAverage];
    }
    
    [self.basicSubjectsTableView reloadData];
    [self.additionalSubjectsTableView reloadData];
    [self.noCountCreditTableView reloadData];
    [self.eventTableView reloadData];
    NSLog(@"xxx");
}

#pragma mark - TableViewDelegate
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.basicSubjectsTableView) {
        if (basicCourseArray.count == 0) {
            self.showStatusBasicSubjectLabel.hidden = NO;
        }else{
            self.showStatusBasicSubjectLabel.hidden = YES;
        }
        return basicCourseArray.count;
    }
    else if (tableView == self.additionalSubjectsTableView){
        if (additionalCourseArray.count == 0) {
            self.showStatusAdditionalSubjectLabel.hidden = NO;
        }else{
            self.showStatusAdditionalSubjectLabel.hidden = YES;
        }
        return additionalCourseArray.count;
    }
    else if (tableView == self.noCountCreditTableView){
        if (noCountCreditArray.count == 0) {
            self.showStatusNoCountCreditLabel.hidden = NO;
        }else{
            self.showStatusNoCountCreditLabel.hidden = YES;
        }
        return noCountCreditArray.count;
    }
    else{
        if (eventCourseArray.count == 0) {
            self.showStatusEventLabel.hidden = NO;
        }else{
            self.showStatusEventLabel.hidden = YES;
        }
        return eventCourseArray.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.basicSubjectsTableView) {
        ARGradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        ARSubjectScoreModel *model = [basicCourseArray objectAtIndex:indexPath.row];
        cell.courseCodeLabel.text = [model getCourseCode];
        cell.courseNameLabel.text = [model getCourseName];
        if ([[model getStatus] isEqualToString:@""]) {
            cell.scoreLabel.text = [model getScore];
            cell.gradeLabel.text = [model getGrade];
        }else{
            cell.scoreLabel.text = @"";
            cell.gradeLabel.text = [model getStatus];
        }
        return cell;
    }
    else if (tableView == self.additionalSubjectsTableView){
        ARGradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        ARSubjectScoreModel *model = [additionalCourseArray objectAtIndex:indexPath.row];
        cell.courseCodeLabel.text = [model getCourseCode];
        cell.courseNameLabel.text = [model getCourseName];
        if ([[model getStatus] isEqualToString:@""]) {
            cell.scoreLabel.text = [model getScore];
            cell.gradeLabel.text = [model getGrade];
        }else{
            cell.scoreLabel.text = @"";
            cell.gradeLabel.text = [model getStatus];
        }
        return cell;
    }
    else if (tableView == self.noCountCreditTableView){
          ARGradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
          ARSubjectScoreModel *model = [noCountCreditArray objectAtIndex:indexPath.row];
          cell.courseCodeLabel.text = [model getCourseCode];
          cell.courseNameLabel.text = [model getCourseName];
          if ([[model getStatus] isEqualToString:@""]) {
              cell.scoreLabel.text = [model getScore];
              cell.gradeLabel.text = [model getGrade];
          }else{
              cell.scoreLabel.text = @"";
              cell.gradeLabel.text = [model getStatus];
          }
          return cell;
    }
    else {
        AREventCourseTableViewCell *eventCell = [tableView dequeueReusableCellWithIdentifier:eventCellIdentifier forIndexPath:indexPath];
        ARSubjectScoreModel *model = [eventCourseArray objectAtIndex:indexPath.row];
        eventCell.courseCodeLabel.text = [model getCourseCode];
        eventCell.courseNameLabel.text = [model getCourseName];
        eventCell.statusLabel.text = [model getStatus];
        return eventCell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (tableView == self.basicSubjectsTableView) {
            if (basicCourseArray.count == 0) {
                self.basicSubjectConstraint.constant = 70;
            }else{
                self.basicSubjectConstraint.constant = 70 * basicCourseArray.count;
            }
        }else if(tableView == self.additionalSubjectsTableView){
            if (additionalCourseArray.count == 0) {
                self.additionalSubjectConstraint.constant = 70;
            }else{
                self.additionalSubjectConstraint.constant = 70 * additionalCourseArray.count ;
            }
        }else if(tableView == self.eventTableView){
            if (eventCourseArray.count == 0) {
                self.eventConstraint.constant = 70;
            }else{
                self.eventConstraint.constant = 70 * eventCourseArray.count;
            }
        }else{
            if (noCountCreditArray.count == 0) {
                self.noCountCreditConstraint.constant = 70;
            }else{
                self.noCountCreditConstraint.constant = 70 * noCountCreditArray.count;
            }
        }
        return 70;
    }else{
        if (tableView == self.basicSubjectsTableView) {
            if (basicCourseArray.count == 0) {
                self.basicSubjectConstraint.constant = 100;
            }else{
                self.basicSubjectConstraint.constant = 100 * basicCourseArray.count;
            }
        }else if(tableView == self.additionalSubjectsTableView){
            if (additionalCourseArray.count == 0) {
                self.additionalSubjectConstraint.constant = 100;
            }else{
                self.additionalSubjectConstraint.constant = 100 * additionalCourseArray.count ;
            }
        }else if(tableView == self.eventTableView){
            if (eventCourseArray.count == 0) {
                self.eventConstraint.constant = 100;
            }else{
                self.eventConstraint.constant = 100 * eventCourseArray.count;
            }
        }else{
            if (noCountCreditArray.count == 0) {
                self.noCountCreditConstraint.constant = 100;
            }else{
                self.noCountCreditConstraint.constant = 100 * noCountCreditArray.count ;
            }
        }
        return 100;
    }
    
   
}

#pragma mark - UIActivityIndicatorView
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)suggestionAction:(id)sender {
    ARSuggestionViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ARSuggestionStoryboard"];
    viewController.termStr = self.termStr;
    viewController.yearId = self.yearId;
    viewController.yearStr = self.yearStr;
    viewController.termArray = self.termArray;
    viewController.yearIdArray = self.yearIdArray;
    viewController.yearNumberArray = self.yearNumberArray;
       [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (IBAction)moveBackAction:(id)sender {
    ARSelectTermViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ARSelectTermStoryboard"];
    viewController.termStr = self.termStr;
    viewController.yearId = self.yearId;
    viewController.yearStr = self.yearStr;
    viewController.termArray = self.termArray;
    viewController.yearIdArray = self.yearIdArray;
    viewController.yearNumberArray = self.yearNumberArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
