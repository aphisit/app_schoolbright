//
//  reachabilityChangedDialog.h
//  JabjaiApp
//
//  Created by toffee on 5/21/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol reachabilityChangedDialogDelegate <NSObject>

@optional
- (void)onAlertDialogClose;

@end
@interface reachabilityChangedDialog : UIViewController
@property (retain, nonatomic) id<reachabilityChangedDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
- (IBAction)closeDialog:(id)sender;


-(void)showDialogInView:(UIView *)targetView message:(NSString*)message;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end
