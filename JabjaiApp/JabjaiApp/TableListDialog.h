//
//  TableListDialog.h
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableListViewCell.h"

@protocol  TableListDialogDelegate

-(void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index;

@end

@interface TableListDialog : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) id<TableListDialogDelegate> delegate;

@property (nonatomic, strong) NSArray *dataArr;

@property (weak, nonatomic) IBOutlet UITableView *tblView;

- (id)initWithRequestCode:(NSString *)requestCode;
- (void)showDialogInView:(UIView *)targetView dataArr:(NSArray *)dataArr;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
