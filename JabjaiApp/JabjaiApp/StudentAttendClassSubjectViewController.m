//
//  StudentAttendClassSubjectViewController.m
//  JabjaiApp
//
//  Created by mac on 1/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "StudentAttendClassSubjectViewController.h"
#import "UTeachingYearModel.h"
#import "APIURL.h"
#import "Utils.h"
#import "DateUtility.h"
#import "SWRevealViewController.h"
#import "SubjectWithSectionTableViewCell.h"
#import "AttendClassReportViewController.h"
#import "Constant.h"

@interface StudentAttendClassSubjectViewController () {
    
    NSMutableArray *teachingYearArray;
    NSMutableArray *subjectArray;
    
    NSNumber *selected_year;
    NSString *selected_semester;
    
    int selected_sectionID;
    NSString *selected_subjectName;
    
    NSInteger connectCounter;
}

@property (strong, nonatomic) TeachingSubjectDialog *teachingSubjectDialog;

@end

@implementation StudentAttendClassSubjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    // register table nib
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([SubjectWithSectionTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.tblView.tableFooterView = [[UIView alloc] init];
    self.tblView.dataSource = self;
    self.tblView.delegate = self;
    
    subjectArray = [[NSMutableArray alloc] init];
    
    connectCounter = 0;
    
    [self callAPIGetStudentSubjectListAllYear];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return subjectArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SubjectWithSectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    USubjectModel *subjectModel = [subjectArray objectAtIndex:indexPath.row];
    NSArray<USectionModel *> *sections = subjectModel.sections;
    
    NSString *subjectTitle = [NSString stringWithFormat:@"%@ (%@)", subjectModel.subjectName, subjectModel.subjectID];
    
    NSString *sectionText = @"";
    if(sections == nil || sections.count == 0) {
        sectionText = @"Section : ";
    }
    else {
        sectionText = [NSString stringWithFormat:@"Section : %@", [sections objectAtIndex:0].sectionName];
    }
    
    cell.subjectNameLabel.text = subjectTitle;
    cell.sectionLabel.text = sectionText;
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    USubjectModel *subjectModel = [subjectArray objectAtIndex:indexPath.row];
    NSArray<USectionModel *> *sections = subjectModel.sections;
    
    NSString *subjectTitle = [NSString stringWithFormat:@"%@ (%@)", subjectModel.subjectName, subjectModel.subjectID];
    int sectionID = [sections objectAtIndex:0].sectionID;
    
    AttendClassReportViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AttendClassReportStoryboard"];
    viewController.sectionID = sectionID;
    viewController.subjectName = subjectTitle;
    viewController.teachingYearArray = teachingYearArray;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

#pragma mark - GetAPIData

- (void)callAPIGetStudentSubjectListAllYear {
    [self getStudentSubjectListAllYearData];
}

- (void)getStudentSubjectListAllYearData {
    NSString *URLString = [APIURL getStudentSubjectListAllYearUrl];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self callAPIGetStudentSubjectListAllYear];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetStudentSubjectListAllYear];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetStudentSubjectListAllYear];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                teachingYearArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    
                    NSDictionary *yearDataDict = [returnedArray objectAtIndex:i];
                    
                    int yearID = [[yearDataDict objectForKey:@"YearId"] intValue];
                    NSMutableString *yearName = [[NSMutableString alloc] initWithFormat:@"%@", [yearDataDict objectForKey:@"Year"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) yearName);
                    
                    NSArray *termArray = [yearDataDict objectForKey:@"lTerm"];
                    
                    NSMutableArray<USemesterModel *> *semesters = [[NSMutableArray alloc] init];
                    
                    for(int j=0; j<termArray.count; j++) {
                        
                        NSDictionary *termDataDict = [termArray objectAtIndex:j];
                        
                        NSMutableString *semesterID = [[NSMutableString alloc] initWithFormat:@"%@", [termDataDict objectForKey:@"TermId"]];
                        NSMutableString *semesterName = [[NSMutableString alloc] initWithFormat:@"%@", [termDataDict objectForKey:@"Term"]];
                        NSMutableString *semesterStartDateXML = [[NSMutableString alloc] initWithFormat:@"%@", [termDataDict objectForKey:@"dStart"]];
                        NSMutableString *semesterEndDateXML = [[NSMutableString alloc] initWithFormat:@"%@", [termDataDict objectForKey:@"dEnd"]];
                        
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) semesterID);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) semesterName);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) semesterStartDateXML);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) semesterEndDateXML);
                        
                        NSDate *startDate = [Utils parseServerDateStringToDate:semesterStartDateXML];
                        NSDate *endDate = [Utils parseServerDateStringToDate:semesterEndDateXML];
                        
                        NSArray *subjectsArray = [termDataDict objectForKey:@"lPlane"];
                        
                        NSMutableArray<USubjectModel *> *subjects = [[NSMutableArray alloc] init];
                        
                        for(int k=0; k<subjectsArray.count; k++) {
                            
                            NSDictionary *subjectDataDict = [subjectsArray objectAtIndex:k];
                            
                            NSMutableString *subjectID = [[NSMutableString alloc] initWithFormat:@"%@", [subjectDataDict objectForKey:@"PlaneId"]];
                            NSMutableString *subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [subjectDataDict objectForKey:@"Plane"]];
                            
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectID);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                            
                            NSArray *sectionArray = [subjectDataDict objectForKey:@"lSection"];
                            
                            NSMutableArray<USectionModel *> *sections = [[NSMutableArray alloc] init];
                            
                            for(int l=0; l<sectionArray.count; l++) {
                                NSDictionary *sectionDataDict = [sectionArray objectAtIndex:l];
                                
                                int sectionID = [[sectionDataDict objectForKey:@"SectionId"] intValue];
                                NSMutableString *sectionName = [[NSMutableString alloc] initWithFormat:@"%@", [sectionDataDict objectForKey:@"Section"]];
                                
                                CFStringTrimWhitespace((__bridge CFMutableStringRef) sectionName);
                                
                                USectionModel *sectionModel = [[USectionModel alloc] init];
                                sectionModel.sectionID = sectionID;
                                sectionModel.sectionName = sectionName;
                                
                                [sections addObject:sectionModel];
                            }
                            
                            USubjectModel *subjectModel = [[USubjectModel alloc] init];
                            subjectModel.subjectID = subjectID;
                            subjectModel.subjectName = subjectName;
                            subjectModel.sections = sections;
                            
                            [subjects addObject:subjectModel];
                            
                        }
                        
                        USemesterModel *semesterModel = [[USemesterModel alloc] init];
                        semesterModel.semesterID = semesterID;
                        semesterModel.semesterName = semesterName;
                        semesterModel.subjects = subjects;
                        semesterModel.startDate = startDate;
                        semesterModel.endDate = endDate;
                        
                        [semesters addObject:semesterModel];
                        
                    }
                    
                    UTeachingYearModel *teachingYearModel = [[UTeachingYearModel alloc] init];
                    teachingYearModel.yearID = yearID;
                    teachingYearModel.yearName = yearName;
                    teachingYearModel.semesters = semesters;
                    
                    [teachingYearArray addObject:teachingYearModel];
                    
                }
                
                //Find max year
                
                for(UTeachingYearModel *model in teachingYearArray) {
                    NSNumber *yearNumber = [[NSNumber alloc] initWithInt:[model.yearName intValue]];
                    
                    if(selected_year == nil || yearNumber > selected_year) {
                        selected_year = yearNumber;
                    }
                    
                }
                
                [self performStudentSubjectListAllYearData];
                
                if(self.teachingSubjectDialog != nil && [self.teachingSubjectDialog isDialogShowing]) {
                    [self.teachingSubjectDialog dismissDialog];
                    self.teachingSubjectDialog = nil;
                }
                
                self.teachingSubjectDialog = [[TeachingSubjectDialog alloc] initWithUTeachingYearModelArray:teachingYearArray];
                self.teachingSubjectDialog.delegate = self;
                [self.teachingSubjectDialog showDialogInView:self.view title:@"Filter"];
                
            }

        }

    }];    
}

#pragma mark - Manage Data

- (void) performStudentSubjectListAllYearData {
    
    if(teachingYearArray == nil) {
        return;
    }
    
    if(subjectArray != nil) {
        [subjectArray removeAllObjects];
    }
    
    subjectArray = [[NSMutableArray alloc] init];
    
    for(UTeachingYearModel *teachingYearModel in teachingYearArray) {
        
        if(selected_year != nil) {
            
            int year = [teachingYearModel.yearName intValue];
            
            if(year == selected_year.intValue) {
                
                if(selected_semester == nil || [selected_semester isEqualToString:@"ทั้งหมด"]) {
                    
                    NSArray *semesters = teachingYearModel.semesters;
                    
                    for(USemesterModel *semesterModel in semesters) {
                        NSArray *subjects = semesterModel.subjects;
                        
                        for(USubjectModel *subjectModel in subjects) {
                            [subjectArray addObject:subjectModel];
                        }
                        
                    }
                    
                }
                else {
                    
                    NSArray *semesters = teachingYearModel.semesters;
                    
                    for(USemesterModel *semesterModel in semesters) {
                        
                        if([selected_semester isEqualToString:semesterModel.semesterName]) {
                            
                            NSArray *subjects = semesterModel.subjects;
                            
                            for(USubjectModel *subjectModel in subjects) {
                                [subjectArray addObject:subjectModel];
                            }
                            
                            break;
                        }
                        
                    }
                    
                }
                
                break;
                
            }
            
        }
        
    }
    
    [self.tblView reloadData];
}

#pragma mark - TeachingSubjectDialogDelegate

- (void)applyFilter:(NSNumber *)schoolYear semester:(NSString *)semester {
    
    selected_year = schoolYear;
    selected_semester = semester;
    
    [self performStudentSubjectListAllYearData];
}

- (void)onTeachingSubjectDialogClose {
    
}


#pragma mark - Class functions
- (IBAction)openDrawer:(id)sender {
    
    [self.revealViewController revealToggle:self.revealViewController];
    
}

- (IBAction)showFilterDialog:(id)sender {
    
    if(self.teachingSubjectDialog != nil) {
        
        [self.teachingSubjectDialog showDialogInView:self.view title:@"Filter"];
        
    }
    
}
@end
