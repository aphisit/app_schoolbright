//
//  CallGetAmountUnreadLeaveAPI.m
//  JabjaiApp
//
//  Created by Mac on 25/6/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallGetAmountUnreadLeaveAPI.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetAmountUnreadLeaveAPI(){
    NSInteger connectCounter;
}

@end

@implementation CallGetAmountUnreadLeaveAPI
-(void)call:(long long)userID schoolID:(long long)schoolID{
    connectCounter = 0;
    [self getAmountLeaveUnread:userID schoolID:schoolID];
}

-(void)getAmountLeaveUnread:(long long)userID schoolID:(long long)schoolID{
    NSString *urlString = [APIURL getAmountUnreadLeave:userID schoolid:schoolID];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getAmountLeaveUnread:userID schoolID:schoolID];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
        }
        else{
            
            NSString *amount = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetAmountUnreadLeaveAPI:)]) {
                [self.delegate callGetAmountUnreadLeaveAPI:amount];
            }
        }
        if (isFail) {
             if (self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetAmountUnreadLeaveAPI:)]) {
                           [self.delegate callGetAmountUnreadLeaveAPI:@"0"];
                       }
        }
        
    }];
}
@end
