//
//  CallTeacherTimeTableDetailAPI.h
//  JabjaiApp
//
//  Created by toffee on 23/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SScheduleDetailModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallTeacherTimeTableDetailAPI;

@protocol CallTeacherTimeTableDetailAPIDelegate <NSObject>

@optional
- (void)callTeacherTimeTableDetailAPI:(CallTeacherTimeTableDetailAPI *)classObj data:(SScheduleDetailModel *)data success:(BOOL)success;
@end

@interface CallTeacherTimeTableDetailAPI : NSObject
@property (nonatomic, weak) id<CallTeacherTimeTableDetailAPIDelegate> delegate;
- (void)call:(long long)subjectId userId:(long long)userId date:(NSDate*)date schoolid:(long long)schoolid;
@end

NS_ASSUME_NONNULL_END
