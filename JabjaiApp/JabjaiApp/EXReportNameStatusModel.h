//
//  EXReportNameStatusModel.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Status
 * 0 : on time
 * 1 : late
 * 3 : absence
 * 4 : personal leave
 * 5 : sick leave
 * 6 : event
 **/

@interface EXReportNameStatusModel : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSInteger status;

- (void)setName:(NSString *)name;
- (void)setStatus:(NSInteger)status;

- (NSString *)getName;
- (NSInteger)getStatus;

@end
