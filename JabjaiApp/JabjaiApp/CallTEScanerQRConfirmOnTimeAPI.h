//
//  CallTEScanerQRConfirmOnTimeAPI.h
//  JabjaiApp
//
//  Created by toffee on 31/7/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CallTEScanerQRConfirmOnTimeAPI;
@protocol CallTEScanerQRConfirmOnTimeAPIDelegate <NSObject>
@optional
- (void)callTEScanerQROnTimeConfirm:(CallTEScanerQRConfirmOnTimeAPI *)classObj resCode:(NSInteger)resCode success:(BOOL)success;
@end
@interface CallTEScanerQRConfirmOnTimeAPI : NSObject
@property (nonatomic, weak) id<CallTEScanerQRConfirmOnTimeAPIDelegate> delegate;
- (void)call:(long long)schoolId userId:(long long)userId idCode:(NSString *)idCode;
@end

NS_ASSUME_NONNULL_END
