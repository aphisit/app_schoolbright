//
//  SNClassRoomviewTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 9/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
@class SNClassRoomviewTableViewCell;
@protocol SNClassRoomviewRadioTableViewCellDelegate <NSObject>
- (void)onPressRadioButton:(SNClassRoomviewTableViewCell *)tableViewCell atIndex:(NSInteger)index;
@end
@interface SNClassRoomviewTableViewCell : UITableViewCell
@property (weak, nonatomic) id<SNClassRoomviewRadioTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UIButton *radioButton;

@property (nonatomic) NSInteger index;
- (void)setRadioButtonSelected;
- (void)setRadioButtonSelectedWithType:(int) type;
- (void)setRadioButtonClearSelected;

- (IBAction)actionPressRadioButton:(id)sender;


@end
