//
//  TeacherTimeTableModel.h
//  JabjaiApp
//
//  Created by mac on 11/21/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeacherTimeTableModel : NSObject

@property (assign, nonatomic) long long subjectID;
@property (strong, nonatomic) NSString *subjectName;
@property (strong, nonatomic) NSString *startTime;
@property (strong, nonatomic) NSString *endTime;
@property (strong, nonatomic) NSString *codeSubject;
@end
