//
//  ReportInOutStatusClassroomModel.m
//  JabjaiApp
//
//  Created by toffee on 28/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutStatusClassroomModel.h"

@implementation ReportInOutStatusClassroomModel

@synthesize subLevelName = _subLevelName;
@synthesize subLevelId = _subLevelId;
@synthesize logData = _logData;

- (void) setSubLevelName:(NSString *)subLevelName{
    _subLevelName = subLevelName;
}
- (void) setSubLevelId:(long long)subLevelId{
    _subLevelId = subLevelId;
}
- (void) setLogData:(NSArray *)logData{
    _logData = logData;
}

- (NSString*)getSubLevelName{
    return _subLevelName;
}
- (long long)getSubLevelId{
    return _subLevelId;
}
- (NSArray*)getLogData{
    return _logData;
}

@end
