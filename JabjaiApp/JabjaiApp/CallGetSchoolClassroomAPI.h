//
//  CallGetSchoolClassroomAPI.h
//  JabjaiApp
//
//  Created by mac on 7/5/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SchoolClassroomModel.h"

@class CallGetSchoolClassroomAPI;

@protocol CallGetSchoolClassroomAPIDelegate <NSObject>

@optional
- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success;

@end

@interface CallGetSchoolClassroomAPI : NSObject

@property (nonatomic, weak) id<CallGetSchoolClassroomAPIDelegate> delegate;

- (void)call:(long long)schoolId classLevelId:(long long)classLevelId;

@end
