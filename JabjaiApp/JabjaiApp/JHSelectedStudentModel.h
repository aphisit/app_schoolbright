//
//  JHSelectedStudentModel.h
//  JabjaiApp
//
//  Created by toffee on 11/22/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JHSelectedStudentModel : NSObject
@property (nonatomic) long long studentId;
@property (nonatomic) NSString *studentName;
@property (nonatomic) NSString *pic;
@property (nonatomic) long long picverion;
@property (nonatomic) BOOL selected;

- (void)setStudentId:(long long)studentId;
- (void)setStudentName:(NSString *)studentName;
- (void)setStudentPic:(NSString *)pic;
- (void)setPicverion:(long long)picverion;
- (void)setSelected:(BOOL)selected;

- (long long)getStudentId;
- (NSString *)getStudentName;
- (NSString *)getStudentPic;
- (long long)getPicverion;
- (BOOL)isSelected;

@end
