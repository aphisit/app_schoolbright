//
//  ReportHomeWorkDetailModel.h
//  JabjaiApp
//
//  Created by mac on 1/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportHomeWorkDetailModel : NSObject

@property (assign, nonatomic) long long messageID;
@property (assign, nonatomic) long long homeworkID;
@property (assign, nonatomic) long long userID;
@property (assign, nonatomic) NSArray *homework;
@property (strong, nonatomic) NSString *subjectName;
@property (strong, nonatomic) NSString *teacherName;
@property (strong, nonatomic) NSDate *dateEndSend;
@property (strong, nonatomic) NSDate *dateStartSend;
@property (strong, nonatomic) NSDate *dateOrder;
@property (strong, nonatomic) NSString *detailHomework;

@end
