//
//  CallStudentTimeTableDetailAPI.h
//  JabjaiApp
//
//  Created by toffee on 16/5/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SScheduleDetailModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallStudentTimeTableDetailAPI;

@protocol CallStudentTimeTableDetailAPIDelegate <NSObject>

@optional
- (void)callStudentTimeTableDetailAPI:(CallStudentTimeTableDetailAPI *)classObj data:(SScheduleDetailModel *)data success:(BOOL)success;
@end

@interface CallStudentTimeTableDetailAPI : NSObject
@property (nonatomic, weak) id<CallStudentTimeTableDetailAPIDelegate> delegate;
- (void)call:(long long)subjectId userId:(long long)userId date:(NSDate*)date schoolid:(long long)schoolid;
@end

NS_ASSUME_NONNULL_END
