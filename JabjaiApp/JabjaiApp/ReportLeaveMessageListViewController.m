//
//  ReportLeaveMessageListViewController.m
//  JabjaiApp
//
//  Created by mac on 4/10/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportLeaveMessageListViewController.h"
#import "ReportLeaveAllDataSelectClassViewController.h"

#import "NoticeReportDateMessageDetailViewController.h"
#import "NoticeReportDateMessageTableViewCell.h"
#import "ProgressTableViewCell.h"
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"
#import "NavigationViewController.h"
#import "ReportLeaveDetailViewController.h"
#import "AppDelegate.h"

@interface ReportLeaveMessageListViewController (){
    
    UIColor *grayColor;
    NoticeReportDateMessageModel *noticeReportDateMessageModel;
    NSMutableArray<NoticeReportDateMessageModel *> *leaveArray;
    NSInteger connectCounter;
    UIColor *acceptLeave;
    UIColor *declineLeave;
    UIColor *waitLeave;
    UIColor *dodgerBlue, *aqua, *monaLisa, *bitterSweet, *ripTide, *mountainMeadow, *perfume, *heliotRope, *alto, *dustyGray, *peachOrange, *texasRose;
    UIColor *cornFlower, *mariner;
    
}
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@end

@implementation ReportLeaveMessageListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setLanguage];
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = grayColor;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NoticeReportDateMessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    acceptLeave = [UIColor colorWithRed:0.13 green:0.86 blue:0.55 alpha:1.0];
    declineLeave = [UIColor colorWithRed:1.00 green:0.32 blue:0.32 alpha:1.0];
    waitLeave = [UIColor colorWithRed:0.26 green:0.78 blue:0.96 alpha:1.0];
    
    // 08/05/2561
    dodgerBlue = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0]; // Now Full
    aqua = [UIColor colorWithRed:0.21 green:0.89 blue:1.00 alpha:1.0]; // Now Select
    monaLisa = [UIColor colorWithRed:1.00 green:0.68 blue:0.62 alpha:1.0]; // Color status 0 Before Select
    bitterSweet = [UIColor colorWithRed:1.00 green:0.48 blue:0.38 alpha:1.0]; // Color status 0 After Select
    ripTide = [UIColor colorWithRed:0.47 green:0.93 blue:0.71 alpha:1.0]; // Color status 1 Before Select
    mountainMeadow = [UIColor colorWithRed:0.13 green:0.76 blue:0.46 alpha:1.0]; // Color status 1 After Select
    perfume = [UIColor colorWithRed:0.80 green:0.62 blue:0.98 alpha:1.0]; // Color status 2 Before Select
    heliotRope = [UIColor colorWithRed:0.75 green:0.51 blue:1.00 alpha:1.0]; // Color status 2 After Select
    alto = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1.0]; // Color status 3 Before Select
    dustyGray = [UIColor colorWithRed:0.60 green:0.60 blue:0.60 alpha:1.0]; // Color status 3 After Select
    peachOrange = [UIColor colorWithRed:1.00 green:0.85 blue:0.59 alpha:1.0]; // Color status 4 Before Select
    texasRose = [UIColor colorWithRed:1.00 green:0.75 blue:0.31 alpha:1.0]; // Color status 4 After Select
    cornFlower = [UIColor colorWithRed:0.58 green:0.80 blue:0.92 alpha:1.0]; // Color status 0 Before Select
    mariner = [UIColor colorWithRed:0.13 green:0.49 blue:0.76 alpha:1.0]; // Color status 1 After Select
    [self getUserLeaveDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLanguage{
   self.headerLeaveTotalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_LEAVE_ALL",nil,[Utils getLanguage],nil);
    self.headerDayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_DAY",nil,[Utils getLanguage],nil);
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(leaveArray == nil) {
        return 0;
    }
    else {
        return leaveArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NoticeReportDateMessageModel *model = [leaveArray objectAtIndex:indexPath.row];
    NoticeReportDateMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.statusColor.layer.cornerRadius = cell.statusColor.frame.size.width / 2;
    cell.statusColor.clipsToBounds = YES;
    cell.statusColor.layer.shouldRasterize = YES;
    
    if (model.status == 0) {
        cell.statusColor.backgroundColor = bitterSweet;
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_NO_ALLOW",nil,[Utils getLanguage],nil);
    }
    else if (model.status == 1){
        cell.statusColor.backgroundColor = mountainMeadow;
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_ALLOW",nil,[Utils getLanguage],nil);
    }
    else if (model.status == 3){
        cell.statusColor.backgroundColor = mariner;
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_WAIT_ALLOW",nil,[Utils getLanguage],nil);
    }
    else if (model.status == 4){
        cell.statusColor.backgroundColor = heliotRope;
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_CANCEL",nil,[Utils getLanguage],nil);
    }
    cell.leaveNameLabel.text = [model leaveType];
    cell.leaveDateLabel.text = [Utils dateInStringFormat:model.date];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NoticeReportDateMessageModel *model = [leaveArray objectAtIndex:indexPath.row];
    if(self.delegate && [self.delegate respondsToSelector:@selector(showReportLeaveDetailLeave:page:)]) {
           [self.delegate showReportLeaveDetailLeave:model.letterID page:3];
       }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void)getUserLeaveDetail{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getUserLeaveDetailWithUserID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUserLeaveDetail];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserLeaveDetail];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserLeaveDetail];
                }
                else {
                    connectCounter = 0;
                }
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                leaveArray = [[NSMutableArray alloc] init];
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long letterID = [[dataDict objectForKey:@"letterId"]longLongValue];
                    
                    int status = [[dataDict objectForKey:@"status"] intValue];
                    
                    NSMutableString *countDate, *leaveType;
                    
                    if (![[dataDict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                        leaveType = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"type"]];
                    }
                    else{
                        leaveType = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"totalLeaveDay"] isKindOfClass:[NSNull class]]) {
                        countDate = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"totalLeaveDay"]];
                    }
                    
                    else{
                        countDate = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getXMLDateFormat]];
                    
                    NSDateFormatter *formatter2 = [Utils getDateFormatter];
                    [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSMutableString *leaveDate = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Date"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveType);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveDate);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) countDate);
                    
                    NSRange dotRange = [leaveDate rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:leaveDate];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:leaveDate];
                    }
                    NoticeReportDateMessageModel *model = [[NoticeReportDateMessageModel alloc] init];
                    model.letterID = letterID;
                    model.leaveType = leaveType;
                    model.date = messageDate;
                    model.countLeave = countDate;
                    model.status = status;
                    
                    noticeReportDateMessageModel = model;
                    [self performData];
                    [leaveArray addObject:model];
                }
                [self.tableView reloadData];
            }
        }
    }];
}

- (void)performData{
    
    if (noticeReportDateMessageModel != nil) {
        self.dateCount.text = noticeReportDateMessageModel.countLeave;
    }
    else{
        
        [self clearDisplay];
    }
}

- (void)clearDisplay {
    self.dateCount.text = @"0";
}


@end
