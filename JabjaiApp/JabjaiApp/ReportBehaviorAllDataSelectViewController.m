//
//  ReportBehaviorAllDataSelectViewController.m
//  JabjaiApp
//
//  Created by mac on 4/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportBehaviorAllDataSelectViewController.h"

#import "ReportListTeacherViewController.h"
#import "ReportBehaviorAllDataCalendarViewController.h"

#import "AlertDialog.h"
#import "UserData.h"
#import "Constant.h"
#import "Utils.h"

@interface ReportBehaviorAllDataSelectViewController (){
    
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    NSMutableArray *statusStringArray;
    
    // The dialog
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    
}

@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";
static NSString *statusRequestCode = @"statusRequestCode";

@implementation ReportBehaviorAllDataSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    self.classLevelTextField.delegate = self;
    self.classroomTextField.delegate = self;
    self.statusTextField.delegate = self;
    if(_classLevelArray != nil && _classroomArray != nil && _statusArray != nil) {
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<_classLevelArray.count; i++) {
            SchoolLevelModel *model = [_classLevelArray objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classroomArray.count; i++) {
            SchoolClassroomModel *model = [_classroomArray objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
        
        statusStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_statusArray.count; i++) {
            BSStatusModel *model = [_statusArray objectAtIndex:i];
            [statusStringArray addObject:[model getStatusName]];
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:_selectedClassLevelIndex] getClassLevelName];
        self.classroomTextField.text = [[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomName];
        self.statusTextField.text = [[_statusArray objectAtIndex:_selectedStatusIndex] getStatusName];
        
    }
    else {
        _selectedClassLevelIndex = -1;
        _selectedClassroomIndex = -1;
        _selectedStatusIndex = 0;
        
        NSMutableArray *statusMutableArray = [[NSMutableArray alloc] init];
        
        BSStatusModel *statusModel = [[BSStatusModel alloc] init];
        [statusModel setStatusId:2];
        [statusModel setStatusName:NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_ALL",nil,[Utils getLanguage],nil)];
        [statusMutableArray addObject:statusModel];
        
        statusModel = [[BSStatusModel alloc] init];
        [statusModel setStatusId:0];
        [statusModel setStatusName:NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_ADD",nil,[Utils getLanguage],nil)];
        [statusMutableArray addObject:statusModel];
        
        statusModel = [[BSStatusModel alloc] init];
        [statusModel setStatusId:1];
        [statusModel setStatusName:NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_REDUCE",nil,[Utils getLanguage],nil)];
        [statusMutableArray addObject:statusModel];
        
        self.statusArray = statusMutableArray;
        
        statusStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_statusArray.count; i++) {
            BSStatusModel *model = [_statusArray objectAtIndex:i];
            [statusStringArray addObject:[model getStatusName]];
        }
        
        self.statusTextField.text = [[_statusArray objectAtIndex:_selectedStatusIndex] getStatusName];
        self.statusId = [[_statusArray objectAtIndex:_selectedStatusIndex] getStatusId];
        
        // Call api
        [self getSchoolClassLevel];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.nextButton layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.nextButton.bounds;
    [self.nextButton.layer insertSublayer:gradientNext atIndex:0];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section2.layer setShadowOpacity:0.3];
    [self.section2.layer setShadowRadius:3.0];
    [self.section2.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section3.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section3.layer setShadowOpacity:0.3];
    [self.section3.layer setShadowRadius:3.0];
    [self.section3.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.nextButton.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.nextButton.layer setShadowOpacity:0.3];
    [self.nextButton.layer setShadowRadius:3.0];
    [self.nextButton.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_BEHAVIORSCORE_REPORT",nil,[Utils getLanguage],nil);
    self.headerClassLevelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_CLASSLEVEL",nil,[Utils getLanguage],nil);
    self.headerClassLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_CALSS",nil,[Utils getLanguage],nil);
    self.headerStatusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_STATUS",nil,[Utils getLanguage],nil);
    
    self.classLevelTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_BEHAVIOR_REP_CLASSROOM",nil,[Utils getLanguage],nil);
    self.classroomTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_BEHAVIOR_REP_CLASS",nil,[Utils getLanguage],nil);
    [self.nextButton setTitle:NSLocalizedStringFromTableInBundle(@"BNT_BEHAVIOR_REP_NEXT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // User press class level textfield
        
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_BEHAVIOR_REP_SELECT_CLASSLEVEL",nil,[Utils getLanguage],nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }
        
        return NO;
    }
    else if(textField.tag == 3) { // User press status textfield
        
        if(statusStringArray != nil && statusStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:statusRequestCode data:statusStringArray];
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - API Caller

- (void)getSchoolClassLevel {
    [self showIndicator];
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    [self showIndicator];
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}

#pragma mark - CallGetSchoolClassLevelAPIDelegate

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classLevelArray = data;
        
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
    }
}

#pragma mark - CallGetStudentClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classroomArray = data;
        
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
        
        if(index == _selectedClassLevelIndex) {
            return;
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classroomTextField.text = @"";
        _selectedClassroomIndex = -1;
        _classLevelId = 0;
        
        // get classroom data with class level
        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:_classLevelId];
        
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        
        if(index == _selectedClassroomIndex) {
            return;
        }
        
        self.classroomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        
        _classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        
    }
    else if([requestCode isEqualToString:statusRequestCode]) {
        
        if(index == _selectedStatusIndex) {
            return;
        }
        
        self.statusTextField.text = [[_statusArray objectAtIndex:index] getStatusName];
        _selectedStatusIndex = index;
        
        _statusId = [[_statusArray objectAtIndex:index] getStatusId];
    }
    
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_BEHAVIOR_REP_SELECT_CLASSLEVEL",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classroomTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_BEHAVIOR_REP_SELECT_CLASS",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.statusTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_BEHAVIOR_REP_SELECT_STATUS",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}



- (IBAction)moveBack:(id)sender {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}

- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        ReportBehaviorAllDataCalendarViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportBehaviorAllDataCalendarStoryboard"];
        
        viewController.classLevelArray = _classLevelArray;
        viewController.classroomArray = _classroomArray;
        viewController.statusArray = _statusArray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.selectedStatusIndex = _selectedStatusIndex;
        viewController.classLevelId = _classLevelId;
        viewController.classroomId = _classroomId;
        viewController.statusId = _statusId;
        
        //        [self.revealViewController pushFrontViewController:viewController animated:YES];
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
    }
    
}
@end
