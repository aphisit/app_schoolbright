//
//  FSCalendarUtils.m
//  Pods
//
//  Created by mac on 12/9/2559 BE.
//
//

#import "CFSCalendarUtils.h"

@implementation CFSCalendarUtils

+(NSDate *)minDate:(NSArray *)dates {
    
    if(dates == nil || dates.count == 0) {
        return nil;
    }
    
    NSDate *minDate = [dates objectAtIndex:0];
    
    for(int i=1; i<dates.count; i++) {
        if([minDate compare:[dates objectAtIndex:i]] == NSOrderedDescending) {
            minDate = [dates objectAtIndex:i];
        }
    }
    
    return minDate;
}

+(NSDate *)maxDate:(NSArray *)dates {
    
    if(dates == nil || dates.count == 0) {
        return nil;
    }
    
    NSDate *maxDate = [dates objectAtIndex:0];
    
    for(int i=1; i<dates.count; i++) {
        if([maxDate compare:[dates objectAtIndex:i]] == NSOrderedAscending) {
            maxDate = [dates objectAtIndex:i];
        }
    }
    
    return maxDate;
}

+(BOOL)sameDate:(NSDate *)date1 date2:(NSDate *)date2 {
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *comps1 = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date1];
    
    NSDateComponents *comps2 = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date2];
    
    if(([comps1 year] == [comps2 year]) && ([comps1 month] == [comps2 month]) && ([comps1 day] == [comps2 day])) {
        return YES;
    }
    else {
        return NO;
    }
    
}

+(BOOL)betweenDate:(NSDate *)date minDate:(NSDate *)minDate maxDate:(NSDate *)maxDate {
    
    BOOL condition1 = ([date compare:minDate] == NSOrderedSame) || ([date compare:minDate] == NSOrderedDescending);
    BOOL condition2 = ([date compare:maxDate] == NSOrderedSame) || ([date compare:maxDate] == NSOrderedAscending);
    BOOL isBetween = condition1 && condition2;
    
    return isBetween;
}


@end
