//
//  ARShowPermissionDialog.m
//  JabjaiApp
//
//  Created by toffee on 25/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "ARShowPermissionDialog.h"

@interface ARShowPermissionDialog ()

@end

@implementation ARShowPermissionDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message{
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    self.titleLabel.text = title;
}

@end
