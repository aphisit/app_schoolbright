//
//  ReportInOutAllDataSelectLevelViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportInOutAllDataSelectLevelViewController.h"
#import "ReportInOutAllDataViewController.h"
#import "UserData.h"
#import "Utils.h"

@interface ReportInOutAllDataSelectLevelViewController (){
     NSMutableArray *expandedSections;
    NSArray <ReportInOutAllDataSelectLevelModel*> *dataLevelArray;
}
@property (strong, nonatomic) CallReportInOutAllDataSelectLevelAPI *callReportInOutAllDataSelectLevelAPI;
@end
static NSString *cellIdentifier = @"Cell";
@implementation ReportInOutAllDataSelectLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    
    expandedSections = [[NSMutableArray alloc] init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportInOutAllDataSelectLevelTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self doCallDataSelectLevel:[UserData getSchoolId] date:[Utils datePunctuateStringFormat:self.date]];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ATTENDCLASS",nil,[Utils getLanguage],nil);
}
- (void)doCallDataSelectLevel:(long long)schoolId date:(NSString *)date {
    [self showIndicator];
    if(self.callReportInOutAllDataSelectLevelAPI != nil) {
        self.callReportInOutAllDataSelectLevelAPI = nil;
    }
    
    self.callReportInOutAllDataSelectLevelAPI = [[CallReportInOutAllDataSelectLevelAPI alloc] init];
    self.callReportInOutAllDataSelectLevelAPI.delegate = self;
    [self.callReportInOutAllDataSelectLevelAPI call:schoolId date:date];
    
}

- (void) callReportInOutAllDataSelectLevelAPI:(CallReportInOutAllDataSelectLevelAPI *)classObj statusAllArray:(NSMutableArray<ReportInOutAllDataSelectLevelModel *> *)datalevelArray success:(BOOL)success{
    if (success) {
        [self stopIndicator];
        dataLevelArray = datalevelArray;
    }
    [self.tableView reloadData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataLevelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReportInOutAllDataSelectLevelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    ReportInOutAllDataSelectLevelModel *model = [[ReportInOutAllDataSelectLevelModel alloc] init];
    model = [dataLevelArray objectAtIndex:indexPath.row];
    cell.levelNameLabel.text = [model getLevelName];
    cell.amountLabel.text = [NSString stringWithFormat:@"%@ %d %@",NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_TOTAL_STUTENTALL",nil,[Utils getLanguage],nil),(NSInteger)[model getAmountStudent],NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    cell.dateLabel.text = [Utils getThaiDateFormatWithDate:self.date];
    [cell setdataCallDetail:[model getLevelId] date:self.date];
    
//    [cell.lookDetailBtn layoutIfNeeded];
    
    if([expandedSections containsObject:@(indexPath.row)]) {
        cell.graphView.hidden = false;
        [cell setStatusGraph:[model getStatus]];
    }else{
        cell.graphView.hidden = true;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([expandedSections containsObject:@(indexPath.row)]) {
        return 850;
    }else{
        return 101;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([expandedSections containsObject:@(indexPath.row)]) {
        [expandedSections removeObject:@(indexPath.row)];
    }
    else {
        [expandedSections addObject:@(indexPath.row)];
    }
    [tableView reloadData];
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}


- (IBAction)moveBack:(id)sender {
    
    ReportInOutAllDataViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutAllDataStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}

//- (IBAction)next:(id)sender {
//    ReportInOutStatusStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutStatusStudentStoryboard"];
//    [self.slideMenuController changeMainViewController:viewController close:YES];
//}
@end
