//
//  TECheckLateScanerQrCodeViewController.m
//  JabjaiApp
//
//  Created by toffee on 4/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "TECheckLateScanerQrCodeViewController.h"
#import "UserData.h"
#import "Utils.h"
@interface TECheckLateScanerQrCodeViewController (){
    ConfirmScanerLateViewController *dialogConfirmScaner;
    AlertDialogConfirm *alertDialog;
    AlertDialog *alertDialogNoUser;
    NSString *idStudentStr ;
    NSBundle *myLangBundle;
}
@property (strong, nonatomic) CallTEGetDataStudentScanBarcodeAPI *callTEGetDataStudentScanBarcodeAPI;
@property (strong, nonatomic) CallTEConfirmScanerBarcode *callTEConfirmScanerBarcode;
@end

@implementation TECheckLateScanerQrCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    _captureSession = nil;
    [self startReading];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer *gradientHeaderView = [Utils getGradientColorHeader];
    gradientHeaderView.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeaderView atIndex:0];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_MENU_SCANER_LATE",nil,[Utils getLanguage],nil);
}

- (BOOL)startReading{
    NSError *error;
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (captureDevice.position == AVCaptureDevicePositionBack) {
        NSLog(@"back camera");
    }
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (!input)
    {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession addInput:input];
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObjects:AVMetadataObjectTypeQRCode,AVMetadataObjectTypeUPCECode,AVMetadataObjectTypeCode39Code,AVMetadataObjectTypeCode39Mod43Code,AVMetadataObjectTypeEAN13Code,AVMetadataObjectTypeEAN8Code,AVMetadataObjectTypeCode93Code,AVMetadataObjectTypeCode128Code,AVMetadataObjectTypePDF417Code,AVMetadataObjectTypeAztecCode,AVMetadataObjectTypeInterleaved2of5Code,AVMetadataObjectTypeITF14Code,AVMetadataObjectTypeDataMatrixCode, nil]];
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.viewforCamera layoutIfNeeded];
    [_videoPreviewLayer setFrame:_viewforCamera.layer.bounds];
    [_viewforCamera.layer addSublayer:_videoPreviewLayer];
    [_captureSession startRunning];
    return YES;
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode] || [[metadataObj type] isEqualToString:AVMetadataObjectTypeCode128Code] || [[metadataObj type] isEqualToString:AVMetadataObjectTypeCode39Code]) {
            [_textView performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            idStudentStr = [metadataObj stringValue];
            NSLog(@"xxx = %@",[metadataObj stringValue]);
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
        }
    }
}

-(void)stopReading{
    [self doCallTEGetDataStudentScanBarcodeAPI:[UserData getSchoolId] idStudent:idStudentStr];
    [_captureSession stopRunning];
    _captureSession = nil;
    //[_videoPreviewLayer removeFromSuperlayer];
}

//call get data student
- (void)doCallTEGetDataStudentScanBarcodeAPI:(long long)schoolId idStudent:(NSString *)idStudent {
    //[self showIndicator];
    if(self.callTEGetDataStudentScanBarcodeAPI != nil) {
        self.callTEGetDataStudentScanBarcodeAPI = nil;
    }
    self.callTEGetDataStudentScanBarcodeAPI = [[CallTEGetDataStudentScanBarcodeAPI alloc] init];
    self.callTEGetDataStudentScanBarcodeAPI.delegate = self;
    [self.callTEGetDataStudentScanBarcodeAPI call:schoolId idStudent:idStudent];
}

- (void)callTEGetDataStudentScanBarcodeAPI:(CallTEGetDataStudentScanBarcodeAPI *)classObj data:(TEDataStudentScanerModel *)data resCode:(NSInteger)resCode success:(BOOL)success{
    //[self stopIndicator];
    if (success && resCode == 200) {
        [self showAlertDialogConfirmScaner:data];
    }else{
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_USER_NOTFOUND",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
    }
    NSLog(@"xxxxx");
}

//call confirm
- (void)doCallComfrimScanerBarcode:(long long)schoolId userId:(long long)userId idCode:(NSString*)idCode{
    //[self showIndicator];
    if(self.callTEConfirmScanerBarcode != nil) {
        self.callTEConfirmScanerBarcode = nil;
    }
    self.callTEConfirmScanerBarcode = [[CallTEConfirmScanerBarcode alloc] init];
    self.callTEConfirmScanerBarcode.delegate = self;
    [self.callTEConfirmScanerBarcode call:schoolId userId:userId idCode:idCode];
}

- (void)callTEConfirmScanerBarcode:(CallTEConfirmScanerBarcode *)classObj resCode:(NSInteger)resCode success:(BOOL)success{
    //[self stopIndicator];
    if (resCode == 200) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_COMPLETED",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        NSLog(@"sucess...");
    }
}

//show dialog confirmScaner
- (void)showAlertDialogConfirmScaner:(TEDataStudentScanerModel *)data {
    if(dialogConfirmScaner != nil && [dialogConfirmScaner isDialogShowing]) {
        [dialogConfirmScaner dismissDialog];
    }
    else {
        dialogConfirmScaner = [[ConfirmScanerLateViewController alloc] init];
        dialogConfirmScaner.delegate = self;
    }
    [dialogConfirmScaner showDialogInView:self.view data:data statusScan:4]; //status 4 is Late
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialogNoUser == nil) {
        alertDialogNoUser = [[AlertDialog alloc] init];
        alertDialogNoUser.delegate = self;
    }
    if(alertDialogNoUser != nil && [alertDialogNoUser isDialogShowing]) {
        [alertDialogNoUser dismissDialog];
    }
    [alertDialogNoUser showDialogInView:self.view title:title message:message];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

//retrue from dialog confirm
- (void)onAlertDialogClose{
     [self performSelectorOnMainThread:@selector(startReading) withObject:nil waitUntilDone:NO];
}

//retrue from dialog show info student
- (void)doCallConfirmScaner{
    [self doCallComfrimScanerBarcode:[UserData getSchoolId] userId:[UserData getUserID] idCode:idStudentStr];
}
- (void)onAlertDialogScanerClose{
     [self performSelectorOnMainThread:@selector(startReading) withObject:nil waitUntilDone:NO];
}

- (IBAction)moveBackAction:(id)sender {
    TELevelClassViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"takeeventAttendanceStoryboard"];
    viewController.mode = self.mode;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
