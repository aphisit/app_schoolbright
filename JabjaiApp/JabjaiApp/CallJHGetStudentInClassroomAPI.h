//
//  CallJHGetStudentInClassroomAPI.h
//  JabjaiApp
//
//  Created by toffee on 11/22/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JHSelectedStudentModel.h"

@class CallJHGetStudentInClassroomAPI;

@protocol CallJHGetStudentInClassroomAPIDelegate <NSObject>

- (void)callJHGetStudentInClassrommAPI:(CallJHGetStudentInClassroomAPI *)classObj data:(NSArray<JHSelectedStudentModel *> *)data success:(BOOL)success;

@end


@interface CallJHGetStudentInClassroomAPI : NSObject
@property (nonatomic, weak) id<CallJHGetStudentInClassroomAPIDelegate> delegate;

- (void)call:(long long)schoolId classroomId:(long long)classroomId;


@end
