//
//  StudyTableViewCell.m
//  JabjaiApp
//
//  Created by toffee on 31/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "StudyTableViewCell.h"
#import "UserData.h"
#import "Utils.h"
@interface StudyTableViewCell () {
   
    CAGradientLayer *gradient;
    NSBundle *myLangBundle;
}
@end
@implementation StudyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat max = MAX(self.colorView.frame.size.width, self.colorView.frame.size.height);
    self.colorView.layer.cornerRadius = max/2.0;
    self.colorView.layer.masksToBounds = YES;
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateStatus:(NSInteger)number{
    
    gradient = [CAGradientLayer layer];
    UIImage * backgroundColorImage;
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource: [UserData getChangLanguage] ofType:@"lproj"]];
    switch (number) {
        case 0:
            
            [self.statusLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,myLangBundle,nil)];
            gradient = [Utils getGradientColorStatus:@"green"];
            gradient.frame = self.colorView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            
            break;
        case 1:
            
            [self.statusLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LATE",nil,myLangBundle,nil)];
            gradient = [Utils getGradientColorStatus:@"yellow"];
            gradient.frame = self.colorView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 2:
         
            [self.statusLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ABSENCE",nil,myLangBundle,nil)];
            gradient = [Utils getGradientColorStatus:@"red"];
            gradient.frame = self.colorView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
            
        case 5:
            //[self.statusBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_EVENT_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            [self.statusLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_EVENT",nil,myLangBundle,nil)];
            gradient = [Utils getGradientColorStatus:@"blue"];
            gradient.frame = self.colorView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 4:
           
            [self.statusLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_PERSONAL",nil,myLangBundle,nil)];
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.colorView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 3:
            
            [self.statusLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SICK",nil,myLangBundle,nil)];
            gradient = [Utils getGradientColorStatus:@"pink"];
            gradient.frame = self.colorView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
            
        case 6:
          
            [self.statusLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NOCHECK",nil,myLangBundle,nil)];
            gradient = [Utils getGradientColorStatus:@"gray"];
            gradient.frame = self.colorView.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.colorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
       
            
            
       
            
        default:
           
            break;
    }
    
}

@end
