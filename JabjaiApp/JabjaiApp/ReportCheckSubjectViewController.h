//
//  ReportCheckSubjectViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableListDialog.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "CallGetTASubjectForReportAPI.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "TASubjectModel.h"
#import "AlertDialog.h"
#import "SlideMenuController.h"

@interface ReportCheckSubjectViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate, CallGetTASubjectForReportAPIDelegate, SlideMenuControllerDelegate, AlertDialogDelegate>

//Global variables
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<TASubjectModel *> *subjectArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
@property (nonatomic) long long selectedClassLevelId;
@property (nonatomic) long long selectedClassroomId;
@property (nonatomic) NSString *selectedSubjectId;

@property (weak, nonatomic) IBOutlet UILabel *headerClassLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSubjectLabel;


@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classroomTextField;
@property (weak, nonatomic) IBOutlet UITextField *subjectTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIView *section2;
@property (weak, nonatomic) IBOutlet UIView *section3;

- (IBAction)actionNext:(id)sender;
- (IBAction)moveBack:(id)sender;


@end
