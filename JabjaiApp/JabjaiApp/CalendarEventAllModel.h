//
//  CalendarEventAllModel.h
//  JabjaiApp
//
//  Created by mac on 12/19/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalendarEventAllModel : NSObject

@property (strong ,nonatomic) NSDate *holidayList;
@property (nonatomic, strong) NSNumber *color;

@end
