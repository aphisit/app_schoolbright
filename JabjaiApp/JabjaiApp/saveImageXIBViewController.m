//
//  saveImageXIBViewController.m
//  JabjaiApp
//
//  Created by toffee on 23/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "saveImageXIBViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Utils.h"
@interface saveImageXIBViewController (){
    BOOL isShowing;
    UIImageView* imgView;
    NSArray *imageArray;
    
    CAGradientLayer *gradientGreen;
    CAGradientLayer *gradientRed;
}

@end

@implementation saveImageXIBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     isShowing = NO;
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - Dialog functions
- (IBAction)saveAction:(id)sender {
    
    NSLog(@"pageControl = %d",self.pageControl.currentPage);
    [imgView sd_setImageWithURL:[NSURL URLWithString:[imageArray objectAtIndex:self.pageControl.currentPage]]];
    UIImageWriteToSavedPhotosAlbum(imgView.image, self, nil, nil);
    [self dismissDialog];
    
}

- (IBAction)cancelAction:(id)sender {
    [self dismissDialog];
}

- (void)showDialogInView:(UIView *)targetView urlImageArray:(NSArray *)urlImageArray{
    imageArray = urlImageArray;
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [self.contentView setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.0]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.contentView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentView setAlpha:1.0];
    [UIView commitAnimations];
    [self.view layoutIfNeeded];
    isShowing = YES;
    
    //setcolor
    gradientGreen = [CAGradientLayer layer];
    gradientGreen = [Utils getGradientColorStatus:@"green"];
    gradientGreen.frame = self.saveBtn.bounds;
    [self.saveBtn.layer insertSublayer:gradientGreen atIndex:0];
    gradientRed = [CAGradientLayer layer];
    gradientRed = [Utils getGradientColorStatus:@"red"];
    gradientRed.frame = self.cancelBtn.bounds;
    [self.cancelBtn.layer insertSublayer:gradientRed atIndex:0];
    
    [self doPageControl:urlImageArray];
}

-(void)doPageControl:(NSArray*)urlImageArray{
    CGRect frame = CGRectMake(0, 0, 0, 0 );
    self.pageControl.numberOfPages = urlImageArray.count;
    for (int i = 0; i < urlImageArray.count; i++) {
        frame.origin.x = self.scrollView.frame.size.width * i;
        frame.size = self.scrollView.frame.size;
        imgView = [[UIImageView alloc] initWithFrame:frame];
        [imgView sd_setImageWithURL:[NSURL URLWithString:[urlImageArray objectAtIndex:i]]];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        [self.scrollView addSubview:imgView];
        
    }
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * urlImageArray.count, self.scrollView.frame.size.height);
    self.scrollView.delegate = self;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger pageNumber = self.scrollView.contentOffset.x / self.scrollView.frame.size.width;
    self.pageControl.currentPage = pageNumber;
}

- (void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return isShowing;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
