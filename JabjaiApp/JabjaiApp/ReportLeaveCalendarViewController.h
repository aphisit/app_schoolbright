//
//  ReportLeaveCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 4/10/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NoticeReportDteControllerDelagate.h"
#import "CFSCalendar.h"
#import "SlideMenuController.h"

@protocol NoticeReportDateViewControllerDelegate <NSObject>

@optional
- (void)onSelectDate:(NSDate *)date;

@end

@interface ReportLeaveCalendarViewController : UIViewController<CFSCalendarDelegate, CFSCalendarDataSource, CFSCalendarDelegateAppearance , NoticeReportDteControllerDelagate, UITableViewDelegate, UITableViewDataSource, SlideMenuControllerDelegate>

@property (retain, nonatomic) id<NoticeReportDateViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) NSCalendar *gregorian;

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;

@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailConstraint;

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (retain, nonatomic) UIViewController *rootViewControler;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;
- (void)onSelectMessage:(long long)letterID;

- (IBAction)openDetail:(id)sender;

@end
