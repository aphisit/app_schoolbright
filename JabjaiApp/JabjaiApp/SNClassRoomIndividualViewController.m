//
//  SNClassRoomIndividualViewController.m
//  JabjaiApp
//
//  Created by toffee on 9/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNClassRoomIndividualViewController.h"
#import "BSSelectStudentsViewController.h"
#import "SNClassLevelGroupViewController.h"
#import "BSSelectBehaviorScoreViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "Constant.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Utils.h"


@interface SNClassRoomIndividualViewController (){
    UIColor *greenBGColor;
    UIColor *redBGColor;
    
    BOOL optionStatusSelectAll;
    NSMutableArray *student;
    NSMutableArray *isSelected;
    NSMutableArray *userAcceptArray;
    // The dialog
    AlertDialog *alertDialog;
    
    NSBundle *myLangBundle;
}
@property (strong, nonatomic) CallGetStudentInClassroomAPI *callGetStudentInClassroomAPI;
@end
static NSString *cellIdentifier = @"Cell";
@implementation SNClassRoomIndividualViewController
@synthesize studentImageArray;

- (void)viewDidLoad {
    
    
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    [super viewDidLoad];
     student = [[NSMutableArray alloc] init];
    userAcceptArray = [[NSMutableArray alloc] init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSStudentNameRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    greenBGColor = [UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0];
     self.titleLabel.text = [[_classroomArray objectAtIndex:self.selectedClassroomIndex] getClassroomName];
    [self setLanguage];
    if(self.selectedStudentArray == nil) {
        [self getStudentInClassroom];
    }
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.nextButton layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.nextButton.bounds;
    [self.nextButton.layer insertSublayer:gradientNext atIndex:0];
}

-(void)setLanguage{
    
     self.headerSendNewsLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS",nil,myLangBundle,nil);
      [self.nextButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - API Caller

- (void)getStudentInClassroom {
    [self showIndicator];
    if(self.callGetStudentInClassroomAPI != nil) {
        self.callGetStudentInClassroomAPI = nil;
    }
    self.callGetStudentInClassroomAPI = [[CallGetStudentInClassroomAPI alloc] init];
    self.callGetStudentInClassroomAPI.delegate = self;
    [self.callGetStudentInClassroomAPI call:[UserData getSchoolId] classroomId:_classroomId];
}
#pragma mark - API Delegate

- (void)callGetStudentInClassrommAPI:(CallGetStudentInClassroomAPI *)classObj data:(NSArray<BSSelectedStudent *> *)data success:(BOOL)success {
    [self stopIndicator];
    if(success && data != nil) {
        _selectedStudentArray = data;
        [self.tableView reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"studenArray = %@",self.selectedStudentArray);
    if(self.selectedStudentArray != nil) {
        NSLog(@"Count = %d",self.selectedStudentArray.count);
        return self.selectedStudentArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BSStudentNameRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    BSSelectedStudent *model = [self.selectedStudentArray objectAtIndex:indexPath.row];
    NSLog(@"row = %d",indexPath.row);
    cell.delegate = self;
    cell.index = indexPath.row;
    NSArray *array = [[NSString stringWithFormat:@"%@", [model getStudentName]] componentsSeparatedByString:@" "];
    
    NSMutableString *lastName = [[NSMutableString alloc] init];
    if (array.count>1) {
        for (int i = 0; i < array.count; i++) {
            if (i>0) {
                [lastName appendFormat:@"%@ ",array[i]];
            }
        }
        cell.titleLabel.text = array[0];
        cell.titleLastname.text = lastName;
    }else{
        cell.titleLabel.text = array[0];
    }
    cell.runNumber.text = [NSString stringWithFormat:@"%d.",indexPath.row+1];
    [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@", [model getStudentPic]]]] placeholderImage:nil options:SDWebImageRefreshCached
        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        if (image == nil || error) {
                                          cell.userImageView.image = [UIImage imageNamed:@"ic_user_info"];
                                        }
        }];
    cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height /2;
    cell.userImageView.layer.masksToBounds = YES;
    cell.userImageView.layer.borderWidth = 0;
    if([model isSelected]) {
        if (![student containsObject:@([model getStudentId])]) {
            [student addObject: @([model getStudentId])];
            [userAcceptArray addObject:[model getStudentName]];
        }
        [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
    }
    else {
        [student removeObject: @([model getStudentId])];
        [userAcceptArray removeObject: [model getStudentName]];
        [cell setRadioButtonClearSelected];
    }
    [cell.shadowViewCell.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.shadowViewCell.layer setShadowOpacity:0.3];
    [cell.shadowViewCell.layer setShadowRadius:3.0];
    [cell.shadowViewCell.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 100; /* Device is iPad */
    }else{
        return 90;
    }
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL isSelected = [[self.selectedStudentArray objectAtIndex:indexPath.row] isSelected];
    [[self.selectedStudentArray objectAtIndex:indexPath.row] setSelected:!isSelected]; // toggle selected status
    NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    NSLog(@"indexpath = %@",indexPath);
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - BSRadionTableViewCellDelegate

- (void)onPressRadioButton:(BSStudentNameRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index{
    
    BOOL isSelected = [[self.selectedStudentArray objectAtIndex:index] isSelected];
    [[self.selectedStudentArray objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    NSLog(@"indexpath = %@",indexPath);
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}
-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)actionNext:(id)sender {
    
    
    if (student.count == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_LEAST",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
    }else{
        SNTimeOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        
        viewController.sendType = self.sendType;
        viewController.sendGroup = self.sendGroup;
        viewController.newsType = self.newsType;
        viewController.level2id = _level2id;
        viewController.studentid = student;
        viewController.userAcceptArray = userAcceptArray;
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    
}

- (IBAction)moveBack:(id)sender {
     SNClassLevelGroupViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNClassLevelGroupStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
