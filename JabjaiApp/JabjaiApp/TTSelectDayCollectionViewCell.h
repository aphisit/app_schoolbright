//
//  TTSelectDayCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 15/5/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TTSelectDayCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *dayLable;
@property (weak, nonatomic) IBOutlet UIView *backgroundColor;
@end

NS_ASSUME_NONNULL_END
