//
//  CallRemoveDataSystemNotificationPOSTAPI.m
//  JabjaiApp
//
//  Created by Mac on 5/3/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import "CallRemoveDataSystemNotificationPOSTAPI.h"
#import "APIURL.h"
#import <AFNetworking/AFNetworking.h>
@implementation CallRemoveDataSystemNotificationPOSTAPI
- (void)call:(NSString *)jsonString{
    [self removeData:jsonString];
}
- (void)removeData:(NSString*)jsonString{
    NSString *URLString = [APIURL getStatusRemoveDataOfNotification];
    NSError *error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&error];
    NSLog(@"Json = %@", json);
    NSLog(@"ObjectData = %@", objectData);
    NSLog(@"jsonString = %@", jsonString);
    
    if(error != nil) {
        NSLog(@"%@", @"Parse JSON data failed");
    }
    else {
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        
        [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
           
            // NSString *response = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"resMessage"]];
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callRemoveDataSystemNotificationPOSTAPI:)]) {
               
                [self.delegate callRemoveDataSystemNotificationPOSTAPI:YES];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callRemoveDataSystemNotificationPOSTAPI:)]) {
                [self.delegate callRemoveDataSystemNotificationPOSTAPI:NO];
            }
            
        }];
        
    }
}
@end
