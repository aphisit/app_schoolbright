//
//  NFNewFutureSlideShowCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 22/5/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NFNewFutureSlideShowCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *images;


@end

NS_ASSUME_NONNULL_END
