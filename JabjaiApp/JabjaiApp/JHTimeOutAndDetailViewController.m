//
//  JHTimeOutAndDetailViewController.m
//  JabjaiApp
//
//  Created by toffee on 11/23/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "JHTimeOutAndDetailViewController.h"
#import "Utils.h"
#import "DateUtility.h"
#import "YearTermModel.h"
#import "JHConfirmJobHomeViewController.h"
#import "JHSelectStudenClassViewController.h"
#import "JHClassLevelViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
//lib choose file
#import "DBAttachmentPickerController.h"
#import "DBAttachment.h"
#import "NSDateFormatter+DBLibrary.h"

@interface JHTimeOutAndDetailViewController (){
    NSInteger  selectStartDateTextField;
    UIImage *ima;
    int exitRownumber;
    AlertDialog *alertDialog;
    NSBundle *myLangBundle;
}
@property (strong, nonatomic) SelectOneDayCalendarViewController *calendarDialog;
@property (strong, nonatomic) NSDate *minDate;
@property (strong, nonatomic) NSDate *maxDate;
@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSDateFormatter *sendDateFormatter;
@property (strong, nonatomic) PhotoAlbumLibary *photoAlbumLibary;
@end
static NSString  *cellIdentifier = @"Cell";
@implementation JHTimeOutAndDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    
    self.startTime.layer.cornerRadius = 5;
    self.startTime.layer.masksToBounds = YES;
    self.stopTime.layer.cornerRadius = 5;
    self.stopTime.layer.masksToBounds = YES;
   
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([JHIconFileCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.fileCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([JHIconFileCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.fileCollectionView.delegate = self;
    self.fileCollectionView.dataSource = self;
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
       
    self.sendDateFormatter = [Utils getDateFormatter];
    self.sendDateFormatter.dateFormat = @"MM/dd/yyyy";
    
    // Set textfield delegate
    self.startTime.delegate = self;
    self.stopTime.delegate = self;
    self.alertDateHomeWork.delegate = self;
    self.alertTimeHomeWork.delegate = self;
    self.detailTextView.delegate = self;
    
    if (self.detail != NULL) {
        self.detailTextView.text = self.detail;
        self.detailTextView.textColor = [UIColor blackColor];
        self.detailTextView.tag = 1;
        self.startTime.text = [self.dateFormatter stringFromDate:self.startDay];
        self.stopTime.text = [self.dateFormatter stringFromDate:self.endDay];
        self.alertDateHomeWork.text = [self.dateFormatter stringFromDate:self.alertDay];
        self.alertTimeHomeWork.text = self.alertTime;
        
        if (self.imagesArray.count > 0) {
            [self.collectionView reloadData];
        }
        if (self.attachmentArray.count > 0) {
            [self.fileCollectionView reloadData];
        }
        
    }else{
        self.detailTextView.text = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_JOBHOME_DETAIL",nil,myLangBundle,nil);
        self.detailTextView.textColor = [UIColor lightGrayColor];
        self.detailTextView.tag = 0;
    }
   
    self.gregorian = [Utils getGregorianCalendar];
    self.maxDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:+1 toDate:[NSDate date] options:0];//[NSDate date];
    self.minDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:0 toDate:[NSDate date] options:0];
    self.calendarDialog = [[SelectOneDayCalendarViewController alloc] initWithTitle:@"เลือกวันที่" minimumDate:self.minDate maximumDate:self.maxDate];
    self.calendarDialog.delegate = self;
    
    // creat PicKerTime
    datePicker = [[UIDatePicker alloc]init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
    [datePicker setLocale:locale];
    //[datePicker setDate:[NSDate date]];
    [datePicker setDatePickerMode:UIDatePickerModeTime]; //setdatatime
    if (@available(iOS 13.4, *)) {
        datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else { 
        // Fallback on earlier versions
    }
    [datePicker addTarget:self action:@selector(doTimeTextField:) forControlEvents:UIControlEventValueChanged];
    [self.alertTimeHomeWork setInputView:datePicker];
    //[self datePickerChanged];
    
    // creat button Done
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(switchDatePickerHidden)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    [self.alertTimeHomeWork setInputAccessoryView:toolBar];
    [self.detailTextView setInputAccessoryView:toolBar];
}


- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
    [self setLanguage];
}
- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
    
    [self.shadowChooseDetail layoutIfNeeded];
    [self.shadowChooseDetail.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.shadowChooseDetail.layer setShadowOpacity:0.3];
    [self.shadowChooseDetail.layer setShadowRadius:3.0];
    [self.shadowChooseDetail.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    [self.shadowChooseImage layoutIfNeeded];
    [self.shadowChooseImage.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.shadowChooseImage.layer setShadowOpacity:0.3];
    [self.shadowChooseImage.layer setShadowRadius:3.0];
    [self.shadowChooseImage.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    [self.shadowChooseFile layoutIfNeeded];
    [self.shadowChooseFile.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.shadowChooseFile.layer setShadowOpacity:0.3];
    [self.shadowChooseFile.layer setShadowRadius:3.0];
    [self.shadowChooseFile.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    
    [self.selectImageBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.selectImageBtn.layer setShadowOpacity:0.3];
    [self.selectImageBtn.layer setShadowRadius:3.0];
    [self.selectImageBtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    [self.selectImageBtn.layer setCornerRadius:10.0];
    self.selectImageBtn.titleLabel.numberOfLines = 0;
    [self.selectImageBtn.titleLabel setTextAlignment:UITextAlignmentCenter];
    
    [self.selectFileBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.selectFileBtn.layer setShadowOpacity:0.3];
    [self.selectFileBtn.layer setShadowRadius:3.0];
    [self.selectFileBtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    [self.selectFileBtn.layer setCornerRadius:10.0];
    self.selectFileBtn.titleLabel.numberOfLines = 0;
    [self.selectFileBtn.titleLabel setTextAlignment:UITextAlignmentCenter];
    
    [self.headerNextbtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.headerNextbtn.layer setShadowOpacity:0.3];
    [self.headerNextbtn.layer setShadowRadius:3.0];
    [self.headerNextbtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
//    [self.headerNextbtn.layer setCornerRadius:10.0];
    
}

-(void)setLanguage{
   
    self.headerInformationLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_INFORMATION",nil,myLangBundle,nil);
    self.headerStartTime.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_START_DATE",nil,myLangBundle,nil);
    self.headderEndTime.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_END_DATE",nil,myLangBundle,nil);
    self.headerAlertDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_ALERT_DATE",nil,myLangBundle,nil);
    self.headerAlertTimeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_ALERT_TIME",nil,myLangBundle,nil);
    self.headerDescriptionLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_DESCRIPTION",nil,myLangBundle,nil);
    self.headerAttachLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_ATTACH_IMAGE",nil,myLangBundle,nil);
    self.headerFileAttachLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_ATTACH_FILE",nil,myLangBundle,nil);
    self.startTime.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_JOBHOME_START_DATE",nil,myLangBundle,nil);
    self.stopTime.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_JOBHOME_END_DATE",nil,myLangBundle,nil);
    self.alertDateHomeWork.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_JOBHOME_ALERT_DATE",nil,myLangBundle,nil);
    self.alertTimeHomeWork.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_JOBHOME_ALERT_TIME",nil,myLangBundle,nil);
    [_headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_JOBHOME_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
    [self.selectFileBtn  setTitle:NSLocalizedStringFromTableInBundle(@"BTN_JOBHOME_CHOOSE_FILE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
    [self.selectImageBtn  setTitle:NSLocalizedStringFromTableInBundle(@"BTN_JOBHOME_CHOOSE_IMAGE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
}

-(void)doTimeTextField:(id)sender {
    
    UIDatePicker *picker = (UIDatePicker*)self.alertTimeHomeWork.inputView;
    self.alertTimeHomeWork.text = [self formatDate:picker.date];
    self.alertTime = [self formatDate:picker.date];
}

#pragma mark - Keyboard
- (void)hideKeyboard {
    [self.detailTextView resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)switchDatePickerHidden
{
    [_alertTimeHomeWork resignFirstResponder];
    [_detailTextView resignFirstResponder];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
   
    
   
    if(textField.tag == 0) { // tag 0 : startDateTextField
        if([self.calendarDialog isDialogShowing]) {
            [self.calendarDialog dismissDialog];
        }
        self.calendarDialog = [[SelectOneDayCalendarViewController alloc] initWithTitle:@"เลือกวันที่" minimumDate:self.minDate maximumDate:self.maxDate];
         self.calendarDialog.delegate = self;
        [self.calendarDialog showDialogInView:self.view];
        selectStartDateTextField = textField.tag;
        return NO;
    }
    else if(textField.tag == 1) { // tag 1 : endDateTextField
        
        if([self.calendarDialog isDialogShowing]) {
            [self.calendarDialog dismissDialog];
        }
        //self.calendarDialog.delegate = self;
        self.calendarDialog = [[SelectOneDayCalendarViewController alloc] initWithTitle:@"เลือกวันที่" minimumDate:self.startDay maximumDate:self.maxDate];
         self.calendarDialog.delegate = self;
        [self.calendarDialog showDialogInView:self.view];
        
        selectStartDateTextField = textField.tag;
        
        return NO;
    }
    else if(textField.tag == 2) { // tag 1 : endDateTextField
        
        if([self.calendarDialog isDialogShowing]) {
            [self.calendarDialog dismissDialog];
        }
         self.calendarDialog = [[SelectOneDayCalendarViewController alloc] initWithTitle:@"เลือกวันที่" minimumDate:self.minDate maximumDate:self.endDay];
         self.calendarDialog.delegate = self;
        [self.calendarDialog showDialogInView:self.view];

        selectStartDateTextField = textField.tag;
        
        return NO;
    }

    
    return YES;
}

#pragma mark - UITextViewDelegate
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    if (textView.tag == 0) {
        self.detailTextView.text = @"";
    }
    self.detailTextView.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView {
    
    if(self.detailTextView.text.length == 0) {
        self.detailTextView.tag = 0;
        self.detailTextView.textColor = [UIColor lightGrayColor];
         self.detailTextView.text = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_JOBHOME_DETAIL",nil,myLangBundle,nil);
        [self.detailTextView resignFirstResponder];
    }else{
        self.detailTextView.tag = 1;
    }
}


-(void) textViewShouldEndEditing:(UITextView *)textView {
    
    if(self.detailTextView.text.length == 0) {
        self.detailTextView.textColor = [UIColor lightGrayColor];
       self.detailTextView.text = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_JOBHOME_DETAIL",nil,myLangBundle,nil);
        [self.detailTextView resignFirstResponder];
    }
}

- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

#pragma mark - CalendarDialogDelegate
-(void)calendarDialogPressOK:(NSArray<NSDate *> *)selectedDates {
    if(selectedDates.count > 0){
        NSDate *selectDate = [selectedDates objectAtIndex:0];
        if(selectStartDateTextField == 0) {
            NSLog(@"NSDate = %@",selectDate);
            self.startTime.text = [self.dateFormatter stringFromDate:selectDate];
            self.startDay = selectDate;
        }
        else if(selectStartDateTextField == 1){
            self.stopTime.text = [self.dateFormatter stringFromDate:selectDate];
            self.endDay = selectDate ;
        }
        else if(selectStartDateTextField == 2){
            self.alertDateHomeWork.text = [self.dateFormatter stringFromDate:selectDate];
            self.alertDay = selectDate ;
        }
    }
}

-(void)calendarDialogPressCancel {
    NSLog(@"%@", @"Cancel calendar dialog");
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (collectionView == self.collectionView) {
        return [self.imagesArray count];
    }else{
        return [self.attachmentArray count];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JHIconFileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    cell.removeFileBtn.tag = indexPath.row;
    
    if (collectionView == self.collectionView) {
        cell.fileImage.image = [self.imagesArray objectAtIndex:indexPath.row];
        cell.typeCollectionView = self.collectionView;
    }else{
        DBAttachment *attachment = self.attachmentArray[indexPath.row];
        CGFloat scale = [UIScreen mainScreen].scale;
        CGSize scaledThumbnailSize = CGSizeMake( 80.f * scale, 80.f * scale );
        [attachment loadThumbnailImageWithTargetSize:scaledThumbnailSize completion:^(UIImage *resultImage) {
            cell.fileImage.image = resultImage;
        }];
        cell.typeCollectionView = self.fileCollectionView;
    }
    return cell;

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize defaultSize = CGSizeMake((self.collectionView.frame.size.width/3)-10, (self.collectionView.frame.size.height));
    return defaultSize;
}

- (BOOL)validateData {
    //NSArray *array = [self.detailTextView.text componentsSeparatedByString:@" "];
    
    // [category isEqualToString:@"Some String"]
    if ([self.startTime.text isEqualToString:@""] || self.startTime.text == NULL){
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_SELECT_START",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    else if ([self.stopTime.text isEqualToString:@""] || self.stopTime.text == NULL){
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_SELECT_END",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    else if ([self.alertDateHomeWork.text isEqualToString:@""] || self.alertDateHomeWork.text == NULL){
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_SELECT_DATE",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    else if ([self.alertTimeHomeWork.text isEqualToString:@""] || self.alertTimeHomeWork.text == NULL){
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_SELECT_TIME",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    else if ( (self.detailTextView.text == (id)[NSNull null] || self.detailTextView.text.length == 0 )){
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_SELECT_DESCRIPTION",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
   
    
    return YES;
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - JHIconFileCollectionViewCellDelegate
- (void)removeFile:(NSInteger)index typeCollectionView:(UICollectionView *)typeCollectionView{
    
    if (typeCollectionView == self.collectionView) {
        [self.imagesArray removeObjectAtIndex:index];
        [self.collectionView reloadData];
    }else{
        [self.attachmentArray removeObjectAtIndex:index];
        [self.fileCollectionView reloadData];
    }
}


#pragma mark - PhotoAlbumDelegate
- (void) replyPhotoAlbum:(PhotoAlbumLibary *)classObj imageArray:(NSMutableArray *)imageArray{
    if (imageArray != nil) {
        self.headerNextbtn.enabled = YES;
        self.imagesArray = imageArray;
        [self.collectionView reloadData];
    }
}

- (IBAction)selectFile:(id)sender {
    self.headerNextbtn.enabled = NO;
    __weak typeof(self) weakSelf = self;
    self.attachmentArray = [[NSMutableArray alloc] initWithCapacity:100];
    DBAttachmentPickerController *attachmentPickerController = [DBAttachmentPickerController attachmentPickerControllerFinishPickingBlock:^(NSArray<DBAttachment *> * _Nonnull attachmentArray)
            {
                for (DBAttachment *model in attachmentArray) {
                    NSArray *typeArray = [model.fileName componentsSeparatedByString:@"."];
                    NSString *type = [typeArray objectAtIndex:typeArray.count-1];
                    
                    if ([type isEqualToString:@"pdf"] || [type isEqualToString:@"docx"] || [type isEqualToString:@"xlsx"]) {
                        [weakSelf.attachmentArray addObjectsFromArray:attachmentArray];
                    }else{
                        [self showAlertDialogWithMessage:@"รองรับเฉพาะไฟล์ PDF,WORD,EXCEL"];
                    }
                    self.headerNextbtn.enabled = YES;
                    [self.fileCollectionView reloadData];
                }
            } cancelBlock:^(){
                self.headerNextbtn.enabled = YES;
            }];
    attachmentPickerController.mediaType =   DBAttachmentMediaTypeOther;
    attachmentPickerController.allowsSelectionFromOtherApps = YES;
    [attachmentPickerController presentOnViewController:self];
}


- (IBAction)selectImage:(id)sender {
    self.headerNextbtn.enabled = NO;
    self.photoAlbumLibary = [[PhotoAlbumLibary alloc] init];
    self.photoAlbumLibary.delegate  = self;
    [self.photoAlbumLibary showPhotoAlbumView:self.view];
}


- (IBAction)actionNext:(id)sender {
    
    if ([self validateData]) {
        JHConfirmJobHomeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHConfirmJobHomeStoryboard"];
        viewController.schoolId = [UserData getSchoolId];
        viewController.levelId = self.levelId;
        viewController.arrayuserId = _studentId;
        viewController.sendtype = self.sendType;
        viewController.dayend = self.endDay;
        viewController.daynotification = self.alertDay;
        viewController.timenotification = self.alertTime;
        viewController.daystart = self.startDay;
        viewController.homeworkdetail = self.detailTextView.text;
        viewController.planeId = self.subjectId;
        viewController.teacherid = [NSString stringWithFormat: @"%d", [UserData getUserID]];
        viewController.fileArray = self.attachmentArray;
        viewController.subJectName = self.subjectName;
        viewController.arrayuserName = self.studentName;
        viewController.imageHomework = self.imagesArray;
        viewController.roomName = _roomName;
        viewController.classLevelArray = _classLevelArray;
        viewController.classRoomArray = _classRoomArray;
        viewController.sujectNameArrray = _sujectNameArrray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.selectedSubjectIndex = _selectedSubjectIndex;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    
    
}

- (IBAction)moveBack:(id)sender {
    
    if (self.sendType == 0) {
        JHClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHClassLevelStoryboard"];
        NSLog(@"subjectID = %@",_subjectId);
        viewController.classLevelArray = _classLevelArray;
        viewController.classroomArray = _classRoomArray;
        viewController.subjectArray = _sujectNameArrray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.selectedSubjectIndex = _selectedSubjectIndex;
        viewController.mode = 1;
        
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
        
    }else{
        JHSelectStudenClassViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHSelectStudenClassStoryboard"];
        viewController.classroomId = self.levelId;
        viewController.subjectId = self.subjectId;
        viewController.subjectName = self.subjectName;
        viewController.sendType = self.sendType;
        viewController.classLevelArray = _classLevelArray;
        viewController.classRoomArray = _classRoomArray;
        viewController.sujectNameArrray = _sujectNameArrray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.selectedSubjectIndex = _selectedSubjectIndex;
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
}
@end
