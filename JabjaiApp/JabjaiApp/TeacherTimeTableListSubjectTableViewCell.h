//
//  TeacherTimeTableListSubjectTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 22/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TeacherTimeTableListSubjectTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *startTimeLable;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLable;
@property (weak, nonatomic) IBOutlet UILabel *subjectLable;
@property (weak, nonatomic) IBOutlet UILabel *codeSubjectLable;
@property (weak, nonatomic) IBOutlet UIView *section1;







@end

NS_ASSUME_NONNULL_END
