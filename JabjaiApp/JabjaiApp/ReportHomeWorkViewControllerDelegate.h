//
//  ReportHomeWorkViewControllerDelegate.h
//  JabjaiApp
//
//  Created by mac on 1/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

//#import <UIKit/UIKit.h>
//
//@interface ReportHomeWorkViewControllerDelegate : UIViewController
//
//@end

#import <Foundation/Foundation.h>

@protocol ReportHomeWorkViewControllerDelegate <NSObject>

//@optional
//- (void)onSelectSubject:(long long)subjectID subjectName:(NSString *)subjectName date:(NSDate *)date;

@optional
- (void)onSelectMessage:(long long)messageID;

@end


