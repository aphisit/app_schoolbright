//
//  SSubjectViewController.h
//  JabjaiApp
//
//  Created by mac on 5/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import ARSlidingPanel;

#import <UIKit/UIKit.h>
#import "TimeTableViewController.h"
#import "SSubjectViewControllerDelegate.h"


@interface SSubjectViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (retain, nonatomic) id<SSubjectViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
