//
//  ChangPasswordDialog.m
//  JabjaiApp
//
//  Created by toffee on 8/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ChangPasswordDialog.h"
#import "Utils.h"
@interface ChangPasswordDialog (){
    BOOL isShowing;
}
@end

@implementation ChangPasswordDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    isShowing = NO;
    self.messageLabel.text = NSLocalizedStringFromTableInBundle(@"DIALOG_CPW_CHANG_PASSWORD",nil,[Utils getLanguage],nil);
    [self.confirmBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_CPW_OK",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm;
    //set color header
    [self.confirmBtn layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.confirmBtn.bounds;
    [self.confirmBtn.layer insertSublayer:gradientConfirm atIndex:0];
    
}

- (void) showDialogInView:(UIView *)targetView message:(NSString *)message{
    self.messageLabel.text = NSLocalizedStringFromTableInBundle(@"DIALOG_CPW_CHANG_PASSWORD",nil,[Utils getLanguage],nil);
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.contentview setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentview setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    isShowing = YES;
}

- (void) dismissDialog{
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(doChangPassword:)]) {
        [self.delegate doChangPassword:self];
    }
}
-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentview setAlpha:0.0];
   
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    isShowing = NO;
}



- (BOOL) isDialogShowing{
 
    return isShowing;

}
- (IBAction)actionCancel:(id)sender {
    [self dismissDialog];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
