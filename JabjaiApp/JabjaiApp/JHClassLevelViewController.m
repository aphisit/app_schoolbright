//
//  JHClassLevelViewController.m
//  JabjaiApp
//
//  Created by toffee on 11/21/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "JHClassLevelViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "TAStudentListViewController.h"
#import "TAStudentStatusModel.h"
#import "JHSelectStudenClassViewController.h"
#import "JHTimeOutAndDetailViewController.h"
#import "Utils.h"
#import "UserInfoViewController.h"

@interface JHClassLevelViewController (){
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    NSMutableArray *classroomIDStringArrray;
    NSMutableArray *sujectIDArrray;
    NSMutableArray *sujectNameArrray;
    NSMutableArray *selectClassroomID;
    NSMutableArray *selectClassroomName;
    NSMutableArray *authorizeSubMenuArray;
    BOOL notArrangeTimeTable;
    NSString *textFildeOnClick;
    // The dialog
    TableListDialog *tableListDialog;
    JHSelectRoomsTableListDialog *jhTableListDialog;
    JHSubjectTableListDialog *subjectTableListDialog;
    AlertDialog *alertDialog;
    UnAuthorizeMenuDialog *unAuthorizeMenuDialog;
    NSBundle *myLangBundle;
}
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomJForobHomeAPI *callGetSchoolClassroomAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomPersonAPI;
@property (strong, nonatomic) CallJHGetSubjectAPI *callJHGetSubjectAPI;
@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end
static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";
static NSString *subjectRequestCode = @"subjectRequestCode";

@implementation JHClassLevelViewController
double timerInterval = 1.0f;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    if ([UserData getUserType] != STUDENT) { [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//call to authorize menu
    }
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    [self doInit];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section2.layer setShadowOpacity:0.3];
    [self.section2.layer setShadowRadius:3.0];
    [self.section2.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section3.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section3.layer setShadowOpacity:0.3];
    [self.section3.layer setShadowRadius:3.0];
    [self.section3.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

- (void) doInit{
    sujectIDArrray = [[NSMutableArray alloc] init];
    sujectNameArrray = [[NSMutableArray alloc] init];
    classroomIDStringArrray = [[NSMutableArray alloc] init];
  
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.classLevelTextField.delegate = self;
    self.classroomTextField.delegate = self;
    self.subjectTextField.delegate = self;
    
    if(_classLevelArray != nil && _classroomArray != nil && _subjectArray != nil) {
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<_classLevelArray.count; i++) {
            SchoolLevelModel *model = [_classLevelArray objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        classroomStringArrray = [[NSMutableArray alloc] init];
        for(int i=0; i<_classroomArray.count; i++) {
            SchoolClassroomModel *model = [_classroomArray objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
            [classroomIDStringArrray addObject:[model getClassroomID]];
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:_selectedClassLevelIndex] getClassLevelName];
        self.classroomTextField.text = [[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomName];
        self.subjectTextField.text = [_subjectArray objectAtIndex:_selectedSubjectIndex].subjectName;
        notArrangeTimeTable = NO;
        
    }
    else {
        _selectedClassLevelIndex = -1;
        _selectedClassroomIndex = -1;
        _selectedSubjectIndex = -1;
        
        notArrangeTimeTable = YES;
       // [self showIndicator];
        [self getSchoolClassLevel];
    }
    
    [self setLanguage];
}

-(void)setLanguage{
    if(self.mode == 1){
        self.showheader.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_SEND_GROUP",nil,myLangBundle,nil);
    }else{
        self.showheader.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_SEND_INDIVIDUAL",nil,myLangBundle,nil);
    }
    self.headerClassLevelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_CLASSLEVEL",nil,myLangBundle,nil);
    self.headerClassLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_CLASSROOM",nil,myLangBundle,nil);
    self.headerSubjectLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_SUBJECT",nil,myLangBundle,nil);
    self.classLevelTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_JOBHOME_CLASSLEVEL",nil,myLangBundle,nil);
    self.classroomTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_JOBHOME_CLASSROOM",nil,myLangBundle,nil);
    self.subjectTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_JOBHOME_SUBJECT",nil,myLangBundle,nil);
    
    
    
    [_headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_TAKE_ATTEN_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
}

#pragma mark - CallGetMenuListAPIDelegate
///check authorize menu
- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
    [self showIndicator];
    // [self popUpLoaddin];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
    [self stopIndicator];
    if ((resCode == 300 || resCode == 200) && success ) {
        NSLog(@"xx");
        authorizeSubMenuArray = [[NSMutableArray alloc] init];
        if (data != NULL) {
            
            for (int i = 0;i < data.count; i++) {
                NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
                for (int j = 0; j < subMenuArray.count; j++) {
                    NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
                    [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
                }
            }
        }
        NSString* notPermissionText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_UNAUTHORRIZE",nil,[Utils getLanguage],nil);
        NSString* pleaseContactText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_PLEASECONTACT",nil,[Utils getLanguage],nil);
        NSString *alertMessage = [NSString stringWithFormat:@"%@\n%@",notPermissionText,pleaseContactText];
        if(self.mode == JH_MODE_PERSON){
            if (![authorizeSubMenuArray containsObject: @"7"]) {
                [self showAlertDialogAuthorizeMenu:alertMessage];
            }
        }else{
            if (![authorizeSubMenuArray containsObject: @"8"]) {
                [self showAlertDialogAuthorizeMenu:alertMessage];
            }
        }
    }else{
        
        self.dbHelper = [[UserInfoDBHelper alloc] init];
        [self.dbHelper deleteUserAllWithMasterId:[UserData getMasterUserID]];
        
        // Logout
        [UserData setUserLogin:NO];
        [UserData saveMasterUserID:0];
        [UserData saveUserID:0];
        [UserData setUserType:USERTYPENULL];
        [UserData setAcademyType:ACADEMYTYPENULL];
        [UserData setFirstName:@""];
        [UserData setLastName:@""];
        [UserData setUserImage:@""];
        [UserData setGender:MALE];
        [UserData setClientToken:@""];
        LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
}

- (void)onAlertDialogClose {
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // when user select class level textfield
      
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        else if(classLevelStringArray.count == 0){
            [self stopIndicator];
        }
        
        return NO;
    }
    else if(textField.tag == 2) { // select classroom textfield

        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_CLASSLEVEL",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            if (_mode == 1) {
                
                [self showLavelRoomTableListDialogWithRequestCode:classroomRequestCode data:_classroomGroupArray];
            }else{
                [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
            }
            
        }else if(classroomStringArrray.count == 0){
             [self stopIndicator];
        }
        
        return NO;
    }
    else if(textField.tag == 3) {
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_CLASSLEVEL",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(self.classroomTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_CLASSROOM",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(notArrangeTimeTable || _subjectArray == nil || _subjectArray.count <= 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_NO_REGISTERED",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(_subjectArray != nil && _subjectArray.count > 0) {
            
            // show dialog
            [self showSubjectTableListDialogWithRequestCode:subjectRequestCode data:_subjectArray];
        }
        
        return NO;
    }
    
    return YES;
}


#pragma mark - API Caller

- (void)getSchoolClassLevel {
     [self showIndicator];
    if(self.callGetSchoolLevelAPI != nil) {
        self.callGetSchoolLevelAPI = nil;
    }
    
    self.callGetSchoolLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolLevelAPI.delegate = self;
    [self.callGetSchoolLevelAPI call:[UserData getSchoolId]];
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    [self showIndicator];
    
    if (_mode == 1) {
        if(self.callGetSchoolClassroomAPI != nil) {
            self.callGetSchoolClassroomAPI = nil;
        }
        
        self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomJForobHomeAPI alloc] init];
        self.callGetSchoolClassroomAPI.delegate = self;
        [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
    }else{
        if(self.callGetSchoolClassroomPersonAPI != nil) {
            self.callGetSchoolClassroomPersonAPI = nil;
        }
        
        self.callGetSchoolClassroomPersonAPI = [[CallGetSchoolClassroomAPI alloc] init];
        self.callGetSchoolClassroomPersonAPI.delegate = self;
        [self.callGetSchoolClassroomPersonAPI call:[UserData getSchoolId] classLevelId:classLevelId];
        
    }
   
}

- (void)getSubjectDataWithClassroomId:(NSString*)classroomId {
    [self showIndicator];
    if(self.callJHGetSubjectAPI != nil) {
        self.callJHGetSubjectAPI = nil;
    }
    
    self.callJHGetSubjectAPI = [[CallJHGetSubjectAPI alloc] init];
    self.callJHGetSubjectAPI.delegate = self;
    [self.callJHGetSubjectAPI call:[UserData getSchoolId] classroomId:classroomId];
}

#pragma mark - CallGetSchoolClassLevelAPIDelegate

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    [self stopIndicator];
    if(success && data != nil) {
        
        _classLevelArray = data;
        
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
    }
   
}

#pragma mark - CallGetSchoolClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomJForobHomeAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    [self stopIndicator];
    if(success && data != nil) {
        
        _classroomArray = data;
        
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            
                SchoolClassroomModel *model = [data objectAtIndex:i];
                [classroomStringArrray addObject:[model classroomName]];
                [classroomIDStringArrray addObject:[model classroomID]];
            
           
        }
    }
}

- (void)callGetSchoolClassroomGroupAPI:(CallGetSchoolClassroomJForobHomeAPI *)classObj data:(NSArray<JHSelectRoomRadioModel *> *)data success:(BOOL)success {
    if(success && data != nil) {
        [self stopIndicator];
        _classroomGroupArray = data;
        
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            
                JHSelectRoomRadioModel *model = [data objectAtIndex:i];
                [classroomStringArrray addObject:[model classRoomName]];
                [classroomIDStringArrray addObject:[model classRoomID]];
            
            
        }
    }
}

#pragma mark - CallGetSubjectAPIDelegate

- (void)callJHGetSubjectAPI:(CallJHGetSubjectAPI *)classObj data:(NSArray<JHSubjectModel *> *)data success:(BOOL)success {
    [self stopIndicator];
    if(success && data != nil) {
        
        _subjectArray = data;
        notArrangeTimeTable = NO;
    }

    for(int i=0; i<data.count; i++) {
        JHSubjectModel *model = [data objectAtIndex:i];
        [sujectIDArrray addObject:[model getSubjectID]];
        [sujectNameArrray addObject:[model getSubjectName]];
        
    }
}

- (void)notArrangeTimeTable:(CallJHGetSubjectAPI *)classObj {
    notArrangeTimeTable = YES;
}

#pragma mark - CallGetStudentInClassAPIDelegate


- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)gotoTAStudentListViewController {
    NSMutableArray *_classroomId = [[NSMutableArray alloc] init];
    NSLog(@"selectGroupRoom = %@",selectClassroomID);
    if (self.mode == 0) {
        [_classroomId addObject:[classroomIDStringArrray objectAtIndex:_selectedClassroomIndex]];
        
        JHSelectStudenClassViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHSelectStudenClassStoryboard"];
        viewController.classroomId = _classroomId;
        viewController.subjectId = [[_subjectArray objectAtIndex:_selectedSubjectIndex] subjectID];
        viewController.subjectName = [[_subjectArray objectAtIndex:_selectedSubjectIndex] subjectName];
        viewController.sendType = 1;
        viewController.classLevelArray = _classLevelArray;
        viewController.classRoomArray = _classroomArray;
        viewController.sujectNameArrray = _subjectArray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.selectedSubjectIndex = _selectedSubjectIndex;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else{
        JHTimeOutAndDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHTimeOutAndDetailStoryboard"];
       
        viewController.levelId = selectClassroomID;
        viewController.sendType = 0;
        viewController.subjectId = [[_subjectArray objectAtIndex:_selectedSubjectIndex] subjectID];
        viewController.subjectName = [[_subjectArray objectAtIndex:_selectedSubjectIndex] subjectName];
        viewController.roomName = selectClassroomName ;
        
        viewController.classLevelArray = _classLevelArray;
        viewController.classRoomArray = _classroomArray;
        viewController.sujectNameArrray = _subjectArray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.selectedSubjectIndex = _selectedSubjectIndex;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//         [self.revealViewController pushFrontViewController:viewController animated:YES];
        
    }
   
}

#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_CLASSLEVEL",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classroomTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_CLASSROOM",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.subjectTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_SUBJECT",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
        [self stopIndicator];
    if([requestCode isEqualToString:classLevelRequestCode]) {
        
        if(index == _selectedClassLevelIndex) {
            return;
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classroomTextField.text = @"";
        self.subjectTextField.text = @"";
        
        _selectedClassroomIndex = -1;
        _selectedSubjectIndex = -1;
        
        // get classroom data with class level
        long long classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        
        [self getSchoolClassroomWithClassLevelId:classLevelId];
        
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        [self stopIndicator];
        if(index == _selectedClassroomIndex) {
            return;
        }
        
        self.classroomTextField.text = [[_classroomArray objectAtIndex:index] classroomName];
        _selectedClassroomIndex = index;
        
        // Clear dependency text field values;
        self.subjectTextField.text = @"";
        _selectedSubjectIndex = -1;
        
        // get subject data with classroom id
        long long classroomId = [[[_classroomArray objectAtIndex:index] classroomID] longLongValue];
        [self getSubjectDataWithClassroomId:[NSString stringWithFormat:@"%lld",classroomId]];
        
    }
    
    
}

- (void) onItemSelectClassrooms:(NSString *)requestCode dataClassroomName:(NSArray*)dataClassroomName dataClassroomID:(NSArray*)dataClassroomID{
    [self stopIndicator];
    NSLog(@"dataClassroom = %@",dataClassroomName);
    selectClassroomID = [[NSMutableArray alloc] init];
    selectClassroomName = [[NSMutableArray alloc] init];
    selectClassroomID = dataClassroomID;
    selectClassroomName = dataClassroomName;
    NSMutableString *sumClassroomName = [NSMutableString  stringWithString:@""] ;
    NSMutableString *sumClassroomID = [NSMutableString  stringWithString:@""] ;
     self.subjectTextField.text = @"";
    for( int i = 0;i<dataClassroomName.count;i++){
        NSLog(@"xxx = %d",dataClassroomID[i]);
        [sumClassroomName appendString:dataClassroomName[i]];
        [sumClassroomID appendString:[NSString stringWithFormat:@"%@",dataClassroomID[i]]];
        if (i<dataClassroomName.count-1) {
            [sumClassroomName appendString:@","];
            [sumClassroomID appendString:@","];
        }
    }
    self.classroomTextField.text = sumClassroomName;
     NSLog(@"dataClassroom = %@",sumClassroomName);
    [self getSubjectDataWithClassroomId:sumClassroomID];
}

#pragma mark - SubjectTableListDialogDelegate
- (void)subjectTableListDialog:(JHSubjectTableListDialog *)dialog onItemSelectWithRequestCode:(NSString *)requestCode atIndex:(NSInteger)index {
    [self stopIndicator];
    if([requestCode isEqualToString:subjectRequestCode]) {
        
        if(index == _selectedSubjectIndex) {
            return;
        }
         //[_subjectArray objectAtIndex:_selectedSubjectIndex].subjectName;
        //self.subjectTextField.text = [sujectNameArrray objectAtIndex:index];
        
        self.subjectTextField.text = [_subjectArray objectAtIndex:index].subjectName;;
        _selectedSubjectIndex = index;
    }
    
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
     //[self stopIndicator];
}

- (void)showLavelRoomTableListDialogWithRequestCode:(NSString *)requestCode data:(JHSelectRoomRadioModel *)data {
    
    if(jhTableListDialog != nil && [jhTableListDialog isDialogShowing]) {
        [jhTableListDialog dismissDialog];
        jhTableListDialog = nil;
    }
    
    jhTableListDialog = [[JHSelectRoomsTableListDialog alloc] initWithRequestCode:requestCode];
    jhTableListDialog.delegate = self;
    [jhTableListDialog showDialogInView:self.view dataArr:data];
    //[self stopIndicator];
}

- (void)showSubjectTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray<JHSubjectModel *> *)data {
    
    if(subjectTableListDialog != nil && [subjectTableListDialog isDialogShowing]) {
        [subjectTableListDialog dismissDialog];
        subjectTableListDialog = nil;
    }
    
    subjectTableListDialog = [[JHSubjectTableListDialog  alloc] initWithRequestCode:requestCode];
    subjectTableListDialog.delegate = self;
    [subjectTableListDialog showDialogInView:self.view subjectArray:data];
}

- (void)showAlertDialogAuthorizeMenu:(NSString *)message {
    if(unAuthorizeMenuDialog != nil && [unAuthorizeMenuDialog isDialogShowing]) {
        [unAuthorizeMenuDialog dismissDialog];
    }
    else {
        unAuthorizeMenuDialog = [[UnAuthorizeMenuDialog alloc] init];
        unAuthorizeMenuDialog.delegate = self;
    }
    [unAuthorizeMenuDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

-(void)onTick:(NSTimer*)timer
{  [self getSchoolClassLevel];
    NSLog(@"Tick...");
}
- (NSTimer *) timer {
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:timerInterval target:self selector:@selector(onTick:) userInfo:nil repeats:YES];
    }
    return _timer;
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Action functions
- (IBAction)openDrawer:(id)sender {
//    [self.revealViewController revealToggle:self.revealViewController];
   
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}

- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        [self gotoTAStudentListViewController];
        //[self getStudentInClassData];
    }
}



@end
