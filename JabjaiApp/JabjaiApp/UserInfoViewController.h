//
//  UserInfoViewController.h
//  JabjaiApp
//
//  Created by mac on 5/17/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "JabjaiApp-Swift.h"
#import "CAPSPageMenu.h"
#import <CoreData/CoreData.h>
#import "SlideMenuController.h"
#import "ChangPasswordDialog.h"
#import "CallGetMenuListAPI.h"
#import "LoginChangPasswordViewController.h"
#import "UserViewController.h"
#import "CardPfileXIBViewController.h"
#import <MKDropdownMenu/MKDropdownMenu.h>
#import "StudyViewController.h"
#import "CallOpenReadAllMessageAPI.h"
#import "MessageInBoxScrollableTextDialog.h"

#import "NFNewFutureSlideShowViewController.h"
#import "UserHealthHitorysTemperatureViewController.h"
#import "UserHealthViewController.h"
#import "StatisticsComeToSchoolViewController.h"
#import "UINoteInNotificationTableViewCell.h"
#import "SNDetailNewsViewController.h"
#import "JHDetailHomeWorkViewController.h"
#import "LIDetailLeaveViewControllerXIB.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
#import "CallInsetDataSystemNotificationPOSTAPI.h"
#import "CallSTSendTokenAndLanguageAPI.h"
extern NSString *tokenStr;
@protocol UserInfoViewControllerDelegate <NSObject>

@optional
@end





@interface UserInfoViewController : UIViewController <CAPSPageMenuDelegate, UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate, ChangPasswordDialogDelegate, CallGetMenuListAPIDelegate,UserViewControllerDelegate,MKDropdownMenuDelegate,MKDropdownMenuDataSource,StudyViewControllerDelegate, CallOpenReadAllMessageAPIDelegate,MessageInBoxScrollableTextDialogDelegate,UserHealthHitorysTemperatureDelegate, UserHealthViewControllerDelegate,StatisticsComeToSchoolDelegate,SNDetailNewsViewControllerDelegate,CallCRGetStatusClosedForRenovationAPIDelegate, CallInsetDataSystemNotificationPOSTAPIDelegate,CallSTSendTokenAndLanguageAPIDelegate>


@property (nonatomic, retain) id<UserInfoViewControllerDelegate> delegate;
// Deep link to open notification message
@property (nonatomic) long long messageID;
@property (nonatomic) BOOL showMessageDialogWhenOpen;
//@property (nonatomic) NSManagedObject schoolImageArray;
@property (weak, nonatomic) IBOutlet UIView *haederView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *baackGroundContentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *notificationButton;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeadMenuConstraint;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightTrailingConstraint;

- (IBAction)openDrawer:(id)sender;
- (IBAction)showNotification:(id)sender;

//-(void) showCardProfile;
//-(void) doClickMenu;
@end
