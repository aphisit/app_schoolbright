//
//  CallStatisticsComeToSchoolAPI.h
//  JabjaiApp
//
//  Created by Mac on 17/7/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AttendToSchoolModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallStatisticsComeToSchoolAPI;

@protocol CallStatisticsComeToSchoolAPIDelegate <NSObject>

- (void)callStatisticsComeToSchoolAPI:(CallStatisticsComeToSchoolAPI *)classObj data:(NSArray<AttendToSchoolModel *> *)data success:(BOOL)success;

@end
@interface CallStatisticsComeToSchoolAPI : NSObject
@property (nonatomic, weak) id<CallStatisticsComeToSchoolAPIDelegate> delegate;
- (void)call:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status;
@end

NS_ASSUME_NONNULL_END
