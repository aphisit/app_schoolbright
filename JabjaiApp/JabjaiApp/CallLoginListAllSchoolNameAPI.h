//
//  CallLoginListAllSchoolNameAPI.h
//  JabjaiApp
//
//  Created by toffee on 26/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginListAllSchoolNameModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallLoginListAllSchoolNameAPI;

@protocol CallLoginListAllSchoolNameAPIDelegate <NSObject>

- (void)callLoginListAllSchoolNameAPI:(CallLoginListAllSchoolNameAPI *)classObj data:(NSArray<LoginListAllSchoolNameModel *> *)data success:(BOOL)success;
@end

@interface CallLoginListAllSchoolNameAPI : NSObject
@property (nonatomic, weak) id<CallLoginListAllSchoolNameAPIDelegate> delegate;
- (void)call;
@end

NS_ASSUME_NONNULL_END
