//
//  ConfirmDialog.h
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ConfirmDialog;

@protocol ConfirmDialogDelegate <NSObject>

@optional
- (void)onConfirmDialog:(ConfirmDialog *)confirmDialog confirm:(BOOL)confirm;

@end

@interface ConfirmDialog : UIViewController

@property (weak ,nonatomic) id<ConfirmDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (strong, nonatomic) IBOutlet UIStackView *stackView;

- (IBAction)actionCancel:(id)sender;
- (IBAction)actionConfirm:(id)sender;

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end
