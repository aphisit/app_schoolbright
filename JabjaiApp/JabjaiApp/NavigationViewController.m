 //
//  NavigationViewController.m
//  JabjaiApp
//
//  Created by mac on 10/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

@import AirshipKit;

#import "NavigationViewController.h"
#import "NavTableViewCell.h"
#import "NavChildTableViewCell.h"
#import "NavUserTableViewCell.h"
#import "MessageInboxViewController.h"
#import "TASelectClassLevelViewController.h"
#import "BSSelectClassLevelViewController.h"
#import "TELevelClassViewController.h"
#import "JHClassLevelViewController.h"
#import "InboxMessageDBManager.h"
#import "UserData.h"
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserInfoDBHelper.h"
#import "MultipleUserModel.h"
#import "AddAccountLoginViewController.h"
#import <Sheriff/GIBadgeView.h>

#import "LoginViewController.h"
#import "NavCenterTableViewCell.h"

#import "ReportListStudentViewController.h"
#import "ReportListTeacherViewController.h"
#import "MGListMargetViewController.h"
#import "MGPasscodeViewController.h"
#import "ASAcceptStudentsMapsViewController.h"

@interface NavigationViewController () {
    
    NSMutableArray *cellDescriptors;
    NSMutableArray *visibleRowsPerSection;
    NSMutableArray *addLineBlack;
    UIAlertController *pending;
    UIActivityIndicatorView* indicator;
    NSInteger connectCounter;
    BOOL showMenuTakeEvent;
    NSMutableArray<MultipleUserModel *> *userArray;
    MultipleUserModel *userModelForFloatingUserIcon1;
    MultipleUserModel *userModelForFloatingUserIcon2;
    UIColor *greenColor;
    UIColor *darkGrayColor;
    UIColor *lightGrayColor;
    BOOL modeShowUser;
    NSBundle *myLangBundle;
    
    NSMutableArray *authorizeMenuArray; // authorize Menu user
    NSMutableArray *authorizeSubMenuArray;
    BOOL authorizeMenuList;
    
    ChangPasswordDialog *changPasswordDialog;
    NSString *amountUnreadLeave;
}

@property (nonatomic, strong) InboxMessageDBManager *inboxMessageDatabaseManager;
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (nonatomic, strong) GIBadgeView *floatingIcon1BadgeView;
@property (nonatomic, strong) GIBadgeView *floatingIcon2BadgeView;
@property (strong, nonatomic) CallGetTakeEventToChackAPI *callGetTakeEventToChackAPI;
@property (strong, nonatomic) CallChackSignUpPin *callChackSignUpPin;
@property (nonatomic, strong) CallGetUnreadMessageCountAPI *callGetUnreadMessageCountAPI;
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@property (nonatomic, strong) CallGetAmountUnreadLeaveAPI *callGetAmountUnreadLeaveAPI;
@property (nonatomic, strong) CallClearDataSystemNotificationPOSTAPI *callClearDataSystemNotificationPOSTAPI;
@end

static NSString *tableCellIdentifier = @"mainNavCell";
static NSString *tableChildCellIdentifier = @"childNavCell";
static NSString *cellIdentifier = @"UserCell";

static NSString *tableCenterCellIdentifier = @"centerNavCell";

static NSString *floatingUserIcon1RequestCode = @"icon1RequestCode";
static NSString *floatingUserIcon2RequestCode = @"icon2RequestCode";

@implementation NavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setModeShowUser:NO];
    connectCounter = 0;
    showMenuTakeEvent = NO;
    authorizeMenuList = NO;
    self.tblExpandable.dataSource = self;
    self.tblExpandable.delegate = self;
    visibleRowsPerSection = [[NSMutableArray alloc] init];
    addLineBlack = [[NSMutableArray alloc] init];
    authorizeMenuArray = [[NSMutableArray alloc] init];
    authorizeSubMenuArray = [[NSMutableArray alloc] init];
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource: [UserData getChangLanguage] ofType:@"lproj"]];
    [self configureTableView];
  
    //setup inbox database manager
    self.inboxMessageDatabaseManager = [[InboxMessageDBManager alloc] init];
    
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    userArray = [[NSMutableArray alloc] init];
    [self checkUsernameAndPasswordIsNull];
    [self updateUserList];
    //[self getUserInfoData];
    [self updateNavbarInfo];
    self.floatingIcon1BadgeView = [GIBadgeView new];
    [self.viewIcon1 addSubview:self.floatingIcon1BadgeView];
    self.viewIcon1.clipsToBounds = NO;
    self.viewIcon1.backgroundColor = [UIColor clearColor];
    
    self.floatingIcon2BadgeView = [GIBadgeView new];
    [self.viewIcon2 addSubview:self.floatingIcon2BadgeView];
    self.viewIcon2.clipsToBounds = NO;
    self.viewIcon2.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapSelectFloatingUserIcon:)];
    [self.floatingUserIcon1 addGestureRecognizer:tap1];
    [self.floatingUserIcon1 setMultipleTouchEnabled:YES];
    [self.floatingUserIcon1 setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapSelectFloatingUserIcon:)];
    [self.floatingUserIcon2 addGestureRecognizer:tap2];
    [self.floatingUserIcon2 setMultipleTouchEnabled:YES];
    [self.floatingUserIcon2 setUserInteractionEnabled:YES];
    
    greenColor = [UIColor colorWithRed:0.23 green:0.84 blue:0.58 alpha:1.0];
    
    darkGrayColor = [UIColor colorWithRed:0.56 green:0.56 blue:0.56 alpha:1.0];
    lightGrayColor = [UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1.0f];
    
    //    [self loadCellDescriptor];
    //   // [self.tblExpandable reloadData];
    [self.tblExpandable setSeparatorColor:lightGrayColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureTableView {
    [self.tblExpandable registerNib:[UINib nibWithNibName:NSStringFromClass([NavTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableCellIdentifier];
    [self.tblExpandable registerNib:[UINib nibWithNibName:NSStringFromClass([NavChildTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableChildCellIdentifier];
    [self.tblExpandable registerNib:[UINib nibWithNibName:NSStringFromClass([NavUserTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self.tblExpandable registerNib:[UINib nibWithNibName:NSStringFromClass([NavCenterTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableCenterCellIdentifier];
    [self.tblExpandable setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
   
}


- (void)viewWillAppear:(BOOL)animated {
    //[self callGetStatusServer];
    authorizeMenuArray = [[NSMutableArray alloc] init];
    authorizeSubMenuArray = [[NSMutableArray alloc] init];
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource: [UserData getChangLanguage] ofType:@"lproj"]];
    CGFloat max = MAX(self.userImage.frame.size.width, self.userImage.frame.size.height);
    self.userImage.layer.cornerRadius = max / 2.0;
    self.userImage.layer.masksToBounds = YES;
    
    max = MAX(self.floatingUserIcon1.frame.size.width, self.floatingUserIcon1.frame.size.height);
    self.floatingUserIcon1.layer.cornerRadius = max / 2.0;
    self.floatingUserIcon1.layer.masksToBounds = YES;
    
    self.floatingUserIcon2.layer.cornerRadius = max / 2.0;
    self.floatingUserIcon2.layer.masksToBounds = YES;
    
    
    if ([UserData getSchoolImage] == nil) {
            self.schoolImage.image = [UIImage imageNamed:@"profile_box_bg"];
    }else{
       self.schoolImage.image = [UIImage imageWithData: [UserData getSchoolImage]];
    }
    [self.viewProfileButton setTitle:NSLocalizedStringFromTableInBundle(@"NAVIGATION_VIEWPROFILE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    [self doCallGetAmountUnreadAPI:[UserData getUserID] schoolId:[UserData getSchoolId]];
    [self updateUserList];
    [self updateNavbarInfo];
    if ([UserData getUserType] != STUDENT) {// check authorize take event
        [self chackStatusTakeEvent];
    }else{
        self.backgroundIndicator.hidden = YES;
        [self loadCellDescriptor];
        [self.tblExpandable reloadData];
    }
    //check AuthorizeMenuForUser
    if ([UserData getUserID] > 0  ) {
        if ([UserData getUserType] == STUDENT) {
            [self callCheckResign:[UserData getUserID] clientToken:[UserData getClientToken]];//list authorize menu
        }
    }
    //check AuthorizeMenuForUser
//    if ([UserData getUserID] > 0  ) {
//        if ([UserData getUserType] == TEACHER) {
//             [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//list authorize menu
//        }
//    }
//    [self loadCellDescriptor];
//    [self.tblExpandable reloadData];
}

- (void) checkUsernameAndPasswordIsNull{
    NSLog(@"U = %@",[UserData getUsername]);
    NSLog(@"P = %@",[UserData getPassword]);
    NSLog(@"MasterID = %d",[UserData getMasterUserID]);
    NSLog(@"ID = %d",[UserData getUserID]);
    NSLog(@"username = %@",[UserData getFirstName]);
    
        if (([[UserData getUsername] isEqual:[NSNull null]] || [UserData getUsername] == NULL)  && ([[UserData getPassword] isEqual:[NSNull null]] || [UserData getUsername] == NULL)) {
       // if ([[UserData getUsername] isEqual:[NSNull null]] && [[UserData getPassword] isEqual:[NSNull null]]) {
            if ([UserData getMasterUserID] != [UserData getUserID]) {
                [self.dbHelper deleteUserWithMasterId:[UserData getMasterUserID] slaveId:[UserData getUserID]];
            }
            [[UAirship push] removeTags:[[UAirship push] tags]];
            [[UAirship push] updateRegistration];
            [UserData setUserLogin:NO];
            [UserData saveMasterUserID:0];
            [UserData saveUserID:0];
            [UserData setUserType:USERTYPENULL];
            [UserData setAcademyType:ACADEMYTYPENULL];
            [UserData setFirstName:@""];
            [UserData setLastName:@""];
            [UserData setUserImage:@""];
            [UserData setGender:MALE];
            [UserData setClientToken:@""];
            [UserData setUsername:@""];
            [UserData setPassword:@""];
            LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
            [self.slideMenuController changeMainViewController:viewController close:YES];
        }
        else{
            NSLog(@"xx");
        }
    
}

#pragma Check Resign
- (void) callCheckResign:(long long)userId clientToken:(NSString *)clientToken{
    [self showIndicator];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
        [self stopIndicator];
        if (resCode == 402 && success ) {
            self.dbHelper = [[UserInfoDBHelper alloc] init];
            [self.dbHelper deleteUserAllWithMasterId:[UserData getMasterUserID]];
            // Logout
            [UserData setUserLogin:NO];
            [UserData saveMasterUserID:0];
            [UserData saveUserID:0];
            [UserData setUserType:USERTYPENULL];
            [UserData setAcademyType:ACADEMYTYPENULL];
            [UserData setFirstName:@""];
            [UserData setLastName:@""];
            [UserData setUserImage:@""];
            [UserData setGender:MALE];
            [UserData setClientToken:@""];
            LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
            [self.slideMenuController changeMainViewController:viewController close:YES];
        }
}

#pragma mark - CallClearDataSystemNotificationPOSTAPI
- (void)callClearDataSystemNotification:(NSString*)token{
    self.callClearDataSystemNotificationPOSTAPI = nil;
    self.callClearDataSystemNotificationPOSTAPI = [[CallClearDataSystemNotificationPOSTAPI alloc] init];
    self.callClearDataSystemNotificationPOSTAPI.delegate = self;
    [self.callClearDataSystemNotificationPOSTAPI call:token];
}

#pragma TableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(modeShowUser) {
        return 1;
    }
    else {
        
        if(cellDescriptors != nil) {
            return cellDescriptors.count;
            
        }
        
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  
    if(modeShowUser) {
        
        if(userArray != nil) {
            return userArray.count + 1;
        }
        else {
            return 1;
        }
        
    }
    else {
        NSLog(@"count = %d",[[visibleRowsPerSection objectAtIndex:section] count]);
        return [[visibleRowsPerSection objectAtIndex:section] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(modeShowUser) {
        NSLog(@"modeShowUser1 = %d" , modeShowUser);
        NavUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;

        if(userArray == nil || userArray.count == 0 || indexPath.row > userArray.count - 1) {

            cell.imgUser.image = [UIImage imageNamed:@"ic_gray_plus"];
            cell.titleLabel.text = NSLocalizedStringFromTableInBundle(@"NAVIGATION_ADDACCOUNT",nil,myLangBundle,nil);
        }
        else {

            MultipleUserModel *model = [userArray objectAtIndex:indexPath.row];

            if([model getImageUrl] == nil || [[model getImageUrl] length] == 0) {

                if([model getGender] == 0) { // Male
                    cell.imgUser.image = [UIImage imageNamed:@"ic_user_info"];
                }
                else { // Female
                    cell.imgUser.image = [UIImage imageNamed:@"ic_user_women"];
                }
            }
            else {
                [cell.imgUser loadImageFromURL:[model getImageUrl]];
            }

            NSString *name = [[NSString alloc] initWithFormat:@"%@ %@", [model getFirstName], [model getLastName]];
            cell.titleLabel.text = name;

        }
        
        return cell;
    }
    else {
        NSLog(@"cellSection = %d" , indexPath.section);
        NSMutableDictionary *currentCellDescriptor = [self getCellDescriptorForIndexPath:indexPath];
        NSString *cellIdentifier = [currentCellDescriptor objectForKey:@"cellIdentifier"];
        NSString *title = [currentCellDescriptor objectForKey:@"title"];
        NSString *storyboardIdentifier = [currentCellDescriptor objectForKey:@"storyboardIdentifier"];
        //NSLog(@"title = %s" , title);

        if([cellIdentifier isEqualToString:tableCellIdentifier]) {
            NSLog(@"indexPart = %d" , indexPath.section);
            NavTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [self.tblExpandable setSeparatorColor:[UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:3.0f]];
            NSString *icon = [currentCellDescriptor objectForKey:@"icon"];
            NSInteger additionalRows = [[currentCellDescriptor objectForKey:@"additionalRows"] integerValue];
            Boolean isExpand = [[currentCellDescriptor objectForKey:@"isExpanded"] boolValue];
            
            cell.navIcon.image = [UIImage imageNamed:icon];
            cell.tag = indexPath.section;
             //ใส่เส้น
            if(title == nil){
                NSLog(@"indexSection = %d",indexPath.section);
//                cell.backgroundColor =  [UIColor blackColor];
                cell.navLabel.text = @"";
                cell.backgroundColor = [UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1.0f];
                cell.userInteractionEnabled = false;
            }else{
                if ([title isEqualToString:@"NAVIGATION_LEAVE_INBOX"]) {
                    cell.navLabel.text = [NSString stringWithFormat:@"%@ (%@)",NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil),amountUnreadLeave];
                }else{
                    cell.navLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil)];
                }
                
                cell.backgroundColor =  [UIColor whiteColor];
                cell.userInteractionEnabled = true;
            }
           
            if(additionalRows == 0) {
                [cell.navArrow setHidden:YES];
            }
            else {
                [cell.navArrow setHidden:NO];
            }

            if(isExpand) {
                if(!cell.navArrow.isHidden) {
                    cell.navArrow.image = [UIImage imageNamed:@"ic_sort_up"];
                }
            }
            else {
                if(!cell.navArrow.isHidden) {
                    cell.navArrow.image = [UIImage imageNamed:@"ic_sort_down"];
                }
            }
            self.tblExpandable.separatorInset = UIEdgeInsetsMake(0, 65, 0, 0);
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            //[self.tblExpandable setSeparatorColor:[UIColor clearColor]]; //x1
            return cell;
        }
        else if([cellIdentifier isEqualToString:tableChildCellIdentifier]) {
            NavChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

            //check inbox cell
            if([storyboardIdentifier isEqualToString:@"MessageInboxStoryboard"]) {
                int actualRowIndex = [[[visibleRowsPerSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] intValue];
                int unreadMessage = [self.inboxMessageDatabaseManager getNumberOfUnreadMessageWithUserID:(int)[UserData getUserID] typeID:actualRowIndex];

                if(unreadMessage > 0) {
                    title = [NSString stringWithFormat:@"%@ (%d)", title, unreadMessage];
                }

            }

            cell.accessoryType = UITableViewCellAccessoryNone;
            
//            cell.navLabel.text = title;
            cell.navLabel.text = NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil);

            //[self.tblExpandable setSeparatorColor:[UIColor clearColor]]; //x1
            return cell;
        }
        
        else if([cellIdentifier isEqualToString:tableCenterCellIdentifier]){
            NavCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableCenterCellIdentifier forIndexPath:indexPath];
            
            cell.backgroundColor = greenColor;
            cell.navLabel.textColor = [UIColor whiteColor];
            cell.navIcon.image = [UIImage imageNamed:@"call_center_worker"];
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            cell.navLabel.text = NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil);
            
            return cell;
        }
        
    }
    //return cell;
    return [[UITableViewCell alloc] init];
}


#pragma TableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"index = %d",indexPath.section);
    if(modeShowUser){
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            return 90;
        }else{
            return 50;
        }
    }
    else{
        if ([addLineBlack containsObject:@(indexPath.section)]) {
             return 6;
        }
        else{
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            {
                return 90;
            }else{
                return 50;
            }
           
            
        }
      

    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
       
        if(modeShowUser) {

            [self setModeShowUser:NO];

            if(userArray == nil || userArray.count == 0 || indexPath.row > userArray.count - 1) { // When select add account
                AddAccountLoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAccountLoginStoryboard"];
                viewController.userArray = userArray;
                
//                self.rootViewControler = [[UINavigationController alloc] initWithRootViewController:viewController];
                
                [self.slideMenuController changeMainViewController:viewController close:YES];
                
//                [self.revealViewController pushFrontViewController:viewController animated:YES];
            }
            else {
                [self popUpLoaddin];

                MultipleUserModel *model = [userArray objectAtIndex:indexPath.row];

                // Login with selected user
                [self reLogin:model];
                [NSTimer scheduledTimerWithTimeInterval:1.0
                                                 target:self
                                               selector:@selector(targetMethod:)
                                               userInfo:nil
                                                repeats:NO];
                
            }
        }
        else {
            NSMutableDictionary *currentCellDescriptor = [self getCellDescriptorForIndexPath:indexPath];
            NSInteger indexOfTappedRow = [[[visibleRowsPerSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] integerValue];
            Boolean isExpandable = [[currentCellDescriptor objectForKey:@"isExpandable"] boolValue];

            if(isExpandable) {
                Boolean shouldExpandAndShowSubRows = NO;
                Boolean isExpanded = [[currentCellDescriptor objectForKey:@"isExpanded"] boolValue];
                NSInteger additionalRows = [[currentCellDescriptor objectForKey:@"additionalRows"] integerValue];
                if(!isExpanded) {
                    shouldExpandAndShowSubRows = YES;
                }

                [[[cellDescriptors objectAtIndex:indexPath.section] objectAtIndex:indexOfTappedRow] setObject:[NSNumber numberWithBool:shouldExpandAndShowSubRows] forKey:@"isExpanded"];
                NSInteger last_index = indexOfTappedRow + additionalRows;

                for(NSInteger i=indexOfTappedRow+1; i<=last_index; i++) {
                    [[[cellDescriptors objectAtIndex:indexPath.section] objectAtIndex:i] setObject:[NSNumber numberWithBool:shouldExpandAndShowSubRows] forKey:@"isVisible"];
                }
                NSLog(@"section = %d",indexPath.section);
                [self updateIndicesOfVisibleRows] ;
                [self.tblExpandable reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];

            }
            else {
                //Go to another view

         //       SWRevealViewController *revealViewController = self.revealViewController;

                NSString *storyboardIdentifier = [currentCellDescriptor objectForKey:@"storyboardIdentifier"];
                NSLog(@"getStoryboardIdentifier = %@",storyboardIdentifier);
                NSLog(@"index = %d",indexPath.section);
                NSLog(@"isExpandable = %d",isExpandable);
               
                if([storyboardIdentifier isEqualToString:@"MessageInboxStoryboard"]) {

                    MessageInboxViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                    viewController.message_id = -1; // -1 mean do not show dialog
                    
                    if(indexOfTappedRow == 1) {
                        viewController.inboxType = ATTENDANCE;
                    }
                    else if(indexOfTappedRow == 2) {
                        viewController.inboxType = PURCHASING;
                    }
                    else if(indexOfTappedRow == 3) {
                        viewController.inboxType = TOPUP;
                    }
                    else if(indexOfTappedRow == 4) {
                        viewController.inboxType = ABSENCEREQUEST;
                    }
                    else if(indexOfTappedRow == 5) {
                        viewController.inboxType = NEWS;
                    }
                    
//                    self.rootViewControler = [[UINavigationController alloc] initWithRootViewController:viewController];
                    
                    [self.slideMenuController changeMainViewController:viewController close:YES];
                    
                    [self.tblExpandable setContentOffset:CGPointZero animated:NO];
                    

//                    [revealViewController pushFrontViewController:viewController animated:YES];
                }
                else if([storyboardIdentifier isEqualToString:@"LoginStoryboard"]) {
                    
                    
                    [self callClearDataSystemNotification:tokenStr];
                    
                    // Logout
                    [[UAirship push] removeTags:[[UAirship push] tags]];
                    [[UAirship push] updateRegistration];
                    [UserData setUserLogin:NO];
                    [UserData saveMasterUserID:0];
                    [UserData saveUserID:0];
                    [UserData setUserType:USERTYPENULL];
                    [UserData setAcademyType:ACADEMYTYPENULL];
                    [UserData setFirstName:@""];
                    [UserData setLastName:@""];
                    [UserData setUserImage:@""];
                    [UserData setGender:MALE];
                    [UserData setClientToken:@""];
                
                    LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
                    [self.slideMenuController changeMainViewController:viewController close:YES];
                    [self.tblExpandable setContentOffset:CGPointZero animated:NO];
                }
                else if([storyboardIdentifier isEqualToString:@"TakeClassAttendanceStoryboard"]) {

                    TASelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                    NSLog(@"nextViewww = %@",viewController);
                     NSLog(@"storyboardIdentifierrr = %@",storyboardIdentifier);
                    
                    if(indexOfTappedRow == 1) {
                        viewController.mode = TA_MODE_CHECK;
                    }
                    else {
                        viewController.mode = TA_MODE_EDIT;
                    }
                    
//                    self.rootViewControler = [[UINavigationController alloc] initWithRootViewController:viewController];
                    
                    [self.slideMenuController changeMainViewController:viewController close:YES];
                    
                    [self.tblExpandable setContentOffset:CGPointZero animated:NO];

//                    [revealViewController pushFrontViewController:viewController animated:YES];
                }
                else if([storyboardIdentifier isEqualToString:@"BSSelectClassLevelStoryboard"]) {

                    BSSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                    NSLog(@"nextViewww = %@",viewController);
                     NSLog(@"storyboardIdentifierrr = %@",storyboardIdentifier);
                    
                    if(indexOfTappedRow == 1) {
                        viewController.mode = BS_MODE_ADD;
                    }
                    else {
                        viewController.mode = BS_MODE_REDUCE;
                    }

//                    self.rootViewControler = [[UINavigationController alloc] initWithRootViewController:viewController];
                    
                    [self.slideMenuController changeMainViewController:viewController close:YES];
                    
//                    [revealViewController pushFrontViewController:viewController animated:YES];
                }
                else if([storyboardIdentifier isEqualToString:@"takeeventAttendanceStoryboard"]) {

                    TELevelClassViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                    NSLog(@"nextViewww = %@",viewController);
                     NSLog(@"storyboardIdentifierrr = %@",storyboardIdentifier);
                   
                    if(indexOfTappedRow == 1) {
                        viewController.mode = TA_MODE_CHECK;
                    }
                    else {
                        viewController.mode = TA_MODE_EDIT;
                    }

//                    self.rootViewControler = [[UINavigationController alloc] initWithRootViewController:viewController];
                    
                    [self.slideMenuController changeMainViewController:viewController close:YES];
                    
                    [self.tblExpandable setContentOffset:CGPointZero animated:NO];
                    
//                    [revealViewController pushFrontViewController:viewController animated:YES];
                }
                else if([storyboardIdentifier isEqualToString:@"JHClassLevelStoryboard"]) {

                    JHClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                    NSLog(@"nextViewww = %@",viewController);
                     NSLog(@"storyboardIdentifierrr = %@",storyboardIdentifier);
                   
                    if(indexOfTappedRow == 1) {
                        viewController.mode = 0;
                    }
                    else {
                        viewController.mode = 1;
                    }

//                    self.rootViewControler = [[UINavigationController alloc] initWithRootViewController:viewController];
                    
                    [self.slideMenuController changeMainViewController:viewController close:YES];
                    
                    [self.tblExpandable setContentOffset:CGPointZero animated:NO];
                    
//                    [revealViewController pushFrontViewController:viewController animated:YES];
                }
                
                else if ([storyboardIdentifier isEqualToString:@"ReportListTeacherStoryboard"]){
                    
                    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                    viewController.authorizeMenuArray = authorizeMenuArray;
                    viewController.authorizeSubMenuArray = authorizeSubMenuArray;
                        [self.slideMenuController changeMainViewController:viewController close:YES];
                }
                else if([storyboardIdentifier isEqualToString:@"MGListMargetViewStoryboard"]){
                    
                    self.callChackSignUpPin = nil;
                    self.callChackSignUpPin = [[ CallChackSignUpPin alloc] init];
                    self.callChackSignUpPin.delegate = self;
                    [self.callChackSignUpPin call:[UserData getUserID] schoolid:[UserData getSchoolId]];
                }
                else if ([storyboardIdentifier isEqualToString:@"ASAcceptStudentsMapsStoryboard"]){
                    ASAcceptStudentsMapsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                        [self.slideMenuController changeMainViewController:viewController close:YES];
                }
                
                else {
                    
                    UIViewController *nextView = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                    NSLog(@"nextViewww = %@",nextView);
                    NSLog(@"storyboardIdentifierrr = %@",storyboardIdentifier);
                    [self.slideMenuController changeMainViewController:nextView close:YES];
                    [self.tblExpandable setContentOffset:CGPointZero animated:NO];
                }
            }

            [self updateIndicesOfVisibleRows] ;
            [self.tblExpandable reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    }


}

#pragma CellDescriptionManangment
- (void)loadCellDescriptor {
    NSString *filePath = nil;
    NSLog(@"id = %d",[UserData getUserID]);
    NSLog(@"token = %@",[UserData getClientToken]);
    if([UserData getAcademyType] == SCHOOL && [UserData getUserType] == STUDENT) {
        filePath = [[NSBundle mainBundle] pathForResource:@"NavItemDescriptor" ofType:@"plist"];
    }
    else if([UserData getAcademyType] == SCHOOL && [UserData getUserType] == TEACHER) {
        filePath = [[NSBundle mainBundle] pathForResource:@"SchoolTeacherNavItemDescriptor" ofType:@"plist"];
    }
    else if([UserData getAcademyType] == UNIVERSITY && [UserData getUserType] == STUDENT) {
        filePath = [[NSBundle mainBundle] pathForResource:@"UniversityStudentNavItemDescriptor" ofType:@"plist"];
    }
    else if([UserData getAcademyType] == UNIVERSITY && [UserData getUserType] == TEACHER) {
        filePath = [[NSBundle mainBundle] pathForResource:@"UniversityTeacherNavItemDescriptor" ofType:@"plist"];
    }
    cellDescriptors = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    [self updateIndicesOfVisibleRows];
    
    [self.tblExpandable reloadData];
}

- (void)updateIndicesOfVisibleRows {
    
    [visibleRowsPerSection removeAllObjects];
    [addLineBlack removeAllObjects];
    for(int section=0; section<cellDescriptors.count; section++) {
        
        NSMutableArray *visibleRow = [[NSMutableArray alloc] init];
        NSMutableArray *currentSectionCells = [cellDescriptors objectAtIndex:section];
        
        for(int row=0; row<currentSectionCells.count; row++) {
            NSMutableDictionary *dict = [currentSectionCells objectAtIndex:row];
            
            NSString *title = [dict objectForKey:@"title"];
            Boolean isVisible = [[dict objectForKey:@"isVisible"] boolValue];

            NSLog(@"%@",NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil));
            NSLog(@"%@",NSLocalizedStringFromTableInBundle(@"NAVIGATION_FLAG_ATTENDANCE",nil,myLangBundle,nil));
            if(isVisible) {
                if ([UserData getUserType] == TEACHER) {
                    if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_FLAG_ATTENDANCE",nil,myLangBundle,nil)]) {
                        if (showMenuTakeEvent == YES) {//check take Event school
                            [visibleRow addObject:[NSNumber numberWithInt:row]];
                        }
                    }else{
                        [visibleRow addObject:[NSNumber numberWithInt:row]];
                    }
                    
                    
//                    if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_FLAG_ATTENDANCE",nil,myLangBundle,nil)]) {
//                        if (showMenuTakeEvent == YES) {//check take Event school
//                            if ([authorizeMenuArray containsObject: @"1"]) { //check take Event Individual
//                                [visibleRow addObject:[NSNumber numberWithInt:row]];
//                            }
//                        }
//
//                    }else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_TAKE_FLAG_ATTENDANCE",nil,myLangBundle,nil)]){
//                        if ([authorizeSubMenuArray containsObject: @"3"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//                    }
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_EDIT_FLAG_ATTENDANCE",nil,myLangBundle,nil)]){
//                        if ([authorizeSubMenuArray containsObject: @"10"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//                    }
//
//                    //Take attendance
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_TAKE_ATTENDANCE",nil,myLangBundle,nil)]){
//
//                        if ([authorizeMenuArray containsObject: @"2"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_TAKE_CLASS_ATTENDANCE",nil,myLangBundle,nil)]){
//
//                        if ([authorizeSubMenuArray containsObject: @"9"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_EDIT_CLASS_ATTENDANCE",nil,myLangBundle,nil)]){
//
//                        if ([authorizeSubMenuArray containsObject: @"11"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//
//                    //Behavior score
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_BEHAVIOR_SCORE",nil,myLangBundle,nil)]){
//
//                        if ([authorizeMenuArray containsObject: @"4"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_ADD_BEHAVIOR_SCORE",nil,myLangBundle,nil)]){//take attendance
//
//                        if ([authorizeSubMenuArray containsObject: @"6"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_REDUCE_BEHAVIOR_SCORE",nil,myLangBundle,nil)]){//take attendance
//
//                        if ([authorizeSubMenuArray containsObject: @"12"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//
//                    //Homework
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_HOMEWORK",nil,myLangBundle,nil)]){
//
//                        if ([authorizeMenuArray containsObject: @"5"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_SEND_INDIVIDUAL",nil,myLangBundle,nil)]){//take attendance
//
//                        if ([authorizeSubMenuArray containsObject: @"7"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_SEND_GROUP",nil,myLangBundle,nil)]){//take attendance
//
//                        if ([authorizeSubMenuArray containsObject: @"8"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//
//                    //Leave
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_LEAVE",nil,myLangBundle,nil)]){
//
//                        if ([authorizeMenuArray containsObject: @"14"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_FULL_DAY",nil,myLangBundle,nil)]){//take attendance
//
//                        if ([authorizeSubMenuArray containsObject: @"30"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_HALF_DAY",nil,myLangBundle,nil)]){
//
//                        if ([authorizeSubMenuArray containsObject: @"31"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//                    //Send message
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_SEND_MESSAGE",nil,myLangBundle,nil)]){
//
//                        if ([authorizeMenuArray containsObject: @"13"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_TOPUP",nil,myLangBundle,nil)]){
//
//                        if ([authorizeMenuArray containsObject: @"15"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//
//
//                    else if ([NSLocalizedStringFromTableInBundle(title,nil,myLangBundle,nil) isEqualToString:NSLocalizedStringFromTableInBundle(@"NAVIGATION_SHOPPING",nil,myLangBundle,nil)]){
//
//                        if ([authorizeMenuArray containsObject: @"16"]) {
//                            [visibleRow addObject:[NSNumber numberWithInt:row]];
//                        }
//
//                    }
//
//                    else{
//                        [visibleRow addObject:[NSNumber numberWithInt:row]];
//                    }
                }else{
                     [visibleRow addObject:[NSNumber numberWithInt:row]];
                }
                
                
                
                if(title == nil){
                    [addLineBlack addObject: @(section)];
                }
            }
        }
        //[self stopIndicator];
        [visibleRowsPerSection addObject:visibleRow];
    }
    NSLog(@"Finish Update");
}

- (NSMutableDictionary*) getCellDescriptorForIndexPath:(NSIndexPath*)indexPath {
    //Get actual index of celldescriptor from index of visual row
    NSLog(@"section = %d",indexPath.section);
    NSMutableArray *visibleRow =[visibleRowsPerSection objectAtIndex:indexPath.section];
    NSInteger indexOfVisibleRow = [[visibleRow objectAtIndex:indexPath.row] integerValue];
    NSMutableDictionary *descriptor = [[cellDescriptors objectAtIndex:indexPath.section] objectAtIndex:indexOfVisibleRow];
    return descriptor;
}

- (IBAction)viewProfile:(id)sender {
    UIViewController *nextView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileStoryboard"];
    [self.slideMenuController changeMainViewController:nextView close:YES];
}

- (IBAction)actionShowUser:(id)sender {
    
    [self setModeShowUser:!modeShowUser];
    if(modeShowUser) {
        [self updateUserList];
    }
    
    [self.tblExpandable reloadData];
}



- (void)getUnreadMessageCountWithUserId:(long long)userId requestCode:(NSString *)requestCode schoolid:(long long)schoolid {
    
    self.callGetUnreadMessageCountAPI = nil;
    self.callGetUnreadMessageCountAPI = [[CallGetUnreadMessageCountAPI alloc] init];
    self.callGetUnreadMessageCountAPI.delegate = self;
    [self.callGetUnreadMessageCountAPI call:userId requestCode:requestCode schoolid:schoolid];
}

#pragma mark - CallGetUnreadMessageCountAPIDelegate
- (void)callGetUnreadMessageCountAPI:(CallGetUnreadMessageCountAPI *)classObj unreadCount:(NSInteger)unreadCount requestCode:(NSString *)requestCode success:(BOOL)success {
    
    if(success) {
        
        if([requestCode isEqualToString:floatingUserIcon1RequestCode]) {
            [self.floatingIcon1BadgeView setBadgeValue:unreadCount];
        }
        else if([requestCode isEqualToString:floatingUserIcon2RequestCode]) {
            [self.floatingIcon2BadgeView setBadgeValue:unreadCount];
        }
    }
    else {
        if([requestCode isEqualToString:floatingUserIcon1RequestCode]) {
            [self.floatingIcon1BadgeView setBadgeValue:0];
        }
        else if([requestCode isEqualToString:floatingUserIcon2RequestCode]) {
            [self.floatingIcon2BadgeView setBadgeValue:0];
        }
    }
}

#pragma mark - Utility

- (void)reLogin:(MultipleUserModel *)model {
        
    if ([[model getUsername]isEqual:[NSNull null]] && [[model getPassword] isEqual:[NSNull null]]) {
        if ([model getMasterId] != [model getSlaveId]) {
            [self.dbHelper deleteUserWithMasterId:[model getMasterId] slaveId:[model getSlaveId]];
        }
        [[UAirship push] removeTags:[[UAirship push] tags]];
        [[UAirship push] updateRegistration];
        [UserData setUserLogin:NO];
        [UserData saveMasterUserID:0];
        [UserData saveUserID:0];
        [UserData setUserType:USERTYPENULL];
        [UserData setAcademyType:ACADEMYTYPENULL];
        [UserData setFirstName:@""];
        [UserData setLastName:@""];
        [UserData setUserImage:@""];
        [UserData setGender:MALE];
        [UserData setClientToken:@""];
        [UserData setUsername:@""];
        [UserData setPassword:@""];
        LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else{
        [UserData saveUserID:[model getSlaveId]];
        [UserData setUserLogin:YES];
        [UserData saveSchoolId:[model getSchoolId]];
        [UserData setFirstName:[model getFirstName]];
        [UserData setLastName:[model getLastName]];
        [UserData setUserImage:[model getImageUrl]];
        [UserData setClientToken:[model getClientToken]];
        [UserData setUsername:[model getUsername]];
        [UserData setPassword:[model getPassword]];
     
        if([model getAcademyType] == 1) {
            [UserData setAcademyType:SCHOOL];
        }
        else {
            [UserData setAcademyType:UNIVERSITY];
        }
        
        if([model getUserType] == 0) {
            [UserData setUserType:STUDENT];
        }
        else {
            [UserData setUserType:TEACHER];
        }
        
        if([model getGender] == 0) {
            [UserData setGender:MALE];
        }
        else {
            [UserData setGender:FEMALE];
        }
        
        // Clear expandable navbar item
        
        if(cellDescriptors != nil) {
            [cellDescriptors removeAllObjects];
            cellDescriptors = nil;
        }
        cellDescriptors = [[NSMutableArray alloc] init];
        
        if(visibleRowsPerSection != nil) {
            [visibleRowsPerSection removeAllObjects];
            visibleRowsPerSection = nil;
        }
        
        visibleRowsPerSection = [[NSMutableArray alloc] init];
        
        [self updateUserList];
        [self updateNavbarInfo];
    }
   // [self chackStatusTakeEvent];
//    [self loadCellDescriptor];
//    [self.tblExpandable reloadData];
}

- (void)updateUserList {
    
        NSArray *users = [self.dbHelper getUserDataWithMasterId:[UserData getMasterUserID]];
        if(userArray != nil) {
            [userArray removeAllObjects];
            userArray = nil;
        }
        userArray = [[NSMutableArray alloc] init];
    
        if(users != nil) {
            //if (users.count > 0) {
                for(MultipleUserModel *model in users) {
                    if([model getSlaveId] != [UserData getUserID]) {
                        [userArray addObject:model];
                    }
                }
        }
        NSLog(@"zzzz");
}

- (void)updateNavbarInfo {
    
    NSString *name = [[NSString alloc] initWithFormat:@"%@ %@", [UserData getFirstName], [UserData getLastName]];
    NSString *imageUrl = [UserData getUserImage];
    
    self.userNameLabel.text = name;
    
    if(imageUrl == nil || imageUrl.length == 0) {
        
        if([UserData getGender] == MALE) {
            self.userImage.image = [UIImage imageNamed:@"ic_user_default"];
        }
        else {
            self.userImage.image = [UIImage imageNamed:@"ic_user_default"];
        }
        
    }
    else {
        [self.userImage loadImageFromURL:imageUrl];
    }
   
    [self updateFloatingIcon];
    // [self getUserInfoData];
}

- (void)updateFloatingIcon {
    
    if(userArray == nil || userArray.count == 0) {
        self.floatingUserIcon1.hidden = YES;
        self.floatingUserIcon2.hidden = YES;
        
        [self.floatingIcon1BadgeView setBadgeValue:0];
        [self.floatingIcon2BadgeView setBadgeValue:0];
        
    }
    else if(userArray.count == 1) {
        self.floatingUserIcon1.hidden = YES;
        self.floatingUserIcon2.hidden = NO;
        
        [self.floatingIcon1BadgeView setBadgeValue:0];
        userModelForFloatingUserIcon2 = [userArray objectAtIndex:0];
        NSString *imageUrl = [userModelForFloatingUserIcon2 getImageUrl];
        if(imageUrl == nil || imageUrl.length == 0) {
            if([userModelForFloatingUserIcon2 getGender] == 0) { // male
                self.floatingUserIcon2.image = [UIImage imageNamed:@"ic_user_info"];
            }
            else {
                self.floatingUserIcon2.image = [UIImage imageNamed:@"ic_user_women"];
            }
        }
        else {
            [self.floatingUserIcon2 loadImageFromURL:imageUrl];
        }
        [self getUnreadMessageCountWithUserId:[userModelForFloatingUserIcon2 getSlaveId] requestCode:floatingUserIcon2RequestCode schoolid:[UserData getSchoolId]];
    }
    else {
        self.floatingUserIcon1.hidden = NO;
        self.floatingUserIcon2.hidden = NO;
        userModelForFloatingUserIcon1 = [userArray objectAtIndex:0];
        userModelForFloatingUserIcon2 = [userArray objectAtIndex:1];
        NSString *imageUrl1 = [userModelForFloatingUserIcon1 getImageUrl];
        NSString *imageUrl2 = [userModelForFloatingUserIcon2 getImageUrl];
        if(imageUrl1 == nil || imageUrl1.length == 0) {
            if([userModelForFloatingUserIcon1 getGender] == 0) { // male
                self.floatingUserIcon1.image = [UIImage imageNamed:@"ic_user_info"];
            }
            else {
                self.floatingUserIcon1.image = [UIImage imageNamed:@"ic_user_women"];
            }
        }
        else {
            [self.floatingUserIcon1 loadImageFromURL:imageUrl1];
        }
        if(imageUrl2 == nil || imageUrl2.length == 0) {
            if([userModelForFloatingUserIcon2 getGender] == 0) { // male
                self.floatingUserIcon2.image = [UIImage imageNamed:@"ic_user_info"];
            }
            else {
                self.floatingUserIcon2.image = [UIImage imageNamed:@"ic_user_women"];
            }
        }
        else {
            [self.floatingUserIcon2 loadImageFromURL:imageUrl2];
        }
        [self getUnreadMessageCountWithUserId:[userModelForFloatingUserIcon1 getSlaveId] requestCode:floatingUserIcon1RequestCode schoolid:[UserData getSchoolId]];
        [self getUnreadMessageCountWithUserId:[userModelForFloatingUserIcon2 getSlaveId] requestCode:floatingUserIcon2RequestCode schoolid:[UserData getSchoolId]];
    }
}

- (void)setModeShowUser:(BOOL)show {
    modeShowUser = show;
    if(modeShowUser) {
        [self.dropdownImage setImage:[UIImage imageNamed:@"ic_dropdown_up_white"]];
        //[self.btnDropdown setImage:[UIImage imageNamed:@"ic_dropdown_up_white"] forState:UIControlStateNormal];
    }
    else {
        [self.dropdownImage setImage:[UIImage imageNamed:@"ic_dropdown_white"]];
        //[self.btnDropdown setImage:[UIImage imageNamed:@"ic_dropdown_white"] forState:UIControlStateNormal];
    }
}
- (void)popUpLoaddin{
    pending = [UIAlertController alertControllerWithTitle:nil message:@"Please wait...\n\n" preferredStyle:UIAlertControllerStyleAlert];
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.color = [UIColor blackColor];
    indicator.translatesAutoresizingMaskIntoConstraints=NO;
    [pending.view addSubview:indicator];
    NSDictionary * views = @{@"pending" : pending.view, @"indicator" : indicator};
    NSArray * constraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[indicator]-(20)-|" options:0 metrics:nil views:views];
    NSArray * constraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[indicator]|" options:0 metrics:nil views:views];
    NSArray * constraints = [constraintsVertical arrayByAddingObjectsFromArray:constraintsHorizontal];
    [pending.view addConstraints:constraints];
    [indicator setUserInteractionEnabled:NO];
    [indicator startAnimating];
    [self presentViewController:pending animated:YES completion:nil];
}
#pragma mark - TapGesture
- (void)onTapSelectFloatingUserIcon:(UITapGestureRecognizer *)gesture {
    [self popUpLoaddin];
    if(gesture.view.tag == 1) { // select floating user icon 1
        [self setModeShowUser:NO];
         //Login with selected user
        [self reLogin:userModelForFloatingUserIcon1];
        [NSTimer scheduledTimerWithTimeInterval:1.0
                                         target:self
                                       selector:@selector(targetMethod:)
                                       userInfo:nil
                                        repeats:NO];
    }
    else if(gesture.view.tag == 2) { // select floating user icon 2
        
        [self setModeShowUser:NO];
      
        [self reLogin:userModelForFloatingUserIcon2];
        [NSTimer scheduledTimerWithTimeInterval:1.0
                                         target:self
                                       selector:@selector(targetMethod:)
                                       userInfo:nil
                                        repeats:NO];
    }
}
-(void)targetMethod:(NSTimer *)timer {
    [indicator stopAnimating];
    [pending dismissViewControllerAnimated:YES completion:nil];
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (void)chackStatusTakeEvent{
    [self showIndicator];
    self.callGetTakeEventToChackAPI = nil;
    self.callGetTakeEventToChackAPI = [[ CallGetTakeEventToChackAPI alloc] init];
    self.callGetTakeEventToChackAPI.delegate = self;
    [self.callGetTakeEventToChackAPI call:[UserData getSchoolId]];
}
#pragma mark - CallGetTAHistoryStudentStatusListAPIDelegate
- (void)callGetTakeEventToChackAPI:(CallGetTakeEventToChackAPI *)classObj data:(BOOL)data success:(BOOL)success {

    [self stopIndicator];
    NSLog(@"data = %d",data);

    showMenuTakeEvent = data;

    [self loadCellDescriptor];
    [self.tblExpandable reloadData];
}

-(void)callCheckSignUpPin:(NSString*)status success:(BOOL)success{
    if(success){
        if ([status isEqualToString:@"Success"]) {
             MGListMargetViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGListMargetViewStoryboard"];
            [self.slideMenuController changeMainViewController:viewController close:YES];
        }else{
            MGPasscodeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGPasscodeStoryboard"];
            [self.slideMenuController changeMainViewController:viewController close:YES];
        }
    }
}

#pragma mark - CallGetAmountUnreadLeaveAPI
-(void)doCallGetAmountUnreadAPI:(long long)userId schoolId:(long long)schoolId{
    self.callGetAmountUnreadLeaveAPI = nil;
    self.callGetAmountUnreadLeaveAPI = [[CallGetAmountUnreadLeaveAPI alloc] init];
    self.callGetAmountUnreadLeaveAPI.delegate = self;
    [self.callGetAmountUnreadLeaveAPI call:userId schoolID:schoolId];
}
-(void)callGetAmountUnreadLeaveAPI:(NSString*)amount{
    amountUnreadLeave = amount;
    NSLog(@"Amount = %@",amount);
}

//- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
//    [self showIndicator];
//   // [self popUpLoaddin];
//    self.callGetMenuListAPI = nil;
//    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
//    self.callGetMenuListAPI.delegate = self;
//    [self.callGetMenuListAPI call:userId clientToken:clientToken];
//}
//- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
////    [pending dismissViewControllerAnimated:YES completion:nil];
////    [indicator stopAnimating];
//
//    [self stopIndicator];
//    //authorizeMenuArray = data;
//    authorizeMenuList = YES;
//
//        // NSMutableArray *mainMenu = [[NSMutableArray alloc] init];
//    if (data != NULL) {
//        for (int i = 0;i < data.count; i++) {
//            NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
//            NSInteger menuId = [[[data objectAtIndex:i] objectForKey:@"menuGroup_Id"] integerValue];
//            [authorizeMenuArray addObject: [NSString stringWithFormat:@"%d",menuId]];
//            for (int j = 0; j < subMenuArray.count; j++) {
//                NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
//                [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
//            }
//        }
//    }
//
//
//     [self loadCellDescriptor];
//}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {

        self.backgroundIndicator.hidden = NO;
        [self.indicator startAnimating];

    }

}
- (void)stopIndicator {

    self.backgroundIndicator.hidden = YES;
    [self.indicator stopAnimating];
}

@end
