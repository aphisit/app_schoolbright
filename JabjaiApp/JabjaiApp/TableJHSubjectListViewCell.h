//
//  TableJHSubjectListViewCell.h
//  JabjaiApp
//
//  Created by toffee on 11/22/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableJHSubjectListViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end
