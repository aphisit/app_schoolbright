//
//  CallTEGetDataStudentScanBarcodeAPI.m
//  JabjaiApp
//
//  Created by toffee on 1/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallTEGetDataStudentScanBarcodeAPI.h"
#import "Utils.h"
#import "APIURL.h"
@interface CallTEGetDataStudentScanBarcodeAPI () {
    NSInteger connectCounter;
    TEDataStudentScanerModel *dataStudent;
    NSInteger resCode;
}
@end
@implementation CallTEGetDataStudentScanBarcodeAPI
- (void)call:(long long)schoolId idStudent:(NSString *)idStudent{
    connectCounter = 0;
    [self doGetDataStudent:schoolId idStudent:idStudent];
}

- (void)doGetDataStudent:(long long)schoolId idStudent:(NSString *)idStudent{
    
    NSString *URLString = [APIURL getTEGetDataStudentScanBarcode:schoolId idStudent:idStudent];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                // [self getStudentInClass:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    // [self getStudentInClass:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            
            else {
                
                NSMutableString *resDesc ,*firstName,*lastName,*picture,*classRoom;
                NSDictionary *dict = returnedData;
                resCode = [[dict objectForKey:@"resCode"] integerValue];
                
                        resDesc = [[NSMutableString alloc] initWithFormat:@"%@", [dict objectForKey:@"resDesc"]];
                        
                        NSData *data = [resDesc dataUsingEncoding:NSUTF8StringEncoding];
                        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                        
                        if(![[json objectForKey:@"name"] isKindOfClass:[NSNull class]]) {
                            firstName = [[NSMutableString alloc] initWithFormat:@"%@", [json objectForKey:@"name"]];
                        }else{
                            firstName = [[NSMutableString alloc] initWithString:@""];
                        }
                        if(![[json objectForKey:@"lastname"] isKindOfClass:[NSNull class]]) {
                            lastName = [[NSMutableString alloc] initWithFormat:@"%@", [json objectForKey:@"lastname"]];
                        }else{
                            lastName = [[NSMutableString alloc] initWithString:@""];
                        }
                        if(![[json objectForKey:@"picture"] isKindOfClass:[NSNull class]]) {
                            picture = [[NSMutableString alloc] initWithFormat:@"%@", [json objectForKey:@"picture"]];
                        }else{
                            picture = [[NSMutableString alloc] initWithString:@""];
                        }
                        if(![[json objectForKey:@"classroom"] isKindOfClass:[NSNull class]]) {
                            classRoom = [[NSMutableString alloc] initWithFormat:@"%@", [json objectForKey:@"classroom"]];
                        }else{
                            classRoom = [[NSMutableString alloc] initWithString:@""];
                        }
                        NSLog(@"xxx");
                
                
                CFStringTrimWhitespace((__bridge CFMutableStringRef) firstName);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) lastName);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) picture);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) classRoom);
                
                dataStudent = [[TEDataStudentScanerModel alloc] init];
                [dataStudent setStudentFirstName:firstName];
                [dataStudent setStudentLastName:lastName];
                [dataStudent setStudentPicture:picture];
                [dataStudent setStudentClass:classRoom];
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTEGetDataStudentScanBarcodeAPI:data:resCode:success:)]) {
                    [self.delegate callTEGetDataStudentScanBarcodeAPI:self data:dataStudent resCode:resCode success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTEGetDataStudentScanBarcodeAPI:data:resCode:success:)]) {
                [self.delegate callTEGetDataStudentScanBarcodeAPI:self data:dataStudent resCode:resCode success:NO];
            }
        }
    }];
}

@end
