//
//  CallGetUnreadMessageCountAPI.h
//  JabjaiApp
//
//  Created by mac on 5/18/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CallGetUnreadMessageCountAPI;
@protocol CallGetUnreadMessageCountAPIDelegate <NSObject>

@optional
- (void)callGetUnreadMessageCountAPI:(CallGetUnreadMessageCountAPI *)classObj unreadCount:(NSInteger)unreadCount success:(BOOL)success;
- (void)callGetUnreadMessageCountAPI:(CallGetUnreadMessageCountAPI *)classObj unreadCount:(NSInteger)unreadCount requestCode:(NSString *)requestCode success:(BOOL)success;

@end

@interface CallGetUnreadMessageCountAPI : NSObject

@property (nonatomic, weak) id<CallGetUnreadMessageCountAPIDelegate> delegate;

@property (nonatomic, strong) NSString *requestCode;

- (void)call:(long long)userID schoolid:(long long)schoolid;
- (void)call:(long long)userID requestCode:(NSString *)requestCode schoolid:(long long)schoolid;

@end
