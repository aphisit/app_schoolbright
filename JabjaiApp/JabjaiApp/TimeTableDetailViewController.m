//
//  TimeTableDetailViewController.m
//  JabjaiApp
//
//  Created by mac on 5/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TimeTableDetailViewController.h"
#import "SScheduleDetailModel.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"
#import "TimeTableViewController.h"

@interface TimeTableDetailViewController () {
    
    SScheduleDetailModel *scheduleDetailModel;
    
    NSInteger connectCounter;
}

@end

@implementation TimeTableDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.headerTitle.text = self.subjectName;
    
    [self clearDisplay];
    [self getScheduleDetailWithSubjectID:self.subjectID date:self.consideredDate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}



#pragma mark - Class actions
- (IBAction)moveBack:(id)sender {
    
    TimeTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TimeTableStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}

#pragma mark - Get API Data
- (void)getScheduleDetailWithSubjectID:(long long)subjectID date:(NSDate *)date {
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getStudentScheduleListDetailURLWithUserID:userID date:date subjectID:subjectID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getScheduleDetailWithSubjectID:subjectID date:date];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getScheduleDetailWithSubjectID:subjectID date:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getScheduleDetailWithSubjectID:subjectID date:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long subjectID = [[dataDict objectForKey:@"ScheduleId"] longLongValue];
                    
                    NSMutableString *subjectName, *subjectCode, *roomNumber, *lecturer, *startTime, *endTime, *clockInTime, *clockOutTime;
                    NSInteger clockInStatus, clockOutStatus;
                    
                    if(![[dataDict objectForKey:@"ScheduleName"] isKindOfClass:[NSNull class]]) {
                        subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScheduleName"]];
                    }
                    else {
                        subjectName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"ScheduleCode"] isKindOfClass:[NSNull class]]) {
                        subjectCode = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScheduleCode"]];
                    }
                    else {
                        subjectCode = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"Room"] isKindOfClass:[NSNull class]]) {
                        roomNumber = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Room"]];
                    }
                    else {
                        roomNumber = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TeacherName"] isKindOfClass:[NSNull class]]) {
                        lecturer = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TeacherName"]];
                    }
                    else {
                        lecturer = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TimeIn"] isKindOfClass:[NSNull class]]) {
                        startTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeIn"]];
                    }
                    else {
                        startTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TimeOut"] isKindOfClass:[NSNull class]]) {
                        endTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeOut"]];
                    }
                    else {
                        endTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"ScanIn"] isKindOfClass:[NSNull class]]) {
                        clockInTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScanIn"]];
                    }
                    else {
                        clockInTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"ScanOut"] isKindOfClass:[NSNull class]]) {
                        clockOutTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScanOut"]];
                    }
                    else {
                        clockOutTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"StatusIn"] isKindOfClass:[NSNull class]]) {
                        clockInStatus = [[dataDict objectForKey:@"StatusIn"] integerValue];
                    }
                    else {
                        clockInStatus = 3;
                    }
                    
                    if(![[dataDict objectForKey:@"StatusOut"] isKindOfClass:[NSNull class]]) {
                        clockOutStatus = [[dataDict objectForKey:@"StatusOut"] integerValue];
                    }
                    else {
                        clockOutStatus = 3;
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectCode);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) roomNumber);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lecturer);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) clockInTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) clockOutTime);
                    
                    SScheduleDetailModel *model = [[SScheduleDetailModel alloc] init];
                    model.subjectID = subjectID;
                    model.subjectName = subjectName;
                    model.subjectCode = subjectCode;
                    model.roomNumber = roomNumber;
                    model.lecturer = lecturer;
                    model.courseStartTime = startTime;
                    model.courseEndTime = endTime;
                    model.clockInTime = clockInTime;
                    model.clockInStatus = clockInStatus;
                    model.clockOutTime = clockOutTime;
                    model.clockOutStatus = clockOutStatus;
                    
                    scheduleDetailModel = model;
                    break;
                    
                }
                
                [self performData];
            }
        }
        
    }];

}

- (void)performData {
    
    if(scheduleDetailModel != nil) {
        self.subjectLabel.text = [[NSString alloc] initWithFormat:@"วิชา %@", scheduleDetailModel.subjectName];
        self.subjectCodeLabel.text = [[NSString alloc] initWithFormat:@"รหัส %@", scheduleDetailModel.subjectCode];
        self.roomLabel.text = [[NSString alloc] initWithFormat:@"ห้อง %@", scheduleDetailModel.roomNumber];
        self.lecturerLabel.text = [[NSString alloc] initWithFormat:@"ผู้สอน %@", scheduleDetailModel.lecturer];
        
        if(scheduleDetailModel.courseStartTime != nil && scheduleDetailModel.courseStartTime.length > 0) {
            self.courseStartTimeLabel.text = [[NSString alloc] initWithFormat:@"%@ น.", scheduleDetailModel.courseStartTime];
        }
        else {
            self.courseStartTimeLabel.text = @"n/a";
        }
        
        if(scheduleDetailModel.courseEndTime != nil && scheduleDetailModel.courseEndTime.length > 0) {
            self.courseEndTimeLabel.text = [[NSString alloc] initWithFormat:@"%@ น.", scheduleDetailModel.courseEndTime];
        }
        else {
            self.courseEndTimeLabel.text = @"n/a";
        }
        
        if(scheduleDetailModel.clockInTime != nil && scheduleDetailModel.clockInTime.length > 0) {
            self.clockInTimeLabel.text = [[NSString alloc] initWithFormat:@"%@ น.", scheduleDetailModel.clockInTime];
        }
        else {
            self.clockInTimeLabel.text = @"-";
        }
        

        
        switch (scheduleDetailModel.clockInStatus) {
            case 0: self.clockInTimeStatusLabel.text = @"เข้าเรียน"; break;
            case 1: self.clockInTimeStatusLabel.text = @"สาย"; break;
            case 3: self.clockInTimeStatusLabel.text = @"ขาด"; break;
            case 4: self.clockInTimeStatusLabel.text = @"ลากิจ"; break;
            case 5: self.clockInTimeStatusLabel.text = @"ลาป่วย"; break;
            case 6: self.clockInTimeStatusLabel.text = @"กิจกรรม"; break;
            case 7: self.clockInTimeStatusLabel.text = @"ลา"; break;
            case 8: self.clockInTimeStatusLabel.text = @"วันหยุด"; break;
            default: self.clockInTimeStatusLabel.text = @"n/a"; break;
        }
        
//        switch (scheduleDetailModel.clockOutStatus) {
//            case 0: self.clockOutTimeStatusLabel.text = @"เข้าเรียน"; break;
//            case 1: self.clockOutTimeStatusLabel.text = @"สาย"; break;
//            case 3: self.clockOutTimeStatusLabel.text = @"ขาด"; break;
//            case 7: self.clockOutTimeStatusLabel.text = @"ลา"; break;
//            case 8: self.clockOutTimeStatusLabel.text = @"วันหยุด"; break;
//            default: self.clockOutTimeStatusLabel.text = @"n/a"; break;
//        }
        
    }
    else {
        [self clearDisplay];
    }
}

- (void)clearDisplay {
    self.subjectLabel.text = @"วิชา ";
    self.subjectCodeLabel.text = @"รหัส ";
    self.roomLabel.text = @"ห้อง ";
    self.lecturerLabel.text = @"ผู้สอน ";
    
    self.courseStartTimeLabel.text = @"-";
    self.courseEndTimeLabel.text = @"-";
    
    self.clockInTimeLabel.text = @"-";
   // self.clockOutTimeLabel.text = @"n/a";
    self.clockInTimeStatusLabel.text = @"-";
   // self.clockOutTimeStatusLabel.text = @"n/a";
}
@end
