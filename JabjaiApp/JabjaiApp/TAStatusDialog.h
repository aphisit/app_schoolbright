//
//  TAStatusDialog.h
//  JabjaiApp
//
//  Created by mac on 7/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@class TAStatusDialog;

@protocol TAStatusDialogDelegate <NSObject>

- (void)taStatusDialog:(TAStatusDialog *)dialog onSelectStatus:(TA_SCAN_STATUS)scanStatus;

@end

@interface TAStatusDialog : UIViewController

@property (nonatomic, retain) id<TAStatusDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIStackView *dialogStackView;
@property (weak, nonatomic) IBOutlet UIButton *statusOnTimeButton;//เข้าเรียน
@property (weak, nonatomic) IBOutlet UIButton *statusLateButton;//สาย
@property (weak, nonatomic) IBOutlet UIButton *statusAbsenceButton;//ขาด
@property (weak, nonatomic) IBOutlet UIButton *statusEventButton;//กิจกรรม
@property (weak, nonatomic) IBOutlet UIButton *statusPersonalLeaveButton;//ลากิจ
@property (weak, nonatomic) IBOutlet UIButton *statusSickLeaveButton;//ลาป่วย
@property (weak, nonatomic) IBOutlet UIButton *noStatusButton;//ไม่ระบุสถานะ

- (IBAction)selectStatus:(id)sender;

- (void)showDialogInView:(UIView *)targetView mode:(NSInteger)mode;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
