//
//  CallReportRefillMoneyWeekAPI.h
//  JabjaiApp
//
//  Created by toffee on 10/17/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>




@class CallReportRefillMoneyWeekAPI;

@protocol CallReportRefillMoneyWeekAPIDelegate <NSObject>

@optional

- (void)callReportRefillMoneyWeekAPI:(CallReportRefillMoneyWeekAPI *)classObj dayArray:(NSArray *)dayArray totalMoneyArray:(NSArray *)totalMoneyArray success:(BOOL)success;
@end

@interface CallReportRefillMoneyWeekAPI : NSObject
@property (nonatomic, weak) id<CallReportRefillMoneyWeekAPIDelegate> delegate;

- (void)call:(long long)schoolId dayStart:(NSString*)dateStart dayEnd:(NSString*)dayEnd;
@end
