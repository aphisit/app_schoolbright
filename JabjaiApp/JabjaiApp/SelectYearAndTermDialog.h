//
//  SelectYearAndTermDialog.h
//  JabjaiApp
//
//  Created by mac on 4/17/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MKDropdownMenu/MKDropdownMenu.h>

#import "YearTermModel.h"

@protocol SelectYearAndTermDialogDelegate

- applyFilter:(NSNumber *)schoolYear semester:(NSString *)semester termID:(NSString *)termID;

@end

@interface SelectYearAndTermDialog : UIViewController <MKDropdownMenuDataSource, MKDropdownMenuDelegate>

@property (nonatomic, retain) id<SelectYearAndTermDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@property (weak, nonatomic) IBOutlet MKDropdownMenu *schoolYearDropdown;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *semesterDropdown;

@property (weak, nonatomic) IBOutlet UIButton *okButton;

- (IBAction)onPressOK:(id)sender;

- (id)initWithYearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict;
- (void)YearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
