//
//  CallTeacherTimeTableDetailAPI.m
//  JabjaiApp
//
//  Created by toffee on 23/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallTeacherTimeTableDetailAPI.h"
#import "Utils.h"
#import "APIURL.h"
@implementation CallTeacherTimeTableDetailAPI{
    NSInteger connectCounter;
    SScheduleDetailModel  *detailModel;
}
-(void)call:(long long)subjectId userId:(long long)userId date:(NSDate *)date schoolid:(long long)schoolid{
    [self getDetailOfSubject:subjectId userId:userId date:date schoolid:schoolid];
}

- (void) getDetailOfSubject:(long long)subjectId userId:(long long)userId date:(NSDate *)date schoolid:(long long)schoolid{
    
    NSString *URLString = [APIURL getTeacherScheduleListDetailURLWithUserID:userId date:date subjectID:subjectId schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDetailOfSubject:subjectId userId:userId date:date schoolid:schoolid];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDetailOfSubject:subjectId userId:userId date:date schoolid:schoolid];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDetailOfSubject:subjectId userId:userId date:date schoolid:schoolid];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long subjectID = [[dataDict objectForKey:@"ScheduleId"] longLongValue];
                    
                    NSMutableString *subjectName, *subjectCode, *roomNumber, *lecturer, *startTime, *endTime, *clockInTime, *clockOutTime, *classRoom;
                    NSInteger clockInStatus, clockOutStatus;
                    
                    if(![[dataDict objectForKey:@"ScheduleName"] isKindOfClass:[NSNull class]]) {
                        subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScheduleName"]];
                    }
                    else {
                        subjectName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"ScheduleCode"] isKindOfClass:[NSNull class]]) {
                        subjectCode = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScheduleCode"]];
                    }
                    else {
                        subjectCode = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"Room"] isKindOfClass:[NSNull class]]) {
                        roomNumber = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Room"]];
                    }
                    else {
                        roomNumber = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"classname"] isKindOfClass:[NSNull class]]) {
                        classRoom = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"classname"]];
                    }
                    else {
                        classRoom = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TeacherName"] isKindOfClass:[NSNull class]]) {
                        lecturer = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TeacherName"]];
                    }
                    else {
                        lecturer = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TimeIn"] isKindOfClass:[NSNull class]]) {
                        startTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeIn"]];
                    }
                    else {
                        startTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TimeOut"] isKindOfClass:[NSNull class]]) {
                        endTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeOut"]];
                    }
                    else {
                        endTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"ScanIn"] isKindOfClass:[NSNull class]]) {
                        clockInTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScanIn"]];
                    }
                    else {
                        clockInTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"ScanOut"] isKindOfClass:[NSNull class]]) {
                        clockOutTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScanOut"]];
                    }
                    else {
                        clockOutTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"StatusIn"] isKindOfClass:[NSNull class]]) {
                        if (![[dataDict objectForKey:@"StatusIn"] isEqualToString:@""]) {
                            clockInStatus = [[dataDict objectForKey:@"StatusIn"] integerValue];
                        }else{
                            clockInStatus = 3;
                        }
                       
                    }
                    else {
                        clockInStatus = 3;
                    }
                    
                    if(![[dataDict objectForKey:@"StatusOut"] isKindOfClass:[NSNull class]]) {
                        clockOutStatus = [[dataDict objectForKey:@"StatusOut"] integerValue];
                    }
                    else {
                        clockOutStatus = 3;
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectCode);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) roomNumber);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) classRoom);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lecturer);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) clockInTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) clockOutTime);
                    
                    SScheduleDetailModel *model = [[SScheduleDetailModel alloc] init];
                    model.subjectID = subjectID;
                    model.subjectName = subjectName;
                    model.subjectCode = subjectCode;
                    model.roomNumber = roomNumber;
                    model.lecturer = lecturer;
                    model.courseStartTime = startTime;
                    model.courseEndTime = endTime;
                    model.clockInTime = clockInTime;
                    model.clockInStatus = clockInStatus;
                    model.clockOutTime = clockOutTime;
                    model.clockOutStatus = clockOutStatus;
                    model.classRoom = classRoom;
                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTeacherTimeTableDetailAPI:data:success:)]) {
                        [self.delegate callTeacherTimeTableDetailAPI:self data:model success:YES];
                    }
                   
                    
                }
                if(isFail) {
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTeacherTimeTableDetailAPI:data:success:)]) {
                        [self.delegate callTeacherTimeTableDetailAPI:self data:nil success:YES];
                    }
                }
               
            }
        }
        
    }];
}
@end
