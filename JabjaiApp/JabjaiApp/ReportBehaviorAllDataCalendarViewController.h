//
//  ReportBehaviorAllDataCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 4/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CFSCalendar.h"
#import "BSReportCalendarViewControllerDelegate.h"

#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "BSStatusModel.h"

#import "BSHistoryModel.h"
#import "CallGetBSHistoryInDateRangeAPI.h"

#import "SlideMenuController.h"

@interface ReportBehaviorAllDataCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, CFSCalendarDelegateAppearance, UITableViewDataSource, UITableViewDelegate ,CallGetBSHistoryInDateRangeAPIDelegate, SlideMenuControllerDelegate>

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<BSStatusModel *> *statusArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedStatusIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger statusId;

@property (nonatomic) NSInteger statusType;

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;

@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)moveBack:(id)sender;
- (IBAction)openDetail:(id)sender;

@end
