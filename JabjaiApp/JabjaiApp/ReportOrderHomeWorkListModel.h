//
//  ReportOrderHomeWorkListModel.h
//  JabjaiApp
//
//  Created by mac on 4/30/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportOrderHomeWorkListModel : NSObject

@property (nonatomic) long long homeworkID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imageNewsIcon;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSDate *dateSend;
@property (nonatomic, strong) NSDate *dateAdd;

@property (nonatomic) int numberFile;
@property (nonatomic) BOOL file;

@end
