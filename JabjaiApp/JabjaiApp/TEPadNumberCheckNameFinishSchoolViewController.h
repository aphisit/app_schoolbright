//
//  TEPadNumberCheckNameFinishSchoolViewController.h
//  JabjaiApp
//
//  Created by toffee on 30/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallTEGetDataStudentScanBarcodeAPI.h"
#import "AlertDialogConfirm.h"
#import "AlertDialog.h"
#import "TEDataStudentScanerModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TEPadNumberCheckNameFinishSchoolDelegate <NSObject>

- (void)replyIdStudent:(TEDataStudentScanerModel*) data studentCode:(NSString*)studentCode mode:(int)mode success:(BOOL)success;
- (void) responseAleatMessage:(NSString*)message mode:(int)mode;


@end

@interface TEPadNumberCheckNameFinishSchoolViewController : UIViewController<CallTEGetDataStudentScanBarcodeAPIDelegate>

@property (retain, nonatomic) id<TEPadNumberCheckNameFinishSchoolDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *headerFinishSchoolLabel;


@property (weak, nonatomic) IBOutlet UILabel *headerCheckFinishSchoolLabel;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UITextField *idStudentTextField;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (nonatomic, assign) int mode;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)nextAction:(id)sender;
- (IBAction)numberButton:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
