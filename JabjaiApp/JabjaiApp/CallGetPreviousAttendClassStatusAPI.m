//
//  CallGetPreviousAttendClassStatusAPI.m
//  JabjaiApp
//
//  Created by mac on 7/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetPreviousAttendClassStatusAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetPreviousAttendClassStatusAPI () {
    NSInteger connectCounter;
    NSMutableArray<TAStudentStatusModel *> *studentStatusArray;
}

@end

@implementation CallGetPreviousAttendClassStatusAPI

- (void)call:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId {
    connectCounter = 0;
    [self getPreviousAttendClassStatus:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId];
}

#pragma mark - Get API Data
- (void)getPreviousAttendClassStatus:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId {
    
    NSString *URLString = [APIURL getPreviousAttendClassStatusWithSchoolId:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getPreviousAttendClassStatus:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getPreviousAttendClassStatus:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if([returnedData isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDict = returnedData;
                
                if([dataDict objectForKey:@"Status"] != nil && [[dataDict objectForKey:@"Status"] integerValue] == -1) {
                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onNoPreviousAttendClassStatus:)]) {
                        [self.delegate onNoPreviousAttendClassStatus:self];
                    }
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getPreviousAttendClassStatus:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(studentStatusArray != nil) {
                    [studentStatusArray removeAllObjects];
                    studentStatusArray = nil;
                }
                
                studentStatusArray = [[NSMutableArray alloc] init];
                
                if(returnedArray.count > 0) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    if(![[dataDict objectForKey:@"Student"] isKindOfClass:[NSNull class]] && [dataDict objectForKey:@"Student"] != nil) {
                        
                        NSArray *studentArray = [dataDict objectForKey:@"Student"];
                        
                        for(int i=0; i<studentArray.count; i++) {
                            NSDictionary *studentDict = [studentArray objectAtIndex:i];
                            
                            long long userId = [[studentDict objectForKey:@"UserId"] longLongValue];
                            BOOL authorized = [[studentDict objectForKey:@"authorized"] boolValue];
                            
                            NSMutableString *studentId, *studentName, *teacherName;
                            long long teacherId = 0;
                            
                            if(![[studentDict objectForKey:@"studentId"] isKindOfClass:[NSNull class]]) {
                                studentId = [[NSMutableString alloc] initWithFormat:@"%@", [studentDict objectForKey:@"studentId"]];
                            }
                            else {
                                studentId = [[NSMutableString alloc] initWithString:@""];
                            }
                            
                            if(![[studentDict objectForKey:@"studentName"] isKindOfClass:[NSNull class]]) {
                                studentName = [[NSMutableString alloc] initWithFormat:@"%@", [studentDict objectForKey:@"studentName"]];
                            }
                            else {
                                studentName = [[NSMutableString alloc] initWithString:@""];
                            }
                            
                            if(![[studentDict objectForKey:@"teachername"] isKindOfClass:[NSNull class]]) {
                                teacherName = [[NSMutableString alloc] initWithFormat:@"%@", [studentDict objectForKey:@"teachername"]];
                            }
                            else {
                                teacherName = [[NSMutableString alloc] initWithString:@""];
                            }
                            
                            if(![[studentDict objectForKey:@"teacherId"] isKindOfClass:[NSNull class]]) {
                                teacherId = [[studentDict objectForKey:@"teacherId"] longLongValue];
                            }
                            
                            int scanStatus = -1;
                            
                            if(![[studentDict objectForKey:@"scanstatus"] isKindOfClass:[NSNull class]]) {
                                int statusCode = [[studentDict objectForKey:@"scanstatus"] intValue];
                                
                                switch (statusCode) {
                                    case 0:
                                        scanStatus = 0; // ontime
                                        break;
                                    case 1:
                                        scanStatus = 1; // late
                                        break;
                                    case 3:
                                        scanStatus = 3; // absence
                                        break;
                                    case 4:
                                        scanStatus = 4; // personal leave
                                        break;
                                    case 5:
                                        scanStatus = 5; // sick leave
                                        break;
                                    case 6:
                                        scanStatus = 6; // event
                                        break;
                                    default:
                                        scanStatus = -1; // not scan
                                        break;
                                }
                            }
                            
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) studentId);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) studentName);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) teacherName);
                            
                            TAStudentStatusModel *model = [[TAStudentStatusModel alloc] init];
                            [model setUserId:userId];
                            [model setScanStatus:scanStatus];
                            [model setStudentId:studentId];
                            [model setStudentName:studentName];
                            [model setTeacherId:teacherId];
                            [model setCheckedTeacherName:teacherName];
                            [model setAuthorized:authorized];
                            
                            [studentStatusArray addObject:model];
                            
                        }
                    }
                }
                
                if(self.delegate && [self.delegate respondsToSelector:@selector(callGetPreviousAttendClassStatusAPI:data:success:)]) {
                    [self.delegate callGetPreviousAttendClassStatusAPI:self data:studentStatusArray success:YES];
                }
                
            }
        }
        
        if(isFail) {
            if(self.delegate && [self.delegate respondsToSelector:@selector(callGetPreviousAttendClassStatusAPI:data:success:)]) {
                [self.delegate callGetPreviousAttendClassStatusAPI:self data:nil success:NO];
            }
        }
    }];
}


@end
