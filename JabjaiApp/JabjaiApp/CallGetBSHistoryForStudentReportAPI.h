//
//  CallGetBSHistoryForStudentReportAPI.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSHistoryModel.h"

@class CallGetBSHistoryForStudentReportAPI;

@protocol CallGetBSHistoryForStudentReportAPIDelegate <NSObject>

- (void)callGetBSHistoryForStudentReportAPI:(CallGetBSHistoryForStudentReportAPI *)classObj data:(NSArray<BSHistoryModel *> *)data remainingScore:(double)remainingScore success:(BOOL)success;

@end

@interface CallGetBSHistoryForStudentReportAPI : NSObject

@property (nonatomic, weak) id<CallGetBSHistoryForStudentReportAPIDelegate> delegate;

- (void)call:(long long)userId schoolid:(long long)schoolid;

@end
