//
//  SNPickerTimeViewController.h
//  JabjaiApp
//
//  Created by toffee on 25/7/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol SNPickerTimeDelegate <NSObject>

@optional
-(void)pickerDialogPressOK:(NSDate*)time;
-(void)pickerDialogPressCancel;


@end
@interface SNPickerTimeViewController : UIViewController
@property (nonatomic, retain) id<SNPickerTimeDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIDatePicker *timePicker;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

- (IBAction)confirmAction:(id)sender;
- (IBAction)cancelAction:(id)sender;

- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end

NS_ASSUME_NONNULL_END
