//
//  CallUHGetHistoryHealthAPI.m
//  JabjaiApp
//
//  Created by Mac on 1/7/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallUHGetHistoryHealthAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallUHGetHistoryHealthAPI (){
    NSInteger connectCounter;
    NSMutableArray<UserHealthHistoryModel*> *temperatureArray;
}

@end

@implementation CallUHGetHistoryHealthAPI
-(void)call:(long long)userId schoolId:(long long)schoolId date:(NSString *)date{
    [self getHistoryHealth:userId schoolId:schoolId date:date];
}
-(void)getHistoryHealth:(long long)userId schoolId:(long long)schoolId date:(NSString*)date{
    NSString *urlString = [APIURL getHealthHistory:userId schoolId:schoolId date:date];
    NSURL *url = [NSURL URLWithString:urlString];
    

        [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            BOOL isFail = NO;
            if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
                
                if(error != nil) {
                    NSLog(@"An error occured : %@" , [error localizedDescription]);
                }
                else if(data == nil) {
                    NSLog(@"Data is null");
                }
                else {
                    NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                    NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
                }
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getHistoryHealth:userId schoolId:schoolId date:date];
                    
                }
                else {
                    
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if(error != nil) {
                    NSLog(@"An error occured : %@", [error localizedDescription]);
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self getHistoryHealth:userId schoolId:schoolId date:date];
                    }
                    else {
                        isFail = YES;
                        connectCounter = 0;
                    }
                    
                }
                else if(![returnedData isKindOfClass:[NSArray class]]) {
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self getHistoryHealth:userId schoolId:schoolId date:date];
                    }
                    else {
                        isFail = YES;
                        connectCounter = 0;
                    }
                }
                else {
                    
                    NSArray *returnedArray = returnedData;
                    connectCounter = 0;
                    
                    if(temperatureArray != nil) {
                        [temperatureArray removeAllObjects];
                        temperatureArray = nil;
                    }
                    
                    temperatureArray = [[NSMutableArray alloc] init];
                    
                    if(returnedArray.count > 0) {
                        for (int i = 0; i < returnedArray.count; i++) {
                            NSDictionary *dict = [returnedArray objectAtIndex:i];
                            NSString *time,*date,*temperature;
                            
                            NSInteger number = [[dict objectForKey:@"No"] integerValue];
                            if(![[dict objectForKey:@"LogTime"] isKindOfClass:[NSNull class]]) {
                                time = [[NSMutableString alloc] initWithFormat:@"%@", [dict objectForKey:@"LogTime"]];
                            }
                            else {
                                time = [[NSMutableString alloc] initWithString:@""];
                            }
                            if(![[dict objectForKey:@"LogDate"] isKindOfClass:[NSNull class]]) {
                                date = [[NSMutableString alloc] initWithFormat:@"%@", [dict objectForKey:@"LogDate"]];
                            }
                            else {
                                date = [[NSMutableString alloc] initWithString:@""];
                            }
                            if(![[dict objectForKey:@"Temperature"] isKindOfClass:[NSNull class]]) {
                                temperature = [[NSMutableString alloc] initWithFormat:@"%@", [dict objectForKey:@"Temperature"]];
                            }
                            else {
                                temperature = [[NSMutableString alloc] initWithString:@""];
                            }
                            
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) time);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) date);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) temperature);
                            
                            UserHealthHistoryModel *model = [[UserHealthHistoryModel alloc] init];
                            
                            [model setNumber:number];
                            [model setTime:time];
                            [model setDate:date];
                            [model setTemPerature:temperature];
                            
                            
                            [temperatureArray addObject:model];
                            
                        }
                        
                    }
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callUHGetHistoryHealthAPI:sucess:)]) {
                        [self.delegate callUHGetHistoryHealthAPI:temperatureArray sucess:YES];
                    }

                }
            }
            
            if(isFail) {
               if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callUHGetHistoryHealthAPI:sucess:)]) {
                   [self.delegate callUHGetHistoryHealthAPI:temperatureArray sucess:NO];
               }
            }
        }];
}

@end
