//
//  RPCheckNameSubjectModel.m
//  JabjaiApp
//
//  Created by toffee on 23/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "RPCheckNameSubjectModel.h"

@implementation RPCheckNameSubjectModel

@synthesize studentId = _studentId;
@synthesize studentName = _studentName;
@synthesize status = _status;
@synthesize studentPic = _studentPic;

- (void)setStudentId:(NSString *)studentId {
    _studentId = studentId;
}

- (void)setStudentName:(NSString *)studentName {
    _studentName = studentName;
}

- (void)setStatus:(NSInteger)status {
    _status = status;
}

- (void)setStudentPic:(NSString *)studentPic {
    _studentPic = studentPic;
}

- (NSString *)getStudentId {
    return _studentId;
}

- (NSString *)getStudentName {
    return _studentName;
}

- (NSInteger)getStatus {
    return _status;
}
- (NSString *)getStudentPic {
    return _studentPic;
}
@end
