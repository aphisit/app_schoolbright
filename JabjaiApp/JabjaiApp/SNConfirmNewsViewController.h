//
//  SNConfirmNewsViewController.h
//  JabjaiApp
//
//  Created by toffee on 10/6/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellUpdateSendNewsPOSTAPI.h"
#import "SWRevealViewController.h"
#import "SNTimeOutImage.h"
#import "SNConfrimCollectionViewCell.h"
#import "SNIconFileCollectionViewCell.h"
#import "SlideMenuController.h"

//@protocol CallUpdateSendNewPOSTAPIDelegate <NSObject>
@protocol SNConfirmNewsViewControllerDelegate <NSObject>
@end
@interface SNConfirmNewsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,SlideMenuControllerDelegate>

@property (nonatomic) NSInteger sendType;
@property (nonatomic) NSInteger newsType;
@property (nonatomic) NSInteger sendGroup;
@property (nonatomic) NSInteger sendTimeType;
@property (nonatomic) NSString* newsDetail;
@property (nonatomic) NSString* setnewsDetail;
@property (nonatomic) NSMutableArray* level2id;
@property (nonatomic) NSMutableArray* studentid;
@property (nonatomic) NSMutableArray* employeesid;
//@property (nonatomic) NSArray* imagesArray;
@property (nonatomic) NSMutableArray* fileArray;
@property (nonatomic) NSMutableArray* userAcceptArray;
@property (nonatomic) NSString* sendDay;
@property (nonatomic) NSDate* dateSend;
@property (nonatomic) NSString* timesend;
@property (nonatomic) NSString* time;
@property (nonatomic) NSString* scheduledTime;
@property (nonatomic) NSMutableArray* imageNews;
@property (nonatomic, weak) id<SNConfirmNewsViewControllerDelegate> delegate;
@property (strong, nonatomic) CellUpdateSendNewsPOSTAPI *cellUpdateSendNewsPOSTAPI;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerSenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRecipientLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSendDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDesciptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachFileLabel;

@property (weak, nonatomic) IBOutlet UILabel *sendTimeTypes;
@property (weak, nonatomic) IBOutlet UILabel *daySends;
@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet UILabel *userAccept;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionview;
@property (weak, nonatomic) IBOutlet UICollectionView *fileCollectionView;






- (IBAction)moveBack:(id)sender;

- (IBAction)confirmAction:(id)sender;

@end
