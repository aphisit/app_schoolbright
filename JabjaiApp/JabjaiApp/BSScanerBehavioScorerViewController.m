//
//  BSScanerBehavioScorerViewController.m
//  JabjaiApp
//
//  Created by toffee on 24/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "BSScanerBehavioScorerViewController.h"
#import "UserData.h"
@interface BSScanerBehavioScorerViewController (){
    AlertDialogConfirm *alertDialog;
    AlertDialog *alertDialogNoUser;
    NSString *idStudentStr ;
}
@property (strong, nonatomic) CallBSGetDataStudentScanBehaviorAPI *callBSGetDataStudentScanBehaviorAPI;
@property (strong, nonatomic) BSScanerStudentDataDialog *dialogConfirmScaner;
@end

@implementation BSScanerBehavioScorerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _captureSession = nil;
    [self setLanguage];
    [self startReading];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer *gradientHeaderView = [Utils getGradientColorHeader];
    gradientHeaderView.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeaderView atIndex:0];
}

-(void)setLanguage{
    if(_mode == BS_MODE_ADD) {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_ADD_SCAN_QRCODE",nil,[Utils getLanguage],nil);
    }
    else{
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REDUCE_SCAN_QRCODE",nil,[Utils getLanguage],nil);
    }
}

- (BOOL)startReading{
    NSError *error;
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (captureDevice.position == AVCaptureDevicePositionBack) {
        NSLog(@"back camera");
    }
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (!input)
    {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession addInput:input];
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObjects:AVMetadataObjectTypeQRCode,AVMetadataObjectTypeUPCECode,AVMetadataObjectTypeCode39Code,AVMetadataObjectTypeCode39Mod43Code,AVMetadataObjectTypeEAN13Code,AVMetadataObjectTypeEAN8Code,AVMetadataObjectTypeCode93Code,AVMetadataObjectTypeCode128Code,AVMetadataObjectTypePDF417Code,AVMetadataObjectTypeAztecCode,AVMetadataObjectTypeInterleaved2of5Code,AVMetadataObjectTypeITF14Code,AVMetadataObjectTypeDataMatrixCode, nil]];
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.viewforCamera layoutIfNeeded];
    [_videoPreviewLayer setFrame:_viewforCamera.layer.bounds];
    [_viewforCamera.layer addSublayer:_videoPreviewLayer];
    [_captureSession startRunning];
    return YES;
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode] || [[metadataObj type] isEqualToString:AVMetadataObjectTypeCode128Code] || [[metadataObj type] isEqualToString:AVMetadataObjectTypeCode39Code]) {
           // [_textView performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            idStudentStr = [metadataObj stringValue];
            NSLog(@"xxx = %@",[metadataObj stringValue]);
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            
        }
    }
    
}

-(void)stopReading{
    [self doCallBSGetDataStudentScanBarcodeAPI:[UserData getSchoolId] idStudent:idStudentStr];
    [_captureSession stopRunning];
    _captureSession = nil;
    //[_videoPreviewLayer removeFromSuperlayer];
}

//call get data student
- (void)doCallBSGetDataStudentScanBarcodeAPI:(long long)schoolId idStudent:(NSString *)idStudent {
    //[self showIndicator];
    if(self.callBSGetDataStudentScanBehaviorAPI != nil) {
        self.callBSGetDataStudentScanBehaviorAPI = nil;
    }
    self.callBSGetDataStudentScanBehaviorAPI = [[CallBSGetDataStudentScanBehaviorAPI alloc] init];
    self.callBSGetDataStudentScanBehaviorAPI.delegate = self;
    [self.callBSGetDataStudentScanBehaviorAPI call:schoolId idStudent:idStudent];
}

- (void)callBSGetDataStudentScanBarcodeAPI:(CallBSGetDataStudentScanBehaviorAPI *)classObj data:(BSDataStudentScanerModel *)data resCode:(NSInteger)resCode success:(BOOL)success{
    //[self stopIndicator];
    if (success && resCode == 200) {
        [self showAlertDialogConfirmScaner:data];
    }else{
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_USER_NOTFOUND",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
    }
}

#pragma mark - BSScanerStudentDataDialog
//show dialog confirmScaner
- (void)showAlertDialogConfirmScaner:(BSDataStudentScanerModel *)data {
    if(self.dialogConfirmScaner != nil && [self.dialogConfirmScaner isDialogShowing]) {
        [self.dialogConfirmScaner dismissDialog];
    }
    else {
        self.dialogConfirmScaner = [[BSScanerStudentDataDialog alloc] init];
        self.dialogConfirmScaner.delegate = self;
    }
        [self.dialogConfirmScaner showDialogInView:self.view data:data mode:self.mode];// 1 Add score /  2 Cut score
}
- (void) confirmResponse:(BSDataStudentScanerModel *)userData{//revert Object
    BSSelectedStudent *model = [[BSSelectedStudent alloc] init];
    NSString *studentName = [NSString stringWithFormat:@"%@ %@",[userData getStudentFirstName], [userData getStudentLastName]];
    [model setStudentName:studentName];
    [model setPic:[userData getStudentPicture]];
    [model setStudentId:[userData getStudentID]];
    [model setSelected:YES];
    NSArray<BSSelectedStudent*> *shoppingList = @[model];
    BSSelectBehaviorScoreViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectBehaviorScoreStoryboard"];
        viewController.status  = self.status;
        viewController.mode = self.mode;
        viewController.selectedStudentArray = shoppingList;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    if(alertDialogNoUser == nil) {
        alertDialogNoUser = [[AlertDialog alloc] init];
        alertDialogNoUser.delegate = self;
    }
    if(alertDialogNoUser != nil && [alertDialogNoUser isDialogShowing]) {
        [alertDialogNoUser dismissDialog];
    }
    [alertDialogNoUser showDialogInView:self.view title:title message:message];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)cancelResponse{
    [self performSelectorOnMainThread:@selector(startReading) withObject:nil waitUntilDone:NO];
}
- (void)onAlertDialogClose{
    [self performSelectorOnMainThread:@selector(startReading) withObject:nil waitUntilDone:NO];
}

- (IBAction)moveAction:(id)sender {
    BSSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectClassLevelStoryboard"];
    viewController.mode = self.mode;
    viewController.status = self.status;
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.selectedStudentArray = _selectedStudentArray;
    viewController.behaviorScoreArray = _behaviorScoreArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

@end
