//
//  UINoteInNotificationTableViewCell.m
//  JabjaiApp
//
//  Created by Mac on 1/9/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "UINoteInNotificationTableViewCell.h"
#import "Utils.h"
@implementation UINoteInNotificationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.detailLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TITLEHEADER_NOTIFICATION",nil,[Utils getLanguage],nil);
    NSLog(@"H = %.2f",self.image.frame.size.height);
    NSLog(@"W = %.2f",self.image.frame.size.width);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
