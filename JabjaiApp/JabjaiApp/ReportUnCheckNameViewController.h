//
//  ReportUnCheckNameViewController.h
//  JabjaiApp
//
//  Created by toffee on 20/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//
@import ARSlidingPanel;
#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CFSCalendar.h"
#import "SlideMenuController.h"
#import "ReportUnCheckListDataTableViewCell.h"
#import "ReportUnCheckListDataModel.h"
#import "ReportListTeacherViewController.h"
#import "CallReportUnCheckAPI.h"
#import "CallReportUnCheckListDataAPI.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
NS_ASSUME_NONNULL_BEGIN

@interface ReportUnCheckNameViewController : UIViewController <CFSCalendarDelegate, CFSCalendarDataSource, CFSCalendarDelegateAppearance, UITableViewDelegate, UITableViewDataSource, SlideMenuControllerDelegate, CallReportUnCheckAPIDelegate, CallReportUnCheckListDataAPIDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>
@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeadMenuConstraint;

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;



- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailConstraint;


- (IBAction)openDetail:(id)sender;

- (IBAction)openDrawer:(id)sender;

@end

NS_ASSUME_NONNULL_END
