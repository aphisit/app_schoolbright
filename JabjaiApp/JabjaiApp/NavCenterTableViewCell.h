//
//  NavCenterTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 4/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavCenterTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *navIcon;
@property (weak, nonatomic) IBOutlet UILabel *navLabel;

@end
