//
//  CallReportInOutAllDataSelectDepartmantAPI.h
//  JabjaiApp
//
//  Created by toffee on 15/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReportInOutAllDataSelectDepartmantModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallReportInOutAllDataSelectDepartmantAPI;

@protocol CallReportInOutAllDataSelectDepartmantAPIDelegate <NSObject>
@optional

- (void)callReportInOutAllDataSelectDepartmantAPI:(CallReportInOutAllDataSelectDepartmantAPI *)classObj dataDepaermantArray:(NSMutableArray<ReportInOutAllDataSelectDepartmantModel *>*)dataDepaermantArray success:(BOOL)success;
@end

@interface CallReportInOutAllDataSelectDepartmantAPI : NSObject
@property (nonatomic, weak) id<CallReportInOutAllDataSelectDepartmantAPIDelegate> delegate;
- (void)call:(long long)schoolId date:(NSString*)date;
@end

NS_ASSUME_NONNULL_END
