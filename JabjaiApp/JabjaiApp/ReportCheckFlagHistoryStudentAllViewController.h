//
//  ReportCheckFlagHistoryStudentAllViewController.h
//  JabjaiApp
//
//  Created by toffee on 30/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportCheckFlagStatusCollectionViewCell.h"
#import "SlideMenuController.h"
#import "ReportCheckFlagPoleCalendarViewController.h"
#import "ReportCheckFlagHistoryStudentAllTableViewCell.h"
#import "RPCheckFlagDateTimeModel.h"
#import "RPCheckNameFlagAttendanceModel.h"
#import "ReportCheckFlagDateTimeOfStudentViewController.h"
#import "CallCheckFlagDateTimeOfStudentAPI.h"
NS_ASSUME_NONNULL_BEGIN

@interface ReportCheckFlagHistoryStudentAllViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate,SlideMenuControllerDelegate, CallCheckFlagDateTimeOfStudentAPIDelegate>
@property (nonatomic) NSArray *studentStatusAbsenArray;
@property (nonatomic) NSArray *studentStatusOnTimeArray;
@property (nonatomic) NSArray *studentStatusLateArray;
@property (nonatomic) NSArray *studentStatusPersonalArray;
@property (nonatomic) NSArray *studentStatusSickArray;
@property (nonatomic) NSArray *studentStatusEventArray;
@property (nonatomic) NSArray *studentStatusAllArray;
@property (nonatomic) NSArray *studentStatusUndefinedArray;
@property (nonatomic) NSArray *studentAllArray;
@property (nonatomic) NSString *nameClassRoom;
@property (nonatomic) long long selectedClassroomId;
@property (nonatomic) NSDate* consideredDate;
@property (nonatomic) NSInteger studentSum;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UILabel *showNoStudent;

- (IBAction)actionMoveBack:(id)sender;



@end

NS_ASSUME_NONNULL_END
