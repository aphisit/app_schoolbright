//
//  TeachingSubjectReportMainViewController.h
//  JabjaiApp
//
//  Created by mac on 12/30/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"

@interface TeachingSubjectReportMainViewController : UIViewController <CAPSPageMenuDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (nonatomic) int sectionID;
@property (strong, nonatomic) NSString *sectionName;
@property (strong, nonatomic) NSString *subjectName;

- (IBAction)moveBack:(id)sender;

@end
