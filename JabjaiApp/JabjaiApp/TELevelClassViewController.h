//
//  TELevelClassViewController.h
//  JabjaiApp
//
//  Created by toffee on 10/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfoDBHelper.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "TableListDialog.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "SWRevealViewController.h"
#import "TAStudentStatusModel.h"
#import "TAStudentStatusTableViewCell.h"
#import "CallTEGetStudentInClassAPI.h"
#import "SlideMenuController.h"
#import "TECheckLateWithIdStudentViewController.h"
#import "TECheckLateScanerQrCodeViewController.h"
#import "TECheckOnTimeScanerQrCodeViewController.h"
#import "CallGetMenuListAPI.h"//check authorize menu
#import "AlertDialog.h"
#import "UnAuthorizeMenuDialog.h"
#import "JHReadFileViewController.h"
#import "LoginViewController.h"
#import "CRClosedForRenovationDialog.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
static NSInteger TE_MODE_CHECK = 1;
static NSInteger TE_MODE_EDIT = 2;

@interface TELevelClassViewController : UIViewController<UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate,TAStudentStatusTableViewCellDelegate, SlideMenuControllerDelegate, CallTEGetStudentInClassAPIDelegate,CallGetMenuListAPIDelegate,UnAuthorizeMenuDialogDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSString* scanStatus;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger mode;
@property (strong, nonatomic) NSArray<TAStudentStatusModel *> *selectedStudentArray;

@property (weak, nonatomic) IBOutlet UILabel *classLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *classRoomLabel;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classRoomTextField;
@property (weak, nonatomic) IBOutlet UILabel *haedText;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIImageView *menuImage;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIView *section2;

@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
- (IBAction)actionNext:(id)sender;
- (IBAction)openDrawer:(id)sender;

@end
