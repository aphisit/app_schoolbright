//
//  AttendToSchoolModel.h
//  JabjaiApp
//
//  Created by mac on 11/18/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AttendToSchoolModel : NSObject

@property (nonatomic, strong) NSNumber *status; // 0 : INTIME, 1 : LATE, 3 : ABSENCE
@property (nonatomic, strong) NSNumber *statusOut;
@property (nonatomic, strong) NSDate *timeIN;
@property (nonatomic, strong) NSDate *timeOut;
@property (nonatomic, strong) NSDate *scanDate;

@end
