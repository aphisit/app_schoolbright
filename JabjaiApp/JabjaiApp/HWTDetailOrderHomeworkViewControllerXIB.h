//
//  HWTDetailOrderHomeworkViewControllerXIB.h
//  JabjaiApp
//
//  Created by Mac on 9/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "CAPSPageMenu.h"
#import "KxMenu.h"
#import "Utils.h"
#import "ReportOrderHomeWorkDetailViewController.h"
//#import "ReportOrderHomeworkDetailPersonViewController.h"
//#import "ReportOrderHomeWorkRecieverViewController.h"
#import "ReportOrderHomeWorkViewController.h"
#import "HWTReciverViewControllerXIB.h"
#import "HWTDetailPersonViewControllerXIB.h"
NS_ASSUME_NONNULL_BEGIN
@protocol HWTDetailOrderHomeworkViewControllerXIBDelegate <NSObject>

- (void) closePage;

@end
@interface HWTDetailOrderHomeworkViewControllerXIB : UIViewController
@property (nonatomic, retain) id<HWTDetailOrderHomeworkViewControllerXIBDelegate> delegate;
@property (assign, nonatomic) long long homeworkID;
@property (nonatomic, strong) NSString *sortData;

@property (nonatomic, strong) HWTDetailPersonViewControllerXIB *detailController;
@property (nonatomic, strong) HWTReciverViewControllerXIB *recieverController;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
- (IBAction)moveBack:(id)sender;
- (void)dismissDetailNewsViewController;
- (BOOL)isDialogShowing;
- (void) showDetailHomeworkController:(UIView*)targetView homeworkID:(long long)homeworkID sortData:(NSString*)sortData;
@end

NS_ASSUME_NONNULL_END
