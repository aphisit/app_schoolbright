//
//  ARGradeTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 21/2/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ARGradeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *courseCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *gradeLabel;

@end

NS_ASSUME_NONNULL_END
