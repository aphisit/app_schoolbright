//
//  CallGMMenuAPI.m
//  JabjaiApp
//
//  Created by toffee on 4/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallGMMenuAPI.h"
#import "APIURL.h"
#import "Utils.h"

@implementation CallGMMenuAPI{
    NSInteger connectCounter;
    NSMutableArray<MenuListModel *> *selectedMenuArray;
}

-(void)call:(long long)schoolId shopId:(long long)shopId{
    connectCounter = 0;
    [self getListMarget:schoolId shopId:shopId ];
}

- (void)getListMarget:(long long)schoolId shopId:(long long)shopId  {
    NSString *URLString = [APIURL getListMenuFood:schoolId shopId:shopId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getListMarget:schoolId shopId:shopId];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                     [self getListMarget:schoolId shopId:shopId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                     [self getListMarget:schoolId shopId:shopId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;

                if(selectedMenuArray != nil) {
                    [selectedMenuArray removeAllObjects];
                    selectedMenuArray = nil;
                }
//                //
                selectedMenuArray = [[NSMutableArray alloc] init];
//
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
//                    //
                    long long productId = [[dataDict objectForKey:@"product_id"] longLongValue];
                    NSInteger price = [[dataDict objectForKey:@"price"] longLongValue];
//
                    NSMutableString *productName;
                    NSMutableString *picture;
                    NSMutableString *barcode;
//
                    if(![[dataDict objectForKey:@"product_name"] isKindOfClass:[NSNull class]]) {
                        productName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"product_name"]];
                    }
                    else {
                        productName = [[NSMutableString alloc] initWithString:@""];
                    }
//                    //
                    if(![[dataDict objectForKey:@"picture"] isKindOfClass:[NSNull class]]) {
                        picture = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"picture"]];
                    }
                    else {
                        picture = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"barcode"] isKindOfClass:[NSNull class]]) {
                        barcode = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"barcode"]];
                    }
                    else {
                        barcode = [[NSMutableString alloc] initWithString:@""];
                    }
//
//
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) productName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) picture);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) barcode);
                   
//
                    MenuListModel *model = [[MenuListModel alloc] init];
                    model.productId = productId;
                    model.productName = productName;
                    model.price = price;
                    model.barcode = barcode;
                    model.picture = picture;
//
//
                    [selectedMenuArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGMMenuAPI:data:success:)]) {
                    [self.delegate callGMMenuAPI:self data:selectedMenuArray success:YES];
                }
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGMMenuAPI:data:success:)]) {
                [self.delegate callGMMenuAPI:self data:selectedMenuArray success:NO];
            }
        }
        
        
        
        
    }
     ];
}
@end
