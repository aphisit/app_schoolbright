//
//  DateUtility.m
//  DateUtilityApp
//
//  Created by mac on 11/6/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "DateUtility.h"
#import "Utils.h"

@implementation DateUtility

+(NSDate *)minDate:(NSArray *)dates {
    
    if(dates == nil || dates.count == 0) {
        return nil;
    }
    
    NSDate *minDate = [dates objectAtIndex:0];
    
    for(int i=1; i<dates.count; i++) {
        if([minDate compare:[dates objectAtIndex:i]] == NSOrderedDescending) {
            minDate = [dates objectAtIndex:i];
        }
    }
    
    return minDate;
}

+(NSDate *)maxDate:(NSArray *)dates {
    
    if(dates == nil || dates.count == 0) {
        return nil;
    }
    
    NSDate *maxDate = [dates objectAtIndex:0];
    
    for(int i=1; i<dates.count; i++) {
        if([maxDate compare:[dates objectAtIndex:i]] == NSOrderedAscending) {
            maxDate = [dates objectAtIndex:i];
        }
    }
    
    return maxDate;
}

+(BOOL)sameDate:(NSDate *)date1 date2:(NSDate *)date2 {
    
    NSCalendar *gregorianCalendar = [Utils getGregorianCalendar];
    
    NSDateComponents *comps1 = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date1];
    
    NSDateComponents *comps2 = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date2];
    
    if(([comps1 year] == [comps2 year]) && ([comps1 month] == [comps2 month]) && ([comps1 day] == [comps2 day])) {
        return YES;
    }
    else {
        return NO;
    }
    
}

+(BOOL)sameMonthOfYear:(NSDate *)date1 date2:(NSDate *)date2 {
    
    NSCalendar *gregorianCalendar = [Utils getGregorianCalendar];
    
    NSDateComponents *comps1 = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date1];
    
    NSDateComponents *comps2 = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date2];
    
    if(([comps1 year] == [comps2 year]) && ([comps1 month] == [comps2 month])) {
        return YES;
    }
    else {
        return NO;
    }
}

+(BOOL)betweenDate:(NSDate *)date minDate:(NSDate *)minDate maxDate:(NSDate *)maxDate {
    
    NSDate *newDate = [self dateWithOutTime:date];
    NSDate *newMinDate = [self dateWithOutTime:minDate];
    NSDate *newMaxDate = [self dateWithOutTime:maxDate];
    
    BOOL condition1 = ([newDate compare:newMinDate] == NSOrderedSame) || ([newDate compare:newMinDate] == NSOrderedDescending);
    BOOL condition2 = ([newDate compare:newMaxDate] == NSOrderedSame) || ([newDate compare:newMaxDate] == NSOrderedAscending);
    BOOL isBetween = condition1 && condition2;
    
    return isBetween;
}

+(NSDate *)startDateOfMonthAndYearInRangeWithMonth:(NSInteger)month year:(NSInteger)year minDate:(NSDate *)minDate maxDate:(NSDate *)maxDate {
    
    NSCalendar *gregorianCalendar = [Utils getGregorianCalendar];
    
    NSDateComponents *minDateComponents = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:minDate];
    NSDateComponents *maxDateComponents = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:maxDate];
    
    NSInteger minYear = [minDateComponents year];
    NSInteger maxYear = [maxDateComponents year];
    
    if(year < minYear || year > maxYear) {
        return nil;
    }
    
    NSDateComponents *startDateComps = [[NSDateComponents alloc] init];
    
    for(NSInteger y = maxYear; y >= minYear; y--) {
        [startDateComps setYear:y];
        [startDateComps setMonth:month];
        [startDateComps setDay:1];
        
        NSDate *startDate = [gregorianCalendar dateFromComponents:startDateComps];
        
        if([self betweenDate:startDate minDate:minDate maxDate:maxDate]) {
            return startDate;
        }
    }
    
    return minDate;
    
}

+(NSDate *)endDateOfMonthAndYearInRangeWithMonth:(NSInteger)month year:(NSInteger)year minDate:(NSDate *)minDate maxDate:(NSDate *)maxDate {
    
    NSCalendar *gregorianCalendar = [Utils getGregorianCalendar];
    NSDateComponents *minDateComponents = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:minDate];
    NSDateComponents *maxDateComponents = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:maxDate];
    
    NSInteger minYear = [minDateComponents year];
    NSInteger maxYear = [maxDateComponents year];
    
    if(year < minYear || year > maxYear) {
        return nil;
    }
    
    NSDateComponents *startDateComps = [[NSDateComponents alloc] init];
    NSDateComponents *endDateComps = [[NSDateComponents alloc] init];
    
    for(NSInteger y = maxYear; y >= minYear; y--) {
        
        [startDateComps setYear:y];
        [startDateComps setMonth:month];
        [startDateComps setDay:1];
        
        NSDate *startDate = [gregorianCalendar dateFromComponents:startDateComps];
        
        [endDateComps setYear:y];
        [endDateComps setMonth:month];
        [endDateComps setDay:[self numberOfDaysInMonthCount:startDate]];
        
        NSDate *endDate = [gregorianCalendar dateFromComponents:endDateComps];
        
        if([self betweenDate:endDate minDate:minDate maxDate:maxDate]) {
            return endDate;
        }
    }
    
    return maxDate;
}

+(NSInteger)numberOfDaysInMonthCount:(NSDate *)date {
    NSCalendar *gregorianCalendar = [Utils getGregorianCalendar];
    
    NSRange dayRange = [gregorianCalendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    
    return dayRange.length;
}

+(NSDate *)dateWithOutTime:(NSDate *)date {
    NSCalendar *gregorianCalendar = [Utils getGregorianCalendar];
    
    NSDateComponents *comps = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    
    NSDate *newDate = [gregorianCalendar dateFromComponents:comps];
    
    return newDate;
}

+ (NSDate *)firstDayOfMonth:(NSDate *)date {
    NSCalendar *gregorianCalendar = [Utils getGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];

    [components setDay:1];
    
    NSDate *firstDayDate = [gregorianCalendar dateFromComponents:components];
    
    return firstDayDate;
}

+ (NSDate *)lastDayOfMonth:(NSDate *)date {
    
    NSCalendar *gregorianCalendar = [Utils getGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    
    NSInteger lastDate = [self numberOfDaysInMonthCount:date];
    
    [components setDay:lastDate];
    
    NSDate *lastDayDate = [gregorianCalendar dateFromComponents:components];
    
    return lastDayDate;
}

@end
