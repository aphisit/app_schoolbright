//
//  UserHealthViewController.h
//  JabjaiApp
//
//  Created by mac on 1/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UserHealthViewController;
@protocol UserHealthViewControllerDelegate <NSObject>

-(void)responseUserHealthHitorysTemperatureViewController;

@end

@interface UserHealthViewController : UIViewController
@property (nonatomic, weak) id<UserHealthViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *genderHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *heightHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *bloodTypeHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *diseaseHeaderLabel;
@property (weak, nonatomic) IBOutlet UIButton *temperatureBtn;


@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *heightLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *bloodTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *diseaseLabel;

@property (weak, nonatomic) IBOutlet UIView *colorLevel;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
- (IBAction)nextAction:(id)sender;

@end
