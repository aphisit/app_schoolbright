//
//  MGListMargetViewController.m
//  JabjaiApp
//
//  Created by toffee on 4/10/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "MGListMargetViewController.h"
#import "UserData.h"
#import "MGListMenuViewController.h"
#import "UserInfoModel.h"
#import "Utils.h"
#import "UserInfoViewController.h"

@interface MGListMargetViewController (){
    NSBundle *myLangBundle;
    UnAuthorizeMenuDialog *unAuthorizeMenuDialog;
    NSMutableArray *authorizeSubMenuArray;
}
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (strong, nonatomic) CallGMListMarGetAPI *callGMListMarGetAPI;
@property (strong, nonatomic) UserInfoModel *userInfoModel;
@property (strong, nonatomic) CallGMGetUserInfoDataAPI *callGMGetUserInfoDataAPI;
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

static NSString *cellIdentifier = @"ListMargetCell";
@implementation MGListMargetViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    if ([UserData getUserType] != STUDENT) { [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//call to authorize menu
    }
   
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.showNoShopLabel.hidden = YES;
    self.monnyLabel.text = [NSString stringWithFormat:@"%d",_userInfoModel.money];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MargetListTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    [self setLanguage];
    [self getUserInfoData];
    [self getStudentInClassData];
    
    // Do any additional setup after loading the view.
}

- (void)viewDidLayoutSubviews{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(void)setLanguage{
    self.headerShoppingLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_SHOPPING", nil, myLangBundle, nil);
    self.headerBalanceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_BALANCE", nil, myLangBundle, nil);
    self.showNoShopLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_NOSHOP", nil, myLangBundle, nil);
}


- (void)getStudentInClassData {
    [self showIndicator];
    if(self.callGMListMarGetAPI != nil) {
        self.callGMListMarGetAPI = nil;
    }
    self.callGMListMarGetAPI = [[CallGMListMarGetAPI alloc] init];
    self.callGMListMarGetAPI.delegate = self;
    [self.callGMListMarGetAPI call:[UserData getSchoolId]] ;
}


- (void)callGMListMarGetAPI:(CallGMListMarGetAPI *)classObj data:(NSArray<MargetListModel *> *)data success:(BOOL)success {
    [self stopIndicator];
    if (data != nil || success == nil) {
        _margetArray = data;
       
        
        if (_margetArray.count == 0) {
            self.showNoShopLabel.hidden = NO;
        }else{
            self.showNoShopLabel.hidden = YES;
        }
        [self.tableView reloadData];
    }
}

- (void)getUserInfoData {
    
    if(self.callGMGetUserInfoDataAPI != nil) {
        self.callGMGetUserInfoDataAPI = nil;
    }
    self.callGMGetUserInfoDataAPI = [[CallGMGetUserInfoDataAPI alloc] init];
    self.callGMGetUserInfoDataAPI.delegate = self;
    [self.callGMGetUserInfoDataAPI getUserInfoData] ;
}

-(void)callGMGetUserInfoDataAPI:(CallGMGetUserInfoDataAPI *)classObj data:(UserInfoModel *)data success:(BOOL)success{
    
    UserInfoModel *model;
    model = data;
    if (data != nil) {
        
        NSString *display = [NSNumberFormatter localizedStringFromNumber:@(model.money)
                                                             numberStyle:NSNumberFormatterDecimalStyle];
        self.monnyLabel.text = [NSString stringWithFormat:@"%@ %@",display,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_B",nil,myLangBundle,nil)];
        
       // _monnyLabel.text = [NSString stringWithFormat:@"%.0f",model.money ];
    }
    
}

#pragma mark - CallGetMenuListAPIDelegate
///check authorize menu
- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
    [self showIndicator];
    // [self popUpLoaddin];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
    [self stopIndicator];
    
    if ((resCode == 300 || resCode == 200) && success ) {
        NSLog(@"xx");
        authorizeSubMenuArray = [[NSMutableArray alloc] init];
        if (data != NULL) {
            
            for (int i = 0;i < data.count; i++) {
                NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
                for (int j = 0; j < subMenuArray.count; j++) {
                    NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
                    [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
                }
            }
        }
        NSString* notPermissionText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_UNAUTHORRIZE",nil,[Utils getLanguage],nil);
        NSString* pleaseContactText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_PLEASECONTACT",nil,[Utils getLanguage],nil);
        NSString *alertMessage = [NSString stringWithFormat:@"%@\n%@",notPermissionText,pleaseContactText];
        
        if (![authorizeSubMenuArray containsObject: @"16"]) {
            [self showAlertDialogAuthorizeMenu:alertMessage];
            NSLog(@"ไม่มีสิทธิ์");
        }
    }else{
        
        self.dbHelper = [[UserInfoDBHelper alloc] init];
        [self.dbHelper deleteUserAllWithMasterId:[UserData getMasterUserID]];
        
        // Logout
        [UserData setUserLogin:NO];
        [UserData saveMasterUserID:0];
        [UserData saveUserID:0];
        [UserData setUserType:USERTYPENULL];
        [UserData setAcademyType:ACADEMYTYPENULL];
        [UserData setFirstName:@""];
        [UserData setLastName:@""];
        [UserData setUserImage:@""];
        [UserData setGender:MALE];
        [UserData setClientToken:@""];
        LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    
}

- (void)onAlertDialogClose {
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   
    if(_margetArray != nil) {
        
        return _margetArray.count;
    }
    else {
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MargetListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    MargetListModel *model;
    
    [cell.sectionView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.sectionView.layer setShadowOpacity:0.3];
    [cell.sectionView.layer setShadowRadius:3.0];
    [cell.sectionView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    model = [_margetArray objectAtIndex:indexPath.row];
    cell.index = indexPath.row;
    cell.margetName.text = [model getShopName];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MargetListModel *model;
    model = [_margetArray objectAtIndex:indexPath.row];
    NSLog(@"nameMarget = %@",model.shopName);
    MGListMenuViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGListMenuViewStoryboard"];
    viewController.shopId = model.shopId;
    viewController.nameShop = model.shopName;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (void)showAlertDialogAuthorizeMenu:(NSString *)message {
    if(unAuthorizeMenuDialog != nil && [unAuthorizeMenuDialog isDialogShowing]) {
        [unAuthorizeMenuDialog dismissDialog];
    }
    else {
        unAuthorizeMenuDialog = [[UnAuthorizeMenuDialog alloc] init];
        unAuthorizeMenuDialog.delegate = self;
    }
    [unAuthorizeMenuDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}


#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)openDarwer:(id)sender {

    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
}
@end
