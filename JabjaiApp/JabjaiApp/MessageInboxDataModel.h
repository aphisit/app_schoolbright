//
//  MessageInboxDataModel.h
//  JabjaiApp
//
//  Created by mac on 11/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageInboxDataModel : NSObject

@property (nonatomic) long long messageID;
@property (nonatomic) long long userID;
@property (nonatomic) int messageType; //1->Attendance, 2->Purchasing, 3->Topup, 4->AbsenceRequest, 5->News, 6->Homework
@property (nonatomic) int status; //0 -> unread, 1 -> read
@property (nonatomic) int statusRead; //0 -> unread, 1 -> read
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSDate *date;

@property (nonatomic) BOOL file;

@end
