//
//  RPPurchasingTableViewDialog.h
//  JabjaiApp
//
//  Created by toffee on 11/6/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPPurchasingTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
@protocol  RPPurchasingTableViewDialogDelegate

//-(void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index;

@end
@interface RPPurchasingTableViewDialog : UIViewController <UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UILabel *headerOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerMenuLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerOkBtn;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTableViewConstraint;

@property (nonatomic, retain) id<RPPurchasingTableViewDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *dialog;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)confirmAction:(id)sender;


- (void)showDialogInView:(UIView *)targetView dataArr:(NSArray *)dataArr;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end

NS_ASSUME_NONNULL_END
