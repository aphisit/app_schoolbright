//
//  BSImageCollectionViewCell.m
//  JabjaiApp
//
//  Created by toffee on 10/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "BSImageCollectionViewCell.h"

@implementation BSImageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
  
}

- (IBAction)removeImageAction:(id)sender {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(removeImage:collectionView:)]) {
        [self.delegate removeImage:self.deleteBtn.tag collectionView:self.collectionView];
    }
}
@end
