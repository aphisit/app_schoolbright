//
//  LoginListAllSchoolNameViewController.h
//  JabjaiApp
//
//  Created by toffee on 25/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallLoginListAllSchoolNameAPI.h"
#import "LoginListAllSchoolNameTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN
@protocol LoginListAllSchoolNameDialogDelegate <NSObject>
@optional
- (void)onAlertDialogClose:(LoginListAllSchoolNameModel*)listSchoolNameModel;
- (void)touchesBeganClose;
@end
@interface LoginListAllSchoolNameDialog : UIViewController<CallLoginListAllSchoolNameAPIDelegate, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate>
@property (retain, nonatomic) id<LoginListAllSchoolNameDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end

NS_ASSUME_NONNULL_END
