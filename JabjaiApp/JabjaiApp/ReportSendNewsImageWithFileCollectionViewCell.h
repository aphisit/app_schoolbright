//
//  ReportSendNewsImageWithFileCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 20/8/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportSendNewsImageWithFileCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *fileBtn;
@property (weak, nonatomic) IBOutlet UIImageView *fileimage;


@end

NS_ASSUME_NONNULL_END
