//
//  ReportHomeWorkMessageViewController.m
//  JabjaiApp
//
//  Created by mac on 1/4/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportHomeWorkMessageViewController.h"
#import "NewsMessageTableViewCell.h"
#import "ProgressTableViewCell.h"
#import "MessageInboxDataModel.h"

#import "ReportHomeworkMessageInboxModel.h"

#import "SWRevealViewController.h"
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"

#import "AppDelegate.h"


static NSString *dataCellIdentifier = @"MessageCell";

@interface ReportHomeWorkMessageViewController () {
    
    NSMutableDictionary<NSNumber *, NSArray<ReportHomeworkMessageInboxModel *> *> *messageSections;
    NSMutableArray<ReportHomeworkMessageInboxModel *> *messages;
    
    UIColor *unreadBGColor;
    UIColor *readBGColor;
    NSInteger connectCounter;
    
}

@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property (nonatomic, strong) JHDetailHomeWorkViewController *jHDetailHomeWorkViewController;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation ReportHomeWorkMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_HOMEWORK",nil,[Utils getLanguage],nil);
    
    messageSections = [[NSMutableDictionary alloc] init];
    messages = [[NSMutableArray alloc] init];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NewsMessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:dataCellIdentifier];
    
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    //[self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [self.timeFormatter setDateFormat:@"HH:mm"];
    }
    
    unreadBGColor = [UIColor whiteColor];
    readBGColor = [UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1.0];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    [self getHomeworkMessageDataWithPage:1];
    
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [self checkDeepLinkMessage];
}

- (void)checkDeepLinkMessage {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.deeplink != nil) {
        self.messageID = appDelegate.deeplink_message_id;
        [self getMessageWithID:self.messageID];
        // Clear deep link
        appDelegate.deeplink = nil;
        appDelegate.deeplink_message_id = -1;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return messageSections.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger page = section + 1;
    return [[messageSections objectForKey:@(page)] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
    
    NSInteger page = indexPath.section + 1;
    ReportHomeworkMessageInboxModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
    cell.titleLabel.text = model.title;
    cell.messageLabel.text = model.message;
    
    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
    cell.statementreadLabel.clipsToBounds = YES;
    cell.statementreadLabel.layer.shouldRasterize = YES;
    
    cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
    cell.schoolLogo.clipsToBounds = YES;
    cell.schoolLogo.layer.shouldRasterize = YES;
    cell.schoolLogo.image = [UIImage imageNamed:@"homework"];
    
    if (model.file == YES) {
        cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
    }
    else{
        cell.iconFile.image = nil;
    }
    
    if(model.status == 0) {
        // Unread
        cell.backgroundColor = unreadBGColor;
        cell.statementreadLabel.hidden = NO;
    }
    else {
        cell.backgroundColor = readBGColor;
        cell.statementreadLabel.hidden = YES;
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger page = indexPath.section + 1;
    ReportHomeworkMessageInboxModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
    if (model.status == 0) {
        [self updateReadStatus:model];
        model.status = 1;
    }
    
    if(self.jHDetailHomeWorkViewController == nil) {
        self.jHDetailHomeWorkViewController = [[JHDetailHomeWorkViewController alloc] init];
    }
    if(self.jHDetailHomeWorkViewController != nil && [self.jHDetailHomeWorkViewController isDialogShowing]) {
        [self.jHDetailHomeWorkViewController dismissDetailHomeWorkViewController];
    }
    [self.jHDetailHomeWorkViewController showDetailHomeWorkViewController:self.view homeworkID:model.messageID];
    
//    ReportHomeWorkDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportHomeWorkDetailStoryboard"];
//
//    viewController.homeworkID = model.homeworkID;
//    viewController.messageID = model.messageID;
//    viewController.subjectName = model.title;
//    viewController.detail = model.message;
//
//    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 120;
}

#pragma mark - Get API Data
- (void)getHomeworkMessageDataWithPage:(NSUInteger)page{
    
    [self showIndicator];
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getHomeworkMessageWithPage:page userID:userID schoolid:schoolid];
    
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getHomeworkMessageDataWithPage:page];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getHomeworkMessageDataWithPage:page];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getHomeworkMessageDataWithPage:page];
                }
                else {
                    connectCounter = 0;
                }
            }
            else{
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    int status = [[dataDict objectForKey:@"nStatus"] intValue];
                    int type = [[dataDict objectForKey:@"nType"] intValue];
                    NSMutableString *messageDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dSend"]];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sTitle"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    BOOL file = [[dataDict objectForKey:@"file"] boolValue];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    NSRange dotRange = [messageDateStr rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:messageDateStr];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:messageDateStr];
                    }
                    
                    ReportHomeworkMessageInboxModel *model = [[ReportHomeworkMessageInboxModel alloc] init];
                    model.messageID = messageID;
                    model.userID = (int)[UserData getUserID];
                    model.messageType = type;
                    model.status =status;
                    model.title = title;
                    model.message = message;
                    model.date = messageDate;
                    model.file = file;
                    
                    [newDataArr addObject:model];
                    
                }
                
                [messages removeAllObjects];
                messages = [[NSMutableArray alloc] initWithArray:newDataArr];
                [messageSections setObject:newDataArr forKey:@(page)];
                
                [self.tableView reloadData];
                
            }
            
            
        }
        
    }];
}

-(void)updateReadStatus:(ReportHomeworkMessageInboxModel *)model {
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:model.messageID userID:userID schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self updateReadStatus:model];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateReadStatus:model];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateReadStatus:model];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                if(returnedArray.count == 0) {
                    NSLog(@"%s", "Update read message status failed (return array size 0)");
                }
            }
        }
    }];
}

- (void)getMessageWithID:(long long)messageID {
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:messageID userID:userID schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getMessageWithID:messageID];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageWithID:messageID];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageWithID:messageID];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                ReportHomeworkMessageInboxModel *messageInboxDataModel = nil;
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    
                    long long homeworkID = [[dataDict objectForKey:@"homework_id"] longLongValue];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"planename"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"planename"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    

                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    
                    messageInboxDataModel = [[ReportHomeworkMessageInboxModel alloc] init];
                    messageInboxDataModel.homeworkID = homeworkID;
                    messageInboxDataModel.messageID = messageID;
                    messageInboxDataModel.userID = (int)[UserData getUserID];
                    messageInboxDataModel.title = title;
                    messageInboxDataModel.message = message;
                    
                    break;
                }
            
            }
            
        }
        
    }];
}


- (void)fetchMoreData {
    
    if(messages.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *lastRowIndexPath = [visiblePaths lastObject];
        
        // Check whether or not the very last row is visible.
        NSInteger numberOfSections = [self.tableView numberOfSections];
        NSInteger lastRowSection = [lastRowIndexPath section];
        NSInteger lastRowRow = [lastRowIndexPath row];
        NSInteger numberOfRowsInSection = [self.tableView numberOfRowsInSection:lastRowSection];
        
        if(lastRowSection == numberOfSections -1 && lastRowRow == numberOfRowsInSection - 1) {
            
            if(messages.count % 20 == 0) {
                NSInteger nextPage = lastRowSection + 2;
                [self getHomeworkMessageDataWithPage:nextPage];
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self fetchMoreData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self fetchMoreData];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)openDrawer:(id)sender {
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
}

- (IBAction)hideNotifications:(id)sender {
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportHomeWorkARSPContainerStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];

}
@end
