//
//  ReportPersonViewController.m
//  JabjaiApp
//
//  Created by mac on 4/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportPersonViewController.h"
#import "MenuReportTableViewCell.h"

#import "ReportInOutViewController.h"
#import "ReportLeaveViewController.h"
#import "ReportRefillMoneyViewController.h"
#import "ReportPurchasingViewController.h"
#import "NavigationViewController.h"
#import "Utils.h"
@interface ReportPersonViewController (){
    
    UIColor *grayColor;
    
}

@end

@implementation ReportPersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MenuReportTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    UIImage *ima;
    if(indexPath.row == 0) {
        //cell.iconReport.image = [UIImage imageNamed:@"in_out"];
        ima = [UIImage imageNamed:@"in_out"];
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_REPORT_WORKING",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 1) {
        //cell.iconReport.image = [UIImage imageNamed:@"leave_report"];
        ima = [UIImage imageNamed:@"leave_report"];
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_LEAVE_REPORT",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 2) {
        //cell.iconReport.image = [UIImage imageNamed:@"coin"];
        ima = [UIImage imageNamed:@"coin"];
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_TOPUP_REPORT_PERSONAL",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 3) {
        //cell.iconReport.image = [UIImage imageNamed:@"purchase"];
         ima = [UIImage imageNamed:@"purchase"];
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_PURCHASING_REPORT_PERSONAL",nil,[Utils getLanguage],nil);
    }
    
    cell.iconReport.image =[self imageWithImage:ima scaledToSize:CGSizeMake(cell.iconReport.frame.size.width,cell.iconReport.frame.size.height)];
    [cell.BackgroundView layoutIfNeeded];
    [cell.BackgroundView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.BackgroundView.layer setShadowOpacity:0.3];
    [cell.BackgroundView.layer setShadowRadius:3.0];
    [cell.BackgroundView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if( (indexPath.row == 0)) {
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportPersonViewController:numberPage:)]) {
            [self.delegate changePageReportPersonViewController:self numberPage:0];
        }
    }
    else if (indexPath.row == 1) { // Leave
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportPersonViewController:numberPage:)]) {
            [self.delegate changePageReportPersonViewController:self numberPage:1];
        }
    }
    else if (indexPath.row == 2) {
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportPersonViewController:numberPage:)]) {
            [self.delegate changePageReportPersonViewController:self numberPage:2];
        }
    }
    else if (indexPath.row == 3) { // Purchasing
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportPersonViewController:numberPage:)]) {
            [self.delegate changePageReportPersonViewController:self numberPage:3];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    // Here pass new size you need
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
