//
//  RPPurchasingTableViewDialog.m
//  JabjaiApp
//
//  Created by toffee on 11/6/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "RPPurchasingTableViewDialog.h"
#import "PurchasingListMenuModel.h"
#import "Utils.h"
@interface RPPurchasingTableViewDialog ()
@property (nonatomic) BOOL isShowing;
@property (nonatomic, strong) NSArray *dataArr;
@end
static CGFloat cellHeight;

static NSString *cellIdentifier = @"Cell";
@implementation RPPurchasingTableViewDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLanguage];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RPPurchasingTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.layer.masksToBounds = YES;
    
    self.dialog.layer.cornerRadius = 10;
    self.dialog.layer.masksToBounds = YES ;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        cellHeight = 50;  /* Device is iPad */
    }else{
        cellHeight = 30;
    }
    
    self.isShowing = NO;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidLayoutSubviews{
    //set color button next
       [self.headerOkBtn layoutIfNeeded];
       CAGradientLayer *gradientNext = [Utils getGradientColorNextAtion];
       gradientNext.frame = self.headerOkBtn.bounds;
       [self.headerOkBtn.layer insertSublayer:gradientNext atIndex:0];
}

- (void)setLanguage{
    self.headerOrderLabel.text = NSLocalizedStringFromTableInBundle(@"DIALOG_PURCHASING_PRI_REP_ORDER", nil, [Utils getLanguage], nil);
    self.headerMenuLabel.text = NSLocalizedStringFromTableInBundle(@"DIALOG_PURCHASING_PRI_REP_MENU", nil, [Utils getLanguage], nil);
    self.headerPriceLabel.text = NSLocalizedStringFromTableInBundle(@"DIALOG_PURCHASING_PRI_REP_PRICE", nil, [Utils getLanguage], nil);
    [self.headerOkBtn setTitle:NSLocalizedStringFromTableInBundle(@"BNT_PURCHASING_PRI_REP_OK", nil, [Utils getLanguage], nil) forState:UIControlStateNormal];
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(_dataArr == nil) {
        return 0;
    }
    else {
        return _dataArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RPPurchasingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    PurchasingListMenuModel *model = [_dataArr objectAtIndex:indexPath.row];
    cell.nameLable.text = [NSString stringWithFormat:@"%@",model.productName];
    cell.amountLable.text = [NSString stringWithFormat:@"%d",model.productAmount];
    cell.priceLabel.text = [NSString stringWithFormat:@"%d",(model.productPrice * model.productAmount)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return cellHeight;
}

#pragma mark - Dialog Functions


-(void)showDialogInView:(UIView *)targetView dataArr:(NSArray *)dataArr {
    _dataArr = dataArr;
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialog setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialog setAlpha:1.0];
    [UIView commitAnimations];
    [self.tableView reloadData];
    
    // Adjust table view frame size;
    int cellNumber = 6;
    if(_dataArr.count < 6) {
        cellNumber = (int)_dataArr.count;
    }
   
    CGFloat tableViewHeight = cellHeight * cellNumber;
    self.heightTableViewConstraint.constant = tableViewHeight;
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialog setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

-(BOOL)isDialogShowing {
    return self.isShowing;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialog.frame, touchLocation)) {
        [self dismissDialog];
    }
}

- (IBAction)confirmAction:(id)sender {
    [self dismissDialog];
}


@end
