//
//  NavTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 10/21/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *navIcon;
@property (weak, nonatomic) IBOutlet UILabel *navLabel;
@property (weak, nonatomic) IBOutlet UIImageView *navArrow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightConstaint;

@end
