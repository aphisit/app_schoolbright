//
//  SuggestionViewController.m
//  JabjaiApp
//
//  Created by mac on 4/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "SuggestionViewController.h"
#import "Utils.h"

@interface SuggestionViewController (){
}

@end

@implementation SuggestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SUGGESTION",nil,[Utils getLanguage],nil);
    self.webView.delegate = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://docs.google.com/forms/d/e/1FAIpQLSdEOFYW_RKFousSiHGb6sZOGFSNinWtjUjnyizu0LJxMF_E4w/viewform"]];
    [self.webView loadRequest:request];

    
}
- (void) viewDidLayoutSubviews{
    //set color header
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self.indicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.indicator stopAnimating];
    self.indicator.hidesWhenStopped = YES;
    
//    if ([self.webView.request.URL.description isEqualToString:@"https://docs.google.com/forms/d/e/1FAIpQLSdEOFYW_RKFousSiHGb6sZOGFSNinWtjUjnyizu0LJxMF_E4w/formResponse"]) {
//        
//        
//        
//    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}

- (IBAction)moveBack:(id)sender {
    
    SuggestionViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SuggestionStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
