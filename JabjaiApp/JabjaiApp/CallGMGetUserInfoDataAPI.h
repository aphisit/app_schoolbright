//
//  CallGMGetUserInfoDataAPI.h
//  JabjaiApp
//
//  Created by toffee on 4/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfoModel.h"
@class CallGMGetUserInfoDataAPI;
@protocol CallGMGetUserInfoDataAPIDelegate <NSObject>

- (void)callGMGetUserInfoDataAPI:(CallGMGetUserInfoDataAPI *)classObj data:(UserInfoModel*)data success:(BOOL)success;
@end
@interface CallGMGetUserInfoDataAPI : NSObject
@property (nonatomic, weak) id<CallGMGetUserInfoDataAPIDelegate> delegate;
- (void)getUserInfoData;

@end
