//
//  BSHistoryModel.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSHistoryModel : NSObject

@property (nonatomic) NSInteger behaviorType; // type 0: add, 1: reduce
@property (nonatomic) NSString *behaviorName;
@property (nonatomic) double behaviorScore;
@property (nonatomic) NSDate *dateTime;
@property (nonatomic) NSString *studentName;
@property (nonatomic) NSString *recorder;
@property (nonatomic) NSString *remark;
@property (nonatomic) NSString *picStudent;

- (void)setBehaviorType:(NSInteger)behaviorType;
- (void)setBehaviorName:(NSString *)behaviorName;
- (void)setBehaviorScore:(double)behaviorScore;
- (void)setDateTime:(NSDate *)dateTime;
- (void)setStudentName:(NSString *)studentName;
- (void)setRecorder:(NSString *)recorder;
- (void)setRemark:(NSString *)remark;
- (void)setPicStudent:(NSString *)picStudent;

- (NSInteger)getBehaviorType;
- (NSString *)getBehaviorName;
- (double)getBehaviorScore;
- (NSDate *)getDateTime;
- (NSString *)getStudentName;
- (NSString *)getRecorder;
- (NSString *)getRemark;
- (NSString *)getPicStudent;

@end
