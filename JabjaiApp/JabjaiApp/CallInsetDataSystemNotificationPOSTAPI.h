//
//  CallInsetDataSystemNotificationPOSTAPI.h
//  JabjaiApp
//
//  Created by Mac on 3/3/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CallInsetDataSystemNotificationPOSTAPIDelegate <NSObject>
@optional
- (void)callInsetDataSystemNotificationPOSTAPI:(BOOL)success;

@end
@interface CallInsetDataSystemNotificationPOSTAPI : NSObject
@property (nonatomic, weak)id<CallInsetDataSystemNotificationPOSTAPIDelegate> delegate;
- (void)call:(NSString *)jsonString;
@end

NS_ASSUME_NONNULL_END
