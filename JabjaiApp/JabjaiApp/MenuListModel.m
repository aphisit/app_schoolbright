//
//  MenuListModel.m
//  JabjaiApp
//
//  Created by toffee on 4/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "MenuListModel.h"

@implementation MenuListModel

@synthesize numberProduct = _numberProduct;

-(void)setNumberProduct:(NSInteger *)numberProduct{
    _numberProduct = numberProduct;
}

-(long long)getProductId{
    return self.productId;
}

-(NSString *)getProductName{
    return self.productName;
}

-(NSInteger *)getPrice{
    return self.price;
}

-(NSString *)barcode{
    return self.barcode;
}

-(NSString *)getPicture{
    return self.picture;
}

-(NSInteger *)getNumberProduct{
    return self.numberProduct;
}

@end
