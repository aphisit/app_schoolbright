//
//  SNSelectTheaCherViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TheacherAllModel.h"
#import "BSStudentNameRadioTableViewCell.h"
#import "SNSelectTeacherModel.h"
#import "SNLevelStudentViewController.h"
#import "SWRevealViewController.h"
#import "SNTimeOutViewController.h"
#import <CoreData/CoreData.h>

#import "SlideMenuController.h"

@interface SNSelectTheaCherViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate>
@property (strong, nonatomic) NSArray<SNSelectTeacherModel *> *addTheacherArrray;
@property (nonatomic) NSInteger sendType;
@property (nonatomic) NSInteger sendGroup;
@property (nonatomic) NSInteger newsType;
@property (nonatomic) NSMutableArray* theaCherImageArray;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerSendNewsLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTheacherLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextLabel;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)moveBack:(id)sender;
- (IBAction)actionNext:(id)sender;


@end
