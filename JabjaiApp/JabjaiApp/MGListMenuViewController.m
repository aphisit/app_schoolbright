//
//  MGListMenuViewController.m
//  JabjaiApp
//
//  Created by toffee on 4/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "MGListMenuViewController.h"
#import <Sheriff/GIBadgeView.h>
#import "AlertDialog.h"
#import "Utils.h"

@interface MGListMenuViewController (){
     NSBundle *myLangBundle;
}
@property (strong, nonatomic) CallGMMenuAPI *callGMMenuAPI;
@property (strong, nonatomic) CallGMGetUserInfoDataAPI *callGMGetUserInfoDataAPI;
@property (nonatomic, strong) GIBadgeView *floatingIcon1BadgeView;
@end
static NSString *cellIdentifier = @"ListMenuCell";
@implementation MGListMenuViewController{
    NSMutableArray *expandedSections;
    NSMutableArray *menuOrderArray;
    NSMutableArray *amountOrderArray;
    NSMutableArray *totalPriceOrderArray;
    NSMutableDictionary* productDict;
    NSInteger numberOrder;
    NSInteger sumOrder;
    NSInteger totalPrice;
    
    NSMutableString *orderJson;
    
    AlertDialog *alertDialog;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    expandedSections = [[NSMutableArray alloc] init];
    productDict = [[NSMutableDictionary alloc] init];
    amountOrderArray = [[NSMutableArray alloc] init];
    menuOrderArray = [[NSMutableArray alloc] init];
    totalPriceOrderArray = [[NSMutableArray alloc] init];
    numberOrder = 0;
    sumOrder = 0;
   
    self.floatingIcon1BadgeView = [GIBadgeView new];
    [self.confirmOrder addSubview:self.floatingIcon1BadgeView];
    self.confirmOrder.clipsToBounds = NO;
    self.confirmOrder.backgroundColor = [UIColor clearColor];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MenuFoodTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    [self setLanguage];
    [self getUserInfoData ];
    [self getStudentInClassData];
    // Do any additional setup after loading the view.
}

- (void)viewDidLayoutSubviews{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)setLanguage{
    self.headerShoppingLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_SHOPPING",nil,myLangBundle,nil);
    [self.headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_MARKET_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal ];
}

- (void)getStudentInClassData {
    [self showIndicator];
    if(self.callGMMenuAPI != nil) {
        self.callGMMenuAPI = nil;
    }
    
    self.callGMMenuAPI = [[CallGMMenuAPI alloc] init];
    self.callGMMenuAPI.delegate = self;
    [self.callGMMenuAPI call:[UserData getSchoolId] shopId:_shopId] ;
}

- (void)callGMMenuAPI:(CallGMMenuAPI *)classObj data:(NSArray<MenuListModel *> *)data success:(BOOL)success {
    
    if (data != nil) {
        _menuArray = data;
        [self stopIndicator];
        [self.tableView reloadData];
    }
    
    
}

- (void)getUserInfoData {
    
    if(self.callGMGetUserInfoDataAPI != nil) {
        self.callGMGetUserInfoDataAPI = nil;
    }
    
    self.callGMGetUserInfoDataAPI = [[CallGMGetUserInfoDataAPI alloc] init];
    self.callGMGetUserInfoDataAPI.delegate = self;
    [self.callGMGetUserInfoDataAPI getUserInfoData] ;
}

-(void)callGMGetUserInfoDataAPI:(CallGMGetUserInfoDataAPI *)classObj data:(UserInfoModel *)data success:(BOOL)success{
    if (data != nil) {
         UserInfoModel *model;
           model = data;
    }
}


#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
        if(_menuArray != nil) {
         
            return _menuArray.count;
        }
        else {
            return 0;
        }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    MenuFoodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    MenuListModel *model;
    model = [_menuArray objectAtIndex:indexPath.row];
    
    [cell.sectionView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.sectionView.layer setShadowOpacity:0.3];
    [cell.sectionView.layer setShadowRadius:3.0];
    [cell.sectionView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [productDict setValue:@"0" forKey:[NSString stringWithFormat:@"%d",[model getProductId]]];
    cell.index = indexPath.row;
    cell.delegate = self;
    cell.menuFoodLabel.text = [model getProductName];
    cell.priceLabel.text = [NSString stringWithFormat:@"%d %@",[model getPrice],NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_BAHT",nil,myLangBundle,nil)];
    
    if ([model getNumberProduct] == nil) {
        cell.numberTextField.text = @"0";
        cell.numberOrderTextField.text = @"";
    }else{
        cell.numberTextField.text = [NSString stringWithFormat:@"%d",[model getNumberProduct]];
        cell.numberOrderTextField.text = [NSString stringWithFormat:@"X%d",[model getNumberProduct]];
    }
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    [cell.numberTextField layoutIfNeeded];
    border.borderColor = [UIColor orangeColor].CGColor;
    border.frame = CGRectMake(0, cell.numberTextField.frame.size.height - borderWidth, cell.numberTextField.frame.size.width, cell.numberTextField.frame.size.height);
    border.borderWidth = borderWidth;
    
   
    cell.numberTextField.enabled = NO;
    [cell.numberTextField.layer addSublayer:border];
    cell.numberTextField.layer.masksToBounds = YES;
    if([expandedSections containsObject:@(indexPath.row)]) {
        cell.numberTextField.hidden = false;
        cell.addNumberLabel.hidden = false;
        cell.deleteNumberLabel.hidden = false;
    }
    else{
        cell.numberTextField.hidden = true;
        cell.addNumberLabel.hidden = true;
        cell.deleteNumberLabel.hidden = true;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
       /* Device is iPad */
        if([expandedSections containsObject:@(indexPath.row)]) {
            return 268;
        }else{
            return 170;
        }
    }else{
        /* Device is iPhone */
        if([expandedSections containsObject:@(indexPath.row)]) {
            return 178;
        }else{
            return 110;
        }
    }
   
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([expandedSections containsObject:@(indexPath.row)]) {
        [expandedSections removeObject:@(indexPath.row)];
    }
    else {
        [expandedSections addObject:@(indexPath.row)];
    }
    [tableView reloadData];
}

-(void)processNumberProduct:(MenuFoodTableViewCell *)tableViewCell atIndex:(NSInteger)index status:(NSString *)status{
    
    MenuListModel *model;
    model = _menuArray[index];
    numberOrder = [model getNumberProduct];
    if ([status isEqualToString:@"ADD"]) {
        numberOrder = numberOrder + 1;
        sumOrder = sumOrder + 1;
    }else if([status isEqualToString:@"DELETE"]&&numberOrder>0){
        numberOrder = numberOrder - 1;
        sumOrder = sumOrder - 1;
    }
    [_floatingIcon1BadgeView setBadgeValue:sumOrder];
    [model setNumberProduct:numberOrder];
    [self.tableView reloadData];
}

-(void)DataOrder{
    int numComma = 0;
    totalPrice = 0;
    orderJson = [[NSMutableString alloc] initWithString:@"["];
    for (int i = 0; i<_menuArray.count; i++) {
        MenuListModel *model;
        model = _menuArray[i];
        if (!([model getNumberProduct] == nil)) {
            NSInteger price = [model getPrice];
            NSInteger numberOrder = [model getNumberProduct];
            [menuOrderArray addObject:[model getProductName]];
            [amountOrderArray addObject:[NSString stringWithFormat:@"%d",[model getNumberProduct]]];
            [totalPriceOrderArray addObject:[NSString stringWithFormat:@"%d",price  * numberOrder]];
            totalPrice = totalPrice + (price  * numberOrder);
            
            [orderJson appendFormat:@"{\"product_id\":%lld,\"amount\":%d}",[model getProductId],[model getNumberProduct]];
            

            if( numComma < sumOrder-1) {
                [orderJson appendString:@","];
                numComma++;
            }
          
            
        }
        
    }
    [orderJson appendString:@"]"];
    NSLog(@"orderJson = %@",orderJson);
   
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}



- (IBAction)moveBack:(id)sender {
    MGListMargetViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGListMargetViewStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];

}

- (IBAction)actionConfirmOrder:(id)sender {
    
    [self DataOrder];
    if (menuOrderArray.count == 0 ) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_MAKET_LEAST", nil, myLangBundle, nil);
        [self showAlertDialogWithMessage:alertMessage];
    }else{
        MGBillSummaryViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGBillSummaryStoryboard"];
        viewController.orderArray = menuOrderArray;
        viewController.amountOrderArray = amountOrderArray;
        viewController.totalPriceOrderArray = totalPriceOrderArray;
        viewController.totalPrice = totalPrice;
        viewController.shopId = _shopId;
        viewController.orderStringJson = orderJson;
        viewController.nameShop = _nameShop;
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
   
}
@end
