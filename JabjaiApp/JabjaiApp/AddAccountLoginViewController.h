//
//  AddAccountLoginViewController.h
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MultipleUserModel.h"
#import "CallGetLoginAPI.h"
#import "SlideMenuController.h"
#import "MUListAllSchoolNameDialog.h"
#import "CallSTSendTokenAndLanguageAPI.h"
#import "CallInsetDataSystemNotificationPOSTAPI.h"
extern NSString *tokenStr;
@interface AddAccountLoginViewController : UIViewController <UITextFieldDelegate,CallGetLoginAPIDelegate, SlideMenuControllerDelegate, MUListAllSchoolNameDialogDelegate,CallSTSendTokenAndLanguageAPIDelegate, CallInsetDataSystemNotificationPOSTAPIDelegate>

// Global variable
@property (strong, nonatomic) NSArray<MultipleUserModel *> *userArray;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *headerSchoolNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerUsernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPasswordLabel;

 //constranint between button login with textgfield
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *distanceLonginBtnWithTextFeildConstraint;

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *schoolNameTextField;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

//- (IBAction)dismissKeyboards:(id)sender;

- (IBAction)actionLogin:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
