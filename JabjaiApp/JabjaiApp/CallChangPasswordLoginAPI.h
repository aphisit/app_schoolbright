//
//  CallChangPasswordLoginAPI.h
//  JabjaiApp
//
//  Created by toffee on 8/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CallChangPasswordLoginAPI;

@protocol CallChangPasswordLoginAPIDelegate <NSObject>
- (void) callChangPasswordLoginAPI :(CallChangPasswordLoginAPI *)classObj resCode:(NSInteger)resCode newToken:(NSString *)newToken success:(BOOL)success ;
@optional

@end

@interface CallChangPasswordLoginAPI : NSObject
@property (weak, nonatomic) id<CallChangPasswordLoginAPIDelegate> delegate;

- (void) call : (NSString *)newPassword userId:(long long)userId token:(NSString*)token schoolid:(long long)schoolid;
@end
