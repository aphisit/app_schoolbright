//
//  ChangPasswordDialog.h
//  JabjaiApp
//
//  Created by toffee on 8/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChangPasswordDialog;
@protocol ChangPasswordDialogDelegate <NSObject>
    @optional
    - (void)doChangPassword:(ChangPasswordDialog *)changPasswordDialog;
@end


@interface ChangPasswordDialog : UIViewController

@property (nonatomic, retain) id<ChangPasswordDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIView *contentview;
- (IBAction)actionCancel:(id)sender;



- (void)showDialogInView:(UIView *)targetView message:(NSString *)message ;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
