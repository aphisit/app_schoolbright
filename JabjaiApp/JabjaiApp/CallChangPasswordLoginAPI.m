//
//  CallChangPasswordLoginAPI.m
//  JabjaiApp
//
//  Created by toffee on 8/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallChangPasswordLoginAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallChangPasswordLoginAPI () {
    NSInteger connectCounter;
}

@end
@implementation CallChangPasswordLoginAPI

- (void)call:(NSString *)newPassword userId:(long long)userId token:(NSString *)token schoolid:(long long)schoolid{
    [self updateNewPassword:newPassword userId:userId token:token schoolid:schoolid];
}

- (void) updateNewPassword:(NSString *)newPassword userId:(long long)userId token:(NSString *)token schoolid:(long long)schoolid{
    NSString *urlString = [APIURL getUpdatePassword:newPassword userId:userId token:token schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
               [self updateNewPassword:newPassword userId:userId token:token schoolid:schoolid];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateNewPassword:newPassword userId:userId token:token schoolid:schoolid];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSDictionary class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateNewPassword:newPassword userId:userId token:token schoolid:schoolid];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                NSDictionary *returnedDict = returnedData;
                connectCounter = 0;
                NSString* token;
                NSInteger resCode = [[returnedDict objectForKey:@"resCode"] intValue];
                
                if(![[returnedDict objectForKey:@"token"] isKindOfClass:[NSNull class]]) {
                    token = [[NSMutableString alloc] initWithFormat:@"%@", [returnedDict objectForKey:@"token"]];
                }
                else {
                    token = [[NSMutableString alloc] initWithString:@""];
                }
             
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callChangPasswordLoginAPI:resCode:newToken:success:)]) {
                        [self.delegate callChangPasswordLoginAPI:self resCode:resCode newToken:token success:YES];
                    }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callChangPasswordLoginAPI:resCode:newToken:success:)]) {
                [self.delegate callChangPasswordLoginAPI:self resCode:nil newToken:nil success:NO];
            }
        }
    }];
    
}

@end
