//
//  BSConfirmBehaviorXIB.h
//  JabjaiApp
//
//  Created by toffee on 24/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSDataStudentScanerModel.h"
#import "BSSelectedStudent.h"
NS_ASSUME_NONNULL_BEGIN

@protocol BSConfirmBehaviorXIBDelegate <NSObject>

- (void)onAlertDialogScanerClose;
- (void)doCallConfirmScaner;

@end
@interface BSConfirmBehaviorXIB : UIViewController

@property (retain, nonatomic) id<BSConfirmBehaviorXIBDelegate> delegate;
- (void)showDialogInView:(UIView *)targetView data:(BSDataStudentScanerModel *)data statusScan:(int)statusScan;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@property (weak, nonatomic) IBOutlet UIImageView *studentPicture;
@property (weak, nonatomic) IBOutlet UILabel *headerStatusLable;
@property (weak, nonatomic) IBOutlet UILabel *headerNameLable;
@property (weak, nonatomic) IBOutlet UILabel *headerLevelLable;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *classRoomLabel;
@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBnt;
@property (weak, nonatomic) IBOutlet UIButton *confirmBnt;

- (IBAction)confirmAction:(id)sender;
- (IBAction)cancelAction:(id)sender;


@end

NS_ASSUME_NONNULL_END
