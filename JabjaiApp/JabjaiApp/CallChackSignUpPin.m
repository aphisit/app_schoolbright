//
//  CallChackSignUpPin.m
//  JabjaiApp
//
//  Created by toffee on 6/13/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallChackSignUpPin.h"
#import "APIURL.h"
#import "Utils.h"

@implementation CallChackSignUpPin{
    NSInteger connectCounter;
    NSString *responseString;
}

- (void)call:(long long)userId schoolid:(long long)schoolid {
    connectCounter = 0;
     [self getListMarget:userId schoolid:schoolid];
}
- (void)getListMarget:(long long)userId schoolid:(long long)schoolid {
    NSString *URLString = [APIURL getCheckSignUpPin:userId schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getListMarget:userId schoolid:schoolid];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            
            connectCounter = 0;
            //responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
              id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             NSString  *status = [returnedData objectForKey:@"resMessage"] ;
            
                
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callCheckSignUpPin:success:)]) {
                [self.delegate callCheckSignUpPin:status success:YES];
            }
        }
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callCheckSignUpPin:success:)]) {
                [self.delegate callCheckSignUpPin:nil success:NO];
            }
        }
       
    }];
    
}

@end
