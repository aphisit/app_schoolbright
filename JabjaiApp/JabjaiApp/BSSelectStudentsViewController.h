//
//  BSSelectStudentsViewController.h
//  JabjaiApp
//
//  Created by mac on 7/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "BSStudentNameRadioTableViewCell.h"
#import "CallGetStudentInClassroomAPI.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "BSSelectedStudent.h"
#import "BehaviorScoreModel.h"
#import <CoreData/CoreData.h>

#import "SlideMenuController.h"

@interface BSSelectStudentsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, BSStudentNameRadioTableViewCellDelegate, CallGetStudentInClassroomAPIDelegate ,SlideMenuControllerDelegate, UISearchBarDelegate>

// Global variables
// BSSelectClassLevelViewController
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger mode;
@property (nonatomic) NSMutableArray* mutableArray;
@property (nonatomic) int status;
// BSSelectStudentsViewController
@property (strong, nonatomic) NSArray<BSSelectedStudent *> *selectedStudentArray;

// BSSelectBehaviorScoreViewController
@property (strong, nonatomic) NSArray<BehaviorScoreModel *> *behaviorScoreArray;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *optSelectButton;
@property (weak, nonatomic) IBOutlet UILabel *noNumberStudent;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)actionNext:(id)sender;
- (IBAction)moveBack:(id)sender;
- (IBAction)actionSelect:(id)sender;


@end
