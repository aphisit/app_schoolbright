//
//  TAStudentStatusModel.m
//  JabjaiApp
//
//  Created by mac on 7/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TAStudentStatusModel.h"

@implementation TAStudentStatusModel

@synthesize userId = _userId;
@synthesize scanStatus = _scanStatus;
@synthesize studentId = _studentId;
@synthesize studentName = _studentName;
@synthesize teacherId = _teacherId;
@synthesize checkedTeacherName = _checkedTeacherName;
@synthesize authorized = _authorized;
@synthesize pic = _pic;
@synthesize studentState = _studentState;
@synthesize statusCheck = _statusCheck;

- (void)setUserId:(long long)userId {
    _userId = userId;
}

- (void)setScanStatus:(int)scanStatus {
    _scanStatus = scanStatus;
}

- (void)setStudentId:(NSString *)studentId {
    _studentId = studentId;
}

- (void)setStudentName:(NSString *)studentName {
    _studentName = studentName;
}

- (void)setTeacherId:(long long)teacherId {
    _teacherId = teacherId;
}

- (void)setCheckedTeacherName:(NSString *)checkedTeacherName {
    _checkedTeacherName = checkedTeacherName;
}

- (void)setAuthorized:(BOOL)authorized {
    _authorized = authorized;
}

- (void)setStudentPic:(NSString *)studentPic {
    _pic = studentPic;
}

- (void)setStudentState:(int)studentState{
    _studentState = studentState;
}

- (void)setStatusCheck:(BOOL)statusCheck{
    _statusCheck = statusCheck;
}


- (long long)getUserId {
    return _userId;
}

- (int)getScanStatus {
    return _scanStatus;
}

- (NSString *)getStudentId {
    return _studentId;
}

- (NSString *)getStudentName {
    return _studentName;
}

- (long long)getTeacherId {
    return _teacherId;
}

- (NSString *)getCheckedTeacherName {
    return _checkedTeacherName;
}

- (BOOL)getAuthorized {
    return _authorized;
}
- (NSString *)getStudentPic {
    return _pic;
}

- (int) getStudentState {
    return  _studentState;
}

- (BOOL)getStatusCheck{
    return _statusCheck;
}


@end
