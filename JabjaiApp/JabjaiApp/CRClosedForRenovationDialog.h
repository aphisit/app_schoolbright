//
//  CRClosedForRenovationDialog.h
//  JabjaiApp
//
//  Created by Mac on 10/2/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CRClosedForRenovationDialog : UIViewController
@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
- (IBAction)closeAppAction:(id)sender;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message;
@end

NS_ASSUME_NONNULL_END
