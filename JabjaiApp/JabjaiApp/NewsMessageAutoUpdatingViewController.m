//
//  NewsMessageAutoUpdatingViewController.m
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NewsMessageAutoUpdatingViewController.h"
#import "NewsMessageTableViewCell.h"
#import "ProgressTableViewCell.h"
#import "MessageInboxDataModel.h"
#import "SWRevealViewController.h"
#import "MessageInBoxScrollableTextDialog.h"
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"
#import "AppDelegate.h"

// 04/01/2561

#import "UserInfoDBHelper.h"

static NSString *dataCellIdentifier = @"MessageCell";

@interface NewsMessageAutoUpdatingViewController () {
    
    NSMutableDictionary<NSNumber *, NSArray<MessageInboxDataModel *> *> *messageSections;
    NSMutableArray<MessageInboxDataModel *> *messages;
    
    UIColor *unreadBGColor;
    UIColor *readBGColor;
    NSInteger connectCounter;
    
    NSManagedObject *schoolImage;
}

@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property (nonatomic, strong) MessageInBoxScrollableTextDialog *messageInboxScrollableTextDialog;
@property (nonatomic, strong) SNDetailNewsViewController *sNDetailNewsViewController;
// 04/01/2561
//@property (strong, nonatomic) UserInfoModel *userInfoModel;
@property (strong, nonatomic) UserInfoDBHelper *dbHelper;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation NewsMessageAutoUpdatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    messageSections = [[NSMutableDictionary alloc] init];
    messages = [[NSMutableArray alloc] init];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NewsMessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:dataCellIdentifier];
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    
    
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [self.timeFormatter setDateFormat:@"HH:mm"];
    }
    
    unreadBGColor = [UIColor whiteColor];
    readBGColor = [UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1.0];
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    [self getNewsMessageDataWithPage:1];
    // 04/01/2561
    [self getSchoolIconNews];
    [self setLanguage];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}


//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
}

-(void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFORM_BROADCAST",nil,[Utils getLanguage],nil);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return messageSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger page = section + 1;
    return [[messageSections objectForKey:@(page)] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
    
    NSInteger page = indexPath.section + 1;
    MessageInboxDataModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
    cell.titleLabel.text = model.title;
    cell.messageLabel.text = model.message;
    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
    cell.statementreadLabel.clipsToBounds = YES;
    cell.statementreadLabel.layer.shouldRasterize = YES;
    
    // 04/01/2561
    NSData *loadedData = [schoolImage valueForKey:@"savedImage"];
    
    UIImage *schoolImage = [UIImage imageWithData:loadedData];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
    [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
    UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    cell.schoolLogo.image = schoolImage2;
    cell.schoolLogo.contentMode = UIViewContentModeCenter;
    
//    cell.schoolLogo.image = [UIImage imageWithData: loadedData];
    cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
    cell.schoolLogo.clipsToBounds = YES;
    cell.schoolLogo.layer.shouldRasterize = YES;
    
    if (model.file == YES) {
        cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
    }
    else{
        cell.iconFile.image = nil;
    }
    
    if (model.status == 0) {
        cell.statementreadLabel.hidden = NO;
    }
    
    else{
        cell.statementreadLabel.hidden = YES;
    }
    
    if(model.status == 0) {
        // Unread
        cell.backgroundColor = unreadBGColor;
    }
    else {
        cell.backgroundColor = readBGColor;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger page = indexPath.section + 1;
    MessageInboxDataModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    [self showMessage:model];
    
    if(model.status == 0) { // message still unread
        [self updateReadStatus:model];
        model.status = 1; //    set message already read
    }

    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
      {
          return 140;
      }else{
          return 240;
      }
}

#pragma mark - Get API Data
- (void)getNewsMessageDataWithPage:(NSUInteger)page {
    
    [self showIndicator];
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getNewsMessageWithPage:page userID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getNewsMessageDataWithPage:page];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getNewsMessageDataWithPage:page];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getNewsMessageDataWithPage:page];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    int status = [[dataDict objectForKey:@"nStatus"] intValue];
                    int type = [[dataDict objectForKey:@"nType"] intValue];
                    NSMutableString *messageDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dSend"]];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sTitle"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    BOOL file = [[dataDict objectForKey:@"file"] boolValue];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    NSRange dotRange = [messageDateStr rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:messageDateStr];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:messageDateStr];
                    }
                    
                    MessageInboxDataModel *model = [[MessageInboxDataModel alloc] init];
                    model.messageID = messageID;
                    model.userID = (int)[UserData getUserID];
                    model.messageType = type;
                    model.status =status;
                    model.title = title;
                    model.message = message;
                    model.date = messageDate;
                    model.file = file;
                    
                    [newDataArr addObject:model];
                    
                }
                
                [messages removeAllObjects];
                messages = [[NSMutableArray alloc] initWithArray:newDataArr];
                [messageSections setObject:newDataArr forKey:@(page)];
            
                [self.tableView reloadData];
            }
            
        }
    }];
}

-(void)updateReadStatus:(MessageInboxDataModel *)model {
    
        long long userID = [UserData getUserID];
        long long schoolid = [UserData getSchoolId];
        NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:model.messageID userID:userID schoolid:schoolid];
        NSURL *URL = [NSURL URLWithString:URLString];
        
        [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
                
                if(error != nil) {
                    NSLog(@"An error occured : %@" , [error localizedDescription]);
                }
                else if(data == nil) {
                    NSLog(@"Data is null");
                }
                else {
                    NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                    NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
                }
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateReadStatus:model];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if(error != nil) {
                    NSLog(@"An error occured : %@", [error localizedDescription]);
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self updateReadStatus:model];
                    }
                    else {
                        connectCounter = 0;
                    }
                    
                }
                else if(![returnedData isKindOfClass:[NSArray class]]) {
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self updateReadStatus:model];
                    }
                    else {
                        connectCounter = 0;
                    }
                    
                }
                else {
                    
                    NSArray *returnedArray = returnedData;
                    connectCounter = 0;
                    if(returnedArray.count == 0) {
                        NSLog(@"%s", "Update read message status failed (return array size 0)");
                    }
                }
            }
        }];
    
}

- (void)fetchMoreData {
    
    if(messages.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *lastRowIndexPath = [visiblePaths lastObject];
        
        // Check whether or not the very last row is visible.
        NSInteger numberOfSections = [self.tableView numberOfSections];
        NSInteger lastRowSection = [lastRowIndexPath section];
        NSInteger lastRowRow = [lastRowIndexPath row];
        NSInteger numberOfRowsInSection = [self.tableView numberOfRowsInSection:lastRowSection];
        
        if(lastRowSection == numberOfSections -1 && lastRowRow == numberOfRowsInSection - 1) {
            
            if(messages.count % 20 == 0) {
                NSInteger nextPage = lastRowSection + 2;
                [self getNewsMessageDataWithPage:nextPage];
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self fetchMoreData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self fetchMoreData];
}

- (void)getSchoolIconNews{
    
    NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSchoolIconNews];
            }
            else {
                connectCounter = 0;
            }
            
        }
        
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolIconNews];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolIconNews];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if (returnedArray.count != 0) {
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    NSString *imageSchoolIconUrl;
                    
                    if(![[dataDict objectForKey:@"school_logo"] isKindOfClass:[NSNull class]]){
                        imageSchoolIconUrl = [dataDict objectForKey:@"school_logo"];
                    }
                    else{
                        imageSchoolIconUrl = @"";
                    }
                    /////
                    NSData  *data;
                    NSManagedObjectContext *context = [self managedObjectContext];
                    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: imageSchoolIconUrl]];
                    schoolImage  = [NSEntityDescription insertNewObjectForEntityForName:@"Entity" inManagedObjectContext:context];
                    if(imageData != nil){
                        
                        UIImage *image = [UIImage imageWithData: imageData];
                        data = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.0)];
                        [schoolImage setValue:data forKey:@"savedImage"];
                    }else{
                        data = [NSData dataWithData:UIImageJPEGRepresentation([UIImage imageNamed:@"school"], 0.0)];
                        [schoolImage setValue:data forKey:@"savedImage"];
                    }
                    /////
                    if(imageSchoolIconUrl != nil && imageSchoolIconUrl.length > 0) {
                        BOOL success = [self.dbHelper updateImageUrlWithSlaveId:[UserData getUserID] imageUrl:imageSchoolIconUrl];
                        NSLog(@"%@", [NSString stringWithFormat:@"update school icon success : %d", success]);
                    }
                    [self.tableView reloadData];
                }
            }
        }
    }];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Dialog
- (void)showMessage:(MessageInboxDataModel *)model {
    if(self.sNDetailNewsViewController == nil) {
        self.sNDetailNewsViewController = [[SNDetailNewsViewController alloc] init];
    }
    
    if(self.sNDetailNewsViewController != nil && [self.sNDetailNewsViewController isDialogShowing]) {
        [self.sNDetailNewsViewController dismissDetailNewsViewController];
    }
   
    [self.sNDetailNewsViewController showDetailNewsViewController:self.view model:model typeSend:0];
//    SNDetailNewsMessageViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNDetailNewsMessageStoryboard"];
//    viewController.model = model;
//    viewController.typySend = 1;
//    [self.slideMenuController changeMainViewController:viewController close:YES];
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];  
    
//    [self.revealViewController revealToggle:self.revealViewController];
}

@end
