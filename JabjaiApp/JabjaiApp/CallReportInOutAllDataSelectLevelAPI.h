//
//  CallReportInOutAllDataSelectLevelAPI.h
//  JabjaiApp
//
//  Created by toffee on 14/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReportInOutAllDataSelectLevelModel.h"

NS_ASSUME_NONNULL_BEGIN

@class CallReportInOutAllDataSelectLevelAPI;

@protocol CallReportInOutAllDataSelectLevelAPIDelegate <NSObject>
@optional

- (void)callReportInOutAllDataSelectLevelAPI:(CallReportInOutAllDataSelectLevelAPI *)classObj statusAllArray:(NSMutableArray<ReportInOutAllDataSelectLevelModel *>*)datalevelArray success:(BOOL)success;
@end
@interface CallReportInOutAllDataSelectLevelAPI : NSObject
@property (nonatomic, weak) id<CallReportInOutAllDataSelectLevelAPIDelegate> delegate;
- (void)call:(long long)schoolId date:(NSString*)date;
@end

NS_ASSUME_NONNULL_END
