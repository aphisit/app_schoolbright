//
//  TeacherTimeTableListViewController.h
//  JabjaiApp
//
//  Created by mac on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeacherTimeTableViewController.h"

@interface TeacherTimeTableListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, TeacherTimeTableViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;


@end
