//
//  SNTimeOutViewController.m
//  JabjaiApp
//
//  Created by toffee on 9/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNTimeOutViewController.h"
#import <QBImagePickerController/QBImagePickerController.h>
#import "SNLevelStudentViewController.h"
#import "Utils.h"
#import "UserData.h"
#import "AlertDialog.h"
//lib choose file
#import "DBAttachmentPickerController.h"
#import "DBAttachment.h"
#import "NSDateFormatter+DBLibrary.h"


@interface SNTimeOutViewController (){
    AlertDialog *alertDialog;
    //UIImage *chosenImage;
   // NSMutableArray * imagesArray;
    UIImage *ima;
    PHImageRequestOptions *requestOptions;
    //NSMutableArray *addImage;
    NSString *dateString;
    int exitRownumber;
    NSBundle *myLangBundle;
    
}

@property (strong, nonatomic) DBAttachmentPickerController *pickerController;
//@property (strong, nonatomic) CellUpdateSendNewsPOSTAPI *cellUpdateSendNewsPOSTAPI;
@property (strong, nonatomic) SelectOneDayCalendarViewController *calendarDialog;
@property (strong, nonatomic) SNPickerTimeViewController *pickerTimeDialog;
@property (strong, nonatomic) NSDate *minDate;
@property (strong, nonatomic) NSDate *maxDate;
@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) PhotoAlbumLibary *photoAlbumLibary;
@end

static NSString  *cellIdentifier = @"Cell";

@implementation SNTimeOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    exitRownumber = 0;
    [self.selectFileCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SNIconFileCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.selectFileCollectionView.delegate = self;
    self.selectFileCollectionView.dataSource = self;
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SNIconFileCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.dateTextFiled.delegate = self;// Set textfield delegate
    self.timeTextFiled.delegate = self;
    self.detail.delegate = self;
    if (self.newsDetail != NULL ) {
        self.detail.text = self.newsDetail;
        self.detail.textColor = [UIColor blackColor];
        self.detail.tag = 1;
        if ( self.sendTimeType == 0) {
            chackRadioSetTime = NO;
            [self doRadio];
        }else{
            chackRadioSetTime = YES;
            [self doRadio];
        }
        [self.detail resignFirstResponder];
        [self.collectionView reloadData];
        [self.selectFileCollectionView reloadData];
    }else{
        self.detail.text = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_SENDNEWS_DETAIL",nil,myLangBundle,nil);
           self.detail.textColor = [UIColor lightGrayColor];
           self.detail.tag = 0;
    }
   
    chackRadioSetTime = self.sendTimeType;
    self.dateTextFiled.hidden = YES;
    self.timeTextFiled.hidden = YES;
    self.dateTextFiled.userInteractionEnabled = false;
    self.timeTextFiled.userInteractionEnabled = false;
    
    //set max min calanda
    self.gregorian = [Utils getGregorianCalendar];
    self.maxDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:+1 toDate:[NSDate date] options:0];//[NSDate date];
    self.minDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:0 toDate:[NSDate date] options:0];
    self.calendarDialog = [[SelectOneDayCalendarViewController alloc] initWithTitle:@"เลือกวันที่" minimumDate:self.minDate maximumDate:self.maxDate];
    self.calendarDialog.delegate = self;
    
    if(self.sendTimeType == 1){
        _topSpeceToSecond.constant = 50;
    }else{
        _topSpeceToSecond.constant = 10;
    }

    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(hideKeyboard)];
    keyboardToolbar.items = @[doneBarButton];
    self.detail.inputAccessoryView = keyboardToolbar;
    [self setLanguage];
    [self doRadio];
   
   }



- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
    
    [self.shadowDetailView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.shadowDetailView.layer setShadowOpacity:0.3];
    [self.shadowDetailView.layer setShadowRadius:3.0];
    [self.shadowDetailView.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    
    [self.shadowImageView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.shadowImageView.layer setShadowOpacity:0.3];
    [self.shadowImageView.layer setShadowRadius:3.0];
    [self.shadowImageView.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    
    [self.shadowFileView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.shadowFileView.layer setShadowOpacity:0.3];
    [self.shadowFileView.layer setShadowRadius:3.0];
    [self.shadowFileView.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    
    [self.selectFileBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.selectFileBtn.layer setShadowOpacity:0.3];
    [self.selectFileBtn.layer setShadowRadius:3.0];
    [self.selectFileBtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    [self.selectFileBtn.layer setCornerRadius:10.0];
    self.selectFileBtn.titleLabel.numberOfLines = 0;
    [self.selectFileBtn.titleLabel setTextAlignment:UITextAlignmentCenter];

    [self.selectImageBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.selectImageBtn.layer setShadowOpacity:0.3];
    [self.selectImageBtn.layer setShadowRadius:3.0];
    [self.selectImageBtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    [self.selectImageBtn.layer setCornerRadius:10.0];
    self.selectImageBtn.titleLabel.numberOfLines = 0;
    [self.selectImageBtn.titleLabel setTextAlignment:UITextAlignmentCenter];
    
    [self.submitBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.submitBtn.layer setShadowOpacity:0.3];
    [self.submitBtn.layer setShadowRadius:3.0];
    [self.submitBtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    
    
}

-(void)setLanguage{
   
    self.headerSendNewsLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS",nil,myLangBundle,nil);
    self.headerDeliveryLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_DELIVERY",nil,myLangBundle,nil);
    self.headerSendNowLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_SENDNOW",nil,myLangBundle,nil);
    self.headerSetTimeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_SETTIME",nil,myLangBundle,nil);
    self.headerDescription.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_DESCIPTION",nil,myLangBundle,nil);
    self.headerAttachLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_ATTACH_IMAGE",nil,myLangBundle,nil);
    self.headerAttachFileLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_ATTACH_FILE",nil,myLangBundle,nil);
    self.dateTextFiled.placeholder = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_DATE",nil,myLangBundle,nil);
    self.timeTextFiled.placeholder = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_TIME",nil,myLangBundle,nil);
    [self.selectImageBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_CHOOSE_IMAGE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    [self.selectFileBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_CHOOSE_FILE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
//    self.detail.text = NSLocalizedString(@"Type your question here", @"placeholder");
//    self.detail.textColor = [UIColor lightGrayColor];
    [self.headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}



#pragma mark - Keyboard
- (void)hideKeyboard
{
    //self.detail.dataDetectorTypes = UIDataDetectorTypeLink;
    [self.detail resignFirstResponder];
}

- (void)switchDatePickerHidden
{
    [_timeTextFiled resignFirstResponder];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (textField.tag == 0) {
        //dialog date
        [self.detail resignFirstResponder];
        self.calendarDialog = [[SelectOneDayCalendarViewController alloc] initWithTitle:@"เลือกวันที่" minimumDate:_minDate maximumDate:_maxDate];
        self.calendarDialog.delegate = self;
        if([self.calendarDialog isDialogShowing]) {
            [self.calendarDialog dismissDialog];
        }
        [self.calendarDialog showDialogInView:self.view];
        return NO;
    }else{
        [self.detail resignFirstResponder];
        //dialog time
        if([self.pickerTimeDialog isDialogShowing]) {
            [self.pickerTimeDialog dismissDialog];
        }
        self.pickerTimeDialog = [[SNPickerTimeViewController alloc]init];
        self.pickerTimeDialog.delegate = self;
        [self.pickerTimeDialog showDialogInView:self.view];
        return NO;
    }
    return YES;
}

#pragma mark - UITextViewDelegate
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    
    if (textView.tag == 0) {
        self.detail.text = @"";
    }

    //self.detail.text = @"";
    self.detail.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView {

    if(self.detail.text.length == 0) {
        self.detail.tag = 0;
        self.detail.textColor = [UIColor lightGrayColor];
        self.detail.text = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_SENDNEWS_DETAIL",nil,myLangBundle,nil);;
        [self.detail resignFirstResponder];
    }else{
        self.detail.tag = 1;
    }
}


-(void) textViewShouldEndEditing:(UITextView *)textView {

    if(self.detail.text.length == 0) {
                self.detail.textColor = [UIColor lightGrayColor];
        self.detail.text = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_SENDNEWS_DETAIL",nil,myLangBundle,nil);
        [self.detail resignFirstResponder];
    }
}



#pragma mark - SelectOneDayCalendarViewController
-(void)calendarDialogPressOK:(NSArray<NSDate *> *)selectedDates{
    if(selectedDates.count > 0){
        
            NSDate *selectDate = [selectedDates objectAtIndex:0];
            NSString *formattedDate = [Utils dateInStringFormat:selectDate];
            self.dateTextFiled.text = formattedDate;
            self.dateSend =  selectDate;
        
       
    }
}
-(void)calendarDialogPressCancel {
    NSLog(@"%@", @"Cancel calendar dialog");
}

#pragma mark - SNPickerTimeViewController
-(void)pickerDialogPressOK:(NSDate *)time{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"H:mm"];
    NSString *currentTime = [dateFormatter stringFromDate:time];
    self.timeTextFiled.text = currentTime;
    self.timeSend =  self.timeTextFiled.text;
}
-(void)pickerDialogPressCancel{
    NSLog(@"%@", @"Cancel calendar dialog");
}

- (void)doRadio{
    
    if(chackRadioSetTime == NO){
        self.dateTextFiled.hidden = YES;
        self.timeTextFiled.hidden = YES;
        self.dateTextFiled.userInteractionEnabled = false;
        self.timeTextFiled.userInteractionEnabled = false;
        [self.btnSettime setImage:[UIImage imageNamed:@"ic_radio_clear"] forState:UIControlStateNormal];
        [self.btnSendNow setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
        chackRadioSetTime = NO;
        self.sendTimeType = 0;
        self.dateTextFiled.text = @"";
        self.timeTextFiled.text = @"";
        self.timeSend = @"";
        //self.dateSend = [NSDate date];
        
        
    }else{
       
        self.dateTextFiled.hidden = NO;
        self.timeTextFiled.hidden = NO;
        [self.btnSettime setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
        [self.btnSendNow setImage:[UIImage imageNamed:@"ic_radio_clear"] forState:UIControlStateNormal];
        chackRadioSetTime = YES;
        self.sendTimeType = 1;
        if(self.dateSend != nil && ![self.timeSend isEqualToString:@""]){
            self.dateTextFiled.text = [Utils dateInStringFormat:self.dateSend];
            self.timeTextFiled.text = self.timeSend;
        }
        self.dateTextFiled.userInteractionEnabled = true;
        self.timeTextFiled.userInteractionEnabled = true;
    }
    [self.detail resignFirstResponder];
}

- (IBAction)sendNowRadio:(id)sender {
    
    chackRadioSetTime = NO;
    [self doRadio];
    _topSpeceToSecond.constant = 10;
    //[self.detail resignFirstResponder];
    //topspeceto
}

- (IBAction)setTimeRadio:(id)sender {
    chackRadioSetTime = YES;
    [self doRadio];
     _topSpeceToSecond.constant = 50;
    //[self.detail resignFirstResponder];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (BOOL)validateData {
   
    if (self.detail.tag == 0){
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_PROVIDE_DETAILS",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    if(self.sendTimeType == 1 && [self.dateTextFiled.text isEqualToString:@""]) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_PLEASE_DATE",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    else if(self.sendTimeType == 1 && [self.timeTextFiled.text isEqualToString:@""]) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_PLEASE_TIME",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
   
    return YES;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (collectionView == self.collectionView) {
        return [self.imagesArray count];
    }else{
        return [self.attachmentArray count];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SNIconFileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    cell.fileBtn.tag = indexPath.row;
    cell.typeCollectionView = collectionView;
    if (collectionView == self.collectionView) {
        
        cell.fileimage.image = [self.imagesArray objectAtIndex:indexPath.row];
        
    }else{

        DBAttachment *attachment = self.attachmentArray[indexPath.row];
        CGFloat scale = [UIScreen mainScreen].scale;
        CGSize scaledThumbnailSize = CGSizeMake( 80.f * scale, 80.f * scale );
        [attachment loadThumbnailImageWithTargetSize:scaledThumbnailSize completion:^(UIImage *resultImage) {
            cell.fileimage.image = resultImage;
        }];
    }
     return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize defaultSize = CGSizeMake((self.collectionView.frame.size.width/3)-10, (self.collectionView.frame.size.height));
    return defaultSize;
}

#pragma  mark - SNIconFileCollectionViewCellDelegate
-(void) removeIndexFile:(NSInteger)index collectionView:(UICollectionView *)collectionView{
    
    if (collectionView == self.collectionView) {
        [self.imagesArray removeObjectAtIndex:index];
        [self.collectionView reloadData];
    }else{
        [self.attachmentArray removeObjectAtIndex:index];
        [self.selectFileCollectionView reloadData];
    }
}

#pragma mark - PhotoAlbumDelegate
- (void) replyPhotoAlbum:(PhotoAlbumLibary *)classObj imageArray:(NSMutableArray *)imageArray{
    if (imageArray != nil) {
        self.submitBtn.enabled = YES;
        self.imagesArray = imageArray;
        [self.collectionView reloadData];
    }
}

- (IBAction)selectFile:(id)sender {
    self.submitBtn.enabled = NO;
    __weak typeof(self) weakSelf = self;
    self.attachmentArray = [[NSMutableArray alloc] initWithCapacity:100];
    DBAttachmentPickerController *attachmentPickerController = [DBAttachmentPickerController attachmentPickerControllerFinishPickingBlock:^(NSArray<DBAttachment *> * _Nonnull attachmentArray)
            {
                for (DBAttachment *model in attachmentArray) {
                    NSArray *typeArray = [model.fileName componentsSeparatedByString:@"."];
                    NSString *type = [typeArray objectAtIndex:typeArray.count-1];
                    if ([type isEqualToString:@"pdf"] || [type isEqualToString:@"docx"] || [type isEqualToString:@"xlsx"]) {
                        [weakSelf.attachmentArray addObjectsFromArray:attachmentArray];
                    }else{
                        [self showAlertDialogWithMessage:@"รองรับเฉพาะไฟล์ PDF,WORD,EXCEL"];
                    }
                    self.submitBtn.enabled = YES;
                    [self.selectFileCollectionView reloadData];
                }
            } cancelBlock:^(){
                self.submitBtn.enabled = YES;
            }];
    
    attachmentPickerController.mediaType =   DBAttachmentMediaTypeOther;
    attachmentPickerController.allowsSelectionFromOtherApps = YES;
    [attachmentPickerController presentOnViewController:self];
    
}

- (IBAction)selectImage:(id)sender {
    self.submitBtn.enabled = NO;
    self.photoAlbumLibary = [[PhotoAlbumLibary alloc] init];
    self.photoAlbumLibary.delegate  = self;
    [self.photoAlbumLibary showPhotoAlbumView:self.view];
}

- (IBAction)backToMenu:(id)sender {
    SNLevelStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SendNewsStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (IBAction)actionSubmit:(id)sender {
    NSString *str = [NSString stringWithFormat:@"%@",self.detail.text];
   // CFStringTrimWhitespace((__bridge CFMutableStringRef) str);
    NSLog(@"Detail = %@",self.detail.text);
    self.newsDetail = str;
    
    dateString = [Utils datePunctuateStringFormat:[NSDate date]];
   // self.sendDay = [NSString stringWithFormat: @"\" %@ \"", dateString];
    if([self validateData]) {
        SNConfirmNewsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNComfirmMainStoryboard"];
        viewController.sendTimeType = self.sendTimeType;
        viewController.sendType = self.sendType;
        viewController.newsType = self.newsType;
        viewController.sendGroup = self.sendGroup;
        viewController.newsDetail = self.newsDetail;
        viewController.level2id = self.level2id;
        viewController.studentid = self.studentid;
        viewController.employeesid = self.employeesid;
        viewController.dateSend = _dateSend;
        viewController.timesend = _timeSend;
        viewController.fileArray = self.attachmentArray;
        viewController.userAcceptArray = self.userAcceptArray;
        viewController.setnewsDetail = self.detail.text;
        viewController.imageNews = self.imagesArray;
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
}


@end

