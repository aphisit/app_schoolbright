//
//  SelectYearAndTermDialog.m
//  JabjaiApp
//
//  Created by mac on 4/17/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "SelectYearAndTermDialog.h"
#import "Utils.h"
#import "DateUtility.h"
#import "YearTermModel.h"

@interface SelectYearAndTermDialog (){
    
    BOOL selectStartDateTextField;
    
    NSMutableArray<NSNumber *> *yearList;
    NSMutableArray *semesterList;
    NSMutableArray *semesterIDList;
    NSMutableArray *statusList;
    
    NSDictionary *yearTermDict; //Key is yearNumber and data are array of YearTermModel
    
    NSNumber *selected_year;
    NSString *selected_semester;
    NSString *select_term_id;
    
}

@property (nonatomic, assign) BOOL isShowing;
@property (strong, nonatomic) UIColor *orangeColor;

@end

@implementation SelectYearAndTermDialog

- (id)initWithYearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict {
    
    self = [super init];
    
    if(self) {
        yearTermDict = dataDict;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.orangeColor = UIColorWithHexString(@"#F56B20");
    
    // Set up dropdown
    self.schoolYearDropdown.dataSource = self;
    self.schoolYearDropdown.delegate = self;
    self.schoolYearDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.schoolYearDropdown.backgroundDimmingOpacity = 0;
    self.schoolYearDropdown.dropdownShowsTopRowSeparator = YES;
    self.schoolYearDropdown.dropdownCornerRadius = 5.0;
    self.schoolYearDropdown.tintColor = [UIColor blackColor];
    self.schoolYearDropdown.layer.borderWidth = 1.0f;
    self.schoolYearDropdown.layer.borderColor = [[UIColor blackColor]CGColor];
    
    self.semesterDropdown.dataSource = self;
    self.semesterDropdown.delegate = self;
    self.semesterDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.semesterDropdown.backgroundDimmingOpacity = 0;
    self.semesterDropdown.dropdownShowsTopRowSeparator = YES;
    self.semesterDropdown.dropdownCornerRadius = 5.0;
    self.semesterDropdown.tintColor = [UIColor blackColor];
    self.semesterDropdown.layer.borderWidth = 1.0f;
    self.semesterDropdown.layer.borderColor = [[UIColor blackColor]CGColor];
    
    yearList = [[NSMutableArray alloc] init];
    semesterList = [[NSMutableArray alloc] init];
    
    [self initializeYearData];
    [self initializeSemesterData];
    
    // Initialize variables
    self.isShowing = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm;
    //set color header
    [self.okButton layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.okButton.bounds;
    [self.okButton.layer insertSublayer:gradientConfirm atIndex:0];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}


#pragma mark - MKDropdownMenuDataSource

-(NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu {
    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component {
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        return yearList.count;
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        return semesterList.count;
    }
    
    return 0;
}

#pragma mark - MKDropdownMenuDelegate

- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
    return 35;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {
    
    NSString *textString;
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        textString = [[NSString alloc] initWithFormat:@"%d", [selected_year intValue]];
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        textString = selected_semester;
    }
    else {
        textString = @"";
    }
    
    NSAttributedString *attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"ThaiSansNeue-Regular" size:24], NSForegroundColorAttributeName: self.orangeColor }];
    
    return attributes;
}

-(NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *textString;
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        textString = [[NSString alloc] initWithFormat:@"%d", [[yearList objectAtIndex:row] intValue]];
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        textString = [semesterList objectAtIndex:row];
    }
    else {
        textString = @"";
    }
    
    NSAttributedString *attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"ThaiSansNeue-Regular" size:24], NSForegroundColorAttributeName: self.orangeColor }];
    
    return attributes;
}

- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
    return NO;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        NSNumber *year = [yearList objectAtIndex:row];
        
        if([year intValue] != [selected_year intValue]) {
            selected_year = year;
            [self initializeSemesterData];
            
            [dropdownMenu reloadAllComponents];
            [self.semesterDropdown reloadAllComponents];
            
        }
        
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        
        NSString *semester = [semesterList objectAtIndex:row];
        
        if(![semester isEqualToString:selected_semester]) {
            selected_semester = semester;
            
            [self adjustTermIDFromSemester];
            
            [dropdownMenu reloadAllComponents];
        
        }
        
    }
    
    
    delay(0.15, ^{
        [dropdownMenu closeAllComponentsAnimated:YES];
    });
}

- (void)adjustTermIDFromSemester {
    
    if(yearTermDict == nil || selected_year == nil) {
        return;
    }
    
    NSArray *yearSemesters = [yearTermDict objectForKey:selected_year];
   
    
    for(int i=0; i<yearSemesters.count; i++) {
        YearTermModel *model = [yearSemesters objectAtIndex:i];
        
        if([selected_semester isEqualToString:@"ทั้งหมด"]) {
            
        }
        else if([selected_semester isEqualToString:model.termName]) {
            
            select_term_id = model.termID;
//            [self.delegate applyFilter:selected_year semester:selected_semester termID:select_term_id];
            
            break;
        }
    }
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Manage dialog data

// extract year from yeartermdict
- (void)initializeYearData {
    
    if(yearTermDict == nil) {
        return;
    }
    
    if(yearList != nil && yearList.count != 0) {
        [yearList removeAllObjects];
    }
    
    yearList = [[NSMutableArray alloc] initWithArray:[yearTermDict allKeys]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
    [yearList sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    // Make first year in array as default
    selected_year = [yearList objectAtIndex:0];
    
}

- (void)initializeSemesterData {
    
    if(yearTermDict == nil || selected_year == nil) {
        return;
    }
    
    if(semesterList != nil && semesterList.count != 0) {
        [semesterList removeAllObjects];
    }
    
    NSArray *yearSemesters = [yearTermDict objectForKey:selected_year];
    NSMutableArray *semesters = [[NSMutableArray alloc] init];
    
    NSMutableArray *allDatesInYear = [[NSMutableArray alloc] init];
    
//    NSMutableArray *semesterID = [[NSMutableArray alloc] init];
    
    for(int i=0; i<yearSemesters.count; i++) {
        YearTermModel *model = [yearSemesters objectAtIndex:i];
        //        NSNumber *semesterNO = [[NSNumber alloc] initWithInt:[model.termName intValue]];
        //        [semesters addObject:semesterNO];
        
        [semesters addObject:model.termName];
        
//        [semesterID addObject:model.termID];
        
        [allDatesInYear addObject:model.termBegin];
        [allDatesInYear addObject:model.termEnd];
    }
    
    //    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    //    [semesters sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    semesterList = [[NSMutableArray alloc] initWithCapacity:semesters.count + 1];
    [semesterList addObject:@"ทั้งหมด"];
    
    for(int i=0; i < semesters.count; i++) {;
        NSString *semesterStr = [[NSString alloc] initWithFormat:@"%@", [semesters objectAtIndex:i]];
        [semesterList addObject:semesterStr];
    }
    
    
    
    // Make the first order in semesterList as default
    selected_semester = [semesterList objectAtIndex:0];
    
}

- (IBAction)onPressOK:(id)sender {
    
    [self dismissDialog];
    
    [self.delegate applyFilter:selected_year semester:selected_semester termID:select_term_id];
    
}

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    [self doDesignLayout];
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    self.titleLabel.text = title;
    
    self.isShowing = YES;
    
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
    
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

- (void)YearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict {
    yearTermDict = dataDict;
    
    [self initializeYearData];
    [self initializeSemesterData];
}

@end
