//
//  TELevelClassViewController.m
//  JabjaiApp
//
//  Created by toffee on 10/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//
@import AirshipKit;
#import "TELevelClassViewController.h"
#import "UserData.h"
#import "TableListDialog.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import "TEClassRoomIndividualViewController.h"
#import "Utils.h"
#import "KxMenu.h"
#import "UserInfoViewController.h"


@interface TELevelClassViewController (){
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    UnAuthorizeMenuDialog *unAuthorizeMenuDialog;
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    NSBundle *myLangBundle;
    CAGradientLayer *gradient;
    NSMutableArray *authorizeSubMenuArray;
}
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (strong, nonatomic) CallTEGetStudentInClassAPI *callTEGetStudentInClassAPI;
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end
static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";

@implementation TELevelClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    if ([UserData getUserType] != STUDENT) {
        [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//call to authorize menu
    }
    
    self.classLevelTextField.delegate = self;
    self.classRoomTextField.delegate = self;
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    [self.menuButton addTarget:self action:@selector(showRightMenu:) forControlEvents:UIControlEventTouchUpInside];
    [self setLanguage];
    [self getSchoolClassLevel];
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    
    //set color header
    [self.headerView layoutIfNeeded];
    gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    //set color button next
    [self.nextBtn layoutIfNeeded];
    gradient = [Utils getGradientColorNextAtion];
    gradient.frame = self.nextBtn.bounds;
    [self.nextBtn.layer insertSublayer:gradient atIndex:0];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section2.layer setShadowOpacity:0.3];
    [self.section2.layer setShadowRadius:3.0];
    [self.section2.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.nextBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.nextBtn.layer setShadowOpacity:0.3];
    [self.nextBtn.layer setShadowRadius:10.0];
    [self.nextBtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
}

-(void)setLanguage{
    if(self.mode == 1){
        self.menuButton.hidden = NO;
        self.menuImage.hidden = NO;
        self.haedText.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ATTENDANCE",nil,myLangBundle,nil);
    }else if(self.mode == 2){
        self.menuImage.hidden = YES;
        self.menuButton.hidden = YES;
        self.haedText.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_EDIT_ATTENDANCE",nil,myLangBundle,nil);
    }
    _classLevelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_CLASSLAVEL",nil,myLangBundle,nil);
     _classRoomLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_CLASSROOM",nil,myLangBundle,nil);
    _classLevelTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_FLAG_CLASSLAVEL",nil,myLangBundle,nil);
    _classRoomTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_FLAG_CLASSROOM",nil,myLangBundle,nil);
    [_nextBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_FLAG_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

///check authorize menu
- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
    [self showIndicator];
    // [self popUpLoaddin];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}

- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
    
        if ((resCode == 300 || resCode == 200) && success ) {
            
            authorizeSubMenuArray = [[NSMutableArray alloc] init];
            if (data != NULL) {
                for (int i = 0;i < data.count; i++) {
                    NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
                    for (int j = 0; j < subMenuArray.count; j++) {
                        NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
                        [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
                    }
                }
            }
            NSString* notPermissionText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_UNAUTHORRIZE",nil,myLangBundle,nil);
            NSString* pleaseContactText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_PLEASECONTACT",nil,myLangBundle,nil);
            NSString *alertMessage = [NSString stringWithFormat:@"%@\n%@",notPermissionText,pleaseContactText];
            if(self.mode == 1){
                if (![authorizeSubMenuArray containsObject: @"3"]) {
                    [self showAlertDialogAuthorizeMenu:alertMessage];
                    NSLog(@"ไม่มีสิทธิ์");
                }
            }else{
                if (![authorizeSubMenuArray containsObject: @"10"]) {
                    [self showAlertDialogAuthorizeMenu:alertMessage];
                    NSLog(@"ไม่มีสิทธิ์");
                }
            }
        }else{
            self.dbHelper = [[UserInfoDBHelper alloc] init];
            [self.dbHelper deleteUserAllWithMasterId:[UserData getMasterUserID]];

            // Logout
            [UserData setUserLogin:NO];
            [UserData saveMasterUserID:0];
            [UserData saveUserID:0];
            [UserData setUserType:USERTYPENULL];
            [UserData setAcademyType:ACADEMYTYPENULL];
            [UserData setFirstName:@""];
            [UserData setLastName:@""];
            [UserData setUserImage:@""];
            [UserData setGender:MALE];
            [UserData setClientToken:@""];
            LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
            [self.slideMenuController changeMainViewController:viewController close:YES];
        }
}

- (void)onAlertDialogClose {
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (void)getSchoolClassLevel {
    [self showIndicator];
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        NSLog(@"CLASSLAVEL = %@",classLevelStringArray);
    }
}
- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    [self showIndicator];
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}
#pragma mark - CallGetStudentClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classroomArray = data;
        
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {  
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showAlertDialogAuthorizeMenu:(NSString *)message {
    if(unAuthorizeMenuDialog != nil && [unAuthorizeMenuDialog isDialogShowing]) {
        [unAuthorizeMenuDialog dismissDialog];
    }
    else {
        unAuthorizeMenuDialog = [[UnAuthorizeMenuDialog alloc] init];
        unAuthorizeMenuDialog.delegate = self;
    }
    [unAuthorizeMenuDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // User press class level textfield
        
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_PLEASE_SELECT_CLASS",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }
        
        return NO;
    }
    
    return YES;
}
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_PLEASE_SELECT_CLASS",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classRoomTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_PLEASE_SELECT_CLASSROOM",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}
#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
        
        //        if(index == _selectedClassLevelIndex) {
        //            return;
        //        }
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classRoomTextField.text = @"";
        _selectedClassroomIndex = -1;
        _classLevelId = 0;
        
        // get classroom data with class level
        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:_classLevelId];
        
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
       // self.level2Id = [[NSMutableArray alloc] init];
        
        if(index == _selectedClassroomIndex) {
            return;
        }
        
        self.classRoomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        _classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        // addID Room
       // [self.level2Id addObject:@(_classroomId)];
        
    }
}

- (void)gotoSelectStudentsPage {
  
    TEClassRoomIndividualViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TEClassRoomIndividualStoryboard"];
    viewController.classroomId = self.classroomId;
    viewController.mode = self.mode;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (void)getStudentInClassroom {
    
    if(self.callTEGetStudentInClassAPI != nil) {
        self.callTEGetStudentInClassAPI = nil;
    }
    self.callTEGetStudentInClassAPI = [[CallTEGetStudentInClassAPI alloc] init];
    self.callTEGetStudentInClassAPI.delegate = self;
    [self.callTEGetStudentInClassAPI call:[UserData getSchoolId] classroomId:self.classroomId teacherId:[UserData getUserID]];
}
#pragma mark - API Delegate

- (void)callTEGetStudentInClassAPI:(CallTEGetStudentInClassAPI *)classObj data:(NSArray<TAStudentStatusModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _selectedStudentArray = data;
    }
    
    if(_selectedStudentArray.count != 0){
        if ([self doChackduplicate1]) {
            [self gotoSelectStudentsPage];
        };
    }else if(_selectedStudentArray.count == 0){
         [self gotoSelectStudentsPage];
        
    }
   
    
}

- (BOOL)doChackduplicate1 {
    BOOL status = NO;
    BOOL alreadyCheck = NO;
    for(TAStudentStatusModel *model in _selectedStudentArray) {
        if([model getStatusCheck] == YES ) {
            alreadyCheck = YES;
            break;
        }
        
    }
    
    if(_mode == TE_MODE_CHECK) {
        if (alreadyCheck == YES) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_CHECKED",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
            status = NO;
        }else{
            status = YES;
        }
      
    }
    else if(_mode == TE_MODE_EDIT ) {
        
        if (!alreadyCheck) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_UNCHECK",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
            status = NO;
        }else{
            status = YES;
        }
      
    }
    

    return status;
}
- (void)doChackduplicate {
    [self getStudentInClassroom];
}
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}
//- (void)showRightMenu:(UIButton *)sender{
//
//}

- (void)showRightMenu:(UIButton *)sender{

    NSArray *menuItems =
    @[
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_MENU_SCANER_ONTIME",nil,[Utils getLanguage],nil)
                     image:nil
                    target:self
                    action:@selector(doScannerOnTimeQRcode:)],
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_MENU_SCANER_LATE",nil,[Utils getLanguage],nil)
                     image:nil
                    target:self
                    action:@selector(doScannerQRcode:)],
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_MENU_IDUSER",nil,[Utils getLanguage],nil)
                     image:nil
                    target:self
                    action:@selector(doAccountStudent:)],


      ];
    const CGFloat W = self.view.bounds.size.width;
    const CGFloat H = self.view.bounds.size.height;
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(W - 16, 18, 40, 40) menuItems:menuItems];
}

- (void)doAccountStudent:(id)sender{
    TECheckLateWithIdStudentViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TECheckLateWithIdStudentStoryboard"];
    viewController.mode = self.mode;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (void)doScannerQRcode:(id)sender{
    TECheckLateScanerQrCodeViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TECheckLateScanerQrCodeStoryboard"];
    viewController.mode = self.mode;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (void)doScannerOnTimeQRcode:(id)sender{
    TECheckOnTimeScanerQrCodeViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TECheckOnTimeScanerQrCodeStoryboard"];
    viewController.mode = self.mode;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (IBAction)actionNext:(id)sender {
    if([self validateData]) {
        [self doChackduplicate];
    }
}

- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
//    [self.revealViewController revealToggle:self.revealViewController];
}
@end
