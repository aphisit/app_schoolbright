//
//  NFNewFutureSlideShowViewController.m
//  JabjaiApp
//
//  Created by toffee on 21/5/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "NFNewFutureSlideShowViewController.h"

@interface NFNewFutureSlideShowViewController (){
    NSMutableArray *imageArray;
    NSTimer *timer;
    NSInteger currentIndex;
}
@property (nonatomic, assign) BOOL isShowing;
@end

@implementation NFNewFutureSlideShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([NFNewFutureSlideShowCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    //[self.collectionView layoutIfNeeded];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    imageArray = [[NSMutableArray alloc] init];
    
    NSString *imageUrlString1 = @"https://i.pinimg.com/736x/51/14/2e/51142e6d11332abb7a2769372587e97c.jpg";
    NSString *encodedImageUrlString1 = [imageUrlString1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *imageURL1 = [NSURL URLWithString: encodedImageUrlString1];
    UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL1]];

    NSString *imageUrlString2 = @"https://i.pinimg.com/564x/c7/78/df/c778dfefc3e104a6217bdee7db3ff6b7.jpg";
    NSString *encodedImageUrlString2 = [imageUrlString2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *imageURL2 = [NSURL URLWithString: encodedImageUrlString2];
    UIImage *img2 = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL2]];
//    UIImage *img1 = [UIImage imageNamed:@"ic_ontime_pig"];
//    UIImage *img2 = [UIImage imageNamed:@"ic_absence"];
//    UIImage *img3 = [UIImage imageNamed:@"ic_personal_pig"];
    [imageArray addObject:img1];
    [imageArray addObject:img2];
//    [imageArray addObject:img3];
    self.isShowing = NO;

}

- (void)showNewFutureSlideShow:(UIView *)targetView{

    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [self.contentView setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.0]];
    [self.collectionView setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.0]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.contentView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentView setAlpha:1.0];
    [UIView commitAnimations];
     self.isShowing = YES;
     [self.collectionView layoutIfNeeded];
}

- (IBAction)leftAction:(id)sender {
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.pageControl.currentPage-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
}

- (IBAction)rightAction:(id)sender {
    if (self.pageControl.currentPage < imageArray.count-1) {
         [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.pageControl.currentPage+1 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
    }
}

- (IBAction)closeAction:(id)sender {
    [self dismissDialog];
}

- (void) dismissDialog{
    [self removeDialogFromView];
    
//    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogClose)]) {
//        [self.delegate onAlertDialogClose];
//    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

-(void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentView setAlpha:0.0];
    
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

#pragma mark - collection view data soure
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    self.pageControl.numberOfPages = imageArray.count;
    return imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NFNewFutureSlideShowCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.images.image = [imageArray objectAtIndex:indexPath.row];
    cell.images.contentMode = UIViewContentModeScaleAspectFit;
    return cell;
}

-(CGSize)collectionView:(UICollectionView*)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    [self.collectionView layoutIfNeeded];
    NSLog(@"collection H = %f",self.collectionView.frame.size.height);
    NSLog(@"collection W = %f",self.collectionView.frame.size.width);
    return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.collectionView layoutIfNeeded];
    currentIndex = scrollView.contentOffset.x / self.collectionView.frame.size.width;
    NSLog(@"%@", [@(currentIndex) stringValue]);
    self.pageControl.currentPage = currentIndex;
}
@end
