//
//  BSBehaviorScoreRadioTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSBehaviorScoreRadioTableViewCell.h"

@implementation BSBehaviorScoreRadioTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setRadioButtonSelected {
    [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
}

- (void)setRadioButtonSelectedWithType:(int) type {
    
    if(type == RADIO_TYPE_RED) {
        [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_red"] forState:UIControlStateNormal];
    }
    else {
        [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
    }
}

- (void)setRadioButtonClearSelected {
    [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_clear"] forState:UIControlStateNormal];
}

- (IBAction)actionPressRadioButton:(id)sender {
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onPressRadioButton:atIndex:)]) {
        [self.delegate onPressRadioButton:self atIndex:_index];
    }
}
@end
