//
//  MGBillSummaryViewController.m
//  JabjaiApp
//
//  Created by toffee on 5/16/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "MGBillSummaryViewController.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "AlertDialogConfirm.h"
#import "MGPasscodeViewController.h"
#import "Utils.h"

@interface MGBillSummaryViewController (){
    AlertDialogConfirm *alertDialog;
    AlertDialog *alertDialogNoSecess;
    MGListMenuDialog *mgListMenuDialog;
    ConfirmPinCodeViewController *confirmPinCodeViewController;
    NSString *statusSubmit;
    NSBundle *myLangBundle;
}


@end
static NSString *cellIdentifier = @"MargetSummaryCell";
@implementation MGBillSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   

    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    statusSubmit = @"SUMMIT";
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MargetBillSummaryTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self setLanguage];
    [self setDetailBill];
    // Do any additional setup after loading the view.
}

- (void)viewDidLayoutSubviews{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerPaymentbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerPaymentbtn.bounds;
    [self.headerPaymentbtn.layer insertSublayer:gradientNext atIndex:0];
}

-(void)setLanguage{
    self.headerShopLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_SHOPPING", nil, myLangBundle, nil);
    self.headerPayMentLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_PAYMENT", nil, myLangBundle, nil);
    self.headerCustomerLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_CUSTOMER", nil, myLangBundle, nil);
    self.headerSellerLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_SELLER", nil, myLangBundle, nil);
    self.headerDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_DATE", nil, myLangBundle, nil);
    self.headerAmountLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_AMOUNT", nil, myLangBundle, nil);
    [self.headerPaymentbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_MARKET_PAYMENT", nil, myLangBundle, nil) forState:UIControlStateNormal];
    
}

-(void)setDetailBill{
   
    [self.firstName setText:[UserData getFirstName]];
    [self.lastName setText:[UserData getLastName]];
    [self.senderImage layoutIfNeeded];
    if ([[UserData getUserImage] isEqualToString: @""] ) {
        self.senderImage.image = [UIImage imageNamed:@"ic_user_info"];
    }else{
        NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [UserData getUserImage]]];
        [self.senderImage setImage:[UIImage imageWithData:imageData]];
        self.senderImage.layer.cornerRadius = self.senderImage.frame.size.height /2;
        self.senderImage.layer.masksToBounds = YES;
        self.senderImage.layer.borderWidth = 0;
    }
    [self.nameShopLabel setText:_nameShop];
    NSString *display = [NSNumberFormatter localizedStringFromNumber:@(_totalPrice)
                                                         numberStyle:NSNumberFormatterDecimalStyle];
    [self.totalMonny setText:[NSString stringWithFormat:@"%@ %@",display,NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_BAHT", nil, myLangBundle, nil)]];
    
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"HH:mm: น. dd/MM/yyyy "];
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [dateFormatter setDateFormat:@"HH:mm น. dd/MM/yyyy"];
    }else{
        [dateFormatter setDateFormat:@"HH:mm dd/MM/yyyy"];
    }
    
    _dateTimeLabel.text = [dateFormatter stringFromDate:[NSDate date]];
    
}
-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
    if(_orderArray != nil) {
        return _orderArray.count;
    }
    else {
        return 0;
    }
    
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
    MargetBillSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.listNameLabel.text = [_orderArray objectAtIndex:indexPath.row];
    cell.priceLabel.text = [_totalPriceOrderArray objectAtIndex:indexPath.row];
    cell.amountLabel.text = [_amountOrderArray objectAtIndex:indexPath.row];
    return  cell;
    
}
-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return 40.5;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
    if(mgListMenuDialog != nil) {
        if([mgListMenuDialog isDialogShowing]) {
            [mgListMenuDialog dismissDialog];
        }
    }
    else {
        mgListMenuDialog = [[MGListMenuDialog alloc] init];
        mgListMenuDialog.delegate = self;
    }
    [mgListMenuDialog showDialogInView:self.view listMenu:_orderArray lisiPrice:_totalPriceOrderArray listAmount:(NSArray *)_amountOrderArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    if(alertDialog != nil) {
        if([alertDialog isDialogShowing]) {
            [alertDialog dismissDialog];
        }
    }
    else {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showAlertDialogNoSecess:(NSString *)message {
    if(alertDialogNoSecess != nil && [alertDialogNoSecess isDialogShowing]) {
        [alertDialogNoSecess dismissDialog];
    }
    else {
        alertDialogNoSecess = [[AlertDialog alloc] init];
        alertDialogNoSecess.delegate = self;
    }
    [alertDialogNoSecess showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}
- (void)onAlertDialogClose {
    _summitBtn.enabled = YES;
    [_headerPayMentLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_RECEIPT", nil, myLangBundle, nil)];
    statusSubmit = @"MOVEBACK";
    
    [_summitBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_MARKET_BACK_PAGE", nil, myLangBundle, nil) forState:UIControlStateNormal];
}

//moveback page confirmcodeviewcontroller
-(void)onAlertPinMoveBack{
    _summitBtn.enabled = YES;
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

//go page ConfirmPinCodeViewController
-(void)confirmPin{
    if(confirmPinCodeViewController != nil) {
        if([confirmPinCodeViewController isDialogShowing]) {
            [confirmPinCodeViewController dismissDialog:nil];
        }
    }
    else {
        confirmPinCodeViewController = [[ConfirmPinCodeViewController alloc] init];
        confirmPinCodeViewController.delegate = self;
    }
    [confirmPinCodeViewController showDialogInView:self.view productJsonString:_orderStringJson shopId:_shopId];
}

#pragma mark - MGPasscodeViewController
//return from ConfirmPinCodeViewController
-(void)successConfirmPin:(NSString *)status{
    [self stopIndicator];
//    if ([status isEqualToString:@"Low Money"]) {
//            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_MAKET_NO_MONNY", nil, myLangBundle, nil);
//            [self showAlertDialogNoSecess:alertMessage];
//    }else{
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_MAKET_SUCCESS", nil, myLangBundle, nil);
            [self showAlertDialogWithMessage:alertMessage];
  //  }
}
- (void)addPinCode{
    MGPasscodeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGPasscodeStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (IBAction)actionSubmit:(id)sender {
    _summitBtn.enabled = NO;
    if ([statusSubmit isEqualToString:@"SUMMIT"]) {
         [self confirmPin];
    }else{
        MGListMargetViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGListMargetViewStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
}

- (IBAction)moveBack:(id)sender {
    MGListMenuViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGListMenuViewStoryboard"];
    viewController.shopId = _shopId;
    viewController.nameShop = _nameShop;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
