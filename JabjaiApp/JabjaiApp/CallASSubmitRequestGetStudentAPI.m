//
//  CallASSubmitRequestGetStudentAPI.m
//  JabjaiApp
//
//  Created by Mac on 13/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallASSubmitRequestGetStudentAPI.h"
#import "APIURL.h"
#import "Utils.h"
@implementation CallASSubmitRequestGetStudentAPI{
    NSInteger connectCounter;
    NSString *status;
}
- (void)call:(long long)schoolID studentID:(long long)studentID{
    connectCounter = 0;
    [self getStatusGetStudent:schoolID studentID:studentID];
}
-(void)getStatusGetStudent:(long long)schoolID studentID:(long long)studentID{
    
    NSString *URLString = [APIURL getStatusGetStudent:schoolID studentID:studentID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }

            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;

                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getStatusGetStudent:schoolID studentID:studentID];

            }
            else {
                isFail = YES;
                self->connectCounter = 0;
            }

        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getStatusGetStudent:schoolID studentID:studentID];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
                
            }
            else {
               
                if (![[returnedData objectForKey:@"status"] isKindOfClass:[NSNull class]]) {
                    self->status = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"status"]];
                }
                else{
                    self->status = [[NSMutableString alloc] initWithString:@""];
                }

                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callASSubmitRequestGetStudentAPI:status:success:)]) {
                    [self.delegate callASSubmitRequestGetStudentAPI:self status:self->status success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callASSubmitRequestGetStudentAPI:status:success:)]) {
                [self.delegate callASSubmitRequestGetStudentAPI:self status:self->status success:NO];
            
            }
        }
     
        
    }
     ];
    
}

@end
