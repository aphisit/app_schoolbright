//
//  SNClassLevelGroupViewController.m
//  JabjaiApp
//
//  Created by toffee on 9/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNClassLevelGroupViewController.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "TableListDialog.h"
#import "Constant.h"
#import "SNLevelStudentViewController.h"
#import "SNClassRoomIndividualViewController.h"
#import "Utils.h"

@interface SNClassLevelGroupViewController (){
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    NSBundle *myLangBundle;
}
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;

@end
static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";


@implementation SNClassLevelGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    self.classLavelTextField.delegate = self;
    self.classRoomTextField.delegate = self;
 
    [self setLanguage];
    [self getSchoolClassLevel];

}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section2.layer setShadowOpacity:0.3];
    [self.section2.layer setShadowRadius:3.0];
    [self.section2.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

-(void)setLanguage{
    
    self.headerSendNewsLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS",nil,myLangBundle,nil);
    self.headerClasslevelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_CLASSLEVEL",nil,myLangBundle,nil);
    self.headerClassLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_CLASS",nil,myLangBundle,nil);
    self.classLavelTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_SENDNEWS_CLASSLEVEL",nil,myLangBundle,nil);
     self.classRoomTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_SENDNEWS_CLASS",nil,myLangBundle,nil);
    
    [_headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(textField.tag == 1) { // User press class level textfield
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        if(self.classLavelTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_CLASSLEVEL",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getSchoolClassLevel {
    [self showIndicator];
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
     //NSLog(@"getSchoolig = %@",[UserData getSchoolId] );
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    [self showIndicator];
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        NSLog(@"CLASSLAVEL = %@",classLevelStringArray);
    }
}

#pragma mark - CallGetStudentClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classroomArray = data;
        
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}
- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
        self.classLavelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        // Clear dependency text field values
        self.classRoomTextField.text = @"";
        _selectedClassroomIndex = -1;
        _classLevelId = 0;
        // get classroom data with class level
        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:_classLevelId];
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        self.level2Id = [[NSMutableArray alloc] init];
        if(index == _selectedClassroomIndex) {
            return;
        }
        self.classRoomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        _classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        // addID Room
        [self.level2Id addObject:@(_classroomId)];
    }
}

- (BOOL)validateData {
    if(self.classLavelTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_CLASSLEVEL",nil,myLangBundle,nil);;
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classRoomTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_CLASSROOM",nil,myLangBundle,nil);;
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    return YES;
}

- (void)gotoSelectStudentsPage {
    NSLog(@"level2Id = %@",self.level2Id);
   SNClassRoomIndividualViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNClassRoomIndividualStoryboard"];
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    viewController.level2id = self.level2Id;
    viewController.sendType = self.sendtype;
    viewController.newsType = self.newstype;
    viewController.sendGroup = self.sendgroup;
    [self.slideMenuController changeMainViewController:viewController close:YES];

}
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)actionNext:(id)sender {
    if([self validateData]) {
        [self gotoSelectStudentsPage];
    }
}

- (IBAction)backToMenu:(id)sender {
    SNLevelStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SendNewsStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}


@end
