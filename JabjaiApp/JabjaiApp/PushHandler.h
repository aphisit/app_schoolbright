//
//  PushHandler.h
//  JabjaiApp
//
//  Created by mac on 12/6/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

@import AirshipKit;
#import <Foundation/Foundation.h>

#import "SlideMenuController.h"

@interface PushHandler : NSObject <UAPushNotificationDelegate>

@end
