//
//  TEPadNumberCheckNameFinishSchoolViewController.m
//  JabjaiApp
//
//  Created by toffee on 30/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "TEPadNumberCheckNameFinishSchoolViewController.h"
#import "Utils.h"
#import "UserData.h"
@interface TEPadNumberCheckNameFinishSchoolViewController (){
    NSMutableString *idStudentStr ;
   // ConfirmScanerLateViewController *dialogConfirmScaner;
//    ConfirmScanerLateViewController *dialogConfirmScaner;
    AlertDialogConfirm *alertDialogNoUser;
    AlertDialog *alertDialog;
}
@property (strong, nonatomic) CallTEGetDataStudentScanBarcodeAPI *callTEGetDataStudentScanBarcodeAPI;
//@property (strong, nonatomic) CallTEConfirmScanerBarcode *callTEConfirmScanerBarcode;
@end

@implementation TEPadNumberCheckNameFinishSchoolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     idStudentStr = [NSMutableString string];
    self.headerFinishSchoolLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_NUMBER_FINISH_SCHOOL",nil,[Utils getLanguage],nil);
    self.idStudentTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_FLAG_SPECIFY",nil,[Utils getLanguage],nil);
    [self.nextBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_FLAG_NEXT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    self.idStudentTextField.enabled = NO;
   
    //set color button next
    [self.nextBtn layoutIfNeeded];
    CAGradientLayer *gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.nextBtn.bounds;
    [self.nextBtn.layer insertSublayer:gradientNext atIndex:0];
    
    [self.nextBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.nextBtn.layer setShadowOpacity:0.3];
    [self.nextBtn.layer setShadowRadius:10.0];
    [self.nextBtn.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

#pragma CallTEGetDataStudentScanBarcodeAPI
//call get data student
- (void)doCallTEGetDataStudentScanBarcodeAPI:(long long)schoolId idStudent:(NSString *)idStudent {
    [self showIndicator];
    if(self.callTEGetDataStudentScanBarcodeAPI != nil) {
        self.callTEGetDataStudentScanBarcodeAPI = nil;
    }
    self.callTEGetDataStudentScanBarcodeAPI = [[CallTEGetDataStudentScanBarcodeAPI alloc] init];
    self.callTEGetDataStudentScanBarcodeAPI.delegate = self;
    [self.callTEGetDataStudentScanBarcodeAPI call:schoolId idStudent:idStudent];
}
-(void)callTEGetDataStudentScanBarcodeAPI:(CallTEGetDataStudentScanBarcodeAPI *)classObj data:(TEDataStudentScanerModel *)data resCode:(NSInteger)resCode success:(BOOL)success{
    [self stopIndicator];
    if (success && resCode == 200) {
        if (data != nil) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(replyIdStudent:studentCode:mode:success:)]) {
                [self.delegate replyIdStudent:data studentCode:idStudentStr mode:self.mode success:YES];
            }
        }else{
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_USER_NOTFOUND",nil,[Utils getLanguage],nil);
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseAleatMessage:mode:)]) {
                [self.delegate responseAleatMessage:alertMessage mode:self.mode];
            }
        }
    }else{
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_USER_NOTFOUND",nil,[Utils getLanguage],nil);
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseAleatMessage:mode:)]) {
                       [self.delegate responseAleatMessage:alertMessage mode:self.mode];
        }
    }
}

- (IBAction)numberButton:(UIButton *)sender {
    if (sender.tag < 10) {
        [idStudentStr appendFormat:@"%@",[@(sender.tag) stringValue]];
    }else if (sender.tag>10){
        [self onAlertDialogClose];
    }
    else{
        NSUInteger characterCount = [idStudentStr length];
        if (characterCount > 0){
              [idStudentStr deleteCharactersInRange:NSMakeRange(characterCount-1, 1)];
        }
    }
    self.idStudentTextField.text = idStudentStr;
    NSLog(@"xxxx = %@",idStudentStr);
}

//retrue from dialog confirm
- (void)onAlertDialogClose{
    idStudentStr = [NSMutableString string];
    self.idStudentTextField.text = idStudentStr;
}

- (IBAction)nextAction:(id)sender {
    if (idStudentStr.length > 0) {
        [self doCallTEGetDataStudentScanBarcodeAPI:[UserData getSchoolId] idStudent:idStudentStr];
    }else{
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_ENTER_ID",nil,[Utils getLanguage],nil);
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(responseAleatMessage:mode:)]) {
            [self.delegate responseAleatMessage:alertMessage mode:self.mode];
        }
    }
}


// Show the indicator
- (void)showIndicator {
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

@end
