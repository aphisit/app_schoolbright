//
//  APIURL.h
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.

#import <Foundation/Foundation.h>
#import "UserData.h"

static NSString *CALLAPI = @"https://api.schoolbright.co";
static NSString *CALLSHOPAPI = @"https://shopapi.schoolbright.co";

//static NSString *CALLAPI = @"https://api-dev.schoolbright.co";//test
//static NSString *CALLSHOPAPI = @"https://memory-shop.schoolbright.co";//test
////static NSString *CALLSHOPAPI = @"https://shopapi-test.schoolbright.co";//test

//static NSString *CALLAPI = @"https://api-demo.schoolbright.co";//test
//static NSString *CALLSHOPAPI = @"https://shopapi-test.schoolbright.co";//test

@interface APIURL : NSObject
+(NSString *)getLoginURL:(NSString *)username password:(NSString *)password schoolId:(long long)schoolId;//
+(NSString *)getUpdatePassword:(NSString *)newPassword userId:(long long)userId token:(NSString *)token schoolid:(long long)schoolid;
+(NSString *)getSchoolDataWithPasswordURL:(NSString *)password;
+(NSString *)getListAllSchool;
+(NSString *)getProvinceURL;
+(NSString *)getDistrictURL:(NSInteger)provinceID;
+(NSString *)getSubDistrictURL:(NSInteger)districtID;
+(NSString *)getCreditBureauSummaryURL;//
+(NSString *)getYearAndTermURL;//
+(NSString *)getAttendSchoolDataURLWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status schoolid:(long long)schoolid;//
+(NSString *)getMessageInboxURLWithLatestID:(NSInteger)latestID messageType:(NSInteger)messageType schoolid:(long long)schoolid;//
+(NSString *)getAllMessageWithPage:(NSUInteger)page userID:(long long)userID status:(NSString*)status schoolId:(long long)schoolId;//
+(NSString *)getNewsMessageWithPage:(NSUInteger)page userID:(long long)userID schoolid:(long long)schoolid;//
+(NSString *)getUpdateReadMessageURLWithMessageID:(long long)messageID userID:(long long)userID schoolid:(long long)schoolid;//
+(NSString *)getChangeFingerprintRequestURL;//
+(NSString *)getUserCredentialURL;//
+(NSString *)getStudentScheduleListURLWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//
+(NSString *)getStudentScheduleListDetailURLWithUserID:(long long)userID date:(NSDate *)date subjectID:(long long)subjectID schoolid:(long long)schoolid;//
+(NSString *)getTeacherScheduleListDetailURLWithUserID:(long long)userID date:(NSDate *)date subjectID:(long long)subjectID schoolid:(long long)schoolid;//
+(NSString *)getBoughtItemWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//
+(NSString *)getTopupMessageWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//
+(NSString *)getUnreadMessageCountWithUserID:(long long)userID schoolid:(long long)schoolid;//
+(NSString *)getFeedBackPOSTURL;//
+(NSString *)getSNNewsPOSTURL;//
+(NSString *)getHomeWorkPOSTURL;//
+(NSString *)getStatusTakeEvent:(long long)schoolId;//
+(NSString *)getAuthorizeMenu:(long long)userId clientToken:(NSString *)clientToken schoolid:(long long)schoolid;
+(NSString *)getAmountUnreadLeave:(long long)userId schoolid:(long long)schoolid;
+(NSString *)getStatusInsertDataOfNotification;
+(NSString *)getStatusRemoveDataOfNotification;
+(NSString *)getStatusClearDataOfNotification:(NSString*)token;


// Take Class Attendance
+(NSString *)getStudentClassLevelWithSchoolId:(long long)schoolId;//
+(NSString *)getSchoolClassroomWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId;//
+(NSString *)getSubjectWithSchoolId:(long long)schoolId classroomId:(long long)classroomId date:(NSString*)date;//
+(NSString *)getStudentInClassWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId date:(NSString*)date;//
+(NSString *)getPreviousAttendClassStatusWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId;//
+(NSString *)getUpdateStudentStatusPOSTWithSchoolId:(long long)schoolId subjectId:(long long)subjectId teacherId:(long long)teacherId date:(NSString*)date;//
+(NSString *)getTEUpdateStudentStatusPOSTWithSchoolId:(long long)schoolId teacherId:(long long)teacherId;//
+(NSString *)getTASubjectListWithSchoolId:(long long)schoolId classroomId:(long long)classroomId;//
+(NSString *)getTAHistoryStudentStatusListWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId date:(NSDate *)date ;//
+(NSString *)getTEHistoryStudentStatusListWithSchoolId:(long long)schoolId classroomId:(long long)classroomId  date:(NSDate *)date;//
+(NSString *)getTAHistoryDateStatusWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId startDate:(NSDate *)startDate endDate:(NSDate *)endDate;//
+(NSString *)getTAStatusCalendarOfReport:(long long)userID day:(NSString *)day subjectID:(long long)subjectID schoolID:(long long)schoolID classroomID:(long long)classroomID;
// Behavior Score
+(NSString *)getStudentInClassroomWithSchoolId:(long long)schoolId classroomId:(long long)classroomId;//
+(NSString *)getAddBehaviorScoreListWithSchoolId:(long long)schoolId;//
+(NSString *)getReduceBehaviorScoreListWithSchoolId:(long long)schoolId;//
+(NSString *)getUpdateStudentBehaviorScorePOSTWithSchoolId:(long long)schoolId teacherId:(long long)teacherId;//
+(NSString *)getBehaviorScoreHistoryInDateRangeWithSchoolId:(long long)schoolId classroomId:(long long)classroomId startDate:(NSDate *)startDate endDate:(NSDate *)endDate type:(NSInteger)type;//
+(NSString *)getBehaviroScoreHistoryForStudentReportWithUserId:(long long)userId schoolid:(long long)schoolid;//
+(NSString *)getImageNews:(long long)userID idMessage:(long long)idMessage schoolid:(long long)schoolid;//

// Executive report
+(NSString *)getEXReportSummaryAttendSchoolWithSchoolId:(long long)schoolId date:(NSDate *)date;//
+(NSString *)getEXReportClassroomAttendSchoolWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId date:(NSDate *)date;//
+(NSString *)getEXReportAttendSchoolTeacherListWithSchoolId:(long long)schoolId date:(NSDate *)date;//
+(NSString *)getEXReportAttendSchoolEmployeeListWithSchoolId:(long long)schoolId date:(NSDate *)date;//

+(NSString *)getUniversityFacultyURL:(NSInteger)schoolID;//
+(NSString *)getUniversityDegreeURL:(NSInteger)schoolID facultyID:(NSInteger)facultyID;//
+(NSString *)getUniversityMajorURL:(NSInteger)schoolID degreeID:(NSInteger)degreeID;//
+(NSString *)getStudentEnrollSubjectURL;//
+(NSString *)getAddSectionUrl:(NSInteger)sectionID schoolid:(long long)schoolid;//
+(NSString *)getDeleteSectionUrl:(NSInteger)sectionID schoolid:(long long)schoolid;//
+(NSString *)getSubjectAddedUrl;//
+(NSString *)getTeachingSubjectUrl;//
+(NSString *)getStudentsInSectionUrl:(NSInteger)sectionID schoolid:(long long)schoolid;//
+(NSString *)getStudentSubjectListAllYearUrl;//
+(NSString *)getAttendToSectionHistoryWithSectionID:(NSInteger)sectionID startDate:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status userType:(USERTYPE)userType schoolid:(long long)schoolid;//ตรวจสอบอีกที
+(NSString *)getTheacherAllUrl:(long long)schoolId;//
+(NSString *)getPersonnelAllUrl:(long long)schoolId;//

//News
+(NSString *)getUpdateSendNewsPOST;//
+ (NSString *)getStatusReplyCodeMessageBox:(long long)userID messageID:(long long)messageID replyCode:(int)replyCode schoolID:(long long)schoolID;


//Take Even/Finish school
+ (NSString *)getTakeEventStudentInClassroomWithSchoolId:(long long)schoolId classroomId:(long long)classroomId teacherId:(NSInteger)teacherId;//
+ (NSString *)getTEGetDataStudentScanBarcode:(long long)schoolId idStudent:(NSString *)idStudent;//
+ (NSString *)getTEConfirmScanerBarcode:(long long)schoolId userId:(long long)userId idCode:(NSString *)idCode;//
+ (NSString *)getTEScanerQROnTimeConfirm:(long long)schoolId userId:(long long)userId idCode:(NSString *)idCode;//
+ (NSString *)getTEFinishSchool:(long long)schoolId userId:(long long)userId idCode:(NSString *)idCode status:(NSString *)status;//

//JobHome
+ (NSString *)getJHSubjectWithSchoolId:(long long)schoolId classroomId:(long long)classroomId;//
+ (NSString *)getJHSubjectModeGroupWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId;//
+ (NSString *)getJHStudentInClassroomWithSchoolId:(long long)schoolId classroomId:(long long)classroomId;//
+(NSString *)getUpdateJobHomePOST;//

//TeacherTimeTable
//Calendar
+ (NSString *)getEventCalendarWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//
+ (NSString *)getEventCalendarListWithUserID:(long long)userID date:(NSDate *)date schoolId:(long long)schoolid; //

//TeacherTimetable
+ (NSString *)getTeacherScheduleListWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//
+ (NSString *)getTeacherTimeDetail:(long long)userId date:(NSDate*)date  ScheduleId:(long long)ScheduleId schoolid:(long long)schoolid;//

//Topup Month
+ (NSString *)getTopupMonthWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//

//Purchase Month
+ (NSString *)getBoughtItemMonthWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//

//Teacher Schedule Month
+ (NSString *)getTeacherScheduleMonthWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//

//Student Schedule Month
+ (NSString *)getStudentScheduleMonthWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//

//Leave Detail
+ (NSString *)getUserLeaveDetailWithUserID:(long long)userID schoolid:(long long)schoolid;//

//LeaveInbox
+ (NSString *)getLeaveInboxWithPage:(NSUInteger)page userID:(long long)userID schoolid:(long long)schoolid;//

//Cover Profile
//+(NSString *)getCoverProfileWithUserID:(long long)userID;

//Leave Report Daily
+ (NSString *)getLeaveReportDailyWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//

//Leave Report Month
+ (NSString *)getLeaveReportMonthlyWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//

//HomeWorkCalendar
+ (NSString *)getHomeworkCalendarWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//
+ (NSString *)getHomeworkCalendarListWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid;//

//HomeworkMessage
+ (NSString *)getHomeworkMessageWithPage:(NSUInteger)page userID:(long long)userID schoolid:(long long)schoolid;//

// 26/03/2561

//Calendar ReportBehavior
+ (NSString *)getCalendarReportBehaviorWithhUserID:(long long)userID date:(NSDate *)date classid:(long long)classid schoolid:(long long)schoolid;//

//SendLetterLeave Add Season
+ (NSString *)getSendLetterDataLeaveWithLeaveType:(NSString *)leaveType description:(NSString *)description dayStart:(NSDate *)dayStart dayEnd:(NSDate *)dayEnd userID:(long long)userID ampher:(NSString *)ampher homeNumber:(NSString *)homeNumber phone:(NSString *)phone province:(NSString *)province road:(NSString *)road tumbon:(NSString *)tumbon season:(NSInteger)season schoolid:(long long)schoolid;//

//Warning Department
+ (NSString *)getWarningWithUserID:(long long)userID schoolid:(long long)schoolid;//

//Fee / TuitionFees
+ (NSString *)getSchoolFeeListWithUserID:(long long)userID schoolid:(long long)schoolid;//
+ (NSString *)getSchoolFeeDetailWithUserID:(long long)userID InvoiceID:(long long)invoiceID schoolid:(long long)schoolid;//
+ (NSString *)getPaymentPrompayWebviewAPI:(long long)studentID amount:(double)amount invoiceID:(long long)invoiceID;
+ (NSString *)getPaymentCardWebviewAPI:(long long)studentID amount:(double)amount invoiceID:(long long)invoiceID;

//Send Image Leave
+ (NSString *)getSNLeavePOSTURL;

//ClassForStudent
+ (NSString *)getClassroomForStudentWithUserID:(long long)userID schoolid:(long long)schoolid;//

//PositionForTeacher
+ (NSString *)getPositionForTeacherWithUserID:(long long)userID schoolid:(long long)schoolid;//

//HealthDataForUser
+ (NSString *)getHealthDataWithUserID:(long long)userID schoolid:(long long)schoolid;//
+ (NSString *)getHealthHistory:(long long)userId schoolId:(long long)schoolId date:(NSString*)date;

//RequestLeaveInbox
+ (NSString *)getRequestLetterWithLetterID:(long long)letterID userID:(long long)userID approve:(NSInteger)approve schoolid:(long long)schoolid;//

//LetterLeaveDetail
+ (NSString *)getLetterLeaveDetailWithLetterID:(long long)letterID userID:(long long)userID schoolid:(long long)schoolid;//

//GetMessageToUser
+ (NSString *)getUserIdOfMessage:(long long)schoolId messageId:(long long)messageId userId:(NSString *)userId;//

//Report New Version

//ReportFee
+ (NSString *)getReportFeeWithUserID:(long long)userId termID:(NSString *)termId schoolid:(long long)schoolid;//

//ReportOrderHomeWork
+ (NSString *)getReportOrderHomeworkWithSchoolID:(long long)schoolId page:(NSUInteger)page;//
+ (NSString *)getReportOrderHomeworkDetailWithSchoolID:(long long)schoolId homeworkID:(long long)homeworkId;//
+ (NSString *)getReportOrderHomeworkRecieverWithSchoolID:(long long)schoolId homeworkID:(long long)homeworkId page:(NSUInteger)page status:(NSString*)status;//

//ReportOrderSendNews
+ (NSString *)getReportSendNewsListWithSchoolID:(long long)schoolId page:(NSUInteger)page sort:(NSString *)sort;//
+ (NSString *)getReportSendNewsDetailWithSchoolID:(long long)schoolId newsID:(long long)newsId;//
+ (NSString *)getReportSendNewsRecieverWithSchoolID:(long long)schoolId newsID:(long long)newsId page:(NSUInteger)page status:(NSString*)status;//

//Market
+ (NSString *)getMargetListName:(long long)schoolId;//
+ (NSString *)getListMenuFood:(long long)schoolId shopId:(long long)shopId;//
+ (NSString *)getUpdateOrder:(long long)schoolId shopId:(long long)shopId userId:(long long)userId;//
+ (NSString *)getCheckSignUpPin:(long long)userId schoolid:(long long)schoolid;//
+ (NSString *)getAddPinCode:(long long)userId pinCode:(NSString*)pinCode schoolid:(long long)schoolid;//
+ (NSString *)getConfirmPinCodePOST;//
+ (NSString *)getConfirmPinProductPOST:(long long)schoolId shopId:(long long)shopId;//

//UpdatePostLeave
+ (NSString *)getUpdateLeavePOST;//

//CancelLeave
+ (NSString *)getCancelLeaveWithLetterID:(long long)letterId UserID:(long long)userId schoolid:(long long)schoolid;//

//ResetPinCode
+ (NSString *)getUpdatePinCode;//

//ImageBarcode
+ (NSString *)getImageBarcodeString:(long long)high width:(long long)width userId:(long long)userId schoolid:(long long)schoolid;//
+ (NSString *)getImageQrcodeString:(long long)high width:(long long)width userId:(long long)userId schoolid:(long long)schoolid;//

//Report(New)
+ (NSString *)getDataRefillMoney:(long long)schoolId date:(NSString*)date;//
+ (NSString *)getDataRefillMoneyWeek:(long long)schoolId dayStart:(NSString*)dayStart dayEnd:(NSString*)dayEnd;//
+ (NSString *)getDataPurchasingDay:(long long)schoolId date:(NSString*)date;//
+ (NSString *)getDataPurchasingWeek:(long long)schoolId dayStart:(NSString*)dayStart dayEnd:(NSString*)dayEnd;//
+ (NSString *)getReportCheckFlagDateTime:(long long)shcoolId studentId:(NSString *)studentId date:(NSString *)date;//
+ (NSString *)getDataReportCheckSubjectDateTime:(NSString*)studentId date:(NSString *)date subjectId:(NSString*)subjectId schoolId:(long long)schoolId;//
+ (NSString *)getReportInOut:(long long)shcoolId date:(NSString *)date;//
+ (NSString *)getReportDataLevel:(long long)shcoolId date:(NSString *)date;//
+ (NSString *)getReportDataDepartmant:(long long)shcoolId date:(NSString *)date;//
+ (NSString *)getReportUnCheck:(long long)shcoolId date:(NSString *)date;//
+ (NSString *)getReportUnCheckListData:(long long)shcoolId date:(NSString *)date;//
+ (NSString *)getReportInOutStatus:(long long)shcoolId dayReports:(NSString *)dayReports levelId:(long long)levelId;//
+ (NSString *)getReportInOutStatusDepaetmant:(long long)shcoolId dayReports:(NSString *)dayReports depaetmanId:(long long)depaetmanId;//
//UserInfo
+ (NSString *)getOpenMessageDataAll:(long long)userId schoolid:(long long)schoolid ;//

//Top up
+ (NSString *)getTopUpMonney:(long long)userId amount:(NSString*)amount schoolid:(long long)schoolid;//
+ (NSString *)getMoveBackTopUp;
+ (NSString *)getQRCodePaymentOfKbank;

//AcademicResults
+ (NSString *)getDetailOfAcademicResults:(long long)schoolId studentId:(long long)studentId year:(NSString *)year term:(NSString*)term;//
+ (NSString *)getYearOfAcademicResults:(long long)schoolId;//
+ (NSString *)getTermOfAcademicResults:(long long)schoolId nYear:(NSInteger )nYear studentId:(long long)studentId;//
+ (NSString *)getStudentPermission:(long long) schoolId studentId:(long long)studentId ;//

//AcceptStudentsMaps
+ (NSString *)getLocationSchool:(long long)schoolID studentID:(long long)studentID;
+ (NSString *)getDistanceSatrtPointAndEndPoint:(NSString*)sourceLocation destinationLocation:(NSString*)destinationLocation;
+ (NSString *)getStatusGetStudent:(long long)schoolID studentID:(long long)studentID;

//ClosedForRenovation
+ (NSString *)getStatusServer;

//SendTokenAndLanguage
+ (NSString *)getStatusSendTokenAndLanguage:(NSString*)token language:(NSString*)language;



@end
