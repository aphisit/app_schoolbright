//
//  SNGroupClassroomTableViewController.m
//  JabjaiApp
//
//  Created by toffee on 9/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNGroupClassroomTableViewController.h"
#import "EXReportClassHeaderTableViewCell.h"
//#import "EXReportClassChildTableViewCell.h"
//#import "EXReportButtonTableViewCell.h"
#import "SNLevelStudentViewController.h"
#import "UserData.h"
#import "Utils.h"
#import "EXReportStudyClassModel.h"
#import "BSStudentNameRadioTableViewCell.h"
@interface SNGroupClassroomTableViewController (){
    NSMutableArray *expandedSections;
    NSMutableArray *sectionKeys;
    NSMutableArray *classLevelIdArray;
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    NSMutableArray  *key;
    NSInteger i;
    NSInteger chackCellRow;
    NSMutableDictionary<NSNumber *, EXReportStudyClassModel *> *studyClassDict;
    NSMutableDictionary *dict ;
    NSInteger *numberindex;
    NSString *classLevelId;
    BOOL checkCell;
}
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;

@end
static NSString *headerCellIdentifier = @"HeaderCell";
static NSString *childCellIdentifier = @"Cell";
static NSString *buttonCellIdentifier = @"ButtonCell";
static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";

@implementation SNGroupClassroomTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    key = [[NSMutableArray alloc] init];
    dict = [[NSMutableDictionary alloc] init];
    i = 0;
    checkCell = NO;
     [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EXReportClassHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:headerCellIdentifier];
     [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSStudentNameRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:childCellIdentifier];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self getSchoolClassLevel];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - API Caller

- (void)getSchoolClassLevel {
    
    sectionKeys = [[NSMutableArray alloc] init];
    expandedSections = [[NSMutableArray alloc] init];
    studyClassDict = [[NSMutableDictionary alloc] init];
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}
#pragma mark - CallGetSchoolClassLevelAPIDelegate
- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    if(success && data != nil) {
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        classLevelIdArray = [[NSMutableArray alloc] init];
        EXReportStudyClassModel *studyClassModel = [[EXReportStudyClassModel alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
            [classLevelIdArray addObject:@([model getClassLevelId])];
            [sectionKeys addObject:@([model getClassLevelId])];
            [studyClassDict setObject:studyClassModel forKey:@([model getClassLevelId])];
            [expandedSections addObject:@"NO"];
        }
        [self.tableView reloadData];
    }
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    if(success && data != nil) {
        _classLevelArray = data;
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        classroomStringArrray = [[NSMutableArray alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
        [dict setObject:data forKey: [NSString stringWithFormat:@"%d",numberindex]];
        [self.tableView reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(classLevelIdArray != nil) {
        i=0;
        checkCell = NO;
        return classLevelIdArray.count + classroomStringArrray.count;

    }
    else {
        return 0;
    }
   }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"index = %d", indexPath.row);

    
    if(checkCell){
        
        if(chackCellRow < [[dict objectForKey: [NSString stringWithFormat:@"%d",[key[i-1] integerValue]]] count]){
            BSStudentNameRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:childCellIdentifier forIndexPath:indexPath];
            NSLog(@"subCell = %d",[[dict objectForKey: [NSString stringWithFormat:@"%d",[key[i-1] integerValue]]] count]);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userInteractionEnabled = YES;
            chackCellRow++;
            return cell;
        }else{
            EXReportClassHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier forIndexPath:indexPath];
            cell.titleLabel.text = [classLevelStringArray objectAtIndex:indexPath.row-[[dict objectForKey: [NSString stringWithFormat:@"%d",[key[i-1] integerValue]]] count]];
            checkCell = NO;
            return cell;
            

        }
    
        

        
    }
    else{
        EXReportClassHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier forIndexPath:indexPath];
        if([expandedSections objectAtIndex:indexPath.row] == @"YES") {
            cell.imgArrow.image = [UIImage imageNamed:@"ic_arrow_down"];
        }
        else {
            cell.imgArrow.image = [UIImage imageNamed:@"ic_arrow_left"];
            
        }
        
        
        if (key.count != 0  ) {
            
            if(i<key.count){
                
                if(indexPath.row == [key[i] integerValue]){
                    NSLog(@"xxxx");
                    chackCellRow = 0;
                    checkCell = YES;
                    i = i + 1;;
                }
                
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = YES;
        cell.titleLabel.text = [classLevelStringArray objectAtIndex:indexPath.row];
        return cell;
    }
    
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    numberindex = indexPath.row;
    if([expandedSections[indexPath.row] isEqualToString : @"YES"]){
        expandedSections[indexPath.row] = @"NO";
       
        
    }else{
        expandedSections[indexPath.row] = @"YES";
        classLevelId = [classLevelIdArray objectAtIndex:indexPath.row];
        [key addObject:@(indexPath.row)];
        i = 0;
        [self getSchoolClassroomWithClassLevelId:[classLevelId integerValue]];
       
    }
    NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];

}

@end
