//
//  BSReportDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "BSStatusModel.h"
#import "BSHistoryModel.h"

#import "SlideMenuController.h"

@interface BSReportDetailViewController : UIViewController <SlideMenuControllerDelegate>

// Global variables

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<BSStatusModel *> *statusArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedStatusIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger statusId;

@property (nonatomic) BSHistoryModel *selectedHistoryModel;

@property (weak, nonatomic) IBOutlet UILabel *datetimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *recorderLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (weak, nonatomic) IBOutlet UILabel *behaviorNameLabel;

- (IBAction)moveBack:(id)sender;

@end
