//
//  BSSelectClassLevelViewController.m
//  JabjaiApp
//
//  Created by mac on 7/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSSelectClassLevelViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "Constant.h"
#import "BSSelectStudentsViewController.h"
#import "Utils.h"
#import "UserInfoViewController.h"
@interface BSSelectClassLevelViewController () {
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    UIColor *greenBGColor;
    UIColor *redBGColor;

    // The dialog
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    UnAuthorizeMenuDialog *unAuthorizeMenuDialog;
    NSBundle *myLangBundle;
    CAGradientLayer *gradient;
    NSMutableArray *authorizeSubMenuArray;
}
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";

@implementation BSSelectClassLevelViewController

@synthesize classLevelId = _classLevelId;
@synthesize classroomId = _classroomId;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    if ([UserData getUserType] != STUDENT) { [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//call to authorize menu
    }
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[UserData getChangLanguage] ofType:@"lproj"]];
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    greenBGColor = [UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0];
    redBGColor = [UIColor colorWithRed:220/255.0 green:43/255.0 blue:69/255.0 alpha:1.0];
    self.classLevelTextField.delegate = self;
    self.classroomTextField.delegate = self;
    [self.menuBtn addTarget:self action:@selector(showRightMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    if(_classLevelArray != nil && _classroomArray != nil) {
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<_classLevelArray.count; i++) {
            SchoolLevelModel *model = [_classLevelArray objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        classroomStringArrray = [[NSMutableArray alloc] init];
        for(int i=0; i<_classroomArray.count; i++) {
            SchoolClassroomModel *model = [_classroomArray objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:_selectedClassLevelIndex] getClassLevelName];
        self.classroomTextField.text = [[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomName];
    }
    else {
        _selectedClassLevelIndex = -1;
        _selectedClassroomIndex = -1;
        // Call api
        [self getSchoolClassLevel];
    }
    
    
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
    [self setLanguage];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section2.layer setShadowOpacity:0.3];
    [self.section2.layer setShadowRadius:3.0];
    [self.section2.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLanguage{
    [self.nextButton layoutIfNeeded];
    if(_mode == BS_MODE_ADD) {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_ADD",nil,myLangBundle,nil);
        gradient = [Utils getGradientColorStatus:@"green"];
        gradient.frame = self.nextButton.bounds;
        [self.nextButton.layer insertSublayer:gradient atIndex:0];
    }
    else {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REDUCE",nil,myLangBundle,nil);
        gradient = [Utils getGradientColorStatus:@"red"];
        gradient.frame = self.nextButton.bounds;
        [self.nextButton.layer insertSublayer:gradient atIndex:0];
    }
   
    self.headerClasslevelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_CLASSLEVEL",nil,myLangBundle,nil);
    self.headerClassLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_CLASSROOM",nil,myLangBundle,nil);
    
    self.classLevelTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_BEHAVIOR_CLASSLAVEL",nil,myLangBundle,nil);
    self.classroomTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_BEHAVIOR_CLASSROOM",nil,myLangBundle,nil);
     [self.nextButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_BEHAVIOR_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}



//#pragma mark - UITextFieldDelegate
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    
//    if(textField.tag == 1) { // User press class level textfield
//        
//        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
//            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
//        }
//        return NO;
//    }
//    else if(textField.tag == 2) { // User press classroom textfield
//        
//        if(self.classLevelTextField.text.length == 0) {
//            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_CLASSLEVEL",nil,myLangBundle,nil);
//            [self showAlertDialogWithMessage:alertMessage];
//        }
//        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
//            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
//        }
//        return NO;
//    }
//    
//    return YES;
//}

#pragma mark - API Caller
- (void)getSchoolClassLevel {
    [self showIndicator];
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    [self showIndicator];
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}

#pragma mark - CallGetSchoolClassLevelAPIDelegate

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
    }
}

#pragma mark - CallGetStudentClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classroomArray = data;
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}
#pragma mark - CallGetMenuListAPIDelegate
///check authorize menu
- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
    [self showIndicator];
    // [self popUpLoaddin];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
    
        [self stopIndicator];
        if ((resCode == 300 || resCode == 200) && success ) {
            NSLog(@"xx");
            authorizeSubMenuArray = [[NSMutableArray alloc] init];
            if (data != NULL) {
                for (int i = 0;i < data.count; i++) {
                    NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
                    for (int j = 0; j < subMenuArray.count; j++) {
                        NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
                        [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
                    }
                }
            }
            NSString* notPermissionText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_UNAUTHORRIZE",nil,[Utils getLanguage],nil);
            NSString* pleaseContactText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_PLEASECONTACT",nil,[Utils getLanguage],nil);
            NSString *alertMessage = [NSString stringWithFormat:@"%@\n%@",notPermissionText,pleaseContactText];
            if(self.mode == 1){
                if (![authorizeSubMenuArray containsObject: @"6"]) {
                    [self showAlertDialogAuthorizeMenu:alertMessage];
                    NSLog(@"ไม่มีสิทธิ์");
                }
            }else{
                if (![authorizeSubMenuArray containsObject: @"12"]) {
                    [self showAlertDialogAuthorizeMenu:alertMessage];
                    NSLog(@"ไม่มีสิทธิ์");
                }
            }
        }else{
            
            self.dbHelper = [[UserInfoDBHelper alloc] init];
            [self.dbHelper deleteUserAllWithMasterId:[UserData getMasterUserID]];
            
            // Logout
            [UserData setUserLogin:NO];
            [UserData saveMasterUserID:0];
            [UserData saveUserID:0];
            [UserData setUserType:USERTYPENULL];
            [UserData setAcademyType:ACADEMYTYPENULL];
            [UserData setFirstName:@""];
            [UserData setLastName:@""];
            [UserData setUserImage:@""];
            [UserData setGender:MALE];
            [UserData setClientToken:@""];
            LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
            [self.slideMenuController changeMainViewController:viewController close:YES];
        }
}

- (void)onAlertDialogClose {
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]){
        if(index == _selectedClassLevelIndex) {
            return;
        }
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        // Clear dependency text field values
        self.classroomTextField.text = @"";
        _selectedClassroomIndex = -1;
        _classLevelId = 0;
        // get classroom data with class level
        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:_classLevelId];
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        if(index == _selectedClassroomIndex) {
            return;
        }
        self.classroomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        _classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
    }
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showAlertDialogAuthorizeMenu:(NSString *)message {
    if(unAuthorizeMenuDialog != nil && [unAuthorizeMenuDialog isDialogShowing]) {
        [unAuthorizeMenuDialog dismissDialog];
    }
    else {
        unAuthorizeMenuDialog = [[UnAuthorizeMenuDialog alloc] init];
        unAuthorizeMenuDialog.delegate = self;
    }
    [unAuthorizeMenuDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_CLASSLEVEL",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    else if(self.classroomTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_CLASSROOM",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        return NO;
    }
    
    return YES;
}

- (void)gotoSelectStudentsPage {
    
    BSSelectStudentsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectStudentsStoryboard"];
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    viewController.status = 2;//status 2 is next
    viewController.selectedStudentArray = _selectedStudentArray;
    viewController.behaviorScoreArray = _behaviorScoreArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Kxmenu
- (void)showRightMenu:(UIButton *)sender{
    NSArray *menuItems;
    if(_mode == BS_MODE_ADD) {
        menuItems =
             @[
               [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_ADD_SCAN_QRCODE",nil,[Utils getLanguage],nil)
                              image:nil
                             target:self
                             action:@selector(doScannerQRcode:)],
               
               [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_ADD_SCORE_USERID",nil,[Utils getLanguage],nil)
                              image:nil
                             target:self
                             action:@selector(doPadNumber:)],
               
               ];
    }else{
        menuItems =
             @[
               [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REDUCE_SCAN_QRCODE",nil,[Utils getLanguage],nil)
                              image:nil
                             target:self
                             action:@selector(doScannerQRcode:)],
               
               [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REDUCE_SCORE_USERID",nil,[Utils getLanguage],nil)
                              image:nil
                             target:self
                             action:@selector(doPadNumber:)],
               
               ];
    }
    
  
      const CGFloat W = self.view.bounds.size.width;
      const CGFloat H = self.view.bounds.size.height;
    
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(W - 16, 18, 40, 40) menuItems:menuItems];
}
-(void)doScannerQRcode:(id)sender{
    BSScanerBehavioScorerViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSScanerBehavioScorerStoryboard"];
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = self.mode;
    viewController.status = 0;//status 0 is scan QR code
    viewController.selectedStudentArray = _selectedStudentArray;
    viewController.behaviorScoreArray = _behaviorScoreArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
-(void)doPadNumber:(id)sender{
    BSPadNumberBehavioViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSPadNumberBehavioStoryboard"];
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = self.mode;
    viewController.status = 1;//status 1 is scan padNumber
    viewController.selectedStudentArray = _selectedStudentArray;
    viewController.behaviorScoreArray = _behaviorScoreArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

#pragma mark - Action fuctions

- (IBAction)openDrawer:(id)sender {
//    [self.revealViewController revealToggle:self.revealViewController];
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}

- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        [self gotoSelectStudentsPage];
    }
}

- (IBAction)selectClassroomBtn:(id)sender {
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_CLASSLEVEL",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
    }
    else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
        [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
    }
}

- (IBAction)selectClassBtn:(id)sender {
    if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
        [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
    }
}

- (IBAction)menuAction:(id)sender {
}
@end
