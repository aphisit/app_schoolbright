//
//  TECheckOnTimeScanerQrCodeViewController.h
//  JabjaiApp
//
//  Created by toffee on 31/7/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "CallTEGetDataStudentScanBarcodeAPI.h"
#import "CallTEScanerQRConfirmOnTimeAPI.h"
#import "ConfirmScanerLateViewController.h"
#import "AlertDialogConfirm.h"
#import "AlertDialog.h"
#import "TELevelClassViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface TECheckOnTimeScanerQrCodeViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate,CallTEGetDataStudentScanBarcodeAPIDelegate,CallTEScanerQRConfirmOnTimeAPIDelegate, AlertDialogConfirmDelegate,AlertDialogDelegate,ConfirmScanerLateViewControllerDelegate ,CallTEGetDataStudentScanBarcodeAPIDelegate>


@property (strong, nonatomic) NSString *typeScanner;
@property (nonatomic,assign) NSInteger mode;
@property (strong, nonatomic) IBOutlet UIView *viewforCamera;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;


-(BOOL)startReading;
-(void)stopReading;
-(void)loadBeepSound;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
- (IBAction)moveBackAction:(id)sender;


@end

NS_ASSUME_NONNULL_END
