//
//  UserViewController.h
//  JabjaiApp
//
//  Created by mac on 5/17/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUIImageView.h"
#import <CoreData/CoreData.h>
#import "CardPfileXIBViewController.h"
#import "SlideMenuController.h"
#import "TableListDialog.h"
#import "CallImageBarCodeAPI.h"
#import "CallGetTakeEventToChackAPI.h"
#import "TASelectClassLevelViewController.h"
#import "CallChackSignUpPin.h"
@class UserViewController;
@protocol UserViewControllerDelegate <NSObject>
-(void)showCardProfile;
- (void) doClickMenu:(UserViewController *)classObj;
@optional
@end

@interface UserViewController : UIViewController <CardPfileXIBViewControllerDelegate, SlideMenuControllerDelegate, TableListDialogDelegate,CallImageBarCodeAPIDelegate,CallGetTakeEventToChackAPIDelegate,CallChackSignUpPinDelegate>

@property (nonatomic, weak) id<UserViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet CustomUIImageView *userImageView;
@property (weak, nonatomic) IBOutlet CustomUIImageView *schoolBackgroundImage;
@property (weak, nonatomic) IBOutlet UIView *schoolBackgroundConstentView;

@property (weak, nonatomic) IBOutlet UIImageView *imageCardimg;
@property (weak, nonatomic) IBOutlet UIView *cardContentView;

@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastLabel;
//@property (weak, nonatomic) IBOutlet UILabel *classRoomLabel;
//@property (weak, nonatomic) IBOutlet UILabel *idStudenLabel;
@property (strong, nonatomic) IBOutlet UILabel *departmentHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *positionHeaderLabel;
//@property (strong, nonatomic) IBOutlet UILabel *levelHeaderLabel;
//@property (strong, nonatomic) IBOutlet UILabel *idStudentHeaderLevel;
@property (weak, nonatomic) IBOutlet UILabel *calandarHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *attendenceHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *shopingHeaderLabel;
@property (weak, nonatomic) IBOutlet UIImageView *calandarHeaderImage;
@property (weak, nonatomic) IBOutlet UIImageView *attendenceHeaderImage;
@property (weak, nonatomic) IBOutlet UIImageView *shopingHeaderImage;


//action view
@property (weak, nonatomic) IBOutlet UIView *calandarView;
@property (weak, nonatomic) IBOutlet UIView *attendanceView;
@property (weak, nonatomic) IBOutlet UIView *shopingView;



@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *departmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;


@property (weak, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
///@property (weak, nonatomic) IBOutlet UILabel *classLabel;
//@property (weak, nonatomic) IBOutlet UILabel *studentIDLabel;
//@property (weak, nonatomic) IBOutlet UIStackView *classStackView;
//@property (weak, nonatomic) IBOutlet UIStackView *studentIDStackView;
//@property (weak, nonatomic) IBOutlet UIStackView *departmentStackView;
//@property (weak, nonatomic) IBOutlet UIStackView *positionStackView;
@property (weak, nonatomic) IBOutlet UIView *scoreView;

//student
@property (weak, nonatomic) IBOutlet UIView *studentFooterView;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentBalanceLabel;


@property (weak, nonatomic) IBOutlet UILabel *teacherBalanceHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *teacherScoreHeaderLabel;
//teacher
@property (weak, nonatomic) IBOutlet UILabel *teacherBalanceLabel;
//@property (weak, nonatomic) IBOutlet UIView *teacherFooterView;
@property (weak, nonatomic) IBOutlet UIView *disableBackgroundView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;





@end
