//
//  USubjectModel.h
//  JabjaiApp
//
//  Created by mac on 12/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "USectionModel.h"

@interface USubjectModel : NSObject

@property (strong, nonatomic) NSString *subjectID;
@property (strong, nonatomic) NSString *subjectName;
@property (strong, nonatomic) NSArray<USectionModel *> *sections;

@end
