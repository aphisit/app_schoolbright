//
//  CallGetEXReportAttendSchoolTeacherListAPI.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXReportNameStatusModel.h"

@class CallGetEXReportAttendSchoolTeacherListAPI;

@protocol CallGetEXReportAttendSchoolTeacherListAPIDelegate <NSObject>

@optional
- (void)callGetEXReportAttendSchoolTeacherListAPI:(CallGetEXReportAttendSchoolTeacherListAPI *)classObj data:(NSArray<EXReportNameStatusModel *> *) data success:(BOOL)success;

@end

@interface CallGetEXReportAttendSchoolTeacherListAPI : NSObject

@property (nonatomic, weak) id<CallGetEXReportAttendSchoolTeacherListAPIDelegate> delegate;

- (void)call:(long long)schoolId date:(NSDate *)date;

@end
