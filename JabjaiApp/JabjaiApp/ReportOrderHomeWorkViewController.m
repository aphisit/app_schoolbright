//
//  ReportOrderHomeWorkViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportOrderHomeWorkViewController.h"

#import "NavigationViewController.h"

#import "ReportListTeacherViewController.h"

#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"
#import "AppDelegate.h"

#import "ReportOrderHomeWorkListModel.h"
#import "ReportOrderHomeWorkListTableViewCell.h"

#import "ReportOrderHomeWorkPageMenuViewController.h"

#import "AlertDialog.h"

@interface ReportOrderHomeWorkViewController (){
    
    NSMutableDictionary<NSNumber *, NSArray<ReportOrderHomeWorkListModel *> *> *reportSections;
    NSMutableArray<ReportOrderHomeWorkListModel *> *messages;
    NSInteger connectCounter;
    UIColor *grayColor;
    UIRefreshControl *refreshControl;
    AlertDialog *alertDialog;
    
}

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property (nonatomic, strong) HWTDetailOrderHomeworkViewControllerXIB *hWTDetailOrderHomeworkViewControllerXIB;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

static NSString *dataCellIdentifier = @"Cell";

@implementation ReportOrderHomeWorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    reportSections = [[NSMutableDictionary alloc] init];
    messages = [[NSMutableArray alloc] init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportOrderHomeWorkListTableViewCell class]) bundle:nil] forCellReuseIdentifier:dataCellIdentifier];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.timeFormatter = [Utils getDateFormatter];
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [self.timeFormatter setDateFormat:@"hh:mm"];
    }
    //[self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    [self getReportOrderHomeworkWithpage:1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}


//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_HOME",nil,[Utils getLanguage],nil);
}

- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialog alloc] init];
    }
    else if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showFinishAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else{

        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
    
}

- (void)onAlertDialogClose {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return reportSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger page = section +1;
    
    return [[reportSections objectForKey:@(page)] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger page = indexPath.section + 1;
    ReportOrderHomeWorkListModel *model = [[reportSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
    ReportOrderHomeWorkListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
    
    if (model.numberFile == 0) {
        cell.attachFileLogo.image = nil;
    }
    else {
        cell.attachFileLogo.image = [UIImage imageNamed:@"attach_file"];
    }
    
    cell.homeworkLogo.image = [UIImage imageNamed:@"new_homework_icon"];
    cell.headTitle.text = model.title;
    cell.dateTitle.text = [[NSString alloc] initWithFormat:@"%@ %@", [self.timeFormatter stringFromDate:model.dateSend], [self.dateFormatter stringFromDate:model.dateSend]];
    cell.detailLabel.text = model.message;
    [cell.sectionView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.sectionView.layer setShadowOpacity:0.3];
    [cell.sectionView.layer setShadowRadius:3.0];
    [cell.sectionView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    return cell;
}


#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger page = indexPath.section + 1;
    
    ReportOrderHomeWorkListModel *model = [[reportSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    if (self.hWTDetailOrderHomeworkViewControllerXIB != nil) {
        self.hWTDetailOrderHomeworkViewControllerXIB = nil;
    }
    self.hWTDetailOrderHomeworkViewControllerXIB = [[HWTDetailOrderHomeworkViewControllerXIB alloc] init];
    [self.hWTDetailOrderHomeworkViewControllerXIB showDetailHomeworkController:self.view homeworkID:model.homeworkID sortData:@""];
//    ReportOrderHomeWorkPageMenuViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportOrderHomeWorkPageMenuStoryboard"];
//        viewController.homeworkID = model.homeworkID;
//        viewController.sortData = nil;
//    [self.slideMenuController changeMainViewController:viewController close:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)getReportOrderHomeworkWithpage:(NSUInteger)page{
    [self showIndicator];
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getReportOrderHomeworkWithSchoolID:schoolID page:page];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getReportOrderHomeworkWithpage:page];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportOrderHomeworkWithpage:page];
                    
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportOrderHomeworkWithpage:page];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int homeworkID = [[dataDict objectForKey:@"homework_id"] intValue];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"message"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"message"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"plane_name"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"plane_name"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *newsDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"daydend"]];
                    
                    NSMutableString *sender;
                    
                    if(![[dataDict objectForKey:@"theacher_name"] isKindOfClass:[NSNull class]]) {
                        sender = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"theacher_name"]];
                    }
                    else {
                        sender = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    int numberFile = [[dataDict objectForKey:@"files"] intValue];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) newsDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    
                    NSRange dotRange = [newsDateStr rangeOfString:@"."];
                    
                    NSDate *newsDate;
                    
                    if(dotRange.length != 0) {
                        newsDate = [formatter dateFromString:newsDateStr];
                    }
                    else {
                        newsDate = [formatter2 dateFromString:newsDateStr];
                    }
                    
                    ReportOrderHomeWorkListModel *model = [[ReportOrderHomeWorkListModel alloc] init];
                    
                    model.homeworkID = homeworkID;
                    model.message = message;
                    model.username = sender;
                    model.dateSend = newsDate;
                    model.title = title;
                    model.numberFile = numberFile;
                    
                    
                    [newDataArr addObject:model];
                    
                }
                
                [messages removeAllObjects];
                messages = [[NSMutableArray alloc] initWithArray:newDataArr];
                [reportSections setObject:newDataArr forKey:@(page)];
                
                [self.tableView reloadData];
            }
            
        }
    }];
    
    
}

- (void)fetchMoreData{
    
    if (messages.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *lastRowIndexPath = [visiblePaths lastObject];
        
        NSInteger numberOfSections = [self.tableView numberOfSections];
        NSInteger lastRowSection = [lastRowIndexPath section];
        NSInteger lastRowRow = [lastRowIndexPath row];
        NSInteger numberOfRowInsection = [self.tableView numberOfRowsInSection:lastRowSection];
        
        if (lastRowSection == numberOfSections -1 && lastRowRow == numberOfRowInsection - 1) {
            if (messages.count % 20 == 0) {
                NSInteger nextpage = lastRowSection + 2;
                
                [self getReportOrderHomeworkWithpage:nextpage];
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self fetchMoreData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self fetchMoreData];
}


- (void)showIndicator{
    if (![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator{
    [self.indicator stopAnimating];
    
}


- (IBAction)moveBack:(id)sender {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
