//
//  MGListMenuViewController.h
//  JabjaiApp
//
//  Created by toffee on 4/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallGMMenuAPI.h"
#import "UserData.h"
#import "MenuFoodTableViewCell.h"
#import "CallGMGetUserInfoDataAPI.h"
#import "MGListMargetViewController.h"
#import "MGSummaryViewController.h"
#import "MGBillSummaryViewController.h"

#import "SlideMenuController.h"

@interface MGListMenuViewController : UIViewController<CallGMMenuAPIDelegate, UITableViewDelegate, UITableViewDataSource, MenuFoodTableViewCellDelegate, CallGMGetUserInfoDataAPIDelegate, SlideMenuControllerDelegate>
@property (nonatomic) long long shopId;
@property (nonatomic) NSString* nameShop;


@property (strong, nonatomic) NSArray<CallGMMenuAPI *> *menuArray;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerShoppingLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *confirmOrder;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)moveBack:(id)sender;
- (IBAction)actionConfirmOrder:(id)sender;

@end
