//
//  SNPickerTimeViewController.m
//  JabjaiApp
//
//  Created by toffee on 25/7/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "SNPickerTimeViewController.h"
#import "Utils.h"
@interface SNPickerTimeViewController ()
@property (nonatomic) BOOL isShowing;
@end

@implementation SNPickerTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isShowing = NO;
    self.dialogView.layer.cornerRadius = 15;
    self.dialogView.layer.masksToBounds = true;
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm,*gradientCancel;
    //set color header
    [self.okButton layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.okButton.bounds;
    [self.okButton.layer insertSublayer:gradientConfirm atIndex:0];

    //set color button next
    [self.cancelButton layoutIfNeeded];
    gradientCancel = [Utils getGradientColorStatus:@"red"];
    gradientCancel.frame = self.cancelButton.bounds;
    [self.cancelButton.layer insertSublayer:gradientCancel atIndex:0];
}

- (IBAction)confirmAction:(id)sender {
    [self dismissDialog];
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(pickerDialogPressOK:)]) {
        [self.delegate pickerDialogPressOK:self.timePicker.date];
    }
}

- (IBAction)cancelAction:(id)sender {
    [self dismissDialog];
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(pickerDialogPressCancel)]) {
        [self.delegate pickerDialogPressCancel];
    }
}

- (void)showDialogInView:(UIView *)targetView{
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

//- (void)dateChanged:(id)sender
//{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"hh:mm a"];
//    NSString *currentTime = [dateFormatter stringFromDate:self.timePicker.date];
//    NSLog(@"%@", currentTime);
//}

@end
