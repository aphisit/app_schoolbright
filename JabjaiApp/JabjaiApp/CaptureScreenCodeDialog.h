//
//  VerifyCodeDialog.h
//  JabjaiApp
//
//  Created by mac on 10/28/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CaptureScreenCodeDialogDelegate <NSObject>

@optional
- (void)onPressCaptureScreen;
- (void)onDismissCaptureDialog;

@end

@interface CaptureScreenCodeDialog : UIViewController

@property (nonatomic, retain) id<CaptureScreenCodeDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *verifyCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hintLabel;
@property (weak, nonatomic) IBOutlet UIButton *captureBtn;

- (IBAction)captureScreen:(id)sender;

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title verifyCode:(NSString *)verifyCode;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
