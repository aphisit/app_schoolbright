//
//  SSubjectModel.h
//  JabjaiApp
//
//  Created by mac on 5/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SSubjectModel : NSObject

@property (assign, nonatomic) long long subjectID;
@property (strong, nonatomic) NSString *subjectName;
@property (strong, nonatomic) NSString *startTime;
@property (strong, nonatomic) NSString *endTime;
@property (strong, nonatomic) NSString *planeID;
@property (strong, nonatomic) NSString *codeSubject;

@end
