//
//  TimeTableViewController.h
//  JabjaiApp
//
//  Created by mac on 5/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//
@import ARSlidingPanel;

//@protocol TimeTableViewControllerDelegate <NSObject>
//
//@optional
//- (void)onSelectDate:(NSDate *)date;
//
//@end

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "SSubjectViewControllerDelegate.h"
#import "CFSCalendar.h"
#import "SlideMenuController.h"

@interface TimeTableViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate,CFSCalendarDelegateAppearance, UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate>

//@property (retain, nonatomic) id<TimeTableViewControllerDelegate> delegate;

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) NSCalendar *gregorian;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;

@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailConstraint;

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)openDrawer:(id)sender;
- (IBAction)openDetail:(id)sender;


@end
