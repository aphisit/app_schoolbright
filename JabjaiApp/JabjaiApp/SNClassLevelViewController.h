//
//  SNClassLevelViewController.h
//  JabjaiApp
//
//  Created by toffee on 8/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
//#import "EXReportClassStatusModel.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"

#import "SlideMenuController.h"

@interface SNClassLevelViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate, SlideMenuControllerDelegate>{

}
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSDate *reportDate;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;

@property (strong, nonatomic) NSMutableDictionary *dicSction; //เก็บid โดยจะมีindex เป็นKey
@property (strong, nonatomic) NSMutableDictionary *dict ;
@property (nonatomic) NSMutableArray* level2Id;
@property (nonatomic) NSInteger sendType;
@property (nonatomic) NSInteger sendGroup;
@property (nonatomic) NSInteger newsType;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *headertitelLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;


- (IBAction)moveBack:(id)sender;
- (IBAction)actionNext:(id)sender;

@end
