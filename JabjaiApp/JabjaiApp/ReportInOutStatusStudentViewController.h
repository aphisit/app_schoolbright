//
//  ReportInOutStatusStudentViewController.h
//  JabjaiApp
//
//  Created by toffee on 27/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportInOutSelectClassRoomCollectionViewCell.h"
#import "ReportInOutStatusStudentCollectionViewCell.h"
#import "CallReportInOutStatusStudentAPI.h"
#import "ReportInOutStatusStudentTableViewCell.h"
#import "ReportInOutAllDataSelectLevelViewController.h"
#import "SlideMenuController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutStatusStudentViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, CallReportInOutStatusStudentAPIDelegate, UITableViewDelegate, UITableViewDataSource, SlideMenuControllerDelegate>
@property (strong, nonatomic) NSDate *dayReports;
@property (nonatomic) long long levelId;

@property (weak, nonatomic) IBOutlet UILabel *classRoomLable;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerNoStudentLabel;

@property (weak, nonatomic) IBOutlet UICollectionView *classroomCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *statusCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *showNoStudent;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *headerView;
- (IBAction)moveBack:(id)sender;

@end

NS_ASSUME_NONNULL_END
