//
//  TimeTableDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 5/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

#import "SlideMenuController.h"

@interface TimeTableDetailViewController : UIViewController <SlideMenuControllerDelegate>

// Global variables
@property (assign, nonatomic) long long subjectID;
@property (strong, nonatomic) NSString *subjectName;
@property (strong, nonatomic) NSDate *consideredDate;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;
@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *subjectCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomLabel;
@property (weak, nonatomic) IBOutlet UILabel *lecturerLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseStartTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseEndTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *clockInLabel;

@property (weak, nonatomic) IBOutlet UILabel *clockInTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *clockInTimeStatusLabel;

- (IBAction)moveBack:(id)sender;

@end
