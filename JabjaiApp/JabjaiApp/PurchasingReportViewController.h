//
//  PurchasingReportViewController.h
//  JabjaiApp
//
//  Created by mac on 5/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import ARSlidingPanel;

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CFSCalendar.h"

#import "SlideMenuController.h"

@protocol PurchasingReportViewControllerDelegate <NSObject>

@optional
- (void)onSelectDate:(NSDate *)date;

@end

@interface PurchasingReportViewController : UIViewController <CFSCalendarDelegate, CFSCalendarDelegateAppearance, CFSCalendarDataSource , UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate>

@property (retain, nonatomic) id<PurchasingReportViewControllerDelegate> delegate;

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailContraint;

@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;
- (IBAction)openDetail:(id)sender;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)openDrawer:(id)sender;

@end
