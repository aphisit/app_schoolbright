//
//  TAReportCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import ARSlidingPanel;

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CFSCalendar.h"

#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "TASubjectModel.h"

#import "CallGetTAHistoryStudentStatusListAPI.h"

#import "SlideMenuController.h"

//@class TAReportCalendarViewController;
//
//@protocol TAReportCalendarViewControllerDelegate <NSObject>
//
//@optional
//- (void)onSelectDate:(TAReportCalendarViewController *)classObj date:(NSDate *)date;
//
//@end

@interface TAReportCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, UITableViewDelegate, UITableViewDataSource ,CallGetTAHistoryStudentStatusListAPIDelegate, SlideMenuControllerDelegate>

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<TASubjectModel *> *subjectArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
@property (nonatomic) long long selectedClassLevelId;
@property (nonatomic) long long selectedClassroomId;
@property (nonatomic) NSString *selectedSubjectId;

//@property (weak, nonatomic) id<TAReportCalendarViewControllerDelegate> delegate;

@property (weak ,nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;

@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailConstraint;


- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)moveBack:(id)sender;
- (IBAction)openDetail:(id)sender;
@end
