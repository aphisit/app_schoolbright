//
//  SchoolClassroomModel.m
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SchoolClassroomModel.h"

@implementation SchoolClassroomModel

-(NSNumber *)getClassroomID {
    return self.classroomID;
}
-(NSString *)getClassroomName {
    return self.classroomName;
}

@end
