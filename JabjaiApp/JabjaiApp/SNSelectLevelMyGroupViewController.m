//
//  SNSelectLevelMyGroupViewController.m
//  JabjaiApp
//
//  Created by toffee on 2/15/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "SNSelectLevelMyGroupViewController.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "TableListDialog.h"
#import "Constant.h"

@interface SNSelectLevelMyGroupViewController (){
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
}
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;
@end
static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";
@implementation SNSelectLevelMyGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.classLavelTextField.delegate = self;
    self.classRoomTextField.delegate = self;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (screenSize.height <= 568.0f) {
            CALayer *bottomBorder = [CALayer layer];
            bottomBorder.frame = CGRectMake(0.0f, self.classLavelTextField.frame.size.height *0.975, self.classLavelTextField.frame.size.width *10, 1.0f);
            bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
            [self.classLavelTextField.layer addSublayer:bottomBorder];
            
            CALayer *bottomBorder1 = [CALayer layer];
            bottomBorder1.frame = CGRectMake(0.0f, self.classRoomTextField.frame.size.height *0.985, self.classRoomTextField.frame.size.width *10, 1.0f);
            bottomBorder1.backgroundColor = [UIColor blackColor].CGColor;
            [self.classRoomTextField.layer addSublayer:bottomBorder1];
            
            
        }
        else{
            
            CALayer *bottomBorder = [CALayer layer];
            bottomBorder.frame = CGRectMake(0.0f, self.classLavelTextField.frame.size.height *1.15, self.classLavelTextField.frame.size.width *10, 1.0f);
            bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
            [self.classLavelTextField.layer addSublayer:bottomBorder];
            
            CALayer *bottomBorder1 = [CALayer layer];
            bottomBorder1.frame = CGRectMake(0.0f, self.classRoomTextField.frame.size.height *1.15, self.classRoomTextField.frame.size.width *10, 1.0f);
            bottomBorder1.backgroundColor = [UIColor blackColor].CGColor;
            [self.classRoomTextField.layer addSublayer:bottomBorder1];
            
        }
        
    }
    [self getSchoolClassLevel];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // User press class level textfield
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        if(self.classLavelTextField.text.length == 0) {
            NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }
        return NO;
    }
    return YES;
}


- (void)getSchoolClassLevel {
    [self showIndicator];
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}
- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    [self showIndicator];
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        NSLog(@"CLASSLAVEL = %@",classLevelStringArray);
    }
}

#pragma mark - CallGetStudentClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classroomArray = data;
        
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}
#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {

    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
        self.classLavelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classRoomTextField.text = @"";
        _selectedClassroomIndex = -1;
        _classLevelId = 0;
        
        // get classroom data with class level
        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:_classLevelId];
        
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        self.level2Id = [[NSMutableArray alloc] init];
        
        if(index == _selectedClassroomIndex) {
            return;
        }
        
        self.classRoomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        _classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        // addID Room
        [self.level2Id addObject:@(_classroomId)];
        
    }
    
}

- (BOOL)validateData {
    
    if(self.classLavelTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classRoomTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกชั้นเรียน";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}
- (void)gotoSelectStudentsPage {
    NSLog(@"level2Id = %@",self.level2Id);
//    SNClassRoomIndividualViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNClassRoomIndividualStoryboard"];
//
//    viewController.classLevelArray = _classLevelArray;
//    viewController.classroomArray = _classroomArray;
//    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
//    viewController.selectedClassroomIndex = _selectedClassroomIndex;
//    viewController.classLevelId = _classLevelId;
//    viewController.classroomId = _classroomId;
//    viewController.mode = _mode;
//    viewController.level2id = self.level2Id;
//    viewController.sendType = self.sendtype;
//    viewController.newsType = self.newstype;
//    viewController.sendGroup = self.sendgroup;
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
//    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionNext:(id)sender {
    if([self validateData]) {
        [self gotoSelectStudentsPage];
    }
}
@end
