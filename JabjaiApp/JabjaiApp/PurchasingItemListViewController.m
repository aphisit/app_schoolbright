//
//  PurchasingItemListViewController.m
//  JabjaiApp
//
//  Created by mac on 5/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "PurchasingItemListViewController.h"
#import "PurchasingTableViewCell.h"
#import "BoughtProductModel.h"
#import "Constant.h"
#import "APIURL.h"
#import "Utils.h"

@interface PurchasingItemListViewController () {
    NSMutableArray<BoughtProductModel *> *productArray;
    
    NSInteger connectCounter;
    UIColor *grayColor;
}

@property (strong, nonatomic) ARSPContainerController *panelControllerContainer;
@property (strong, nonatomic) PurchasingReportViewController *purchasingReportViewController;

@end

@implementation PurchasingItemListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.panelControllerContainer = (ARSPContainerController *)self.parentViewController;
    self.purchasingReportViewController = (PurchasingReportViewController *)self.panelControllerContainer.mainViewController;
    self.purchasingReportViewController.delegate = self;
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = grayColor;
    
    connectCounter = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PurchasingTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:[NSDate date]];
    [self getPurchasingItemListWithDate:[NSDate date]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(productArray == nil) {
        return 0;
    }
    else {
        return productArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BoughtProductModel *model = [productArray objectAtIndex:indexPath.row];
    
    PurchasingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
//    cell.productLabel.text = model.productName;
//    cell.quantityLabel.text = [[NSString alloc] initWithFormat:@"%ld", model.amount ];
//    cell.priceLabel.text = [[NSString alloc] initWithFormat:@"%.2lf บ.", model.amount * model.price];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

#pragma mark - PurchasingReportViewControllerDelegate;
- (void)onSelectDate:(NSDate *)date {
    
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:date];
    [self getPurchasingItemListWithDate:date];
}

#pragma mark - Get API Data
- (void)getPurchasingItemListWithDate:(NSDate *)date {
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getBoughtItemWithUserID:userID date:date schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getPurchasingItemListWithDate:date];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getPurchasingItemListWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getPurchasingItemListWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(productArray != nil) {
                    [productArray removeAllObjects];
                    productArray = nil;
                }
                
                productArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long productID = [[dataDict objectForKey:@"Id"] longLongValue];
                    NSInteger amount = [[dataDict objectForKey:@"Amount"] integerValue];
                    double price = [[dataDict objectForKey:@"Price"] doubleValue];
                    
                    NSMutableString *productName;
                    
                    if(![[dataDict objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
                        productName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Name"]];
                    }
                    else {
                        productName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) productName);
                    
                    BoughtProductModel *model = [[BoughtProductModel alloc] init];
                    model.productName = productName;
                    model.productID = productID;
                    model.amount = amount;
                    model.price = price;
                    
                    [productArray addObject:model];
                    
                }
                
                [self.tableView reloadData];
            }
        }
        
    }];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

@end
