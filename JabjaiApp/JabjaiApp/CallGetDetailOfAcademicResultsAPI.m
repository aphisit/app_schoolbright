//
//  CallGetDetailOfAcademicResultsAPI.m
//  JabjaiApp
//
//  Created by toffee on 19/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallGetDetailOfAcademicResultsAPI.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetDetailOfAcademicResultsAPI () {
    NSInteger connectCounter;
    NSMutableArray<ARSubjectScoreModel *> *basicCourseArray;
    NSMutableArray<ARSubjectScoreModel *> *additionalCourseArray;
    NSMutableArray<ARSubjectScoreModel *> *evenCourseArray;
    NSMutableArray<ARSubjectScoreModel *> *noCountCreditArray;
    ARDetailModel *model;

}
@end

@implementation CallGetDetailOfAcademicResultsAPI
- (void) call:(long long)schoolId studentId:(long long)studentId year:(NSString *)year term:(NSString*)term{
    connectCounter = 0;
    [self getDetailOfAcademicResultsAPI:schoolId studentId:studentId year:year term:term];
}

- (void) getDetailOfAcademicResultsAPI:(long long)schoolId studentId:(long long)studentId year:(NSString *)year term:(NSString*)term{
    NSString *URLString = [APIURL getDetailOfAcademicResults:schoolId studentId:studentId year:year term:term];
    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet]; ;
    NSString *result = [URLString stringByAddingPercentEncodingWithAllowedCharacters:set];
    NSURL *url = [NSURL URLWithString:result];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getDetailOfAcademicResultsAPI:schoolId studentId:studentId year:year term:term];
            }
            else {
                isFail = YES;
                self->connectCounter = 0;
            }
        } else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getDetailOfAcademicResultsAPI:schoolId studentId:studentId year:year term:term];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
            }
            else {
                self->connectCounter = 0;
                NSDictionary *dataDict = returnedData;
                NSMutableString  *studentName, *studentID, *branch, *levelName, *subLevelName, *schoolNameTH, *schoolNameEN, *schoolImg,*termStr,*gradeAverage;
                NSMutableString *courseCode, *courseName, *status,*score, *grade;
                NSMutableString *courseGroupName;
                
                //double gradeAverage;
                //int term;
                NSArray *planCourses;
                self->basicCourseArray = [[NSMutableArray alloc] init];
                self->additionalCourseArray = [[NSMutableArray alloc] init];
                self->evenCourseArray = [[NSMutableArray alloc] init];
                self->noCountCreditArray = [[NSMutableArray alloc] init];
                
               // term = (int)[dataDict objectForKey:@"Term"];
                NSInteger year = [[dataDict objectForKey:@"Year"] integerValue];
                //gradeAverage = [[dataDict objectForKey:@"GradeAverage"] doubleValue];
                if ([[UserData getChangLanguage] isEqualToString:@"th"]) {
                    if(![[dataDict objectForKey:@"FullName"] isKindOfClass:[NSNull class]]) {
                        studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"FullName"]];
                    }
                    else {
                        studentName = [[NSMutableString alloc] initWithString:@"-"];
                    }
                }else{
                    if(![[dataDict objectForKey:@"FullName"] isKindOfClass:[NSNull class]]) {
                        studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"FullName"]];
                    }
                    else {
                        studentName = [[NSMutableString alloc] initWithString:@"-"];
                    }
                }
                if(![[dataDict objectForKey:@"SStudentId"] isKindOfClass:[NSNull class]]) {
                    studentID = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SStudentId"]];
                }
                else {
                    studentID = [[NSMutableString alloc] initWithString:@"-"];
                }
                
                if(![[dataDict objectForKey:@"Branch"] isKindOfClass:[NSNull class]]) {
                    branch = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Branch"]];
                }
                else {
                    branch = [[NSMutableString alloc] initWithString:@"-"];
                }
                
                if(![[dataDict objectForKey:@"LevelName"] isKindOfClass:[NSNull class]]) {
                    levelName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"LevelName"]];
                }
                else {
                    levelName = [[NSMutableString alloc] initWithString:@"-"];
                }
                
                if(![[dataDict objectForKey:@"SubLevelName"] isKindOfClass:[NSNull class]]) {
                    subLevelName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SubLevelName"]];
                }
                else {
                    subLevelName = [[NSMutableString alloc] initWithString:@"-"];
                }

                if(![[dataDict objectForKey:@"SchoolName"] isKindOfClass:[NSNull class]]) {
                    schoolNameTH = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolName"]];
                }
                else {
                    schoolNameTH = [[NSMutableString alloc] initWithString:@"-"];
                }
                
                if(![[dataDict objectForKey:@"SchoolNameEn"] isKindOfClass:[NSNull class]]) {
                    schoolNameEN = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolNameEn"]];
                }
                else {
                    schoolNameEN = [[NSMutableString alloc] initWithString:@"-"];
                }
                
                if(![[dataDict objectForKey:@"SchoolImage"] isKindOfClass:[NSNull class]]) {
                    schoolImg = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolImage"]];
                }
                else {
                    schoolImg = [[NSMutableString alloc] initWithString:@""];
                }
                
                if(![[dataDict objectForKey:@"Term"] isKindOfClass:[NSNull class]]) {
                    termStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Term"]];
                }
                else {
                    termStr = [[NSMutableString alloc] initWithString:@"-"];
                }
                if(![[dataDict objectForKey:@"GradeAverage"] isKindOfClass:[NSNull class]]) {
                    gradeAverage = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"GradeAverage"]];
                }
                else {
                    gradeAverage = [[NSMutableString alloc] initWithString:@"-"];
                }
                if(![[dataDict objectForKey:@"PlanCourses"] isKindOfClass:[NSNull class]]) {
                    planCourses = [dataDict objectForKey:@"PlanCourses"];
                    
                }
                
                
                for (int i = 0; i < planCourses.count; i++) {
                    NSDictionary *planCoursesDict = [planCourses objectAtIndex:i];
                    if(![[planCoursesDict objectForKey:@"CourseCode"] isKindOfClass:[NSNull class]]) {
                        courseCode = [[NSMutableString alloc] initWithFormat:@"%@", [planCoursesDict objectForKey:@"CourseCode"]];
                    }
                    else {
                        courseCode = [[NSMutableString alloc] initWithString:@"-"];
                    }
                    if(![[planCoursesDict objectForKey:@"CourseName"] isKindOfClass:[NSNull class]]) {
                        courseName = [[NSMutableString alloc] initWithFormat:@"%@", [planCoursesDict objectForKey:@"CourseName"]];
                    }
                    else {
                        courseName = [[NSMutableString alloc] initWithString:@"-"];
                    }
                    if(![[planCoursesDict objectForKey:@"GetSpecial"] isKindOfClass:[NSNull class]]) {
                        status = [[NSMutableString alloc] initWithFormat:@"%@", [planCoursesDict objectForKey:@"GetSpecial"]];
                    }
                    else {
                        status = [[NSMutableString alloc] initWithString:@"-"];
                    }
                    if(![[planCoursesDict objectForKey:@"CourseGroupName"] isKindOfClass:[NSNull class]]) {
                        courseGroupName = [[NSMutableString alloc] initWithFormat:@"%@", [planCoursesDict objectForKey:@"CourseGroupName"]];
                    }
                    else {
                        courseGroupName = [[NSMutableString alloc] initWithString:@"-"];
                    }
                    if(![[planCoursesDict objectForKey:@"Grade"] isKindOfClass:[NSNull class]]) {
                        grade = [[NSMutableString alloc] initWithFormat:@"%@", [planCoursesDict objectForKey:@"Grade"]];
                        if ([grade isEqualToString:@""]) {
                             grade = [[NSMutableString alloc] initWithString:@"0"];
                        }
                    }
                    else {
                        grade = [[NSMutableString alloc] initWithString:@"0"];
                    }
                    if ([term isEqualToString:@"1"]) {
                        if(![[planCoursesDict objectForKey:@"Term1Score"] isKindOfClass:[NSNull class]]) {
                            score = [[NSMutableString alloc] initWithFormat:@"%@", [planCoursesDict objectForKey:@"Term1Score"]];
                            if ([score isEqualToString:@""]) {
                                score = [[NSMutableString alloc] initWithString:@"0"];
                            }
                        }
                        else {
                            score = [[NSMutableString alloc] initWithString:@"0"];
                        }
                    }else if ([term isEqualToString:@"2"]){
                        if(![[planCoursesDict objectForKey:@"Term2Score"] isKindOfClass:[NSNull class]]) {
                            score = [[NSMutableString alloc] initWithFormat:@"%@", [planCoursesDict objectForKey:@"Term2Score"]];
                            if ([score isEqualToString:@""]) {
                                score = [[NSMutableString alloc] initWithString:@"0"];
                            }
                        }
                        else {
                            score = [[NSMutableString alloc] initWithString:@"0"];
                        }
                    }else{
                        if(![[planCoursesDict objectForKey:@"AverageScore"] isKindOfClass:[NSNull class]]) {
                            score = [[NSMutableString alloc] initWithFormat:@"%@", [planCoursesDict objectForKey:@"AverageScore"]];
                            if ([score isEqualToString:@""]) {
                                score = [[NSMutableString alloc] initWithString:@"0"];
                            }
                        }
                        else {
                            score = [[NSMutableString alloc] initWithString:@"0"];
                        }
                    }
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) courseCode);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) courseName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) status);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) score);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) grade);
                    ARSubjectScoreModel *scoreModel = [[ARSubjectScoreModel alloc] init];
                    [scoreModel setCourseCode:courseCode];
                    [scoreModel setCourseName:courseName];
                    [scoreModel setScore:score];
                    [scoreModel setGrade:grade];
                    [scoreModel setStatus:status];
                    
                    if ([courseGroupName isEqualToString:@"รายวิชาพื้นฐาน"]) {
                        [self->basicCourseArray addObject:scoreModel];
                    }else if ([courseGroupName isEqualToString:@"รายวิชาเพิ่มเติม"]){
                        [self->additionalCourseArray addObject:scoreModel];
                    }else if ([courseGroupName isEqualToString:@"รายวิชาเสริมไม่คิดหน่วยกิต"]){
                        [self->noCountCreditArray addObject:scoreModel];
                    }else{
                        [self->evenCourseArray addObject:scoreModel];
                    }
                }
                
                CFStringTrimWhitespace((__bridge CFMutableStringRef) studentName);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) studentID);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) branch);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) levelName);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) subLevelName);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) schoolNameTH);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) schoolNameEN);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) schoolImg);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) termStr);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) gradeAverage);
                
                self->model = [[ARDetailModel alloc] init];
                [self->model setTerm:termStr];
                [self->model setYear:year];
                [self->model setGradeAverage:gradeAverage];
                [self->model setName:studentName];
                [self->model setStudentId:studentID];
                [self->model setBranch:branch];
                [self->model setLevelName:levelName];
                [self->model setSubLevelName:subLevelName];
                [self->model setSchoolNameTH:schoolNameTH];
                [self->model setSchoolNameEN:schoolNameEN];
                [self->model setSchoolNameImg:schoolImg];
                [self->model setBasicCoursesArray:self->basicCourseArray];
                [self->model setAdditionalCoursesArray:self->additionalCourseArray];
                [self->model setEventCoursesArray:self->evenCourseArray];
                [self->model setNoCountCreditArray:self->noCountCreditArray];
                
                NSLog(@"xxx");
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(CallGetDetailOfAcademicResultsAPI:data:success:)]) {
                                   [self.delegate CallGetDetailOfAcademicResultsAPI:self data:self->model success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(CallGetDetailOfAcademicResultsAPI:data:success:)]) {
                [self.delegate CallGetDetailOfAcademicResultsAPI:self data:self->model success:NO];
            }
        }
        
    }];
}
@end
