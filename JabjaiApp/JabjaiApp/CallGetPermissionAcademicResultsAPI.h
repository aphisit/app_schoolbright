//
//  CallGetPermissionAcademicResultsAPI.h
//  JabjaiApp
//
//  Created by toffee on 25/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CallGetPermissionAcademicResultsAPI;
@protocol CallGetPermissionAcademicResultsAPIDelegate <NSObject>
@optional
- (void)callGetPermissionAcademicResultsAPI:( CallGetPermissionAcademicResultsAPI *)classObj data:(BOOL)data success:(BOOL)success;
@end
@interface CallGetPermissionAcademicResultsAPI : NSObject
@property (nonatomic, weak) id<CallGetPermissionAcademicResultsAPIDelegate> delegate;
- (void)call:(long long)schoolId studentId:(long long)studentId;
@end

NS_ASSUME_NONNULL_END
