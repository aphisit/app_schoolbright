//
//  ReportHomeWorkTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 1/4/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportHomeWorkTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *redView;
@property (weak, nonatomic) IBOutlet UILabel *labelSubjectName;
@property (weak, nonatomic) IBOutlet UILabel *labelDetail;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeSend;

@end
