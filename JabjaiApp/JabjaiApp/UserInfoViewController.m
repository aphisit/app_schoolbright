//
//  UserInfoViewController.m
//  JabjaiApp
//
//  Created by mac on 5/17/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//
@import AirshipKit;
#import "UserInfoViewController.h"
#import "NewsMessageTableViewCell.h"
#import "MessageTableViewCell.h"
#import "MessageInboxDataModel.h"
#import "StudyViewController.h"
#import "UserViewController.h"
#import <Sheriff/GIBadgeView.h>
#import "CallGetUnreadMessageCountAPI.h"
#import "AppDelegate.h"
#import "ReportInOutAllDataViewController.h"
#import "CallGetImageNewsAPI.h"
#import "CallMessageInUserAPI.h"
#import "BSBehaviorTableViewCell.h"
#import "MessageInboxDialog.h"
#import "MessageInboxLongTextDialog.h"
#import <AFNetworking/AFNetworking.h>
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"
// 04/01/2561
#import "UserInfoModel.h"
#import "UserInfoDBHelper.h"
#import "NoticeInboxDetailViewController.h"
#import "TeacherTimeTableViewController.h"
#import "NoticeInboxStatusViewController.h"
#import <Crashlytics/Crashlytics.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "KxMenu.h"

static NSString *dataCellIdentifier = @"MessageCell";
static NSString *statusCellIdentifier = @"Cell";
static NSString *emojiCellIdentifier = @"EmoCell";
static NSInteger itemPerPage = 20;
NSString *statusTakeEvent;
@interface UserInfoViewController () <CallGetUnreadMessageCountAPIDelegate> {
    NSMutableDictionary<NSNumber *, NSArray<MessageInboxDataModel *> *> *messageSections;
    NSMutableArray<MessageInboxDataModel *> *messages;
    UIColor *unreadBGColor;
    UIColor *readBGColor;
    UIColor *OrangeColor;
    UIColor *GrayColor;
    NSMutableArray *imageArray;
    NSArray *returnedArray;
    NSInteger connectCounter;
    BOOL isTableViewShowing;
    long long massageID;
    NSManagedObject *schoolImage;
    NSUInteger userID;
    NSBundle *myLangBundle;
    UIRefreshControl *refreshControl;
    
    CAGradientLayer *gradient;
    CAGradientLayer *greenGradient;
    CAGradientLayer *yellowGradient;
    CAGradientLayer *blueGradient;
    CAGradientLayer *pinkGradient;
    CAGradientLayer *purpleGradient;
    CAGradientLayer *redGradient;
    CAGradientLayer *blackGradient;
    CAGradientLayer *purpleSoftGradient;
    
    UIImage * greenColorImage;
    UIImage * yellowColorImage;
    UIImage * blueColorImage;
    UIImage * pinkColorImage;
    UIImage * purpleColorImage;
    UIImage * redColorImage;
    UIImage * blackColorImage;
    UIImage * purpleSoftColorImage;
    NSString *sortData; //status read
    ChangPasswordDialog *changPasswordDialog;
    StudyViewController *studyViewController;
    MKDropdownMenu *dropdownMenu;

}
@property (strong, nonatomic) CallGetImageNewsAPI *callGetImageNews;
@property (nonatomic) CAPSPageMenu *pageMenu;
//extern MKDropdownMenu *mmKDropdownMenu;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property (nonatomic, strong) GIBadgeView *badgeView;
@property (nonatomic, strong) MessageInboxDialog *messageInboxDialog;
@property (nonatomic, strong) MessageInboxLongTextDialog *messageInboxLongTextDialog;
@property (nonatomic, strong) MessageInBoxScrollableTextDialog *messageInboxScrollableTextDialog;
@property (nonatomic, strong) CallGetUnreadMessageCountAPI *callGetUnreadMessageCountAPI;
@property (strong, nonatomic) CallMessageInUserAPI *callGetIdUserOfMessageAPI;
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@property (nonatomic, strong) CallOpenReadAllMessageAPI *callOpenReadAllMessageAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (strong, nonatomic) CardPfileXIBViewController *cardPfileXIBViewController;
@property (nonatomic, strong) NFNewFutureSlideShowViewController *nFNewFutureSlideShowViewController;
@property (nonatomic, strong) UserHealthHitorysTemperatureViewController *userHealthHitorysTemperatureViewController;
@property (nonatomic, strong) SNDetailNewsViewController *sNDetailNewsViewController;
@property (nonatomic, strong) JHDetailHomeWorkViewController *jHDetailHomeWorkViewController;
// 04/01/2561
@property (strong, nonatomic) UserInfoModel *userInfoModel;
@property (strong, nonatomic) UserInfoDBHelper *dbHelper;
@property (strong, nonatomic) UserViewController *userViewController;
@property (nonatomic, strong) LIDetailLeaveViewControllerXIB *lIDetailLeaveViewControllerXIB;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@property (nonatomic, strong) CallInsetDataSystemNotificationPOSTAPI *callInsetDataSystemNotificationPOSTAPI;
@property (nonatomic, strong) CallSTSendTokenAndLanguageAPI *callSTSendTokenAndLanguageAPI;
@end



@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    messageSections = [[NSMutableDictionary alloc] init];
    messages = [[NSMutableArray alloc] init];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NewsMessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:dataCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:statusCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSBehaviorTableViewCell class]) bundle:nil] forCellReuseIdentifier:emojiCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([UINoteInNotificationTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"NoteCell"];
    
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [self.timeFormatter setDateFormat:@"HH:mm"];
    }
    
    ////Get Color Gradient
    greenGradient = [CAGradientLayer layer];
    yellowGradient = [CAGradientLayer layer];
    redGradient = [CAGradientLayer layer];
    blueGradient = [CAGradientLayer layer];
    pinkGradient = [CAGradientLayer layer];
    purpleGradient = [CAGradientLayer layer];
    blackGradient = [CAGradientLayer layer];
    purpleSoftGradient = [CAGradientLayer layer];
    
    greenGradient = [Utils getGradientColorStatus:@"green"];
    yellowGradient = [Utils getGradientColorStatus:@"yellow"];
    redGradient = [Utils getGradientColorStatus:@"red"];
    blueGradient = [Utils getGradientColorStatus:@"blue"];
    pinkGradient = [Utils getGradientColorStatus:@"pink"];
    purpleGradient = [Utils getGradientColorStatus:@"purple"];
    blackGradient = [Utils getGradientColorStatus:@"gray"];
    purpleSoftGradient = [Utils getGradientColorStatus:@"purplesoft"];

    unreadBGColor = [UIColor whiteColor];
    readBGColor = [UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1.0];

    OrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1.0];
    GrayColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.badgeView = [GIBadgeView new];
    [self.notificationButton.imageView addSubview:self.badgeView];
    self.notificationButton.imageView.clipsToBounds = NO;
    sortData = @"";
    self.sortButton.hidden = YES;
    self.sortButton.enabled = false;
    self.noDataLabel.hidden = YES;
    self.rightTrailingConstraint.constant = 12;
    
    [self.sortButton addTarget:self action:@selector(showRightMenu:) forControlEvents:UIControlEventTouchUpInside];
     myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    
    [self setLanguage];
    [self hideTableView];
    
    

    //Call API
    if ([UserData getUserType] == TEACHER) {
        [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]]; //call authorize menu for user
    }
    [self getMessageDataWithPage:1 status:sortData];
    [self getUnreadMessageCount];
    [self getUserInfoData];
    [self getSchoolIconNews];
    
   

    // 05/04/2561
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = OrangeColor;
    refreshControl.backgroundColor = readBGColor;
    [self.tableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [self setHeader];
    [self setupPageMenu];
}

- (void)viewDidAppear:(BOOL)animated{
    NSString *UDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];  //UDID Device
    [self callInsetDataSystemNotification:[NSString stringWithFormat:@"{\"Token\":\"%@\",\"UserID\":%d,\"SchoolID\":%lld,\"Imei\":\"%@\",\"System\":\"IOS\"}",tokenStr,[UserData getUserID],[UserData getSchoolId],UDID]];
    [self doSendTokenAndLanguage:tokenStr language:[UserData getChangLanguage]];
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [self callGetStatusServer];//call status servert
    [self checkDeepLinkMessage];
   
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
//Check online of server
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}
- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");

        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

//set color and size width header
-(void)setHeader{
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        NSLog(@"const = %f",self.heightHeadMenuConstraint.constant);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            CGFloat screenHeight = [[UIScreen mainScreen]bounds].size.height;
                self.heightHeadMenuConstraint.constant = 70.0f;
        }
        else{
            self.heightHeadMenuConstraint.constant = 105.0f;
        }
}

- (void) doDesignLayout{
    //set color header
    [self.haederView layoutIfNeeded];
    gradient = [Utils getGradientColorHeader];
    gradient.frame = self.haederView.bounds;
    [self.haederView.layer insertSublayer:gradient atIndex:0];
}

-(void)setLanguage{
    _infoLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_USERINFO",nil,myLangBundle,nil);
    self.noDataLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil);
}

- (void)refreshTable{
    [refreshControl endRefreshing];
    [self.tableView reloadData];
}

- (void) checkUsernameAndPasswordIsNull{
        if (([[UserData getUsername] isEqual:[NSNull null]] || [UserData getUsername] == NULL)  && ([[UserData getPassword] isEqual:[NSNull null]] || [UserData getUsername] == NULL)) {
            if ([UserData getMasterUserID] != [UserData getUserID]) {
                [self.dbHelper deleteUserWithMasterId:[UserData getMasterUserID] slaveId:[UserData getUserID]];
            }
            [[UAirship push] removeTags:[[UAirship push] tags]];
            [[UAirship push] updateRegistration];
            [UserData setUserLogin:NO];
            [UserData saveMasterUserID:0];
            [UserData saveUserID:0];
            [UserData setUserType:USERTYPENULL];
            [UserData setAcademyType:ACADEMYTYPENULL];
            [UserData setFirstName:@""];
            [UserData setLastName:@""];
            [UserData setUserImage:@""];
            [UserData setGender:MALE];
            [UserData setClientToken:@""];
            [UserData setUsername:@""];
            [UserData setPassword:@""];
            LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
            [self.slideMenuController changeMainViewController:viewController close:YES];
        }
        else{
            NSLog(@"xx");
        }
    
}

- (void)getUserInfoData {
   
    NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUserInfoData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(returnedArray.count != 0) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    NSMutableString *firstName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sName"]];
                    NSMutableString *lastName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sLastname"]];
                    NSInteger sex = [[dataDict objectForKey:@"cSex"] integerValue];
                    NSMutableString *urlImageSchool = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"school_image"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) firstName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lastName);
                    
                    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: urlImageSchool]];
                    [UserData setSchoolImage:imageData];

                }
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)checkDeepLinkMessage {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.deeplink != nil) {
        NSLog(@"Message ID  = %d",appDelegate.deeplink_message_id);
        _messageID = appDelegate.deeplink_message_id;
        self.showMessageDialogWhenOpen = YES;
        
        [self showTableView];
        NSString *userId = [self CalllistIdmember];
        self.callGetIdUserOfMessageAPI = [[CallMessageInUserAPI alloc] init];
        self.callGetIdUserOfMessageAPI.delegate = self;
        [self.callGetIdUserOfMessageAPI call:userId messageId:_messageID];
        
       // [self getMessageWithID:self.messageID];
        //[self showMessage:model];
        // Clear deep link
        appDelegate.deeplink = nil;
        appDelegate.deeplink_message_id = -1;
    }
}

-(NSString*)CalllistIdmember{
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    //userIdArray = [[NSMutableArray alloc] init];
    NSMutableString *userIdArray = [[NSMutableString alloc] init];
    NSArray *users = [self.dbHelper getUserDataWithMasterId:[UserData getMasterUserID]];
    if(users != nil) {
        for(int i=0; i< users.count; i++) {
            [userIdArray appendFormat:@"%@",[NSString stringWithFormat:@"%d",[users[i]slaveId]]];
            if(i+1 != users.count) {
                [userIdArray appendString:@","];
            }
        }
    }
    return userIdArray;
}

-  (void)callMessageInUserAPI:(CallMessageInUserAPI *)classObj userId:(NSInteger *)userId{
    userID = userId;
    [self getMessageDataWithPage:1 status:sortData ];
    [self getMessageWithID:self.messageID];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return messageSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger page = section + 1;
    if (page == 1) {
       
        return [[messageSections objectForKey:@(page)] count]+1;
    }else{
        return [[messageSections objectForKey:@(page)] count];
    }
    
    //return [[messageSections objectForKey:@(page)] count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *mCell;
    NSInteger page = indexPath.section + 1;
    NSLog(@"row = %d",indexPath.row);
    if (page == 1) {
        if (indexPath.row == 0) {
            UINoteInNotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoteCell" forIndexPath:indexPath];
            mCell = cell;
        }else{
            MessageInboxDataModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row-1];
                if(model.messageType == 1 ) {
                    MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:statusCellIdentifier forIndexPath:indexPath];
                    [cell.statusLabel layoutIfNeeded];
                    [cell.statementreadLabel layoutIfNeeded];
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    NSLog(@"xxx = %@",model.title);
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                   
                    
                    if(model.status == 0) {
                        if ([UserData getUserType] == TEACHER) {
                            cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,[Utils getLanguage],nil);
                            greenGradient.frame = cell.statusLabel.bounds;
                            UIGraphicsBeginImageContext(greenGradient.bounds.size);
                            [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                            greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                            UIGraphicsEndImageContext();
                            cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                        }else{
                            cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,[Utils getLanguage],nil);
                            greenGradient.frame = cell.statusLabel.bounds;
                            UIGraphicsBeginImageContext(greenGradient.bounds.size);
                            [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                            greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                            UIGraphicsEndImageContext();
                            cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                        }
                        
                    }
                    
                    else if(model.status == 1) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LATE",nil,[Utils getLanguage],nil);
                        yellowGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(yellowGradient.bounds.size);
                        [yellowGradient renderInContext:UIGraphicsGetCurrentContext()];
                        yellowColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:yellowColorImage];
                    }
                    else if(model.status == 3) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ABSENCE",nil,[Utils getLanguage],nil);
                        redGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(redGradient.bounds.size);
                        [redGradient renderInContext:UIGraphicsGetCurrentContext()];
                        redColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:redColorImage];
                    }
                    else if(model.status == 11) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SICK",nil,[Utils getLanguage],nil);
                        pinkGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(pinkGradient.bounds.size);
                        [pinkGradient renderInContext:UIGraphicsGetCurrentContext()];
                        pinkColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:pinkColorImage];
                    }
                    else if(model.status == 10) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_PERSONAL",nil,[Utils getLanguage],nil);
                        purpleGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(purpleGradient.bounds.size);
                        [purpleGradient renderInContext:UIGraphicsGetCurrentContext()];
                        purpleColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleColorImage];
                    }
                    else if(model.status == 12) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_EVENT",nil,[Utils getLanguage],nil);
                        blueGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(blueGradient.bounds.size);
                        [blueGradient renderInContext:UIGraphicsGetCurrentContext()];
                        blueColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:blueColorImage];
                    }
                    else if(model.status == 13) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_ONTIME",nil,[Utils getLanguage],nil);
                        greenGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(greenGradient.bounds.size);
                        [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                        greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                    }
                    else if(model.status == 14) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_EARLY",nil,[Utils getLanguage],nil);
                        redGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(redGradient.bounds.size);
                        [redGradient renderInContext:UIGraphicsGetCurrentContext()];
                        redColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:redColorImage];
                    }
                    else if(model.status == 15) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_OVERTIME",nil,[Utils getLanguage],nil);
                        greenGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(greenGradient.bounds.size);
                        [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                        greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                    }
                    
                    else if(model.status == 31) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ATTENDANCE_IS_NO",nil,[Utils getLanguage],nil);
                        blackGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(blackGradient.bounds.size);
                        [blackGradient renderInContext:UIGraphicsGetCurrentContext()];
                        greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                    }
                    
                    else if(model.status == 9) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_HOLIDAY",nil,[Utils getLanguage],nil);
                        yellowGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(yellowGradient.bounds.size);
                        [yellowGradient renderInContext:UIGraphicsGetCurrentContext()];
                        yellowColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:yellowColorImage];
                    }
                    
                    else if(model.status == 8) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_DAY_HOLIDAY",nil,[Utils getLanguage],nil);
                        greenGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(greenGradient.bounds.size);
                        [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                        greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                    }
                    
                    else if(model.status == 21) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_MATERNITY",nil,[Utils getLanguage],nil);
                        purpleSoftGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                        [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                        purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                    }
                    else if(model.status == 22) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_VACATION",nil,[Utils getLanguage],nil);
                        purpleSoftGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                        [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                        purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                    }
                    else if(model.status == 23) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_RELIGION",nil,[Utils getLanguage],nil);
                        purpleSoftGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                        [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                        purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                    }
                    else if(model.status == 24) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_MILITARY",nil,[Utils getLanguage],nil);
                        purpleSoftGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                        [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                        purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                    }
                    else if(model.status == 25) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TRAINING",nil,[Utils getLanguage],nil);
                        purpleSoftGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                        [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                        purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                    }
                    else if(model.status == 26) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_GOVERNMENT",nil,[Utils getLanguage],nil);
                        purpleSoftGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                        [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                        purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                    }
                    
                    else {
                        cell.statusLabel.text = @"n/a";
                        cell.statusLabel.backgroundColor = [UIColor blackColor];
                    }
                    
                    cell.statusLabel.layer.cornerRadius = cell.statusLabel.frame.size.width / 2;
                    cell.statusLabel.clipsToBounds = YES;
                    cell.statusLabel.layer.shouldRasterize = YES;
                    
                    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                    cell.statementreadLabel.clipsToBounds = YES;
                    cell.statementreadLabel.layer.shouldRasterize = YES;
                    
                    if (model.statusRead == 0) {
                        cell.statementreadLabel.hidden = NO;
                        cell.bgView.backgroundColor = unreadBGColor;
                    }
                    else{
                        cell.statementreadLabel.hidden = YES;
                        cell.bgView.backgroundColor = readBGColor;
                    }

                    mCell = cell;
                }
                
                else if (model.messageType == 10){
                    
                    MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:statusCellIdentifier forIndexPath:indexPath];
                    [cell.statusLabel layoutIfNeeded];
                    [cell.statementreadLabel layoutIfNeeded];
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    NSLog(@"xxx = %@",model.title);
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                    
                    if([model.message rangeOfString:@"เข้าตรงเวลา" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,[Utils getLanguage],nil);
                        greenGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(greenGradient.bounds.size);
                        [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                        greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                    }
                    else if(model.status == 0) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIMR",nil,[Utils getLanguage],nil);
                        greenGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(greenGradient.bounds.size);
                        [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                        greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                    }
                    else if(model.status == 1) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LATE",nil,[Utils getLanguage],nil);
                        yellowGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(yellowGradient.bounds.size);
                        [yellowGradient renderInContext:UIGraphicsGetCurrentContext()];
                        yellowColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:yellowColorImage];
                    }
                    else if(model.status == 3) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ABSENCE",nil,[Utils getLanguage],nil);
                        redGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(redGradient.bounds.size);
                        [redGradient renderInContext:UIGraphicsGetCurrentContext()];
                        redColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:redColorImage];
                    }
                    else if(model.status == 11) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SICK",nil,[Utils getLanguage],nil);
                        pinkGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(pinkGradient.bounds.size);
                        [pinkGradient renderInContext:UIGraphicsGetCurrentContext()];
                        pinkColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:pinkColorImage];
                    }
                    else if(model.status == 10) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_PERSONAL",nil,[Utils getLanguage],nil);
                        purpleGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(purpleGradient.bounds.size);
                        [purpleGradient renderInContext:UIGraphicsGetCurrentContext()];
                        purpleColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleColorImage];
                    }
                    else if(model.status == 12) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_EVENT",nil,[Utils getLanguage],nil);
                        blueGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(blueGradient.bounds.size);
                        [blueGradient renderInContext:UIGraphicsGetCurrentContext()];
                        blueColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:blueColorImage];
                    }
                    else if(model.status == 13) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_ONTIME",nil,[Utils getLanguage],nil);
                        greenGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(greenGradient.bounds.size);
                        [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                        greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                    }
                    else if(model.status == 14) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_EARLY",nil,[Utils getLanguage],nil);
                        greenGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(greenGradient.bounds.size);
                        [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                        greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                    }
                    else if(model.status == 15) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_OVERTIME",nil,[Utils getLanguage],nil);
                        purpleGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(purpleGradient.bounds.size);
                        [purpleGradient renderInContext:UIGraphicsGetCurrentContext()];
                        purpleColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleColorImage];
                    }
                    else {
                        cell.statusLabel.text = @"n/a";
                        cell.statusLabel.backgroundColor = [UIColor blackColor];
                    }
                    
                    cell.statusLabel.layer.cornerRadius = cell.statusLabel.frame.size.width / 2;
                    cell.statusLabel.clipsToBounds = YES;
                    cell.statusLabel.layer.shouldRasterize = YES;
                    
                    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                    cell.statementreadLabel.clipsToBounds = YES;
                    cell.statementreadLabel.layer.shouldRasterize = YES;
                    
                    if (model.statusRead == 0) {
                        cell.statementreadLabel.hidden = NO;
                        cell.bgView.backgroundColor = unreadBGColor;
                    }
                    
                    else{
                        cell.statementreadLabel.hidden = YES;
                        cell.bgView.backgroundColor = readBGColor;
                    }
                    mCell = cell;
                }
                
                else if (model.messageType == 9 && [model.title containsString:@"แจ้งคะแนนพฤติกรรม"])
                {
                    NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
                    [cell.statementreadLabel layoutIfNeeded];
                    [cell.schoolLogo layoutIfNeeded];
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    NSLog(@"xxx = %@",model.title);
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                    
                    cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                    cell.schoolLogo.clipsToBounds = YES;
                    cell.schoolLogo.layer.shouldRasterize = YES;
                   
                    if([model.message rangeOfString:@"ยินดีด้วยค่ะ" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                        
                        UIImage *schoolImage = [UIImage imageNamed:@"emoji_happy"];
                        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                        [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                        UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.schoolLogo.image = schoolImage2;
                        cell.schoolLogo.contentMode = UIViewContentModeCenter;
                        
                        cell.schoolLogo.image = schoolImage2;
                        cell.iconFile.image = nil;
                    }
                    else if([model.message rangeOfString:@"เสียใจด้วยค่ะ" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                        UIImage *schoolImage = [UIImage imageNamed:@"emoji_cry"];
                        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                        [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                        UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.schoolLogo.image = schoolImage2;
                        cell.schoolLogo.contentMode = UIViewContentModeCenter;
                        cell.schoolLogo.image = schoolImage2;
                        cell.iconFile.image = nil;
                    }
            //
            //        else {
            //
            //            cell.schoolLogo.image = nil;
            //            cell.iconFile.image = nil;
            //        }
                    
                    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                    cell.statementreadLabel.clipsToBounds = YES;
                    cell.statementreadLabel.layer.shouldRasterize = YES;
                    
                    if (model.statusRead == 0) {
                        cell.statementreadLabel.hidden = NO;
                        cell.bgView.backgroundColor = unreadBGColor;
                    }
                    else{
                        cell.statementreadLabel.hidden = YES;
                        cell.bgView.backgroundColor = readBGColor;
                    }
                   
                    mCell = cell;
                }
                
                else if (model.messageType == 7 && [model.title containsString:@"รายงานผู้บริหาร"])
                {
                    BSBehaviorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:emojiCellIdentifier forIndexPath:indexPath];
                   
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    NSLog(@"xxx = %@",model.title);
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                    [cell.statementreadLabel layoutIfNeeded];
                    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                    cell.statementreadLabel.clipsToBounds = YES;
                    cell.statementreadLabel.layer.shouldRasterize = YES;
                    
                    if (model.statusRead == 0) {
                        cell.statementreadLabel.hidden = NO;
                        cell.bgView.backgroundColor = unreadBGColor;
                    }
                    else{
                        cell.statementreadLabel.hidden = YES;
                        cell.bgView.backgroundColor = readBGColor;
                    }
                    
                    mCell = cell;
                    
                }
                
                else if (model.messageType == 11)
                {
                    BSBehaviorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:emojiCellIdentifier forIndexPath:indexPath];
                    
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    NSLog(@"xxx = %@",model.title);
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                    [cell.statementreadLabel layoutIfNeeded];
                    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                    cell.statementreadLabel.clipsToBounds = YES;
                    cell.statementreadLabel.layer.shouldRasterize = YES;
                    if (model.statusRead == 0) {
                        cell.statementreadLabel.hidden = NO;
                        cell.bgView.backgroundColor = unreadBGColor;
                    }
                    else{
                        cell.statementreadLabel.hidden = YES;
                        cell.bgView.backgroundColor = readBGColor;
                    }
                    mCell = cell;
                }
                
                else if (model.messageType == 8)//leave
                {
            //        BSBehaviorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:emojiCellIdentifier forIndexPath:indexPath];
                    
                    NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
                    
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    NSLog(@"xxx = %@",model.title);
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                    cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                    cell.schoolLogo.clipsToBounds = YES;
                    cell.schoolLogo.layer.shouldRasterize = YES;
            //        cell.schoolLogo.image = [UIImage imageNamed:@"envelope"];
            //        [cell.schoolLogo layoutIfNeeded];
                    UIImage *schoolImage = [UIImage imageNamed:@"envelope"];
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                    [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                    UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.schoolLogo.image = schoolImage2;
                     [cell.statementreadLabel layoutIfNeeded];
                    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                    cell.statementreadLabel.clipsToBounds = YES;
                    cell.statementreadLabel.layer.shouldRasterize = YES;
                    cell.iconFile.image = nil;

                    if (model.statusRead == 0) {
                        cell.statementreadLabel.hidden = NO;
                        cell.bgView.backgroundColor = unreadBGColor;
                    }
                    else{
                        cell.statementreadLabel.hidden = YES;
                        cell.bgView.backgroundColor = readBGColor;
                    }
                    mCell = cell;
                }
                
                else if (model.messageType == 3 || model.messageType == 12) {
                    
                    BSBehaviorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:emojiCellIdentifier forIndexPath:indexPath];
                    
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    NSLog(@"xxx = %@",model.title);
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                    
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    NSLog(@"xxx = %@",model.title);
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                    [cell.statementreadLabel layoutIfNeeded];
                    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                    cell.statementreadLabel.clipsToBounds = YES;
                    cell.statementreadLabel.layer.shouldRasterize = YES;
                    
                    if (model.statusRead == 0) {
                        cell.statementreadLabel.hidden = NO;
                        cell.bgView.backgroundColor = unreadBGColor;
                    }
                    else{
                        cell.statementreadLabel.hidden = YES;
                        cell.bgView.backgroundColor = readBGColor;
                    }
                    mCell = cell;
                }
                else if (model.messageType == 2) {
                    
                    BSBehaviorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:emojiCellIdentifier forIndexPath:indexPath];
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    NSLog(@"xxx = %@",model.title);
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                    
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    NSLog(@"xxx = %@",model.title);
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                    [cell.statementreadLabel layoutIfNeeded];
                    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                    cell.statementreadLabel.clipsToBounds = YES;
                    cell.statementreadLabel.layer.shouldRasterize = YES;
                    
                    if (model.statusRead == 0) {
                        cell.statementreadLabel.hidden = NO;
                        cell.bgView.backgroundColor = unreadBGColor;
                    }
                    else{
                        cell.statementreadLabel.hidden = YES;
                        cell.bgView.backgroundColor = readBGColor;
                    }
                    mCell = cell;
                }
                else {
                    NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
                    NSLog(@"message = %@",model.message);
                    NSLog(@"title = %@",model.title);
                    NSLog(@"messageType = %d",model.messageType);
                    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                    cell.titleLabel.text = model.title;
                    cell.messageLabel.text = model.message;
                    
                    massageID = model.messageID;
                    if(model.messageType == 5 && [model.title containsString:@"แจ้งประกาศข่าวสาร"]) {
                    
                        // 04/01/2561
                        NSData *loadedData = [schoolImage valueForKey:@"savedImage"];
                        UIImage *schoolImage = [UIImage imageWithData:loadedData];
                        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                        [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                        UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.schoolLogo.image = schoolImage2;
                        cell.schoolLogo.contentMode = UIViewContentModeCenter;
                        cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                        cell.schoolLogo.clipsToBounds = YES;
                        cell.schoolLogo.layer.shouldRasterize = YES;
                       // [self getSchoolIconNews];
                        
                        if (model.file == YES) {
                            cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                        }
                        else{
                            cell.iconFile.image = nil;
                        }
                        
            //            cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                    }
                    
                    else if(model.messageType == 5 && [model.title containsString:@"แจ้งประกาศกิจกรรม"]) {
                        
                        // 04/01/2561
                     
                        NSData *loadedData = [schoolImage valueForKey:@"savedImage"];
                        UIImage *schoolImage = [UIImage imageWithData:loadedData];
                        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                        [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                        UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.schoolLogo.image = schoolImage2;
                        cell.schoolLogo.contentMode = UIViewContentModeCenter;
                        cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                        cell.schoolLogo.clipsToBounds = YES;
                        cell.schoolLogo.layer.shouldRasterize = YES;
                        
                        if (model.file == YES) {
                            cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                        }
                        else{
                            cell.iconFile.image = nil;
                        }
                        //cell.schoolLogo.image = [UIImage imageWithData:imageData];
                        // [self getSchoolIconNews];
            //            cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                    }
                    
                    else if (model.messageType == 5 && [model.title containsString:@"การบ้าน"]){
                        cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                        cell.schoolLogo.clipsToBounds = YES;
                        cell.schoolLogo.layer.shouldRasterize = YES;
                        
            //            cell.schoolLogo.image = [UIImage imageNamed:@"homework"];
                        
                        UIImage *schoolImage = [UIImage imageNamed:@"homework"];
                        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                        [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                        UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.schoolLogo.image = schoolImage2;
                        cell.iconFile.image = nil;
                        
                        if (model.file == YES) {
                            cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                        }
                        else{
                            cell.iconFile.image = nil;
                        }
                    }
                    
                    else if (model.messageType == 6 && [model.title containsString:@"การบ้านวิชา"]){
                        cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                        cell.schoolLogo.clipsToBounds = YES;
                        cell.schoolLogo.layer.shouldRasterize = YES;
                        
            //            cell.schoolLogo.image = [UIImage imageNamed:@"homework"];
                        
                        UIImage *schoolImage = [UIImage imageNamed:@"homework"];
                        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                        [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                        UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.schoolLogo.image = schoolImage2;
                        
                        cell.iconFile.image = nil;
                        
                        if (model.file == YES) {
                            cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                        }
                        else{
                            cell.iconFile.image = nil;
                        }
                    }
                    
                    else{
                        UIImage *schoolImage = [UIImage imageNamed:@"school"];
                        UIGraphicsBeginImageContextWithOptions(CGSizeMake(100, 100), YES, 5.0);
                        [schoolImage drawInRect:CGRectMake(0, 0, 100, 100)];
                        UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.schoolLogo.image = schoolImage2;
                        cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                        cell.schoolLogo.layer.shouldRasterize = YES;
                        cell.schoolLogo.clipsToBounds = YES;
                        cell.iconFile.image = nil;
                    }
                    [cell.statementreadLabel layoutIfNeeded];
                    cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                    cell.statementreadLabel.clipsToBounds = YES;
                    cell.statementreadLabel.layer.shouldRasterize = YES;
                 
                    if (model.statusRead == 0) {
                        cell.statementreadLabel.hidden = NO;
                        cell.bgView.backgroundColor = unreadBGColor;
                    }
                    else{
                        cell.statementreadLabel.hidden = YES;
                        cell.bgView.backgroundColor = readBGColor;
                    }
                    mCell = cell;
                }
        }
        return mCell;
    }else{
        MessageInboxDataModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row];
            
        //    if(model.messageType == 1 && ![model.message containsString:@"คะแนนพฤติกรรม"]) { // Attendance message
            if(model.messageType == 1 ) {
                MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:statusCellIdentifier forIndexPath:indexPath];
                [cell.statusLabel layoutIfNeeded];
                [cell.statementreadLabel layoutIfNeeded];
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                NSLog(@"xxx = %@",model.title);
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
               
                
                if(model.status == 0) {
                    if ([UserData getUserType] == TEACHER) {
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,[Utils getLanguage],nil);
                        greenGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(greenGradient.bounds.size);
                        [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                        greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                    }else{
                        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,[Utils getLanguage],nil);
                        greenGradient.frame = cell.statusLabel.bounds;
                        UIGraphicsBeginImageContext(greenGradient.bounds.size);
                        [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                        greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                    }
                    
                }
                
                else if(model.status == 1) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LATE",nil,[Utils getLanguage],nil);
                    yellowGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(yellowGradient.bounds.size);
                    [yellowGradient renderInContext:UIGraphicsGetCurrentContext()];
                    yellowColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:yellowColorImage];
                }
                else if(model.status == 3) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ABSENCE",nil,[Utils getLanguage],nil);
                    redGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(redGradient.bounds.size);
                    [redGradient renderInContext:UIGraphicsGetCurrentContext()];
                    redColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:redColorImage];
                }
                else if(model.status == 11) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SICK",nil,[Utils getLanguage],nil);
                    pinkGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(pinkGradient.bounds.size);
                    [pinkGradient renderInContext:UIGraphicsGetCurrentContext()];
                    pinkColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:pinkColorImage];
                }
                else if(model.status == 10) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_PERSONAL",nil,[Utils getLanguage],nil);
                    purpleGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(purpleGradient.bounds.size);
                    [purpleGradient renderInContext:UIGraphicsGetCurrentContext()];
                    purpleColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleColorImage];
                }
                else if(model.status == 12) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_EVENT",nil,[Utils getLanguage],nil);
                    blueGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(blueGradient.bounds.size);
                    [blueGradient renderInContext:UIGraphicsGetCurrentContext()];
                    blueColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:blueColorImage];
                }
                else if(model.status == 13) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_ONTIME",nil,[Utils getLanguage],nil);
                    greenGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(greenGradient.bounds.size);
                    [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                    greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                }
                else if(model.status == 14) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_EARLY",nil,[Utils getLanguage],nil);
                    redGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(redGradient.bounds.size);
                    [redGradient renderInContext:UIGraphicsGetCurrentContext()];
                    redColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:redColorImage];
                }
                else if(model.status == 15) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_OVERTIME",nil,[Utils getLanguage],nil);
                    greenGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(greenGradient.bounds.size);
                    [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                    greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                }
                
                else if(model.status == 31) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ATTENDANCE_IS_NO",nil,[Utils getLanguage],nil);
                    blackGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(blackGradient.bounds.size);
                    [blackGradient renderInContext:UIGraphicsGetCurrentContext()];
                    greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                }
                
                else if(model.status == 9) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_HOLIDAY",nil,[Utils getLanguage],nil);
                    yellowGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(yellowGradient.bounds.size);
                    [yellowGradient renderInContext:UIGraphicsGetCurrentContext()];
                    yellowColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:yellowColorImage];
                }
                
                else if(model.status == 8) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_DAY_HOLIDAY",nil,[Utils getLanguage],nil);
                    greenGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(greenGradient.bounds.size);
                    [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                    greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                }
                
                else if(model.status == 21) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_MATERNITY",nil,[Utils getLanguage],nil);
                    purpleSoftGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                    [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                    purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                }
                else if(model.status == 22) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_VACATION",nil,[Utils getLanguage],nil);
                    purpleSoftGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                    [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                    purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                }
                else if(model.status == 23) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_RELIGION",nil,[Utils getLanguage],nil);
                    purpleSoftGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                    [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                    purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                }
                else if(model.status == 24) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_MILITARY",nil,[Utils getLanguage],nil);
                    purpleSoftGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                    [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                    purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                }
                else if(model.status == 25) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TRAINING",nil,[Utils getLanguage],nil);
                    purpleSoftGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                    [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                    purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                }
                else if(model.status == 26) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_GOVERNMENT",nil,[Utils getLanguage],nil);
                    purpleSoftGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(purpleSoftGradient.bounds.size);
                    [purpleSoftGradient renderInContext:UIGraphicsGetCurrentContext()];
                    purpleSoftColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleSoftColorImage];
                }
                
                else {
                    cell.statusLabel.text = @"n/a";
                    cell.statusLabel.backgroundColor = [UIColor blackColor];
                }
                
                cell.statusLabel.layer.cornerRadius = cell.statusLabel.frame.size.width / 2;
                cell.statusLabel.clipsToBounds = YES;
                cell.statusLabel.layer.shouldRasterize = YES;
                
                cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                cell.statementreadLabel.clipsToBounds = YES;
                cell.statementreadLabel.layer.shouldRasterize = YES;
                
                if (model.statusRead == 0) {
                    cell.statementreadLabel.hidden = NO;
                    cell.bgView.backgroundColor = unreadBGColor;
                }
                else{
                    cell.statementreadLabel.hidden = YES;
                    cell.bgView.backgroundColor = readBGColor;
                }

                mCell = cell;
            }
            
            else if (model.messageType == 10){
                
                MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:statusCellIdentifier forIndexPath:indexPath];
                [cell.statusLabel layoutIfNeeded];
                [cell.statementreadLabel layoutIfNeeded];
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                NSLog(@"xxx = %@",model.title);
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
                
                if([model.message rangeOfString:@"เข้าตรงเวลา" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,[Utils getLanguage],nil);
                    greenGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(greenGradient.bounds.size);
                    [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                    greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                }
                else if(model.status == 0) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIMR",nil,[Utils getLanguage],nil);
                    greenGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(greenGradient.bounds.size);
                    [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                    greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                }
                else if(model.status == 1) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LATE",nil,[Utils getLanguage],nil);
                    yellowGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(yellowGradient.bounds.size);
                    [yellowGradient renderInContext:UIGraphicsGetCurrentContext()];
                    yellowColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:yellowColorImage];
                }
                else if(model.status == 3) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ABSENCE",nil,[Utils getLanguage],nil);
                    redGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(redGradient.bounds.size);
                    [redGradient renderInContext:UIGraphicsGetCurrentContext()];
                    redColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:redColorImage];
                }
                else if(model.status == 11) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SICK",nil,[Utils getLanguage],nil);
                    pinkGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(pinkGradient.bounds.size);
                    [pinkGradient renderInContext:UIGraphicsGetCurrentContext()];
                    pinkColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:pinkColorImage];
                }
                else if(model.status == 10) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_PERSONAL",nil,[Utils getLanguage],nil);
                    purpleGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(purpleGradient.bounds.size);
                    [purpleGradient renderInContext:UIGraphicsGetCurrentContext()];
                    purpleColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleColorImage];
                }
                else if(model.status == 12) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_EVENT",nil,[Utils getLanguage],nil);
                    blueGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(blueGradient.bounds.size);
                    [blueGradient renderInContext:UIGraphicsGetCurrentContext()];
                    blueColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:blueColorImage];
                }
                else if(model.status == 13) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_ONTIME",nil,[Utils getLanguage],nil);
                    greenGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(greenGradient.bounds.size);
                    [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                    greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                }
                else if(model.status == 14) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_EARLY",nil,[Utils getLanguage],nil);
                    greenGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(greenGradient.bounds.size);
                    [greenGradient renderInContext:UIGraphicsGetCurrentContext()];
                    greenColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:greenColorImage];
                }
                else if(model.status == 15) {
                    cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECK_OUT_OVERTIME",nil,[Utils getLanguage],nil);
                    purpleGradient.frame = cell.statusLabel.bounds;
                    UIGraphicsBeginImageContext(purpleGradient.bounds.size);
                    [purpleGradient renderInContext:UIGraphicsGetCurrentContext()];
                    purpleColorImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.statusLabel.backgroundColor = [UIColor colorWithPatternImage:purpleColorImage];
                }
                else {
                    cell.statusLabel.text = @"n/a";
                    cell.statusLabel.backgroundColor = [UIColor blackColor];
                }
                
                cell.statusLabel.layer.cornerRadius = cell.statusLabel.frame.size.width / 2;
                cell.statusLabel.clipsToBounds = YES;
                cell.statusLabel.layer.shouldRasterize = YES;
                
                cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                cell.statementreadLabel.clipsToBounds = YES;
                cell.statementreadLabel.layer.shouldRasterize = YES;
                
                if (model.statusRead == 0) {
                    cell.statementreadLabel.hidden = NO;
                    cell.bgView.backgroundColor = unreadBGColor;
                }
                
                else{
                    cell.statementreadLabel.hidden = YES;
                    cell.bgView.backgroundColor = readBGColor;
                }
                mCell = cell;
            }
            
            else if (model.messageType == 9 && [model.title containsString:@"แจ้งคะแนนพฤติกรรม"])
            {
                NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
                [cell.statementreadLabel layoutIfNeeded];
                [cell.schoolLogo layoutIfNeeded];
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                NSLog(@"xxx = %@",model.title);
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
                
                cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                cell.schoolLogo.clipsToBounds = YES;
                cell.schoolLogo.layer.shouldRasterize = YES;
               
                if([model.message rangeOfString:@"ยินดีด้วยค่ะ" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                    
                    UIImage *schoolImage = [UIImage imageNamed:@"emoji_happy"];
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                    [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                    UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.schoolLogo.image = schoolImage2;
                    cell.schoolLogo.contentMode = UIViewContentModeCenter;
                    
                    cell.schoolLogo.image = schoolImage2;
                    cell.iconFile.image = nil;
                }
                else if([model.message rangeOfString:@"เสียใจด้วยค่ะ" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                    UIImage *schoolImage = [UIImage imageNamed:@"emoji_cry"];
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                    [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                    UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.schoolLogo.image = schoolImage2;
                    cell.schoolLogo.contentMode = UIViewContentModeCenter;
                    cell.schoolLogo.image = schoolImage2;
                    cell.iconFile.image = nil;
                }
        //
        //        else {
        //
        //            cell.schoolLogo.image = nil;
        //            cell.iconFile.image = nil;
        //        }
                
                cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                cell.statementreadLabel.clipsToBounds = YES;
                cell.statementreadLabel.layer.shouldRasterize = YES;
                
                if (model.statusRead == 0) {
                    cell.statementreadLabel.hidden = NO;
                    cell.bgView.backgroundColor = unreadBGColor;
                }
                else{
                    cell.statementreadLabel.hidden = YES;
                    cell.bgView.backgroundColor = readBGColor;
                }
               
                mCell = cell;
            }
            
            else if (model.messageType == 7 && [model.title containsString:@"รายงานผู้บริหาร"])
            {
                BSBehaviorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:emojiCellIdentifier forIndexPath:indexPath];
               
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                NSLog(@"xxx = %@",model.title);
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
                [cell.statementreadLabel layoutIfNeeded];
                cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                cell.statementreadLabel.clipsToBounds = YES;
                cell.statementreadLabel.layer.shouldRasterize = YES;
                
                if (model.statusRead == 0) {
                    cell.statementreadLabel.hidden = NO;
                    cell.bgView.backgroundColor = unreadBGColor;
                }
                else{
                    cell.statementreadLabel.hidden = YES;
                    cell.bgView.backgroundColor = readBGColor;
                }
                
                mCell = cell;
                
            }
            
            else if (model.messageType == 11)
            {
                BSBehaviorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:emojiCellIdentifier forIndexPath:indexPath];
                
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                NSLog(@"xxx = %@",model.title);
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
                [cell.statementreadLabel layoutIfNeeded];
                cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                cell.statementreadLabel.clipsToBounds = YES;
                cell.statementreadLabel.layer.shouldRasterize = YES;
                if (model.statusRead == 0) {
                    cell.statementreadLabel.hidden = NO;
                    cell.bgView.backgroundColor = unreadBGColor;
                }
                else{
                    cell.statementreadLabel.hidden = YES;
                    cell.bgView.backgroundColor = readBGColor;
                }
                mCell = cell;
            }
            
            else if (model.messageType == 8)//leave
            {
        //        BSBehaviorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:emojiCellIdentifier forIndexPath:indexPath];
                
                NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
                
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                NSLog(@"xxx = %@",model.title);
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
                cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                cell.schoolLogo.clipsToBounds = YES;
                cell.schoolLogo.layer.shouldRasterize = YES;
        //        cell.schoolLogo.image = [UIImage imageNamed:@"envelope"];
        //        [cell.schoolLogo layoutIfNeeded];
                UIImage *schoolImage = [UIImage imageNamed:@"envelope"];
                UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                cell.schoolLogo.image = schoolImage2;
                 [cell.statementreadLabel layoutIfNeeded];
                cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                cell.statementreadLabel.clipsToBounds = YES;
                cell.statementreadLabel.layer.shouldRasterize = YES;
                cell.iconFile.image = nil;

                if (model.statusRead == 0) {
                    cell.statementreadLabel.hidden = NO;
                    cell.bgView.backgroundColor = unreadBGColor;
                }
                else{
                    cell.statementreadLabel.hidden = YES;
                    cell.bgView.backgroundColor = readBGColor;
                }
                mCell = cell;
            }
            
            else if (model.messageType == 3 || model.messageType == 12) {
                
                BSBehaviorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:emojiCellIdentifier forIndexPath:indexPath];
                
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                NSLog(@"xxx = %@",model.title);
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
                
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                NSLog(@"xxx = %@",model.title);
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
                [cell.statementreadLabel layoutIfNeeded];
                cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                cell.statementreadLabel.clipsToBounds = YES;
                cell.statementreadLabel.layer.shouldRasterize = YES;
                
                if (model.statusRead == 0) {
                    cell.statementreadLabel.hidden = NO;
                    cell.bgView.backgroundColor = unreadBGColor;
                }
                else{
                    cell.statementreadLabel.hidden = YES;
                    cell.bgView.backgroundColor = readBGColor;
                }
                mCell = cell;
            }
            else if (model.messageType == 2) {
                
                BSBehaviorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:emojiCellIdentifier forIndexPath:indexPath];
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                NSLog(@"xxx = %@",model.title);
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
                
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                NSLog(@"xxx = %@",model.title);
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
                [cell.statementreadLabel layoutIfNeeded];
                cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                cell.statementreadLabel.clipsToBounds = YES;
                cell.statementreadLabel.layer.shouldRasterize = YES;
                
                if (model.statusRead == 0) {
                    cell.statementreadLabel.hidden = NO;
                    cell.bgView.backgroundColor = unreadBGColor;
                }
                else{
                    cell.statementreadLabel.hidden = YES;
                    cell.bgView.backgroundColor = readBGColor;
                }
                mCell = cell;
            }
            
            else {
               
                NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
                NSLog(@"message = %@",model.message);
                NSLog(@"title = %@",model.title);
                NSLog(@"messageType = %d",model.messageType);
                cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
                cell.titleLabel.text = model.title;
                cell.messageLabel.text = model.message;
                
                
                massageID = model.messageID;
                if(model.messageType == 5 && [model.title containsString:@"แจ้งประกาศข่าวสาร"]) {
                    
                    // 04/01/2561
                    
                    NSData *loadedData = [schoolImage valueForKey:@"savedImage"];
                    UIImage *schoolImage = [UIImage imageWithData:loadedData];
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                    [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                    UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.schoolLogo.image = schoolImage2;
                    cell.schoolLogo.contentMode = UIViewContentModeCenter;
                    cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                    cell.schoolLogo.clipsToBounds = YES;
                    cell.schoolLogo.layer.shouldRasterize = YES;
                   // [self getSchoolIconNews];
                    
                    if (model.file == YES) {
                        cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                    }
                    else{
                        cell.iconFile.image = nil;
                    }
                    
        //            cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                }
                
                else if(model.messageType == 5 && [model.title containsString:@"แจ้งประกาศกิจกรรม"]) {
                    
                    // 04/01/2561
                 
                    NSData *loadedData = [schoolImage valueForKey:@"savedImage"];
                    UIImage *schoolImage = [UIImage imageWithData:loadedData];
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                    [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                    UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.schoolLogo.image = schoolImage2;
                    cell.schoolLogo.contentMode = UIViewContentModeCenter;
                    cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                    cell.schoolLogo.clipsToBounds = YES;
                    cell.schoolLogo.layer.shouldRasterize = YES;
                    
                    if (model.file == YES) {
                        cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                    }
                    else{
                        cell.iconFile.image = nil;
                    }
                    //cell.schoolLogo.image = [UIImage imageWithData:imageData];
                    // [self getSchoolIconNews];
        //            cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                }
                
                else if (model.messageType == 5 && [model.title containsString:@"การบ้าน"]){
                    cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                    cell.schoolLogo.clipsToBounds = YES;
                    cell.schoolLogo.layer.shouldRasterize = YES;
                    
        //            cell.schoolLogo.image = [UIImage imageNamed:@"homework"];
                    
                    UIImage *schoolImage = [UIImage imageNamed:@"homework"];
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                    [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                    UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.schoolLogo.image = schoolImage2;
                    cell.iconFile.image = nil;
                    
                    if (model.file == YES) {
                        cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                    }
                    else{
                        cell.iconFile.image = nil;
                    }
                }
                
                else if (model.messageType == 6 && [model.title containsString:@"การบ้านวิชา"]){
                    cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                    cell.schoolLogo.clipsToBounds = YES;
                    cell.schoolLogo.layer.shouldRasterize = YES;
                    
        //            cell.schoolLogo.image = [UIImage imageNamed:@"homework"];
                    
                    UIImage *schoolImage = [UIImage imageNamed:@"homework"];
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5.0);
                    [schoolImage drawInRect:CGRectMake(0, 0, 80, 80)];
                    UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.schoolLogo.image = schoolImage2;
                    
                    cell.iconFile.image = nil;
                    
                    if (model.file == YES) {
                        cell.iconFile.image = [UIImage imageNamed:@"ic_selectfile"];
                    }
                    else{
                        cell.iconFile.image = nil;
                    }
                }
                
                else{
                    UIImage *schoolImage = [UIImage imageNamed:@"school"];
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(100, 100), YES, 5.0);
                    [schoolImage drawInRect:CGRectMake(0, 0, 100, 100)];
                    UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cell.schoolLogo.image = schoolImage2;
                    cell.schoolLogo.layer.cornerRadius = cell.schoolLogo.frame.size.width / 2;
                    cell.schoolLogo.layer.shouldRasterize = YES;
                    cell.schoolLogo.clipsToBounds = YES;
                    cell.iconFile.image = nil;
                }
                [cell.statementreadLabel layoutIfNeeded];
                cell.statementreadLabel.layer.cornerRadius = cell.statementreadLabel.frame.size.width / 2;
                cell.statementreadLabel.clipsToBounds = YES;
                cell.statementreadLabel.layer.shouldRasterize = YES;
             
                if (model.statusRead == 0) {
                    cell.statementreadLabel.hidden = NO;
                    cell.bgView.backgroundColor = unreadBGColor;
                }
                else{
                    cell.statementreadLabel.hidden = YES;
                    cell.bgView.backgroundColor = readBGColor;
                }
                mCell = cell;
            }
            return mCell;
    }
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        
    NSInteger page = indexPath.section + 1;
    if (page == 1) {
        if (indexPath.row > 0) {
             MessageInboxDataModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row-1];
                      [self getUnreadMessageCount];
                      if(model.statusRead == 0) { // message still unread
                          [self updateReadStatus:model];
                          model.statusRead = 1; // set message already read
                      }
                      
                      if(model.messageType == 7) { // message type executive we do not show dialog but we go to executive report
                          ReportInOutAllDataViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutAllDataStoryboard"];
                          [self.slideMenuController changeMainViewController:viewController close:YES];
                          
                      }
                      // 11/01/2018
                      else if (model.messageType == 5 && [model.title containsString:@"การบ้าน"]){
                          if(self.jHDetailHomeWorkViewController == nil) {
                              self.jHDetailHomeWorkViewController = [[JHDetailHomeWorkViewController alloc] init];
                          }
                          if(self.jHDetailHomeWorkViewController != nil && [self.jHDetailHomeWorkViewController isDialogShowing]) {
                              [self.jHDetailHomeWorkViewController dismissDetailHomeWorkViewController];
                          }
                          self.jHDetailHomeWorkViewController.delegate = self;
                          [self.jHDetailHomeWorkViewController showDetailHomeWorkViewController:self.view homeworkID:model.messageID];
                      }
                      else if (model.messageType == 6 && [model.title containsString:@"การบ้านวิชา"]){
                          if(self.jHDetailHomeWorkViewController == nil) {
                              self.jHDetailHomeWorkViewController = [[JHDetailHomeWorkViewController alloc] init];
                          }
                          if(self.jHDetailHomeWorkViewController != nil && [self.jHDetailHomeWorkViewController isDialogShowing]) {
                              [self.jHDetailHomeWorkViewController dismissDetailHomeWorkViewController];
                          }
                          self.jHDetailHomeWorkViewController.delegate = self;
                          [self.jHDetailHomeWorkViewController showDetailHomeWorkViewController:self.view homeworkID:model.messageID];

                      }
                      else if ((model.messageType == 8||model.messageType == -8) && [model.title containsString:@"จดหมายแจ้งการลา"]){
                          if (self.lIDetailLeaveViewControllerXIB != nil) {
                              self.lIDetailLeaveViewControllerXIB = nil;
                          }
                          self.lIDetailLeaveViewControllerXIB = [[LIDetailLeaveViewControllerXIB alloc] init];
                          [self.lIDetailLeaveViewControllerXIB showDetailLeaveViewController:self.view letterID:model.messageID];
//                          NoticeInboxStatusViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeInboxStatusStoryboard"];
//                          viewController.letterID = model.messageID;
//                          viewController.page = 1;
//                          [self.slideMenuController changeMainViewController:viewController close:YES];
                      }
                      else {
                          [self showMessage:model];
                          [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                      }
        }
    }else{
        MessageInboxDataModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row];
           [self getUnreadMessageCount];
           if(model.statusRead == 0) { // message still unread
               [self updateReadStatus:model];
               model.statusRead = 1; // set message already read
           }
           
           if(model.messageType == 7) { // message type executive we do not show dialog but we go to executive report
               ReportInOutAllDataViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutAllDataStoryboard"];
               [self.slideMenuController changeMainViewController:viewController close:YES];
               
           }
           // 11/01/2018
           else if (model.messageType == 5 && [model.title containsString:@"การบ้าน"]){
               if(self.jHDetailHomeWorkViewController == nil) {
                   self.jHDetailHomeWorkViewController = [[JHDetailHomeWorkViewController alloc] init];
               }
               if(self.jHDetailHomeWorkViewController != nil && [self.jHDetailHomeWorkViewController isDialogShowing]) {
                   [self.jHDetailHomeWorkViewController dismissDetailHomeWorkViewController];
               }
               self.jHDetailHomeWorkViewController.delegate = self;
               [self.jHDetailHomeWorkViewController showDetailHomeWorkViewController:self.view homeworkID:model.messageID];

           }
           else if (model.messageType == 6 && [model.title containsString:@"การบ้านวิชา"]){
               if(self.jHDetailHomeWorkViewController == nil) {
                   self.jHDetailHomeWorkViewController = [[JHDetailHomeWorkViewController alloc] init];
               }
               if(self.jHDetailHomeWorkViewController != nil && [self.jHDetailHomeWorkViewController isDialogShowing]) {
                   [self.jHDetailHomeWorkViewController dismissDetailHomeWorkViewController];
               }
               self.jHDetailHomeWorkViewController.delegate = self;
               [self.jHDetailHomeWorkViewController showDetailHomeWorkViewController:self.view homeworkID:model.messageID];

           }
           else if ((model.messageType == 8||model.messageType == -8) && [model.title containsString:@"จดหมายแจ้งการลา"]){
               
               if (self.lIDetailLeaveViewControllerXIB != nil) {
                   self.lIDetailLeaveViewControllerXIB = nil;
               }
               self.lIDetailLeaveViewControllerXIB = [[LIDetailLeaveViewControllerXIB alloc] init];
               [self.lIDetailLeaveViewControllerXIB showDetailLeaveViewController:self.view letterID:model.messageID];

//               NoticeInboxStatusViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeInboxStatusStoryboard"];
//               viewController.letterID = model.messageID;
//               viewController.page = 1;
//               [self.slideMenuController changeMainViewController:viewController close:YES];
           }
           else {
               [self showMessage:model];
               [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
           }
    }
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger page = indexPath.section + 1;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (page == 1) {
            if (indexPath.row == 0) {
                return 250;
            }
        }
        return 140;
    }else{
        if (page == 1) {
            if (indexPath.row == 0) {
                return 350;
            }
        }
        return 240;
    }
   
}
//#pragma mark - JHDetailHomeWorkViewController
//- (void) openImageOfHomeWork:(JHDetailHomeWorkViewController *)objClass imageArray:(NSArray *)imageArray{
//    if(self.jHReadImageViewController != nil) {
//        self.jHReadImageViewController = nil;
//    }
//    self.jHReadImageViewController = [[JHReadImageViewController alloc] init];
//    [self.jHReadImageViewController showPagesReadImageInView:self.view ImageArray:imageArray];
//}
//- (void)openFileOfHomeWork:(JHDetailHomeWorkViewController *)objClass pathFile:(NSString *)pathFile{
//    if(self.jHReadFileViewController != nil) {
//        self.jHReadFileViewController = nil;
//    }
//    self.jHReadFileViewController = [[JHReadFileViewController alloc] init];
//    [self.jHReadFileViewController showPagesReadFileInView:self.view partFile:pathFile];
//}


- (void)getStudentInClassroom {
    if(self.callGetImageNews != nil) {
        self.callGetImageNews = nil;
    }
    self.callGetImageNews = [[CallGetImageNewsAPI alloc] init];
    self.callGetImageNews.delegate = self;
    [self.callGetImageNews call:massageID];
}
#pragma mark - API Delegate

- (void)callImageNewsPOSTAPI:(CallGetImageNewsAPI *)classObj imageArray:(NSArray *)imageArray {
    NSLog(@"imageArray = %@",imageArray);
}

#pragma mark - Get API Data
- (void)getMessageDataWithPage:(NSUInteger)page status:(NSString*)status {
    //[self showIndicator];
    long long userID = [UserData getUserID];
    long long schoolId = [UserData getSchoolId];
    NSString *URLString = [APIURL getAllMessageWithPage:page userID:userID status:status schoolId:schoolId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        //[self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getMessageDataWithPage:page status:status];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageDataWithPage:page status:status];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageDataWithPage:page status:status];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                //get Image News
               
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    int status = [[dataDict objectForKey:@"LogStatus"] intValue];
                    int statusRead = [[dataDict objectForKey:@"nStatus"] intValue];
                    
                    int type = [[dataDict objectForKey:@"nType"] intValue];
                   
                    NSMutableString *messageDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dSend"]];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sTitle"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    BOOL file = [[dataDict objectForKey:@"file"] boolValue];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    NSRange dotRange = [messageDateStr rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:messageDateStr];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:messageDateStr];
                    }
                    
                    MessageInboxDataModel *model = [[MessageInboxDataModel alloc] init];
                    model.messageID = messageID;
                    model.userID = (int)[UserData getUserID];
                    model.messageType = type;
                    model.status =status;
                    model.title = title;
                    model.message = message;
                    model.date = messageDate;
                    model.file = file;
                    model.statusRead = statusRead;
                    
                    [newDataArr addObject:model];
                    
                }
                
                [messages removeAllObjects];
                messages = [[NSMutableArray alloc] initWithArray:newDataArr];
                [messageSections setObject:newDataArr forKey:@(page)];
                
                [self.tableView reloadData];
            }
            
        }
    }];
    
}

-(void)updateReadStatus:(MessageInboxDataModel *)model {
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:model.messageID userID:userID schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self updateReadStatus:model];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateReadStatus:model];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateReadStatus:model];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(returnedArray.count == 0) {
                    NSLog(@"%s", "Update read message status failed (return array size 0)");
                }
                
                [self getUnreadMessageCount];
            }
 
        }
        
    }];
}

- (void)getMessageWithID:(long long)messageID{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:messageID userID:userID schoolid:schoolid];
    //NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:1259906 userID:userID schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getMessageWithID:messageID];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageWithID:messageID];
                }
                else {
                    connectCounter = 0;
                }
            }

            else {
                if (returnedData != nil) {
                    NSDictionary *dataDict = returnedData;
                    connectCounter = 0;
                                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getXMLDateFormat]];
                                    
                    NSDateFormatter *formatter2 = [Utils getDateFormatter];
                    [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                    MessageInboxDataModel *messageInboxDataModel = nil;
                    int messageID,status,statusRead,type;
                    if(![[dataDict objectForKey:@"nMessageID"] isKindOfClass:[NSNull class]]) {
                        messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    }
                    else {
                        messageID = 0;
                    }
                    if(![[dataDict objectForKey:@"LogStatus"] isKindOfClass:[NSNull class]]) {
                        status = [[dataDict objectForKey:@"LogStatus"] intValue];
                    }
                    else {
                        status = 0;
                    }
                    if(![[dataDict objectForKey:@"nStatus"] isKindOfClass:[NSNull class]]) {
                        statusRead = [[dataDict objectForKey:@"nStatus"] intValue];
                    }
                    else {
                        statusRead = 0;
                    }
                    if(![[dataDict objectForKey:@"nType"] isKindOfClass:[NSNull class]]) {
                        type = [[dataDict objectForKey:@"nType"] intValue];
                    }
                    else {
                        type = 0;
                    }
                        
                    NSMutableString *messageDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dSend"]];
                    NSMutableString *message;
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    NSMutableString *title;
                    if(![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sTitle"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    NSRange dotRange = [messageDateStr rangeOfString:@"."];
                    NSDate *messageDate;
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:messageDateStr];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:messageDateStr];
                    }
                    messageInboxDataModel = [[MessageInboxDataModel alloc] init];
                    messageInboxDataModel.messageID = messageID;
                    messageInboxDataModel.userID = (int)[UserData getUserID];
                    messageInboxDataModel.messageType = type;
                    messageInboxDataModel.status = status;
                    messageInboxDataModel.title = title;
                    messageInboxDataModel.message = message;
                    messageInboxDataModel.date = messageDate;
                    messageInboxDataModel.statusRead = statusRead;

                    if(messageInboxDataModel != nil) {
                        [self showMessage:messageInboxDataModel];
                                        // check to update table view whether current message has read
                        [messageSections enumerateKeysAndObjectsUsingBlock:^(NSNumber * _Nonnull key, NSArray<MessageInboxDataModel *> * _Nonnull obj, BOOL * _Nonnull stop) {
                        BOOL shouldStop = NO;
                        for(MessageInboxDataModel *model in obj) {
                            if(model.messageID == messageInboxDataModel.messageID) {
                                [self updateReadStatus:model];
                                model.statusRead = 1; //update read status
                                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:[key integerValue] - 1] withRowAnimation:UITableViewRowAnimationFade];
                                shouldStop = YES;
                                break;
                            }
                        }
                        if(shouldStop) {
                            *stop = YES; // stop enumerate
                        }
                        }];
                    }
                }
            }
        }
        
    }];
}

- (void)getUnreadMessageCount {
    
    if(self.callGetUnreadMessageCountAPI == nil) {
        self.callGetUnreadMessageCountAPI = [[CallGetUnreadMessageCountAPI alloc] init];
        self.callGetUnreadMessageCountAPI.delegate = self;
    }
    
    [self.callGetUnreadMessageCountAPI call:[UserData getUserID] schoolid:[UserData getSchoolId]];
}

- (void)fetchMoreData {
    
    if(messages.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *lastRowIndexPath = [visiblePaths lastObject];
        
        // Check whether or not the very last row is visible.
        NSInteger numberOfSections = [self.tableView numberOfSections];
        NSInteger lastRowSection = [lastRowIndexPath section];
        NSInteger lastRowRow = [lastRowIndexPath row];
        NSInteger numberOfRowsInSection = [self.tableView numberOfRowsInSection:lastRowSection];
        
        if(lastRowSection == numberOfSections -1 && lastRowRow == numberOfRowsInSection - 1) {
            
            if(messages.count % itemPerPage == 0) {
                NSInteger nextPage = lastRowSection + 2;
                [self getMessageDataWithPage:nextPage status:sortData];
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self fetchMoreData];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self fetchMoreData];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Dialog
- (void)showMessage:(MessageInboxDataModel *)model {
    if(model.messageType == 2 || model.messageType == 3 || model.messageType == 12) { //2:purchasing, 3:topup
        NSString *adjustMessage = [self adjustPurchasingAndTopupText:model.message];
        NSString *messageStr;
        NSString *noteStr;
        NSRange noteRange = [adjustMessage rangeOfString:@"หาก"];
        if(noteRange.length != 0) {
            messageStr = [adjustMessage substringToIndex:noteRange.location-1]; //-1 minus new line position
            noteStr = [NSString stringWithFormat:@"*%@", [adjustMessage substringFromIndex:noteRange.location]];
        }
        else {
            messageStr = adjustMessage;
            noteStr = @"";
        }
        
        if(self.messageInboxLongTextDialog == nil) {
            self.messageInboxLongTextDialog = [[MessageInboxLongTextDialog alloc] init];
        }
        
        if(self.messageInboxLongTextDialog != nil && [self.messageInboxLongTextDialog isDialogShowing]) {
            [self.messageInboxLongTextDialog dismissDialog];
        }
        [self.messageInboxLongTextDialog showDialogInView:self.view title:model.title message:model.message noteMessage:noteStr messageDate:model.date];
        
    }
    else if(model.messageType == 1 ) { // 1:attendance
        
        NSString *adjustMessage = [self adjustAttendaceText:model.message];
        if(self.messageInboxDialog == nil) {
            self.messageInboxDialog = [[MessageInboxDialog alloc] init];
        }
        
        if(self.messageInboxDialog != nil && [self.messageInboxDialog isDialogShowing]) { 
            [self.messageInboxDialog dismissDialog];
        }
        
        [self.messageInboxDialog showDialogInView:self.view title:model.title message:adjustMessage messageDate:model.date];
    }
    
   else if(model.messageType == 5) {//5:news
//        SNDetailNewsMessageViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNDetailNewsMessageStoryboard"];
//        viewController.model = model;
//        viewController.typySend = 0;
//        [self.slideMenuController changeMainViewController:viewController close:YES];
       if(self.sNDetailNewsViewController == nil) {
           self.sNDetailNewsViewController = [[SNDetailNewsViewController alloc] init];
       }
       
       if(self.sNDetailNewsViewController != nil && [self.sNDetailNewsViewController isDialogShowing]) {
           [self.sNDetailNewsViewController dismissDetailNewsViewController];
       }
       [self.sNDetailNewsViewController showDetailNewsViewController:self.view model:model typeSend:0];
    }
           
    else if(model.messageType == 6){//6:homework
        if(self.jHDetailHomeWorkViewController == nil) {
            self.jHDetailHomeWorkViewController = [[JHDetailHomeWorkViewController alloc] init];
        }
        if(self.jHDetailHomeWorkViewController != nil && [self.jHDetailHomeWorkViewController isDialogShowing]) {
            [self.jHDetailHomeWorkViewController dismissDetailHomeWorkViewController];
        }
        self.jHDetailHomeWorkViewController.delegate = self;
        [self.jHDetailHomeWorkViewController showDetailHomeWorkViewController:self.view homeworkID:model.messageID];
//        ReportHomeWorkDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportHomeWorkDetailStoryboard"];
//        viewController.messageID = model.messageID;
//        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (model.messageType == 8 || model.messageType == -8 ){
        
        if (self.lIDetailLeaveViewControllerXIB != nil) {
            self.lIDetailLeaveViewControllerXIB = nil;
        }
        self.lIDetailLeaveViewControllerXIB = [[LIDetailLeaveViewControllerXIB alloc] init];
        [self.lIDetailLeaveViewControllerXIB showDetailLeaveViewController:self.view letterID:model.messageID];
        
//        NoticeInboxStatusViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeInboxStatusStoryboard"];
//        viewController.letterID = model.messageID;
//        viewController.page = 1;
//        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
   
    else { // other type 4:absencerequest

            self.messageInboxScrollableTextDialog = [[MessageInBoxScrollableTextDialog alloc] init];
            self.messageInboxScrollableTextDialog.delegate = self;
            if(self.messageInboxScrollableTextDialog != nil && [self.messageInboxScrollableTextDialog isDialogShowing]) {
                       [self.messageInboxScrollableTextDialog dismissDialog];
            }
            [self.messageInboxScrollableTextDialog showDialogInView:self.view title:model.title message:model.message messageDate:model.date messageID:model.messageID];
        
    }
}

-(void)messageInBoxScrollableTextClose{
    NSLog(@"close");
}

#pragma mark - Actions
- (IBAction)openDrawer:(id)sender {
    
    if (dropdownMenu != nil) {
        [dropdownMenu closeAllComponentsAnimated:YES];
        dropdownMenu = nil;
    }
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
//    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)showNotification:(id)sender {
    [self callGetStatusServer];
    if (dropdownMenu != nil) {
        [dropdownMenu closeAllComponentsAnimated:YES];
        dropdownMenu = nil;
    }
    if(isTableViewShowing) {
        self.sortButton.hidden = YES;
        self.sortButton.enabled = false;
        self.noDataLabel.hidden = YES;
        self.rightTrailingConstraint.constant = 12;
        [self hideTableView];
    }
    else {
        self.sortButton.hidden = NO;
        self.sortButton.enabled = true;
        //self.noDataLabel.hidden = NO;
        if ([[messageSections objectForKey:@(1)] count] > 0) {
            self.noDataLabel.hidden = YES;
        }else{
            self.noDataLabel.hidden = NO;
        }
        self.rightTrailingConstraint.constant = 56;
        [self showTableView];
    }
    
}

#pragma mark - Utility
-(NSString *)adjustPurchasingAndTopupText:(NSString *)str {
    //NSString *str = @"มีรายการใช้ระบบจับจ่าย จำนวน 8.00 บาท วันที่ 05/09/2016 ยอดคงเหลือ 2246.00 บาท หากไม่ถูกต้องติดต่อหมาย เลข 091-8233139";
    
    NSMutableString *resultStr = [[NSMutableString alloc] init];
    
    NSRange range1 = [str rangeOfString:@"จำนวน"];
    NSRange range2 = [str rangeOfString:@"วันที่"];
    NSRange range3 = [str rangeOfString:@"ยอดคงเหลือ"];
    NSRange range4 = [str rangeOfString:@"หาก"];
    
    if(range1.length != 0 && range2.length != 0 && range3.length != 0 && range4.length != 0) {
        
        NSMutableString *str1 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(0, range1.location)]];
        NSMutableString *str2 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(range1.location, range2.location - str1.length)]];
        NSMutableString *str3 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(range2.location, range3.location - str1.length - str2.length)]];
        NSMutableString *str4 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(range3.location, range4.location - str1.length - str2.length - str3.length)]];
        NSMutableString *str5 = [[NSMutableString alloc] initWithString:[str substringFromIndex:range4.location]];
        
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str1);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str2);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str3);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str4);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str5);
        
        [resultStr appendString:str1];
        
        // adjust text "จำนวน"
        NSRange str2Range = [str2 rangeOfString:@" "];
        if(str2Range.length != 0) {
            NSMutableString *str2FirstSectionStr = [[NSMutableString alloc] initWithString:[str2 substringWithRange:NSMakeRange(0, str2Range.location)]];
            NSMutableString *str2SecondSectionStr = [[NSMutableString alloc] initWithString:[str2 substringFromIndex:str2Range.location + str2Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str2FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str2SecondSectionStr);
            
            [resultStr appendFormat:@"\n%@ : %@", str2FirstSectionStr, str2SecondSectionStr];
        }
        
        // adjust text "วันที่"
        NSRange str3Range = [str3 rangeOfString:@" "];
        if(str3Range.length != 0) {
            NSMutableString *str3FirstSectionStr = [[NSMutableString alloc] initWithString:[str3 substringWithRange:NSMakeRange(0, str3Range.location)]];
            NSMutableString *str3SecondSectionStr = [[NSMutableString alloc] initWithString:[str3 substringFromIndex:str3Range.location + str3Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str3FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str3SecondSectionStr);
            
            [resultStr appendFormat:@"\n%@ : %@", str3FirstSectionStr, str3SecondSectionStr];
        }
        
        // adjust text "ยอดคงเหลือ"
        NSRange str4Range = [str4 rangeOfString:@" "];
        if(str4Range.length != 0) {
            NSMutableString *str4FirstSectionStr = [[NSMutableString alloc] initWithString:[str4 substringWithRange:NSMakeRange(0, str4Range.location)]];
            NSMutableString *str4SecondSectionStr = [[NSMutableString alloc] initWithString:[str4 substringFromIndex:str4Range.location + str4Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str4FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str4SecondSectionStr);
            
            [resultStr appendFormat:@"\n%@ : %@", str4FirstSectionStr, str4SecondSectionStr];
        }
        
        [resultStr appendFormat:@"\n%@", str5];
        
    }
    else {
        return str;
    }
    
    return resultStr;
    
}

-(NSString *)adjustAttendaceText:(NSString *)str {
    
    //NSString *str = @"ถึงโรงเรียนเวลา 00:04 น. สถานะ เข้าตรงเวลา";
    NSRange range1 = [str rangeOfString:@"สถานะ"];
    if(range1.length != 0) {
        // Substring first section
        NSString *firstSectionStr = [str substringWithRange:NSMakeRange(0, range1.location)];
        NSRange sec1Range = [firstSectionStr rangeOfString:@" "];
        
        if(sec1Range.length != 0) {
            
            NSMutableString *sec1FirstSectionStr = [[NSMutableString alloc] initWithString:[firstSectionStr substringWithRange:NSMakeRange(0, sec1Range.location)]];
            NSMutableString *sec1SecondSectionStr = [[NSMutableString alloc] initWithString:[firstSectionStr substringFromIndex:sec1Range.location + sec1Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec1FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec1SecondSectionStr);
            
            firstSectionStr = [NSString stringWithFormat:@"%@ : %@", sec1FirstSectionStr, sec1SecondSectionStr];
        }
        
        // Substring second section
        NSString *secondSectionStr = [str substringFromIndex:range1.location];
        NSRange sec2Range = [secondSectionStr rangeOfString:@" "];
        
        if(sec2Range.length != 0) {
            
            NSMutableString *sec2FirstSectionStr = [[NSMutableString alloc] initWithString:[secondSectionStr substringWithRange:NSMakeRange(0, sec2Range.location)]];
            NSMutableString *sec2SecondSectionStr = [[NSMutableString alloc] initWithString:[secondSectionStr substringFromIndex:sec2Range.location + sec2Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec2FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec2SecondSectionStr);
            
            secondSectionStr = [NSString stringWithFormat:@"%@ : %@", sec2FirstSectionStr, sec2SecondSectionStr];
        }
        
        NSString *resultString;
        
        if(firstSectionStr != nil && firstSectionStr.length > 0 && secondSectionStr != nil && secondSectionStr.length > 0) {
            resultString = [NSString stringWithFormat:@"%@\n%@", firstSectionStr, secondSectionStr];
        }
        else if(firstSectionStr != nil && firstSectionStr.length > 0) {
            resultString = [NSString stringWithFormat:@"%@", firstSectionStr];
        }
        else if(secondSectionStr != nil && secondSectionStr.length > 0) {
            resultString = [NSString stringWithFormat:@"%@", secondSectionStr];
        }
        
        return resultString;
        
    }
    else {
        return str;
    }
    
}
//วิชา  (กิจกรรมแนะแนว) ช่วงเวลา 10:00 - 11:00 เวลาเช็คชื่อ 14:52 สถานะ เข้าเรียน
//-(NSString *)adjustSubjectAttendaceText:(NSString *)str {
//
//}


- (void)hideTableView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.tableView setAlpha:0.0];
    [self.contentView setAlpha:1.0];
    [UIView commitAnimations];
    isTableViewShowing = NO;
    
}

- (void)showTableView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.tableView setAlpha:1.0];
    [self.contentView setAlpha:0.0];
    [UIView commitAnimations];
    
    isTableViewShowing = YES;
}

- (void)getSchoolIconNews{
    
    NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSchoolIconNews];
            }
            else {
                connectCounter = 0;
            }
            
        }
        
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolIconNews];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolIconNews];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if (returnedArray.count != 0) {
                    
                    self.userInfoModel = [[UserInfoModel alloc] init];
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    NSString *imageSchoolIconUrl;
                    
                    if(![[dataDict objectForKey:@"school_logo"] isKindOfClass:[NSNull class]]){
                        imageSchoolIconUrl = [dataDict objectForKey:@"school_logo"];
                        
                        /////
                        NSData  *data;
                        NSManagedObjectContext *context = [self managedObjectContext];
                        NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: imageSchoolIconUrl]];
                        schoolImage  = [NSEntityDescription insertNewObjectForEntityForName:@"Entity" inManagedObjectContext:context];
                            
                        if(imageData != nil){
                              
                            UIImage *image = [UIImage imageWithData: imageData];
                            data = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.0)];
                            [schoolImage setValue:data forKey:@"savedImage"];
                        }else{
                            data = [NSData dataWithData:UIImageJPEGRepresentation([UIImage imageNamed:@"school"], 0.0)];
                            [schoolImage setValue:data forKey:@"savedImage"];
                        }
                        /////
                    }
                    else{
                        imageSchoolIconUrl = @"";
                    }
                    
                    self.userInfoModel.imageUrl = imageSchoolIconUrl;
                    [UserData setSchoolPic:imageSchoolIconUrl];
                    if(imageSchoolIconUrl != nil && imageSchoolIconUrl.length > 0) {
                        BOOL success = [self.dbHelper updateImageUrlWithSlaveId:[UserData getUserID] imageUrl:imageSchoolIconUrl];
                        NSLog(@"%@", [NSString stringWithFormat:@"update school icon success : %d", success]);
                    }
                }
                
            }
            
            [self.tableView reloadData];
        }
        
        
        
    }];
    
}

#pragma mark - Pager

- (void)setupPageMenu {
    
    _userViewController.delegate = self;
    
    if ([UserData getUserType] != STUDENT) {
        
        UserViewController *controller1 = [[UserViewController alloc] init];
        controller1.title = [NSLocalizedStringFromTableInBundle(@"LABEL_INFO_USERINFO",nil,myLangBundle,nil) uppercaseString];
        
        StatisticsComeToSchoolViewController *controller2 = [self.storyboard instantiateViewControllerWithIdentifier:@"StatisticsComeToSchoolStoryboard"];
        controller2.title = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ATTENDANCE_TEACHER",nil,myLangBundle,nil);
        controller2.delegate = self;
        
        NSArray *controllerArray = @[controller1, controller2];
        
        NSDictionary *parameters;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            parameters = @{
                           CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                           CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                           CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                           CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                           CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                           CAPSPageMenuOptionMenuMargin: @(40),
                           CAPSPageMenuOptionMenuHeight: @(50),
                           CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                           CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:32.0],
                           CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                           CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                           CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                           CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                           };
        }
        else{
            parameters = @{
                           CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                           CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                           CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                           CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                           CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                           CAPSPageMenuOptionMenuMargin: @(20),
                           CAPSPageMenuOptionMenuHeight: @(40),
                           CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                           CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:22.0],
                           CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                           CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                           CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                           CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                           };
        }
        [self.contentView layoutIfNeeded];
        _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
        
        // For the first time set first tab background to orange and label color to white
        NSArray *menuItems = _pageMenu.menuItems;
        MenuItemView *menuItemView = [menuItems objectAtIndex:_pageMenu.currentPageIndex];
        menuItemView.backgroundColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.40 alpha:1.0];
        menuItemView.titleLabel.textColor = [UIColor whiteColor];
        
        //Optional delegate
        _pageMenu.delegate = self;
        
        [self.contentView addSubview:_pageMenu.view];
        
    }
    else{
        
        UserViewController *controller1 = [[UserViewController alloc] init];
        controller1.title = [NSLocalizedStringFromTableInBundle(@"LABEL_INFO_USERINFO",nil,myLangBundle,nil) uppercaseString];
        
        StatisticsComeToSchoolViewController *controller2 = [self.storyboard instantiateViewControllerWithIdentifier:@"StatisticsComeToSchoolStoryboard"];
        controller2.title = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ATTENDANCE_STUDENT",nil,myLangBundle,nil);
        controller2.delegate = self;
        
        UserHealthViewController *controller3 = [[UserHealthViewController alloc] init];
        controller3.delegate = self;
        controller3.title = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_HEALTH",nil,myLangBundle,nil);
        
        NSArray *controllerArray = @[controller1, controller2, controller3];
        
        NSDictionary *parameters;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            parameters = @{
                           CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                           CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                           CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                           CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                           CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                           CAPSPageMenuOptionMenuMargin: @(40),
                           CAPSPageMenuOptionMenuHeight: @(50),
                           CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                           CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:32.0],
                           CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                           CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                           CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                           CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                           };
        }
        else{
            parameters = @{
                           CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                           CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                           CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                           CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                           CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                           CAPSPageMenuOptionMenuMargin: @(20),
                           CAPSPageMenuOptionMenuHeight: @(40),
                           CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                           CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:22.0],
                           CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                           CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                           CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                           CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                           };
        }
        
        [self.contentView layoutIfNeeded];
        _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
        
        // For the first time set first tab background to orange and label color to white
        NSArray *menuItems = _pageMenu.menuItems;
        MenuItemView *menuItemView = [menuItems objectAtIndex:_pageMenu.currentPageIndex];
        menuItemView.backgroundColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.40 alpha:1.0];
        menuItemView.titleLabel.textColor = [UIColor whiteColor];
        
        //Optional delegate
        _pageMenu.delegate = self;
        
        [self.contentView addSubview:_pageMenu.view];
        
    }

}
#pragma mark - UserHealthHitorysTemperatureViewController
-(void)responseUserHealthHitorysTemperatureViewController{
    self.userHealthHitorysTemperatureViewController = [[UserHealthHitorysTemperatureViewController alloc] init];
    [self.userHealthHitorysTemperatureViewController showPageHitorysTemperatuer:self.parentViewController.view];
}
#pragma CAPSPageMenuDelegate

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    NSArray *menuItems = _pageMenu.menuItems;
    int count = 0;
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.40 alpha:1.0];
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        count++;
    }
}

#pragma mark - CallGetUnreadMessageCountAPIDelegate

- (void)callGetUnreadMessageCountAPI:(CallGetUnreadMessageCountAPI *)classObj unreadCount:(NSInteger)unreadCount success:(BOOL)success {
    
    if(success) {
        [self.badgeView setBadgeValue:unreadCount];
    }
    else {
        [self.badgeView setBadgeValue:0];
    }
}
-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)showRightMenu:(UIButton *)sender{
    
    NSArray *menuItems =
    @[
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ALL",nil,[Utils getLanguage],nil)
                     image:nil
                    target:self
                    action:@selector(sortAll:)],
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_READ",nil,[Utils getLanguage],nil)
                     image:nil
                    target:self
                    action:@selector(sortRead:)],
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_UNREAD",nil,[Utils getLanguage],nil)
                     image:nil
                    target:self
                    action:@selector(sortUnRead:)],
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SELECT_ALL_READ",nil,[Utils getLanguage],nil)
                     image:nil
                    target:self
                    action:@selector(readAll:)]
      ];

    const CGFloat W = self.view.bounds.size.width;
    const CGFloat H = self.view.bounds.size.height;
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(W - 16, 18, 40, 40) menuItems:menuItems];
}

- (void) sortAll:(id)sender
{
    sortData = @"";
   
   // NSIndexPath *topPath = [NSIndexPath indexPathForRow:0 inSection:0];
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
         [self getMessageDataWithPage:1 status:sortData ];
    });
    
    NSLog(@"%@", sortData);
}

- (void) sortRead:(id)sender
{
    sortData = @"read";
    //NSIndexPath *topPath = [NSIndexPath indexPathForRow:0 inSection:0];
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        [self getMessageDataWithPage:1 status:sortData ];
    });
    NSLog(@"%@", sortData);
}

- (void) sortUnRead:(id)sender
{
    sortData = @"unread";
    //NSIndexPath *topPath = [NSIndexPath indexPathForRow:0 inSection:0];
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        [self getMessageDataWithPage:1 status:sortData ];
    });
    NSLog(@"%@", sortData);
}

- (void) readAll:(id)sender
{
    //sortData = @"Readall";
    //NSIndexPath *topPath = [NSIndexPath indexPathForRow:0 inSection:0];
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        [self callOpenMessageDataAll:[UserData getUserID] schoolid:[UserData getSchoolId]];
    });
    [self getUnreadMessageCount];
    NSLog(@"%@", sortData);
}
#pragma mark - chang password
- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
    //[self showIndicator];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}

- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
   // [self stopIndicator];

        if (resCode == 300) {//User Not Update PassWord
            [self showAlertDialogWithChangPassword:@"แจ้งเตือน"];
        }
        else if (resCode == 401 || resCode == 402 || resCode == 301){ //User Not Found, User Delete, Token Not Match
            [UserData setUserLogin:NO];
            [UserData saveMasterUserID:0];
            [UserData saveUserID:0];
            [UserData setUserType:USERTYPENULL];
            [UserData setAcademyType:ACADEMYTYPENULL];
            [UserData setFirstName:@""];
            [UserData setLastName:@""];
            [UserData setUserImage:@""];
            [UserData setGender:MALE];
            [UserData setClientToken:@""];

            LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
            [self.slideMenuController changeMainViewController:viewController close:YES];
        }


}

-(void)callOpenMessageDataAll:(long long)userId schoolid:(long long)schoolid{
    self.callOpenReadAllMessageAPI = nil;
    self.callOpenReadAllMessageAPI = [[ CallOpenReadAllMessageAPI alloc] init];
    self.callOpenReadAllMessageAPI.delegate = self;
    [self.callOpenReadAllMessageAPI call:userId schoolid:schoolid];
}

-(void)callOpenReadAllMessageAPI:(CallOpenReadAllMessageAPI *)classObj data:(NSString *)data sucess:(BOOL)sucess{
    if (data != nil && sucess) {
        sortData = @"";
        [self getMessageDataWithPage:1 status:sortData];
    }
}

#pragma mark - CallInsetDataSystemNotificationPOSTAPI
- (void)callInsetDataSystemNotification:(NSString*)jsonString{
    self.callInsetDataSystemNotificationPOSTAPI = nil;
    self.callInsetDataSystemNotificationPOSTAPI = [[CallInsetDataSystemNotificationPOSTAPI alloc] init];
    self.callInsetDataSystemNotificationPOSTAPI.delegate = self;
    [self.callInsetDataSystemNotificationPOSTAPI call:jsonString];
}

#pragma mark - CallSTSendTokenAndLanguageAPI
- (void) doSendTokenAndLanguage:(NSString*)token language:(NSString*)language{
    if ( self.callSTSendTokenAndLanguageAPI != nil) {
        self.callSTSendTokenAndLanguageAPI = nil;
    }
    self.callSTSendTokenAndLanguageAPI = [[CallSTSendTokenAndLanguageAPI alloc] init];
    self.callSTSendTokenAndLanguageAPI.delegate = self;
    [self.callSTSendTokenAndLanguageAPI call:token language:language];
}
- (void)callSTSendTokenAndLanguageAPI:(CallSTSendTokenAndLanguageAPI *)classObj statusCode:(NSInteger)statusCode success:(BOOL)success{
    NSLog(@"%d",statusCode);
    
}

- (void)showAlertDialogWithChangPassword:(NSString *)message { // Dialog ChangPassword

    if(changPasswordDialog != nil && [changPasswordDialog isDialogShowing]) {
        [changPasswordDialog dismissDialog];
    }
    else {
        changPasswordDialog = [[ChangPasswordDialog alloc] init];
    }
    changPasswordDialog.delegate = self;
    [changPasswordDialog showDialogInView:self.view message:message];
    
}
//cormfrom back ChangPasswordDialog
- (void) doChangPassword:(ChangPasswordDialog *)changPasswordDialog{
    LoginChangPasswordViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginChangPasswordStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

-(void)showCardProfile{
        NSLog(@"xxxx = %@",self.view);
        if(self.cardPfileXIBViewController != nil) {
            if([self.cardPfileXIBViewController isDialogShowing]) {
                [self.cardPfileXIBViewController dismissDialog];
            }
        }
        else {
            self.cardPfileXIBViewController = [[CardPfileXIBViewController alloc] init];
            self.cardPfileXIBViewController.delegate = self;
        }
    UserViewController *newsFeed = [[UserViewController alloc]init];
    [self.view addSubview:newsFeed.view];
    
        [ self.cardPfileXIBViewController showDialogInView:self.view];
}
- (void) doClickMenu:(UserViewController *)classObj{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TeacherTimeTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TeacherTimeTableStoryBoard"];
    [self.slideMenuController changeMainViewController:vc close:YES];
}

#pragma mark - NFNewFutureSlideShowViewController
- (void)showNFNewFutureSlideShow{
    if(self.nFNewFutureSlideShowViewController != nil) {
        if([self.nFNewFutureSlideShowViewController isDialogShowing]) {
            [self.nFNewFutureSlideShowViewController dismissDialog];
        }
    }else{
        self.nFNewFutureSlideShowViewController = [[NFNewFutureSlideShowViewController alloc] init];
        //[self.view layoutIfNeeded];
        [self.nFNewFutureSlideShowViewController showNewFutureSlideShow:self.view];
    }
    
}

#pragma mark - StatisticsComeToSchoolViewController
- (void)removeMKDropdownMenu:(MKDropdownMenu *)mKDropdownMenu{
    dropdownMenu = mKDropdownMenu;
}


//- (void)statisticsDialog:(NSString *)title status:(ATTENDSTATUS)status times:(NSInteger)times totalTimes:(NSInteger)totalTimes{
//        if(self.creditBureauGraphDialog != nil && [self.creditBureauGraphDialog isDialogShowing]) {
//            [self.creditBureauGraphDialog dismissDialog];
//        }
//        self.creditBureauGraphDialog = [[CreditBureauGraphDialog alloc] init];
//        [self.creditBureauGraphDialog showDialogInView:self.view title:title status:status times:times totalTimes:totalTimes];
//}


@end
