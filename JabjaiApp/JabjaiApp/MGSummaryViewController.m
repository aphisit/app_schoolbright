//
//  MGSummaryViewController.m
//  JabjaiApp
//
//  Created by toffee on 4/20/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "MGSummaryViewController.h"
#import "APIURL.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "AlertDialogConfirm.h"
#import <AFNetworking/AFNetworking.h>

@interface MGSummaryViewController (){
    // The dialog
    AlertDialogConfirm *alertDialog;
    AlertDialog *alertDialogNoSecess;
    MGSuccessAlertDialog *successAlertDialog;
}
@property (strong, nonatomic) CallUpdateMargetPOSTAPI *callUpdateMargetPOSTAPI;
@end

@implementation MGSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString * resultConditionsListOrder = [_orderArray componentsJoinedByString:@"\n"];
    self.menuOrderLabel.text = resultConditionsListOrder;
    
    NSString * resultConditionsNumberOrder = [_amountOrderArray componentsJoinedByString:@"\n"];
    self.numberOrder.text = resultConditionsNumberOrder;
    
    NSString * resultConditionsPrice = [_totalPriceOrderArray componentsJoinedByString:@"\n"];
    self.priceLabel.text = resultConditionsPrice;
    
    self.totalLabel.text = [NSString stringWithFormat:@"%d",_totalPrice];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    _dateLabel.text = [dateFormatter stringFromDate:[NSDate date]];
   
    // Do any additional setup after loading the view.
}
-(void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.contentview.frame.size.width, self.contentview.frame.size.height);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - API Caller

- (void)updateOrder:(NSString *)jsonString {
    
    if(self.callUpdateMargetPOSTAPI != nil) {
        self.callUpdateMargetPOSTAPI = nil;
    }
    self.callUpdateMargetPOSTAPI = [[CallUpdateMargetPOSTAPI alloc] init];
    self.callUpdateMargetPOSTAPI.delegate = self;
    [self.callUpdateMargetPOSTAPI call:[UserData getSchoolId] shopId:_shopId userId:[UserData getUserID] jsonString:jsonString];
    
   
}
-(void)callUpdateMargetPOSTAPI:(CallUpdateMargetPOSTAPI *)classObj response:(NSDictionary *)response chackMonny:(NSString *)chackMonny success:(BOOL)success{
    [self stopIndicator];
    
    
    if(success) {
        if ([chackMonny isEqualToString:@"Low Monny"]) {
            NSString *alertMessage = @"ยอดเงินคงเหลือไม่เพียงพอสำหรับการทำรายการ";
            [self showAlertDialogNoSecess:alertMessage];
        }else{
           // NSString *alertMessage = @"ทำรายการสำเร็จ";
            [self showPaymentDialog:response];
        }

        
    }
    else {
        NSString *alertMessage = @"ทำรายการไม่สำเร็จ\nโปรดลองใหม่";
        [self showAlertDialogNoSecess:alertMessage];
    }
}
- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil) {
        if([alertDialog isDialogShowing]) {
            [alertDialog dismissDialog];
        }
    }
    else {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}
- (void)showAlertDialogNoSecess:(NSString *)message {
    
    if(alertDialogNoSecess != nil && [alertDialogNoSecess isDialogShowing]) {
        [alertDialogNoSecess dismissDialog];
    }
    else {
        alertDialogNoSecess = [[AlertDialog alloc] init];
        alertDialogNoSecess.delegate = self;
    }
    
    [alertDialogNoSecess showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showPaymentDialog:(NSDictionary *)data {
    
    if(successAlertDialog != nil && [successAlertDialog isDialogShowing]) {
        [successAlertDialog dismissDialog];
    }
    else {
        successAlertDialog = [[MGSuccessAlertDialog alloc] init];
        successAlertDialog.delegate = self;
    }
    
    [successAlertDialog showDialogInView:self.view data:data];
}
- (void)onAlertDialogClose {
    
    
    MGListMargetViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGListMargetViewStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    
}

- (IBAction)moveBack:(id)sender {
    MGListMenuViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGListMenuViewStoryboard"];
     viewController.shopId = _shopId;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
- (IBAction)actionSubmit:(id)sender {
    [self showIndicator];
    [self updateOrder:_orderStringJson];
}
@end
