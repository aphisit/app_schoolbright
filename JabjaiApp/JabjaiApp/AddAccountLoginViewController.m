//
//  AddAccountLoginViewController.m
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import AirshipKit;

#import "AddAccountLoginViewController.h"
#import "AddAccountMainViewController.h"
#import "SWRevealViewController.h"
#import "LoginUserModel.h"
#import "VerifyCodeDialog.h"
#import "AlertDialog.h"
#import "MultilineAlertDialog.h"
#import "UserData.h"
#import "UserInfoDBHelper.h"
#import "Utils.h"


@interface AddAccountLoginViewController () {
    
    VerifyCodeDialog *verifyCodeDialog;
    AlertDialog *alertDialog;
    MUListAllSchoolNameDialog *muListAllSchoolNameDialog;
    MultilineAlertDialog *multilineAlertDialog;
    CGPoint lastOffset;
    float keyboardHeight;
    
    long long schoolID;
    BOOL statusShowKeyboard;
}

@property (nonatomic, strong) CallGetLoginAPI *callGetLoginAPI;
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (nonatomic, strong) CallSTSendTokenAndLanguageAPI *callSTSendTokenAndLanguageAPI;
@property (nonatomic, strong) CallInsetDataSystemNotificationPOSTAPI *callInsetDataSystemNotificationPOSTAPI;
//@property (nonatomic, strong) UIView *activeField;

@end

@implementation AddAccountLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.schoolNameTextField.delegate = self;
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    statusShowKeyboard = NO;
    if(self.userArray == nil) {
        self.userArray = [self.dbHelper getUserDataWithMasterId:[UserData getMasterUserID]];
    }
    [self setLanguage];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}
- (void)viewWillDisappear:(BOOL)animated {
    [self deregisterFromKeyboardNotifications];
    [super viewWillDisappear:animated];
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer  *gradientHeaderView,*gradientBtnLogin;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeaderView = [Utils getGradientColorHeader];
    gradientHeaderView.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeaderView atIndex:0];
    
    //set color button next
    [self.btnLogin layoutIfNeeded];
    gradientBtnLogin = [Utils getGradientColorHeader];
    gradientBtnLogin.frame = self.btnLogin.bounds;
    [self.btnLogin.layer insertSublayer:gradientBtnLogin atIndex:0];
}

- (void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MANAGE_ACC_ADD",nil,[Utils getLanguage],nil);
    self.headerSchoolNameLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MANAGE_ACC_SCHOOL",nil,[Utils getLanguage],nil);
    self.headerUsernameLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MANAGE_ACC_USERNAME",nil,[Utils getLanguage],nil);
    self.headerPasswordLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MANAGE_ACC_PASSWORD",nil,[Utils getLanguage],nil);
    
    self.usernameTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_MANAGE_ACC_USERID",nil,[Utils getLanguage],nil);
    self.passwordTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_MANAGE_ACC_PASSWORD",nil,[Utils getLanguage],nil);
    self.schoolNameTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_MANAGE_ACC_SELECT_SCHOOL",nil,[Utils getLanguage],nil);
    
    
    [self.btnLogin setTitle:NSLocalizedStringFromTableInBundle(@"BTN_MANAGE_ACC_LOGIN",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    return YES;
}

#pragma KeyboardDelegate
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
            selector:@selector(keyboardWasShown:)
            name:UIKeyboardDidShowNotification object:nil];
   [[NSNotificationCenter defaultCenter] addObserver:self
             selector:@selector(keyboardWillBeHidden:)
             name:UIKeyboardWillHideNotification object:nil];
}
- (void)deregisterFromKeyboardNotifications {
 
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
- (void)keyboardWasShown:(NSNotification *)notification {
    if (statusShowKeyboard == NO) {
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        CGPoint buttonOrigin = self.btnLogin.frame.origin;
        CGFloat buttonHeight = self.btnLogin.frame.size.height;
        CGRect visibleRect = self.view.frame;
        visibleRect.size.height -= keyboardSize.height;
        if (buttonOrigin.y < visibleRect.size.height) {// fix size iphone screen some version
            //visibleRect.size.height -= visibleRect.size.height - buttonOrigin.y;
            buttonOrigin.y += visibleRect.size.height - buttonOrigin.y;
        }
        NSLog(@"%d ",CGRectContainsPoint(visibleRect, buttonOrigin));
        if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
             CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y+80 - visibleRect.size.height + buttonHeight);
            [self.scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
    
}
- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - CallSTSendTokenAndLanguageAPI
- (void) doSendTokenAndLanguage:(NSString*)token language:(NSString*)language{
    if ( self.callSTSendTokenAndLanguageAPI != nil) {
        self.callSTSendTokenAndLanguageAPI = nil;
    }
    self.callSTSendTokenAndLanguageAPI = [[CallSTSendTokenAndLanguageAPI alloc] init];
    self.callSTSendTokenAndLanguageAPI.delegate = self;
    [self.callSTSendTokenAndLanguageAPI call:token language:language];
}
- (void)callSTSendTokenAndLanguageAPI:(CallSTSendTokenAndLanguageAPI *)classObj statusCode:(NSInteger)statusCode success:(BOOL)success{
    NSLog(@"%d",statusCode);
}

#pragma mark - Call API
- (void) getLoginWithUserName:(NSString *)userName password:(NSString *)password schoolID:(long long)schoolID {
    self.callGetLoginAPI = nil;
    self.callGetLoginAPI = [[CallGetLoginAPI alloc] init];
    self.callGetLoginAPI.delegate = self;
    [self.callGetLoginAPI call:userName password:password schoolID:schoolID];
    [self showIndicator];
}

#pragma mark - CallGetLoginAPIDelegate
- (void)callGetLoginAPI:(CallGetLoginAPI *)classObj data:(LoginUserModel *)data username:(NSString *)username password:(NSString *)password success:(BOOL)success {
    [self stopIndicator];
    self.btnLogin.userInteractionEnabled = YES;
    if(success && data != nil) {
        MultipleUserModel *userModel = [[MultipleUserModel alloc] init];
        userModel.masterId = [UserData getMasterUserID];
        userModel.slaveId = data.userId;
        userModel.schoolId = data.schoolId;
        userModel.userType = data.userType;
        userModel.academyType = data.academyType;
        userModel.imageUrl = data.imageUrl;
        userModel.firstName = data.firstName;
        userModel.lastName = data.lastName;
        userModel.gender = data.gender;
        userModel.clientToken = data.token;
        userModel.username = username;
        userModel.password = password;
        [self insertUserIntoDB:userModel];
    }
    else {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *message1 = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_USER_PASS_INCORRECT",nil,[Utils getLanguage],nil);
        NSString *message2 = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_STUDENT",nil,[Utils getLanguage],nil);
        NSString *message3 = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_TEACHER",nil,[Utils getLanguage],nil);
        NSString *textMessage = [NSString stringWithFormat:@"%@\n\n%@\n%@", message1, message2, message3];
        [self showMultilineAlertDialogWithTitle:alertTitle message:textMessage];
    }
}

#pragma mark - CallInsetDataSystemNotificationPOSTAPI
- (void)callInsetDataSystemNotification:(NSString*)jsonString{
    self.callInsetDataSystemNotificationPOSTAPI = nil;
    self.callInsetDataSystemNotificationPOSTAPI = [[CallInsetDataSystemNotificationPOSTAPI alloc] init];
    self.callInsetDataSystemNotificationPOSTAPI.delegate = self;
    [self.callInsetDataSystemNotificationPOSTAPI call:jsonString];
}

- (void)onUnRegisterFinger:(CallGetLoginAPI *)classObj confirmCode:(NSString *)confirmCode {
    [self stopIndicator];
    self.btnLogin.userInteractionEnabled = YES;
    NSString *title = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_FINGERPRINTS",nil,[Utils getLanguage],nil);
    [self showVerifyCodeDialogWithTitle:title verifyCode:confirmCode];
}

- (void)incorrectUserNameOrPassword:(CallGetLoginAPI *)classObj {
    [self stopIndicator];
    self.btnLogin.userInteractionEnabled = YES;
    NSString *alertTitle = @"แจ้งเตือน";
    NSString *message1 = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_USER_PASS_INCORRECT",nil,[Utils getLanguage],nil);
    NSString *message2 = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_STUDENT",nil,[Utils getLanguage],nil);
    NSString *message3 = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_TEACHER",nil,[Utils getLanguage],nil);
    NSString *textMessage = [NSString stringWithFormat:@"%@\n\n%@\n%@", message1, message2, message3];
    [self showMultilineAlertDialogWithTitle:alertTitle message:textMessage];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    if(alertDialog == nil) {
        alertDialog = [[AlertDialog alloc] init];
    }
    else if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showMultilineAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    if(multilineAlertDialog == nil) {
        multilineAlertDialog = [[MultilineAlertDialog alloc] init];
    }
    else if(multilineAlertDialog != nil && [multilineAlertDialog isDialogShowing]) {
        [multilineAlertDialog dismissDialog];
    }
    [multilineAlertDialog showDialogInView:self.view title:title message:message];
}

- (void)showVerifyCodeDialogWithTitle:(NSString *)title verifyCode:(NSString *)verifyCode {
    if(verifyCodeDialog == nil) {
        verifyCodeDialog = [[VerifyCodeDialog alloc] init];
    }
    else if(verifyCodeDialog != nil && [verifyCodeDialog isDialogShowing]) {
        [verifyCodeDialog dismissDialog];
    }
    [verifyCodeDialog showDialogInView:self.view title:title verifyCode:verifyCode];
}

#pragma mark - Utility
- (void)insertUserIntoDB:(MultipleUserModel *)userModel {
    BOOL isDuplicated = NO;
    if(self.userArray != nil) {
        for(MultipleUserModel *model in self.userArray) {
            
            if([model getMasterId] == [userModel getMasterId] && [model getSlaveId] == [userModel getSlaveId]) {
                isDuplicated = YES;
                break;
            }
        }
    }
    if(isDuplicated) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_ALREADY",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else if([userModel getSlaveId] == [UserData getMasterUserID]) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_NOT_ADD_ACCOUNT",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else {
        BOOL success = [self.dbHelper insertData:userModel];
        if(success) {
            NSString *tag = [NSString stringWithFormat:@"%lld", [userModel getSlaveId]];
            [[UAirship push] addTag:tag];
            [[UAirship push] updateRegistration];
            NSString *UDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];  //UDID Device
            [self callInsetDataSystemNotification:[NSString stringWithFormat:@"{\"Token\":\"%@\",\"UserID\":%d,\"SchoolID\":%lld,\"Imei\":\"%@\",\"System\":\"IOS\"}",tokenStr,[UserData getUserID],[UserData getSchoolId],UDID]];
            [self backToPreviousPage];
        }
        else {
            NSString *alertTitle = @"แจ้งเตือน";
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_INCORRECT",nil,[Utils getLanguage],nil);
            [self showAlertDialogWithTitle:alertTitle message:alertMessage];
        }
    }
}
- (void)backToPreviousPage {
    //AddAccountMainStoryboard
    AddAccountMainViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAccountMainStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - UITextFieldDelegate
-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField.tag == 2) {
        statusShowKeyboard = YES;
        [self showlistAllSchoolNameDialog];
        return NO;
    }
    return YES;
}

#pragma mark - MUListAllSchoolNameDialog
-(void)showlistAllSchoolNameDialog{
    if(muListAllSchoolNameDialog != nil && [muListAllSchoolNameDialog isDialogShowing]) {
        [muListAllSchoolNameDialog dismissDialog];
    }
    muListAllSchoolNameDialog = [[MUListAllSchoolNameDialog alloc] init];
    muListAllSchoolNameDialog.delegate = self;
    [muListAllSchoolNameDialog showDialogInView:self.view];
}

-(void)onAlertDialogClose:(MUAllSchoolModel *)listSchoolNameModel{
    statusShowKeyboard = NO;
    self.schoolNameTextField.text = [listSchoolNameModel getSchoolName];
    schoolID = [listSchoolNameModel getSchoolID];
}
- (void)touchesBeganClose{
    statusShowKeyboard = NO;
}

- (IBAction)actionLogin:(id)sender {
    [self doSendTokenAndLanguage:tokenStr language:[UserData getChangLanguage]];
    NSString *userName = self.usernameTextField.text;
    NSString *password = self.passwordTextField.text;
    NSString *schoolName = self.schoolNameTextField.text;
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    if (schoolName.length == 0){
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_ENTER_SCHOOL",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else if(userName.length == 0) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_ENTER_NAME",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else if(password.length == 0) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_ENTER_PASS",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else {
        self.btnLogin.userInteractionEnabled = NO;
        [self getLoginWithUserName:userName password:password schoolID:schoolID];
    }
}
- (IBAction)moveBack:(id)sender {
    [self backToPreviousPage];
}



@end
