//
//  AboutViewController.m
//  JabjaiApp
//
//  Created by mac on 12/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "AboutViewController.h"
#import "SWRevealViewController.h"
#import "SettingViewController.h"
#import "Utils.h"
@interface AboutViewController (){
}

@end

@implementation AboutViewController



- (void)viewDidLoad {
    [super viewDidLoad];

    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *appVersion = [infoDict objectForKey:@"CFBundleShortVersionString"];
    self.versionLabel.text = [[NSString alloc] initWithFormat:@"%@ : %@",NSLocalizedStringFromTableInBundle(@"LABEL_ABOUNT_VERSION",nil,[Utils getLanguage],nil), appVersion];
    [self setLanguage];
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void)doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

- (void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ABOUNT",nil,[Utils getLanguage],nil);
    self.headerCompanyLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ABOUNT_JABJAI",nil,[Utils getLanguage],nil);
    self.headerAddressLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ABOUNT_ADDRESS",nil,[Utils getLanguage],nil);
    self.headerWebsiteLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ABOUNT_WEBSITE",nil,[Utils getLanguage],nil);
    self.headerPhoneLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ABOUNT_PHONE",nil,[Utils getLanguage],nil);
    self.headerEmailLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ABOUNT_EMAIL",nil,[Utils getLanguage],nil);
    self.headerVersionLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ABOUNT_VERSION",nil,[Utils getLanguage],nil);

    self.addressLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ABOUNT_ADDRESSS",nil,[Utils getLanguage],nil);
    self.websiteTextView.text = @"https://www.schoolbright.co";
    self.emailLabel.text = @"marketing@jabjai.school\nnarin@jabjai.school";
    self.phoneTextView.text = @"02-029-9373 / 091-823-3139";

}

- (IBAction)moveBack:(id)sender {
    SettingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];

}
@end
