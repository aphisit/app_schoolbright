//
//  CallRPUpdateNewPinPOSTAPI.h
//  JabjaiApp
//
//  Created by toffee on 6/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CallRPUpdateNewPinPOSTAPI;
@protocol CallRPUpdateNewPinPOSTAPIDelegate <NSObject>
@optional

- (void)callRPUpdateNewPinPOSTAPI:(CallRPUpdateNewPinPOSTAPI *)classObj response:(NSString *)response success:(BOOL)success;

@end
@interface CallRPUpdateNewPinPOSTAPI : NSObject
@property (nonatomic, weak) id<CallRPUpdateNewPinPOSTAPIDelegate> delegate;

- (void)call:(NSString*)jsonString;


@end
