//
//  ReportCheckSubjectCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CFSCalendar.h"

#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "TASubjectModel.h"
#import "CallGetTAHistoryStudentStatusListAPI.h"
#import "SlideMenuController.h"
#import "ReportCheckSubjectHistoryStudentAllViewController.h"
#import "CallTAGetStatusCalendaAPI.h"
@interface ReportCheckSubjectCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate,CFSCalendarDelegateAppearance, UITableViewDelegate, UITableViewDataSource ,CallGetTAHistoryStudentStatusListAPIDelegate, SlideMenuControllerDelegate,CallTAGetStatusCalendaAPIDelegate>

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<TASubjectModel *> *subjectArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
@property (nonatomic) long long selectedClassLevelId;
@property (nonatomic) long long selectedClassroomId;
@property (nonatomic) NSString *selectedSubjectId;
@property (nonatomic) NSString *nameClassRoom;
@property (nonatomic) NSString *nameSubject;;
//@property (weak, nonatomic) id<TAReportCalendarViewControllerDelegate> delegate;

@property (weak ,nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UILabel *headerOnTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAbsenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSickLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPersonalLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEventLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerUndefinedLabel;

@property (weak, nonatomic) IBOutlet UILabel *onTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *lateLabel;
@property (weak, nonatomic) IBOutlet UILabel *absenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *sickLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *undefinedLabel;
@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;
@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *viewNextAction;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailConstraint;


- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)moveBack:(id)sender;
- (IBAction)openDetail:(id)sender;

@end
