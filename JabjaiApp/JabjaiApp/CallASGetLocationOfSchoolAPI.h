//
//  CallASGetLocationOfSchoolAPI.h
//  JabjaiApp
//
//  Created by Mac on 11/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ACLocationModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallASGetLocationOfSchoolAPI;
@protocol CallASGetLocationOfSchoolAPIDelegate <NSObject>

- (void)callASGetLocationOfSchoolAPI:(CallASGetLocationOfSchoolAPI *)classObj data:(ACLocationModel *)dada success:(BOOL)success;

@end
@interface CallASGetLocationOfSchoolAPI : NSObject
@property (nonatomic,weak)id<CallASGetLocationOfSchoolAPIDelegate> delegate;
- (void) call:(long long)schoolID studentID:(long long)studentID;
@end

NS_ASSUME_NONNULL_END
