//
//  JHReadImageViewController.m
//  JabjaiApp
//
//  Created by Mac on 3/8/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "JHReadImageViewController.h"
#import "Utils.h"
#import "AlertDialogConfirm.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface JHReadImageViewController (){
    NSTimer *timer;
    AlertDialogConfirm *alertDialog;
    UIImageView* imgView;
    NSArray *imagesArray;
}

@end

@implementation JHReadImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.scrollView.delegate = self;
    // Do any additional setup after loading the view from its nib.
}


- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}

-(void)showPagesReadImageInView:(UIView *)targetView ImageArray:(NSArray *)imageArray{
    imagesArray = imageArray;
    NSLog(@"System Version is %@",[[UIDevice currentDevice] systemVersion]);
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    }
    else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height)];
    }
    [targetView addSubview:self.view];
    [self.view layoutIfNeeded];
    [self doPageControl:imageArray];
}

-(void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
}

- (IBAction)closeAction:(id)sender {
    
    [self removeDialogFromView];
}

- (IBAction)saveFileAction:(id)sender{
    [self showIndicator];
    self.saveImageBtn.enabled = NO;
    [self doSaveImage];
}

-(void)doPageControl:(NSArray*)imageArray{
    CGRect frame = CGRectMake(0, 0, 0, 0 );
    [self.scrollView layoutIfNeeded];
    self.pageControl.numberOfPages = imageArray.count;
    for (int i = 0; i < imageArray.count; i++) {
        frame.origin.x = self.scrollView.frame.size.width * i;
        frame.size = self.scrollView.frame.size;
        imgView = [[UIImageView alloc] initWithFrame:frame];
        [imgView sd_setImageWithURL:[imagesArray objectAtIndex:i]];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        [self.scrollView addSubview:imgView];
    }
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * imageArray.count, self.scrollView.frame.size.height);
    self.scrollView.delegate = self;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger pageNumber = self.scrollView.contentOffset.x / self.scrollView.frame.size.width;
    self.pageControl.currentPage = pageNumber;
}

-(void)doSaveImage{
    self.saveImageBtn.enabled = YES;
    [imgView sd_setImageWithURL:[imagesArray objectAtIndex:self.pageControl.currentPage]];
    UIImageWriteToSavedPhotosAlbum(imgView.image, self, nil, nil);
    [self stopIndicator];
    NSString *alertMessage = @"บันทึกเสร็จสิ้น";
    [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *mainView = window.rootViewController.view;
    [alertDialog showDialogInView:mainView title:title message:message];
}

-(void)onAlertDialogClose{
    [imgView setImage:nil];
    [self doPageControl:imagesArray];
}



@end
