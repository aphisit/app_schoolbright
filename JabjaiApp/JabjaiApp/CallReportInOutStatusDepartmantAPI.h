//
//  CallReportInOutStatusDepartmantAPI.h
//  JabjaiApp
//
//  Created by toffee on 6/3/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReportInOutStatusDepartmentModel.h"

NS_ASSUME_NONNULL_BEGIN
@class CallReportInOutStatusDepartmantAPI;
@protocol CallReportInOutStatusDepartmantAPIDelegat <NSObject>

@optional
- (void) callReportInOutStatusDepartmantAPI:(CallReportInOutStatusDepartmantAPI *)classObj dataDepartment:(NSMutableArray<ReportInOutStatusDepartmentModel *>*)dataDepartment success:(BOOL)success;

@end
@interface CallReportInOutStatusDepartmantAPI : NSObject
@property (nonatomic,weak) id<CallReportInOutStatusDepartmantAPIDelegat> delegate;
- (void)call:(long long)schoolId dayReports:(NSString*)dayReports departmentId:(long long)departmentId;
@end

NS_ASSUME_NONNULL_END
