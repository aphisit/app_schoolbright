//
//  TimeTableStudentViewController.h
//  JabjaiApp
//
//  Created by toffee on 8/5/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StudentSubjectTimeTableViewCell.h"
#import "TTSelectDayCollectionViewCell.h"
#import "StudentSubjectDetailTimeTableViewCell.h"
#import "SlideMenuController.h"
#import "CallStudentTimeTableAPI.h"
#import "CallStudentTimeTableDetailAPI.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
NS_ASSUME_NONNULL_BEGIN

@interface TimeTableStudentViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate ,UITableViewDataSource,SlideMenuControllerDelegate,CallStudentTimeTableAPIDelegate,CallStudentTimeTableDetailAPIDelegate, CallCRGetStatusClosedForRenovationAPIDelegate >

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *monthLable;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *showHaveNotSubject;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *enableLoadDataView;
@property (weak, nonatomic) IBOutlet UIView *loadingView;

- (IBAction)openDrawer:(id)sender;
@end

NS_ASSUME_NONNULL_END
