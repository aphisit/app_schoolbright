//
//  CallGetTASubjectForReportAPI.m
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetTASubjectForReportAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetTASubjectForReportAPI () {
    NSInteger connectCounter;
    NSMutableArray<TASubjectModel *> *subjectArray;
}

@end

@implementation CallGetTASubjectForReportAPI

- (instancetype)init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(long long)schoolId classroomId:(long long)classroomId {
    [self getDataFromServer:schoolId classroomId:classroomId];
}

- (void)getDataFromServer:(long long)schoolId classroomId:(long long)classroomId {
    NSString *urlString = [APIURL getTASubjectListWithSchoolId:schoolId classroomId:classroomId];
    NSURL *url = [NSURL URLWithString:urlString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataFromServer:schoolId classroomId:classroomId];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId classroomId:classroomId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if([returnedData isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDict = returnedData;
                if([dataDict objectForKey:@"status"] != nil && [[dataDict objectForKey:@"status"] integerValue] == -1) {
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(notArrangeTimeTable:)]) {
                        [self.delegate notArrangeTimeTable:self];
                    }
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId classroomId:classroomId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                if(subjectArray != nil) {
                    [subjectArray removeAllObjects];
                    subjectArray = nil;
                }
                subjectArray = [[NSMutableArray alloc] init];
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    NSString *subjectId = [dataDict objectForKey:@"planeid"];
                    NSString *subjectName = [dataDict objectForKey:@"planename"];
                    TASubjectModel *model = [[TASubjectModel alloc] init];
                    model.subjectId = subjectId;
                    model.subjectName = subjectName;
                    [subjectArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTASubjectForReportAPI:data:success:)]) {
                    [self.delegate callGetTASubjectForReportAPI:self data:subjectArray success:YES];
                }
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTASubjectForReportAPI:data:success:)]) {
                [self.delegate callGetTASubjectForReportAPI:self data:nil success:NO];
            }
        }
    }];
}

@end
