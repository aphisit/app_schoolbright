//
//  SNSelectPersonnelViewController.m
//  JabjaiApp
//
//  Created by toffee on 9/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNSelectPersonnelViewController.h"
#import "APIURL.h"
#import "UserData.h"
#import "Utils.h"
#import "AlertDialog.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SNSelectPersonnelViewController (){

    NSMutableArray *employeesid;
    NSMutableArray *personnelId;
    NSMutableArray *userAcceptArray;
    NSMutableArray *userImageArray;
    AlertDialog *alertDialog;
    NSBundle *myLangBundle;
}

@end

@implementation SNSelectPersonnelViewController{
    NSMutableArray *data1;
    NSMutableArray *isSelected;
}
@synthesize personnelImageArray;
static NSString *cellIdentifier = @"Cell";
- (void)viewDidLoad {
    [super viewDidLoad];

    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSStudentNameRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self doPersonnel];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    [self setLanguage];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
}

-(void)setLanguage{
    
    self.headerSendNewsLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS",nil,myLangBundle,nil);
    self.headerPersonnelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_EMPLOYEES",nil,myLangBundle,nil);
    [self.headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

-(void)doPersonnel{
    [self showIndicator];
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getPersonnelAllUrl:schoolID] ;
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self stopIndicator];
        id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        NSArray *returnedArray = returnedData;
        self.addPersonnelArrray = [[NSArray alloc] init];
        data1 = [[NSMutableArray alloc] init];
        isSelected = [[NSMutableArray alloc] init];
        personnelId = [[NSMutableArray alloc] init];
        employeesid = [[NSMutableArray alloc] init];
        userAcceptArray = [[NSMutableArray alloc] init];
        userImageArray = [[NSMutableArray alloc] init];
        NSData  *imaData;
        NSManagedObjectContext *context = [self managedObjectContext];
        for(int i=0; i<returnedArray.count; i++) {
            NSDictionary *dataDict = [returnedArray objectAtIndex:i];
            long long PersonnelId = [[dataDict objectForKey:@"emplyoeesid"] longLongValue];
            NSMutableString *PersonnelName;
            NSMutableString *pic;
            if(![[dataDict objectForKey:@"emplyoeesname"] isKindOfClass:[NSNull class]]) {
                PersonnelName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"emplyoeesname"]];
            }
            else {
                PersonnelName = [[NSMutableString alloc] initWithString:@""];
            }
            
            if(![[dataDict objectForKey:@"pic"] isKindOfClass:[NSNull class]]) {
                pic = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"pic"]];
            }
            else {
                pic = [[NSMutableString alloc] initWithString:@""];
            }
            CFStringTrimWhitespace((__bridge CFMutableStringRef) PersonnelName);
            [data1 addObject:PersonnelName ];
            [userImageArray addObject:pic];
            [isSelected addObject: @"NO"];
            [personnelId addObject:@(PersonnelId)];
            _addPersonnelArrray = data1;
        }
//        NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"Entity"];
//        personnelImageArray = [[context executeFetchRequest:fetch error:nil]mutableCopy];
        [self.tableView reloadData];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.addPersonnelArrray != nil) {
        
        return self.addPersonnelArrray.count;
    }
    else {
        //        self.doTheacher;
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BSStudentNameRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    NSLog(@"isSelect = %@",isSelected);
  
    
    cell.delegate = self;
    cell.index = indexPath.row;
    NSArray *array = [[NSString stringWithFormat:@"%@", [self.addPersonnelArrray objectAtIndex:indexPath.row]] componentsSeparatedByString:@" "];
    NSMutableString *lastName = [[NSMutableString alloc] init];
    if (array.count>1) {
        for (int i = 0; i < array.count; i++) {
            if (i>0) {
                [lastName appendFormat:@"%@ ",array[i]];
            }
        }
        cell.titleLabel.text = array[0];
        cell.titleLastname.text = lastName;
    }else{
        cell.titleLabel.text = array[0];
    }
    cell.runNumber.text = [NSString stringWithFormat:@"%d.",indexPath.row+1];
    [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@", [userImageArray objectAtIndex:indexPath.row ]]]] placeholderImage:nil options:SDWebImageRefreshCached
        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        if (image == nil || error) {
                                          cell.userImageView.image = [UIImage imageNamed:@"ic_user_info"];
                                        }
        }];
   
    cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height /2;
    cell.userImageView.layer.masksToBounds = YES;
    cell.userImageView.layer.borderWidth = 0;
    
    if([[isSelected objectAtIndex:indexPath.row]  isEqual: @"YES"]) {
        [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
    }
    else {
        [cell setRadioButtonClearSelected];
    }
    [cell.shadowViewCell.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.shadowViewCell.layer setShadowOpacity:0.3];
    [cell.shadowViewCell.layer setShadowRadius:3.0];
    [cell.shadowViewCell.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 100; /* Device is iPad */
    }else{
        return 90;
    }
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"number = %d",index);
    if([isSelected[indexPath.row] isEqualToString : @"YES"]){
        isSelected[indexPath.row] = @"NO";
        [employeesid removeObject:[personnelId objectAtIndex:indexPath.row]];
        [userAcceptArray removeObject:[data1 objectAtIndex:indexPath.row]];
    }else{
        isSelected[indexPath.row] = @"YES";
        [employeesid addObject:[personnelId objectAtIndex:indexPath.row]];
        [userAcceptArray addObject:[data1 objectAtIndex:indexPath.row]];
    }
    NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)onPressRadioButton:(BSStudentNameRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index{
    
    NSLog(@"number = %d",index);
    if([isSelected[index] isEqualToString : @"YES"]){
        isSelected[index] = @"NO";
        [employeesid removeObject:[personnelId objectAtIndex:index]];
        [userAcceptArray removeObject:[data1 objectAtIndex:index]];
    }else{
        isSelected[index] = @"YES";
        [employeesid addObject:[personnelId objectAtIndex:index]];
        [userAcceptArray addObject:[data1 objectAtIndex:index]];
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)moveBack:(id)sender {
    SNLevelStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SendNewsStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (IBAction)actionNext:(id)sender {
    
    if (employeesid.count == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_LEAST",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
    }else{
        SNTimeOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        viewController.sendType = self.sendType;
        viewController.sendGroup = self.sendGroup;
        viewController.newsType = self.newsType;
        viewController.employeesid = employeesid;
        viewController.userAcceptArray = userAcceptArray;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
}
@end
