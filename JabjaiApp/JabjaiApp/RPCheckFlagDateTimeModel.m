//
//  RPCheckFlagDateTimeModel.m
//  JabjaiApp
//
//  Created by toffee on 18/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "RPCheckFlagDateTimeModel.h"
@implementation RPCheckFlagDateTimeModel
@synthesize date = _date;
@synthesize time = _time;
@synthesize status = _status;

- (void)setDate:(NSString *)date{
    _date = date;
}

- (void)setTime:(NSString *)time{
    _time = time;
}

- (void)setStatus:(NSString *)status{
    _status = status;
}

- (NSString*) getDate{
    return _date;
}

- (NSString*)getTime{
    return _time;
}

- (NSString*)getStatus{
    return _status;
}

@end
