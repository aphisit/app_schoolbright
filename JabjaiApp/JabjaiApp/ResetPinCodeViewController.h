//
//  ResetPinCodeViewController.h
//  JabjaiApp
//
//  Created by toffee on 6/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallRPUpdateNewPinPOSTAPI.h"
#import "AlertDialogConfirm.h"
#import "SlideMenuController.h"

@protocol ResetPinCodeViewControllerDelegate <NSObject>
@optional

- (void)successConfirmPin;
- (void)onMoveBack; //back to RPOriginPinCodeViewController

@end
static NSString *ADD_PINCODE = @"ADD_PINCODE";
static NSString *CONFIRM_PINCODE = @"CONFIRM_PINCODE";
@interface ResetPinCodeViewController : UIViewController <ResetPinCodeViewControllerDelegate, CallRPUpdateNewPinPOSTAPIDelegate,AlertDialogConfirmDelegate, SlideMenuControllerDelegate>

@property (nonatomic, retain) id<ResetPinCodeViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *headerPinLabel;
@property (strong, nonatomic) IBOutlet UILabel *confirmPinUnfair;



@property (weak, nonatomic) IBOutlet UIView *passView1;
@property (weak, nonatomic) IBOutlet UIView *passView2;
@property (weak, nonatomic) IBOutlet UIView *passView3;
@property (weak, nonatomic) IBOutlet UIView *passView4;
@property (weak, nonatomic) IBOutlet UIView *passView5;
@property (weak, nonatomic) IBOutlet UIView *passView6;

- (IBAction)actionButtonNumber:(UIControl*)sender;
- (IBAction)openDarwer:(id)sender;
- (IBAction)moveBack:(id)sender;

- (void)showDialogInView:(UIView *)targetView pinOld:(NSString*)pinOld ;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end
