//
//  ReportSendNewsPersonRecieveTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 4/9/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportSendNewsPersonRecieveTableViewCell.h"

@implementation ReportSendNewsPersonRecieveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.personImage.layer.cornerRadius = self.personImage.frame.size.width / 2;
    self.personImage.clipsToBounds = YES;
    self.personImage.layer.shouldRasterize = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
