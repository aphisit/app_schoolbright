//
//  MargetListModel.h
//  JabjaiApp
//
//  Created by toffee on 4/10/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MargetListModel : NSObject
@property (nonatomic) long long shopId;
@property (nonatomic) NSString *shopName;
@property (nonatomic) NSString *picture;

- (long long)getShopId;
- (NSString *)getShopName;
- (NSString *)getPicture;

@end
