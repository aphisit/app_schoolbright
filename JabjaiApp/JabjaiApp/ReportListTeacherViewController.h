//
//  ReportListTeacherViewController.h
//  JabjaiApp
//
//  Created by mac on 4/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"
#import "CAPSPageMenu.h"

#import "ReportStudentViewController.h"
#import "ReportExcutiveViewController.h"
#import "ReportPersonViewController.h"
//report student viewcontroller
#import "ReportCheckNameViewController.h"
#import "ReportLeaveAllDataSelectClassViewController.h"
#import "ReportBehaviorAllDataSelectViewController.h"
#import "ReportOrderHomeWorkViewController.h"
#import "ReportSendNewsListViewController.h"
// report excutive viewcontroller
#import "ReportInOutAllDataViewController.h"
#import "ReportUnCheckNameViewController.h"
#import "ReportRefillMoneyAllDataViewController.h"
#import "ReportPurchasingAllDataViewController.h"
#import "ReportFeeViewController.h"
//report personal viewcontroller
#import "ReportInOutViewController.h"
#import "ReportLeaveViewController.h"
#import "ReportRefillMoneyViewController.h"
#import "ReportPurchasingViewController.h"

@interface ReportListTeacherViewController : UIViewController <SlideMenuControllerDelegate, CAPSPageMenuDelegate, ReportStudentViewControllerDelegate,ReportExcutiveViewControllerDelegate, ReportPersonViewControllerDelegate>

@property (nonatomic) NSInteger page;
@property (strong, nonatomic) NSMutableArray* authorizeMenuArray;
@property (strong, nonatomic) NSMutableArray* authorizeSubMenuArray;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, strong) ReportStudentViewController *studentViewcontroller;
@property (nonatomic, strong) ReportExcutiveViewController *excutiveViewController;
@property (nonatomic, strong) ReportPersonViewController *personViewcontroller;

- (IBAction)openDrawer:(id)sender;

- (void)showFinishAlertDialogWithTitle:(NSString *)title message:(NSString *)message;

@end
