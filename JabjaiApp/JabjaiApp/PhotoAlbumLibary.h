//
//  PhotoAlbumLibary.h
//  JabjaiApp
//
//  Created by toffee on 3/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoAlbumCollectionViewCell.h"
#import <Photos/Photos.h>
NS_ASSUME_NONNULL_BEGIN
@class PhotoAlbumLibary;
@protocol PhotoAlbumLibaryDelegate <NSObject>
- (void)replyPhotoAlbum:(PhotoAlbumLibary *)classObj imageArray:(NSMutableArray*)imageArray;
@end

@interface PhotoAlbumLibary : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) id <PhotoAlbumLibaryDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UICollectionView *albumCollectionView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerAllpictureLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerDoneLabel;

- (IBAction)moveAction:(id)sender;
- (IBAction)chooseAction:(id)sender;
- (void)showPhotoAlbumView:(UIView *)targetView ;
- (void)dismissPhotoAlbum;
- (BOOL)isPhotoAlbumShowing;
- (void)allPhotosCollected:(NSArray*)imgArray;
@end

NS_ASSUME_NONNULL_END
