//
//  JHSubjectModel.m
//  JabjaiApp
//
//  Created by toffee on 11/22/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "JHSubjectModel.h"

@implementation JHSubjectModel
-(NSString *)getSubjectID {
    return self.subjectID;
}
-(NSString *)getSubjectName {
    return self.subjectName;
}

@end
