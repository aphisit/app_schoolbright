//
//  SNClassRoomview.m
//  JabjaiApp
//
//  Created by toffee on 9/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNClassRoomview.h"
#import "SNClassLevelViewController.h"
#import "SchoolClassroomModel.h"

@implementation SNClassRoomview

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setRadioButtonSelected {
    [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
}

- (void)setRadioButtonSelectedWithType:(int) type {
    
    if(type == RADIO_TYPE_RED) {
        [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_red"] forState:UIControlStateNormal];
    }
    else {
        [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
    }
}
- (void)setRadioButtonClearSelected {
    [self.radioButton setImage:[UIImage imageNamed:@"ic_radio_clear"] forState:UIControlStateNormal];
}


- (IBAction)actionPressRadioButton:(id)sender {
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onPressRadioButton:atIndex:roomID:tag:roomString:)]) {
        SNClassLevelViewController *model = self.delegate;
        SchoolClassroomModel *roomID1 = [model.dict objectForKey:[NSString stringWithFormat:@"%@", @([sender tag])]][_index-1];
        NSLog(@"ID = %d",[[roomID1 classroomID] integerValue] );
        NSLog(@"tag = %d", [sender tag]);
        NSInteger roomId = [[roomID1 classroomID] integerValue];
        NSString *roomString = [roomID1 classroomName];
        [self.delegate onPressRadioButton:self atIndex:_index roomID:roomId tag:[sender tag] roomString:roomString ];
     }
}
@end
