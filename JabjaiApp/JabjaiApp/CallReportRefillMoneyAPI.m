//
//  CallReportRefillMoneyAPI.m
//  JabjaiApp
//
//  Created by toffee on 10/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallReportRefillMoneyAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallReportRefillMoneyAPI () {
    NSInteger connectCounter;
   
}
@end
@implementation CallReportRefillMoneyAPI

- (void) call:(long long)schoolId date:(NSString *)date{
    connectCounter = 0;
    [self getDataGraphRefill:schoolId date:date];
}

#pragma mark - Get API Data
- (void)getDataGraphRefill:(long long)schoolId date:(NSString*)date {
    
    NSString *URLString = [APIURL getDataRefillMoney:schoolId date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataGraphRefill:schoolId date:date];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataGraphRefill:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataGraphRefill:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                NSMutableArray *timeArray;
                NSMutableArray *totalMoneyArray;
                
                if(returnedArray.count != 0) {
                    timeArray  = [[NSMutableArray alloc] init];
                    totalMoneyArray  = [[NSMutableArray alloc] init];
                    NSString *dateTime ;
                    for (int i = 0; i < returnedArray.count; i++) {
                        NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                        
                        if(![[dataDict objectForKey:@"Date"] isKindOfClass:[NSNull class]]) {
                            
                            dateTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Date"]];
                            NSArray *cutDateTime = [dateTime componentsSeparatedByString:@"T"];
                            NSArray *cutTime = [[cutDateTime objectAtIndex:1] componentsSeparatedByString:@":"];
                            NSString *time = [NSString stringWithFormat:@"%@.%@",cutTime[0],cutTime[1]];
                            [timeArray addObject:time];
                            
                        }
                        else {
                            dateTime = [[NSMutableString alloc] initWithString:@""];
                        }
                        
                        NSInteger totalMoney = [[dataDict objectForKey:@"TotatlMoney"] integerValue];
                        [totalMoneyArray addObject:[NSString stringWithFormat:@"%d",totalMoney]];
                        NSLog(@"xxxx");
                    }
                    
                    
                }
             
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportRefillMoneyAPI:timeArray:totalMoneyArray:success:)]) {
                    [self.delegate callReportRefillMoneyAPI:self timeArray:timeArray totalMoneyArray:totalMoneyArray success:YES];
                }
                
                
            
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportRefillMoneyAPI:timeArray:totalMoneyArray:success:)]) {
                    [self.delegate callReportRefillMoneyAPI:self timeArray:nil totalMoneyArray:nil success:NO];
                }
            }
            
        }
        
    }];
    
}
@end
