//
//  TFTuitionDetailViewController.m
//  JabjaiApp
//
//  Created by Mac on 2/10/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "TFTuitionDetailViewController.h"
#import "Utils.h"
#import "UserData.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface TFTuitionDetailViewController (){
    NSArray<TFProductModel *>*productArray;
    NSArray<TFInvoicesModel *>*invoicesArray;
    int paymentType;
    double pricePay;
}
@property (nonatomic, strong) CallTFGetTuitionDetailAPI *callTFGetTuitionDetailAPI;
@property (nonatomic, strong) AlertDialog *alertDialog;
@end

@implementation TFTuitionDetailViewController
    


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    [self.paymentBtn layoutIfNeeded];
    CAGradientLayer  *paymentGradient = [Utils getGradientColorNextAtion];
    paymentGradient.frame = self.paymentBtn.bounds;
    [self.paymentBtn.layer insertSublayer:paymentGradient atIndex:0];
    self.orderTableView.delegate = self;
    self.orderTableView.dataSource = self;
    self.orderTableView.tableFooterView = [[UIView alloc] init];
    [self.orderTableView registerNib:[UINib nibWithNibName:NSStringFromClass([SchoolFeePriceListTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    self.installmentTableView.delegate = self;
    self.installmentTableView.dataSource = self;
    self.installmentTableView.tableFooterView = [[UIView alloc] init];
    [self.installmentTableView registerNib:[UINib nibWithNibName:NSStringFromClass([TFInvoicesTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.radioPromptpayBtn setImage: [UIImage imageNamed:@"ic_radio_clear"]forState:UIControlStateNormal];
    [self.radioPromptpayBtn setImage: [UIImage imageNamed:@"ic_radio_green"]forState: UIControlStateSelected];
    [self.radioCreditBtn setImage: [UIImage imageNamed:@"ic_radio_clear"]forState:UIControlStateNormal];
    [self.radioCreditBtn setImage: [UIImage imageNamed:@"ic_radio_green"]forState: UIControlStateSelected];
    paymentType = -1;
    pricePay = 0.0;
    
    self.headerOrderLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_LIST",nil,[Utils getLanguage],nil)];
    self.headerInstallmentLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PARTIALLY_PAID",nil,[Utils getLanguage],nil);
    self.headerTotalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_TOTAL",nil,[Utils getLanguage],nil);
    self.headerRemainingBalanceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_BALANCE",nil,[Utils getLanguage],nil);
    self.headerPaymentLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PAYMANT",nil,[Utils getLanguage],nil);
    self.headerPrompayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PREOMPAY",nil,[Utils getLanguage],nil);
    self.headerCardLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_CARD",nil,[Utils getLanguage],nil);
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE",nil,[Utils getLanguage],nil);
    [self.headerPayBtn setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PAY",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    
    
    [self doCallTFGetTuitionDetailAPI:self.studentID invoicesId:self.invoiceID schoolId:self.schoolID];
    // Do any additional setup after loading the view.
}

//- (void) viewDidLayoutSubviews{
//    [self.headerView layoutIfNeeded];
//    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
//    gradient.frame = self.headerView.bounds;
//    [self.headerView.layer insertSublayer:gradient atIndex:0];
//    
//    [self.paymentBtn layoutIfNeeded];
//    CAGradientLayer  *paymentGradient = [Utils getGradientColorNextAtion];
//    paymentGradient.frame = self.paymentBtn.bounds;
//    [self.paymentBtn.layer insertSublayer:paymentGradient atIndex:0];
//}

#pragma mark - CallTFGetTuitionDetailAPI
- (void) doCallTFGetTuitionDetailAPI:(long long)studentId invoicesId:(long long)invoicesId schoolId:(long long)schoolId{
    [self showIndicator];
    if(self.callTFGetTuitionDetailAPI != nil) {
        self.callTFGetTuitionDetailAPI = nil;
    }
    self.callTFGetTuitionDetailAPI = [[CallTFGetTuitionDetailAPI alloc] init];
    self.callTFGetTuitionDetailAPI.delegate = self;
    [self.callTFGetTuitionDetailAPI call:studentId invoiceID:invoicesId schoolId:schoolId ];
}
- (void) callTFGetTuitionDetailAPI:(CallTFGetTuitionDetailAPI *)classObj data:(TFitionFeesModel *)data success:(BOOL)success{
    NSLog(@"xxx");
    [self stopIndicator];
    if (data != nil && success) {
        double sum = 0;
        productArray = data.productListArray;
        invoicesArray = data.invoicesArray;
        NSDate *date = [Utils parseServerDateStringToDate:data.issueDate];
        self.dateLabel.text = [Utils dateInStringFormat:date format:@"dd/MM/yyyy"];
        self.schoolNameTHLabel.text = data.schoolThaiName;
        self.schoolNameENLabel.text = data.schoolEngName;
        
        
        if ([[UserData getChangLanguage]isEqual:@"en"]) {
            self.yearLabel.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_YEAR",nil,[Utils getLanguage],nil),[@([data.yearName integerValue] - 543)stringValue]];
        }else{
            self.yearLabel.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_YEAR",nil,[Utils getLanguage],nil),data.yearName];
        }
        
        
        self.termLabel.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_SEMESTER",nil,[Utils getLanguage],nil),data.termName];
        self.studentNameLabel.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_NAME",nil,[Utils getLanguage],nil),data.studentName];
        self.studentIDLabel.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_ID",nil,[Utils getLanguage],nil),data.studentCode];
        self.classLabel.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_CLASS",nil,[Utils getLanguage],nil),data.levelName];
        self.classroomLabel.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_ROOM",nil,[Utils getLanguage],nil),data.className];
        [self.schoolImage sd_setImageWithURL:[NSURL URLWithString:data.imageUrl]];
        for (int i = 0; i < data.invoicesArray.count; i++) {
            TFInvoicesModel *model = [[TFInvoicesModel alloc] init];
            model = [data.invoicesArray objectAtIndex:i];
            sum = sum + model.amount;
        }
        pricePay = data.price - sum;
        NSNumber *remainingBalanceNumber = [NSNumber numberWithFloat:data.price - sum];
        NSNumber *totalPriceNumber = [NSNumber numberWithFloat:data.price];
        NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
        [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numFormatter setUsesSignificantDigits:NO];
        [numFormatter setMaximumFractionDigits:2];
        [numFormatter setMinimumFractionDigits:2];
        [numFormatter setUsesGroupingSeparator:YES];
        [numFormatter setGroupingSeparator:@","];
        [numFormatter setDecimalSeparator:@"."];
        [numFormatter setGroupingSize:3];
        NSString *stringRemainingBalance = [numFormatter stringFromNumber:remainingBalanceNumber];
        NSString *stringTotalPrice = [numFormatter stringFromNumber:totalPriceNumber];
        
        self.remainingBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",stringRemainingBalance,NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_BAHT",nil,[Utils getLanguage],nil)];
        self.totalPriceLabel.text = [NSString stringWithFormat:@"%@ %@",stringTotalPrice,NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_BAHT",nil,[Utils getLanguage],nil)] ;
        if (data.status == 0) {
            if ([[NSDate date] compare:[Utils parseServerDateStringToDate:data.dueDate]] == NSOrderedAscending) {
                self.statusPaymentLabel.text = [[NSString alloc] initWithFormat:@"( %@ %@ )",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_INFROM_DUE_DATE",nil,[Utils getLanguage],nil), [Utils dateInStringFormat:[Utils parseServerDateStringToDate:data.dueDate] format:@"dd/MM/yyyy"]];
                self.statusPaymentLabel.textColor = [UIColor colorWithRed: 0.51 green: 0.51 blue: 0.51 alpha: 1.00];
            }
            else if ([[NSDate date] compare:[Utils parseServerDateStringToDate:data.dueDate]] == NSOrderedDescending){
                self.statusPaymentLabel.text = [NSString stringWithFormat:@"( %@ )",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_OVERDUE",nil,[Utils getLanguage],nil)];
                self.statusPaymentLabel.textColor = [UIColor colorWithRed:0.93 green:0.23 blue:0.23 alpha:1.0];
            }
        }
        else if (data.status == 1){
            self.statusPaymentLabel.text = [NSString stringWithFormat:@"( %@ )",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PAID",nil,[Utils getLanguage],nil)];
            self.statusPaymentLabel.textColor = [UIColor colorWithRed:0.27 green:0.71 blue:0.26 alpha:1.0];
        }
      
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            self.orderTableViewHighConstraint.constant = 60 * productArray.count;
            self.installmentTableViewHighConstraint.constant = 40 * invoicesArray.count;

            if (data.invoicesArray.count > 0) {
                if (pricePay > 0) {
                    self.paymentView.hidden = NO;
                }else{
                    self.paymentView.hidden = YES;
                    self.paymentHighViewConstraint.constant = 0;
                }
               
                [self.installmentTableView reloadData];
                
            }else{
                self.headerTotalLabel.hidden = YES;
                self.totalPriceLabel.hidden = YES;
                self.headerInstallmentLabel.hidden = YES;
                self.orderLineView.hidden = YES;
                self.installmentTableViewHighConstraint.constant = 0;
                self.closeInstallmentConstraint.constant = 0;
            }
        }else{
            self.orderTableViewHighConstraint.constant = 90 * productArray.count;
            self.installmentTableViewHighConstraint.constant = 70 * invoicesArray.count;
                        
            if (data.invoicesArray.count > 0) {
                
                if (pricePay > 0) {
                    self.paymentView.hidden = NO;
            
                }else{
                    self.paymentView.hidden = YES;
                    self.paymentHighViewConstraint.constant = 0;
                    
                }
                
                [self.installmentTableView reloadData];
            }else{
                self.headerTotalLabel.hidden = YES;
                self.totalPriceLabel.hidden = YES;
                self.headerInstallmentLabel.hidden = YES;
                self.orderLineView.hidden = YES;
                self.installmentTableViewHighConstraint.constant = 0;
                self.closeInstallmentConstraint.constant = 0;

            }
        }
        [self.orderTableView reloadData];
    }
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.orderTableView) {
        return productArray.count;
    }else{
        return invoicesArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numFormatter setUsesSignificantDigits:NO];
    [numFormatter setMaximumFractionDigits:2];
    [numFormatter setMinimumFractionDigits:2];
    [numFormatter setUsesGroupingSeparator:YES];
    [numFormatter setGroupingSeparator:@","];
    [numFormatter setDecimalSeparator:@"."];
    [numFormatter setGroupingSize:3];
    
    if (tableView == self.orderTableView) {
        TFProductModel *model = [productArray objectAtIndex:indexPath.row];
        SchoolFeePriceListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        NSNumber *num = [NSNumber numberWithFloat:model.productPrice];
        NSString *stringNum = [numFormatter stringFromNumber:num];
        NSLog(@"%@", stringNum);
        cell.priceListLabel.text = model.productName;
        cell.priceLabel.text = [[NSString alloc] initWithFormat:@"%@ %@" , stringNum,NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_BAHT",nil,[Utils getLanguage],nil)];
        return cell;
    }else{
        TFInvoicesModel *model = [invoicesArray objectAtIndex:indexPath.row];
        TFInvoicesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        NSNumber *num = [NSNumber numberWithFloat:model.amount];
        NSString *amountString = [numFormatter stringFromNumber:num];
        
        cell.roundLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PAID_ON",nil,[Utils getLanguage],nil);
        
//        cell.roundLabel.text = [NSString stringWithFormat:@"%@ %d",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PAID_ON",nil,[Utils getLanguage],nil),model.paymentGroupId];
        cell.dateLabel.text = [Utils dateInStringFormat:[Utils parseServerDateStringToDate:model.paymentDate] format:@"dd/MM/yyyy"];
        cell.amountLabel.text = [NSString stringWithFormat:@"%@ %@",amountString,NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_BAHT",nil,[Utils getLanguage],nil)];
        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.orderTableView) {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            return 60;
        }else{
            return 90;
        }
    }
    else{
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            return 40;
        }else{
            return 70;
        }
    }
    
}


- (void)selectPaymentTypeAction:(UIButton*)button{
    NSLog(@"Tag = %d",button.tag);
    paymentType = [@(button.tag)intValue]; //0 = prompay, 1 = Card
    
    if (button.tag == 0) {//Promptpay
        self.radioPromptpayBtn.selected = YES;
        self.radioCreditBtn.selected = NO;
       }else{//Credits
        self.radioCreditBtn.selected = YES;
        self.radioPromptpayBtn.selected = NO;
    }
    
//    if (button.tag == 0) {//Promptpay
//        if (self.radioPromptpayBtn.selected == YES) {
//            self.radioPromptpayBtn.selected = NO;
//        }else{
//            self.radioPromptpayBtn.selected = YES;
//            self.radioCreditBtn.selected = NO;
//        }
//
//    }else{//Credits
//
//        if ( self.radioCreditBtn.selected == YES) {
//            self.radioCreditBtn.selected = NO;
//        }else{
//            self.radioCreditBtn.selected = YES;
//            self.radioPromptpayBtn.selected = NO;
//        }
//    }
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(self.alertDialog != nil && [self.alertDialog isDialogShowing]) {
        [self.alertDialog dismissDialog];
    }
    else {
        self.alertDialog = [[AlertDialog alloc] init];
    }
    [self.alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)backAction:(id)sender {
    SchoolFeeListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SchoolFeeStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (IBAction)paymentAction:(id)sender {
    TFPaymentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TFPaymentStoryboard"];
    if (paymentType > -1) {
        viewController.amount = pricePay;
        viewController.schoolID = [UserData getSchoolId];
        viewController.studentID = [UserData getUserID];
        viewController.paymentType = paymentType;
        viewController.invoiceID = self.invoiceID;
        [self.slideMenuController changeMainViewController:viewController close:YES]; 
    }else{
        [self showAlertDialogWithMessage:@"กรุณาเลือกประเภทชำระเงิน"];
    }
   
}
@end
