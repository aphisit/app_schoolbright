//
//  ReportInOutAllDataViewController.m
//  JabjaiApp
//
//  Created by mac on 4/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportInOutAllDataViewController.h"
#import "ReportListTeacherViewController.h"
#import "AlertDialog.h"
#import "Utils.h"
#import "DateUtility.h"

@interface ReportInOutAllDataViewController (){
    UIColor *OrangeColor;
    UIColor *GrayColor;
    AlertDialog *alertDialog;
    NSDate *date;
}
@property (strong, nonatomic) SelectCalendaDayFormerDialog *calendarDialog;
@property (strong, nonatomic) NSDate *minDate;
@property (strong, nonatomic) NSDate *maxDate;
@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) NSDateFormatter *sendDateFormatter;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation ReportInOutAllDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self callGetStatusServer];
    OrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1.0];
    GrayColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
    self.sendDateFormatter = [Utils getDateFormatter];
    self.sendDateFormatter.dateFormat = @"MM/dd/yyyy";
    date = [NSDate date];
}
- (void)viewDidLayoutSubviews {
    [self setupPageMenu];
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ATTENDCLASS",nil,[Utils getLanguage],nil);
}
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialog alloc] init];
    }
    else if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showFinishAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else{
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)onAlertDialogClose {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}

- (void)setupPageMenu {
    
    self.studentController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutAllDataStudentStoryboard"];
    //    self.flagPoleController = [[ReportCheckFlagpoleSelectViewController alloc] init];
    self.studentController.title = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_STUDENT",nil,[Utils getLanguage],nil);
    self.studentController.date = date;
    
    
    self.personController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutAllDataPersonalStoryboard"];
    //    self.subjectController = [[ReportCheckSubjectSelectViewController alloc] init];
    self.personController.title =NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_STFF",nil,[Utils getLanguage],nil);
    self.personController.date = date;
    
    NSArray *controllerArray = @[self.studentController, self.personController];
    
    // Color background for each controller tab
    NSDictionary *parameters;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(40),
                       CAPSPageMenuOptionMenuHeight: @(50),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:32.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    else{
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(20),
                       CAPSPageMenuOptionMenuHeight: @(40),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:22.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    NSArray *menuItems = _pageMenu.menuItems;
    MenuItemView *menuItemView = [menuItems objectAtIndex:_pageMenu.currentPageIndex];
    menuItemView.backgroundColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.40 alpha:1.0];
    menuItemView.titleLabel.textColor = [UIColor whiteColor];
    
    //Optional delegate
    _pageMenu.delegate = self;
    
    [self.contentView addSubview:_pageMenu.view];
    
}

#pragma CAPSPageMenuDelegate
- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    NSLog(@"WillMoveToPage");
    
    NSArray *menuItems = _pageMenu.menuItems;
    
    int count = 0;
    
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.40 alpha:1.0];
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        
        count++;
    }
    
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
    NSLog(@"DidMoveToPage");
    
    NSArray *menuItems = _pageMenu.menuItems;
    
    int count = 0;
    
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.40 alpha:1.0];
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        
        count++;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CalendarDialogDelegate
-(void)calendarDialogPressOK:(NSArray<NSDate *> *)selectedDates {
    if(selectedDates.count > 0) {
        NSDate *selectDate = [selectedDates objectAtIndex:0];
        NSLog(@"NSDate = %@",[self.sendDateFormatter stringFromDate:selectDate]);
        date = selectDate;
        [self setupPageMenu];
    }
}

- (IBAction)calendarAction:(id)sender {
    self.gregorian = [Utils getGregorianCalendar];
    self.maxDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:+1 toDate:[NSDate date] options:0];//[NSDate date];
    self.minDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:0 toDate:[NSDate date] options:0];
    self.calendarDialog = [[SelectCalendaDayFormerDialog alloc] init];
    self.calendarDialog.delegate = self;
    if([self.calendarDialog isDialogShowing]) {
        [self.calendarDialog dismissDialog];
    }
    [self.calendarDialog showDialogInView:self.view];
}
- (IBAction)moveBack:(id)sender {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
