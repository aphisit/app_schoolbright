//
//  SNSelectTheaCherViewController.m
//  JabjaiApp
//
//  Created by toffee on 9/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNSelectTheaCherViewController.h"
#import "APIURL.h"
#import "UserData.h"
#import "Utils.h"
#import "AlertDialog.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SNSelectTheaCherViewController (){
    NSMutableArray<TheacherAllModel *> *theacherAll;
    NSMutableArray *nameTeacherArray;
    NSMutableArray *teacherId;
    NSMutableArray *isSelected;
    NSMutableArray *employeesid;
    NSMutableArray *userAcceptArray;
    NSMutableArray *userImageArray;
    AlertDialog *alertDialog;
    NSBundle *myLangBundle;
}

@end
static NSString *cellIdentifier = @"Cell";
//NSMutableArray *addTheacherArrray;
@implementation SNSelectTheaCherViewController
//@synthesize theaCherImageArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSStudentNameRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self doTheacher];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self setLanguage];
    // Do any additional setup after loading the view.
}
- (void) viewDidLayoutSubviews{
     [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextLabel layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextLabel.bounds;
    [self.headerNextLabel.layer insertSublayer:gradientNext atIndex:0];
}

-(void)setLanguage{
    
    self.headerSendNewsLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS",nil,myLangBundle,nil);
    self.headerTheacherLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_TEACHERS",nil,myLangBundle,nil);
    [self.headerNextLabel setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

-(void)doTheacher{
    
    [self showIndicator];
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getTheacherAllUrl:schoolID] ;
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self stopIndicator];
        id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        NSArray *returnedArray = returnedData;
        self.addTheacherArrray = [[NSArray alloc] init];
        self->nameTeacherArray = [[NSMutableArray alloc] init];
        self->teacherId = [[NSMutableArray alloc] init];
        self->isSelected = [[NSMutableArray alloc] init];
        self->employeesid = [[NSMutableArray alloc] init];
        self->userAcceptArray = [[NSMutableArray alloc] init];
        self->userImageArray = [[NSMutableArray alloc] init];
//        NSData  *imaData;
//        NSManagedObjectContext *context = [self managedObjectContext];
            for(int i=0; i<returnedArray.count; i++) {
                NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                long long theacherId = [[dataDict objectForKey:@"emplyoeesid"] longLongValue];
                NSMutableString  *pic ;
                NSMutableString *theacherName;
                if(![[dataDict objectForKey:@"emplyoeesname"] isKindOfClass:[NSNull class]]) {
                    theacherName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"emplyoeesname"]];
                }
                else {
                    theacherName = [[NSMutableString alloc] initWithString:@""];
                }
                
                if(![[dataDict objectForKey:@"pic"] isKindOfClass:[NSNull class]]) {
                    pic = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"pic"]];
                }
                else {
                    pic = [[NSMutableString alloc] initWithString:@""];
                }

                CFStringTrimWhitespace((__bridge CFMutableStringRef) theacherName);
                CFStringTrimWhitespace((__bridge CFMutableStringRef) pic);
                [self->nameTeacherArray addObject:theacherName ];
                [self->userImageArray addObject:pic];
                [self->isSelected addObject: @"NO"];
                [self->teacherId addObject:@(theacherId)];
                self.addTheacherArrray = self->nameTeacherArray;
            
//                NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: pic]];
//                NSManagedObject *newImage  = [NSEntityDescription insertNewObjectForEntityForName:@"Entity" inManagedObjectContext:context];
//
//                if(imageData != nil){
//                    UIImage *image = [UIImage imageWithData: imageData];
//                    imaData = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.0)];
//                    [newImage setValue:imaData forKey:@"savedImage"];
//                }else{
//                    imaData = [NSData dataWithData:UIImageJPEGRepresentation([UIImage imageNamed:@"ic_user_info"], 0.0)];
//                    [newImage setValue:imaData forKey:@"savedImage"];
//                }
            }
//        NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"Entity"];
//        theaCherImageArray = [[context executeFetchRequest:fetch error:nil]mutableCopy];
        [self.tableView reloadData];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  
    if(self.addTheacherArrray != nil) {
        
        return self.addTheacherArrray.count;
    }
    else {

        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BSStudentNameRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    NSLog(@"row = %d",indexPath.row);
    SNSelectTeacherModel *model = [self.addTheacherArrray objectAtIndex:indexPath.row];
    cell.delegate = self;
    cell.index = indexPath.row;
    NSArray *array = [[NSString stringWithFormat:@"%@", [self.addTheacherArrray objectAtIndex:indexPath.row]] componentsSeparatedByString:@" "];
    NSMutableString *lastName = [[NSMutableString alloc] init];
    if (array.count>1) {
        for (int i = 0; i < array.count; i++) {
            if (i>0) {
                [lastName appendFormat:@"%@ ",array[i]];
            }
        }
        cell.titleLabel.text = array[0];
        cell.titleLastname.text = lastName;
    }else{
        cell.titleLabel.text = array[0];
    }
    cell.runNumber.text = [NSString stringWithFormat:@"%d.",indexPath.row+1];
    [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@", [userImageArray objectAtIndex:indexPath.row ]]]] placeholderImage:nil options:SDWebImageRefreshCached
        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        if (image == nil || error) {
                                          cell.userImageView.image = [UIImage imageNamed:@"ic_user_info"];
                                        }
        }];
    
    cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height /2;
    cell.userImageView.layer.masksToBounds = YES;
    cell.userImageView.layer.borderWidth = 0;

    if([[isSelected objectAtIndex:indexPath.row]  isEqual: @"YES"]) {
        [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
    }
    else {
        [cell setRadioButtonClearSelected];
    }
    [cell.shadowViewCell.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.shadowViewCell.layer setShadowOpacity:0.3];
    [cell.shadowViewCell.layer setShadowRadius:3.0];
    [cell.shadowViewCell.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 100; /* Device is iPad */
    }else{
        return 90;
    }
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *test;
    NSLog(@"number = %d",index);
    if([isSelected[indexPath.row] isEqualToString : @"YES"]){
        isSelected[indexPath.row] = @"NO";
        [employeesid removeObject:[teacherId objectAtIndex:indexPath.row]];
        [userAcceptArray removeObject:[nameTeacherArray objectAtIndex:indexPath.row]];
        
    }else{
        isSelected[indexPath.row] = @"YES";
        [employeesid addObject:[teacherId objectAtIndex:indexPath.row]];
        [userAcceptArray addObject:[nameTeacherArray objectAtIndex:indexPath.row] ];
        
    }
    NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)onPressRadioButton:(BSStudentNameRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index{
    NSString *test;
    NSLog(@"number = %d",index);
    if([isSelected[index] isEqualToString : @"YES"]){
        isSelected[index] = @"NO";
        [employeesid removeObject:[teacherId objectAtIndex:index]];
        [userAcceptArray removeObject:[nameTeacherArray objectAtIndex:index]];
        
    }else{
        isSelected[index] = @"YES";
        [employeesid addObject:[teacherId objectAtIndex:index]];
        [userAcceptArray addObject:[nameTeacherArray objectAtIndex:index] ];

    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}
-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)moveBack:(id)sender {
    SNLevelStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SendNewsStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (IBAction)actionNext:(id)sender {
    
    if (employeesid.count == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_LEAST",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
    }else{
        SNTimeOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        viewController.sendType = self.sendType;
        viewController.sendGroup = self.sendGroup;
        viewController.newsType = self.newsType;
        viewController.employeesid = employeesid;
        viewController.userAcceptArray = userAcceptArray;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
}


@end
