//
//  ARDetailAcademicResultsViewController.h
//  JabjaiApp
//
//  Created by toffee on 21/2/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARSelectTermViewController.h"
#import "ARSuggestionViewController.h"
#import "ARGradeTableViewCell.h"
#import "AREventCourseTableViewCell.h"
#import "CallGetDetailOfAcademicResultsAPI.h"
NS_ASSUME_NONNULL_BEGIN

@interface ARDetailAcademicResultsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,CallGetDetailOfAcademicResultsAPIDelegate>

@property (nonatomic, strong) NSString *termStr;
@property (nonatomic, strong) NSString *yearStr;
@property (nonatomic, strong) NSString *yearId;
@property (nonatomic, strong) NSArray *termArray;
@property (nonatomic, strong) NSMutableArray *yearNumberArray;
@property (nonatomic, strong) NSMutableArray *yearIdArray;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;






@property (weak, nonatomic) IBOutlet UILabel *headerSubjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerGradeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerBasicCourseLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAdditionalLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerNotCountCreditLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEventLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerGPALabel;

@property (weak, nonatomic) IBOutlet UILabel *schoolNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *schoolNameENLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageSchool;

@property (weak, nonatomic) IBOutlet UILabel *termLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *branchLabel;
@property (weak, nonatomic) IBOutlet UILabel *classRoomLabel;
@property (weak, nonatomic) IBOutlet UILabel *gradeAverageLabel;

@property (weak, nonatomic) IBOutlet UILabel *showStatusBasicSubjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *showStatusAdditionalSubjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *showStatusEventLabel;
@property (weak, nonatomic) IBOutlet UILabel *showStatusNoCountCreditLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UITableView *basicSubjectsTableView;
@property (weak, nonatomic) IBOutlet UITableView *additionalSubjectsTableView;
@property (weak, nonatomic) IBOutlet UITableView *eventTableView;
@property (weak, nonatomic) IBOutlet UITableView *noCountCreditTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *noCountCreditConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *basicSubjectConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *additionalSubjectConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *eventConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)suggestionAction:(id)sender;

- (IBAction)moveBackAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
