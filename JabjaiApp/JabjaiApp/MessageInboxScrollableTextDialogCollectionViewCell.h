//
//  MessageInboxScrollableTextDialogCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 10/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageInboxScrollableTextDialogCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *imageNews;
@property (weak, nonatomic) IBOutlet UIImageView *images;

@end
