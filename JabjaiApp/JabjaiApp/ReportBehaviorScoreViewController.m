//
//  ReportBehaviorScoreViewController.m
//  JabjaiApp
//
//  Created by mac on 4/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportBehaviorScoreViewController.h"
#import "ReportBehaviorScoreDetailViewController.h"
#import "BehaviorDateScoreTableViewCell.h"
#import "UserData.h"
#import "Utils.h"

#import "ReportListStudentViewController.h"

@interface ReportBehaviorScoreViewController (){
    UIColor *greenColor;
    UIColor *redColor;
}

@property (strong, nonatomic) CallGetBSHistoryForStudentReportAPI *callGetBSHistoryForStudentReportAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

static NSString *cellIdentifier = @"Cell";

@implementation ReportBehaviorScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    [self setLanguage];
    greenColor = [UIColor colorWithRed:12/255.0 green:190/255.0 blue:64/255.0 alpha:1.0];
    redColor = [UIColor colorWithRed:255/255.0 green:57/255.0 blue:56/255.0 alpha:1.0];
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BehaviorDateScoreTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    if(self.bsHistoryArray == nil) {
        [self getBSHistoryData];
    }
    else {
        self.remainingScoreLabel.text = [NSString stringWithFormat:@"%@", [NSNumber numberWithDouble:self.remainingScore]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    [self.remainingScoreView layoutIfNeeded];
       CAGradientLayer  *remainingScoreGradient = [Utils getGradientColorStatus:@"green"];
       remainingScoreGradient.frame = self.remainingScoreView.bounds;
    [self.remainingScoreView.layer insertSublayer:remainingScoreGradient atIndex:0];
}

- (void) setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_BEHAVIOR_REP",nil,[Utils getLanguage],nil);
    self.headerDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_BEHAVIOR_REP_DAY",nil,[Utils getLanguage],nil);
    self.headerListLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_BEHAVIOR_REP_LIST",nil,[Utils getLanguage],nil);
    self.headerScoreLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_BEHAVIOR_REP_SCORE",nil,[Utils getLanguage],nil);
    self.headerRemainingScoreLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_BEHAVIOR_REP_REMAINING_SORE",nil,[Utils getLanguage],nil);
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.bsHistoryArray == nil) {
        return 0;
    }
    else {
        return self.bsHistoryArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BehaviorDateScoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    BSHistoryModel *model = [self.bsHistoryArray objectAtIndex:indexPath.row];
    
    if([model getBehaviorType] == 0) { // type add
        cell.scoreLabel.backgroundColor = greenColor;
        cell.scoreLabel.text = [NSString stringWithFormat:@"+%@",[NSNumber numberWithDouble:[model getBehaviorScore]] ];
    } else {
        cell.scoreLabel.backgroundColor = redColor;
        cell.scoreLabel.text = [NSString stringWithFormat:@"-%@", [NSNumber numberWithDouble:[model getBehaviorScore]]];
    }
    
    cell.dateLabel.text = [Utils getBuddhistEraFormatWithDate:[model getDateTime]];
    cell.behaviorNameLabel.text = [model getBehaviorName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BSHistoryModel *model = [self.bsHistoryArray objectAtIndex:indexPath.row];
    
    [self gotoBSReportForStudentDetailViewController:model];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

#pragma mark - API Caller

- (void)getBSHistoryData {
    
    if(self.callGetBSHistoryForStudentReportAPI != nil) {
        self.callGetBSHistoryForStudentReportAPI = nil;
    }
    
    self.callGetBSHistoryForStudentReportAPI = [[CallGetBSHistoryForStudentReportAPI alloc] init];
    self.callGetBSHistoryForStudentReportAPI.delegate = self;
    [self.callGetBSHistoryForStudentReportAPI call:[UserData getUserID] schoolid:[UserData getSchoolId]];
}

#pragma mark - API Caller delegate

- (void)callGetBSHistoryForStudentReportAPI:(CallGetBSHistoryForStudentReportAPI *)classObj data:(NSArray<BSHistoryModel *> *)data remainingScore:(double)remainingScore success:(BOOL)success {
    
    if(success) {
        
        self.remainingScore = remainingScore;
        self.remainingScoreLabel.text = [NSString stringWithFormat:@"%@", [NSNumber numberWithDouble:remainingScore]];
        self.bsHistoryArray = data;
        [self.tableView reloadData];
    }
}

#pragma mark - Utility

- (void)gotoBSReportForStudentDetailViewController:(BSHistoryModel *)model {
    
    ReportBehaviorScoreDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportBehaviorScoreDetailStoryboard"];
    viewController.selectedHistoryModel = model;
    viewController.bsHistoryArray = _bsHistoryArray;
    viewController.remainingScore = self.remainingScore;
    [self.slideMenuController changeMainViewController:viewController close:YES];
    //[self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (IBAction)moveBack:(id)sender {
    
    ReportListStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListStudentStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
