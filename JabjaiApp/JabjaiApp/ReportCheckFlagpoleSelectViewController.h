//
//  ReportCheckFlagpoleSelectViewController.h
//  JabjaiApp
//
//  Created by mac on 4/27/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TAStudentStatusModel.h"
#import "SchoolClassroomModel.h"
#import "SchoolLevelModel.h"
#import "TableListDialog.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "SWRevealViewController.h"
#import "TAStudentStatusModel.h"
#import "TAStudentStatusTableViewCell.h"

#import "SlideMenuController.h"

@interface ReportCheckFlagpoleSelectViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate,TAStudentStatusTableViewCellDelegate, SlideMenuControllerDelegate>{}
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (strong, nonatomic) NSArray<TAStudentStatusModel *> *selectedStudentArray;
@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classRoomTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;

- (IBAction)actionNext:(id)sender;

@end
