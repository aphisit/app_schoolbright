//
//  ReportLeaveCalendarViewController.m
//  JabjaiApp
//
//  Created by mac on 4/10/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportLeaveCalendarViewController.h"

#import "NoticeReportDateCalendarModel.h"
#import "NoticeReportDateTableViewCell.h"
#import "NoticeReportDateModel.h"

#import "NoticeReportDateMessageDetailViewController.h"
#import "NavigationViewController.h"

#import "DateUtility.h"
#import "APIURL.h"
#import "UserData.h"
#import "Constant.h"
#import "Utils.h"

#import "ReportLeaveDetailViewController.h"

@interface ReportLeaveCalendarViewController (){
    
    UIColor *grayColor;
    UIColor *headerTitleColor;
    UIColor *todayColor;
    UIColor *selectionColor;
    
    NSMutableDictionary *fillDefaultColors;
    NSMutableDictionary *fillSelectionColors;
    NSMutableDictionary *borderSelectionColors;
    NSMutableDictionary *titleDefaultColors;
    NSMutableDictionary *titleSelectionColors;
    
    NSMutableArray *weekendDays;
    
    NSMutableArray *holidayArray;
    
    NSInteger connectCounter;
    
    NSMutableArray<NoticeReportDateModel *> *leaveArray;
    
    UIColor *dodgerBlue, *aqua, *monaLisa, *bitterSweet, *ripTide, *mountainMeadow, *perfume, *heliotRope, *alto, *dustyGray, *peachOrange, *texasRose;
    
    UIColor *cornFlower, *mariner;
}


@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSDateFormatter *dateFormatter2;

@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;

@property (nonatomic) NSDate *selectionDate;
@property (nonatomic) NSDate *aDate;

@property (strong, nonatomic) UIColor *eventGreenColor;
@property (strong, nonatomic) UIColor *todayColor;
@property (strong, nonatomic) UIColor *customTextSelectedColor;
@property (strong, nonatomic) UIColor *customTextDeselectedColor;

@property (nonatomic, strong) NSDateFormatter *timeFormatter;

@property (nonatomic) NSDate *considerDate;

@end

@implementation ReportLeaveCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    selectionColor = [UIColor colorWithRed:212/255.0 green:44/255.0 blue:31/255.0 alpha:1.0];
    
    // 26/10/2017
    self.eventGreenColor = [UIColor colorWithRed:62/255.0 green:194/255.0 blue:45/255.0 alpha:1.0];
    self.todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    self.customTextSelectedColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.customTextDeselectedColor = [UIColor colorWithRed:190/255.0 green:194/255.0 blue:197/255.0 alpha:1.0];
    
    //04/05/2017
    
    dodgerBlue = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0]; // Now Full
    aqua = [UIColor colorWithRed:0.21 green:0.89 blue:1.00 alpha:1.0]; // Now Select
    monaLisa = [UIColor colorWithRed:1.00 green:0.68 blue:0.62 alpha:1.0]; // Color status 0 Before Select
    bitterSweet = [UIColor colorWithRed:1.00 green:0.48 blue:0.38 alpha:1.0]; // Color status 0 After Select
    ripTide = [UIColor colorWithRed:0.47 green:0.93 blue:0.71 alpha:1.0]; // Color status 1 Before Select
    mountainMeadow = [UIColor colorWithRed:0.13 green:0.76 blue:0.46 alpha:1.0]; // Color status 1 After Select
    perfume = [UIColor colorWithRed:0.80 green:0.62 blue:0.98 alpha:1.0]; // Color status 2 Before Select
    heliotRope = [UIColor colorWithRed:0.75 green:0.51 blue:1.00 alpha:1.0]; // Color status 2 After Select
    alto = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1.0]; // Color status 3 Before Select
    dustyGray = [UIColor colorWithRed:0.60 green:0.60 blue:0.60 alpha:1.0]; // Color status 3 After Select
    peachOrange = [UIColor colorWithRed:1.00 green:0.85 blue:0.59 alpha:1.0]; // Color status 4 Before Select
    texasRose = [UIColor colorWithRed:1.00 green:0.75 blue:0.31 alpha:1.0]; // Color status 4 After Select
    
    cornFlower = [UIColor colorWithRed:0.58 green:0.80 blue:0.92 alpha:1.0]; // Color status 0 Before Select
    mariner = [UIColor colorWithRed:0.13 green:0.49 blue:0.76 alpha:1.0]; // Color status 1 After Select
    
    // 26/10/2017
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.dateFormatter2 = [Utils getDateFormatter];
    self.dateFormatter2.dateFormat = @"dd";
    
    self.gregorian = [Utils getGregorianCalendar];
    
    self.minimumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
    self.maximumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:1 toDate:[NSDate date] options:0];
    
    self.considerDate = [NSDate date];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height >= 1024.0f) {
            self.heightCalendarConstraint.constant = 450.0f;
        }
    }
    else{
        self.heightCalendarConstraint.constant = 300.0f;
    }
    
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_up"] forState:UIControlStateNormal];
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_down"] forState:UIControlStateSelected];
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = grayColor;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    connectCounter = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NoticeReportDateTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    self.dateTitle.text = [Utils getThaiDateFormatWithDate: self.considerDate];
    
    [self getLeaveReportDailyWithDate:self.considerDate];
    
}

- (void)createCalendar {
    self.containerView.backgroundColor = grayColor;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, height)];
    
    calendar.dataSource = self;
    calendar.delegate = self;
    calendar.backgroundColor = grayColor;
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
    calendar.appearance.selectionColor = selectionColor;

    // 04/05/2018
    calendar.appearance.todayColor = dodgerBlue;
    calendar.appearance.titleTodayColor = [UIColor whiteColor];
    calendar.appearance.todaySelectionColor = dodgerBlue;
    calendar.appearance.selectionColor = [UIColor clearColor];
    calendar.appearance.titleSelectionColor = [UIColor blackColor];
    calendar.appearance.borderSelectionColor = dodgerBlue;
    [self.containerView addSubview:calendar];
    self.calendar = calendar;
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 95, 34);
    previousButton.backgroundColor = grayColor;//[UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    nextButton.frame = CGRectMake(CGRectGetWidth(self.containerView.frame) - 95, 5, 95, 34);
    nextButton.frame = CGRectMake(self.view.frame.size.width - nextButton.frame.size.width - 95, 5, 95, 34);
    nextButton.backgroundColor = grayColor; //[UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:nextButton];
    self.nextButton = nextButton;
    [self getLeaveReportMonthlyWithDate:calendar.currentPage];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [self createCalendar];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar{
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar{
    return self.maximumDate;
}

#pragma mark - CFSCalendarDelegate

- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
    if([DateUtility sameMonthOfYear:date date2:calendar.currentPage]) {
        
        //        [self getStudentHomeworkCalendar:calendar.currentPage];
        
        return YES;
    }
    
    return NO;
}

- (void)onSelectDate:(NSDate *)date {
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:date];
    self.considerDate = date;
    [self getLeaveReportDailyWithDate:date];
}

- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
    
    //    if (self.delegate && [self.delegate respondsToSelector:@selector(onSelectDate:)]) {
    //        [self.delegate onSelectDate:date];
    //    }
    
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:date];
    self.considerDate = date;
    [self getLeaveReportDailyWithDate:date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd/MM/yyyy";
    NSLog(@"%@", [formatter stringFromDate:date]);
    
}

#pragma mark - Class buttons event

- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
    [self getLeaveReportMonthlyWithDate:previousMonth];
}

- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
    [self getLeaveReportMonthlyWithDate:nextMonth];
}

- (void)calendarCurrentPageDidChange:(CFSCalendar *)calendar {
    
    NSString *dateAsString = [self.dateFormatter stringFromDate:calendar.currentPage];
    NSLog(@"%@", [NSString stringWithFormat:@"Did change page %@", dateAsString]);
    
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date {
    
    if(fillDefaultColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if([fillDefaultColors objectForKey:key] != nil) {
            return [fillDefaultColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date{
    
    if (fillSelectionColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([fillSelectionColors objectForKey:key] != nil) {
            return [fillSelectionColors objectForKey:key];
        }
    }
    
    return nil;
    
}
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance borderSelectionColorForDate:(NSDate *)date{
    
    if (borderSelectionColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([borderSelectionColors objectForKey:key] != nil) {
            return [borderSelectionColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date{
    
    if (titleDefaultColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([titleDefaultColors objectForKey:key] != nil) {
            return [titleDefaultColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleSelectionColorForDate:(NSDate *)date{
    
    if (titleSelectionColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([titleSelectionColors objectForKey:key] != nil) {
            return [titleSelectionColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (void)getLeaveReportMonthlyWithDate:(NSDate *)date{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getLeaveReportMonthlyWithUserID:userID date:date schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    //    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            //            [self stopIndicator];
            
            if (error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if (data == nil){
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if ((connectCounter < TRY_CONNECT_MAX)) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getLeaveReportMonthlyWithDate:date];
            }
            else{
                connectCounter = 0;
            }
        }
        
        else{
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getLeaveReportMonthlyWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getLeaveReportMonthlyWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                holidayArray = [[NSMutableArray alloc] init];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *holidayStr = [[NSMutableString alloc] initWithFormat:@"%@" , [dataDict objectForKey:@"date"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)holidayStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *holiday = [formatter dateFromString:holidayStr];
                    
                    NoticeReportDateCalendarModel *model = [[NoticeReportDateCalendarModel alloc] init];
                    
                    [model setLeaveList:holiday];
                    
                    [holidayArray addObject:model];
                    
                }
                
                [self updateHomeworkDay];
            }
            
        }
        
        
    }];
    
}

- (void)updateHomeworkDay{
    
    if (holidayArray == nil) {
        return;
    }
    
    if (fillDefaultColors != nil) {
        [fillDefaultColors removeAllObjects];
    }
    
    if (fillSelectionColors != nil) {
        [fillSelectionColors removeAllObjects];
    }
    
    if (borderSelectionColors != nil) {
        [borderSelectionColors removeAllObjects];
    }
    
    if (titleDefaultColors != nil) {
        [titleDefaultColors removeAllObjects];
    }
    
    if (titleSelectionColors != nil) {
        [titleSelectionColors removeAllObjects];
    }
    
    fillDefaultColors = [[NSMutableDictionary alloc] init];
    fillSelectionColors = [[NSMutableDictionary alloc] init];
    borderSelectionColors = [[NSMutableDictionary alloc] init];
    titleDefaultColors = [[NSMutableDictionary alloc] init];
    titleSelectionColors = [[NSMutableDictionary alloc] init];
    
    for (NoticeReportDateCalendarModel *model in holidayArray) {
        NSString *dateKey = [self.dateFormatter stringFromDate:model.leaveList];
//        [fillDefaultColors setObject:self.eventGreenColor forKey:dateKey];
        
        NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:model.leaveList];
        NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
        if([today day] == [otherDay day] &&
           [today month] == [otherDay month] &&
           [today year] == [otherDay year] &&
           [today era] == [otherDay era]) {
            
            [fillDefaultColors setObject:dodgerBlue forKey:dateKey];
            [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
            [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
        }
        
        else{
            
            [fillDefaultColors setObject:ripTide forKey:dateKey];
            [fillSelectionColors setObject:mountainMeadow forKey:dateKey];
            [borderSelectionColors setObject:[UIColor clearColor] forKey:dateKey];
            [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
            [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
            
        }

    }
    
    [self.calendar reloadData];
    
}
//
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(leaveArray == nil) {
        return 0;
    }
    else {
        return leaveArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NoticeReportDateModel *model = [leaveArray objectAtIndex:indexPath.row];
    
    NoticeReportDateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.statusColor.layer.cornerRadius = cell.statusColor.frame.size.width / 2;
    cell.statusColor.clipsToBounds = YES;
    cell.statusColor.layer.shouldRasterize = YES;
    
    if ([model.status isEqual:@"1"]){
        cell.statusColor.backgroundColor = mariner;
        cell.statusLabel.text = @"รออนุญาต";
    }
    else if ([model.status isEqual:@"2"]){
        cell.statusColor.backgroundColor = mountainMeadow;
        cell.statusLabel.text = @"อนุญาต";
    }
    else if ([model.status isEqual:@"3"]){
        cell.statusColor.backgroundColor = bitterSweet;
        cell.statusLabel.text = @"ไม่อนุญาต";
    }
    else if ([model.status isEqual:@"4"]){
        cell.statusColor.backgroundColor = heliotRope;
        cell.statusLabel.text = @"ยกเลิก";
    }
    
    cell.leaveNameLabel.text = [[NSString alloc] initWithFormat:@"%@ %@", [UserData getFirstName] , [UserData getLastName]];
    cell.leaveDateLabel.text = [Utils getThaiDateFormatWithDate:self.considerDate];
    
    //    cell.circleDate.layer.cornerRadius = cell.circleDate.frame.size.width / 2;
    //    cell.circleDate.clipsToBounds = YES;
    //    cell.circleDate.layer.shouldRasterize = YES;
    //    cell.circleDate.text = [self.dateFormatter2 stringFromDate:self.considerDate];
    //
    //    cell.reportDateList.text = [Utils getThaiDateFormatWithDate:self.considerDate];
    //
    //    cell.reportCauseList.text = model.leaveCause;
    //
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NoticeReportDateModel *model = [leaveArray objectAtIndex:indexPath.row];
//    NoticeReportDateMessageDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeReportDateMessageDetailStoryboard"];
    
    ReportLeaveDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportLeaveDetailStoryboard"];
    
    viewController.letterID = model.letterID;
    viewController.page = 3;
    
    NavigationViewController *leftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NavigationStoryboard"];
    
//    UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:viewController];
//    leftViewController.mainViewControler = nvc;
    
    SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:viewController leftMenuViewController:leftViewController];
    
    [self.parentViewController presentViewController:slideMenuController animated:NO completion:nil];

    
//    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    //    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}
//
//
- (void)getLeaveReportDailyWithDate:(NSDate *)date{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getLeaveReportDailyWithUserID:userID date:date schoolid:schoolid];
    
    NSURL *url = [NSURL URLWithString:URLString];
    
    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getLeaveReportDailyWithDate:date];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getLeaveReportDailyWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getLeaveReportDailyWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if (leaveArray != nil) {
                    [leaveArray removeAllObjects];
                    leaveArray = nil;
                }
                
                leaveArray = [[NSMutableArray alloc] init];
                
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long letterID = [[dataDict objectForKey:@"letterid"]longLongValue];
                    
                    NSMutableString *leaveType;
                    
                    if (![[dataDict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                        leaveType = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"type"]];
                    }
                    else{
                        leaveType = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *status;
                    
                    if (![[dataDict objectForKey:@"status"] isKindOfClass:[NSNull class]]) {
                        status = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"status"]];
                    }
                    else{
                        status = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveType);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) status);
                    
                    NoticeReportDateModel *model = [[NoticeReportDateModel alloc] init];
                    model.letterID = letterID;
                    model.leaveCause = leaveType;
                    model.status = status;
                    
                    [leaveArray addObject:model];
                }
                
                [self.tableView reloadData];
            }
        }
        
    }];
    
}
//
#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openDetail:(id)sender {
    
    self.arrowStatement.selected = !self.arrowStatement.selected;
    
    if (self.arrowStatement.isSelected == YES) {
        //        self.topDetailConstraint.constant = -300.0f;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailConstraint.constant = -450.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailConstraint.constant = 0.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = -300.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
        
        
        
    }else{
        //        self.topDetailConstraint.constant = 0.0f;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = -450.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailConstraint.constant = 0.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailConstraint.constant = -300.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
    }
    
}
@end
