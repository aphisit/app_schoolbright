//
//  UserInfoDBHelper.h
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MultipleUserModel.h"

@interface UserInfoDBHelper : NSObject

- (BOOL)insertData:(MultipleUserModel *)dataModel;
- (BOOL)updateImageUrlWithSlaveId:(long long)slaveId imageUrl:(NSString *)imageUrl;
- (BOOL)updateToken:(long long)slaveId token:(NSString *)token;
- (void)deleteUserWithMasterId:(long long)masterId slaveId:(long long)slaveId;
- (void)deleteUserAllWithMasterId :(long long)masterId;
- (NSArray<MultipleUserModel *> *)getUserDataWithMasterId:(long long)masterId;
- (BOOL)isMasterIdExistsInDBWithMasterId:(long long)masterId slaveId:(long long)slaveId;
- (BOOL)doCheckColumnDB;


@end
