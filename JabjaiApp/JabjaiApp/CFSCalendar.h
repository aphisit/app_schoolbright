//
//  CFSCalendar.h
//  CFSCalendar
//
//  Created by Wenchao Ding on 29/1/15.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
// 
//  https://github.com/WenchaoD
//
//
//  CFSCalendar is a superior awesome calendar control with high performance, high customizablility and very simple usage.
//
//  @see CFSCalendarDataSource
//  @see CFSCalendarDelegate
//  @see CFSCalendarDelegateAppearance
//  @see CFSCalendarAppearance
//

#import <UIKit/UIKit.h>
#import "CFSCalendarAppearance.h"
#import "CFSCalendarConstance.h"

//! Project version number for CFSCalendar.
FOUNDATION_EXPORT double CFSCalendarVersionNumber;

//! Project version string for CFSCalendar.
FOUNDATION_EXPORT const unsigned char CFSCalendarVersionString[];

typedef NS_ENUM(NSUInteger, CFSCalendarScope) {
    CFSCalendarScopeMonth,
    CFSCalendarScopeWeek
};

typedef NS_ENUM(NSUInteger, CFSCalendarScrollDirection) {
    CFSCalendarScrollDirectionVertical,
    CFSCalendarScrollDirectionHorizontal
};

typedef NS_ENUM(NSUInteger, CFSCalendarPlaceholderType) {
    CFSCalendarPlaceholderTypeNone          = 0,
    CFSCalendarPlaceholderTypeFillHeadTail  = 1,
    CFSCalendarPlaceholderTypeFillSixRows   = 2
};

NS_ASSUME_NONNULL_BEGIN

@class CFSCalendar;

/**
 * CFSCalendarDataSource is a source set of CFSCalendar. The basic job is to provide event、subtitle and min/max day to display for calendar.
 */
@protocol CFSCalendarDataSource <NSObject>

@optional

/**
 * Asks the dataSource for a title for the specific date as a replacement of the day text
 */
- (nullable NSString *)calendar:(CFSCalendar *)calendar titleForDate:(NSDate *)date;

/**
 * Asks the dataSource for a subtitle for the specific date under the day text.
 */
- (nullable NSString *)calendar:(CFSCalendar *)calendar subtitleForDate:(NSDate *)date;

/**
 * Asks the dataSource for an image for the specific date.
 */
- (nullable UIImage *)calendar:(CFSCalendar *)calendar imageForDate:(NSDate *)date;

/**
 * Asks the dataSource the minimum date to display.
 */
- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar;

/**
 * Asks the dataSource the maximum date to display.
 */
- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar;

/**
 * Asks the dataSource the number of event dots for a specific date.
 *
 * @see
 *
 *   - (UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance eventColorForDate:(NSDate *)date;
 *   - (NSArray *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance eventColorsForDate:(NSDate *)date;
 */
- (NSInteger)calendar:(CFSCalendar *)calendar numberOfEventsForDate:(NSDate *)date;

/**
 * This function is deprecated
 */
- (BOOL)calendar:(CFSCalendar *)calendar hasEventForDate:(NSDate *)date CFSCalendarDeprecated(-calendar:numberOfEventsForDate:);

@end


/**
 * The delegate of a CFSCalendar object must adopt the CFSCalendarDelegate protocol. The optional methods of CFSCalendarDelegate manage selections、 user events and help to manager the frame of the calendar.
 */
@protocol CFSCalendarDelegate <NSObject>

@optional

/**
 * Asks the delegate whether the specific date is allowed to be selected by tapping.
 */
- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date;

/**
 * Tells the delegate a date in the calendar is selected by tapping.
 */
- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date;

/**
 * Asks the delegate whether the specific date is allowed to be deselected by tapping.
 */
- (BOOL)calendar:(CFSCalendar *)calendar shouldDeselectDate:(NSDate *)date;

/**
 * Tells the delegate a date in the calendar is deselected by tapping.
 */
- (void)calendar:(CFSCalendar *)calendar didDeselectDate:(NSDate *)date;

/**
 * Tells the delegate the calendar is about to change the bounding rect.
 */
- (void)calendar:(CFSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated;

/**
 * Tells the delegate the calendar is about to change the current page.
 */
- (void)calendarCurrentPageDidChange:(CFSCalendar *)calendar;

/**
 * These functions are deprecated
 */
- (void)calendarCurrentScopeWillChange:(CFSCalendar *)calendar animated:(BOOL)animated CFSCalendarDeprecated(-calendar:boundingRectWillChange:animated:);
- (void)calendarCurrentMonthDidChange:(CFSCalendar *)calendar CFSCalendarDeprecated(-calendarCurrentPageDidChange:);

@end

/**
 * CFSCalendarDelegateAppearance determines the fonts and colors of components in the calendar, but more specificly. Basically, if you need to make a global customization of appearance of the calendar, use CFSCalendarAppearance. But if you need different appearance for different days, use CFSCalendarDelegateAppearance.
 *
 * @see CFSCalendarAppearance
 */
@protocol CFSCalendarDelegateAppearance <NSObject>

@optional

/**
 * Asks the delegate for a fill color in unselected state for the specific date.
 */
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date;

/**
 * Asks the delegate for a fill color in selected state for the specific date.
 */
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date;

/**
 * Asks the delegate for day text color in unselected state for the specific date.
 */
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date;

/**
 * Asks the delegate for day text color in selected state for the specific date.
 */
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleSelectionColorForDate:(NSDate *)date;

/**
 * Asks the delegate for subtitle text color in unselected state for the specific date.
 */
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance subtitleDefaultColorForDate:(NSDate *)date;

/**
 * Asks the delegate for subtitle text color in selected state for the specific date.
 */
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance subtitleSelectionColorForDate:(NSDate *)date;

/**
 * Asks the delegate for event colors for the specific date.
 */
- (nullable NSArray<UIColor *> *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance eventDefaultColorsForDate:(NSDate *)date;

/**
 * Asks the delegate for multiple event colors in selected state for the specific date.
 */
- (nullable NSArray<UIColor *> *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance eventSelectionColorsForDate:(NSDate *)date;

/**
 * Asks the delegate for a border color in unselected state for the specific date.
 */
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance borderDefaultColorForDate:(NSDate *)date;

/**
 * Asks the delegate for a border color in selected state for the specific date.
 */
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance borderSelectionColorForDate:(NSDate *)date;

/**
 * Asks the delegate for an offset for day text for the specific date.
 */
- (CGPoint)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleOffsetForDate:(NSDate *)date;

/**
 * Asks the delegate for an offset for subtitle for the specific date.
 */
- (CGPoint)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance subtitleOffsetForDate:(NSDate *)date;

/**
 * Asks the delegate for an offset for image for the specific date.
 */
- (CGPoint)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance imageOffsetForDate:(NSDate *)date;

/**
 * Asks the delegate for an offset for event dots for the specific date.
 */
- (CGPoint)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance eventOffsetForDate:(NSDate *)date;


/**
 * Asks the delegate for a border radius for the specific date.
 */
- (CGFloat)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance borderRadiusForDate:(NSDate *)date;

/**
 * These functions are deprecated
 */
- (CFSCalendarCellStyle)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance cellStyleForDate:(NSDate *)date CFSCalendarDeprecated(-calendar:appearance:cellShapeForDate:);
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillColorForDate:(NSDate *)date CFSCalendarDeprecated(-calendar:appearance:fillDefaultColorForDate:);
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance selectionColorForDate:(NSDate *)date CFSCalendarDeprecated(-calendar:appearance:fillSelectionColorForDate:);
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance eventColorForDate:(NSDate *)date CFSCalendarDeprecated(-calendar:appearance:eventDefaultColorsForDate:);
- (nullable NSArray *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance eventColorsForDate:(NSDate *)date CFSCalendarDeprecated(-calendar:appearance:eventDefaultColorsForDate:);
- (CFSCalendarCellShape)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance cellShapeForDate:(NSDate *)date CFSCalendarDeprecated(-calendar:appearance:borderRadiusForDate:);
@end

#pragma mark - Primary

IB_DESIGNABLE
@interface CFSCalendar : UIView

// @REVERSE_CALENDAR
// to enable or disable reverse mode 
@property (assign, nonatomic) BOOL reverseCalendar;

@property (weak, nonatomic) NSExtensionContext *extensionContext;

/**
 * The object that acts as the delegate of the calendar.
 */
@property (weak, nonatomic) IBOutlet id<CFSCalendarDelegate> delegate;

/**
 * The object that acts as the data source of the calendar.
 */
@property (weak, nonatomic) IBOutlet id<CFSCalendarDataSource> dataSource;

/**
 * A special mark will be put on 'today' of the calendar.
 */
@property (nullable, strong, nonatomic) NSDate *today;

/**
 * The current page of calendar
 *
 * @desc In week mode, current page represents the current visible week; In month mode, it means current visible month.
 */
@property (strong, nonatomic) NSDate *currentPage;

/**
 * The locale of month and weekday symbols. Change it to display them in your own language.
 *
 * e.g. To display them in Chinese:
 * 
 *    calendar.locale = [NSLocale localeWithLocaleIdentifier:@"zh-CN"];
 */
@property (copy, nonatomic) NSLocale *locale;

/**
 * The scroll direction of CFSCalendar. 
 *
 * e.g. To make the calendar scroll vertically
 *
 *    calendar.scrollDirection = CFSCalendarScrollDirectionVertical;
 */
@property (assign, nonatomic) CFSCalendarScrollDirection scrollDirection;

/**
 * The scope of calendar, change scope will trigger an inner frame change, make sure the frame has been correctly adjusted in 
 *
 *    - (void)calendar:(CFSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated;
 */
@property (assign, nonatomic) CFSCalendarScope scope;

/**
 * A UIPanGestureRecognizer instance which enables the control of scope on the whole day-area. Not available if the scrollDirection is vertical
 *
 * e.g.
 *
 *    calendar.scopeGesture.enabled = YES;
 */
@property (readonly, nonatomic) UIPanGestureRecognizer *scopeGesture;

/**
 * The placeholder type of CFSCalendar. Default is CFSCalendarPlaceholderTypeFillSixRows;
 *
 * e.g. To hide all placeholder of the calendar
 *
 *    calendar.placeholderType = CFSCalendarPlaceholderTypeNone;
 */
#if TARGET_INTERFACE_BUILDER
@property (assign, nonatomic) IBInspectable NSUInteger placeholderType;
#else
@property (assign, nonatomic) CFSCalendarPlaceholderType placeholderType;
#endif

/**
 * The index of the first weekday of the calendar. Give a '2' to make Monday in the first column.
 */
@property (assign, nonatomic) IBInspectable NSUInteger firstWeekday;

/**
 * The height of month header of the calendar. Give a '0' to remove the header.
 */
@property (assign, nonatomic) IBInspectable CGFloat headerHeight;

/**
 * The height of weekday header of the calendar.
 */
@property (assign, nonatomic) IBInspectable CGFloat weekdayHeight;

/**
 * A Boolean value that determines whether users can select a date.
 */
@property (assign, nonatomic) IBInspectable BOOL allowsSelection;

/**
 * A Boolean value that determines whether users can select more than one date.
 */
@property (assign, nonatomic) IBInspectable BOOL allowsMultipleSelection;

/**
 *  @SELECT_RANGE
 *  A Boolean value that determines whether users can range of date;
 */
@property (assign, nonatomic) IBInspectable BOOL allowsRangeSelection;

/**
 * A Boolean value that determines whether paging is enabled for the calendar.
 */
@property (assign, nonatomic) IBInspectable BOOL pagingEnabled;

/**
 * A Boolean value that determines whether scrolling is enabled for the calendar.
 */
@property (assign, nonatomic) IBInspectable BOOL scrollEnabled;

/**
 * A Boolean value that determines whether scoping animation is centered a visible selected date. Default is YES.
 */
@property (assign, nonatomic) IBInspectable BOOL focusOnSingleSelectedDate;

/**
 * A Boolean value that determines whether the calendar should show a handle for control the scope. Default is NO;
 */
@property (assign, nonatomic) IBInspectable BOOL showsScopeHandle;

/**
 * The multiplier of line height while paging enabled is NO. Default is 1.0;
 */
@property (assign, nonatomic) IBInspectable CGFloat lineHeightMultiplier;

/**
 * The calendar appearance used to control the global fonts、colors .etc
 */
@property (readonly, nonatomic) CFSCalendarAppearance *appearance;

/**
 * A date object representing the minimum day enable、visible and selectable. (read-only)
 */
@property (readonly, nonatomic) NSDate *minimumDate;

/**
 * A date object representing the maximum day enable、visible and selectable. (read-only)
 */
@property (readonly, nonatomic) NSDate *maximumDate;

/**
 * A date object identifying the section of the selected date. (read-only)
 */
@property (readonly, nonatomic) NSDate *selectedDate;

/**
 * The dates representing the selected dates. (read-only)
 */
@property (readonly, nonatomic) NSArray *selectedDates;

/**
 * Reload the dates and appearance of the calendar.
 */
- (void)reloadData;

/**
 * Change the scope of the calendar. Make sure `-calendar:boundingRectWillChange:animated` is correctly adopted.
 *
 * @param scope The target scope to change.
 * @param animated YES if you want to animate the scoping; NO if the change should be immediate.
 */
- (void)setScope:(CFSCalendarScope)scope animated:(BOOL)animated;

/**
 * Selects a given date in the calendar.
 *
 * @param date A date in the calendar.
 */
- (void)selectDate:(NSDate *)date;

/**
 * Selects a given date in the calendar, optionally scrolling the date to visible area.
 *
 * @param date A date in the calendar.
 * @param scrollToDate A Boolean value that determines whether the calendar should scroll to the selected date to visible area.
 */
- (void)selectDate:(NSDate *)date scrollToDate:(BOOL)scrollToDate;

/**
 * Deselects a given date of the calendar.
 * @param date A date in the calendar.
 */
- (void)deselectDate:(NSDate *)date;

/**
 * Changes the current page of the calendar.
 *
 * @param currentPage Representing weekOfYear in week mode, or month in month mode.
 * @param animated YES if you want to animate the change in position; NO if it should be immediate.
 */
- (void)setCurrentPage:(NSDate *)currentPage animated:(BOOL)animated;

/**
 * Returns the frame for a non-placeholder cell relative to the super view of the calendar.
 *
 * @param date A date is the calendar.
 */
- (CGRect)frameForDate:(NSDate *)date;

/**
 * Returns the midpoint for a non-placeholder cell relative to the super view of the calendar.
 *
 * @param date A date is the calendar.
 */
- (CGPoint)centerForDate:(NSDate *)date;

@end




#pragma mark - Deprecate

@interface CFSCalendar (Deprecated)
@property (assign, nonatomic) IBInspectable BOOL showsPlaceholders CFSCalendarDeprecated('placeholderType');
@property (strong, nonatomic) NSDate *currentMonth CFSCalendarDeprecated('currentPage');
@property (assign, nonatomic) CFSCalendarFlow flow CFSCalendarDeprecated('scrollDirection');
- (void)setSelectedDate:(NSDate *)selectedDate CFSCalendarDeprecated(-selectDate:);
- (void)setSelectedDate:(NSDate *)selectedDate animate:(BOOL)animate CFSCalendarDeprecated(-selectDate:scrollToDate:);

@property (strong, nonatomic) NSString *identifier DEPRECATED_MSG_ATTRIBUTE("Changing calendar identifier is NOT RECOMMENDED. You should always use this library as a Gregorian calendar. Try to express other calendar as subtitles just as System calendar app does."); // Deprecated in 2.3.1

// Use NSDateFormatter
- (NSString *)stringFromDate:(NSDate *)date format:(NSString *)format CFSCalendarDeprecated([NSDateFormatter stringFromDate:]);
- (NSString *)stringFromDate:(NSDate *)date CFSCalendarDeprecated([NSDateFormatter stringFromDate:]);
- (NSDate *)dateFromString:(NSString *)string format:(NSString *)format CFSCalendarDeprecated([NSDateFormatter dateFromString:]);
- (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day CFSCalendarDeprecated([NSDateFormatter dateFromString:]);

// Use NSCalendar.
- (NSInteger)yearOfDate:(NSDate *)date CFSCalendarDeprecated(NSCalendar component:fromDate:]);
- (NSInteger)monthOfDate:(NSDate *)date CFSCalendarDeprecated(NSCalendar component:fromDate:]);
- (NSInteger)dayOfDate:(NSDate *)date CFSCalendarDeprecated(NSCalendar component:fromDate:]);
- (NSInteger)weekdayOfDate:(NSDate *)date CFSCalendarDeprecated(NSCalendar component:fromDate:]);
- (NSInteger)weekOfDate:(NSDate *)date CFSCalendarDeprecated(NSCalendar component:fromDate:]);
- (NSInteger)hourOfDate:(NSDate *)date CFSCalendarDeprecated(NSCalendar component:fromDate:]);
- (NSInteger)miniuteOfDate:(NSDate *)date CFSCalendarDeprecated(NSCalendar component:fromDate:]);
- (NSInteger)secondOfDate:(NSDate *)date CFSCalendarDeprecated(NSCalendar component:fromDate:]);
- (NSDate *)dateByIgnoringTimeComponentsOfDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateBySettingHour:minute:seconds:ofDate:options:]);
- (NSDate *)tomorrowOfDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateByAddingUnit:value:toDate:options:]);;
- (NSDate *)yesterdayOfDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateByAddingUnit:value:toDate:options:]);
- (NSDate *)dateByAddingYears:(NSInteger)years toDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateByAddingUnit:value:toDate:options:]);
- (NSDate *)dateBySubstractingYears:(NSInteger)years fromDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateByAddingUnit:value:toDate:options:]);
- (NSDate *)dateByAddingMonths:(NSInteger)months toDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateByAddingUnit:value:toDate:options:]);
- (NSDate *)dateBySubstractingMonths:(NSInteger)months fromDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateByAddingUnit:value:toDate:options:]);
- (NSDate *)dateByAddingWeeks:(NSInteger)weeks toDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateByAddingUnit:value:toDate:options:]);
- (NSDate *)dateBySubstractingWeeks:(NSInteger)weeks fromDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateByAddingUnit:value:toDate:options:]);
- (NSDate *)dateByAddingDays:(NSInteger)days toDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateByAddingUnit:value:toDate:options:]);
- (NSDate *)dateBySubstractingDays:(NSInteger)days fromDate:(NSDate *)date CFSCalendarDeprecated([NSCalendar dateByAddingUnit:value:toDate:options:]);
- (NSInteger)yearsFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate CFSCalendarDeprecated([NSCalendar components:fromDate:toDate:options:]);
- (NSInteger)monthsFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate CFSCalendarDeprecated([NSCalendar components:fromDate:toDate:options:]);
- (NSInteger)daysFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate CFSCalendarDeprecated([NSCalendar components:fromDate:toDate:options:]);
- (NSInteger)weeksFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate CFSCalendarDeprecated([NSCalendar components:fromDate:toDate:options:]);
- (BOOL)isDate:(NSDate *)date1 equalToDate:(NSDate *)date2 toCalendarUnit:(CFSCalendarUnit)unit CFSCalendarDeprecated([NSCalendar -isDate:equalToDate:toUnitGranularity:]);
- (BOOL)isDateInToday:(NSDate *)date CFSCalendarDeprecated([NSCalendar -isDateInToday:]);


@end

NS_ASSUME_NONNULL_END

