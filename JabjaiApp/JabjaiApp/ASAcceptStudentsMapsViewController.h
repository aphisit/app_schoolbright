//
//  ASAcceptStudentsMapsViewController.h
//  JabjaiApp
//
//  Created by Mac on 11/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SlideMenuController.h"
#import "CallASGetLocationOfSchoolAPI.h"
#import "CallASGetPathStartAndEndOfGoogleMapsAPI.h"
#import "CallASSubmitRequestGetStudentAPI.h"
#import "AlertDialogConfirm.h"
#import "AlertDialog.h"
#import "ASCloseFuturePickupDialog.h"
#import "UserInfoViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface ASAcceptStudentsMapsViewController : UIViewController<SlideMenuControllerDelegate,CallASGetLocationOfSchoolAPIDelegate,CallASGetPathStartAndEndOfGoogleMapsAPIDelegate,CLLocationManagerDelegate, CallASSubmitRequestGetStudentAPIDelegate,ASCloseFuturePickupDialogDelegate>
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *refreshBtn;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *mapContentView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *kilometerLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)openDrawer:(id)sender;
- (IBAction)confirmAction:(id)sender;
- (IBAction)refreshAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
