//
//  ConfirmPinCodeViewController.m
//  JabjaiApp
//
//  Created by toffee on 6/18/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ConfirmPinCodeViewController.h"
#import <CommonCrypto/CommonHMAC.h>
#import "UserData.h"
#import "UIView+Shake.h"
#import "Utils.h"
@interface ConfirmPinCodeViewController (){
    NSString *numberConfirmPin;
    NSMutableArray  *numberArray;
    NSMutableString *numberCode;
    NSString* productStringJson;
    NSBundle *myLangBundle;
}

@property (strong, nonatomic) CallPayMonnyPOSTAPI *callPayMonnyPOSTAPI;
@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic, strong) AlertDialog* alertDialog;
@property (nonatomic, strong) UIView *viewTarget;
@end

@implementation ConfirmPinCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    numberArray = [[NSMutableArray alloc] init];
    numberCode = [[NSMutableString alloc] init];
    self.confirmPinUnfair.hidden = YES;
    self.contentView.layer.masksToBounds = YES;
    self.isShowing = NO;
    
    _passView1.layer.cornerRadius = _passView1.bounds.size.width/2;
    _passView1.layer.masksToBounds = YES;
    _passView1.layer.borderWidth = 1;
    
    _passView2.layer.cornerRadius = _passView2.bounds.size.width/2;
    _passView2.layer.masksToBounds = YES;
    _passView2.layer.borderWidth = 1;
    
    _passView3.layer.cornerRadius = _passView3.bounds.size.width/2;
    _passView3.layer.masksToBounds = YES;
    _passView3.layer.borderWidth = 1;
    
    _passView4.layer.cornerRadius = _passView4.bounds.size.width/2;
    _passView4.layer.masksToBounds = YES;
    _passView4.layer.borderWidth = 1;
    
    _passView5.layer.cornerRadius = _passView5.bounds.size.width/2;
    _passView5.layer.masksToBounds = YES;
    _passView5.layer.borderWidth = 1;
    
    _passView6.layer.cornerRadius = _passView6.bounds.size.width/2;
    _passView6.layer.masksToBounds = YES;
    _passView6.layer.borderWidth = 1;
    
    [self setLanguage];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidLayoutSubviews{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLanguage{
    
    self.headerShopLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_SHOPPING", nil, myLangBundle, nil);
    self.headerConfirmPinLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_PIN", nil, myLangBundle, nil);
    self.confirmPinUnfair.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_PIN_INCORRECT", nil, myLangBundle, nil);
}

-(void)showDialogInView:(UIView *)targetView productJsonString:(NSString *)productJsonString shopId:(long long)shopId{
    [self clearPin];
    productStringJson = productJsonString;
    _shopId = shopId;
    _viewTarget = targetView;
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
        [targetView addSubview:self.view];
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height-20)];
        [targetView addSubview:self.view];
    }
    
    
//    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
//    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
//    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.contentView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentView setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
    
}
-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}
- (void)dismissDialog:(NSString *)status {
    
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(successConfirmPin:)]) {
        [self.delegate successConfirmPin:status];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

-(void)doPayMonnyOrder:(NSString*)jsonString shopId:(long long)shopId{
        [self showIndicator];
        if(self.callPayMonnyPOSTAPI != nil) {
            self.callPayMonnyPOSTAPI = nil;
        }
        self.callPayMonnyPOSTAPI = [[CallPayMonnyPOSTAPI alloc] init];
        self.callPayMonnyPOSTAPI.delegate = self;
        [self.callPayMonnyPOSTAPI call:jsonString shopId:shopId ] ;
    
}

- (void)callPayMonnyPOSTAPI:(CallPayMonnyPOSTAPI *)classObj status:(NSString *)status success:(BOOL)success{
        [self stopIndicator];
    if([status isEqualToString:@"not match data"]) {
        _confirmPinUnfair.hidden = NO;
        [self.passView1 shake:5   // 10 times
            withDelta:5    // 5 points wide
        ];
        [self.passView2 shake:5   // 10 times
            withDelta:5    // 5 points wide
        ];
        [self.passView3 shake:5   // 10 times
            withDelta:5    // 5 points wide
        ];
        [self.passView4 shake:5   // 10 times
            withDelta:5    // 5 points wide
        ];
        [self.passView5 shake:5   // 10 times
            withDelta:5    // 5 points wide
        ];
        [self.passView6 shake:5   // 10 times
            withDelta:5    // 5 points wide
        ];
        
        [self clearPin];
    }
    else if([status isEqualToString:@"not have pin code"]){
        if(self.delegate && [self.delegate respondsToSelector:@selector(addPinCode)]) {
            [self.delegate addPinCode];
        }
    }
    else if([status isEqualToString:@"some of the product is not available"]){
        [self showAlertDialogWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_MAKET_PRODUCT_IS_NOT", nil, myLangBundle, nil)];
    }
    else if([status isEqualToString:@"Low Money"]){
        [self showAlertDialogWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_MAKET_NO_MONNY", nil, myLangBundle, nil)];
    }
    else if([status isEqualToString:@"Success"]){
        [self dismissDialog:status];
    }
    else if([status isEqualToString:@"Amount exceeded"]){
        [self showAlertDialogWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_MAKET_NO_SUCCESS", nil, myLangBundle, nil)];
    }
    else{
        NSLog(@"");
    }
}
//CreateJson
-(NSString *)createJsonString:(NSString*)pinCode{
    
    NSString  *jsonString = [[NSString alloc] initWithFormat:@"{\"PinCode\" : { \"User_Id\" : \"%lld\" , \"Pin\" : \"%@\"}, \"productdatas\" : %@ }", (long long)[UserData getUserID]  ,pinCode,productStringJson];
    return jsonString;
}

//hash Pin sha256
- (NSMutableString *)hmacForKey1:(NSString *)key andStringData:(NSString *)data
{
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];

    NSMutableString *stringOut = [NSMutableString stringWithCapacity:HMAC.length];
    const unsigned char *macOutBytes = HMAC.bytes;

    for (NSInteger i=0; i<HMAC.length; ++i) {
        [stringOut appendFormat:@"%02x", macOutBytes[i]];
    }
    return stringOut;

}
-(void)clearPin{
 
    [_passView1 setBackgroundColor:[UIColor clearColor]];
    [_passView2 setBackgroundColor:[UIColor clearColor]];
    [_passView3 setBackgroundColor:[UIColor clearColor]];
    [_passView4 setBackgroundColor:[UIColor clearColor]];
    [_passView5 setBackgroundColor:[UIColor clearColor]];
    [_passView6 setBackgroundColor:[UIColor clearColor]];
    
    numberArray = [[NSMutableArray alloc] init];
    numberCode = [[NSMutableString alloc] init];
    
}

- (IBAction)actionButtonNumber:(UIControl*)sender {
    
    if (sender.tag != 10) {
        if (numberArray.count < 6) {
            [numberCode appendString:[NSString stringWithFormat:@"%d",sender.tag]];
            [numberArray addObject:[NSString stringWithFormat:@"%d",sender.tag]];
            
            if (numberArray.count == 1) {
                
                [_passView1 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 2) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 3) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 4) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 5) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 6) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
                [_passView6 setBackgroundColor:[UIColor grayColor]];
                
                NSString *key = @"ra7$K:L.]%";
                NSMutableString *hmac1 = [self hmacForKey1:key andStringData:numberCode];
                NSString *jsonString = [self createJsonString:hmac1];
                //[self doConfirmPinCode:jsonString];
                [self doPayMonnyOrder:jsonString shopId:_shopId];
            }
            
        }
    }else{
        _confirmPinUnfair.hidden = YES;
        if (numberArray.count > 0) {
            [numberCode deleteCharactersInRange:NSMakeRange(numberArray.count-1, 1)];
            [numberArray removeObjectAtIndex:numberArray.count-1];
            [_passView1 setBackgroundColor:[UIColor clearColor]];
            [_passView2 setBackgroundColor:[UIColor clearColor]];
            [_passView3 setBackgroundColor:[UIColor clearColor]];
            [_passView4 setBackgroundColor:[UIColor clearColor]];
            [_passView5 setBackgroundColor:[UIColor clearColor]];
            [_passView6 setBackgroundColor:[UIColor clearColor]];
            if (numberArray.count == 1) {
                
                [_passView1 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 2) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 3) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 4) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 5) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 6) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
                [_passView6 setBackgroundColor:[UIColor grayColor]];
            }
            
        }
    }

}

- (void)showAlertDialogWithMessage:(NSString *)message {
    if(self.alertDialog != nil) {
        if([self.alertDialog isDialogShowing]) {
            [self.alertDialog dismissDialog];
        }
    }
    else {
        self.alertDialog = [[AlertDialog alloc] init];
        self.alertDialog.delegate = self;
    }
    [self.alertDialog showDialogInView:self.viewTarget title:@"แจ้งเตือน" message:message];
}


- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)moveBack:(id)sender {
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertPinMoveBack)]) {
        [self.delegate onAlertPinMoveBack];
    }
}


@end
