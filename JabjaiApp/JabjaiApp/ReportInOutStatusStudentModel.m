//
//  ReportInOutStatusStudentModel.m
//  JabjaiApp
//
//  Created by toffee on 28/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutStatusStudentModel.h"

@implementation ReportInOutStatusStudentModel

@synthesize nameStudent = _nameStudent;
@synthesize studentId = _studentId;
@synthesize status = _status;
@synthesize pictuer = _pictuer;
@synthesize studentState = _studentState;
- (void) setNameStudent:(NSString *)nameStudent{
    _nameStudent = nameStudent;
}
- (void) setStudentId:(long long)studentId{
    _studentId = studentId;
}
- (void) setStatus:(NSString *)status{
    _status = status;
}
-(void) setPictuer:(NSString *)pictuer{
    _pictuer = pictuer;
}
- (void)setStudentState:(NSInteger)studentState{
    _studentState = studentState;
}

- (NSString*)getNameStudent{
    return _nameStudent;
}
- (long long)getStudentId{
    return _studentId;
}
- (NSString*)getStatus{
    return _status;
}
- (NSString*)getPictuer{
    return _pictuer;
}
-(NSInteger)getStudentState{
    return _studentState;
}

@end
