//
//  BehaviorScoreModel.m
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BehaviorScoreModel.h"

@implementation BehaviorScoreModel

@synthesize behaviorScoreType = _behaviorScoreType;
@synthesize behaviorScoreId = _behaviorScoreId;
@synthesize behaviorScoreName = _behaviorScoreName;
@synthesize score = _score;
@synthesize selected = _selected;

- (void)setBehaviorScoreType:(NSInteger)behaviorScoreType {
    _behaviorScoreType = behaviorScoreType;
}

- (void)setBehaviorScoreId:(long long)behaviorScoreId {
    _behaviorScoreId = behaviorScoreId;
}

- (void)setBehaviorScoreName:(NSString *)behaviorScoreName {
    _behaviorScoreName = behaviorScoreName;
}

- (void)setScore:(NSInteger)score {
    _score = score;
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
}

- (NSInteger)getBehaviorScoreType {
    return _behaviorScoreType;
}

- (long long)getBehaviorScoreId {
    return _behaviorScoreId;
}

- (NSString *)getBehaviorScoreName {
    return _behaviorScoreName;
}

- (NSInteger)getScore {
    return _score;
}

- (BOOL)isSelected {
    return _selected;
}

@end
