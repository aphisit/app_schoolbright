//
//  VerifyCodeDialog.m
//  JabjaiApp
//
//  Created by mac on 10/29/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "VerifyCodeDialog.h"
#import "Utils.h"
@interface VerifyCodeDialog ()

@property (nonatomic, assign) BOOL isShowing;

@end

@implementation VerifyCodeDialog

- (void)viewDidLoad {
    [super viewDidLoad];

    // Decorate view
    self.dialogView.layer.cornerRadius = 10;
    self.dialogView.layer.masksToBounds = YES;
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)].CGPath;
    self.headerView.layer.mask = maskLayer;
    self.headerView.layer.masksToBounds = YES;
    
    self.closeBtn.layer.cornerRadius = 20;
    self.closeBtn.layer.masksToBounds = YES;
    
    // Initialize variables
    self.isShowing = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm,*gradientCancel;
    //set color header
    [self.closeBtn layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.closeBtn.bounds;
    [self.closeBtn.layer insertSublayer:gradientConfirm atIndex:0];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

#pragma mark - VerifyCodeDialog Functions
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title verifyCode:(NSString *)verifyCode {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [self doDesignLayout];
    [UIView commitAnimations];
    [self.titleLabel setText:title];
    [self.verifyCodeLabel setText:verifyCode];
    
    self.isShowing = YES;
}

- (void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onVerifyCodeDialogClose)]) {
        [self.delegate onVerifyCodeDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

@end
