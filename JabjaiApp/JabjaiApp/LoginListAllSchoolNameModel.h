//
//  LoginListAllSchoolNameModel.h
//  JabjaiApp
//
//  Created by toffee on 26/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginListAllSchoolNameModel : NSObject
@property (nonatomic) long long schoolID;
@property (nonatomic) NSString *schoolName;

-(void)setSchoolID:(long long)schoolID;
-(void)setSchoolName:(NSString * _Nonnull)schoolName;

-(long long)getSchoolID;
-(NSString*)getSchoolName;
@end

NS_ASSUME_NONNULL_END
