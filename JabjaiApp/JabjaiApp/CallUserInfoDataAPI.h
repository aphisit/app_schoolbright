//
//  CallUserInfoDataAPI.h
//  JabjaiApp
//
//  Created by toffee on 7/2/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CallUserInfoDataAPI;
@protocol CallUserInfoDataAPIDelegate <NSObject>

@end
@interface CallUserInfoDataAPI : NSObject
@property (nonatomic, weak) id<CallUserInfoDataAPIDelegate> delegate;

@end
