//
//  MGListMargetViewController.h
//  JabjaiApp
//
//  Created by toffee on 4/10/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfoDBHelper.h"
#import "CallGMListMarGetAPI.h"
#import "MargetListTableViewCell.h"
#import "CallGMGetUserInfoDataAPI.h"
#import "SlideMenuController.h"
#import "UnAuthorizeMenuDialog.h"
#import "CallGetMenuListAPI.h"
#import "LoginViewController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface MGListMargetViewController : UIViewController<CallGMListMarGetAPIDelegate, UITableViewDelegate, UITableViewDataSource, MargetListTableViewCellDelegate, CallGMGetUserInfoDataAPIDelegate, SlideMenuControllerDelegate,UnAuthorizeMenuDialogDelegate,CallGetMenuListAPIDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>
  
@property (strong, nonatomic) NSArray<CallGMListMarGetAPI *> *margetArray;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerShoppingLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerBalanceLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *monnyLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UILabel *showNoShopLabel;

- (IBAction)openDarwer:(id)sender;



@end
