//
//  ReportSendNewsPersonRecieveTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 4/9/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportSendNewsPersonRecieveTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *personImage;
@property (weak, nonatomic) IBOutlet UILabel *personName;
@property (weak, nonatomic) IBOutlet UILabel *statusRead;
@property (weak, nonatomic) IBOutlet UILabel *timeRead;

@end
