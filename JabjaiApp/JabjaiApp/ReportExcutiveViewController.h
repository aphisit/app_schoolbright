//
//  ReportExcutiveViewController.h
//  JabjaiApp
//
//  Created by mac on 4/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"
#import "CallGetMenuListAPI.h"

@class ReportExcutiveViewController;
@protocol ReportExcutiveViewControllerDelegate <NSObject>

- (void)changePageReportExcutiveViewController:(ReportExcutiveViewController *)classObj numberPage:(int)numberPage;
@end

@interface ReportExcutiveViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate,CallGetMenuListAPIDelegate>
@property (nonatomic, weak) id<ReportExcutiveViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
