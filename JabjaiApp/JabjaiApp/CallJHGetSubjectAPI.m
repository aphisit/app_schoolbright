//
//  CallJHGetSubjectAPI.m
//  JabjaiApp
//
//  Created by toffee on 11/22/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallJHGetSubjectAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallJHGetSubjectAPI () {
    NSInteger connectCounter;
    NSMutableArray<JHSubjectModel *> *subjectArray;
}
@end

@implementation CallJHGetSubjectAPI
- (void)call:(long long)schoolId classroomId:(NSString*)classroomId {
    connectCounter = 0;
    [self getSubjectWithSchoolId:schoolId classroomId:classroomId];
}

#pragma mark - Get API Data
- (void)getSubjectWithSchoolId:(long long)schoolId classroomId:(NSString*)classroomId {
    
    NSString *URLString = [APIURL getJHSubjectWithSchoolId:schoolId classroomId:classroomId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSubjectWithSchoolId:schoolId classroomId:classroomId];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubjectWithSchoolId:schoolId classroomId:classroomId];
                }
                else {
                    
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if([returnedData isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDict = returnedData;
                
                if([dataDict objectForKey:@"Status"] != nil && [[dataDict objectForKey:@"Status"] integerValue] == -1) {
                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(notArrangeTimeTable:)]) {
                        [self.delegate notArrangeTimeTable:self];
                    }
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubjectWithSchoolId:schoolId classroomId:classroomId];
                }
                else {
                    
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(subjectArray != nil) {
                    [subjectArray removeAllObjects];
                    subjectArray = nil;
                }
                
                subjectArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSString *subjectID = [dataDict objectForKey:@"planeid"] ;
                    
                    NSMutableString *subjectName;
                    
                    if(![[dataDict objectForKey:@"planename"] isKindOfClass:[NSNull class]]) {
                        subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"planename"]];
                    }
                    else {
                        subjectName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                   
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                  
                    
                    JHSubjectModel *model = [[JHSubjectModel alloc] init];
                    model.subjectID = subjectID;
                    model.subjectName = subjectName;
                   
                    
                    [subjectArray addObject:model];
                    
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callJHGetSubjectAPI:data:success:)]) {
                    [self.delegate callJHGetSubjectAPI:self data:subjectArray success:YES];
                }
                
                return ;
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callJHGetSubjectAPI:data:success:)]) {
                [self.delegate callJHGetSubjectAPI:self data:nil success:NO];
            }
        }
        
    }];
    
}

@end
