//
//  ReportInOutAllDataStudentModel.h
//  JabjaiApp
//
//  Created by toffee on 8/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutAllDataStudentModel : NSObject

@property (nonatomic) NSInteger status_0;
@property (nonatomic) NSInteger status_1;
@property (nonatomic) NSInteger status_2;
@property (nonatomic) NSInteger status_3;
@property (nonatomic) NSInteger status_4;
@property (nonatomic) NSInteger status_5;
@property (nonatomic) NSInteger status_6;

- (void)setStatus_0:(NSInteger)status_0;
- (void)setStatus_1:(NSInteger)status_1;
- (void)setStatus_2:(NSInteger)status_2;
- (void)setStatus_3:(NSInteger)status_3;
- (void)setStatus_4:(NSInteger)status_4;
- (void)setStatus_5:(NSInteger)status_5;
- (void)setStatus_6:(NSInteger)status_6;

- (NSInteger)getStatus_0;
- (NSInteger)getStatus_1;
- (NSInteger)getStatus_2;
- (NSInteger)getStatus_3;
- (NSInteger)getStatus_4;
- (NSInteger)getStatus_5;
- (NSInteger)getStatus_6;





@end

NS_ASSUME_NONNULL_END
