//
//  CallGetEXReportAttendSchoolTeacherListAPI.m
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetEXReportAttendSchoolTeacherListAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetEXReportAttendSchoolTeacherListAPI () {
    NSInteger connectCounter;
    NSMutableArray<EXReportNameStatusModel *> *nameStatusArray;
    
}

@end
@implementation CallGetEXReportAttendSchoolTeacherListAPI

- (instancetype)init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(long long)schoolId date:(NSDate *)date {
    [self getDataFromServer:schoolId date:date];
}

- (void)getDataFromServer:(long long)schoolId date:(NSDate *)date {
    
    NSString *urlString = [APIURL getEXReportAttendSchoolTeacherListWithSchoolId:schoolId date:date];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataFromServer:schoolId date:date];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(nameStatusArray != nil) {
                    [nameStatusArray removeAllObjects];
                    nameStatusArray = nil;
                }
                nameStatusArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSString *name = [dataDict objectForKey:@"theachername"];
                    NSInteger status = [[dataDict objectForKey:@"status"] integerValue];
                    
                    EXReportNameStatusModel *model = [[EXReportNameStatusModel alloc] init];
                    [model setName:name];
                    [model setStatus:status];
                    
                    [nameStatusArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetEXReportAttendSchoolTeacherListAPI:data:success:)]) {
                    [self.delegate callGetEXReportAttendSchoolTeacherListAPI:self data:nameStatusArray success:YES];
                }
                
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetEXReportAttendSchoolTeacherListAPI:data:success:)]) {
                [self.delegate callGetEXReportAttendSchoolTeacherListAPI:self data:nil success:NO];
            }
        }
    }];
}
@end
