//
//  TEReportHistoryStudentStatusViewController.h
//  JabjaiApp
//
//  Created by toffee on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TEReportCalendarViewController.h"
#import "CallGetTEHistoryStudentStatusList.h"

@interface TEReportHistoryStudentStatusViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CallGetTEHistoryStudentStatusListAPIDelegate>
@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
