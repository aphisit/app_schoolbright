//
//  Utils.h
//  JabjaiApp
//
//  Created by mac on 10/28/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constant.h"
#import "TAStudentStatusModel.h"
#import "BSSelectedStudent.h"
#import "BehaviorScoreModel.h"

static inline void delay(NSTimeInterval delay, dispatch_block_t block) {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), block);
}

static UIColor * UIColorWithHexString(NSString *hex) {
    unsigned int rgb = 0;
    [[NSScanner scannerWithString:
      [[hex uppercaseString] stringByTrimmingCharactersInSet:
       [[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEF"] invertedSet]]]
     scanHexInt:&rgb];
    return [UIColor colorWithRed:((CGFloat)((rgb & 0xFF0000) >> 16)) / 255.0
                           green:((CGFloat)((rgb & 0xFF00) >> 8)) / 255.0
                            blue:((CGFloat)(rgb & 0xFF)) / 255.0
                           alpha:1.0];
}

@interface Utils : NSObject

+ (void)downloadDataFromURL:(NSURL *)url withCompletionHandler:(void(^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler;
+ (void)uploadImageFromURL:(NSURL *)url data:(NSData *)data imageparameterName:(NSString *)imageParameterName imageFileName:(NSString *)imageFileName mimeType:(MIME_TYPE)mimeType parameters:(NSDictionary<NSString *, NSString *> *)parameters withCompletionHandler:(void(^)(id responseObject, NSError *error))completionHandler;

+ (void)uploadFileFromURL:(NSURL *)url data:(NSData *)data imageparameterName:(NSString *)imageParameterName imageFileName:(NSString *)imageFileName mimeType:(NSString *)mimeType parameters:(NSDictionary<NSString *, NSString *> *)parameters withCompletionHandler:(void(^)(id responseObject, NSError *error))completionHandler;

+ (BOOL)isValidEmail:(NSString *)email;
+ (BOOL)stringIsNumeric:(NSString *)str;
+ (BOOL)isInteger:(NSString *)str;
+ (BOOL)isIntegerWithDouble:(double)number;
+ (NSBundle *)getLanguage;
+ (NSString*)getStatus:(NSInteger)number;
+ (NSString *)encodeString:(NSString *)str;
+ (NSString *)getSeverDateTimeFormat;
+ (NSString *)dateToServerDateFormat:(NSDate *)date;
+ (NSString *)getXMLDateFormat;
+ (NSArray *)getThaiMonthRangeWithStartMonth:(NSInteger)startMonth endMonth:(NSInteger)endMonth;
+ (NSInteger)getThaiMonthNumberWithName:(NSString *)month;
+ (NSString *)dateInStringFormat:(NSDate *)date;
+ (NSString *)datePunctuateStringFormat:(NSDate *)date;
+ (NSString *)dateInStringFormat:(NSDate *)date format:(NSString *)format;
+ (NSDate *)parseServerDateStringToDate:(NSString *)dateStr;
+ (NSDateFormatter *)getDateFormatter;
+ (NSCalendar *)getGregorianCalendar;
+ (NSString *)getThaiDateFormatWithDate:(NSDate *)date;
+ (NSString *)getBuddhistEraFormatWithDate:(NSDate *)date;
+ (unsigned)parseIntFromData:(NSData *)data;
+ (NSString *)getStudentStatusJSON:(NSArray<TAStudentStatusModel *> *)studentStatusArray;
+ (NSString *)getStudentsBehaviorsScoreJSONWithSelectedStudentArray:(NSArray<BSSelectedStudent *> *)selectedStudentsArray selectedBehaviorsScoreArray:(NSArray<BehaviorScoreModel *> *)selectedBehaviorsScoreArray;
+ (NSInteger)compareVersion:(NSString *)version1 version2:(NSString *)version2;
+ (NSInteger)getSendNewsJSONWithSelectedStudentArray:(NSMutableArray *)selectedTeacherArray;
+(CAGradientLayer *)getGradientColorHeader;
+(CAGradientLayer *)getGradientColorNextAtion;
+(CAGradientLayer *)getGradientColorStatus:(NSString*)stringColor;
+ (NSString *)getThaiDateAcronymFormatWithDate:(NSDate *)date;
+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size ;

@end
