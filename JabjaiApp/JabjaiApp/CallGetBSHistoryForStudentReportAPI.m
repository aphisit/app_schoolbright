//
//  CallGetBSHistoryForStudentReportAPI.m
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//
#import "CallGetBSHistoryForStudentReportAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetBSHistoryForStudentReportAPI () {
    NSInteger connectCounter;
    NSMutableArray<BSHistoryModel *> *bsHistoryModelArray;
    double remainingScore;
}
@end

@implementation CallGetBSHistoryForStudentReportAPI

- (id)init {
    self = [super init];
    if(self) {
        connectCounter = 0;
        remainingScore = 0;
    }
    return self;
}

- (void)call:(long long)userId schoolid:(long long)schoolid{
    [self getBSHistoryData:userId schoolid:schoolid];
}

#pragma mark - Get API Data

- (void)getBSHistoryData:(long long)userId schoolid:(long long)schoolid {
    
    NSString *URLString = [APIURL getBehaviroScoreHistoryForStudentReportWithUserId:userId schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getBSHistoryData:userId schoolid:schoolid];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getBSHistoryData:userId schoolid:schoolid];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getBSHistoryData:userId schoolid:schoolid];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(bsHistoryModelArray != nil) {
                    [bsHistoryModelArray removeAllObjects];
                    bsHistoryModelArray = nil;
                }
                
                bsHistoryModelArray = [[NSMutableArray alloc] init];
                
                if(returnedArray.count > 0) {
                    NSDictionary *returnedDict = [returnedArray objectAtIndex:0];
                    
                    remainingScore = [[returnedDict objectForKey:@"LastScore"] doubleValue];
                    
                    if([returnedDict objectForKey:@"BehaviorsHistory"] != nil) {
                        
                        NSArray *dataArray = [returnedDict objectForKey:@"BehaviorsHistory"];
                        
                        for(int i=0; i<dataArray.count; i++) {
                            
                            NSDictionary *dataDict = [dataArray objectAtIndex:i];
                            
                            NSMutableString *behaviorName, *studentName, *recorder, *remark;
                            
                            if(![[dataDict objectForKey:@"BehaviorsName"] isKindOfClass:[NSNull class]]) {
                                behaviorName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"BehaviorsName"]];
                            }
                            else {
                                behaviorName = [[NSMutableString alloc] initWithString:@""];
                            }
                            
                            if(![[dataDict objectForKey:@"TheacherName"] isKindOfClass:[NSNull class]]) {
                                recorder = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TheacherName"]];
                            }
                            else {
                                recorder = [[NSMutableString alloc] initWithString:@""];
                            }
                            
                            if(![[dataDict objectForKey:@"Note"] isKindOfClass:[NSNull class]]) {
                                remark = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Note"]];
                            }
                            else {
                                remark = [[NSMutableString alloc] initWithString:@""];
                            }
                            
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) behaviorName);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) recorder);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) remark);
                            
                            studentName = [[NSMutableString alloc] initWithString:@""];
                            
                            NSInteger type = [[dataDict objectForKey:@"BehaviorsType"] integerValue];
                            double score = [[dataDict objectForKey:@"Score"] doubleValue];
                            NSString *dateStr = [dataDict objectForKey:@"Day"];
                            NSDate *dateTime = [Utils parseServerDateStringToDate:dateStr];
                            
                            BSHistoryModel *model = [[BSHistoryModel alloc] init];
                            [model setBehaviorType:type];
                            [model setBehaviorName:behaviorName];
                            [model setBehaviorScore:score];
                            [model setDateTime:dateTime];
                            [model setStudentName:studentName];
                            [model setRecorder:recorder];
                            [model setRemark:remark];
                            
                            [bsHistoryModelArray addObject:model];

                        }
                    }
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetBSHistoryForStudentReportAPI:data:remainingScore:success:)]) {
                    
                    [self.delegate callGetBSHistoryForStudentReportAPI:self data:bsHistoryModelArray remainingScore:remainingScore success:YES];
                }
                
            }
            
        }
        
        // check failed
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetBSHistoryForStudentReportAPI:data:remainingScore:success:)]) {
                
                [self.delegate callGetBSHistoryForStudentReportAPI:self data:nil remainingScore:remainingScore success:NO];
            }
        }
        
    }];
        
}

@end
