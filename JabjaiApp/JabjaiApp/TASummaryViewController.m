//
//  TASummaryViewController.m
//  JabjaiApp
//
//  Created by mac on 7/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TASummaryViewController.h"
#import "TASummaryHeaderTableViewCell.h"
#import "TASummaryChildTableViewCell.h"
#import "TAStudentListViewController.h"
#import "TASelectClassLevelViewController.h"
#import "Utils.h"
#import "UserData.h"
#import "AlertDialogConfirm.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TASummaryViewController () {
    
    NSArray *sectionKeys;
    NSMutableArray *expandedSections;
    NSMutableDictionary<NSString *, NSMutableArray<TAStudentStatusModel *> *> *studentInTableSection;
    NSInteger totalStudents;
    BOOL updateStatusSuccess;
    
    CallUpdateStudentStatusPOSTAPI *callUpdateStudentStatusPOSTAPI;
    
    // The dialog
    AlertDialogConfirm *alertDialog;
    AlertDialog *alertDialogNoSeccess;
    
    NSBundle *myLangBundle;
}

@end

static NSString *headerCellIdentifier = @"HeaderCell";
static NSString *childCellIdentifier = @"ChildCell";

static NSString *SECTION_ONTIME = @"SECTION_ONTIME";
static NSString *SECTION_LATE = @"SECTION_LATE";
static NSString *SECTION_ABSENCE = @"SECTION_ABSENCE";
static NSString *SECTION_PERSONAL_LEAVE = @"SECTION_PERSONAL_LEAVE";
static NSString *SECTION_SICK_LEAVE = @"SECTION_SICK_LEAVE";
static NSString *SECTION_EVENT_LEAVE = @"SECTION_EVENT_LEAVE";
static NSString *SECTION_UNDEFINED = @"SECTION_UNDEFINED";

@implementation TASummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    [self setLanguage];
    
    // register table nib
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TASummaryHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:headerCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TASummaryChildTableViewCell class]) bundle:nil] forCellReuseIdentifier:childCellIdentifier];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    // Initial table expandable variables
    sectionKeys = @[SECTION_ONTIME, SECTION_LATE, SECTION_ABSENCE, SECTION_PERSONAL_LEAVE, SECTION_SICK_LEAVE, SECTION_EVENT_LEAVE, SECTION_UNDEFINED];
    expandedSections = [[NSMutableArray alloc] init];
    studentInTableSection = [[NSMutableDictionary alloc] init];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_ONTIME];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_LATE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_ABSENCE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_PERSONAL_LEAVE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_SICK_LEAVE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_EVENT_LEAVE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_UNDEFINED];
    
    if(self.studentStatusArray != nil) {
        totalStudents = self.studentStatusArray.count;
    }
    else {
        totalStudents = 0;
    }
    
    updateStatusSuccess = NO;
    [self performStudentStatusData];
    [self.tableView reloadData];
    
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.submitBtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.submitBtn.bounds;
    [self.submitBtn.layer insertSublayer:gradientNext atIndex:0];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLanguage{
  
    if(self.mode == TA_MODE_CHECK){
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN",nil,myLangBundle,nil);
    }else if(self.mode == TA_MODE_EDIT){
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_EDIT",nil,myLangBundle,nil);
    }
    self.headerSummaryLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_SUMMARY",nil,myLangBundle,nil);
    [self.submitBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_TAKE_ATTEN_SUBMIT",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
}

- (void)performStudentStatusData {
    
    if(self.studentStatusArray != nil) {
        
        for(NSString *key in studentInTableSection.allKeys) {
            [[studentInTableSection objectForKey:key] removeAllObjects];
        }
        
        for(TAStudentStatusModel *model in self.studentStatusArray) {
            
            switch ([model getScanStatus]) {
                case 0:
                    [[studentInTableSection objectForKey:SECTION_ONTIME] addObject:model];
                    break;
                case 1:
                    [[studentInTableSection objectForKey:SECTION_LATE] addObject:model];
                    break;
                case 3:
                    [[studentInTableSection objectForKey:SECTION_ABSENCE] addObject:model];
                    break;
                case 4:
                    [[studentInTableSection objectForKey:SECTION_PERSONAL_LEAVE] addObject:model];
                    break;
                case 5:
                    [[studentInTableSection objectForKey:SECTION_SICK_LEAVE] addObject:model];
                    break;
                case 6:
                    [[studentInTableSection objectForKey:SECTION_EVENT_LEAVE] addObject:model];
                    break;
                default:
                    [[studentInTableSection objectForKey:SECTION_UNDEFINED] addObject:model];
                    break;
            }
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(sectionKeys != nil) {
        return sectionKeys.count;
    }
    else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([expandedSections containsObject:@(section)]) {
        NSString *key = [sectionKeys objectAtIndex:section];
        
        if([studentInTableSection objectForKey:key] != nil) {
            NSArray *students = [studentInTableSection objectForKey:key];
            return students.count + 1;
        }
        else {
            return 1;
        }
    }
    else {
        return 1; // show table header cell
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *key = [sectionKeys objectAtIndex:indexPath.section];
    
    if(indexPath.row == 0) { // For the first row of each section we'll show header
        TASummaryHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier forIndexPath:indexPath];
        
        BOOL isExpanded = [expandedSections containsObject:@(indexPath.section)];
        
        if(isExpanded) {
            cell.arrowImageView.image = [UIImage imageNamed:@"ic_sort_up"];
        }
        else {
            cell.arrowImageView.image = [UIImage imageNamed:@"ic_sort_down"];
        }
        
        NSArray *students = [studentInTableSection objectForKey:key];
        
        NSInteger numberOfStudents = 0;
        
        if(students != nil && students.count > 0) {
            [cell.arrowImageView setAlpha:1.0f];
            numberOfStudents = students.count;
        }
        else {
            [cell.arrowImageView setAlpha:0.0f];
        }
        
        NSString *titleStr;
        
        if([key isEqualToString:SECTION_ONTIME]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_ONTIME",nil,myLangBundle,nil);
        }
        else if([key isEqualToString:SECTION_LATE]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_LATE",nil,myLangBundle,nil);
        }
        else if([key isEqualToString:SECTION_ABSENCE]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_ABSENCE",nil,myLangBundle,nil);
        }
        else if([key isEqualToString:SECTION_PERSONAL_LEAVE]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_PERSONAL_LEAVE",nil,myLangBundle,nil);
        }
        else if([key isEqualToString:SECTION_SICK_LEAVE]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_SICK_LEAVE",nil,myLangBundle,nil);
        }
        else if([key isEqualToString:SECTION_EVENT_LEAVE]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_EVENT_LEAVE",nil,myLangBundle,nil);
        }
        else {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN_UNDEFINED",nil,myLangBundle,nil);;
        }
        
        cell.titleLabel.text = titleStr;
        cell.numberLabel.text = [NSString stringWithFormat:@"%li/%li", (long)numberOfStudents, (long)totalStudents];
        
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.userInteractionEnabled = YES;
        
        return cell;
        
    }
    else {
        
        TASummaryChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:childCellIdentifier forIndexPath:indexPath];
        TAStudentStatusModel *model = [[studentInTableSection objectForKey:key] objectAtIndex:indexPath.row - 1];
        NSArray *array = [[NSString stringWithFormat:@"%@", [model getStudentName]] componentsSeparatedByString:@" "];
        if (array.count>1) {
            cell.titleLabel.text =  [NSString stringWithFormat:@"%@ %@",array[0],array[1]];
        }else{
            cell.titleLabel.text = array[0];
        }
        cell.runNumber.text = [NSString stringWithFormat:@"%ld.",indexPath.row];
        if ([model getStudentPic] == NULL || [[model getStudentPic] isEqual:@""]) {
            cell.userImageView.image = [UIImage imageNamed:@"ic_user_info"];
        }
        else{
            
            [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:[model getStudentPic]]];
        }
        
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height /2;
        cell.userImageView.layer.masksToBounds = YES;
        cell.userImageView.layer.borderWidth = 0;
        //cell.titleLabel.text = [NSString stringWithFormat:@"%li. %@", (long)indexPath.row, [model getStudentName]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0) {
        return 60;
    }
    else {
        return 44;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([expandedSections containsObject:@(indexPath.section)]) {
        
        if(indexPath.row == 0) { // If select header of each section, then toggle up and down
            [expandedSections removeObject:@(indexPath.section)];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    else {
        [expandedSections addObject:@(indexPath.section)];
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - API Caller

- (void)updateStudentStatusWithJSON:(NSString *)jsonString {
    
    if(callUpdateStudentStatusPOSTAPI != nil) {
        callUpdateStudentStatusPOSTAPI = nil;
    }
    
    [self showIndicator];
    
    callUpdateStudentStatusPOSTAPI = [[CallUpdateStudentStatusPOSTAPI alloc] init];
    callUpdateStudentStatusPOSTAPI.delegate = self;
    [callUpdateStudentStatusPOSTAPI call:[UserData getSchoolId] subjectId:self.subjectId teacherId:[UserData getUserID] jsonString:jsonString date:[Utils dateToServerDateFormat:self.dateCalendar]];
}

- (void)callUpdateStudentStatusPOSTAPI:(CallUpdateStudentStatusPOSTAPI *)classObj response:(NSString *)response success:(BOOL)success {
    
    [self stopIndicator];
    updateStatusSuccess = success;
    
    if(success) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_TAKE_ATTEN_COMPLETED",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
    }
    else {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_TAKE_ATTEN_NO_COMPLETED",nil,myLangBundle,nil);
        [self showAlertDialogNoSeccess:alertMessage];
    }
}

#pragma mark - Dialog

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showAlertDialogNoSeccess:(NSString *)message {
    
    if(alertDialogNoSeccess != nil && [alertDialogNoSeccess isDialogShowing]) {
        [alertDialogNoSeccess dismissDialog];
    }
    else {
        alertDialogNoSeccess = [[AlertDialog alloc] init];
        alertDialogNoSeccess.delegate = self;
    }
    
    [alertDialogNoSeccess showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)onAlertDialogClose {
    _submitBtn.enabled = YES;
    if(updateStatusSuccess) {
        TASelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TakeClassAttendanceStoryboard"];
        viewController.mode = _mode;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}


#pragma mark - Action functions
- (IBAction)moveBack:(id)sender {
    
    TAStudentListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TAStudentListStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.subjectArray = _subjectArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.selectedSubjectIndex = _selectedSubjectIndex;
    viewController.mode = _mode;
    
    viewController.studentStatusArray = _studentStatusArray;
    viewController.classroomId = _classroomId;
    viewController.subjectId = _subjectId;
    viewController.subjectName = _subjectName;
    
    [self.slideMenuController changeMainViewController:viewController close:YES]
    ;
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (IBAction)actionSubmit:(id)sender {
    _submitBtn.enabled = NO;
    NSString *jsonString = [Utils getStudentStatusJSON:_studentStatusArray];
    NSLog(@"%@", jsonString);
    [self updateStudentStatusWithJSON:jsonString];
    
}
@end
