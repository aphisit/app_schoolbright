//
//  CallGetSchoolClassroomJForobHomeAPI.h
//  JabjaiApp
//
//  Created by toffee on 1/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JHSelectRoomRadioModel.h"

@class CallGetSchoolClassroomJForobHomeAPI;

@protocol CallGetSchoolClassroomJForobHomeAPIDelegate <NSObject>
@optional
- (void)callGetSchoolClassroomGroupAPI:(CallGetSchoolClassroomJForobHomeAPI *)classObj data:(NSArray<JHSelectRoomRadioModel *> *)data success:(BOOL)success;

@end
@interface CallGetSchoolClassroomJForobHomeAPI : NSObject
@property (nonatomic, weak) id<CallGetSchoolClassroomJForobHomeAPIDelegate> delegate;

- (void)call:(long long)schoolId classLevelId:(long long)classLevelId;

@end
