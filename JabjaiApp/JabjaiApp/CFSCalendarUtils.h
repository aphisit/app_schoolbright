//
//  FSCalendarUtils.h
//  Pods
//
//  Created by mac on 12/9/2559 BE.
//
//

#import <Foundation/Foundation.h>

@interface CFSCalendarUtils : NSObject

+(NSDate *)minDate:(NSArray *)dates;
+(NSDate *)maxDate:(NSArray *)dates;
+(BOOL)sameDate:(NSDate *)date1 date2:(NSDate *)date2;
+(BOOL)betweenDate:(NSDate *)date minDate:(NSDate *)minDate maxDate:(NSDate *)maxDate;

@end
