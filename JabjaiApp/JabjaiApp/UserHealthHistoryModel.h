//
//  UserHealthHistoryModel.h
//  JabjaiApp
//
//  Created by Mac on 1/7/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserHealthHistoryModel : NSObject
@property (nonatomic) NSInteger number;
@property (strong, nonatomic)NSString *time;
@property (strong, nonatomic)NSString *date;
@property (strong, nonatomic)NSString *temPerature;

-(void)setNumber:(NSInteger)number;
-(void)setTime:(NSString *)time;
-(void)setDate:(NSString * _Nonnull)date;
-(void)setTemPerature:(NSString * _Nonnull)temPerature;

-(NSInteger)getNumber;
-(NSString *)getTime;
-(NSString *)getDate;
-(NSString *)getTemperature;


@end

NS_ASSUME_NONNULL_END
