//
//  BSSelectedStudent.h
//  JabjaiApp
//
//  Created by mac on 7/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSSelectedStudent : NSObject

@property (nonatomic) long long studentId;
@property (nonatomic) NSString *studentName;
@property (nonatomic) NSString *pic;
@property (nonatomic) BOOL selected;

- (void)setStudentId:(long long)studentId;
- (void)setStudentName:(NSString *)studentName;
- (void)setStudentPic:(NSString *)pic;
- (void)setSelected:(BOOL)selected;

- (long long)getStudentId;
- (NSString *)getStudentName;
- (NSString *)getStudentPic;
- (BOOL)isSelected;

@end
