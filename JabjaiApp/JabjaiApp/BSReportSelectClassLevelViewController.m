//
//  BSReportSelectClassLevelViewController.m
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSReportSelectClassLevelViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "Constant.h"
#import <ARSlidingPanel/ARSPContainerController.h>
#import "BSReportScoreListViewController.h"
#import "BSReportARSPContainerController.h"

#import "BSReportCalendarViewController.h"

@interface BSReportSelectClassLevelViewController () {
    
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    NSMutableArray *statusStringArray;
    
    // The dialog
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;

}

@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;

@end

static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";
static NSString *statusRequestCode = @"statusRequestCode";

@implementation BSReportSelectClassLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.classLevelTextField.delegate = self;
    self.classroomTextField.delegate = self;
    self.statusTextField.delegate = self;
    
    if(_classLevelArray != nil && _classroomArray != nil && _statusArray != nil) {
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classLevelArray.count; i++) {
            SchoolLevelModel *model = [_classLevelArray objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classroomArray.count; i++) {
            SchoolClassroomModel *model = [_classroomArray objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
        
        statusStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_statusArray.count; i++) {
            BSStatusModel *model = [_statusArray objectAtIndex:i];
            [statusStringArray addObject:[model getStatusName]];
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:_selectedClassLevelIndex] getClassLevelName];
        self.classroomTextField.text = [[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomName];
        self.statusTextField.text = [[_statusArray objectAtIndex:_selectedStatusIndex] getStatusName];
        
    }
    else {
        _selectedClassLevelIndex = -1;
        _selectedClassroomIndex = -1;
        _selectedStatusIndex = 0;
        
        NSMutableArray *statusMutableArray = [[NSMutableArray alloc] init];
        
        BSStatusModel *statusModel = [[BSStatusModel alloc] init];
        [statusModel setStatusId:2];
        [statusModel setStatusName:@"ทั้งหมด"];
        [statusMutableArray addObject:statusModel];
        
        statusModel = [[BSStatusModel alloc] init];
        [statusModel setStatusId:0];
        [statusModel setStatusName:@"เพิ่ม"];
        [statusMutableArray addObject:statusModel];
        
        statusModel = [[BSStatusModel alloc] init];
        [statusModel setStatusId:1];
        [statusModel setStatusName:@"ลด"];
        [statusMutableArray addObject:statusModel];
        
        self.statusArray = statusMutableArray;
        
        statusStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_statusArray.count; i++) {
            BSStatusModel *model = [_statusArray objectAtIndex:i];
            [statusStringArray addObject:[model getStatusName]];
        }
        
        self.statusTextField.text = [[_statusArray objectAtIndex:_selectedStatusIndex] getStatusName];
        self.statusId = [[_statusArray objectAtIndex:_selectedStatusIndex] getStatusId];

        // Call api
        [self getSchoolClassLevel];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (screenSize.height <= 568.0f) {
            CALayer *bottomBorder = [CALayer layer];
            bottomBorder.frame = CGRectMake(0.0f, self.classLevelTextField.frame.size.height *0.975, self.classLevelTextField.frame.size.width *10, 1.0f);
            bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
            [self.classLevelTextField.layer addSublayer:bottomBorder];
            
            CALayer *bottomBorder1 = [CALayer layer];
            bottomBorder1.frame = CGRectMake(0.0f, self.classroomTextField.frame.size.height *0.985, self.classroomTextField.frame.size.width *10, 1.0f);
            bottomBorder1.backgroundColor = [UIColor blackColor].CGColor;
            [self.classroomTextField.layer addSublayer:bottomBorder1];
            
            CALayer *bottomBorder2 = [CALayer layer];
            bottomBorder2.frame = CGRectMake(0.0f, self.statusTextField.frame.size.height *0.985, self.statusTextField.frame.size.width *10, 1.0f);
            bottomBorder2.backgroundColor = [UIColor blackColor].CGColor;
            [self.statusTextField.layer addSublayer:bottomBorder1];
        }
        else{
            
            CALayer *bottomBorder = [CALayer layer];
            bottomBorder.frame = CGRectMake(0.0f, self.classLevelTextField.frame.size.height *1.15, self.classLevelTextField.frame.size.width *10, 1.0f);
            bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
            [self.classLevelTextField.layer addSublayer:bottomBorder];
            
            CALayer *bottomBorder1 = [CALayer layer];
            bottomBorder1.frame = CGRectMake(0.0f, self.classroomTextField.frame.size.height *1.15, self.classroomTextField.frame.size.width *10, 1.0f);
            bottomBorder1.backgroundColor = [UIColor blackColor].CGColor;
            [self.classroomTextField.layer addSublayer:bottomBorder1];
            
            CALayer *bottomBorder2 = [CALayer layer];
            bottomBorder2.frame = CGRectMake(0.0f, self.statusTextField.frame.size.height *1.15, self.statusTextField.frame.size.width *10, 1.0f);
            bottomBorder2.backgroundColor = [UIColor blackColor].CGColor;
            [self.statusTextField.layer addSublayer:bottomBorder2];
            
        }
    }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // User press class level textfield
        
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }
        
        return NO;
    }
    else if(textField.tag == 3) { // User press status textfield
        
        if(statusStringArray != nil && statusStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:statusRequestCode data:statusStringArray];
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - API Caller

- (void)getSchoolClassLevel {
    [self showIndicator];
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    [self showIndicator];
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}

#pragma mark - CallGetSchoolClassLevelAPIDelegate

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classLevelArray = data;
        
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
    }
}

#pragma mark - CallGetStudentClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classroomArray = data;
        
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
        
        if(index == _selectedClassLevelIndex) {
            return;
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classroomTextField.text = @"";
        _selectedClassroomIndex = -1;
        _classLevelId = 0;
        
        // get classroom data with class level
        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:_classLevelId];
        
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        
        if(index == _selectedClassroomIndex) {
            return;
        }
        
        self.classroomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        
        _classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        
    }
    else if([requestCode isEqualToString:statusRequestCode]) {
        
        if(index == _selectedStatusIndex) {
            return;
        }
        
        self.statusTextField.text = [[_statusArray objectAtIndex:index] getStatusName];
        _selectedStatusIndex = index;
        
        _statusId = [[_statusArray objectAtIndex:index] getStatusId];
    }
    
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classroomTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกชั้นเรียน";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.statusTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกสถานะ";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Action functions
- (IBAction)openDrawer:(id)sender {
//    [self.revealViewController revealToggle:self.revealViewController];
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}

- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        BSReportCalendarViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSReportCalendarStoryboard"];
        
        viewController.classLevelArray = _classLevelArray;
        viewController.classroomArray = _classroomArray;
        viewController.statusArray = _statusArray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.selectedStatusIndex = _selectedStatusIndex;
        viewController.classLevelId = _classLevelId;
        viewController.classroomId = _classroomId;
        viewController.statusId = _statusId;
                
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
    }
}
@end
