//
//  EXReportStatusDetailModel.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EXReportStatusDetailModel : NSObject

@property (nonatomic) long long iid;
@property (nonatomic) NSString *title;
@property (nonatomic) NSInteger amount;
@property (nonatomic) double percent;
@property (nonatomic) BOOL isButton;

- (void)setIId:(long long)iid;
- (void)setTitle:(NSString *)title;
- (void)setAmount:(NSInteger)amount;
- (void)setPercent:(double)percent;
- (void)setIsButton:(BOOL)isButton;

- (long long)getIId;
- (NSString *)getTitle;
- (NSInteger)getAmount;
- (double)getPercent;
- (BOOL)getIsButton;

@end
