
//  ReportOrderHomeWorkPageMenuViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportOrderHomeWorkPageMenuViewController.h"
#import "ReportOrderHomeWorkViewController.h"
#import "KxMenu.h"
#import "Utils.h"

@interface ReportOrderHomeWorkPageMenuViewController (){
    
    CAPSPageMenu *pageMenu;
    
    UIColor *OrangeColor;
    UIColor *GrayColor;
    
    NSString *sortData;
    
}

@end

@implementation ReportOrderHomeWorkPageMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
    OrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1.0];
    GrayColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
    
    [self setupPagemenu];
    self.sortButton.hidden = YES;
    self.sortButton.enabled = false;
    
    [self.sortButton addTarget:self action:@selector(showRightMenu:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
    
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_HOME",nil,[Utils getLanguage],nil);
}

- (void)setupPagemenu{
    
    self.detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportOrderHomeworkDetailPersonStoryboard"];
    self.detailController.title = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_DETAIL",nil,[Utils getLanguage],nil);
    self.detailController.homeworkID = self.homeworkID;

    self.recieverController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportOrderHomeWorkRecieverStoryboard"];
    self.recieverController.title = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_RECIPIENT",nil,[Utils getLanguage],nil);
    self.recieverController.homeworkID = self.homeworkID;
    self.recieverController.sortData = self.sortData;
    
    NSArray *controllerArray = @[self.detailController, self.recieverController];
    
    NSDictionary *parameters;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(40),
                       CAPSPageMenuOptionMenuHeight: @(50),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:32.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    else{
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(20),
                       CAPSPageMenuOptionMenuHeight: @(40),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:22.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    [self.contentView layoutIfNeeded];
    pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    // For the first time set first tab background to orange and label color to white
    NSArray *menuItems = pageMenu.menuItems;
    MenuItemView *menuItemView = [menuItems objectAtIndex:pageMenu.currentPageIndex];
    menuItemView.backgroundColor = OrangeColor;
    menuItemView.titleLabel.textColor = [UIColor whiteColor];
    
    //Optional delegate
    pageMenu.delegate = self;
    
    [self.contentView addSubview:pageMenu.view];
    
}

#pragma CAPSPageMenuDelegate

- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index{
    
    NSArray *menuItems = pageMenu.menuItems;
    
    int count = 0;
    
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = OrangeColor;
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        
        count++;
    }
    
    if (index == 0) {
        self.sortButton.hidden = YES;
        self.sortButton.enabled = false;
    }
    else{
        self.sortButton.hidden = NO;
        self.sortButton.enabled = true;
    }
    
}


- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
    NSArray *menuItems = pageMenu.menuItems;
    
    int count = 0;
    
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = OrangeColor;
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        
        count++;
    }
    
    if (index == 0) {
        self.sortButton.hidden = YES;
        self.sortButton.enabled = false;
    }
    else{
        self.sortButton.hidden = NO;
        self.sortButton.enabled = true;
        
    }
    
}





- (void)showRightMenu:(UIButton *)sender{
    
    UIImage *allImage = [UIImage imageNamed:@"sort_with_all"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [allImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *allImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *readImage = [UIImage imageNamed:@"sort_with_read"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [readImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *readImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *unreadImage = [UIImage imageNamed:@"sort_with_unread"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [unreadImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *unreadImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    NSArray *menuItems =
    @[
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_ALL",nil,[Utils getLanguage],nil)
                     image:allImage2
                    target:self
                    action:@selector(sortAll:)],
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_READ",nil,[Utils getLanguage],nil)
                     image:readImage2
                    target:self
                    action:@selector(sortRead:)],
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_UNREND",nil,[Utils getLanguage],nil)
                     image:unreadImage2
                    target:self
                    action:@selector(sortUnRead:)],
      ];
    
    //    KxMenuItem *first = menuItems[0];
    //    first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
    //    first.alignment = NSTextAlignmentLeft;
    
    const CGFloat W = self.view.bounds.size.width;
    const CGFloat H = self.view.bounds.size.height;
    
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(W - 16, 18, 40, 40) menuItems:menuItems];
    
}

- (void) sortAll:(id)sender
{
    sortData = nil;
    //    self.recieverController.sortData = sortData;
    
    self.recieverController.sortChangeData = sortData;
    
    NSIndexPath *topPath = [NSIndexPath indexPathForRow:0 inSection:0];
    //[self.recieverController.tableView  scrollToRowAtIndexPath:topPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        
        [self.recieverController getRecieverListWithhomeworkID:self.homeworkID page:1 readStatus:sortData];
        
    });
    
    
    NSLog(@"%@", sortData);
}

- (void) sortRead:(id)sender
{
    sortData = @"read";
    
    self.recieverController.sortChangeData = sortData;
    
    NSIndexPath *topPath = [NSIndexPath indexPathForRow:0 inSection:0];
    //[self.recieverController.tableView  scrollToRowAtIndexPath:topPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        
        [self.recieverController getRecieverListWithhomeworkID:self.homeworkID page:1 readStatus:sortData];
        
    });
    
    NSLog(@"%@", sortData);
    
    //    self.recieverController.sortData = sortData;
}

- (void) sortUnRead:(id)sender
{
    sortData = @"unread";
    self.recieverController.sortChangeData = sortData;
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
    [self.recieverController getRecieverListWithhomeworkID:self.homeworkID page:1 readStatus:sortData];
        
    });
    NSLog(@"%@", sortData);
}
- (IBAction)moveBack:(id)sender {
    
    ReportOrderHomeWorkViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportOrderHomeWorkStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
