//
//  CallTEConfirmScanerBarcode.h
//  JabjaiApp
//
//  Created by toffee on 3/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CallTEConfirmScanerBarcode;
@protocol CallTEConfirmScanerBarcodeDelegate <NSObject>
@optional
- (void)callTEConfirmScanerBarcode:(CallTEConfirmScanerBarcode *)classObj resCode:(NSInteger)resCode success:(BOOL)success;
@end

@interface CallTEConfirmScanerBarcode : NSObject
@property (nonatomic, weak) id<CallTEConfirmScanerBarcodeDelegate> delegate;
- (void)call:(long long)schoolId userId:(long long)userId idCode:(NSString *)idCode;
@end

NS_ASSUME_NONNULL_END
