//
//  JHSelectRoomsTableListDialog.h
//  JabjaiApp
//
//  Created by toffee on 1/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JHSelectRoomTableListViewCell.h"
#import "JHSelectRoomRadioModel.h"


@protocol  JHSelectRoomTableListDialogDelegate

-(void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index;
-(void)onItemSelectClassrooms:(NSString *)requestCode dataClassroomName:(NSArray*)dataClassroomName dataClassroomID:(NSArray*)dataClassroomID;
@end

@interface JHSelectRoomsTableListDialog : UIViewController <UITableViewDataSource, UITableViewDelegate, JHSelectRoomRadioTableViewCellDelegate>
@property (nonatomic, retain) id<JHSelectRoomTableListDialogDelegate> delegate;

@property (nonatomic, strong) NSArray *dataArr;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIView *content;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

- (id)initWithRequestCode:(NSString *)requestCode;
- (void)showDialogInView:(UIView *)targetView dataArr:(NSArray *)dataArr;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
- (IBAction)actionNext:(id)sender;

@end
