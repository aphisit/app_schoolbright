//
//  JHConfirmJobHomeViewController.m
//  JabjaiApp
//
//  Created by toffee on 12/1/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "JHConfirmJobHomeViewController.h"
#import "JHTimeOutAndDetailViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "APIURL.h"
#import "ChangeDate.h"
#import <QBImagePickerController/QBImagePickerController.h>
#import "Utils.h"
#import "AlertDialogConfirm.h"
#import "JHClassLevelViewController.h"
#import "DBAttachment.h"
@interface JHConfirmJobHomeViewController (){
    NSString *jsonString;
    NSInteger homeworkid;
    PHImageRequestOptions *requestOptions;
    NSInteger num;
    AlertDialogConfirm *alertDialog;
    NSString *subjectId;
    NSBundle *myLangBundle;
}

@end
static NSString *cellIdentifier = @"Cell";
@implementation JHConfirmJobHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    subjectId = self.planeId;
    self.daySendHomeWork.text = [NSString stringWithFormat:@"%@ %@ %@",[Utils dateInStringFormat:self.daystart ],NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_TO",nil,myLangBundle,nil),[Utils dateInStringFormat:self.dayend ]] ;
    self.dayNotification.text = [NSString stringWithFormat:@"%@ %@ %@",[Utils dateInStringFormat:self.daynotification],NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_TIME",nil,myLangBundle,nil),self.timenotification];
    self.subjectName.text = self.subJectName;
    self.detail.text = self.homeworkdetail;
    if (self.sendtype == 1) {
        self.sendType.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_SEND_INDIVIDUAL",nil,myLangBundle,nil);
    }else{
        self.sendType.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_SEND_GROUP",nil,myLangBundle,nil);
    }
    if (self.arrayuserName.count != 0) {
        NSString * resultConditions = [self.arrayuserName componentsJoinedByString:@"\n"];
        self.student.text = resultConditions;
    }else{
         NSString * resultConditions = [self.roomName componentsJoinedByString:@"\n"];
        self.student.text = resultConditions;
    }
    [self.collectionview registerNib:[UINib nibWithNibName:NSStringFromClass([JHIconFileCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.collectionview.delegate = self;
    self.collectionview.dataSource = self;
    
    [self.fileCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([JHIconFileCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.fileCollectionView.delegate = self;
    self.fileCollectionView.dataSource = self;
    //hidden collectionview
    if (self.imageHomework.count < 1) {
        self.collectionview.hidden = YES;
    }
    if (self.fileArray.count < 1) {
        self.fileCollectionView.hidden = YES;
    }
    [self setLanguage];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
}

-(void)setLanguage{
    NSBundle *myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
     self.headerSummarizeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_SUMMARIZE",nil,myLangBundle,nil);
    self.headerSubjectLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_SUBJECT",nil,myLangBundle,nil);
    self.headerDiliveryLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_DELIVERY",nil,myLangBundle,nil);
    self.headerRecipientLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_RECIPIENT",nil,myLangBundle,nil);
    self.headerStartDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_SUBMIT_START_DATE",nil,myLangBundle,nil);
    self.headerAlertDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_SUBMIT_ALERT_DATE",nil,myLangBundle,nil);
    self.headerAttachLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_ATTACH_IMAGE",nil,myLangBundle,nil);
    self.headerAttachImageLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_JOBHOME_CHOOSE_FILE",nil,myLangBundle,nil);
      self.headerDescription.text = NSLocalizedStringFromTableInBundle(@"LABEL_JOBHOME_DESCRIPTION",nil,myLangBundle,nil);
  
    [_headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_JOBHOME_SUBMIT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.collectionview) {
        return [self.imageHomework count];
    }else{
        return self.fileArray.count;
    }
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
     JHIconFileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.removeFileBtn.hidden = YES;
    if (collectionView == self.collectionview) {
       
        cell.fileImage.image = [self.imageHomework objectAtIndex:indexPath.row];
        
    }else{
       
        DBAttachment *attachment = self.fileArray[indexPath.row];
        CGFloat scale = [UIScreen mainScreen].scale;
        CGSize scaledThumbnailSize = CGSizeMake( 80.f * scale, 80.f * scale );
        [attachment loadThumbnailImageWithTargetSize:scaledThumbnailSize completion:^(UIImage *resultImage) {
            cell.fileImage.image = resultImage;
        }];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize defaultSize = CGSizeMake((self.collectionview.frame.size.width/3)-10, (self.collectionview.frame.size.height));
    return defaultSize;
}

- (void)doSetParameter{
//    self.homeworkdetail = [self.homeworkdetail stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
//    self.homeworkdetail = [NSString stringWithFormat: @"\"%@\"", self.homeworkdetail];
    self.planeId = [NSString stringWithFormat: @"\"%@\"", self.planeId];
//    //เก็บ id studen
    NSMutableString *studenId = [[NSMutableString alloc] initWithString:@"["];
    for(int i=0; i<self.arrayuserId.count; i++) {
        [studenId appendFormat:@"%@", [self.arrayuserId objectAtIndex:i]];
        if(i+1 != self.arrayuserId.count) {
            [studenId appendString:@","];
        }
    }
    [studenId appendString:@"]"];
    
    //id room
    NSMutableString *classroomId = [[NSMutableString alloc] initWithString:@"["];
    for(int i=0; i<self.levelId.count; i++) {
        [classroomId appendFormat:@"%@", [self.levelId objectAtIndex:i]];
        
        if(i+1 != self.levelId.count) {
            [classroomId appendString:@","];
        }
    }
    [classroomId appendString:@"]"];
    //self.homeworkdetail = [self.homeworkdetail stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
    self.timenotification = [NSString stringWithFormat:@"%@ %@",[Utils datePunctuateStringFormat:self.daynotification],self.timenotification];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    jsonString = [[NSString alloc] initWithFormat:@"{\"schoolid\" : %lld , \"arraylevel\" : %@ , \"arrayuser\" : %@ , \"sendtype\" : %d,\"dayend\" : \"%@\",\"daynotification\" : \"%@\",\"daystart\" : \"%@\",\"planeid\" : %@,\"teacherid\" : %@ }", self.schoolId, classroomId, studenId, self.sendtype, [Utils datePunctuateStringFormat:self.dayend],self.timenotification, [Utils datePunctuateStringFormat:self.daystart], self.planeId, self.teacherid];
    NSLog(@"jsonString = %@",jsonString);
    [self updateSNSendNews:jsonString imageArray:(NSMutableArray*)self.imageHomework];
}

#pragma mark - insert data to server
- (void)updateSNSendNews:(NSString *)jsonString imageArray:(NSMutableArray *)imageArray{
    NSString *URLString = [APIURL getUpdateJobHomePOST];
    NSError* error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    //NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableLeaves error:&error];
    NSMutableDictionary *json = [[NSMutableDictionary alloc] init];
    json = [[NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableLeaves error:&error] mutableCopy];
    [json setObject:self.homeworkdetail forKey:@"homeworkdetail" ];
    
    static NSString *KEY_HOMEWORKID = @"homeworkid";
    static NSString *KEY_SCHOOLID = @"schoolid";
    if(error != nil) {
        NSLog(@"%@", @"Parse JSON data failed");
    }
    else {
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"responseObject = %@", responseObject);
            homeworkid = [[responseObject objectForKey:@"homeworkid"] integerValue];
            NSString *urlString = [APIURL getHomeWorkPOSTURL];
            NSURL *url = [NSURL URLWithString:urlString];
            NSString *schoolID = [[NSString alloc] initWithFormat:@"%lld", (long long)[UserData getSchoolId]];
            NSString *homeworkID = [[NSString alloc] initWithFormat:@"%lld", (long long)homeworkid];
            NSLog(@"num = %d",  homeworkid);
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            [parameters setObject:schoolID forKey:KEY_SCHOOLID];
            [parameters setObject:homeworkID forKey:KEY_HOMEWORKID];
            if(imageArray.count > 0){
                for (UIImage *ima in imageArray) {
                    if (ima != nil) {
                        NSData *adjestIma;
                        if (((unsigned long)[UIImageJPEGRepresentation(ima, 1.0) length]) > 500000) {
                            adjestIma = UIImageJPEGRepresentation(ima, 0.4);
                        }else{
                            adjestIma = UIImageJPEGRepresentation(ima, 1.0);
                        }
                        NSLog(@"image(%ld)byte",(unsigned long)[adjestIma length]);
                        [Utils uploadImageFromURL:url data:adjestIma imageparameterName:@"image" imageFileName:[NSString stringWithFormat:@"%d",num++] mimeType:JPEG parameters:parameters withCompletionHandler:^(id responseObject, NSError *error) {
                        }];
                    }
                }
            }
            if(self.fileArray.count > 0){
                for (DBAttachment *model in self.fileArray) {
                    NSString *path = model.originalFileResource;
                    NSError* error = nil;
                    NSData *fileData = [NSData dataWithContentsOfFile:path options: 0 error: &error];
                    NSArray *typeArray = [model.fileName componentsSeparatedByString:@"."];
                    NSString *type = [typeArray objectAtIndex:typeArray.count-1];
                    [Utils uploadFileFromURL:url data:fileData imageparameterName:[typeArray objectAtIndex:0] imageFileName:[NSString stringWithFormat:@"%d",num++] mimeType:type parameters:parameters withCompletionHandler:^(id responseObject, NSError *error) {
                    }];
                }
            }
            if(error != nil) {
                NSLog(@"error");
            }
            else {
                NSLog(@"response = %@", responseObject);
                [self validateData];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        }];
    }
}

- (void)validateData {
    [self stopIndicator];
    NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_JOBHOME_COMPLETED",nil,myLangBundle,nil);
    [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    if(alertDialog == nil) {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - AlertDialogDelegate
- (void)onAlertDialogClose {
    JHClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHClassLevelStoryboard"];
    if (self.sendtype == 0) {
        viewController.mode = 1;
    }else{
        viewController.mode = 0;
    }
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (IBAction)moveBack:(id)sender {
    JHTimeOutAndDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHTimeOutAndDetailStoryboard"];
    viewController.classLevelArray = _classLevelArray;
    viewController.classRoomArray = _classRoomArray;
    viewController.sujectNameArrray = _sujectNameArrray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.selectedSubjectIndex = _selectedSubjectIndex;
    viewController.sendType = _sendtype;
    viewController.studentId = _arrayuserId;
    viewController.studentName = _arrayuserName;
    viewController.levelId = self.levelId;
    viewController.subjectId = subjectId;
    viewController.subjectName = _subJectName;
    viewController.roomName = _roomName;
    viewController.detail = self.homeworkdetail;
    viewController.alertTime = self.timenotification;
    viewController.alertDay = self.daynotification;
    viewController.startDay = self.daystart;
    viewController.endDay = self.dayend;
    viewController.imagesArray = self.imageHomework;
    viewController.attachmentArray = self.fileArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (IBAction)confirmAction:(id)sender {
    [self showIndicator];
    [self doSetParameter];
}
@end
