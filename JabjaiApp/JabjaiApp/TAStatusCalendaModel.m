//
//  TAStatusCalendaModel.m
//  JabjaiApp
//
//  Created by Mac on 18/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "TAStatusCalendaModel.h"

@implementation TAStatusCalendaModel
@synthesize schoolID = _schoolID;
@synthesize day = _day;

- (void)setSchoolID:(long long)schoolID{
    _schoolID = schoolID;
}
- (void)setDay:(NSString *)day{
    _day = day;
}

- (long long)getSchoolID{
    return _schoolID;
}
- (NSString *)getDay{
    return _day;
}
@end
