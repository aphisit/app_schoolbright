//
//  CallReportInOutAPI.m
//  JabjaiApp
//
//  Created by toffee on 8/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallReportInOutAPI.h"
#import "APIURL.h"
#import "Utils.h"


@interface CallReportInOutAPI () {
    NSInteger connectCounter;
    NSMutableArray <ReportInOutAllDataStudentModel *> *statusArray;
    
}

@end
@implementation CallReportInOutAPI
- (void) call:(long long)schoolId date:(NSString *)date{
    [self getDataReportInOut:schoolId date:date];
}

- (void) getDataReportInOut:(long long)schoolId date:(NSString *)date{
    NSString *URLString = [APIURL getReportInOut:schoolId date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
               [self getDataReportInOut:schoolId date:date];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                   [self getDataReportInOut:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataReportInOut:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                NSInteger status0,status1,status2,status3,status4,status5,status6;
                NSArray* data,*dataStudent,*dataEmployee;
                NSDictionary *statusAllDict;
              
                
                if(statusArray != nil) {
                    [statusArray removeAllObjects];
                    statusArray = nil;
                }
                statusArray = [[NSMutableArray alloc] init];

                NSDictionary* dataDict = [returnedArray objectAtIndex:0];
                
                if(![[dataDict objectForKey:@"student"] isKindOfClass:[NSNull class]]) {
                    dataStudent = [dataDict objectForKey:@"student"];
                }
                else {
                    
                    dataStudent = nil;
                }
                if(![[dataDict objectForKey:@"employees"] isKindOfClass:[NSNull class]]) {
                    dataEmployee = [dataDict objectForKey:@"employees"];
                }
                else {
                    dataEmployee = nil;
                }
                
                for (int i = 0;i < dataDict.count; i++) {
                    
                    if (i == 0) {
                        data = dataStudent; // keep data student
                    }else{
                        data = dataEmployee; // keep data teacher
                    }
                    ReportInOutAllDataStudentModel *model = [[ReportInOutAllDataStudentModel alloc] init];
                    if (data.count > 0) {
                        statusAllDict = [data objectAtIndex:0];
                        status0 = [[statusAllDict objectForKey:@"status_0"] integerValue];
                        status1 = [[statusAllDict objectForKey:@"status_1"] integerValue];
                        status2 = [[statusAllDict objectForKey:@"status_2"] integerValue];
                        status3 = [[statusAllDict objectForKey:@"status_3"] integerValue];
                        status4 = [[statusAllDict objectForKey:@"status_4"] integerValue];
                        status5 = [[statusAllDict objectForKey:@"status_5"] integerValue];
                        status6 = [[statusAllDict objectForKey:@"status_6"] integerValue];
                     
                        [model setStatus_0:status0];
                        [model setStatus_1:status1];
                        [model setStatus_2:status2];
                        [model setStatus_3:status3];
                        [model setStatus_4:status4];
                        [model setStatus_5:status5];
                        [model setStatus_6:status6];
                        
                        NSLog(@"xxx");
                    }else{
                        [model setStatus_0:0];
                        [model setStatus_1:0];
                        [model setStatus_2:0];
                        [model setStatus_3:0];
                        [model setStatus_4:0];
                        [model setStatus_5:0];
                        [model setStatus_6:0];
                    }
                    [statusArray addObject:model];
                }

                connectCounter = 0;
      
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportInOutAPI:statusAllArray:success:)]) {
                    [self.delegate callReportInOutAPI:self statusAllArray:statusArray success:YES];
                }
   
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportInOutAPI:statusAllArray:success:)]) {
                    [self.delegate callReportInOutAPI:self statusAllArray:statusArray success:NO];
                }
            }
        
        }
        
    }];
}

@end
