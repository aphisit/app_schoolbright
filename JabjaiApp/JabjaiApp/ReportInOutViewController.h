//
//  ReportInOutViewController.h
//  JabjaiApp
//
//  Created by mac on 4/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CAPSPageMenu.h"
#import "CalendarDialog.h"
#import "Attend2SchoolReportFilterDialog.h"
#import "SlideMenuController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface ReportInOutViewController : UIViewController <CAPSPageMenuDelegate, Attend2SchoolReportFilterDialogDelegate, SlideMenuControllerDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *headerLable;

- (IBAction)moveBack:(id)sender;
- (IBAction)showFilterDialog:(id)sender;

@end
