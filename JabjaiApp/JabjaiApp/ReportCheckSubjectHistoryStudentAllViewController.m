//
//  ReportCheckSubjectHistoryStudentAllViewController.m
//  JabjaiApp
//
//  Created by toffee on 14/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportCheckSubjectHistoryStudentAllViewController.h"
#import "Utils.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UserData.h"

@interface ReportCheckSubjectHistoryStudentAllViewController (){
    int backgroundInterger;
    NSArray *studentArray;
    NSDate *consideredDate;
    
    NSMutableArray<RPCheckNameSubjectDateTimeModel*> *dateTimeAllArray,*dateTimeOnTimeArray, *dateTimeLateArray, *dateTimeAbsenArray, *dateTimePersonalArray, *dateTimeSickArray, *dateTimeEventArray, *dateTimeUndefinedArray;
}
@property (strong, nonatomic) CallReportCheckSubjectDateTimeStudentAPI *callReportCheckSubjectDateTimeStudentAPI;
@property (strong, nonatomic) RPDataStudentCheckSubjectModel *dateTimeModel;
@end
static NSString *cellIdentifier = @"Cell";
@implementation ReportCheckSubjectHistoryStudentAllViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
    backgroundInterger = 0;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportCheckFlagHistoryStudentAllTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportCheckFlagStatusCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    studentArray = self.studentAllArray;
    
    
    
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_REPORT_SUBJECT",nil,[Utils getLanguage],nil);
    self.showNoStudent.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_NOSTUDENT",nil,[Utils getLanguage],nil);
    self.dateLabel.text = [Utils getThaiDateFormatWithDate:self.consideredDate];
    self.amountLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %d %@",NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ROOM",nil,[Utils getLanguage],nil), self.nameClassRoom, NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_AMOUNT_STUDENT",nil,[Utils getLanguage],nil), self.studentStatusAllArray.count, NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    self.subjectLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_SUBJECT",nil,[Utils getLanguage],nil),self.nameSubject];
}

-(void)doCallReportCheckSubjectDataTime:(NSString*)studentId date:(NSString*)date subjectId:(NSString*)subjectId schoolId:(long long)schoolId{
    [self showIndicator];
    if(self.callReportCheckSubjectDateTimeStudentAPI != nil) {
        self.callReportCheckSubjectDateTimeStudentAPI = nil;
    }
    self.callReportCheckSubjectDateTimeStudentAPI = [[CallReportCheckSubjectDateTimeStudentAPI alloc] init];
    self.callReportCheckSubjectDateTimeStudentAPI.delegate = self;
    [self.callReportCheckSubjectDateTimeStudentAPI call:studentId date:date subjectId:subjectId schoolId:schoolId];
}

- (void) callReportCheckSubjectDateTimeStudentAPI:(CallReportCheckSubjectDateTimeStudentAPI *)classObj dateTimeArray:(NSMutableArray<RPCheckNameSubjectDateTimeModel *> *)dateTimeArray success:(BOOL)success{
    [self stopIndicator];
    if (success) {
        
        dateTimeAllArray = [[NSMutableArray alloc] init];
        dateTimeOnTimeArray = [[NSMutableArray alloc] init];
        dateTimeLateArray = [[NSMutableArray alloc] init];
        dateTimeAbsenArray = [[NSMutableArray alloc] init];
        dateTimePersonalArray = [[NSMutableArray alloc] init];
        dateTimeSickArray = [[NSMutableArray alloc] init];
        dateTimeEventArray = [[NSMutableArray alloc] init];
        dateTimeUndefinedArray = [[NSMutableArray alloc] init];
        dateTimeAllArray = dateTimeArray;
       
        
        for (int i = 0; i < dateTimeArray.count; i++) {
            RPCheckNameSubjectDateTimeModel *model = [[RPCheckNameSubjectDateTimeModel alloc] init];
            model = [dateTimeArray objectAtIndex:i];
            
            if ([[model getStatus] isEqualToString: @"0"]) {
                [dateTimeOnTimeArray addObject:model];
                
            }
            else if ([[model getStatus] isEqualToString: @"1"]){
                [dateTimeLateArray addObject:model];
                
            }
            else if ([[model getStatus] isEqualToString: @"3"]){
                [dateTimeAbsenArray addObject:model];
                
            }
            else if ([[model getStatus] isEqualToString: @"10"]){
                [dateTimePersonalArray addObject:model];
                
            }
            else if ([[model getStatus] isEqualToString: @"11"]){
                [dateTimeSickArray addObject:model];
                
            }
            else if ([[model getStatus] isEqualToString: @"12"]){
                [dateTimeEventArray addObject:model];
                
            }else{
                [dateTimeUndefinedArray addObject:model];
                
            }
        }
    }
    
        ReportCheckSubjectDateTimeOfStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportCheckSubjectDateTimeOfStudentStoryboard"];
        viewController.studentStatusOnTimeArray = self.studentStatusOnTimeArray;
        viewController.studentStatusLateArray = self.studentStatusLateArray;
        viewController.studentStatusPersonalArray = self.studentStatusPersonalArray;
        viewController.studentStatusSickArray = self.studentStatusSickArray;
        viewController.studentStatusEventArray = self.studentStatusEventArray;
        viewController.studentStatusAbsenArray = self.studentStatusAbsenArray;
        viewController.studentStatusAllArray = self.studentStatusAllArray;
        viewController.studentStatusUndefinedArray = self.studentStatusUndefinedArray;
        viewController.studentAllArray = self.studentAllArray;
        viewController.nameClassRoom = self.nameClassRoom;
        viewController.nameSubject = self.nameSubject;
        viewController.selectedClassroomId = self.selectedClassroomId;
        viewController.consideredDate = self.consideredDate;
        viewController.selectedSubjectId = self.selectedSubjectId;
        viewController.studentId = [_dateTimeModel getStudentId];
        viewController.studentName = [_dateTimeModel getStudentName];
        viewController.studentPic = [_dateTimeModel getStudentPic];
    
        viewController.dateTimeAllArray = dateTimeAllArray;
        viewController.dateTimeOnTimeArray = dateTimeOnTimeArray;
        viewController.dateTimeLateArray = dateTimeLateArray;
        viewController.dateTimeAbsenArray = dateTimeAbsenArray;
        viewController.dateTimeSickArray = dateTimeSickArray;
        viewController.dateTimePersonalArray = dateTimePersonalArray;
        viewController.dateTimeEventArray = dateTimeEventArray;
        viewController.dateTimeUndefinedArray = dateTimeUndefinedArray;
    
        [self.slideMenuController changeMainViewController:viewController close:YES];
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 8;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ReportCheckFlagStatusCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ALL",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.studentStatusAllArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_allwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_allgray"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor grayColor];
            cell.amountLabel.textColor = [UIColor grayColor];
            
        }
        
    }
    else if (indexPath.row == 1) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ONTIME",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.studentStatusOnTimeArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockgreen"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:0.24 green:0.79 blue:0.53 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:0.24 green:0.79 blue:0.53 alpha:1.0];
        }
    }
    else if (indexPath.row == 2) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_LATE",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.studentStatusLateArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockyellow"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:1.00 green:0.66 blue:0.15 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:1.00 green:0.66 blue:0.15 alpha:1.0];
        }
    }
    else if (indexPath.row == 3) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ABSENCE",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.studentStatusAbsenArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockred"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.39 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.39 alpha:1.0];
        }
    }
    else if (indexPath.row == 4) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_SICK",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.studentStatusSickArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockviolet"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:0.88 green:0.40 blue:1.00 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:0.88 green:0.40 blue:1.00 alpha:1.0];
        }
    }
    else if (indexPath.row == 5) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PERSONAL",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.studentStatusPersonalArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockpurple"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:0.60 green:0.00 blue:0.75 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:0.60 green:0.00 blue:0.75 alpha:1.0];
        }
    }
    else if (indexPath.row == 6) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_EVENT",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.studentStatusEventArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockblue"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:0.00 green:0.43 blue:0.82 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:0.00 green:0.43 blue:0.82 alpha:1.0];
        }
    }else{
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_UNDEFINED",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.studentStatusUndefinedArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockblack"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:0.36 green:0.36 blue:0.36 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:0.36 green:0.36 blue:0.36 alpha:1.0];
        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    backgroundInterger = indexPath.row;
    
    if (backgroundInterger == 0) {
        studentArray = self.studentAllArray;
    }else if (backgroundInterger == 1){
        studentArray = self.studentStatusOnTimeArray;
    }else if (backgroundInterger == 2){
        studentArray = self.studentStatusLateArray;
    }else if (backgroundInterger == 3){
        studentArray = _studentStatusAbsenArray;
    }else if (backgroundInterger == 4){
        studentArray = self.studentStatusSickArray;
    }else if (backgroundInterger == 5){
        studentArray = self.studentStatusPersonalArray;
    }else if (backgroundInterger == 6){
        studentArray = self.studentStatusEventArray;
    }else if (backgroundInterger == 7){
        studentArray = self.studentStatusUndefinedArray;
    }
    
    [self.collectionView reloadData];
    [self.tableView reloadData];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (studentArray.count == 0) {
        self.showNoStudent.hidden = NO;
    }else{
        self.showNoStudent.hidden = YES;
    }
    return studentArray.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ReportCheckFlagHistoryStudentAllTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    RPDataStudentCheckSubjectModel *model  = [studentArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.nameLabel.text = [model getStudentName];
    cell.runNumberLabel.text = [NSString stringWithFormat:@"%d.",indexPath.row+1];
    if ([model getStudentPic] == NULL || [[model getStudentPic] isEqual:@""]) {
        cell.userImage.image = [UIImage imageNamed:@"ic_user_info"];
    }
    else{
        [cell.userImage sd_setImageWithURL:[NSURL URLWithString:[model getStudentPic]]];
    }
    
    cell.userImage.layer.cornerRadius = cell.userImage.frame.size.height /2;
    cell.userImage.layer.masksToBounds = YES;
    cell.userImage.layer.borderWidth = 0;
    
   // [cell updateStatus:[model getStatus]];
    
    if ([model getStudentState] == 0) {
        [cell updateStatus:[model getStatus]];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.0];
        cell.userInteractionEnabled = YES;
    }
    else if ([model getStudentState] == 1){
        [cell updateStatus:21];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    else if ([model getStudentState] == 2 ){//Resing
        [cell updateStatus:22];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    else if ([model getStudentState] == 3 ){//Suspended
        [cell updateStatus:23];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    else if ([model getStudentState] == 4 ){//Graduate
        [cell updateStatus:24];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    else if ([model getStudentState] == 5 ){//Lost contract
        [cell updateStatus:25];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    else if ([model getStudentState] == 6 ){//Retire
        [cell updateStatus:26];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _dateTimeModel = studentArray[indexPath.row];
    NSArray* dateArray =  [[Utils datePunctuateStringFormat:_consideredDate] componentsSeparatedByString:@"/"];
    NSString* date = [NSString stringWithFormat:@"%@/%@/%@",dateArray[0],dateArray[1],dateArray[2]];
    [self doCallReportCheckSubjectDataTime:[_dateTimeModel getStudentId] date:date subjectId:self.selectedSubjectId schoolId:[UserData getSchoolId]];
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)actionMoveBack:(id)sender {
    ReportCheckSubjectCalendarViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportCheckSubjectCalendarStoryboard"];
    viewController.nameClassRoom = self.nameClassRoom;
    viewController.selectedClassroomId = self.selectedClassroomId;
    viewController.nameSubject = self.nameSubject;
    viewController.selectedSubjectId = self.selectedSubjectId;
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}

@end
