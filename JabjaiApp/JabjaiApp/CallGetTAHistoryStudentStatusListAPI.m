//
//  CallGetTAHistoryStudentStatusListAPI.m
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetTAHistoryStudentStatusListAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"
#import "UserData.h"

@interface CallGetTAHistoryStudentStatusListAPI () {
    NSInteger connectCounter;
    NSMutableArray<RPDataStudentCheckSubjectModel *> *studentStatusModelArray;
}

@end
@implementation CallGetTAHistoryStudentStatusListAPI

- (instancetype)init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId date:(NSDate *)date {
    [self getDataFromServer:schoolId classroomId:classroomId subjectId:subjectId date:date];
}

- (void)getDataFromServer:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId date:(NSDate *)date {
    
    NSString *urlString = [APIURL getTAHistoryStudentStatusListWithSchoolId:schoolId classroomId:classroomId subjectId:subjectId date:date];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataFromServer:schoolId classroomId:classroomId subjectId:subjectId date:date];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId classroomId:classroomId subjectId:subjectId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId classroomId:classroomId subjectId:subjectId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {

                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(studentStatusModelArray != nil) {
                    [studentStatusModelArray removeAllObjects];
                    studentStatusModelArray = nil;
                }
                
                studentStatusModelArray = [[NSMutableArray alloc] init];
                
                if(returnedArray.count > 0) {
                    NSDictionary *dict = [returnedArray objectAtIndex:0];
                    NSArray *dataArray = [dict objectForKey:@"ScheduleScan4Level2"];
                    
                    if(dataArray != nil) {
                        for(int i=0; i<dataArray.count; i++) {
                            NSDictionary *dataDict = [dataArray objectAtIndex:i];
                            NSMutableString *studentName, *studentPic;
                            NSInteger studentState = 0;
                            
                            if(![[dataDict objectForKey:@"Student_State"] isKindOfClass:[NSNull class]]) {
                                studentState = [ [dataDict objectForKey:@"Student_State"]integerValue];
                            }
                            
                            if ([[UserData getChangLanguage] isEqualToString:@"th"]) {
                                if(![[dataDict objectForKey:@"studentname"] isKindOfClass:[NSNull class]]) {
                                    studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentname"]];
                                }
                                else {
                                    studentName = [[NSMutableString alloc] initWithString:@""];
                                }
                            }else{
                                if(![[dataDict objectForKey:@"studentnameEN"] isKindOfClass:[NSNull class]]) {
                                    studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentnameEN"]];
                                    if ([studentName isEqualToString:@" "]) {
                                        studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentname"]];
                                    }
                                }
                                else {
                                    studentName = [[NSMutableString alloc] initWithString:@""];
                                }
                                
                            }
                            
                            if(![[dataDict objectForKey:@"pic"] isKindOfClass:[NSNull class]]) {
                                studentPic = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"pic"]];
                            }
                            else {
                                studentPic = [[NSMutableString alloc] initWithString:@""];
                            }
                            
                           
                            
                            long long studentId = [[dataDict objectForKey:@"studentid"] longLongValue];
                            NSString *status = [dataDict objectForKey:@"status"] ;
                            
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) studentName);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) studentPic);
                            
                            RPDataStudentCheckSubjectModel *model = [[RPDataStudentCheckSubjectModel alloc] init];
                            [model setStudentId:[NSString stringWithFormat:@"%lld",studentId]];
                            [model setStudentName:studentName];
                            [model setStatus:[status integerValue]];
                            [model setStudentPic:studentPic];
                            [model setStudentState:studentState];
                            [studentStatusModelArray addObject:model];
                        }
                    }
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTAHistoryStudentStatusListAPI:data:success:)]) {
                    [self.delegate callGetTAHistoryStudentStatusListAPI:self data:studentStatusModelArray success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTAHistoryStudentStatusListAPI:data:success:)]) {
                [self.delegate callGetTAHistoryStudentStatusListAPI:self data:nil success:NO];
            }
        }
    }];
}
@end
