//
//  JHReadImageViewController.h
//  JabjaiApp
//
//  Created by Mac on 3/8/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "AlertDialogConfirm.h"
#import <SDWebImage/UIImageView+WebCache.h>
NS_ASSUME_NONNULL_BEGIN
@class JHReadImageViewController;

@protocol JHReadImageViewControllerDelegate <NSObject>
@end

@interface JHReadImageViewController : UIViewController <AlertDialogConfirmDelegate>
-(void)showPagesReadImageInView:(UIView *)targetView ImageArray:(NSArray*)imageArray;
@property (nonatomic, retain) id<JHReadImageViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *hideLoadView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *saveImageBtn;

- (IBAction)closeAction:(id)sender;
- (IBAction)saveFileAction:(id)sender;
@end

NS_ASSUME_NONNULL_END
