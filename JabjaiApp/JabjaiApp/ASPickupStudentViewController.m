//
//  ASPickupStudentViewController.m
//  JabjaiApp
//
//  Created by Mac on 4/2/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import "ASPickupStudentViewController.h"
#import "Utils.h"
#import "UserData.h"
//#import <SDWebImage/UIImageView+WebCache.h>
//#import <SDWebImage/UIImage+GIF.h>

#import <GoogleMaps/GoogleMaps.h>
@interface ASPickupStudentViewController (){
    CAGradientLayer  *submitGradient;
    UIButton *pickupButton;
}
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CallASGetLocationOfSchoolAPI *callASGetLocationOfSchoolAPI;
@property (nonatomic,strong) CallASSubmitRequestGetStudentAPI *callASSubmitRequestGetStudentAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) AlertDialogConfirm *successDialog;
@property (nonatomic, strong) ASCloseFuturePickupDialog *aSCloseFuturePickupDialog;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;

@end

@implementation ASPickupStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"xxx = %@",tokenStr);
    [self callGetStatusServer];
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACCEPT_STU",nil,[Utils getLanguage],nil);
    /////////
    self.conView.backgroundColor = [UIColor clearColor];
    self.conView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.conView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.conView.layer.shadowOpacity = 0.8;
    self.conView.layer.shadowRadius = 3.0;
    [self.conView layoutIfNeeded];
    pickupButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [pickupButton addTarget:self
               action:@selector(pickupStudentAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [pickupButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_ACCEPT_STU",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    [pickupButton.titleLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:35.0]];
    pickupButton.layer.masksToBounds = YES;
    pickupButton.layer.cornerRadius = 10;
    pickupButton.backgroundColor = [UIColor greenColor];
    pickupButton.frame = self.conView.bounds;
    pickupButton.backgroundColor = [UIColor colorWithRed: 0.62 green: 0.62 blue: 0.62 alpha: 1.00];
    [self.conView addSubview:pickupButton];
    
    submitGradient = [Utils getGradientColorStatus:@"green"];
    submitGradient.frame = self.conView.bounds;
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
        [_locationManager startUpdatingLocation];
    self.playFileGifImg.animationImages = [[NSArray alloc]initWithObjects:[UIImage imageNamed:@"animation SMALL-1"],
                                           [UIImage imageNamed:@"animation SMALL-2"],
                                           [UIImage imageNamed:@"animation SMALL-3"],
                                           [UIImage imageNamed:@"animation SMALL-4"],
                                           nil];
    [self.playFileGifImg setAnimationRepeatCount:0];
    self.playFileGifImg.animationDuration = 2;
    [self.playFileGifImg startAnimating];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        [self getLocationSchool:[UserData getSchoolId] studentID:[UserData getUserID]];
        NSLog(@"");
    }
    else if (status == kCLAuthorizationStatusDenied) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"App Permission Denied"
                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
            CLLocationManager *locationManager = [[CLLocationManager alloc] init];
            if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        NSLog(@"denied"); // denied
    }
}

#pragma mark - CallASGetLocationOfSchoolAPI
- (void) getLocationSchool:(long long)schoolID studentID:(long long)studentID{
    [self showIndicator];
    if(self.callASGetLocationOfSchoolAPI != nil) {
        self.callASGetLocationOfSchoolAPI = nil;
    }
    self.callASGetLocationOfSchoolAPI = [[CallASGetLocationOfSchoolAPI alloc] init];
    self.callASGetLocationOfSchoolAPI.delegate = self;
    [self.callASGetLocationOfSchoolAPI call:schoolID studentID:studentID];
}
- (void)callASGetLocationOfSchoolAPI:(CallASGetLocationOfSchoolAPI *)classObj data:(ACLocationModel *)dada success:(BOOL)success{
    [self stopIndicator];

    CAGradientLayer *layerToRemovePickupButton;
    for (CALayer *layer in pickupButton.layer.sublayers) {
        if ([layer isKindOfClass:[CAGradientLayer class]]) {
            layerToRemovePickupButton = (CAGradientLayer *)layer;
        }
    }
    [layerToRemovePickupButton removeFromSuperlayer];
    
    if (dada != nil && success) {
        [pickupButton layoutIfNeeded];
        if (dada.statusIsActive == 1) {// permission
            self.refreshBtn.enabled = YES;
            
            if (dada.statusLacation == 1) {// location have = 1 , not have = 0
                CLLocation *locationSchool = [[CLLocation alloc] initWithLatitude:[dada latitude] longitude:[dada longitude]];
                int radius = [[self.locationManager location] distanceFromLocation:locationSchool];
                float kilomate = radius / 1000.0;
                self.kilomateLabel.text = [NSString stringWithFormat:@"%.2f km",kilomate];
                
                if (dada.statusAccept == 0) {// submit yes = 1 , no = 0
                    CLLocation *location = [self.locationManager location];
                    CLLocationCoordinate2D coordinate = [location coordinate];
                    if (coordinate.latitude == 0 || coordinate.longitude == 0) {
                        pickupButton.enabled = NO;
                        self.refreshBtn.enabled = NO;
                        pickupButton.backgroundColor = [UIColor colorWithRed: 0.62 green: 0.62 blue: 0.62 alpha: 1.00];
                        
                    }else{
                        if (kilomate <= 1.00) {
                            pickupButton.enabled = YES;
                            self.refreshBtn.enabled = YES;
                            [self->pickupButton.layer insertSublayer:submitGradient atIndex:0];
                            [pickupButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_ACCEPT_STU",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
                        }else{
                            pickupButton.enabled = NO;
                            self.refreshBtn.enabled = NO;
                           
                            [self showAlertASCloseFuturePickupDialog:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_LIMIT_DISTANCE",nil,[Utils getLanguage],nil)];
                            pickupButton.backgroundColor = [UIColor colorWithRed: 0.62 green: 0.62 blue: 0.62 alpha: 1.00];
                            
                        }
                    }
                }else{
                    pickupButton.enabled = NO;
                    self.refreshBtn.enabled = NO;
                    pickupButton.backgroundColor = [UIColor colorWithRed: 0.62 green: 0.62 blue: 0.62 alpha: 1.00];
                    [pickupButton setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_PICKUP_SUBMIT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
                    [self showAlertDialogWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_PICKUP_SUBMIT",nil,[Utils getLanguage],nil)];
                    
                }
            }else{
                pickupButton.enabled = NO;
                self.refreshBtn.enabled = NO;
                [self showAlertASCloseFuturePickupDialog:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_LOCATION_HAS_NOT",nil,[Utils getLanguage],nil)];
                pickupButton.backgroundColor = [UIColor colorWithRed: 0.62 green: 0.62 blue: 0.62 alpha: 1.00];
                
            }
        }else{
            pickupButton.enabled = NO;
            self.refreshBtn.enabled = NO;
            [self showAlertASCloseFuturePickupDialog:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_NOT_SUBPORT",nil,[Utils getLanguage],nil)];
            pickupButton.backgroundColor = [UIColor colorWithRed: 0.62 green: 0.62 blue: 0.62 alpha: 1.00];
            
        }
        
      

    }else{
        pickupButton.enabled = NO;
        self.refreshBtn.enabled = NO;
        pickupButton.backgroundColor = [UIColor colorWithRed: 0.62 green: 0.62 blue: 0.62 alpha: 1.00];
    }
}
#pragma mark - CallASSubmitRequestGetStudentAPI
-(void)getStatusSubmitGetStudent:(long long)schoolID studentID:(long long)studentID{
    [self showIndicator];
    if(self.callASSubmitRequestGetStudentAPI != nil) {
        self.callASSubmitRequestGetStudentAPI = nil;
    }
    self.callASSubmitRequestGetStudentAPI = [[CallASSubmitRequestGetStudentAPI alloc] init];
    self.callASSubmitRequestGetStudentAPI.delegate = self;
    [self.callASSubmitRequestGetStudentAPI call:schoolID studentID:studentID];
}

- (void)callASSubmitRequestGetStudentAPI:(CallASSubmitRequestGetStudentAPI *)classObj status:(NSString *)status success:(BOOL)success{
    [self stopIndicator];
    CAGradientLayer *layerToRemovePickupButton;
    for (CALayer *layer in pickupButton.layer.sublayers) {
        if ([layer isKindOfClass:[CAGradientLayer class]]) {
            layerToRemovePickupButton = (CAGradientLayer *)layer;
        }
    }
    [layerToRemovePickupButton removeFromSuperlayer];
    
    if (status != nil && status) {
        if ([[status lowercaseString]isEqual:@"ok"]) {
            pickupButton.enabled = NO;
            self.refreshBtn.enabled = NO;
            [pickupButton setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_PICKUP_SUBMIT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
            [self showAlertDialogWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_ACCEPT_STU_PICKUP_SUCCESS",nil,[Utils getLanguage],nil)];
            pickupButton.backgroundColor = [UIColor colorWithRed: 0.62 green: 0.62 blue: 0.62 alpha: 1.00];
        }
    }else{
        pickupButton.enabled = NO;
        pickupButton.backgroundColor = [UIColor colorWithRed: 0.62 green: 0.62 blue: 0.62 alpha: 1.00];
        
    }
}

#pragma mark - AlertDialogConfirm
- (void)showAlertDialogWithMessage:(NSString *)message {
    if(self.successDialog != nil) {
        if([self.successDialog isDialogShowing]) {
            [self.successDialog dismissDialog];
        }
    }
    else {
        self.successDialog = [[AlertDialogConfirm alloc] init];
    }
    [self.successDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - ASCloseFuturePickupDialog
- (void)showAlertASCloseFuturePickupDialog:(NSString *)message {
    if(self.aSCloseFuturePickupDialog != nil && [self.aSCloseFuturePickupDialog isDialogShowing]) {
        [self.aSCloseFuturePickupDialog dismissDialog];
    }
    else {
        self.aSCloseFuturePickupDialog = [[ASCloseFuturePickupDialog alloc] init];
        self.aSCloseFuturePickupDialog.delegate = self;
    }
    [self.aSCloseFuturePickupDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        pickupButton.enabled = NO;
        [self.indicator startAnimating];
        
    }
}
- (void)stopIndicator {
    pickupButton.enabled = YES;
    [self.indicator stopAnimating];
    
}

- (IBAction)refreshAction:(id)sender {
    [self getLocationSchool:[UserData getSchoolId] studentID:[UserData getUserID]];
}

-(void) pickupStudentAction:(UIButton*)sender
 {
     [self getStatusSubmitGetStudent:[UserData getSchoolId] studentID:[UserData getUserID]];
 }

- (IBAction)openDrawer:(id)sender {
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
}
@end
