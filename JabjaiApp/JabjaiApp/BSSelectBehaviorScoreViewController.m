//
//  BSSelectBehaviorScoreViewController.m
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSSelectBehaviorScoreViewController.h"
#import "BSSelectStudentsViewController.h"
#import "BSSummaryViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "Constant.h"
#import "BSAlertStatusDialog.h"
#import "Utils.h"

@interface BSSelectBehaviorScoreViewController () {
    
    UIColor *greenBGColor;
    UIColor *redBGColor;
    
    // The dialog
    AlertDialog *alertDialog;
    BSAlertStatusDialog *alertDialogStatus;
    NSBundle *myLangBundle ;
    CAGradientLayer *gradient;
    
    NSMutableArray *imageAlbumArray;
}

@property (strong, nonatomic) CallGetAddBehaviorScoreListAPI *callGetAddBehaviorScoreListAPI;
@property (strong, nonatomic) CallGetReduceBehaviorScoreListAPI *callGetReduceBehaviorScoreListAPI;
@property (strong, nonatomic) PhotoAlbumLibary *photoAlbumLibary;

@end

static NSString *cellIdentifier = @"Cell";

@implementation BSSelectBehaviorScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    
    // Do any additional setup after loading the view.
   
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    [self.imageCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([BSImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.imageCollectionView.dataSource = self;
    self.imageCollectionView.delegate = self;
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSBehaviorScoreRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    greenBGColor = [UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0];
    redBGColor = [UIColor colorWithRed:220/255.0 green:43/255.0 blue:69/255.0 alpha:1.0];
    
   
    
    if(self.behaviorScoreArray == nil) {
        
        if(_mode == BS_MODE_ADD) {
            [self getAddBehaviorScoreData];
        }
        else {
            [self getReduceBehaviorScoreData];
        }
    }
    
}
- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
    [self setLanguage];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    self.openAlbumBnt.layer.cornerRadius = 5;
    [self.openAlbumBnt.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.openAlbumBnt.layer setShadowOpacity:0.3];
    [self.openAlbumBnt.layer setShadowRadius:3.0];
    [self.openAlbumBnt.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    //self.openAlbumBnt.clipsToBounds = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)setLanguage{
    [self.nextButton layoutIfNeeded];
    if(_mode == BS_MODE_ADD) {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_ADD",nil,myLangBundle,nil);
        gradient = [Utils getGradientColorStatus:@"green"];
        gradient.frame = self.nextButton.bounds;
        [self.nextButton.layer insertSublayer:gradient atIndex:0];
    }
    else {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REDUCE",nil,myLangBundle,nil);
        gradient = [Utils getGradientColorStatus:@"red"];
        gradient.frame = self.nextButton.bounds;
        [self.nextButton.layer insertSublayer:gradient atIndex:0];
    }
     [self.nextButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_BEHAVIOR_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
    [self.openAlbumBnt setTitle:NSLocalizedStringFromTableInBundle(@"BTN_BEHAVIOR_CHOOSE_IMG",nil,myLangBundle,nil) forState:UIControlStateNormal];
    self.openAlbumBnt.titleLabel.numberOfLines = 0;
    [self.openAlbumBnt.titleLabel setTextAlignment:UITextAlignmentCenter];
    
    
    
}

#pragma mark - UICollectionViewDataSource
-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return  imageAlbumArray.count;
}
- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    BSImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.image.image = [imageAlbumArray objectAtIndex:indexPath.row];
    cell.deleteBtn.tag = indexPath.row;
    cell.collectionView = collectionView;
    cell.delegate = self;
    return cell;
}
#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize defaultSize = CGSizeMake((self.imageCollectionView.frame.size.width/3)-10, (self.imageCollectionView.frame.size.height));
    return defaultSize;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.behaviorScoreArray != nil) {
        return self.behaviorScoreArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BSBehaviorScoreRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    BehaviorScoreModel *model = [self.behaviorScoreArray objectAtIndex:indexPath.row];
    cell.delegate = self;
    cell.index = indexPath.row;
    cell.titleLabel.text = [model getBehaviorScoreName];
    
    if([model getBehaviorScoreType] == 0) { // type add
        cell.scoreLabel.text = [NSString stringWithFormat:@"+%i", (int)[model getScore]];
    }
    else {
        cell.scoreLabel.text = [NSString stringWithFormat:@"-%i", (int)[model getScore]];
    }
    
    if([model isSelected]) {
        if(self.mode == BS_MODE_ADD) {
            [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
        }
        else {
            [cell setRadioButtonSelectedWithType:RADIO_TYPE_RED];
        }
    }
    else {
        [cell setRadioButtonClearSelected];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 100; /* Device is iPad */
    }else{
        return 55;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"index = %d",indexPath.row);
     BehaviorScoreModel *model = [self.behaviorScoreArray objectAtIndex:indexPath.row];
    NSString *alertMessage = [model getBehaviorScoreName];
    [self showAlertStatus:alertMessage];
}

#pragma mark - BSBehaviorScoreTableViewCellDelegate

- (void)onPressRadioButton:(BSBehaviorScoreRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index {
    
    BOOL isSelected = [[self.behaviorScoreArray objectAtIndex:index] isSelected];
    [[self.behaviorScoreArray objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - API Caller

- (void)getAddBehaviorScoreData {
    
    if(self.callGetAddBehaviorScoreListAPI != nil) {
        self.callGetAddBehaviorScoreListAPI = nil;
    }
    
    self.callGetAddBehaviorScoreListAPI = [[CallGetAddBehaviorScoreListAPI alloc] init];
    self.callGetAddBehaviorScoreListAPI.delegate = self;
    [self.callGetAddBehaviorScoreListAPI call:[UserData getSchoolId]];
}

- (void)getReduceBehaviorScoreData {
    
    if(self.callGetReduceBehaviorScoreListAPI != nil) {
        self.callGetReduceBehaviorScoreListAPI = nil;
    }
    
    self.callGetReduceBehaviorScoreListAPI = [[CallGetReduceBehaviorScoreListAPI alloc] init];
    self.callGetReduceBehaviorScoreListAPI.delegate = self;
    [self.callGetReduceBehaviorScoreListAPI call:[UserData getSchoolId]];
}

#pragma mark - API Delegate
- (void)callGetAddBehaviorScoreListAPI:(CallGetAddBehaviorScoreListAPI *)classObj data:(NSArray<BehaviorScoreModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _behaviorScoreArray = data;
        
        [self.tableView reloadData];
    }
}

- (void)callGetReduceBehaviorScoreListAPI:(CallGetReduceBehaviorScoreListAPI *)classObj data:(NSArray<BehaviorScoreModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _behaviorScoreArray = data;
        
        [self.tableView reloadData];
    }
}

#pragma mark - Dialog

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil) {
        if([alertDialog isDialogShowing]) {
            [alertDialog dismissDialog];
        }
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showAlertStatus:(NSString *)message {
    
    if(alertDialogStatus != nil) {
        if([alertDialogStatus isDialogShowing]) {
            [alertDialogStatus dismissDialog];
        }
    }
    else {
        alertDialogStatus = [[BSAlertStatusDialog alloc] init];
        alertDialogStatus.delegate = self;
    }
    [alertDialogStatus showDialogInView:self.view title:@"แจ้งเตือน" message:message mode:_mode];
}

#pragma mark - Utility

- (BOOL)validateData {
    if(_behaviorScoreArray != nil) {
        for(BehaviorScoreModel *model in _behaviorScoreArray) {
            
            if([model isSelected]) {
                return YES;
            }
        }
    }
    return NO;
}

- (void)gotoBSSummaryViewController {
    BSSummaryViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSummaryStoryboard"];
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    viewController.status = self.status;
    viewController.selectedStudentArray = _selectedStudentArray;
    viewController.behaviorScoreArray = _behaviorScoreArray;
    viewController.imageArray = imageAlbumArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

#pragma mark - BSImageCollectionViewCellDelegate
-(void)removeImage:(NSInteger)index collectionView:(UICollectionView *)collectiobView{
    NSLog(@"xxxx");
    [imageAlbumArray removeObjectAtIndex:index];
    [self.imageCollectionView reloadData];
}

#pragma mark - PhotoAlbumLibaryDelegate
- (void) replyPhotoAlbum:(PhotoAlbumLibary *)classObj imageArray:(NSMutableArray *)imageArray{
    imageAlbumArray = imageArray;
    [self.imageCollectionView reloadData];
}

#pragma mark - Action functions
- (IBAction)selectPhotoAlbum:(id)sender {
//    self.photoAlbumLibary = [[PhotoAlbumLibary alloc]init];
//    self.photoAlbumLibary.delegate = self;
//    [self.photoAlbumLibary showPhotoAlbumView:self.view];
    //NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_LEAST_DETAIL",nil,myLangBundle,nil);
    NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_CHOOSE_IMAGE",nil,myLangBundle,nil);
    [self showAlertDialogWithMessage:alertMessage];
}

- (IBAction)actionNext:(id)sender {
    if([self validateData]) {
        [self gotoBSSummaryViewController];
    }
    else {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_LEAST_DETAIL",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
    }
}

- (IBAction)moveBack:(id)sender {
    
    if (self.status == 0) {
        
        BSScanerBehavioScorerViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSScanerBehavioScorerStoryboard"];
        viewController.classLevelArray = _classLevelArray;
        viewController.classroomArray = _classroomArray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.classLevelId = _classLevelId;
        viewController.classroomId = _classroomId;
        viewController.mode = _mode;
        viewController.status = self.status;
        viewController.selectedStudentArray = _selectedStudentArray;
        viewController.behaviorScoreArray = _behaviorScoreArray;
         [self.slideMenuController changeMainViewController:viewController close:YES];
        
        
       
    }else if(self.status == 1){
        BSPadNumberBehavioViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSPadNumberBehavioStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
        viewController.classLevelArray = _classLevelArray;
            viewController.classroomArray = _classroomArray;
            viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
            viewController.selectedClassroomIndex = _selectedClassroomIndex;
            viewController.classLevelId = _classLevelId;
            viewController.classroomId = _classroomId;
            viewController.mode = _mode;
            viewController.status = self.status;
            viewController.selectedStudentArray = _selectedStudentArray;
            viewController.behaviorScoreArray = _behaviorScoreArray;
    }
    else if(self.status == 2){
        BSSelectStudentsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectStudentsStoryboard"];
                        viewController.classLevelArray = _classLevelArray;
                        viewController.classroomArray = _classroomArray;
                        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
                        viewController.selectedClassroomIndex = _selectedClassroomIndex;
                        viewController.classLevelId = _classLevelId;
                        viewController.classroomId = _classroomId;
                        viewController.mode = _mode;
                        viewController.status = self.status;
                        viewController.selectedStudentArray = _selectedStudentArray;
                        viewController.behaviorScoreArray = _behaviorScoreArray;
                        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
   
}
@end
