//
//  CallTEGetStudentInClassAPI.m
//  JabjaiApp
//
//  Created by toffee on 10/30/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallTEGetStudentInClassAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@implementation CallTEGetStudentInClassAPI{
    NSInteger connectCounter;
    NSMutableArray<TAStudentStatusModel *> *studentStatusArray;
}
- (void)call:(long long)schoolId classroomId:(long long)classroomId teacherId:(NSInteger)teacherId{
    connectCounter = 0;
    [self getStudentInClass:schoolId classroomId:classroomId teacherId:teacherId];
}

#pragma mark - Get API Data
- (void)getStudentInClass:(long long)schoolId classroomId:(long long)classroomId teacherId:(NSInteger)teacherId{
    
    NSString *URLString = [APIURL getTakeEventStudentInClassroomWithSchoolId:schoolId classroomId:classroomId teacherId:teacherId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
               // [self getStudentInClass:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId];
            }
            else {
                
                isFail = YES;
                self->connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                   // [self getStudentInClass:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                   // [self getStudentInClass:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                self->connectCounter = 0;
                
                if(self->studentStatusArray != nil) {
                    [self->studentStatusArray removeAllObjects];
                    self->studentStatusArray = nil;
                }
                
                self->studentStatusArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long userId = [[dataDict objectForKey:@"UserId"] longLongValue];
                    BOOL authorized = [[dataDict objectForKey:@"authorized"] boolValue];
                    BOOL statusCheck = [[dataDict objectForKey:@"statusCheck"] boolValue];
                    
                    NSMutableString *studentId, *studentName, *teacherName , *studentPic ;
                    long long teacherId = 0;
                    NSInteger studentState = 0;
                    
                    if(![[dataDict objectForKey:@"studentId"] isKindOfClass:[NSNull class]]) {
                        studentId = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentId"]];
                    }
                    else {
                        studentId = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if ([[UserData getChangLanguage] isEqualToString:@"th"]) {
                        if(![[dataDict objectForKey:@"studentName"] isKindOfClass:[NSNull class]]) {
                            studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentName"]];
                            if ([studentName isEqualToString:@" "]) {
                                studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentNameEN"]];
                            }
                        }
                        else {
                            studentName = [[NSMutableString alloc] initWithString:@""];
                        }
                    }else{
                        if(![[dataDict objectForKey:@"studentNameEN"] isKindOfClass:[NSNull class]]) {
                            studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentNameEN"]];
                            if ([studentName isEqualToString:@" "]) {
                                studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentName"]];
                            }
                        }
                        else {
                            studentName = [[NSMutableString alloc] initWithString:@""];
                        }
                        
                    }
                    
                    
                    if(![[dataDict objectForKey:@"teachername"] isKindOfClass:[NSNull class]]) {
                        teacherName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"teachername"]];
                    }
                    else {
                        teacherName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"pic"] isKindOfClass:[NSNull class]]) {
                        studentPic = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"pic"]];
                    }
                    else {
                        studentPic = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"Student_State"] isKindOfClass:[NSNull class]]) {
                        studentState = [ [dataDict objectForKey:@"Student_State"]integerValue];
                    }
                    
                    if(![[dataDict objectForKey:@"teacherId"] isKindOfClass:[NSNull class]]) {
                        teacherId = [[dataDict objectForKey:@"teacherId"] longLongValue];
                    }
                    
                    int scanStatus = -1;
                    
                    if(![[dataDict objectForKey:@"scanstatus"] isKindOfClass:[NSNull class]]) {
                        // int statusCode = [[dataDict objectForKey:@"scanstatus"] integerValue];
                        NSMutableString   *statusCode = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"scanstatus"]];
                        NSLog(@"scanStatus = %@",statusCode);
                        if([statusCode isEqualToString:@""]){
                            scanStatus = -1;
                        }else{
                            scanStatus = [statusCode integerValue];
                        }
                        switch (scanStatus) {
                            case 0:
                                scanStatus = 0; // ontime
                                break;
                            case 1:
                                scanStatus = 1; // late
                                break;
                            case 3:
                                scanStatus = 3; // absence
                                break;
                            case 10:
                                scanStatus = 10; // personal leave
                                break;
                            case 11:
                                scanStatus = 11; // sick leave
                                break;
                            case 12:
                                scanStatus = 12; // event
                                break;
                            default:
                                scanStatus = -1; // not scan
                                break;
                        }
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentId);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) teacherName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentPic);
                    
                    TAStudentStatusModel *model = [[TAStudentStatusModel alloc] init];
                    [model setUserId:userId];
                    [model setScanStatus:scanStatus];
                    [model setStudentId:studentId];
                    [model setStudentName:studentName];
                    [model setTeacherId:teacherId];
                    [model setCheckedTeacherName:teacherName];
                    [model setAuthorized:authorized];
                    [model setStudentPic:studentPic];
                    [model setStudentState:studentState];
                    [model setStatusCheck:statusCheck];
                    
                    [studentStatusArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTEGetStudentInClassAPI:data:success:)]) {
                    [self.delegate callTEGetStudentInClassAPI:self data:studentStatusArray success:YES];
                }
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTEGetStudentInClassAPI:data:success:)]) {
                [self.delegate callTEGetStudentInClassAPI:self data:nil success:NO];
            }
        }
        
    }];
}


@end
