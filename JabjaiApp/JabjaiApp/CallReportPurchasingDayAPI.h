//
//  CallReportPurchasingDayAPI.h
//  JabjaiApp
//
//  Created by toffee on 10/18/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CallReportPurchasingDayAPI;

@protocol CallReportPurchasingDayAPIDelegate <NSObject>

@optional

- (void)callReportPurchasingDayAPI:(CallReportPurchasingDayAPI *)classObj timeArray:(NSArray *)timeArray totalMoneyArray:(NSArray *)totalMoneyArray success:(BOOL)success;


@end
@interface CallReportPurchasingDayAPI : NSObject
@property (nonatomic, weak) id<CallReportPurchasingDayAPIDelegate> delegate;

- (void)call:(long long)schoolId date:(NSString*)date;
@end
