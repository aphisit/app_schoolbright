//
//  ReportCheckFlagPoleViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportCheckFlagPoleViewController.h"
#import "UserData.h"
#import "ReportCheckFlagPoleCalendarViewController.h"
#import "ReportListTeacherViewController.h"
#import "NavigationViewController.h"
#import "AppDelegate.h"
#import "Utils.h"

@interface ReportCheckFlagPoleViewController (){
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    NSString *nameClassRoom;
}
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;

@end

static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";

@implementation ReportCheckFlagPoleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.classLevelTextField.delegate = self;
    self.classRoomTextField.delegate = self;
    _selectedClassLevelIndex = -1;
    _selectedClassroomIndex = -1;
    [self getSchoolClassLevel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientNext;
    //set color button next
    [self.nextButton layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.nextButton.bounds;
    [self.nextButton.layer insertSublayer:gradientNext atIndex:0];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section2.layer setShadowOpacity:0.3];
    [self.section2.layer setShadowRadius:3.0];
    [self.section2.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.nextButton.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.nextButton.layer setShadowOpacity:0.3];
    [self.nextButton.layer setShadowRadius:3.0];
    [self.nextButton.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    self.headerClassLevelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_CLASSLEVEL",nil,[Utils getLanguage],nil);
    self.headerClassLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_CLASS",nil,[Utils getLanguage],nil);
    self.classLevelTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_CHECKNAME_REP_CLASS",nil,[Utils getLanguage],nil);
    self.classRoomTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_CHECKNAME_REP_CLASSROOM",nil,[Utils getLanguage],nil);
    [self.nextButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_CHECKNAME_REP_NEXT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

- (void)getSchoolClassLevel {
    [self showIndicator];
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        [self stopIndicator];
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        NSLog(@"CLASSLAVEL = %@",classLevelStringArray);
    }
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    [self showIndicator];
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}
#pragma mark - CallGetStudentClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _classroomArray = data;
        [self stopIndicator];
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // User press class level textfield
        
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CHECKNAME_REP_SELECT_CLASSLEVEL",nil,[Utils getLanguage],nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }
        return NO;
    }
    return YES;
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;
    [tableListDialog showDialogInView:topView dataArr:data];
}
- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;
    [alertDialog showDialogInView:topView title:@"แจ้งเตือน" message:message];
}
#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    if([requestCode isEqualToString:classLevelRequestCode]) {
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        // Clear dependency text field values
        self.classRoomTextField.text = @"";
        _selectedClassroomIndex = -1;
        _classLevelId = 0;
        // get classroom data with class level
        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:_classLevelId];
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        // self.level2Id = [[NSMutableArray alloc] init];
        
        if(index == _selectedClassroomIndex) {
            return;
        }
        self.classRoomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        nameClassRoom = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        _classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        // addID Room
        // [self.level2Id addObject:@(_classroomId)];
        
    }
    
}
#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CHECKNAME_REP_SELECT_CLASSLEVEL",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classRoomTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CHECKNAME_REP_SELECT_CLASS",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    return YES;
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ReportCheckFlagPoleCalendarViewController *viewController = (ReportCheckFlagPoleCalendarViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ReportCheckFlagPoleCalendarStotyboard"];
                viewController.classLevelArray = _classLevelArray;
                viewController.classroomArray = _classroomArray;
                viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
                viewController.selectedClassroomIndex = _selectedClassroomIndex;
                viewController.selectedClassroomId = _classroomId;
                viewController.nameClassRoom = nameClassRoom;
        
        NavigationViewController *leftViewController = (NavigationViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"NavigationStoryboard"];
        
        SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:viewController leftMenuViewController:leftViewController];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [[appDelegate window] setRootViewController:slideMenuController];

    }
    
}

- (IBAction)moveBack:(id)sender {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    
}
@end
