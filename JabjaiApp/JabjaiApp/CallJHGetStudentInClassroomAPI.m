//
//  CallJHGetStudentInClassroomAPI.m
//  JabjaiApp
//
//  Created by toffee on 11/22/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallJHGetStudentInClassroomAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"
@interface CallJHGetStudentInClassroomAPI () {
    NSInteger connectCounter;
    NSMutableArray<JHSelectedStudentModel *> *selectedStudentArray;
}
@end
@implementation CallJHGetStudentInClassroomAPI
- (void)call:(long long)schoolId classroomId:(long long)classroomId {
    connectCounter = 0;
    [self getStudentInClass:schoolId classroomId:classroomId];
}

#pragma mark - Get API Data

- (void)getStudentInClass:(long long)schoolId classroomId:(long long)classroomId {
    
    NSString *URLString = [APIURL getJHStudentInClassroomWithSchoolId:schoolId classroomId:classroomId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getStudentInClass:schoolId classroomId:classroomId];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentInClass:schoolId classroomId:classroomId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentInClass:schoolId classroomId:classroomId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(selectedStudentArray != nil) {
                    [selectedStudentArray removeAllObjects];
                    selectedStudentArray = nil;
                }
                
                selectedStudentArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long studentId = [[dataDict objectForKey:@"studentid"] longLongValue];
                    long long picverion = [[dataDict objectForKey:@"picverion"] longLongValue];
                    NSMutableString *studentName;
                    NSMutableString *studentPic;
                    
                    
                    
                    if ([[UserData getChangLanguage] isEqualToString:@"th"]) {
                        if(![[dataDict objectForKey:@"studentname"] isKindOfClass:[NSNull class]]) {
                            studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentname"]];
                        }
                        else {
                            studentName = [[NSMutableString alloc] initWithString:@""];
                        }
                    }else{
                        if(![[dataDict objectForKey:@"studentnameEN"] isKindOfClass:[NSNull class]]) {
                            studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentnameEN"]];
                            if ([studentName isEqualToString:@" "]) {
                                studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentname"]];
                            }
                        }
                        else {
                            studentName = [[NSMutableString alloc] initWithString:@""];
                        }
                        
                    }
                    
                    
                    
                    if(![[dataDict objectForKey:@"pic"] isKindOfClass:[NSNull class]]) {
                        studentPic = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"pic"]];
                    }
                    else {
                        studentPic = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentPic);
                    
                    JHSelectedStudentModel *model = [[JHSelectedStudentModel alloc] init];
                    [model setStudentId:studentId];
                    [model setStudentName:studentName];
                    [model setStudentPic:studentPic];
                    [model setPicverion:picverion];
                    [model setSelected:NO];
                    
                    [selectedStudentArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callJHGetStudentInClassrommAPI:data:success:)]) {
                    [self.delegate callJHGetStudentInClassrommAPI:self data:selectedStudentArray success:YES];
                }
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callJHGetStudentInClassrommAPI:data:success:)]) {
                [self.delegate callJHGetStudentInClassrommAPI:self data:nil success:NO];
            }
        }
        
        
    }];
}


@end
