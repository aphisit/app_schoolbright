//
//  CallGetTEHistoryStudentStatusList.h
//  JabjaiApp
//
//  Created by toffee on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPCheckNameFlagAttendanceModel.h"


@class CallGetTEHistoryStudentStatusList;

@protocol CallGetTEHistoryStudentStatusListAPIDelegate <NSObject>

@optional
- (void)callGetTEHistoryStudentStatusListAPI:(CallGetTEHistoryStudentStatusList *)classObj data:(NSMutableArray<RPCheckNameFlagAttendanceModel *> *)data success:(BOOL)success;

@end

@interface CallGetTEHistoryStudentStatusList : NSObject

@property (nonatomic, weak) id<CallGetTEHistoryStudentStatusListAPIDelegate> delegate;

- (void)call:(long long)schoolId classroomId:(long long)classroomId  date:(NSDate *)date;


@end
