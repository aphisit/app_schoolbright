//
//  TeacherTimeTableViewController.h
//  JabjaiApp
//
//  Created by mac on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import ARSlidingPanel;

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CFSCalendar.h"

#import "SlideMenuController.h"

@protocol TeacherTimeTableViewControllerDelegate <NSObject>

@optional
- (void)onSelectDate:(NSDate *)date;

@end

@interface TeacherTimeTableViewController : UIViewController <CFSCalendarDelegate, CFSCalendarDataSource, CFSCalendarDelegateAppearance, UITableViewDelegate, UITableViewDataSource, SlideMenuControllerDelegate>

//@property (retain, nonatomic) id<TeacherTimeTableViewControllerDelegate> delegate;

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeadMenuConstraint;

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;



- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailConstraint;

- (IBAction)openDetail:(id)sender;

- (IBAction)openDrawer:(id)sender;

@end
