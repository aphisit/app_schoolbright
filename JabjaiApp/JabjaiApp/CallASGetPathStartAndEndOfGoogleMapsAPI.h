//
//  CallASGetPathStartAndEndOfGoogleMapsAPI.h
//  JabjaiApp
//
//  Created by Mac on 12/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASDistanceModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallASGetPathStartAndEndOfGoogleMapsAPI;
@protocol CallASGetPathStartAndEndOfGoogleMapsAPIDelegate <NSObject>

- (void) CallASGetPathStartAndEndOfGoogleMapsAPI:(CallASGetPathStartAndEndOfGoogleMapsAPI*)classObj data:(ASDistanceModel*)data success:(BOOL)success;

@end
@interface CallASGetPathStartAndEndOfGoogleMapsAPI : NSObject
@property (nonatomic,weak) id<CallASGetPathStartAndEndOfGoogleMapsAPIDelegate> delegate;
- (void)call:(NSString*)sourceLocation destinationLocation:(NSString*)destinationLocation;
@end

NS_ASSUME_NONNULL_END
