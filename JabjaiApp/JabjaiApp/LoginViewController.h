//
//  LoginViewController.h
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SchoolCodeDialogViewController.h"
#import "SlideMenuController.h"
#import "LoginListAllSchoolNameDialog.h"
#import "CallSTSendTokenAndLanguageAPI.h"

extern NSString *tokenStr;

@interface LoginViewController : UIViewController <SchoolCodeDialogViewControllerDelegate, SlideMenuControllerDelegate,LoginListAllSchoolNameDialogDelegate,UITextFieldDelegate,CallSTSendTokenAndLanguageAPIDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *headerSchoolNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerUsernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPasswordLabel;
// UI View
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *schoolNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIButton *languageBtn;
@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) UIViewController *rootViewControler;

//- (IBAction)forgotPasswordAction:(id)sender;
//- (IBAction)signupAction:(id)sender;//button register
- (IBAction)loginAction:(id)sender;
- (IBAction)DismissKeyboard:(id)sender;
- (IBAction)changLanguage:(id)sender;
    
-(void)setLanguage;

@end
