 //
//  StudyViewController.m
//  JabjaiApp
//
//  Created by mac on 10/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "StudyViewController.h"
#import "APIURL.h"
#import "Utils.h"
#import "YearTermModel.h"
#import "DateUtility.h"
#import "AttendToSchoolModel.h"
#import "CreditBureauGraphDialog.h"
#import "UserData.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "UserViewController.h"
//MKDropdownMenu *GlobalInt;
NSString *xx;
@interface StudyViewController () {
    UIColor *yellowColor;
    UIColor *greenColor;
    UIColor *onTimeColor;
    UIColor *lateColor;
    UIColor *absenceColor;
    UIColor *sickColor;
    UIColor *personalColor;
    UIColor *eventColor;
    UIColor *unCheckColor;
    
    NSMutableArray *attendToSchoolArray;
    NSMutableArray *yData;
    NSMutableArray *xData;
    NSMutableArray *DateList;
    
    NSInteger totalStatus;
    NSInteger statusOnTime;
    NSInteger statusLate;
    NSInteger statusAbsence;
    NSInteger statusSick;
    NSInteger statusPersonal;
    NSInteger statusEvent;
    NSInteger statusNoCheckPercent;
    
    NSString *selected_calendar;

    // The dialog
    TableListWithHeaderDialog *dialogSelectMonth;
    TableListDialog *dialogSelectYear;
    CreditBureauGraphDialog *creditBureauGraphDialog;
    specifyDateDialog *dialogSpecifyDate;
    
    // Data Containers
    NSMutableDictionary *yearTermDict; //Key is yearNumber and data are array of YearTermModel
    
    NSString *weekly_text;
    NSString *monthly_text;
    NSString *yearly_text;
    
    NSString *buttonFontName;
    CGFloat buttonFontSize;
    UIFont *buttonUIFont;
    
    NSBundle *myLangBundle;
    
    // List of data show in listview
    NSMutableArray *years;
    NSInteger selectedYearIndex;
    NSInteger connectCounter;
    MKDropdownMenu* mKDropdownMenu;
    
    float onTimePercent, latePercent, absencePercent, personalPercent, sickPercent, eventPercent, noCheckPercent;
   
    
   
}
@property (strong, nonatomic) UIColor *orangeColor;


@end
static NSString *cellIdentifier = @"StudyCell";
static NSString *selectMonthRequestCode = @"SELECT_MONTH";
static NSString *selectYearRequestCode = @"SELECT_YEAR";
//MKDropdownMenu* mKDropdownMenu;

@implementation StudyViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.orangeColor = UIColorWithHexString(@"#F56B20");
    yellowColor = [UIColor colorWithRed:254/255.0 green:180/255.0 blue:59/255.0 alpha:1.0];
    greenColor = [UIColor colorWithRed:34/255.0 green:125/255.0 blue:117/255.0 alpha:1.0];
    
    
    onTimeColor = [UIColor colorWithRed:0.19 green:0.74 blue:0.31 alpha:1.0];
    lateColor = [UIColor colorWithRed:1.00 green:0.76 blue:0.33 alpha:1.0];
    absenceColor = [UIColor colorWithRed:0.94 green:0.43 blue:0.43 alpha:1.0];
    sickColor = [UIColor colorWithRed:0.87 green:0.43 blue:0.98 alpha:1.0];
    personalColor = [UIColor colorWithRed:0.58 green:0.11 blue:0.70 alpha:1.0];
    eventColor = [UIColor colorWithRed:0.11 green:0.45 blue:0.77 alpha:1.0];
    unCheckColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    
//    buttonFontName = self.monthlyBtn.titleLabel.font.fontName;
//    buttonFontSize = self.monthlyBtn.titleLabel.font.pointSize;
    buttonUIFont = [UIFont fontWithName:buttonFontName size:buttonFontSize];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([StudyTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self.tableView layoutIfNeeded];
    // setup list view data variables
    years = [[NSMutableArray alloc] init];
    selectedYearIndex = 0;
    connectCounter = 0;
    // setup the dialog
    dialogSelectMonth = [[TableListWithHeaderDialog alloc] initWithRequestCode:selectMonthRequestCode];
    dialogSelectMonth.delegate = self;
    dialogSelectYear = [[TableListDialog alloc] initWithRequestCode:selectYearRequestCode];
    dialogSelectYear.delegate = self;
    creditBureauGraphDialog = [[CreditBureauGraphDialog alloc] init];
    [self.pieChart setBackgroundColor:[UIColor clearColor]];
    xData = [[NSMutableArray alloc] init];
    yData = [[NSMutableArray alloc] init];
    [self setLanguage];
    [self doSelectDayCalendar:@"week"];
    [self setDropdowe];
    [self decorateButton];
    [self setupPieChartView];
    [self setupGraphData];
    //set up graphLegend
//    [self.viewOnTime setBackgroundColor:onTimeColor];
//    [self.viewLate setBackgroundColor:lateColor];
//    [self.viewAbsence setBackgroundColor:absenceColor];
//    [self.viewSick setBackgroundColor:sickColor];
//    [self.viewPersonal setBackgroundColor:personalColor];
//    [self.viewEvent setBackgroundColor:eventColor];
    //set circle UIview
//    CGFloat max1 = MAX(self.viewOnTime.frame.size.width, self.viewOnTime.frame.size.height);
//    self.viewOnTime.layer.cornerRadius = max1 / 2.0;
//    CGFloat max2 = MAX(self.viewLate.frame.size.width, self.viewLate.frame.size.height);
//    self.viewLate.layer.cornerRadius = max2 / 2.0;
//    CGFloat max3 = MAX(self.viewAbsence.frame.size.width, self.viewAbsence.frame.size.height);
//    self.viewAbsence.layer.cornerRadius = max3 / 2.0;
//    CGFloat max4 = MAX(self.viewSick.frame.size.width, self.viewSick.frame.size.height);
//    self.viewSick.layer.cornerRadius = max4 / 2.0;
//    CGFloat max5 = MAX(self.viewPersonal.frame.size.width, self.viewPersonal.frame.size.height);
//    self.viewPersonal.layer.cornerRadius = max5 / 2.0;
//    CGFloat max6 = MAX(self.viewEvent.frame.size.width, self.viewEvent.frame.size.height);
//    self.viewEvent.layer.cornerRadius = max6 / 2.0;
   // [self declaredDialog];
    if([UserData getAcademyType] == SCHOOL) {
        [self getYearAndTermData];
       // [self performSelectWeeklyButton];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    
    if(self.pieChart != nil) {
        [self.pieChart animateWithYAxisDuration:2.0 easingOption:ChartEasingOptionEaseOutCirc];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [mKDropdownMenu closeAllComponentsAnimated:YES];
    NSLog(@"xxxx");
}

-(void)viewWillDisappear:(BOOL)animated{
    [mKDropdownMenu closeAllComponentsAnimated:YES];
    NSLog(@"xxxx");
}







//- (void)declaredDialog{
//    // Attach tap event to each legend view
//    UITapGestureRecognizer *onTimeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleOnTimeStackViewTap:)];
//    [self.onTimeStackView addGestureRecognizer:onTimeTap];
//
//    UITapGestureRecognizer *lateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLateStackViewTap:)];
//    [self.lateStackView addGestureRecognizer:lateTap];
//
//    UITapGestureRecognizer *absenceTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleAbsenceStackViewTap:)];
//    [self.absenceStackView addGestureRecognizer:absenceTap];
//
//    UITapGestureRecognizer *sickTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSickStackViewTap:)];
//    [self.sickStackView addGestureRecognizer:sickTap];
//
//    UITapGestureRecognizer *personalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePersonalStackViewTap:)];
//    [self.personalStackView addGestureRecognizer:personalTap];
//
//    UITapGestureRecognizer *eventTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleEventStackViewTap:)];
//    [self.eventStackView addGestureRecognizer:eventTap];
//}

-(void)setLanguage{
    weekly_text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_WEEKLY",nil,myLangBundle,nil);
    monthly_text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_MONTHLY",nil,myLangBundle,nil);
    yearly_text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_YEARLY",nil,myLangBundle,nil);
//    _reportAttendanceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_REPORT_ATTENDANCE",nil,myLangBundle,nil);
//    _onTimeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,myLangBundle,nil);
//    _lateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LATE",nil,myLangBundle,nil);
//    _absenceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ABSENCE",nil,myLangBundle,nil);
}

- (void) setDropdowe{
    // Set up dropdown
    self.dateTimeDropdown.dataSource = self;
    self.dateTimeDropdown.delegate = self;
    self.dateTimeDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.dateTimeDropdown.backgroundDimmingOpacity = 0;
    self.dateTimeDropdown.dropdownShowsTopRowSeparator = YES;
    self.dateTimeDropdown.dropdownCornerRadius = 5.0;
    self.dateTimeDropdown.tintColor = [UIColor blackColor];
   // self.dateTimeDropdown.layer.cornerRadius = 5;
    self.dateTimeDropdown.layer.masksToBounds = YES;
    
}

- (void)decorateButton {
//    self.yearlyBtn.layer.cornerRadius = 5.0;
//    self.yearlyBtn.layer.masksToBounds = YES;
//    self.yearlyBtn.backgroundColor = greenColor;
//    [self.yearlyBtn setTitle:yearly_text forState:UIControlStateNormal];
//    [self.yearlyBtn.titleLabel setFont:buttonUIFont];
    
    
//    self.monthlyBtn.layer.cornerRadius = 5.0;
//    self.monthlyBtn.layer.masksToBounds = YES;
//    self.monthlyBtn.backgroundColor = greenColor;
//    [self.monthlyBtn setTitle:monthly_text forState:UIControlStateNormal];
//    [self.monthlyBtn.titleLabel setFont:buttonUIFont];
//
//    self.weeklyBtn.layer.cornerRadius = 5.0;
//    self.weeklyBtn.layer.masksToBounds = YES;
//    self.weeklyBtn.backgroundColor = yellowColor;
//    [self.weeklyBtn setTitle:weekly_text forState:UIControlStateNormal];
//    [self.weeklyBtn.titleLabel setFont:buttonUIFont];
}

#pragma mark - Pie Chart

- (void)setupPieChartView {
    self.pieChart.centerText = @"xxxx";
    self.pieChart.delegate = self;
    self.pieChart.usePercentValuesEnabled = YES;
    self.pieChart.drawSlicesUnderHoleEnabled = NO;
    //self.pieChart.drawSliceTextEnabled = NO;
    self.pieChart.holeRadiusPercent = 0.7;
    self.pieChart.transparentCircleRadiusPercent = 0.0;
    self.pieChart.chartDescription.enabled = NO;
    [self.pieChart setExtraOffsetsWithLeft:5.f top:10.f right:5.f bottom:5.f];
    self.pieChart.drawCenterTextEnabled = YES;
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    self.pieChart.drawHoleEnabled = YES;
    self.pieChart.rotationAngle = 0.0;
    self.pieChart.rotationEnabled = YES;
    self.pieChart.highlightPerTapEnabled = NO;
    self.pieChart.entryLabelColor = [UIColor whiteColor];
    //self.pieChart.frame.size = CGRectMake(0, 0, 100, 100);
    //[self.pieChart setFrame:CGRectMake(25.0, 65.0, 500.0, 500.0)];
    ChartLegend *l = self.pieChart.legend;
    [l setEnabled:NO];
    
        if ([UserData getUserType] == STUDENT) {
            NSMutableAttributedString *centerText = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_REPORT_STUDENT",nil,myLangBundle,nil)];
            [centerText setAttributes:@{
                                        NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:28.f],
                                        NSParagraphStyleAttributeName: paragraphStyle
                                        } range:NSMakeRange(0, centerText.length)];
            self.pieChart.centerAttributedText = centerText;
        }else{
            NSMutableAttributedString *centerText = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_REPORT_ATTENDANCE",nil,myLangBundle,nil)];
            [centerText setAttributes:@{
                                        NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:28.f],
                                        NSParagraphStyleAttributeName: paragraphStyle
                                        } range:NSMakeRange(0, centerText.length)];
            self.pieChart.centerAttributedText = centerText;
        }

    
}

- (void)setupGraphData {
    if(yData.count == 0) {
        [self.pieChart setAlpha:0.0];
        //[self.graphLegendView setAlpha:0.0];
        //[self.graphLegendView setAlpha:0.0];
        [self.noDataLabel setAlpha:1.0];
        return;
    }
    else {
        //self.pieChart.centerText = @"xxxxxxxxxxx";
        [self.pieChart setAlpha:1.0];
        //[self.graphLegendView setAlpha:1.0];
        [self.noDataLabel setAlpha:0.0];
    }
    
    NSMutableArray *values = [[NSMutableArray alloc] init];
    
    // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
    for (int i = 0; i < yData.count; i++)
    {
        //[values addObject:[[PieChartDataEntry alloc] initWithValue:([[yData objectAtIndex:i] integerValue]) label:[xData objectAtIndex:i]]];
        [values addObject:[[PieChartDataEntry alloc] initWithValue:([[yData objectAtIndex:i] integerValue]) label:@""]];//Show title in piechart
      
    }
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithEntries:values label:@""];
    dataSet.sliceSpace = 2.0;
    
    // add a lot of colors

    NSMutableArray *colors = [[NSMutableArray alloc] init];
    [colors addObject:onTimeColor];
    [colors addObject:lateColor];
    [colors addObject:absenceColor];
    [colors addObject:sickColor];
    [colors addObject:personalColor];
    [colors addObject:eventColor];
    [colors addObject:unCheckColor];
    [colors addObjectsFromArray:ChartColorTemplates.vordiplom];
    [colors addObjectsFromArray:ChartColorTemplates.joyful];
    [colors addObjectsFromArray:ChartColorTemplates.colorful];
    [colors addObjectsFromArray:ChartColorTemplates.liberty];
    [colors addObjectsFromArray:ChartColorTemplates.pastel];
    [colors addObject:[UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f]];
   
    dataSet.colors = colors;
    
    PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
    
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
    [data setValueFont:[UIFont fontWithName:@"TH Sarabun New" size:0.0f]];
    [data setValueTextColor:UIColor.whiteColor];
    
    
    
    
    
    //[self.pieChart centerText : @"xxxxxxx"];
    self.pieChart.data = data;
    [self.pieChart highlightValue:nil];
    
    
    
//    [self.pieChart animateWithXAxisDuration:2.0 easingOption:ChartEasingOptionEaseOutBack];
    
    [self.pieChart animateWithYAxisDuration:2.0 easingOption:ChartEasingOptionEaseOutCirc];
    
}

#pragma ChartDelegate

-(void)chartValueSelected:(ChartViewBase *)chartView entry:(ChartDataEntry *)entry highlight:(ChartHighlight *)highlight {
    
    NSInteger selectedIndex = highlight.x
    ;
    
    NSString *dialogTitle = @"สถิติการมาเรียน";
    
    if(selectedIndex == 0) { // Select on time
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:ONTIME times:statusOnTime totalTimes:totalStatus];
    }
    else if(selectedIndex == 1) {
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:LATE times:statusLate totalTimes:totalStatus];
    }
    else if(selectedIndex == 2) {
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:ABSENCE times:statusAbsence totalTimes:totalStatus];
    }
    else if(selectedIndex == 3) {
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:SICK times:statusSick totalTimes:totalStatus];
    }
    else if(selectedIndex == 4) {
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:PERSONAL times:statusPersonal totalTimes:totalStatus];
    }
    else if(selectedIndex == 5){
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:EVENT times:statusEvent totalTimes:totalStatus];
    }else{
         [self showCreditBureauGraphDialogWithTitle:dialogTitle status:UNCHECK times:statusNoCheckPercent totalTimes:totalStatus];
    }
    
    NSString *logStr = [NSString stringWithFormat:@"chartValueSelected at index %i", (int)selectedIndex];
    NSLog(@"%@", logStr);
}

#pragma mark - Update chart data
- (void)updatePieChartDataWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    [self getAttendSchoolData:startDate endDate:endDate status:-1];
}

#pragma mark - Graph Legend Tap
-(void)handleOnTimeStackViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:ONTIME times:statusOnTime totalTimes:totalStatus];
}

-(void)handleLateStackViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:LATE times:statusLate totalTimes:totalStatus];
}

-(void)handleAbsenceStackViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:ABSENCE times:statusAbsence totalTimes:totalStatus];
}
-(void)handleSickStackViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:SICK times:statusSick totalTimes:totalStatus];
}
-(void)handlePersonalStackViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:PERSONAL times:statusPersonal totalTimes:totalStatus];
}
-(void)handleEventStackViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:EVENT times:statusEvent totalTimes:totalStatus];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 7;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StudyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    // onTimeLegendStr = [NSString stringWithFormat:@"%@\n(%.0f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,myLangBundle,nil)
    if (indexPath.row == 0) {
        cell.amountStatusLabel.text = [NSString stringWithFormat:@"%d %@",statusOnTime,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        cell.percentLabel.text = [NSString stringWithFormat:@"(%.0f%%)",onTimePercent];
    }
    else if (indexPath.row == 1){
        cell.amountStatusLabel.text = [NSString stringWithFormat:@"%d %@",statusLate,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        cell.percentLabel.text = [NSString stringWithFormat:@"(%.0f%%)",latePercent];
    }
    else if (indexPath.row == 2){
        cell.amountStatusLabel.text = [NSString stringWithFormat:@"%d %@",statusAbsence,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        cell.percentLabel.text = [NSString stringWithFormat:@"(%.0f%%)",absencePercent];
    }
    else if (indexPath.row == 3){
        cell.amountStatusLabel.text = [NSString stringWithFormat:@"%d %@",statusSick,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        cell.percentLabel.text = [NSString stringWithFormat:@"(%.0f%%)",sickPercent];
    }
    else if (indexPath.row == 4){
        cell.amountStatusLabel.text = [NSString stringWithFormat:@"%d %@",statusPersonal,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        cell.percentLabel.text = [NSString stringWithFormat:@"(%.0f%%)",personalPercent];
    }
    else if (indexPath.row == 5){
        cell.amountStatusLabel.text = [NSString stringWithFormat:@"%d %@",statusEvent,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        cell.percentLabel.text = [NSString stringWithFormat:@"(%.0f%%)",eventPercent];
    }
    else if (indexPath.row == 6){
        cell.amountStatusLabel.text = [NSString stringWithFormat:@"%d %@",statusNoCheckPercent,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_TIMES", nil, [Utils getLanguage], nil)];
        cell.percentLabel.text = [NSString stringWithFormat:@"(%.0f%%)",noCheckPercent];
    }
    
    [cell updateStatus:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *dialogTitle = @"สถิติการมาเรียน";
    
    if(indexPath.row == 0) { // Select on time
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:ONTIME times:statusOnTime totalTimes:totalStatus];
    }
    else if(indexPath.row == 1) {
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:LATE times:statusLate totalTimes:totalStatus];
    }
    else if(indexPath.row == 2) {
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:ABSENCE times:statusAbsence totalTimes:totalStatus];
    }
    else if(indexPath.row == 3) {
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:SICK times:statusSick totalTimes:totalStatus];
    }
    else if(indexPath.row == 4) {
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:PERSONAL times:statusPersonal totalTimes:totalStatus];
    }
    else if(indexPath.row == 5){
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:EVENT times:statusEvent totalTimes:totalStatus];
    }else{
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:UNCHECK times:statusNoCheckPercent totalTimes:totalStatus];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark View Controller Functions
- (IBAction)yearlyButtonTap:(id)sender {
    
    if(years == nil) {
        return;
    }
    
    NSMutableArray *yearStrArr = [[NSMutableArray alloc] init];
    
    for(NSNumber *year in years) {
        [yearStrArr addObject:[NSString stringWithFormat:@"%i", [year intValue]]];
    }
    
    [self showDialogSelectYearWithYearArray:yearStrArr];
}

- (IBAction)monthlyButtonTap:(id)sender {

    
    NSMutableArray *monthsTermDataKeys = [[NSMutableArray alloc] init];
    NSMutableDictionary *monthsDict = [[NSMutableDictionary alloc] init];
    
    if(yearTermDict == nil) {
        return;
    }
    
    // Start
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSNumber *selectedYear = [years objectAtIndex:selectedYearIndex];
    NSMutableArray *termArr = [yearTermDict objectForKey:selectedYear];
    
    [monthsTermDataKeys addObject:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ALL",nil,myLangBundle,nil)];
    [monthsDict setObject:[[NSArray alloc] initWithObjects:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ALL",nil,myLangBundle,nil), nil] forKey:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ALL",nil,myLangBundle,nil)];
    
    for(YearTermModel *model in termArr) {
        
        NSInteger startMonth = [gregorian component:NSCalendarUnitMonth fromDate:model.termBegin];
        NSInteger endMonth = [gregorian component:NSCalendarUnitMonth fromDate:model.termEnd];
        
        NSArray *monthsInTerm = [Utils getThaiMonthRangeWithStartMonth:startMonth endMonth:endMonth];
        
        if(monthsInTerm != nil) {
            
            [monthsTermDataKeys addObject:model.termName];
            [monthsDict setObject:monthsInTerm forKey:model.termName];
            
        }
    }
    
    [self showDialogSelectMonthWithMonthDict:monthsDict monthsTermDataKeys:monthsTermDataKeys];
    
}

- (IBAction)weeklyButtonTap:(id)sender {
    
    NSLog(@"%@", @"Tap Weekly Button");

    [self performSelectWeeklyButton];
}


#pragma mark TableListWithHeaderDialogDelegate

- (void)onItemSelectWithRequestCode:(NSString *)requestCode headerSectionString:(NSString *)headerSectionString selectedString:(NSString *)selectedString {
    
    NSLog(@"%@, %@", headerSectionString, selectedString);
    
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSInteger gregorianYear = [gregorian component:NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSNumber *selectedYear = [years objectAtIndex:selectedYearIndex];
    NSInteger yearAD = [selectedYear integerValue];
    
    if(yearAD > gregorianYear) {
        yearAD -= 543; //Convert from BE to AD
    }
    
    NSMutableArray *termArr = [yearTermDict objectForKey:selectedYear];
    
    if([headerSectionString isEqualToString:selectedString]) { // If user select all months
        
        [self performSelectYearlyButton];
        
    }
    else {
        
        NSInteger selectedMonthNumber = [Utils getThaiMonthNumberWithName:selectedString];
        
        for(YearTermModel *model in termArr) {
            if([model.termName isEqualToString:headerSectionString]) {
                NSInteger startMonth = [gregorian component:NSCalendarUnitMonth fromDate:model.termBegin];
                NSInteger endMonth = [gregorian component:NSCalendarUnitMonth fromDate:model.termEnd];
                
                if(selectedMonthNumber >= startMonth && selectedMonthNumber <= endMonth) {
                    NSDate *startMonth = [DateUtility startDateOfMonthAndYearInRangeWithMonth:selectedMonthNumber year:yearAD minDate:model.termBegin maxDate:model.termEnd];
                    NSDate *endMonth = [DateUtility endDateOfMonthAndYearInRangeWithMonth:selectedMonthNumber year:yearAD minDate:model.termBegin maxDate:model.termEnd];
                    
                    [self updatePieChartDataWithStartDate:startMonth endDate:endMonth];
                    
                    break;
                }
                
            }
        }
        
        // Change color and text of buttons
//        self.weeklyBtn.backgroundColor = greenColor;
//        [self.weeklyBtn setTitle:weekly_text forState:UIControlStateNormal];
//        [self.weeklyBtn.titleLabel setFont:buttonUIFont];
//        self.monthlyBtn.backgroundColor = yellowColor;
//        [self.monthlyBtn setTitle:selectedString forState:UIControlStateNormal];
//        [self.monthlyBtn.titleLabel setFont:buttonUIFont];
//        self.yearlyBtn.backgroundColor = yellowColor;
//        [self.yearlyBtn setTitle:[NSString stringWithFormat:@"%i", [selectedYear intValue]] forState:UIControlStateNormal];
//        [self.yearlyBtn.titleLabel setFont:buttonUIFont];
        
    }

}


#pragma mark TableListDialogDelegate
-(void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:selectYearRequestCode]) {
        selectedYearIndex = index;
        [self performSelectYearlyButton];
    }
}

-(void)doSelectDayCalendar:(NSString*)selectMode{
    
    NSDate *dateNow = [NSDate date];
    //day
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    [gregorian setFirstWeekday:2]; // Sunday == 1, Saturday == 7
    NSUInteger adjustedWeekdayOrdinal = [gregorian ordinalityOfUnit:NSWeekdayCalendarUnit inUnit:NSWeekCalendarUnit forDate:dateNow];
    NSLog(@"Adjusted weekday ordinal: %d", adjustedWeekdayOrdinal);
    //month
    NSCalendar *gregorian1 = [Utils getGregorianCalendar];
    NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:dateNow];
    [comp setDay:1];
    NSDate *firstDayOfMonthDate = [gregorian dateFromComponents:comp];
    //year
   
    NSDateComponents *components= [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:dateNow];
    components.month = 1;
    components.day = 1;
    NSDate *firstDayOfYear = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    
    
    NSString *daysNow = [Utils getThaiDateFormatWithDate:dateNow]; // 20 /กันยายน /2561
    
    //day
    NSDate *daysAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-(adjustedWeekdayOrdinal-1) toDate:[NSDate date] options:0];
    NSString *daysStr = [Utils getThaiDateFormatWithDate:daysAgo];
    //month
    NSDate *monthAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfMonthDate options:0];
    NSString *monthStr = [Utils getThaiDateFormatWithDate:monthAgo];
    //year
    NSDate *yearAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfYear options:0];
    NSString *yearStr = [Utils getThaiDateFormatWithDate:yearAgo];
    
    DateList = [[NSMutableArray alloc] init];
    [DateList addObject:[NSString stringWithFormat:@"%@ %@ - %@",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_WEEKLY",nil,myLangBundle,nil),daysStr,daysNow]];
    [DateList addObject:[NSString stringWithFormat:@"%@ %@ - %@",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_MONTHLY",nil,myLangBundle,nil),monthStr,daysNow]];
    [DateList addObject:[NSString stringWithFormat:@"%@ %@ - %@",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_YEARLY",nil,myLangBundle,nil),yearStr,daysNow]];
    [DateList addObject:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SELECTDATE",nil,myLangBundle,nil)];
    
    
    if ([selectMode isEqualToString:@"week"]) {
        selected_calendar = [DateList objectAtIndex:0];
        NSDate *days = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-(adjustedWeekdayOrdinal-1) toDate:[NSDate date] options:0];
         [self updatePieChartDataWithStartDate:days endDate:dateNow];
    }else if([selectMode isEqualToString:@"month"]){
        NSDate *month = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfMonthDate options:0];
        [self updatePieChartDataWithStartDate:month endDate:dateNow];
    }else if([selectMode isEqualToString:@"year"]){
        NSDate *year = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfYear options:0];
        [self updatePieChartDataWithStartDate:year endDate:dateNow];
    }
   
    NSLog(@"xxx");
}



-(void)performSelectWeeklyButton {
    
    NSDate *endDate = [NSDate date];
     NSDate *sixDaysAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-6 toDate:endDate options:0];
  
    [self updatePieChartDataWithStartDate:sixDaysAgo endDate:endDate];
    
    // Change color and text of buttons
//    self.weeklyBtn.backgroundColor = yellowColor;
//    [self.weeklyBtn setTitle:weekly_text forState:UIControlStateNormal];
//    [self.weeklyBtn.titleLabel setFont:buttonUIFont];
//    self.monthlyBtn.backgroundColor = greenColor;
//    [self.monthlyBtn setTitle:monthly_text forState:UIControlStateNormal];
//    [self.monthlyBtn.titleLabel setFont:buttonUIFont];
//    self.yearlyBtn.backgroundColor = greenColor;
//    [self.yearlyBtn setTitle:yearly_text forState:UIControlStateNormal];
//    [self.yearlyBtn.titleLabel setFont:buttonUIFont];
    
    // Set current study year at 0
    selectedYearIndex = 0;

}

-(void)performSelectYearlyButton {
    
    NSNumber *selectedYear = [years objectAtIndex:selectedYearIndex];
    NSMutableArray *termArr = [yearTermDict objectForKey:selectedYear];
    NSMutableArray *allDatesInYear = [[NSMutableArray alloc] init];
    
    for(YearTermModel *model in termArr) {
        [allDatesInYear addObject:model.termBegin];
        [allDatesInYear addObject:model.termEnd];
    }
    
    NSDate *startDate = [DateUtility minDate:allDatesInYear];
    NSDate *endDate = [DateUtility maxDate:allDatesInYear];
    
    [self updatePieChartDataWithStartDate:startDate endDate:endDate];
    
    //Clear other button to defaults
   // self.weeklyBtn.backgroundColor = greenColor;
//    [self.weeklyBtn setTitle:weekly_text forState:UIControlStateNormal];
//    [self.weeklyBtn.titleLabel setFont:buttonUIFont];
//    self.monthlyBtn.backgroundColor = greenColor;
//    [self.monthlyBtn setTitle:monthly_text forState:UIControlStateNormal];
//    [self.monthlyBtn.titleLabel setFont:buttonUIFont];
//    self.yearlyBtn.backgroundColor = yellowColor;
    
    NSNumber *yearNumber = [years objectAtIndex:selectedYearIndex];
//    [self.yearlyBtn setTitle:[NSString stringWithFormat:@"%i", [yearNumber intValue]] forState:UIControlStateNormal];
//    [self.yearlyBtn.titleLabel setFont:buttonUIFont];
}

#pragma mark - GetAPIData

-(void)getYearAndTermData {
    NSString *URLString = [APIURL getYearAndTermURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getYearAndTermData];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getYearAndTermData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getYearAndTermData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                yearTermDict = [[NSMutableDictionary alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    NSInteger yearNumber;
                    NSInteger yearID = [[dataDict objectForKey:@"Yearid"] integerValue];
                    if(![[dataDict objectForKey:@"YearNumber"] isKindOfClass:[NSNull class]]) {
                        yearNumber = [[dataDict objectForKey:@"YearNumber"] integerValue];
                    }
                    else {
                        yearNumber = 0;  
                    }
                    NSMutableString *termID = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Termid"]];
                    NSMutableString *termName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TermName"]];
                    NSMutableString *startDateStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dStart"]];
                    NSMutableString *endDateStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dEnd"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) termID);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) termName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endDateStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *lStartDate = [formatter dateFromString:startDateStr];
                    NSDate *lEndDate = [formatter dateFromString:endDateStr];
                    
                    YearTermModel *model = [[YearTermModel alloc] init];
                    [model setYearID:[[NSNumber alloc] initWithInteger:yearID]];
                    [model setYearNumber:[[NSNumber alloc] initWithInteger:yearNumber]];
                    [model setTermID:termID];
                    [model setTermName:termName];
                    [model setTermBegin:lStartDate];
                    [model setTermEnd:lEndDate];
                    
                    if([yearTermDict objectForKey:model.yearNumber] != nil) {
                        NSMutableArray *termArr = [yearTermDict objectForKey:model.yearNumber];
                        [termArr addObject:model];
                        [yearTermDict setObject:termArr forKey:model.yearNumber];
                    }
                    else {
                        NSMutableArray *termArr = [[NSMutableArray alloc] init];
                        [termArr addObject:model];
                        [yearTermDict setObject:termArr forKey:model.yearNumber];
                    }
                }
                
                // Update yearlist data
                years = [[NSMutableArray alloc] initWithArray:yearTermDict.allKeys];
                NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
                [years sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            }

        }

    }];
    
}

-(void)getAttendSchoolData:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status {
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getAttendSchoolDataURLWithStartDate:startDate endDate:endDate status:status schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getAttendSchoolData:startDate endDate:endDate status:status];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getAttendSchoolData:startDate endDate:endDate status:status];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getAttendSchoolData:startDate endDate:endDate status:status];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                attendToSchoolArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSString *statusStr = [dataDict objectForKey:@"StatusIN"];
                    NSMutableString *scanDateTimeStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dScan"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) scanDateTimeStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *scanDate = [formatter dateFromString:scanDateTimeStr];
                    
                    NSInteger status;
                    
                    if([statusStr isKindOfClass:[NSNull class]] || [statusStr length] == 0) {
                        status = 3; //Absence
                    }
                    else {
                        status = [statusStr integerValue];
                    }
                    
                    AttendToSchoolModel *model = [[AttendToSchoolModel alloc] init];
                    [model setStatus:[[NSNumber alloc] initWithInteger:status]];
                    [model setScanDate:scanDate];
                    
                    [attendToSchoolArray addObject:model];
                }
                
                // prepare data for pie chart
                [xData removeAllObjects];
                [yData removeAllObjects];
                
                statusOnTime = 0;
                statusLate = 0;
                statusAbsence = 0;
                statusSick = 0;
                statusPersonal = 0;
                statusEvent = 0;
                statusNoCheckPercent = 0;
                
                
                for(AttendToSchoolModel *model in attendToSchoolArray) {
                    
                    if([model.status integerValue] == 0 || [model.status integerValue] == 7) {
                        statusOnTime++;
                    }
                    else if([model.status integerValue] == 1) {
                        statusLate++;
                    }
                    else if([model.status integerValue] == 3) {
                        statusAbsence++;
                    }
                    else if([model.status integerValue] == 4 || [model.status integerValue] == 10) {
                        statusPersonal++;
                    }
                    else if([model.status integerValue] == 5 || [model.status integerValue] == 11) {
                        statusSick++;
                    }
                    else if([model.status integerValue] == 6 || [model.status integerValue] == 12) {
                        statusEvent++;
                    }
                    else if([model.status integerValue] == 99 ) {
                        statusNoCheckPercent++;
                    }
                }
                
                totalStatus = statusOnTime + statusLate + statusAbsence + statusPersonal + statusSick + statusEvent + statusNoCheckPercent;
                
                [xData addObject:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,myLangBundle,nil)];
                [xData addObject:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LATE",nil,myLangBundle,nil)];
                [xData addObject:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ABSENCE",nil,myLangBundle,nil)];
                [xData addObject:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SICK",nil,myLangBundle,nil)];
                [xData addObject:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_PERSONAL",nil,myLangBundle,nil)];
                [xData addObject:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_EVENT",nil,myLangBundle,nil)];
                [xData addObject:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NOCHECK",nil,myLangBundle,nil)];
                
                [yData addObject:[[NSNumber alloc] initWithInteger:statusOnTime]];
                [yData addObject:[[NSNumber alloc] initWithInteger:statusLate]];
                [yData addObject:[[NSNumber alloc] initWithInteger:statusAbsence]];
                [yData addObject:[[NSNumber alloc] initWithInteger:statusSick]];
                [yData addObject:[[NSNumber alloc] initWithInteger:statusPersonal]];
                [yData addObject:[[NSNumber alloc] initWithInteger:statusEvent]];
                [yData addObject:[[NSNumber alloc] initWithInteger:statusNoCheckPercent]];
                
                onTimePercent = (float)statusOnTime/totalStatus * 100;
                latePercent = (float)statusLate/totalStatus * 100;
                absencePercent = (float)statusAbsence/totalStatus * 100;
                personalPercent = (float)statusPersonal/totalStatus * 100;
                sickPercent = (float)statusSick/totalStatus * 100;
                eventPercent = (float)statusEvent/totalStatus * 100;
                noCheckPercent = (float)statusNoCheckPercent/totalStatus * 100;
//
//                NSString *onTimeLegendStr;
//                NSString *lateLegendStr;
//                NSString *absenceLegendStr;
//                NSString *personalLegendStr;
//                NSString *sickLegendStr;
//                NSString *eventLegendStr;
//
//                if(onTimePercent == 0 || onTimePercent == 100) {
//                    onTimeLegendStr = [NSString stringWithFormat:@"%@\n(%.0f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,myLangBundle,nil), onTimePercent];
//                }
//                else {
//                    onTimeLegendStr = [NSString stringWithFormat:@"%@\n(%.1f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ONTIME",nil,myLangBundle,nil), onTimePercent];
//                }
//
//                if(latePercent == 0 || latePercent == 100) {
//                    lateLegendStr = [NSString stringWithFormat:@"%@\n(%.0f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LATE",nil,myLangBundle,nil), latePercent];
//                }
//                else {
//                    lateLegendStr = [NSString stringWithFormat:@"%@\n(%.1f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LATE",nil,myLangBundle,nil), latePercent];
//                }
//
//                if(absencePercent == 0 || absencePercent == 100) {
//                    absenceLegendStr = [NSString stringWithFormat:@"%@\n(%.0f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ABSENCE",nil,myLangBundle,nil), absencePercent];
//                }
//                else {
//                    absenceLegendStr = [NSString stringWithFormat:@"%@\n(%.1f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_ABSENCE",nil,myLangBundle,nil), absencePercent];
//
//                }
//
//                if(personalPercent == 0 || personalPercent == 100) {
//                    personalLegendStr = [NSString stringWithFormat:@"%@\n(%.0f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_PERSONAL",nil,myLangBundle,nil), personalPercent];
//                }
//                else {
//                    personalLegendStr = [NSString stringWithFormat:@"%@\n(%.1f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_PERSONAL",nil,myLangBundle,nil), personalPercent];
//
//                }
//
//                if(sickPercent == 0 || sickPercent == 100) {
//                    sickLegendStr = [NSString stringWithFormat:@"%@\n(%.0f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SICK",nil,myLangBundle,nil), sickPercent];
//                }
//                else {
//                    sickLegendStr = [NSString stringWithFormat:@"%@\n(%.1f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SICK",nil,myLangBundle,nil), sickPercent];
//
//                }
//
//                if(eventPercent == 0 || eventPercent == 100) {
//                    eventLegendStr = [NSString stringWithFormat:@"%@\n(%.0f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_EVENT",nil,myLangBundle,nil), eventPercent];
//                }
//                else {
//                    eventLegendStr = [NSString stringWithFormat:@"%@\n(%.1f%%)",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_EVENT",nil,myLangBundle,nil), eventPercent];
//
//                }
                
//                [self.onTimeLabel setText:onTimeLegendStr];
//                [self.lateLabel setText:lateLegendStr];
//                [self.absenceLabel setText:absenceLegendStr];
//                [self.personalLabel setText:personalLegendStr];
//                [self.sickLabel setText:sickLegendStr];
//                [self.eventLabel setText:eventLegendStr];
                
                [self setupGraphData];
                [self.tableView reloadData];
            }
        }

    }];
}

#pragma mark - Dialog
- (void)showDialogSelectMonthWithMonthDict:(NSDictionary *)monthsDict  monthsTermDataKeys:(NSArray *)monthsTermDataKeys {
    
    if(dialogSelectMonth != nil && [dialogSelectMonth isDialogShowing]) {
        [dialogSelectMonth dismissDialog];
    }
    [dialogSelectMonth showDialogInView:self.parentViewController.view stringDataDict:monthsDict dataDictKeys:monthsTermDataKeys];
    
}

- (void)showDialogSelectYearWithYearArray:(NSArray *)yearArray {
    
    if(dialogSelectYear != nil && [dialogSelectYear isDialogShowing]) {
        [dialogSelectYear dismissDialog];
    }
    
    [dialogSelectYear showDialogInView:self.parentViewController.view dataArr:yearArray];
}

- (void)showCreditBureauGraphDialogWithTitle:(NSString *)title status:(ATTENDSTATUS)status times:(NSInteger)times totalTimes:(NSInteger)totalTimes {
    
    if(creditBureauGraphDialog != nil && [creditBureauGraphDialog isDialogShowing]) {
        [creditBureauGraphDialog dismissDialog];
    }

    [creditBureauGraphDialog showDialogInView:self.parentViewController.view title:title status:status times:times totalTimes:totalTimes];
}

#pragma mark - MKDropdownMenuDataSource
-(NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu {
    
    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component {
    
    return DateList.count;
}

#pragma mark - MKDropdownMenuDelegate
- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
  // [self declaredDialog];
    mKDropdownMenu = dropdownMenu;
   
    return 35;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {

    NSString *textString;
    
    textString = selected_calendar;

    NSAttributedString *attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:20], NSForegroundColorAttributeName: [UIColor blackColor] }];

    return attributes;
}

-(NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {

    NSString *textString;
    textString = [DateList objectAtIndex:row];
    NSAttributedString *attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:20], NSForegroundColorAttributeName: [UIColor blackColor] }];
    return attributes;
}

- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
    [self.delegate removeMKDropdownMenu:dropdownMenu];
    return NO;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        selected_calendar = [DateList objectAtIndex:row];
        if (row == 0) {
            [self doSelectDayCalendar:@"week"];
        }else if (row == 1){
            [self doSelectDayCalendar:@"month"];
        }else if(row == 2){
            [self doSelectDayCalendar:@"year"];
        }else if (row == 3){
            [self doShowDialogSpecifyDate];
        }
        [dropdownMenu reloadAllComponents];
    }
    delay(0.0, ^{
        [dropdownMenu closeAllComponentsAnimated:YES];
    });
}

- (void) doShowDialogSpecifyDate{
    if(dialogSpecifyDate != nil && [dialogSpecifyDate isDialogShowing]) {
        [dialogSpecifyDate dismissDialog];
    }
    else {
        dialogSpecifyDate = [specifyDateDialog alloc];
        dialogSpecifyDate.delegate = self;
    }
    
    UserViewController *rootController = (UserViewController*)[[(AppDelegate*)
                                                                [[UIApplication sharedApplication]delegate] window] rootViewController];
    [dialogSpecifyDate showDialogInView:rootController.view];
}

- (void) onAlertDialogConfirm:(NSDate *)startDate endDate:(NSDate *)endDate{
    [self updatePieChartDataWithStartDate:startDate endDate:endDate];
   
}



@end
