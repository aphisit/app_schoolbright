//
//  RPListPurchasingViewController.m
//  JabjaiApp
//
//  Created by Mac on 11/8/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "RPListPurchasingViewController.h"
#import "Utils.h"
#import "UserData.h"
@interface RPListPurchasingViewController ()

@end

@implementation RPListPurchasingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PURCHASING_PRI_REP_LIST", nil, [Utils getLanguage], nil);
    self.headerTotalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PURCHASING_PRI_REP_TATAL", nil, [Utils getLanguage], nil);
   
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self; 
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RPListPurchasingTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.shopNameLabel.text = self.dataListProduct.shopName;
    self.totalLabel.text = [@(self.dataListProduct.totalPrice)stringValue];
    
    NSDate *dateTime = [Utils parseServerDateStringToDate:self.dataListProduct.paymentDate];
    NSDateFormatter *dateFormatter = [Utils getDateFormatter];
    //[dateFormatter setDateFormat:@"HH:mm:ss"];
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [dateFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [dateFormatter setDateFormat:@"HH:mm"];
    }
    
    NSString *time = [dateFormatter stringFromDate:dateTime];
    self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@",[Utils getThaiDateFormatWithDate:dateTime],time];
    
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    [self.totalView layoutIfNeeded];
    CAGradientLayer  *totalViewGradient = [Utils getGradientColorStatus:@"green"];
    totalViewGradient.frame = self.totalView.bounds;
    [self.totalView.layer insertSublayer:totalViewGradient atIndex:0];
    self.totalView.layer.shadowRadius  = 1.5f;
    self.totalView.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    self.totalView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    self.totalView.layer.shadowOpacity = 0.9f;
    self.totalView.layer.masksToBounds = NO;
}



#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataListProduct.listProduct.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RPListPurchasingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    PurchasingListMenuModel *model = [self.dataListProduct.listProduct objectAtIndex:indexPath.row];
    cell.productNameLabel.text = [model productName];
    cell.amountLabel.text = [@([model productAmount])stringValue];
    cell.priceTotalLabel.text = [@([model productPrice] * [model productAmount])stringValue];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return 87;
    }else{
       return 150;
    }
    
}
- (IBAction)moveBackAction:(id)sender {
    ReportPurchasingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportPurchasingStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
