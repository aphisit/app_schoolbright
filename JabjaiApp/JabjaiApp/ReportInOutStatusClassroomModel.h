//
//  ReportInOutStatusClassroomModel.h
//  JabjaiApp
//
//  Created by toffee on 28/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutStatusClassroomModel : NSObject

@property (nonatomic) NSString *subLevelName;
@property (nonatomic) long long subLevelId;
@property (nonatomic) NSArray *logData;

- (void) setSubLevelName:(NSString * _Nonnull)subLevelName;
- (void) setSubLevelId:(long long)subLevelId;
- (void) setLogData:(NSArray * _Nonnull)logData;

- (NSString*) getSubLevelName;
- (long long) getSubLevelId;
- (NSArray*) getLogData;

@end

NS_ASSUME_NONNULL_END
