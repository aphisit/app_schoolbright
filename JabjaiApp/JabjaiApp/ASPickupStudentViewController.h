//
//  ASPickupStudentViewController.h
//  JabjaiApp
//
//  Created by Mac on 4/2/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import <CoreLocation/CoreLocation.h>
#import "CallASGetLocationOfSchoolAPI.h"
#import "CallASSubmitRequestGetStudentAPI.h"
#import "AlertDialogConfirm.h"
#import "ASCloseFuturePickupDialog.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"

NS_ASSUME_NONNULL_BEGIN
extern NSString *tokenStr;
@interface ASPickupStudentViewController : UIViewController <SlideMenuControllerDelegate,CLLocationManagerDelegate,CallASGetLocationOfSchoolAPIDelegate,CallASSubmitRequestGetStudentAPIDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>

//- (IBAction)openDrawer:(id)sender;
//- (IBAction)pickupAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *playFileGifImg;
@property (weak, nonatomic) IBOutlet UILabel *kilomateLabel;
@property (weak, nonatomic) IBOutlet UIButton *refreshBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *mapContentView;
@property (weak, nonatomic) IBOutlet UIView *conView;

- (IBAction)openDrawer:(id)sender;
- (IBAction)refreshAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
