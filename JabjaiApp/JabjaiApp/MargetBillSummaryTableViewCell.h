//
//  MargetBillSummaryTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 5/16/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MargetBillSummaryTableViewCellDelegate <NSObject>

@end
@interface MargetBillSummaryTableViewCell : UITableViewCell

@property (weak, nonatomic) id<MargetBillSummaryTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *listNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;


@end
