//
//  RefillMoneyViewController.m
//  JabjaiApp
//
//  Created by mac on 11/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "RefillMoneyViewController.h"
#import "RefillMoneyWebsiteViewController.h"
#import "UserData.h"
#import "UserInfoModel.h"
#import "UserInfoDBHelper.h"
#import "MultipleUserModel.h"
#import "Utils.h"
#import "APIURL.h"
#import "AlertDialog.h"
#import "UserInfoViewController.h"

//NSString *baht = @"";

@interface RefillMoneyViewController ()
{
    NSInteger connectCounter;
    AlertDialog *alertDialog;
    UnAuthorizeMenuDialog *unAuthorizeMenuDialog;
    NSString *baht;
    NSArray *moneyArray;
    NSMutableArray *authorizeSubMenuArray;
}

@property (strong, nonatomic) UserInfoModel *userInfoModel;
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (nonatomic, strong) RFSpecifyNumberMoneyViewController *rFSpecifyNumberMoneyViewController;
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end
static NSString *cellIdentifier = @"Cell";
@implementation RefillMoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    if ([UserData getUserType] != STUDENT) { [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//call to authorize menu
    }
    self.headLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_TOPUPMONEY",nil,[Utils getLanguage],nil);
    moneyArray =  [NSArray arrayWithObjects:@"50",@"100",@"300",@"500", nil];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RFSelectMonyTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
 
    baht = @"";
    self.dbHelper = [[UserInfoDBHelper alloc]init];
    connectCounter = 0;
    [self getUserInfoData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.nextButton layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.nextButton.bounds;
    [self.nextButton.layer insertSublayer:gradientNext atIndex:0];
}

#pragma mark - CallGetMenuListAPIDelegate
///check authorize menu
- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
    //[self showIndicator];
    // [self popUpLoaddin];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
    if ((resCode == 300 || resCode == 200) && success ) {
        NSLog(@"xx");
        authorizeSubMenuArray = [[NSMutableArray alloc] init];
        if (data != NULL) {
            
            for (int i = 0;i < data.count; i++) {
                NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
                for (int j = 0; j < subMenuArray.count; j++) {
                    NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
                    [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
                }
            }
        }
        NSString* notPermissionText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_UNAUTHORRIZE",nil,[Utils getLanguage],nil);
        NSString* pleaseContactText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_PLEASECONTACT",nil,[Utils getLanguage],nil);
        NSString *alertMessage = [NSString stringWithFormat:@"%@\n%@",notPermissionText,pleaseContactText];
        if (![authorizeSubMenuArray containsObject: @"15"]) {
            [self showAlertDialogAuthorizeMenu:alertMessage];
            NSLog(@"ไม่มีสิทธิ์");
        }
    }else{
        
        self.dbHelper = [[UserInfoDBHelper alloc] init];
        [self.dbHelper deleteUserAllWithMasterId:[UserData getMasterUserID]];
        
        // Logout
        [UserData setUserLogin:NO];
        [UserData saveMasterUserID:0];
        [UserData saveUserID:0];
        [UserData setUserType:USERTYPENULL];
        [UserData setAcademyType:ACADEMYTYPENULL];
        [UserData setFirstName:@""];
        [UserData setLastName:@""];
        [UserData setUserImage:@""];
        [UserData setGender:MALE];
        [UserData setClientToken:@""];
        LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
   
}

- (void)onAlertDialogClose {
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}


- (void)getUserInfoData{
    
    NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUserInfoData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                NSString *firstName,*lastName;
                connectCounter = 0;
                
                if(returnedArray.count != 0) {
                    
                    self.userInfoModel = [[UserInfoModel alloc] init];
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    float money = [[dataDict objectForKey:@"nMoney"] floatValue];
                    float creditLimits = [[dataDict objectForKey:@"nMax"] floatValue];
                    //NSString firstName =
                    
                    if(![[dataDict objectForKey:@"sName"] isKindOfClass:[NSNull class]]) {
                        firstName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sName"]];
                    }
                    else {
                        firstName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"sLastname"] isKindOfClass:[NSNull class]]) {
                        lastName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sLastname"]];
                    }
                    else {
                        lastName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    self.userInfoModel.money = money;
                    self.userInfoModel.creditLimits = creditLimits;
                    self.userInfoModel.firstName = firstName;
                    self.userInfoModel.lastName = lastName;
                    
                    [self updateUserData];
                    
                    
                }
                
            }
            
        }
        
    }];
    
}

- (void)updateUserData{
    
    if (self.userInfoModel != nil) {
        self.userNameLable.text = [NSString stringWithFormat:@"%@ %@",self.userInfoModel.firstName,self.userInfoModel.lastName];
        if ([[UserData getUserImage] isEqualToString: @""] ) {
            
            self.userImage.image = [UIImage imageNamed:@"ic_user_info"];
        }else{
            NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [UserData getUserImage]]];
            [self.userImage setImage:[UIImage imageWithData:imageData]];
            self.userImage.layer.cornerRadius = self.userImage.frame.size.height /2;
            self.userImage.layer.masksToBounds = YES;
            self.userImage.layer.borderWidth = 0;
        }
        if ([UserData getUserType] != STUDENT) {
           
            if ([Utils isIntegerWithDouble:self.userInfoModel.money]) {
                
                NSString *display = [NSNumberFormatter localizedStringFromNumber:@(self.userInfoModel.money)
                                                                     numberStyle:NSNumberFormatterDecimalStyle];
                
                self.summary.text = [[NSString alloc]initWithFormat:@"%@ %@ %@" ,NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_BALANCE",nil,[Utils getLanguage],nil), display,  NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_BAHT",nil,[Utils getLanguage],nil)];
            }
            else{
                self.summary.text = [[NSString alloc]initWithFormat:@"%@ %.2f %@",NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_BALANCE",nil,[Utils getLanguage],nil) , self.userInfoModel.money , NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_BAHT",nil,[Utils getLanguage],nil)];
            }
        }
        
        else{
            
            if ([Utils isIntegerWithDouble:self.userInfoModel.money]) {
                
                NSString *display = [NSNumberFormatter localizedStringFromNumber:@(self.userInfoModel.money)
                                                                     numberStyle:NSNumberFormatterDecimalStyle];
                self.summary.text = [[NSString alloc]initWithFormat:@"%@ %@ %@",NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_BALANCE",nil,[Utils getLanguage],nil) , display, NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_BAHT",nil,[Utils getLanguage],nil)];
            }
            else{
                self.summary.text = [[NSString alloc]initWithFormat:@"%@ %.2f %@",NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_BALANCE",nil,[Utils getLanguage],nil) , self.userInfoModel.money, NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_BAHT",nil,[Utils getLanguage],nil)];
            }
            
        }
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return moneyArray.count + 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RFSelectMonyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row < moneyArray.count) {
        cell.moneyLable.text = [NSString stringWithFormat:@"%@ %@ %@",NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_TOPUP",nil,[Utils getLanguage],nil),[moneyArray objectAtIndex:indexPath.row],NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_BAHT",nil,[Utils getLanguage],nil)];
    }else{
        cell.moneyLable.text = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_SPECIFY_AMOUNT",nil,[Utils getLanguage],nil)];
    }
    [cell.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.section1.layer setShadowOpacity:0.3];
    [cell.section1.layer setShadowRadius:3.0];
    [cell.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            /* Device is iPad */
             return 150;
    
        }else{
            /* Device is iPhone */
             return 90;
    
        }
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < moneyArray.count) {
        RefillMoneyWebsiteViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RefillMoneyWebsiteStoryboard"];
        viewController.numberMoney = [moneyArray objectAtIndex:indexPath.row];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }else{
        [self doSpecifyNumberMonney];
    }
   
}

//specify number money
- (void)doSpecifyNumberMonney{
    if(self.rFSpecifyNumberMoneyViewController != nil && [self.rFSpecifyNumberMoneyViewController isDialogShowing]) {
        [self.rFSpecifyNumberMoneyViewController dismissDialog];
    }
    else {
        self.rFSpecifyNumberMoneyViewController = [[RFSpecifyNumberMoneyViewController alloc] init];
        self.rFSpecifyNumberMoneyViewController.delegate = self;
    }
    [self.rFSpecifyNumberMoneyViewController showDialogInView:self.view];
}

- (void)showAlertDialogAuthorizeMenu:(NSString *)message {
    if(unAuthorizeMenuDialog != nil && [unAuthorizeMenuDialog isDialogShowing]) {
        [unAuthorizeMenuDialog dismissDialog];
    }
    else {
        unAuthorizeMenuDialog = [[UnAuthorizeMenuDialog alloc] init];
        unAuthorizeMenuDialog.delegate = self;
    }
    [unAuthorizeMenuDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

//response specify money
- (void) confirmNumberMoney:(RFSpecifyNumberMoneyViewController *)SpecifyNumberMoneyDialog amountMoney:(NSInteger)amountMoney{
    RefillMoneyWebsiteViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RefillMoneyWebsiteStoryboard"];
    viewController.numberMoney = [NSString stringWithFormat:@"%d",amountMoney];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
//    [self.revealViewController revealToggle:self.revealViewController];
    
}
@end
