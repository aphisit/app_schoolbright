//
//  CallGetYearOfAcademicResultsAPI.h
//  JabjaiApp
//
//  Created by toffee on 21/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARSelectTermYearModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallGetYearOfAcademicResultsAPI;
@protocol CallGetYearOfAcademicResultsAPIDelegate <NSObject>
@optional
- (void)callGetYearOfAcademicResultsAPI:( CallGetYearOfAcademicResultsAPI *)classObj data:(NSArray <ARSelectTermYearModel*> *)data success:(BOOL)success;
@end
@interface CallGetYearOfAcademicResultsAPI : NSObject
@property (nonatomic, weak) id<CallGetYearOfAcademicResultsAPIDelegate> delegate;
- (void)call:(long long)schoolId;
@end

NS_ASSUME_NONNULL_END
