//
//  CallConfirmPinAPI.m
//  JabjaiApp
//
//  Created by toffee on 6/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallConfirmPinAPI.h"
#import "APIURL.h"
#import "UserData.h"
#import <AFNetworking/AFNetworking.h>
@implementation CallConfirmPinAPI
-(void)call:(NSString *)jsonString {
    [self doConfirmPinCode:jsonString ];
    
}
- (void)doConfirmPinCode:(NSString*)jsonString{
    NSString *URLString = [APIURL getConfirmPinCodePOST];
    NSError* error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableLeaves error:&error];

    if(error != nil) {
        NSLog(@"%@", @"Parse JSON data failed");
    }
    else {
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"responseObject = %@", responseObject);
            NSString  *status = [responseObject objectForKey:@"resMessage"] ;
            if(self.delegate && [self.delegate respondsToSelector:@selector(callConfirmPinAPI:status:success:)]) {
                [self.delegate callConfirmPinAPI:self status:status success:YES];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callUpdateBehaviorScorePOSTAPI:response:success:)]) {
               [self.delegate callConfirmPinAPI:self status:nil success:NO];
            }
            
        }];
        
    }
}
@end
