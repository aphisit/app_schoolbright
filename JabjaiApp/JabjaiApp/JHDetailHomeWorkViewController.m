//
//  JHDetailHomeWorkViewController.m
//  JabjaiApp
//
//  Created by Mac on 4/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "JHDetailHomeWorkViewController.h"
#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"
@interface JHDetailHomeWorkViewController (){
    NSInteger connectCounter;
    ReportHomeWorkDetailModel *reportHomeworkDetailModel;
    NSMutableArray *imageArray,*fileArray,*typeFileArray;
    NSArray *returnedArray;
    UIView *view;
    NSInteger imgConstant,fileConstant;
}
@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) JHReadImageViewController *jHReadImageViewController;
@property (nonatomic, strong) JHReadFileViewController *jHReadFileViewController;
@end

@implementation JHDetailHomeWorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    imgConstant = self.highImageConstraint.constant;
    fileConstant = self.highFileConstraint.constant;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}
- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}

- (void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_HOMEWORK",nil,[Utils getLanguage],nil);
    self.headerSubjectLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_HOMEWORK_SUBJECT",nil,[Utils getLanguage],nil);
    self.headerSenderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_HOMEWORK_SENDER",nil,[Utils getLanguage],nil);
    self.headerStartDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_HOMEWORK_SUBMIT_DAY",nil,[Utils getLanguage],nil);
    
    self.headerEndDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_HOMEWORK_DAYSEND",nil,[Utils getLanguage],nil);
    self.headerDescriptionLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_HOMEWORK_DETAIL",nil,[Utils getLanguage],nil);
    self.headerAttachImageLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_HOMEWORK_ATTACTH_IMAGE",nil,[Utils getLanguage],nil);
    self.headerAttachFileLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_HOMEWORK_ATTACTH_FILE",nil,[Utils getLanguage],nil);

}

- (void)showDetailHomeWorkViewController:(UIView *)targetView homeworkID:(long long)homeworkID{
    view = targetView;
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
        [targetView addSubview:self.view];
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height-20)];
        [targetView addSubview:self.view];
    }

    // Initialize dialog view hide it
    [self.contentView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentView setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
    
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    imageArray = [[NSMutableArray alloc] init];
    fileArray = [[NSMutableArray alloc] init];
    typeFileArray = [[NSMutableArray alloc] init];
   
    [self.collecttionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.collecttionView.delegate = self;
    self.collecttionView.dataSource = self;
    
    [self.fileCollecttionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.fileCollecttionView.delegate = self;
    self.fileCollecttionView.dataSource = self;
    [self setLanguage];
    self.highImageConstraint.constant = imgConstant;
    self.highFileConstraint.constant = fileConstant;
    [self.collecttionView reloadData];
    [self.fileCollecttionView reloadData];
    [self getHomeWorkWithMessageID:homeworkID];
    [self getImageNewsWithMessageID:homeworkID];
    
}

-(void)removeDetailNewsViewController {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDetailHomeWorkViewController{
    [self removeDetailNewsViewController];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(closePageHomeWork)]) {
        [self.delegate closePageHomeWork];
    }
}
- (BOOL)isDialogShowing{
    return self.isShowing;
}


- (void)getImageNewsWithMessageID:(long long)messageID{
    [self showIndicator];
    [self showIndicatorFile];
    //add image to array
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getImageNews:userID idMessage:messageID schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
        }else{
            [self stopIndicator];
            [self stopIndicatorFile];
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            self->returnedArray = returnedData;
            if(self->returnedArray.count == 0){
                self->returnedArray = NULL;
//                [self.collecttionView reloadData];
//                [self.fileCollecttionView reloadData];
            }
            if (self->returnedArray != nil || self->returnedArray != NULL) {
                for (int i=0; i<self->returnedArray.count; i++) {
                    
                    if ([[[returnedArray objectAtIndex:i]objectForKey:@"filetype"] isEqualToString:@"image/jpeg"]) {
                        [self->imageArray addObject:[[self->returnedArray objectAtIndex:i]objectForKey:@"filename"] ];
                    }else{
                        NSString *fileType = [[self->returnedArray objectAtIndex:i]objectForKey:@"filetype"];
                        NSString *fileName = [[returnedArray objectAtIndex:i]objectForKey:@"filename"];
                        [typeFileArray addObject:fileType];
                        [self->fileArray addObject:fileName];
                    }
                }
                //[self.collecttionView layoutIfNeeded];
                //self.highImageConstraint.constant = 66; //66 is value constant of constant
                float wCollection = self.collecttionView.frame.size.width / ((self.collecttionView.frame.size.width/3)-10);
                float amountRow = self->imageArray.count / (int)wCollection;
                NSInteger modImg = self->imageArray.count % (int)wCollection ;
                
                if (modImg > 0) {
                     amountRow ++;
                }
                
                self.highImageConstraint.constant = self->imgConstant * amountRow+((amountRow*5)*2);

                //[self.fileCollectionView layoutIfNeeded];
                float wFileCollection = self.fileCollecttionView.frame.size.width / ((self.fileCollecttionView.frame.size.width/3)-10);
                float amountFileRow = self->fileArray.count / (int)wFileCollection;
                NSInteger modFile = self->fileArray.count % (int)wFileCollection ;

                if (modFile > 0) {
                    amountFileRow ++;
                }
                self.highFileConstraint.constant = self->fileConstant * amountFileRow + ((amountFileRow*5)*2);
               
                [self.collecttionView reloadData];
                [self.fileCollecttionView reloadData];
            }
        }
    }];
}

- (void)getHomeWorkWithMessageID:(long long)messageID{
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:messageID userID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getHomeWorkWithMessageID:messageID];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSArray *returnedArray = returnedData;
                connectCounter = 0;
            NSDictionary *dataDict = [NSDictionary alloc];
            NSString *subjectName,*teacherName,*endSendDate,*startSendDate,*detail,*orderDate;
                NSLog(@"returnedArrayCount = %d",returnedArray.count);
            
                dataDict = [returnedData objectForKey:@"homework"];
        
            if(![[returnedData objectForKey:@"dSend"] isKindOfClass:[NSNull class]]) {
                orderDate = [returnedData objectForKey:@"dSend"];
            }
            else {
                orderDate = [returnedData objectForKey:@""];
            }
            if (![dataDict isKindOfClass:[NSNull class]]) {
              
                if(![[dataDict objectForKey:@"planename"] isKindOfClass:[NSNull class]]) {
                    subjectName = [dataDict objectForKey:@"planename"];
                }
                else {
                    subjectName = [dataDict objectForKey:@""];
                }
                
                if(![[dataDict objectForKey:@"teachername"] isKindOfClass:[NSNull class]]) {
                    teacherName = [dataDict objectForKey:@"teachername"];
                }
                else {
                    teacherName = [dataDict objectForKey:@""];
                }
                
                if(![[dataDict objectForKey:@"daystart"] isKindOfClass:[NSNull class]]) {
                    startSendDate = [dataDict objectForKey:@"daystart"];
                }
                else {
                    startSendDate = [dataDict objectForKey:@""];
                }
                
                if(![[dataDict objectForKey:@"dayend"] isKindOfClass:[NSNull class]]) {
                    endSendDate = [dataDict objectForKey:@"dayend"];
                }
                else {
                    endSendDate = [dataDict objectForKey:@""];
                }
                
                if(![[dataDict objectForKey:@"detail"] isKindOfClass:[NSNull class]]) {
                    detail = [dataDict objectForKey:@"detail"];
                }
                else {
                    detail = [dataDict objectForKey:@""];
                }
            }
            NSDateFormatter *formatter = [Utils getDateFormatter];
            [formatter setDateFormat:[Utils getXMLDateFormat]];
            
            NSDateFormatter *formatter2 = [Utils getDateFormatter];
            [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
            
            NSDate *orderHomeworkEndDate;
            orderHomeworkEndDate = [formatter2 dateFromString:endSendDate];
            
            NSDate *sendHomeworkStartDate;
            sendHomeworkStartDate = [formatter2 dateFromString:startSendDate];
            
            NSDate *sendHomeworkOrderDate;
            sendHomeworkOrderDate = [formatter dateFromString:orderDate];
            
            ReportHomeWorkDetailModel *model = [[ReportHomeWorkDetailModel alloc] init];
            model.subjectName = subjectName;
            model.teacherName = teacherName;
            model.dateEndSend = orderHomeworkEndDate;
            model.dateStartSend = sendHomeworkStartDate;
            model.dateOrder = sendHomeworkOrderDate;
            model.detailHomework = detail;
            reportHomeworkDetailModel = model;
            [self performData:model];
        }
    }];
}

- (void)performData:(ReportHomeWorkDetailModel*)model {
    if(reportHomeworkDetailModel != nil) {
        self.subjectLabel.text = model.subjectName;
        self.senderLabel.text = model.teacherName;
        self.detailHomework.text = model.detailHomework;
        self.orderDateLabel.text = [self.dateFormatter stringFromDate:model.dateOrder];
        self.startEndDateLabel.text = [NSString stringWithFormat:@"%@ - %@",[self.dateFormatter stringFromDate:model.dateStartSend],[self.dateFormatter stringFromDate:model.dateEndSend]];
        }
    else {
        [self clearDisplay];
    }
}
- (void)clearDisplay {
}

#pragma mark - CollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (collectionView == self.collecttionView) {
        if (imageArray.count < 1) {
            self.collecttionView.hidden = YES;
            return 0;
        }else{
            self.collecttionView.hidden = NO;
            return imageArray.count;
        }
        
    }else{
        if (typeFileArray.count < 1) {
            self.fileCollecttionView.hidden = YES;
            return 0;
        }else{
            self.fileCollecttionView.hidden = NO;
            return typeFileArray.count;
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MessageInboxScrollableTextDialogCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (collectionView == self.collecttionView) {
        NSData *imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [imageArray objectAtIndex:indexPath.row]]];
        cell.imageNews.tag = indexPath.row;
        cell.images.image = [UIImage imageWithData:imageData];
        [cell.imageNews addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        cell.imageNews.tag = indexPath.row;
        
        if ([[typeFileArray objectAtIndex:indexPath.row] isEqualToString:@"PDF"]) {
            [cell.images setImage:[UIImage imageNamed:@"ic_pdf"]];
        }else if ([[typeFileArray objectAtIndex:indexPath.row] isEqualToString:@"MS Word"]){
             [cell.images setImage:[UIImage imageNamed:@"ic_word"]];
        }else{
            [cell.images setImage:[UIImage imageNamed:@"ic_excel"]];
        }
        [cell.imageNews addTarget:self action:@selector(selectFileTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
     return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize defaultSize;
    if (collectionView == self.collecttionView) {
        defaultSize = CGSizeMake((self.collecttionView.frame.size.width/3)-10, (imgConstant));
    }else{
        defaultSize = CGSizeMake((self.fileCollecttionView.frame.size.width/3)-10, (fileConstant));
    }
    return defaultSize;
}

#pragma mark - JHReadImageViewController
- (void)okButtonTapped:(UIButton *)sender  {
//    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(openImageOfHomeWork:imageArray:)]) {
//        [self.delegate openImageOfHomeWork:self imageArray:imageArray];
//    }
    self.jHReadImageViewController = [[JHReadImageViewController alloc] init];
    [self.jHReadImageViewController showPagesReadImageInView:view ImageArray:imageArray];
}
#pragma mark - JHReadFileViewController
- (void)selectFileTapped:(UIButton *)sender  {
//    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(openFileOfHomeWork:pathFile:)]) {
//        [self.delegate openFileOfHomeWork:self pathFile:[fileArray objectAtIndex:sender.tag]];
//    }
    self.jHReadFileViewController = [[JHReadFileViewController alloc] init];
    [self.jHReadFileViewController showPagesReadFileInView:view partFile:[fileArray objectAtIndex:sender.tag]];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (void)showIndicatorFile {
    // Show the indicator
    if(![self.indicatorFile isAnimating]) {
        [self.indicatorFile startAnimating];
    }
}

- (void)stopIndicatorFile {
    [self.indicatorFile stopAnimating];
}

- (IBAction)moveBack:(id)sender {
    [self dismissDetailHomeWorkViewController];
}
@end
