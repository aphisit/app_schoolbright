//
//  BSImageCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 10/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol BSImageCollectionViewCellDelegate <NSObject>

- (void)removeImage:(NSInteger)index collectionView:(UICollectionView*)collectiobView;

@end


@interface BSImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) id<BSImageCollectionViewCellDelegate> delegate;
@property (weak, nonatomic) UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
- (IBAction)removeImageAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
