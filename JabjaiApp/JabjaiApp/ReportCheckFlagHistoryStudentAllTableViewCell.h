//
//  ReportCheckFlagHistoryStudentAllTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 7/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportCheckFlagHistoryStudentAllTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *statusBtn;
@property (weak, nonatomic) IBOutlet UILabel *runNumberLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundColorView;


- (void)updateStatus:(int)status;
@end

NS_ASSUME_NONNULL_END
