//
//  CallTAGetStatusCalendaAPI.m
//  JabjaiApp
//
//  Created by Mac on 18/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallTAGetStatusCalendaAPI.h"
#import "Utils.h"
#import "APIURL.h"
@interface CallTAGetStatusCalendaAPI(){
    NSInteger connectCounter;
    NSMutableArray<TAStatusCalendaModel *> *dataArray;
}
@end
@implementation CallTAGetStatusCalendaAPI{
    
}
- (void)call:(long long)userID day:(NSDate *)day subjectID:(long long)subjectID schoolID:(long long)schoolID classroomID:(long long)classroomID{
    connectCounter = 0;
    [self getStatusCalendar:userID day:day subjectID:subjectID schoolID:schoolID classroomID:classroomID];
}
- (void)getStatusCalendar:(long long)userID day:(NSDate *)day subjectID:(long long)subjectID schoolID:(long long)schoolID classroomID:(long long)classroomID{
    NSString *URLString = [APIURL getTAStatusCalendarOfReport:userID day:[Utils dateToServerDateFormat:day] subjectID:subjectID schoolID:schoolID classroomID:classroomID];
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

        BOOL isFail = NO;

        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }

            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;

                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getStatusCalendar:userID day:day subjectID:subjectID schoolID:schoolID classroomID:classroomID];
            }
            else {

                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);

                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;

                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getStatusCalendar:userID day:day subjectID:subjectID schoolID:schoolID classroomID:classroomID];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }

            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {

                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;

                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getStatusCalendar:userID day:day subjectID:subjectID schoolID:schoolID classroomID:classroomID];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
            }
            else {

                NSArray *returnedArray = returnedData;
                self->connectCounter = 0;
                if(self->dataArray != nil) {
                    [self->dataArray removeAllObjects];
                    self->dataArray = nil;
                }
                self->dataArray = [[NSMutableArray alloc] init];
                
                for (int i = 0; i < returnedArray.count; i++) {
                    NSMutableString *day;
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    long long schoolID = [[dataDict objectForKey:@"SchoolID"] longLongValue];
                    if(![[dataDict objectForKey:@"daybuy"] isKindOfClass:[NSNull class]]) {
                        day = [[NSMutableString alloc] initWithFormat:@"%@",[dataDict objectForKey:@"daybuy"]];
                    }else{
                        day = [[NSMutableString alloc] initWithFormat:@""];
                    }
                    
                    TAStatusCalendaModel *model = [[TAStatusCalendaModel alloc] init];
                    [model setDay:day];
                    [model setSchoolID:schoolID];
                    [self->dataArray addObject:model];
                }
               
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTAGetStatusCalendaAPI:data:success:)]) {

                    [self.delegate callTAGetStatusCalendaAPI:self data:self->dataArray success:YES];
                }
            }

        }

        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callTAGetStatusCalendaAPI:data:success:)]) {

                [self.delegate callTAGetStatusCalendaAPI:self data:self->dataArray success:NO];
            }
        }

    }];
}
@end
