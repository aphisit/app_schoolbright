//
//  BSSummaryImageCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 16/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BSSummaryImageCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *behaviorScoreImage;

@end

NS_ASSUME_NONNULL_END
