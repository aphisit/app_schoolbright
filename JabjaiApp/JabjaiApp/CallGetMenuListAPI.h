//
//  CallGetMenuListAPI.h
//  JabjaiApp
//
//  Created by toffee on 8/17/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CallGetMenuListAPI;
@protocol CallGetMenuListAPIDelegate <NSObject>
@optional
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray*)data resCode:(NSInteger)resCode success:(BOOL)success;

@end

@interface CallGetMenuListAPI : NSObject

    @property (nonatomic, weak) id<CallGetMenuListAPIDelegate> delegate;
- (void)call:(long long)userId clientToken:(NSString*)clientToken schoolid:(long long)schoolid;

@end
