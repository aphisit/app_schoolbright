//
//  CFSCalendarScopeHandle.h
//  CFSCalendar
//
//  Created by dingwenchao on 4/29/16.
//  Copyright © 2016 wenchaoios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CFSCalendar;

@interface CFSCalendarScopeHandle : UIView <UIGestureRecognizerDelegate>

@property (weak, nonatomic) UIPanGestureRecognizer *panGesture;
@property (weak, nonatomic) CFSCalendar *calendar;

- (void)handlePan:(id)sender;

@end
