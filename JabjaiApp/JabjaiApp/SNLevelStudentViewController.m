//
//  SNLevelStudentViewController.m
//  JabjaiApp
//
//  Created by toffee on 8/28/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNLevelStudentViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "SNClassLevelViewController.h"
#import "SNSelectTheaCherViewController.h"
#import "SNTimeOutViewController.h"
#import "SNClassRoomIndividualViewController.h"
#import "SNClassLevelGroupViewController.h"
#import "SNSelectPersonnelViewController.h"
#import "SNAddUserGroupViewController.h"
#import "APIURL.h"
#import "Utils.h"
#import "UserInfoViewController.h"
@interface SNLevelStudentViewController (){

    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    NSMutableArray *teacherId;
    NSMutableArray *employeesid;
    NSMutableArray *personnelId;
    NSMutableArray *authorizeSubMenuArray;
    NSMutableArray *userAcceptArray;
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    UnAuthorizeMenuDialog *unAuthorizeMenuDialog;
    NSArray *category,*recipient;
    NSBundle *myLangBundle;

  
}
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
//@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;

@end
static NSString *classLevelRequestCode = @"group";
static NSString *classroomRequestCode = @"oneUser";

@implementation SNLevelStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    if ([UserData getUserType] != STUDENT) { [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//call to authorize menu
    }
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    
    userAcceptArray = [[NSMutableArray alloc] init];
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }

    
    // Do any additional setup after loading the view.
    self.classLevelTextField.delegate = self;
    self.userTextField.delegate = self;
    if(_classLevelArray != nil) {
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classLevelArray.count; i++) {
            SchoolLevelModel *model = [_classLevelArray objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:_selectedClassLevelIndex] getClassLevelName];

    }
    else {
        _selectedClassLevelIndex = -1;
        _selectedClassLevelIndex = -1;

    }
    [self setLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.nextButton layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.nextButton.bounds;
    [self.nextButton.layer insertSublayer:gradientNext atIndex:0];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section2.layer setShadowOpacity:0.3];
    [self.section2.layer setShadowRadius:3.0];
    [self.section2.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

-(void)setLanguage{
   
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS",nil,myLangBundle,nil);
    self.headerDeliveryLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_DELIVERY",nil,myLangBundle,nil);
    self.headerRecipientLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_RECIPIENT",nil,myLangBundle,nil);
    self.classLevelTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_SENDNEWS_DELIVERY",nil,myLangBundle,nil);
    self.userTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_SENDNEWS_RECIPIENT",nil,myLangBundle,nil);
     [self.nextButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

#pragma mark - CallGetMenuListAPIDelegate
///check authorize menu
- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
   // [self showIndicator];
    // [self popUpLoaddin];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
    
    if ((resCode == 300 || resCode == 200) && success ) {
        NSLog(@"xx");
        authorizeSubMenuArray = [[NSMutableArray alloc] init];
        if (data != NULL) {
            for (int i = 0;i < data.count; i++) {
                NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
                for (int j = 0; j < subMenuArray.count; j++) {
                    NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
                    [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
                }
            }
        }
        NSString* notPermissionText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_UNAUTHORRIZE",nil,[Utils getLanguage],nil);
        NSString* pleaseContactText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_PLEASECONTACT",nil,[Utils getLanguage],nil);
        NSString *alertMessage = [NSString stringWithFormat:@"%@\n%@",notPermissionText,pleaseContactText];
        
        if (![authorizeSubMenuArray containsObject: @"13"]) {
            [self showAlertDialogAuthorizeMenu:alertMessage];
            NSLog(@"ไม่มีสิทธิ์");
        }
    }else{
        
        self.dbHelper = [[UserInfoDBHelper alloc] init];
        [self.dbHelper deleteUserAllWithMasterId:[UserData getMasterUserID]];
        
        // Logout
        [UserData setUserLogin:NO];
        [UserData saveMasterUserID:0];
        [UserData saveUserID:0];
        [UserData setUserType:USERTYPENULL];
        [UserData setAcademyType:ACADEMYTYPENULL];
        [UserData setFirstName:@""];
        [UserData setLastName:@""];
        [UserData setUserImage:@""];
        [UserData setGender:MALE];
        [UserData setClientToken:@""];
        LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    
   
}

- (void)onAlertDialogClose {
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}



- (IBAction)openDrawer:(id)sender {
//    [self.revealViewController revealToggle:self.revealViewController];
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}

- (IBAction)actionNext:(id)sender {
    if([self validateData]) {
        [self gotoSelectStudentsPage];
    }

    
}

- (IBAction)selectMenu:(id)sender {
    SNAddUserGroupViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNAddUserGroupStoryBoard"];
   
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
   category = [NSArray arrayWithObjects:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_GROUP",nil,myLangBundle,nil),NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_INDIVIDUAL",nil,myLangBundle,nil),nil];
  
    
    if(textField.tag == 1) { // User press class level textfield
        NSLog(@"Level = %@", category);
        if(category != nil && category.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:category];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        
        if ([self.classLevelTextField.text  isEqual: NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_GROUP",nil,myLangBundle,nil)]) {
            recipient =[NSArray arrayWithObjects:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL",nil,myLangBundle,nil),
                NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_EMPLOY_TEACHER",nil,myLangBundle,nil),
                NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_EMPLOYEES",nil,myLangBundle,nil),
                NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_TEACHERS",nil,myLangBundle,nil),
                NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_STUDENTS",nil,myLangBundle,nil),
                NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_STUDENT_BY_CLASS",nil,myLangBundle,nil),nil];
            
            if(self.classLevelTextField.text.length == 0) {
                NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_SEND_FORMAT",nil,myLangBundle,nil);
                [self showAlertDialogWithMessage:alertMessage];
            }
            else if(recipient != nil && recipient.count > 0) {
                [self showTableListDialogWithRequestCode:classroomRequestCode data:recipient];
            }

            
        }else{
            recipient =[NSArray arrayWithObjects:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_TEACHERS",nil,myLangBundle,nil),NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_EMPLOYEES",nil,myLangBundle,nil),NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_STUDENTS",nil,myLangBundle,nil),nil];
            if(self.classLevelTextField.text.length == 0) {
                NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_SEND_FORMAT",nil,myLangBundle,nil);
                [self showAlertDialogWithMessage:alertMessage];
            }
            else if(recipient != nil && recipient.count > 0) {
                [self showTableListDialogWithRequestCode:classroomRequestCode data:recipient];
            }

        }
        
        
        
        
        return NO;
    }
    
    return YES;
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}
    
- (void)showAlertDialogAuthorizeMenu:(NSString *)message {
    if(unAuthorizeMenuDialog != nil && [unAuthorizeMenuDialog isDialogShowing]) {
            [unAuthorizeMenuDialog dismissDialog];
    }
    else {
        unAuthorizeMenuDialog = [[UnAuthorizeMenuDialog alloc] init];
        unAuthorizeMenuDialog.delegate = self;
    }
        [unAuthorizeMenuDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
        
       
        
        self.classLevelTextField.text = [category objectAtIndex:index];
        
        
    }
    if([requestCode isEqualToString:classroomRequestCode]) {
        
        NSLog(@"classroomRequestCode = %@" , classroomRequestCode );
        NSLog(@"requestCode = %@" , requestCode );
        
       
        self.userTextField.text = [recipient objectAtIndex:index];
        
        
    }

    
}

#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_SEND_FORMAT",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.userTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_RECIPIENT",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}

- (void)gotoSelectStudentsPage {
    if([self.userTextField.text  isEqual: NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL",nil,myLangBundle,nil)]){
        SNTimeOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        userAcceptArray[0] = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL",nil,myLangBundle,nil);
        viewController.sendType = 0;
        viewController.sendGroup = 0;
        viewController.newsType = 1;
        viewController.userAcceptArray = userAcceptArray;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    }
    else if([self.userTextField.text  isEqual: NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_EMPLOY_TEACHER",nil,myLangBundle,nil)]){
        SNTimeOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        userAcceptArray[0] = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_EMPLOY_TEACHER",nil,myLangBundle,nil);
        viewController.sendType = 0;
        viewController.sendGroup = 2;
        viewController.newsType = 1;
        viewController.userAcceptArray = userAcceptArray;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    }
    else if([self.userTextField.text  isEqual: NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_TEACHERS",nil,myLangBundle,nil)]){
        SNTimeOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        userAcceptArray[0] = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_TEACHERS",nil,myLangBundle,nil);
        viewController.sendType = 0;
        viewController.sendGroup = 5;
        viewController.newsType = 1;
        viewController.userAcceptArray = userAcceptArray;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    }
    else if([self.userTextField.text  isEqual: NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_EMPLOYEES",nil,myLangBundle,nil)]){
        SNTimeOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        userAcceptArray[0] = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_EMPLOYEES",nil,myLangBundle,nil);
        viewController.sendType = 0;
        viewController.sendGroup = 4;
        viewController.newsType = 1;
        viewController.userAcceptArray = userAcceptArray;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else if([self.userTextField.text  isEqual: NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_STUDENTS",nil,myLangBundle,nil)]){
        SNTimeOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        userAcceptArray[0] = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_ALL_STUDENTS",nil,myLangBundle,nil);
        viewController.sendType = 0;
        viewController.sendGroup = 1;
        viewController.newsType = 1;
        viewController.userAcceptArray = userAcceptArray;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else if([self.userTextField.text  isEqual: NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_STUDENT_BY_CLASS",nil,myLangBundle,nil)]){
        SNClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"snClassLevelViewController"];
        viewController.sendType = 0;
        viewController.sendGroup = 3;
        viewController.newsType = 1;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
//รายบุคคล
    else if([self.userTextField.text  isEqual: NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_STUDENTS",nil,myLangBundle,nil)]){
        SNClassLevelGroupViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNClassLevelGroupStoryboard"];
        viewController.sendtype = 1;
        viewController.newstype = 1;
        viewController.sendgroup = 0;
        [self.slideMenuController changeMainViewController:viewController close:YES];
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else if([self.userTextField.text  isEqual: NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_TEACHERS",nil,myLangBundle,nil)]){
        SNSelectTheaCherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNSelectTheacherStoryboard"];
        viewController.sendType = 1;
        viewController.newsType = 1;
        viewController.sendGroup = 0;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    
    
    else if([self.userTextField.text  isEqual: NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_EMPLOYEES",nil,myLangBundle,nil)]){
        SNSelectPersonnelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNSelectPersonnelStoryboard"];
        viewController.sendType = 1;
        viewController.newsType = 1;
        viewController.sendGroup = 0;

        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    
    

   
}






@end
