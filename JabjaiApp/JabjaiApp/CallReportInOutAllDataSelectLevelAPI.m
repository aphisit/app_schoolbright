//
//  CallReportInOutAllDataSelectLevelAPI.m
//  JabjaiApp
//
//  Created by toffee on 14/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallReportInOutAllDataSelectLevelAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallReportInOutAllDataSelectLevelAPI () {
    NSInteger connectCounter;
    NSMutableArray <ReportInOutAllDataSelectLevelModel *> *dataLevelArray;
    
}

@end
@implementation CallReportInOutAllDataSelectLevelAPI
- (void) call:(long long)schoolId date:(NSString *)date{
    [self getDataLevel:schoolId date:date];
}

- (void) getDataLevel:(long long)schoolId date:(NSString *)date{
    NSString *URLString = [APIURL getReportDataLevel:schoolId date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataLevel:schoolId date:date];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataLevel:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataLevel:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
//                NSInteger status0,status1,status2,status3,status4,status5,status6;
//                NSArray* data;
//                NSDictionary *statusAllDict;
                NSString *levelName;
                
                if(dataLevelArray != nil) {
                    [dataLevelArray removeAllObjects];
                    dataLevelArray = nil;
                }
                
                dataLevelArray = [[NSMutableArray alloc] init];
                for (int i = 0; i < returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long levelId = [[dataDict objectForKey:@"level_Id"] longLongValue];
                    NSInteger amountStudent = [[dataDict objectForKey:@"student_Number"] integerValue];
                    NSDictionary *statusArray = [dataDict objectForKey:@"statuses"];
                    
                    if(![[dataDict objectForKey:@"level_Name"] isKindOfClass:[NSNull class]]) {
                        
                        levelName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"level_Name"]];
                    }
                    else {
                        levelName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) levelName);
                    
                    ReportInOutAllDataSelectLevelModel *model = [[ReportInOutAllDataSelectLevelModel alloc]init];
                    [model setLevelName:levelName];
                    [model setLevelId:levelId];
                    [model setAmountStudent:amountStudent];
                    [model setStatus:statusArray];
                    
                    [dataLevelArray addObject:model];
                }
                
                

                connectCounter = 0;
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportInOutAllDataSelectLevelAPI:statusAllArray:success:)]) {
                    [self.delegate callReportInOutAllDataSelectLevelAPI:self statusAllArray:dataLevelArray success:YES];
                }
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportInOutAllDataSelectLevelAPI:statusAllArray:success:)]) {
                    [self.delegate callReportInOutAllDataSelectLevelAPI:self statusAllArray:dataLevelArray success:NO];
                }
            }
            
        }
        
    }];
}
@end
