//
//  ARSelectTermViewController.m
//  JabjaiApp
//
//  Created by toffee on 20/2/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "ARSelectTermViewController.h"
#import "Utils.h"
#import "UserData.h"
@interface ARSelectTermViewController (){
    TableListDialog *tableListDialog;
   // NSMutableArray *yearNumberArray,*yearIdArray;
    //NSArray  *termArray;
    //NSString *termStr, *yearId;
    BOOL permission;
}
@property (strong, nonatomic) CallGetYearOfAcademicResultsAPI *callGetYearOfAcademicResultsAPI;
@property (strong, nonatomic) CallGetTermOfAcademicResultsAPI *callGetTermOfAcademicResultsAPI;
@property (strong, nonatomic) CallGetPermissionAcademicResultsAPI *callGetPermissionAcademicResultsAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@property (strong, nonatomic) AlertDialog *alertDialog;
@end
static NSString *yearRequestCode = @"yearRequestCode";
static NSString *termRequestCode = @"termRequestCode";
@implementation ARSelectTermViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_RESULT",nil,[Utils getLanguage],nil);
    self.headerSchoolYearLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_SCHOOL_YEAR",nil,[Utils getLanguage],nil);
    self.headerSemesterLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ACADEMIC_SEMESTER",nil,[Utils getLanguage],nil);
    self.yearTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_ACADEMIC_SELECT_YEAR",nil,[Utils getLanguage],nil);
    self.termTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_ACADEMIC_SEMESTER",nil,[Utils getLanguage],nil);
    [self.nextBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_ACADEMIC_NEXT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    
    self.yearTextField.delegate = self;
    self.termTextField.delegate = self;
    [self getStudentPermission:[UserData getSchoolId] studentId:[UserData getUserID]];
    if (!([_termStr isEqualToString:@""]||_termStr == nil) && !([_yearStr isEqualToString:@""] || _yearStr == nil)) {
        self.yearTextField.text = self.yearStr;
        self.termTextField.text = self.termStr;
    }
    
    if (self.yearNumberArray == nil && self.yearIdArray == nil) {
        [self getYearOfAcademicResults:[UserData getSchoolId]];
        self.yearNumberArray = [[NSMutableArray alloc] init];
        self.yearIdArray = [[NSMutableArray alloc] init];
    }
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout {
    //set color header
    CAGradientLayer *gradientOrange, *gradientGreen;
    [self.headerView layoutIfNeeded];
    gradientOrange = [Utils getGradientColorHeader];
    gradientOrange.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientOrange atIndex:0];
    [self.nextBtn layoutIfNeeded];
    gradientGreen = [Utils getGradientColorNextAtion];
    gradientGreen.frame = self.nextBtn.bounds;
    [self.nextBtn.layer insertSublayer:gradientGreen atIndex:0];
}

#pragma mark - CallGetPermissionAcademicResultsAPI
-(void)getStudentPermission:(long long)schoolId studentId:(long long)studentId{
    NSLog(@"%@",[UserData getClientToken]);
    if(self.callGetPermissionAcademicResultsAPI != nil) {
             self.callGetPermissionAcademicResultsAPI = nil;
    }
    self.callGetPermissionAcademicResultsAPI = [[CallGetPermissionAcademicResultsAPI alloc] init];
    self.callGetPermissionAcademicResultsAPI.delegate = self;
    [self.callGetPermissionAcademicResultsAPI call:schoolId studentId:studentId] ;
}
-(void)callGetPermissionAcademicResultsAPI:(CallGetPermissionAcademicResultsAPI *)classObj data:(BOOL)data success:(BOOL)success{
    permission = data;
    if (permission == NO && success) {
        [self showAlertDialogWithTitle:@"แจ้งเตือน" message:@"ยังไม่มีการแจ้งผลการเรียน"];
    }
}

#pragma mark - CallGetYearOfAcademicResultsAPI
-(void)getYearOfAcademicResults:(long long)schoolId{
    if(self.callGetYearOfAcademicResultsAPI != nil) {
          self.callGetYearOfAcademicResultsAPI = nil;
    }
      self.callGetYearOfAcademicResultsAPI = [[CallGetYearOfAcademicResultsAPI alloc] init];
      self.callGetYearOfAcademicResultsAPI.delegate = self;
      [self.callGetYearOfAcademicResultsAPI call:schoolId];
}
-(void)callGetYearOfAcademicResultsAPI:(CallGetYearOfAcademicResultsAPI *)classObj data:(NSArray<ARSelectTermYearModel *> *)data success:(BOOL)success{
    if (data != nil && success) {
        
        for (int i = 0; i < data.count; i++) {
            ARSelectTermYearModel *model = [[ARSelectTermYearModel alloc] init];
            model = [data objectAtIndex:i];
            [self.yearNumberArray addObject:[NSString stringWithFormat:@"%d",[model getYearNumber]]];
            [self.yearIdArray addObject:[NSString stringWithFormat:@"%d",[model getYearId]]];
        }
    }
}

#pragma mark - CallGetTermOfAcademicResultsAPI
-(void)getTermOfAcademicResults:(long long)schoolId nYear:(NSInteger)nYear studentId:(long long)studentId{
    if(self.callGetTermOfAcademicResultsAPI != nil) {
          self.callGetTermOfAcademicResultsAPI = nil;
    }
      self.callGetTermOfAcademicResultsAPI = [[CallGetTermOfAcademicResultsAPI alloc] init];
      self.callGetTermOfAcademicResultsAPI.delegate = self;
      [self.callGetTermOfAcademicResultsAPI call:schoolId nYear:nYear studentId:studentId];
}
-(void)callGetTermOfAcademicResultsAPI:(CallGetTermOfAcademicResultsAPI *)classObj data:(NSArray *)data success:(BOOL)success{
    NSLog(@"xxx");
    if (data != nil && success) {
        self.termArray = data;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField.tag == 1) {
        if (permission == NO) {
            [self showAlertDialogWithTitle:@"แจ้งเตือน" message:@"ยังไม่มีการแจ้งผลการเรียน"];
        }else{
            
            [self showTableListDialogWithRequestCode:yearRequestCode data:self.yearNumberArray];
        }
        return NO;
    }else{
        if (permission == NO) {
            [self showAlertDialogWithTitle:@"แจ้งเตือน" message:@"ยังไม่มีการแจ้งผลการเรียน"];
        }else{
            if(self.yearTextField.text.length == 0) {
                [self showAlertDialogWithTitle:@"แจ้งเตือน" message:@"กรุณาเลือกปีการศึกษา"];
            }else{
                [self showTableListDialogWithRequestCode:termRequestCode data:self.termArray];
            }
            
        }
        
        return NO;
    }
    return YES;
}

#pragma mark - TableViewDialogDelegate
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index{
    if ([requestCode isEqualToString:yearRequestCode]) {
        self.yearStr = self.yearNumberArray[index];
        self.yearId = self.yearIdArray[index];
        self.yearTextField.text = [self.yearNumberArray objectAtIndex:index];
        [self getTermOfAcademicResults:[UserData getSchoolId] nYear:[self.yearIdArray[index]integerValue] studentId:[UserData getUserID]];
    }else{
        self.termStr = [self.termArray objectAtIndex:index];
        self.termTextField.text = [self.termArray objectAtIndex:index];
    }
    
}
#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    if(self.alertDialog == nil) {
        self.alertDialog = [[AlertDialog alloc] init];
        //self.alertDialog.delegate = self;
    }
    if(self.alertDialog != nil && [self.alertDialog isDialogShowing]) {
        [self.alertDialog dismissDialog];
    }
    [self.alertDialog showDialogInView:self.view title:title message:message];
}

- (BOOL)validateData {
    
    if(self.yearTextField.text.length == 0) {
        [self showAlertDialogWithTitle:@"แจ้งเตือน" message:NSLocalizedStringFromTableInBundle(@"DIALOG_ACADEMIC_SELECT_YEAR",nil,[Utils getLanguage],nil)];
        return NO;
    }
    else if(self.termTextField.text.length == 0) {
        [self showAlertDialogWithTitle:@"แจ้งเตือน" message:NSLocalizedStringFromTableInBundle(@"DIALOG_ACADEMIC_SEMESTER",nil,[Utils getLanguage],nil)];
        return NO;
    }
    return YES;
}

- (IBAction)openDarwerAction:(id)sender {
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
}

- (IBAction)nextAction:(id)sender {
    if (permission == YES) {
        if ([self validateData]) {
            ARDetailAcademicResultsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ARDetailAcademicResultsStoryboard"];
            viewController.termStr = self.termStr;
            viewController.yearId = self.yearId;
            viewController.yearStr = self.yearStr;
            viewController.termArray = self.termArray;
            viewController.yearIdArray = self.yearIdArray;
            viewController.yearNumberArray = self.yearNumberArray;
            [self.slideMenuController changeMainViewController:viewController close:YES];
        }
       
    }else{
        [self showAlertDialogWithTitle:@"แจ้งเตือน" message:@"ยังไม่มีการแจ้งผลการเรียน"];
    }
    
}
@end
