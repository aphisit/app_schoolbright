//
//  BSSummaryUserTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 16/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BSSummaryUserTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageUser;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;

@end

NS_ASSUME_NONNULL_END
