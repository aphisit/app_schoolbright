//
//  CFSCalendarAppearance.h
//  Pods
//
//  Created by DingWenchao on 6/29/15.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//
//  https://github.com/WenchaoD
//

#import "CFSCalendarConstance.h"

@class CFSCalendar;

typedef NS_ENUM(NSInteger, CFSCalendarCellState) {
    CFSCalendarCellStateNormal      = 0,
    CFSCalendarCellStateSelected    = 1,
    CFSCalendarCellStatePlaceholder = 1 << 1,
    CFSCalendarCellStateDisabled    = 1 << 2,
    CFSCalendarCellStateToday       = 1 << 3,
    CFSCalendarCellStateWeekend     = 1 << 4,
    CFSCalendarCellStateTodaySelected = CFSCalendarCellStateToday|CFSCalendarCellStateSelected
};

typedef NS_OPTIONS(NSUInteger, CFSCalendarCaseOptions) {
    CFSCalendarCaseOptionsHeaderUsesDefaultCase      = 0,
    CFSCalendarCaseOptionsHeaderUsesUpperCase        = 1,
    
    CFSCalendarCaseOptionsWeekdayUsesDefaultCase     = 0 << 4,
    CFSCalendarCaseOptionsWeekdayUsesUpperCase       = 1 << 4,
    CFSCalendarCaseOptionsWeekdayUsesSingleUpperCase = 2 << 4,
};

/**
 * CFSCalendarAppearance determines the fonts and colors of components in the calendar.
 *
 * @see CFSCalendarDelegateAppearance
 */
@interface CFSCalendarAppearance : NSObject

/**
 * The font of the day text.
 *
 * @warning The size of font is adjusted by calendar size. To turn it off, set adjustsFontSizeToFitContentSize to NO;
 */
@property (strong, nonatomic) UIFont   *titleFont;

/**
 * The font of the subtitle text.
 *
 * @warning The size of font is adjusted by calendar size. To turn it off, set adjustsFontSizeToFitContentSize to NO;
 */
@property (strong, nonatomic) UIFont   *subtitleFont;

/**
 * The font of the weekday text.
 *
* @warning The size of font is adjusted by calendar size. To turn it off, set adjustsFontSizeToFitContentSize to NO;
 */
@property (strong, nonatomic) UIFont   *weekdayFont;

/**
 * The font of the month text.
 *
 * @warning The size of font is adjusted by calendar size. To turn it off, set adjustsFontSizeToFitContentSize to NO;
 */
@property (strong, nonatomic) UIFont   *headerTitleFont;

/**
 * The offset of the day text from default position.
 */
@property (assign, nonatomic) CGPoint  titleOffset;

/**
 * The offset of the day text from default position.
 */
@property (assign, nonatomic) CGPoint  subtitleOffset;

/**
 * The offset of the event dots from default position.
 */
@property (assign, nonatomic) CGPoint eventOffset;

/**
 * The offset of the image from default position.
 */
@property (assign, nonatomic) CGPoint imageOffset;

/**
 * The color of event dots.
 */
@property (strong, nonatomic) UIColor  *eventDefaultColor;

/**
 * The color of event dots.
 */
@property (strong, nonatomic) UIColor  *eventSelectionColor;

/**
 * The color of weekday text.
 */
@property (strong, nonatomic) UIColor  *weekdayTextColor;

/**
 * The color of month header text.
 */
@property (strong, nonatomic) UIColor  *headerTitleColor;

/**
 * The date format of the month header.
 */
@property (strong, nonatomic) NSString *headerDateFormat;

/**
 * The alpha value of month label staying on the fringes.
 */
@property (assign, nonatomic) CGFloat  headerMinimumDissolvedAlpha;

/**
 * The day text color for unselected state.
 */
@property (strong, nonatomic) UIColor  *titleDefaultColor;

/**
 * The day text color for selected state.
 */
@property (strong, nonatomic) UIColor  *titleSelectionColor;

/**
 * The day text color for today in the calendar.
 */
@property (strong, nonatomic) UIColor  *titleTodayColor;

/**
 * The day text color for days out of current month.
 */
@property (strong, nonatomic) UIColor  *titlePlaceholderColor;

/**
 * The day text color for weekend.
 */
@property (strong, nonatomic) UIColor  *titleWeekendColor;

/**
 * The subtitle text color for unselected state.
 */
@property (strong, nonatomic) UIColor  *subtitleDefaultColor;

/**
 * The subtitle text color for selected state.
 */
@property (strong, nonatomic) UIColor  *subtitleSelectionColor;

/**
 * The subtitle text color for today in the calendar.
 */
@property (strong, nonatomic) UIColor  *subtitleTodayColor;

/**
 * The subtitle text color for days out of current month.
 */
@property (strong, nonatomic) UIColor  *subtitlePlaceholderColor;

/**
 * The subtitle text color for weekend.
 */
@property (strong, nonatomic) UIColor  *subtitleWeekendColor;

/**
 * The fill color of the shape for selected state.
 */
@property (strong, nonatomic) UIColor  *selectionColor;

/**
 * The fill color of the shape for today.
 */
@property (strong, nonatomic) UIColor  *todayColor;

/**
 * The fill color of the shape for today and selected state.
 */
@property (strong, nonatomic) UIColor  *todaySelectionColor;

/**
 * The border color of the shape for unselected state.
 */
@property (strong, nonatomic) UIColor  *borderDefaultColor;

/**
 * The border color of the shape for selected state.
 */
@property (strong, nonatomic) UIColor  *borderSelectionColor;

/**
 * The border radius, while 1 means a circle, 0 means a rectangle, and the middle value will give it a corner radius.
 */
@property (assign, nonatomic) CGFloat borderRadius;

/**
 * The case options manage the case of month label and weekday symbols.
 *
 * @see CFSCalendarCaseOptions
 */
@property (assign, nonatomic) CFSCalendarCaseOptions caseOptions;

/**
 * A Boolean value indicates whether the calendar should adjust font size by its content size.
 *
 * @see titleFont
 * @see subtitleFont
 * @see weekdayFont
 * @see headerTitleFont
 */
@property (assign, nonatomic) BOOL adjustsFontSizeToFitContentSize;

#if TARGET_INTERFACE_BUILDER

// For preview only
@property (assign, nonatomic) BOOL      fakeSubtitles;
@property (assign, nonatomic) BOOL      fakeEventDots;
@property (assign, nonatomic) NSInteger fakedSelectedDay;

#endif

/**
 * Triggers an appearance update.
 */
- (void)invalidateAppearance;

@end

/**
 * These functions and attributes are deprecated.
 */
@interface CFSCalendarAppearance (Deprecated)

@property (assign, nonatomic) CFSCalendarCellStyle cellStyle CFSCalendarDeprecated('cellShape');
@property (assign, nonatomic) BOOL useVeryShortWeekdaySymbols CFSCalendarDeprecated('caseOptions');
@property (assign, nonatomic) BOOL autoAdjustTitleSize CFSCalendarDeprecated('adjustFontSizeToFitContentSize');
@property (assign, nonatomic) BOOL adjustsFontSizeToFitCellSize CFSCalendarDeprecated('adjustFontSizeToFitContentSize');
@property (assign, nonatomic) CGFloat titleTextSize CFSCalendarDeprecated('titleFont');
@property (assign, nonatomic) CGFloat subtitleTextSize CFSCalendarDeprecated('subtitleFont');
@property (assign, nonatomic) CGFloat weekdayTextSize CFSCalendarDeprecated('weekdayFont');
@property (assign, nonatomic) CGFloat headerTitleTextSize CFSCalendarDeprecated('headerTitleFont');
@property (assign, nonatomic) CGFloat titleVerticalOffset CFSCalendarDeprecated('titleOffset');
@property (assign, nonatomic) CGFloat subtitleVerticalOffset CFSCalendarDeprecated('subtitleOffset');
@property (strong, nonatomic) UIColor *eventColor CFSCalendarDeprecated('eventDefaultColor');
@property (assign, nonatomic) CFSCalendarCellShape cellShape CFSCalendarDeprecated('borderRadius');

@end



