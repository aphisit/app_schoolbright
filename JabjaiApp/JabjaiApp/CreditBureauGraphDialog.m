//
//  CreditBureauGraphDialog.m
//  JabjaiApp
//
//  Created by mac on 11/19/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "CreditBureauGraphDialog.h"
#import "UserData.h"
#import "Utils.h"
@interface CreditBureauGraphDialog () {
}

@property (nonatomic, assign) BOOL isShowing;

@end

@implementation CreditBureauGraphDialog

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.dialogView.layer.masksToBounds = YES;
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)].CGPath;
    self.headerView.layer.mask = maskLayer;
    self.headerView.layer.masksToBounds = YES;
    
    //self.cancelBtn.layer.cornerRadius = 20;
    self.cancelBtn.layer.masksToBounds = YES;
    
    // Initialize variables
    self.isShowing = NO;
    
    [self.cancelBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_CLOSE",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm;
    //set color header
    [self.cancelBtn layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.cancelBtn.bounds;
    [self.cancelBtn.layer insertSublayer:gradientConfirm atIndex:0];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

#pragma mark - Dialog Functions
- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title status:(ATTENDSTATUS)status times:(NSInteger)times totalTimes:(NSInteger)totalTimes {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    
    [self.titleLabel setText:title];
    NSBundle *myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[UserData getChangLanguage] ofType:@"lproj"]];
    if(status == ONTIME) {
        [_emoImage setImage:[UIImage imageNamed:@"ic_ontime_pig"]];
        [_attendStatusLabel setText:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_ONTIME",nil,myLangBundle,nil)];
    }
    else if(status == LATE) {
        [_emoImage setImage:[UIImage imageNamed:@"ic_lest_pig"]];
        [_attendStatusLabel setText:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_LATE",nil,myLangBundle,nil)];
    }
    else if(status == ABSENCE) {
        [_emoImage setImage:[UIImage imageNamed:@"ic_absence"]];
        [_attendStatusLabel setText:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_ABSENCE",nil,myLangBundle,nil)];
    }
    else if(status == SICK) {
        [_emoImage setImage:[UIImage imageNamed:@"ic_sick_pig"]];
        [_attendStatusLabel setText:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_SICK",nil,myLangBundle,nil)];
    }
    else if(status == PERSONAL) {
        [_emoImage setImage:[UIImage imageNamed:@"ic_personal_pig"]];
        [_attendStatusLabel setText:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_PERSONAL",nil,myLangBundle,nil)];
    }
    else if(status == EVENT){
        [_emoImage setImage:[UIImage imageNamed:@"ic_event_pig"]];
        [_attendStatusLabel setText:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_EVENT",nil,myLangBundle,nil)];
    }
    else if(status == OTHER){
        [_emoImage setImage:[UIImage imageNamed:@"ic_event_pig"]];
        [_attendStatusLabel setText:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_OTHER",nil,myLangBundle,nil)];
    }
    else{
        [_emoImage setImage:[UIImage imageNamed:@"ic_absence"]];
        [_attendStatusLabel setText:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_UNCHECK",nil,myLangBundle,nil)];
    }
    
    [_totalLabel setText:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_TOTAL",nil,myLangBundle,nil)];
    [_statusTimesLabel setText:[NSString stringWithFormat:@"%i %@", (int)times,NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_TIMES",nil,myLangBundle,nil)]];
    [_totalStatusTimesLabel setText:[NSString stringWithFormat:@"%i %@", (int)totalTimes,NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_TIMES",nil,myLangBundle,nil)]];
    
    self.isShowing = YES;
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
    
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

@end
