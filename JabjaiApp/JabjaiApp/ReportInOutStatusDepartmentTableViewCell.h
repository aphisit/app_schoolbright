//
//  ReportInOutStatusDepartmentTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 7/3/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutStatusDepartmentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameDepartmentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *picDepartmentImage;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (weak, nonatomic) IBOutlet UILabel *runNumberLabel;

- (void)updateStatus:(int)status;
@end

NS_ASSUME_NONNULL_END
