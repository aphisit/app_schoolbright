//
//  TeacherTimeTableListViewController.m
//  JabjaiApp
//
//  Created by mac on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TeacherTimeTableListViewController.h"
#import "TeacherTimeTableModel.h"
#import "SSubjectTableViewCell.h"
#import "Constant.h"
#import "APIURL.h"
#import "Utils.h"

@interface TeacherTimeTableListViewController (){
    
    NSMutableArray<TeacherTimeTableModel *> *subjectArray;
    NSInteger connectCounter;
    UIColor *grayColor;
}

@property (strong, nonatomic) ARSPContainerController *panelControllerContainer;
@property (strong, nonatomic) TeacherTimeTableViewController *teacherTimeTableViewController;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;

@end

@implementation TeacherTimeTableListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
//    self.panelControllerContainer = (ARSPContainerController *)self.parentViewController;
//    self.teacherTimeTableViewController = (TeacherTimeTableViewController *)self.panelControllerContainer.mainViewController;
//    self.teacherTimeTableViewController.delegate = self;
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = grayColor;
    
    connectCounter = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SSubjectTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:[NSDate date]];
    [self getTeacherTimetableListWithDate:[NSDate date]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(subjectArray == nil) {
        return 0;
    }
    else {
        return subjectArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    TeacherTimeTableModel *model = [subjectArray objectAtIndex:indexPath.row];

    SSubjectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.labelSubject.text = model.subjectName;
    cell.labelTimeStart.text = model.startTime;
    cell.labelTimeEnd.text = model.endTime;
    

    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

#pragma mark - TeacherTimeTableViewControllerDelegate
- (void)onSelectDate:(NSDate *)date {
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:date];
    [self getTeacherTimetableListWithDate:date];
}

#pragma mark - Get API Data
- (void) getTeacherTimetableListWithDate:(NSDate *)date {
    
    long long userID = [UserData getUserID];
    long long schoolId = [UserData getSchoolId];
    NSString *URLString = [APIURL getTeacherScheduleListWithUserID:userID date:date schoolid:schoolId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getTeacherTimetableListWithDate:date];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getTeacherTimetableListWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getTeacherTimetableListWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(subjectArray != nil) {
                    [subjectArray removeAllObjects];
                    subjectArray = nil;
                }
                
                subjectArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long subjectID = [[dataDict objectForKey:@"ScheduleId"] longLongValue];
                    
                    NSMutableString *subjectName, *startTime, *endTime;
                    
                    if(![[dataDict objectForKey:@"ScheduleName"] isKindOfClass:[NSNull class]]) {
                        subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScheduleName"]];
                    }
                    else {
                        subjectName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TimeIn"] isKindOfClass:[NSNull class]]) {
                        startTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeIn"]];
                    }
                    else {
                        startTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TimeOut"] isKindOfClass:[NSNull class]]) {
                        endTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeOut"]];
                    }
                    else {
                        endTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endTime);
                    
                    TeacherTimeTableModel *model = [[TeacherTimeTableModel alloc] init];
                    model.subjectID = subjectID;
                    model.subjectName = subjectName;
                    model.startTime = startTime;
                    model.endTime = endTime;
                    
                    [subjectArray addObject:model];
                    
                }
                
                [self.tableView reloadData];
            }
        }
        
    }];
    
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

@end
