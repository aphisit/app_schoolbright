//
//  ARSelectTermYearModel.h
//  JabjaiApp
//
//  Created by toffee on 21/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ARSelectTermYearModel : NSObject
@property (nonatomic) NSInteger yearId;
@property (nonatomic) NSInteger yearNumber;

-(void)setYearNumber:(NSInteger)yearNumber;
-(void)setYearId:(NSInteger)yearId;

-(NSInteger)getYearNumber;
-(NSInteger)getYearId;
@end

NS_ASSUME_NONNULL_END
