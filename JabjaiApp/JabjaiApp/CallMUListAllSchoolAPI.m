//
//  CallMUListAllSchoolAPI.m
//  JabjaiApp
//
//  Created by toffee on 28/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallMUListAllSchoolAPI.h"
#import "APIURL.h"
#import "Utils.h"
@implementation CallMUListAllSchoolAPI{
    NSInteger connectCounter;
    NSMutableArray<MUAllSchoolModel *> *schoolArray;
}
-(void)call{
    [self getAllSchool];
}
-(void)getAllSchool{
    NSString *URLString = [APIURL getListAllSchool];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            BOOL isFail = NO;
            
            if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

                if(error != nil) {
                    NSLog(@"An error occured : %@" , [error localizedDescription]);
                }
                else if(data == nil) {
                    NSLog(@"Data is null");
                }
                else {
                    NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                    NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
                }

                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;

                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getAllSchool];

                }
                else {

                    isFail = YES;
                    connectCounter = 0;
                }

            }
            else {
                id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if(error != nil) {
                    NSLog(@"An error occured : %@", [error localizedDescription]);
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self getAllSchool];
                    }
                    else {
                        isFail = YES;
                        connectCounter = 0;
                    }
                    
                }
                else if(![returnedData isKindOfClass:[NSArray class]]) {
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self getAllSchool];
                    }
                    else {
                        isFail = YES;
                        connectCounter = 0;
                    }
                }
                else {
                    
                    NSArray *returnedArray = returnedData;
                    connectCounter = 0;
                    
                    if(schoolArray != nil) {
                        [schoolArray removeAllObjects];
                        schoolArray = nil;
                    }
                    schoolArray = [[NSMutableArray alloc] init];
                    for(int i=0; i<returnedArray.count; i++) {
                         NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                        NSString *schoolName;
                        long long schoolID = [[dataDict objectForKey:@"SchoolID"] longLongValue];
                        
                        if ([[UserData getChangLanguage]isEqualToString:@"th"]) {
                            if(![[dataDict objectForKey:@"SchoolName"] isKindOfClass:[NSNull class]]) {
                                        schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolName"]];
                            }
                            else {
                                schoolName = [[NSMutableString alloc] initWithString:@""];
                            }
                        }
                        else{
                            if(![[dataDict objectForKey:@"SchoolNameEN"] isKindOfClass:[NSNull class]]) {
                                        schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolNameEN"]];
                            }
                            else {
                                schoolName = [[NSMutableString alloc] initWithString:@""];
                            }

                        }
                       
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) schoolName);
                        MUAllSchoolModel *model = [[MUAllSchoolModel alloc] init];
                        [model setSchoolID:schoolID];
                        [model setSchoolName:schoolName];
                        [schoolArray addObject:model];
                    }

                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callMUListAllSchoolNameAPI:data:success:)]) {
                        [self.delegate callMUListAllSchoolNameAPI:self data:schoolArray success:YES];
                    }
                }
                
            }
            
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callMUListAllSchoolNameAPI:data:success:)]) {
                    [self.delegate callMUListAllSchoolNameAPI:self data:schoolArray success:YES];
                }
            }
         
            
        }
         ];
}
@end
