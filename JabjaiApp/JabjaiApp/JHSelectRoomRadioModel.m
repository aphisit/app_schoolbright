//
//  JHSelectRoomRadioModel.m
//  JabjaiApp
//
//  Created by toffee on 1/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "JHSelectRoomRadioModel.h"

@implementation JHSelectRoomRadioModel
@synthesize classRoomID = _classRoomID;
@synthesize classRoomName = _classRoomName;
@synthesize selected = _selected;

-(void)setClassRoomID:(NSNumber *)classRoomID{
    _classRoomID = classRoomID;
}

-(void)setRoomName:(NSString *)roomName{
    _classRoomName = roomName;
}

-(void)setSelected:(BOOL)selected{
    _selected = selected;
}

-(NSNumber *)getClassRoomID{
    return _classRoomID;
}
-(NSString *)getRoomName{
    return _classRoomName;
}
-(BOOL)isSelected{
    return _selected;
}

@end
