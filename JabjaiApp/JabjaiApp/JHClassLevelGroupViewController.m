//
//  JHClassLevelGroupViewController.m
//  JabjaiApp
//
//  Created by toffee on 11/21/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "JHClassLevelGroupViewController.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "JHTimeOutAndDetailViewController.h"

@interface JHClassLevelGroupViewController (){
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    NSMutableArray *classLevelStringArray;
   // NSMutableArray *classroomStringArrray;
    NSMutableArray *sujectIDArrray;
    NSMutableArray *sujectNameArrray;
}
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallJHGetSubjectModeGroupAPI *callJHGetSubjectAPI;
@end
static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *subjectRequestCode = @"subjectRequestCode";
@implementation JHClassLevelGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    sujectIDArrray = [[NSMutableArray alloc] init];
    sujectNameArrray = [[NSMutableArray alloc] init];
    //SWRevealViewController *revealViewController = self.revealViewController;
    self.classLavelTextField.delegate = self;
    self.classRoomTextField.delegate = self;
    [self getSchoolClassLevel];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // User press class level textfield
        
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        
        if(self.classLavelTextField.text.length == 0) {
            NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(sujectNameArrray != nil && sujectNameArrray.count > 0) {
            [self showTableListDialogWithRequestCode:subjectRequestCode data:sujectNameArrray];
        }
        
        return NO;
    }
    
    return YES;
}



- (void)getSchoolClassLevel {
    
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    //NSLog(@"getSchoolig = %@",[UserData getSchoolId] );
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}

- (void)getSubjectDataWithClassroomId:(long long)classLevelId {
    
    if(self.callJHGetSubjectAPI != nil) {
        self.callJHGetSubjectAPI = nil;
    }
    
    self.callJHGetSubjectAPI = [[CallJHGetSubjectModeGroupAPI alloc] init];
    self.callJHGetSubjectAPI.delegate = self;
    [self.callJHGetSubjectAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}


- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        NSLog(@"CLASSLAVEL = %@",classLevelStringArray);
    }
}

#pragma mark - CallGetStudentClassroomAPIDelegate

- (void)callJHGetSubjectModeGroupAPI:(CallJHGetSubjectModeGroupAPI *)classObj data:(NSArray<JHSubjectModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _subjectArray = data;
        //notArrangeTimeTable = NO;
    }
    for(int i=0; i<data.count; i++) {
        JHSubjectModel *model = [data objectAtIndex:i];
        [sujectIDArrray addObject:[model getSubjectID]];
        [sujectNameArrray addObject:[model getSubjectName]];
        
    }
}
#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}
- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
        
        //        if(index == _selectedClassLevelIndex) {
        //            return;
        //        }
        self.classLavelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classRoomTextField.text = @"";
        _selectedClassroomIndex = -1;
        _classLevelId = 0;
        
        // get classroom data with class level
        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSubjectDataWithClassroomId:_classLevelId];
        
    }
    else if([requestCode isEqualToString:subjectRequestCode]) {
        self.levelId = [[NSMutableArray alloc] init];
        
        if(index == _selectedClassroomIndex) {
            return;
        }
        
        self.classRoomTextField.text = [[sujectNameArrray objectAtIndex:index] getSubjectName];
        _selectedClassroomIndex = index;
        _subjectId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        // addID Room
        //[self.levelId addObject:@(_subjectId)];
        
    }
    
}

- (BOOL)validateData {
    
    if(self.classLavelTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classRoomTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกชั้นเรียน";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}

- (void)gotoSelectStudentsPage {
    NSLog(@"level2Id = %@",self.levelId);
    JHTimeOutAndDetailViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JHTimeOutAndDetailStoryboard"];
//
//    viewController.classLevelArray = _classLevelArray;
//    viewController.classroomArray = _classroomArray;
//    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
//    viewController.selectedClassroomIndex = _selectedClassroomIndex;
//    viewController.classLevelId = _classLevelId;
//    viewController.classroomId = _classroomId;
//    viewController.mode = _mode;
    viewController.levelId = self.levelId;
    viewController.sendType = 0;
//    viewController.newsType = self.newstype;
//    viewController.sendGroup = self.sendgroup;
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
- (IBAction)actionNext:(id)sender {
    if([self validateData]) {
        [self gotoSelectStudentsPage];
    }
}

- (IBAction)openDrawer:(id)sender {
     [self.revealViewController revealToggle:self.revealViewController];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
