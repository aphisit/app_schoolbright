//
//  MessageBoxScrollableTextDialog.m
//  JabjaiApp
//
//  Created by mac on 11/24/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "MessageInBoxScrollableTextDialog.h"
#import "Utils.h"
#import "UserData.h"
#import "Utils.h"
#import "APIURL.h"
#import "ZMImageSliderViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface MessageInBoxScrollableTextDialog () {
    NSMutableArray *imageArray,*fileArray,*typeFileArray;
    NSMutableArray *typeImageArray;
    NSArray *returnedArray;
    JHReadFileViewController *openFileView;
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, assign) BOOL isShowing;

@end

@implementation MessageInBoxScrollableTextDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    imageArray = [[NSMutableArray alloc] init];
    typeImageArray = [[NSMutableArray alloc] init];
    fileArray = [[NSMutableArray alloc] init];
    typeFileArray = [[NSMutableArray alloc] init];
   
//    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
//    self.collectionView.delegate = self;
//    self.collectionView.dataSource = self;
//
//    [self.fileCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
//    self.fileCollectionView.delegate = self;
//    self.fileCollectionView.dataSource = self;
    
    self.dialogView.layer.masksToBounds = YES;
    self.closeBtn.layer.masksToBounds = YES;
    self.dateFormatter = [Utils getDateFormatter];
    
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm 'น.'"];
    }else{
        [self.dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    }
    
    [self.messageTextView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:NULL];
    //[self.messageTextView setEditable:NO];
    
    self.isShowing = NO;
    [self.closeBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_CLOSE",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm;
    //set color header
    [self.closeBtn layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.closeBtn.bounds;
    [self.closeBtn.layer insertSublayer:gradientConfirm atIndex:0];
    
  
}

- (void)dealloc {
    [self.messageTextView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    //Align textview content center vertically
    UITextView *tv = object;
    CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])/2.0;
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
    [tv setContentInset:UIEdgeInsetsMake(topCorrect,0,0,0)];
}

- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

-(void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message messageDate:(NSDate *)messageDate messageID:(long long)messageID {
//    self.scrollView.contentSize = CGSizeMake(self.contentview.frame.size.width, self.contentview.frame.size.height);
    self.messageTextView.text = @"";
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
   // [self.titleLabel setText:title];
    self.messageTextView.text = [NSString stringWithFormat:@"%@", message] ;
     NSArray *array = [[NSString stringWithFormat:@"%@", [self.dateFormatter stringFromDate:messageDate]] componentsSeparatedByString:@" "];
    NSLog(@"DATE = %@",array[0]);
    NSLog(@"TIME = %@",array[1]);
    NSString *xx;
    xx = [self.dateFormatter stringFromDate:array[0]];
    [self.dateLabel setText:array[0]];
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.timeLabel setText:[array[1] stringByAppendingString:@" น."]];
    }else{
        [self.timeLabel setText:array[1]];
    }
    
     //CGSize smallestViewSize = [_dialogView systemLayoutSizeFittingSize: UILayoutFittingCompressedSize];

       self.isShowing = YES;
    
//    //add image to array
//    long long userID = [UserData getUserID];
//    NSString *URLString = [APIURL getImageNews:userID idMessage:messageID];
//    NSURL *URL = [NSURL URLWithString:URLString];
//    imageArray = [[NSMutableArray alloc] init];
//    typeFileArray = [[NSMutableArray alloc] init];
//    fileArray = [[NSMutableArray alloc]init];
//
//    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//
//
//        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
//
//            if(error != nil) {
//                NSLog(@"An error occured : %@" , [error localizedDescription]);
//            }
//            else if(data == nil) {
//                NSLog(@"Data is null");
//            }
//            else {
//                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
//                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
//            }
//
//        }
//        else {
//            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//            returnedArray = returnedData;
//            if(returnedArray.count == 0){
//                returnedArray = NULL;
//                //[self.collectionView reloadData];
//            }
//
//            if (returnedArray != nil || returnedArray != NULL) {
//                for (int i=0; i<returnedArray.count; i++) {
//
//                    if ([[[returnedArray objectAtIndex:i]objectForKey:@"filetype"] isEqualToString:@"image/jpeg"]) {
//                        [imageArray addObject:[[returnedArray objectAtIndex:i]objectForKey:@"filename"] ];
//                    }else{
//                        NSString *fileType = [[returnedArray objectAtIndex:i]objectForKey:@"filetype"];
//                        NSString *fileName = [[returnedArray objectAtIndex:i]objectForKey:@"filename"];
//                        [typeFileArray addObject:fileType];
//                        [fileArray addObject:fileName];
//                    }
//
//                    //[typeImageArray addObject:[[returnedArray objectAtIndex:i]objectForKey:@"filetype"] ];
//                }
//
//            }
////            [self.collectionView reloadData];
////            [self.fileCollectionView reloadData];
//
//        }
//
//
//    }];
}
-(void)removeDialogFromView {
    
    imageArray = [[NSMutableArray alloc] init];
    typeImageArray = [[NSMutableArray alloc] init];
    fileArray = [[NSMutableArray alloc] init];
    typeFileArray = [[NSMutableArray alloc] init];
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
    
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(messageInBoxScrollableTextClose)]) {
        [self.delegate messageInBoxScrollableTextClose];
    }
}
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    
//    if (collectionView == self.collectionView) {
//        
//        return imageArray.count;
//    }else{
//        return typeFileArray.count;
//    }
//    
//}
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    MessageInboxScrollableTextDialogCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
//    if (collectionView == self.collectionView) {
//        cell.imageNews.tag = indexPath.row;
//        [cell.images sd_setImageWithURL:[NSURL URLWithString: [imageArray objectAtIndex:indexPath.row]]];
//        [cell.imageNews addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//        return cell;
//    }else{
//        cell.imageNews.tag = indexPath.row;
//        if ([[typeFileArray objectAtIndex:indexPath.row] isEqualToString:@"PDF"]) {
//            [cell.images setImage:[UIImage imageNamed:@"ic_pdf"]];
//        }else if ([[typeFileArray objectAtIndex:indexPath.row] isEqualToString:@"MS Word"]){
//            [cell.images setImage:[UIImage imageNamed:@"ic_word"]];
//        }else{
//            [cell.images setImage:[UIImage imageNamed:@"ic_excel"]];
//        }
//        [cell.imageNews addTarget:self action:@selector(selectFileTapped:) forControlEvents:UIControlEventTouchUpInside];
//        return cell;
//    }
//}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
//    CGSize defaultSize = CGSizeMake((self.collectionView.frame.size.width/3)-10, (self.collectionView.frame.size.height));
//    return defaultSize;
//}

- (void)okButtonTapped:(UIButton *)sender {
    if (imageArray.count > 0) {
        NSLog(@"index = %d",sender.tag);
        ZMImageSliderViewController*controller = [[ZMImageSliderViewController alloc] initWithOptions:sender.tag imageUrls:imageArray];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)selectFileTapped:(UIButton *)sender  {
    openFileView = [[JHReadFileViewController alloc] init];
    [openFileView showPagesReadFileInView:self.view partFile:[fileArray objectAtIndex:sender.tag]];
}

-(BOOL)isDialogShowing {
    return self.isShowing;
}

@end
