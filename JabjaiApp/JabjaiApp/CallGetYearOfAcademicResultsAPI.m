//
//  CallGetYearOfAcademicResultsAPI.m
//  JabjaiApp
//
//  Created by toffee on 21/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallGetYearOfAcademicResultsAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallGetYearOfAcademicResultsAPI () {
    NSInteger connectCounter;
    NSMutableArray *yearArray;
}
@end
@implementation CallGetYearOfAcademicResultsAPI
-(void) call:(long long)schoolId{
    connectCounter = 0;
    [self getYearOfAcademicResults:schoolId];
}

-(void)getYearOfAcademicResults:(long long)schoolId{
    NSString *URLString = [APIURL getYearOfAcademicResults:schoolId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getYearOfAcademicResults:schoolId];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }

        } else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getYearOfAcademicResults:schoolId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            
            else {
                connectCounter = 0;
                NSArray *dataArray = returnedData;
                yearArray = [[NSMutableArray alloc] init];
                for (int i = 0; i < dataArray.count; i++) {
                    NSDictionary *dataDict = [dataArray objectAtIndex:i];
                    NSInteger year = [[dataDict objectForKey:@"YearNumber"] integerValue];
                    NSInteger yearId = [[dataDict objectForKey:@"nYear"] integerValue];
                    ARSelectTermYearModel *model = [[ARSelectTermYearModel alloc] init];
                    [model setYearNumber:year];
                    [model setYearId:yearId];
                    [yearArray addObject:model];
                }
                NSLog(@"xxx");
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetYearOfAcademicResultsAPI:data:success:)]) {
                                   [self.delegate callGetYearOfAcademicResultsAPI:self data:yearArray success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetYearOfAcademicResultsAPI:data:success:)]) {
                [self.delegate callGetYearOfAcademicResultsAPI:self data:yearArray success:NO];
            }
        }
        
    }];
}
@end
