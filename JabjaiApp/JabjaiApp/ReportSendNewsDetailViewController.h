//
//  ReportSendNewsDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 4/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportSendNewsImageWithFileCollectionViewCell.h"
#import "saveImageXIBViewController.h"
#import "SlideMenuController.h"
#import "ReportSendNewsOpenFileViewController.h"

@interface ReportSendNewsDetailViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource , saveImageXIBViewControllerDelegate, SlideMenuControllerDelegate>
@property (assign, nonatomic) long long newsID;

@property (weak, nonatomic) IBOutlet UILabel *headerSendTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAmountReciptientLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerReadLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerUnReadLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDateSendLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTimeSendLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachFileLabel;




@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *sendTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *allCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *unreadCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

//@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UICollectionView *imageNewsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *fileNewsCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highImaCollectionConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highFileCollectionConstraint;


- (void)getReportSendNewsDetailWithNewsID:(long long)newsId;

@end
