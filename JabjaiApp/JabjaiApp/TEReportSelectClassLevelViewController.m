//
//  TEReportSelectClassLevelViewController.m
//  JabjaiApp
//
//  Created by toffee on 11/15/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TEReportSelectClassLevelViewController.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "Utils.h"

#import "TEReportCalendarViewController.h"

#import "TEReportARSPContainerController.h"

@interface TEReportSelectClassLevelViewController (){
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    CAGradientLayer *gradient;
}
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;

@end
static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";
@implementation TEReportSelectClassLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.classLevelTextField.delegate = self;
    self.classRoomTextField.delegate = self;
    
//    gradient = [Utils getGradientColorHeader];
//    gradient.frame = self.headerView.bounds;
//    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    if(_classLevelArray != nil && _classroomArray != nil ) {
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classLevelArray.count; i++) {
            SchoolLevelModel *model = [_classLevelArray objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        classroomStringArrray = [[NSMutableArray alloc] init];
        for(int i=0; i<_classroomArray.count; i++) {
            SchoolClassroomModel *model = [_classroomArray objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:_selectedClassLevelIndex] getClassLevelName];
        self.classRoomTextField.text = [[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomName];
       
    }else{
        _selectedClassLevelIndex = -1;
        _selectedClassroomIndex = -1;
         [self getSchoolClassLevel];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (screenSize.height <= 568.0f) {
            CALayer *bottomBorder = [CALayer layer];
            bottomBorder.frame = CGRectMake(0.0f, self.classLevelTextField.frame.size.height *0.975, self.classLevelTextField.frame.size.width *10, 1.0f);
            bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
            [self.classLevelTextField.layer addSublayer:bottomBorder];
            
            CALayer *bottomBorder1 = [CALayer layer];
            bottomBorder1.frame = CGRectMake(0.0f, self.classRoomTextField.frame.size.height *0.985, self.classRoomTextField.frame.size.width *10, 1.0f);
            bottomBorder1.backgroundColor = [UIColor blackColor].CGColor;
            [self.classRoomTextField.layer addSublayer:bottomBorder1];
            
           
        }
        else{
            
            CALayer *bottomBorder = [CALayer layer];
            bottomBorder.frame = CGRectMake(0.0f, self.classLevelTextField.frame.size.height *1.15, self.classLevelTextField.frame.size.width *10, 1.0f);
            bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
            [self.classLevelTextField.layer addSublayer:bottomBorder];
            
            CALayer *bottomBorder1 = [CALayer layer];
            bottomBorder1.frame = CGRectMake(0.0f, self.classRoomTextField.frame.size.height *1.15, self.classRoomTextField.frame.size.width *10, 1.0f);
            bottomBorder1.backgroundColor = [UIColor blackColor].CGColor;
            [self.classRoomTextField.layer addSublayer:bottomBorder1];
            
            
            
        }
    }
       
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getSchoolClassLevel {
     [self showIndicator];
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
         [self stopIndicator];
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        NSLog(@"CLASSLAVEL = %@",classLevelStringArray);
    }
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
     [self showIndicator];
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}
#pragma mark - CallGetStudentClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _classroomArray = data;
         [self stopIndicator];
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // User press class level textfield
        
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}
- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}
#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {

        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classRoomTextField.text = @"";
        _selectedClassroomIndex = -1;
        _classLevelId = 0;
        
        // get classroom data with class level
        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:_classLevelId];
        
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        // self.level2Id = [[NSMutableArray alloc] init];
        
        if(index == _selectedClassroomIndex) {
            return;
        }
        self.classRoomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        _classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        // addID Room
        // [self.level2Id addObject:@(_classroomId)];
        
    }
    
}
#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classRoomTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกชั้นเรียน";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    return YES;
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}
- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        
        TEReportCalendarViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TEReportCalendarStoryboard"];
        viewController.classLevelArray = _classLevelArray;
        viewController.classroomArray = _classroomArray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.selectedClassroomId = _classroomId;
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//            [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    

    
}

- (IBAction)openDrawer:(id)sender {
//    [self.revealViewController revealToggle:self.revealViewController];
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
