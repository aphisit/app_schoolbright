//
//  MenuFoodTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 4/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@class MenuFoodTableViewCell;

@protocol MenuFoodTableViewCellDelegate <NSObject>

- (void)processNumberProduct:(MenuFoodTableViewCell *)tableViewCell atIndex:(NSInteger)index status:(NSString*)status;

@end

@interface MenuFoodTableViewCell : UITableViewCell


@property (weak, nonatomic) id<MenuFoodTableViewCellDelegate> delegate;

@property (nonatomic) NSInteger index;
@property (nonatomic) NSString* status;
    @property (weak, nonatomic) IBOutlet UIView *sectionView;
    @property (weak, nonatomic) IBOutlet UILabel *menuFoodLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;



@property (weak, nonatomic) IBOutlet UIButton *deleteNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *addNumberLabel;

@property (weak, nonatomic) IBOutlet UITextField *numberTextField;
@property (weak, nonatomic) IBOutlet UILabel *numberOrderTextField;

- (IBAction)actionDeleteProductButton:(id)sender;
- (IBAction)actionAddProductButton:(id)sender;




@end
