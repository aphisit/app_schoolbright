//
//  CallTEUpdateStudentStatusPOSTAPI.h
//  JabjaiApp
//
//  Created by toffee on 11/15/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CallTEUpdateStudentStatusPOSTAPI;
@protocol CallTEUpdateStudentStatusPOSTAPIDelegate <NSObject>
@optional
- (void)callTEUpdateStudentStatusPOSTAPI:(CallTEUpdateStudentStatusPOSTAPI *)classObj response:(NSString *)response success:(BOOL)success;

@end
@interface CallTEUpdateStudentStatusPOSTAPI : NSObject
@property (nonatomic, weak) id<CallTEUpdateStudentStatusPOSTAPIDelegate> delegate;

- (void)call:(long long)schoolId  teacherId:(long long)teacherId jsonString:(NSString *)jsonString;
@end
