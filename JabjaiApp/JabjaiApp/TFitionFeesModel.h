//
//  TFitionFeesModel.h
//  JabjaiApp
//
//  Created by Mac on 5/10/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TFInvoicesModel.h"
#import "TFProductModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TFitionFeesModel : NSObject
@property (strong, nonatomic) NSString *termName;
@property (strong, nonatomic) NSString *yearName;
@property (strong, nonatomic) NSString *studentName;
@property (strong, nonatomic) NSString *studentCode;
@property (strong, nonatomic) NSString *levelName;
@property (strong, nonatomic) NSString *className;
@property (strong, nonatomic) NSString *imageUrl;
@property (strong, nonatomic) NSString *schoolEngName;
@property (strong, nonatomic) NSString *schoolThaiName;
@property (strong, nonatomic) NSString *dueDate;
@property (strong, nonatomic) NSString *issueDate;
@property (assign, nonatomic) int status;
@property (assign, nonatomic) double price;
@property (strong, nonatomic) NSArray<TFProductModel *> *productListArray;
@property (strong, nonatomic) NSArray<TFInvoicesModel *> *invoicesArray;
//@property (strong, nonatomic) NSString *paymentDate;


@end

NS_ASSUME_NONNULL_END
