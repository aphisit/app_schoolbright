//
//  CFSCalendarAppearance.m
//  Pods
//
//  Created by DingWenchao on 6/29/15.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//
//  https://github.com/WenchaoD
//

#import "CFSCalendarAppearance.h"
#import "CFSCalendarDynamicHeader.h"
#import "CFSCalendarExtensions.h"

@interface CFSCalendarAppearance ()

@property (weak  , nonatomic) CFSCalendar *calendar;

@property (strong, nonatomic) NSMutableDictionary *backgroundColors;
@property (strong, nonatomic) NSMutableDictionary *titleColors;
@property (strong, nonatomic) NSMutableDictionary *subtitleColors;
@property (strong, nonatomic) NSMutableDictionary *borderColors;

@property (strong, nonatomic) NSString *titleFontName;
@property (strong, nonatomic) NSString *subtitleFontName;
@property (strong, nonatomic) NSString *weekdayFontName;
@property (strong, nonatomic) NSString *headerTitleFontName;

@property (assign, nonatomic) CGFloat titleFontSize;
@property (assign, nonatomic) CGFloat subtitleFontSize;
@property (assign, nonatomic) CGFloat weekdayFontSize;
@property (assign, nonatomic) CGFloat headerTitleFontSize;

@property (assign, nonatomic) CGFloat preferredTitleFontSize;
@property (assign, nonatomic) CGFloat preferredSubtitleFontSize;
@property (assign, nonatomic) CGFloat preferredWeekdayFontSize;
@property (assign, nonatomic) CGFloat preferredHeaderTitleFontSize;

@property (readonly, nonatomic) UIFont *preferredTitleFont;
@property (readonly, nonatomic) UIFont *preferredSubtitleFont;
@property (readonly, nonatomic) UIFont *preferredWeekdayFont;
@property (readonly, nonatomic) UIFont *preferredHeaderTitleFont;

- (void)adjustTitleIfNecessary;

- (void)invalidateFonts;
- (void)invalidateTextColors;
- (void)invalidateTitleFont;
- (void)invalidateSubtitleFont;
- (void)invalidateWeekdayFont;
- (void)invalidateHeaderFont;
- (void)invalidateTitleTextColor;
- (void)invalidateSubtitleTextColor;
- (void)invalidateWeekdayTextColor;
- (void)invalidateHeaderTextColor;

- (void)invalidateBorderColors;
- (void)invalidateFillColors;
- (void)invalidateEventColors;
- (void)invalidateBorderRadius;

@end

@implementation CFSCalendarAppearance

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _adjustsFontSizeToFitContentSize = YES;
        
        _titleFontSize = _preferredTitleFontSize  = CFSCalendarStandardTitleTextSize;
        _subtitleFontSize = _preferredSubtitleFontSize = CFSCalendarStandardSubtitleTextSize;
        _weekdayFontSize = _preferredWeekdayFontSize = CFSCalendarStandardWeekdayTextSize;
        _headerTitleFontSize = _preferredHeaderTitleFontSize = CFSCalendarStandardHeaderTextSize;
        
        _titleFontName = [UIFont systemFontOfSize:1].fontName; //@"ThaiSansNeue-SemiBold";
        _subtitleFontName = [UIFont systemFontOfSize:1].fontName;
        _weekdayFontName = [UIFont systemFontOfSize:1].fontName;
        _headerTitleFontName = [UIFont systemFontOfSize:1].fontName;
        
        _headerTitleColor = CFSCalendarStandardTitleTextColor;
        _headerDateFormat = @"MMMM yyyy";
        _headerMinimumDissolvedAlpha = 0.2;
        _weekdayTextColor = CFSCalendarStandardTitleTextColor;
        _caseOptions = CFSCalendarCaseOptionsHeaderUsesDefaultCase|CFSCalendarCaseOptionsWeekdayUsesDefaultCase;
        
        _backgroundColors = [NSMutableDictionary dictionaryWithCapacity:5];
        _backgroundColors[@(CFSCalendarCellStateNormal)]      = [UIColor clearColor];
        _backgroundColors[@(CFSCalendarCellStateSelected)]    = CFSCalendarStandardSelectionColor;
        _backgroundColors[@(CFSCalendarCellStateDisabled)]    = [UIColor clearColor];
        _backgroundColors[@(CFSCalendarCellStatePlaceholder)] = [UIColor clearColor];
        _backgroundColors[@(CFSCalendarCellStateToday)]       = CFSCalendarStandardTodayColor;
        
        _titleColors = [NSMutableDictionary dictionaryWithCapacity:5];
        _titleColors[@(CFSCalendarCellStateNormal)]      = [UIColor blackColor];
        _titleColors[@(CFSCalendarCellStateSelected)]    = [UIColor whiteColor];
        _titleColors[@(CFSCalendarCellStateDisabled)]    = [UIColor grayColor];
        _titleColors[@(CFSCalendarCellStatePlaceholder)] = [UIColor lightGrayColor];
        _titleColors[@(CFSCalendarCellStateToday)]       = [UIColor whiteColor];
        
        _subtitleColors = [NSMutableDictionary dictionaryWithCapacity:5];
        _subtitleColors[@(CFSCalendarCellStateNormal)]      = [UIColor darkGrayColor];
        _subtitleColors[@(CFSCalendarCellStateSelected)]    = [UIColor whiteColor];
        _subtitleColors[@(CFSCalendarCellStateDisabled)]    = [UIColor lightGrayColor];
        _subtitleColors[@(CFSCalendarCellStatePlaceholder)] = [UIColor lightGrayColor];
        _subtitleColors[@(CFSCalendarCellStateToday)]       = [UIColor whiteColor];
        
        _borderColors[@(CFSCalendarCellStateSelected)] = [UIColor clearColor];
        _borderColors[@(CFSCalendarCellStateNormal)] = [UIColor clearColor];
        
        _borderRadius = 1.0;
        _eventDefaultColor = CFSCalendarStandardEventDotColor;
        _eventSelectionColor = CFSCalendarStandardEventDotColor;
        
        _borderColors = [NSMutableDictionary dictionaryWithCapacity:2];
        
#if TARGET_INTERFACE_BUILDER
        _fakeEventDots = YES;
#endif
        
    }
    return self;
}

- (void)setTitleFont:(UIFont *)titleFont
{
    BOOL needsInvalidating = NO;
    if (![_titleFontName isEqualToString:titleFont.fontName]) {
        _titleFontName = titleFont.fontName;
        needsInvalidating = YES;
    }
    if (_titleFontSize != titleFont.pointSize) {
        _titleFontSize = titleFont.pointSize;
        needsInvalidating = YES;
    }
    if (needsInvalidating) {
        [self invalidateTitleFont];
    }
}

- (UIFont *)titleFont
{
    return [UIFont fontWithName:_titleFontName size:_titleFontSize];
}

- (void)setSubtitleFont:(UIFont *)subtitleFont
{
    BOOL needsInvalidating = NO;
    if (![_subtitleFontName isEqualToString:subtitleFont.fontName]) {
        _subtitleFontName = subtitleFont.fontName;
        needsInvalidating = YES;
    }
    if (_subtitleFontSize != subtitleFont.pointSize) {
        _subtitleFontSize = subtitleFont.pointSize;
        needsInvalidating = YES;
    }
    if (needsInvalidating) {
        [self invalidateSubtitleFont];
    }
}

- (UIFont *)subtitleFont
{
    return [UIFont fontWithName:_subtitleFontName size:_subtitleFontSize];
}

- (void)setWeekdayFont:(UIFont *)weekdayFont
{
    BOOL needsInvalidating = NO;
    if (![_weekdayFontName isEqualToString:weekdayFont.fontName]) {
        _weekdayFontName = weekdayFont.fontName;
        needsInvalidating = YES;
    }
    if (_weekdayFontSize != weekdayFont.pointSize) {
        _weekdayFontSize = weekdayFont.pointSize;
        needsInvalidating = YES;
    }
    if (needsInvalidating) {
        [self invalidateWeekdayFont];
    }
}

- (UIFont *)weekdayFont
{
    return [UIFont fontWithName:_weekdayFontName size:_weekdayFontSize];
}

- (void)setHeaderTitleFont:(UIFont *)headerTitleFont
{
    BOOL needsInvalidating = NO;
    if (![_headerTitleFontName isEqualToString:headerTitleFont.fontName]) {
        _headerTitleFontName = headerTitleFont.fontName;
        needsInvalidating = YES;
    }
    if (_headerTitleFontSize != headerTitleFont.pointSize) {
        _headerTitleFontSize = headerTitleFont.pointSize;
        needsInvalidating = YES;
    }
    if (needsInvalidating) {
        [self invalidateHeaderFont];
    }
}

- (void)setTitleOffset:(CGPoint)titleOffset
{
    if (!CGPointEqualToPoint(_titleOffset, titleOffset)) {
        _titleOffset = titleOffset;
        [_calendar.collectionView.visibleCells setValue:@YES forKey:@"needsAdjustingViewFrame"];
        [_calendar.collectionView.visibleCells makeObjectsPerformSelector:@selector(setNeedsLayout)];
    }
}

- (void)setSubtitleOffset:(CGPoint)subtitleOffset
{
    if (!CGPointEqualToPoint(_subtitleOffset, subtitleOffset)) {
        _subtitleOffset = subtitleOffset;
        [_calendar.collectionView.visibleCells setValue:@YES forKey:@"needsAdjustingViewFrame"];
        [_calendar.collectionView.visibleCells makeObjectsPerformSelector:@selector(setNeedsLayout)];
    }
}

- (void)setImageOffset:(CGPoint)imageOffset
{
    if (!CGPointEqualToPoint(_imageOffset, imageOffset)) {
        _imageOffset = imageOffset;
        [_calendar.collectionView.visibleCells setValue:@YES forKey:@"needsAdjustingViewFrame"];
        [_calendar.collectionView.visibleCells makeObjectsPerformSelector:@selector(setNeedsLayout)];
    }
}

- (void)setEventOffset:(CGPoint)eventOffset
{
    if (!CGPointEqualToPoint(_eventOffset, eventOffset)) {
        _eventOffset = eventOffset;
        [_calendar.collectionView.visibleCells setValue:@YES forKey:@"needsAdjustingViewFrame"];
        [_calendar.collectionView.visibleCells makeObjectsPerformSelector:@selector(setNeedsLayout)];
    }
}

- (UIFont *)headerTitleFont
{
    return [UIFont fontWithName:_headerTitleFontName size:_headerTitleFontSize];
}

- (void)setTitleDefaultColor:(UIColor *)color
{
    if (color) {
        _titleColors[@(CFSCalendarCellStateNormal)] = color;
    } else {
        [_titleColors removeObjectForKey:@(CFSCalendarCellStateNormal)];
    }
    [self invalidateTitleTextColor];
}

- (UIColor *)titleDefaultColor
{
    return _titleColors[@(CFSCalendarCellStateNormal)];
}

- (void)setTitleSelectionColor:(UIColor *)color
{
    if (color) {
        _titleColors[@(CFSCalendarCellStateSelected)] = color;
    } else {
        [_titleColors removeObjectForKey:@(CFSCalendarCellStateSelected)];
    }
    [self invalidateTitleTextColor];
}

- (UIColor *)titleSelectionColor
{
    return _titleColors[@(CFSCalendarCellStateSelected)];
}

- (void)setTitleTodayColor:(UIColor *)color
{
    if (color) {
        _titleColors[@(CFSCalendarCellStateToday)] = color;
    } else {
        [_titleColors removeObjectForKey:@(CFSCalendarCellStateToday)];
    }
    [self invalidateTitleTextColor];
}

- (UIColor *)titleTodayColor
{
    return _titleColors[@(CFSCalendarCellStateToday)];
}

- (void)setTitlePlaceholderColor:(UIColor *)color
{
    if (color) {
        _titleColors[@(CFSCalendarCellStatePlaceholder)] = color;
    } else {
        [_titleColors removeObjectForKey:@(CFSCalendarCellStatePlaceholder)];
    }
    [self invalidateTitleTextColor];
}

- (UIColor *)titlePlaceholderColor
{
    return _titleColors[@(CFSCalendarCellStatePlaceholder)];
}

- (void)setTitleWeekendColor:(UIColor *)color
{
    if (color) {
        _titleColors[@(CFSCalendarCellStateWeekend)] = color;
    } else {
        [_titleColors removeObjectForKey:@(CFSCalendarCellStateWeekend)];
    }
    [self invalidateTitleTextColor];
}

- (UIColor *)titleWeekendColor
{
    return _titleColors[@(CFSCalendarCellStateWeekend)];
}

- (void)setSubtitleDefaultColor:(UIColor *)color
{
    if (color) {
        _subtitleColors[@(CFSCalendarCellStateNormal)] = color;
    } else {
        [_subtitleColors removeObjectForKey:@(CFSCalendarCellStateNormal)];
    }
    [self invalidateSubtitleTextColor];
}

-(UIColor *)subtitleDefaultColor
{
    return _subtitleColors[@(CFSCalendarCellStateNormal)];
}

- (void)setSubtitleSelectionColor:(UIColor *)color
{
    if (color) {
        _subtitleColors[@(CFSCalendarCellStateSelected)] = color;
    } else {
        [_subtitleColors removeObjectForKey:@(CFSCalendarCellStateSelected)];
    }
    [self invalidateSubtitleTextColor];
}

- (UIColor *)subtitleSelectionColor
{
    return _subtitleColors[@(CFSCalendarCellStateSelected)];
}

- (void)setSubtitleTodayColor:(UIColor *)color
{
    if (color) {
        _subtitleColors[@(CFSCalendarCellStateToday)] = color;
    } else {
        [_subtitleColors removeObjectForKey:@(CFSCalendarCellStateToday)];
    }
    [self invalidateSubtitleTextColor];
}

- (UIColor *)subtitleTodayColor
{
    return _subtitleColors[@(CFSCalendarCellStateToday)];
}

- (void)setSubtitlePlaceholderColor:(UIColor *)color
{
    if (color) {
        _subtitleColors[@(CFSCalendarCellStatePlaceholder)] = color;
    } else {
        [_subtitleColors removeObjectForKey:@(CFSCalendarCellStatePlaceholder)];
    }
    [self invalidateSubtitleTextColor];
}

- (UIColor *)subtitlePlaceholderColor
{
    return _subtitleColors[@(CFSCalendarCellStatePlaceholder)];
}

- (void)setSubtitleWeekendColor:(UIColor *)color
{
    if (color) {
        _subtitleColors[@(CFSCalendarCellStateWeekend)] = color;
    } else {
        [_subtitleColors removeObjectForKey:@(CFSCalendarCellStateWeekend)];
    }
    [self invalidateSubtitleTextColor];
}

- (UIColor *)subtitleWeekendColor
{
    return _subtitleColors[@(CFSCalendarCellStateWeekend)];
}

- (void)setSelectionColor:(UIColor *)color
{
    if (color) {
        _backgroundColors[@(CFSCalendarCellStateSelected)] = color;
    } else {
        [_backgroundColors removeObjectForKey:@(CFSCalendarCellStateSelected)];
    }
    [self invalidateFillColors];
}

- (UIColor *)selectionColor
{
    return _backgroundColors[@(CFSCalendarCellStateSelected)];
}

- (void)setTodayColor:(UIColor *)todayColor
{
    if (todayColor) {
        _backgroundColors[@(CFSCalendarCellStateToday)] = todayColor;
    } else {
        [_backgroundColors removeObjectForKey:@(CFSCalendarCellStateToday)];
    }
    [self invalidateFillColors];
}

- (UIColor *)todayColor
{
    return _backgroundColors[@(CFSCalendarCellStateToday)];
}

- (void)setTodaySelectionColor:(UIColor *)todaySelectionColor
{
    if (todaySelectionColor) {
        _backgroundColors[@(CFSCalendarCellStateToday|CFSCalendarCellStateSelected)] = todaySelectionColor;
    } else {
        [_backgroundColors removeObjectForKey:@(CFSCalendarCellStateToday|CFSCalendarCellStateSelected)];
    }
    [self invalidateFillColors];
}

- (UIColor *)todaySelectionColor
{
    return _backgroundColors[@(CFSCalendarCellStateToday|CFSCalendarCellStateSelected)];
}

- (void)setEventDefaultColor:(UIColor *)eventDefaultColor
{
    if (![_eventDefaultColor isEqual:eventDefaultColor]) {
        _eventDefaultColor = eventDefaultColor;
        [self invalidateEventColors];
    }
}

- (void)setBorderDefaultColor:(UIColor *)color
{
    if (color) {
        _borderColors[@(CFSCalendarCellStateNormal)] = color;
    } else {
        [_borderColors removeObjectForKey:@(CFSCalendarCellStateNormal)];
    }
    [self invalidateBorderColors];
}

- (UIColor *)borderDefaultColor
{
    return _borderColors[@(CFSCalendarCellStateNormal)];
}

- (void)setBorderSelectionColor:(UIColor *)color
{
    if (color) {
        _borderColors[@(CFSCalendarCellStateSelected)] = color;
    } else {
        [_borderColors removeObjectForKey:@(CFSCalendarCellStateSelected)];
    }
    [self invalidateBorderColors];
}

- (UIColor *)borderSelectionColor
{
    return _borderColors[@(CFSCalendarCellStateSelected)];
}

- (void)setBorderRadius:(CGFloat)borderRadius
{
    borderRadius = MAX(0.0, borderRadius);
    borderRadius = MIN(1.0, borderRadius);
    if (_borderRadius != borderRadius) {
        _borderRadius = borderRadius;
        [self invalidateBorderRadius];
    }
}

- (void)setWeekdayTextColor:(UIColor *)weekdayTextColor
{
    if (![_weekdayTextColor isEqual:weekdayTextColor]) {
        _weekdayTextColor = weekdayTextColor;
        [self invalidateWeekdayTextColor];
    }
}

- (void)setHeaderTitleColor:(UIColor *)color
{
    if (![_headerTitleColor isEqual:color]) {
        _headerTitleColor = color;
        [self invalidateHeaderTextColor];
    }
}

- (void)setHeaderMinimumDissolvedAlpha:(CGFloat)headerMinimumDissolvedAlpha
{
    if (_headerMinimumDissolvedAlpha != headerMinimumDissolvedAlpha) {
        _headerMinimumDissolvedAlpha = headerMinimumDissolvedAlpha;
        [_calendar.header.collectionView.visibleCells makeObjectsPerformSelector:@selector(setNeedsLayout)];
        [_calendar.visibleStickyHeaders makeObjectsPerformSelector:@selector(setNeedsLayout)];
    }
}

- (void)setHeaderDateFormat:(NSString *)headerDateFormat
{
    if (![_headerDateFormat isEqual:headerDateFormat]) {
        _headerDateFormat = headerDateFormat;
        [_calendar invalidateHeaders];
    }
}

- (void)setAdjustsFontSizeToFitContentSize:(BOOL)adjustsFontSizeToFitContentSize
{
    if (_adjustsFontSizeToFitContentSize != adjustsFontSizeToFitContentSize) {
        _adjustsFontSizeToFitContentSize = adjustsFontSizeToFitContentSize;
        if (adjustsFontSizeToFitContentSize) {
            [self invalidateFonts];
        }
    }
}

- (UIFont *)preferredTitleFont
{
    return [UIFont fontWithName:_titleFontName size:_adjustsFontSizeToFitContentSize?_preferredTitleFontSize:_titleFontSize];
}

- (UIFont *)preferredSubtitleFont
{
    return [UIFont fontWithName:_subtitleFontName size:_adjustsFontSizeToFitContentSize?_preferredSubtitleFontSize:_subtitleFontSize];
}

- (UIFont *)preferredWeekdayFont
{
    return [UIFont fontWithName:_weekdayFontName size:_adjustsFontSizeToFitContentSize?_preferredWeekdayFontSize:_weekdayFontSize];
}

- (UIFont *)preferredHeaderTitleFont
{
    return [UIFont fontWithName:_headerTitleFontName size:_adjustsFontSizeToFitContentSize?_preferredHeaderTitleFontSize:_headerTitleFontSize];
}

- (void)adjustTitleIfNecessary
{
    if (!self.calendar.floatingMode) {
        if (_adjustsFontSizeToFitContentSize) {
            CGFloat factor       = (_calendar.scope==CFSCalendarScopeMonth) ? 6 : 1.1;
            _preferredTitleFontSize       = _calendar.collectionView.fs_height/3/factor;
            _preferredTitleFontSize       -= (_preferredTitleFontSize-CFSCalendarStandardTitleTextSize)*0.5;
            _preferredSubtitleFontSize    = _calendar.collectionView.fs_height/4.5/factor;
            _preferredSubtitleFontSize    -= (_preferredSubtitleFontSize-CFSCalendarStandardSubtitleTextSize)*0.75;
            _preferredHeaderTitleFontSize = _preferredTitleFontSize * 1.25;
            _preferredWeekdayFontSize     = _preferredTitleFontSize;
            
        }
    } else {
        _preferredHeaderTitleFontSize = 20;
        if (CFSCalendarDeviceIsIPad) {
            _preferredHeaderTitleFontSize = CFSCalendarStandardHeaderTextSize * 1.5;
            _preferredTitleFontSize = CFSCalendarStandardTitleTextSize * 1.3;
            _preferredSubtitleFontSize = CFSCalendarStandardSubtitleTextSize * 1.15;
            _preferredWeekdayFontSize = _preferredTitleFontSize;
        }
        CGFloat multiplier = 1+(_calendar.lineHeightMultiplier-1)/4;
        _preferredHeaderTitleFontSize *= multiplier;
        _preferredTitleFontSize *= multiplier;
        _preferredSubtitleFontSize *= multiplier;
        _preferredSubtitleFontSize *= multiplier;
    }
    
    // reload appearance
    [self invalidateFonts];
}

- (void)setCaseOptions:(CFSCalendarCaseOptions)caseOptions
{
    if (_caseOptions != caseOptions) {
        _caseOptions = caseOptions;
        [_calendar invalidateWeekdaySymbols];
        [_calendar invalidateHeaders];
    }
}

- (void)invalidateAppearance
{
    [self invalidateFonts];
    [self invalidateTextColors];
    [self invalidateBorderColors];
    [self invalidateFillColors];
}

- (void)invalidateFonts
{
    [self invalidateTitleFont];
    [self invalidateSubtitleFont];
    [self invalidateWeekdayFont];
    [self invalidateHeaderFont];
}

- (void)invalidateTextColors
{
    [self invalidateTitleTextColor];
    [self invalidateSubtitleTextColor];
    [self invalidateWeekdayTextColor];
    [self invalidateHeaderTextColor];
}

- (void)invalidateBorderColors
{
    [_calendar.collectionView.visibleCells makeObjectsPerformSelector:_cmd];
}

- (void)invalidateFillColors
{
    [_calendar.collectionView.visibleCells makeObjectsPerformSelector:_cmd];
}

- (void)invalidateEventColors
{
    [_calendar.collectionView.visibleCells makeObjectsPerformSelector:_cmd];
}

- (void)invalidateBorderRadius
{
    [_calendar.collectionView.visibleCells makeObjectsPerformSelector:_cmd];
}

- (void)invalidateTitleFont
{
    [_calendar.collectionView.visibleCells makeObjectsPerformSelector:_cmd];
}

- (void)invalidateSubtitleFont
{
    [_calendar.collectionView.visibleCells makeObjectsPerformSelector:_cmd];
}

- (void)invalidateTitleTextColor
{
    [_calendar.collectionView.visibleCells makeObjectsPerformSelector:_cmd];
}

- (void)invalidateSubtitleTextColor
{
    [_calendar.collectionView.visibleCells makeObjectsPerformSelector:_cmd];
}

- (void)invalidateWeekdayFont
{
    [_calendar invalidateWeekdayFont];
    [_calendar.visibleStickyHeaders makeObjectsPerformSelector:_cmd];
}

- (void)invalidateWeekdayTextColor
{
    [_calendar invalidateWeekdayTextColor];
    [_calendar.visibleStickyHeaders makeObjectsPerformSelector:_cmd];
}

- (void)invalidateHeaderFont
{
    [_calendar.header.collectionView.visibleCells makeObjectsPerformSelector:_cmd];
    [_calendar.visibleStickyHeaders makeObjectsPerformSelector:_cmd];
}

- (void)invalidateHeaderTextColor
{
    [_calendar.header.collectionView.visibleCells makeObjectsPerformSelector:_cmd];
    [_calendar.visibleStickyHeaders makeObjectsPerformSelector:_cmd];
}

@end


@implementation CFSCalendarAppearance (Deprecated)

- (void)setCellStyle:(CFSCalendarCellStyle)cellStyle
{
    self.cellShape = (CFSCalendarCellShape)cellStyle;
}

- (CFSCalendarCellStyle)cellStyle
{
    return (CFSCalendarCellStyle)self.cellShape;
}

- (void)setUseVeryShortWeekdaySymbols:(BOOL)useVeryShortWeekdaySymbols
{
    _caseOptions &= 15;
    self.caseOptions |= (useVeryShortWeekdaySymbols*CFSCalendarCaseOptionsWeekdayUsesSingleUpperCase);
}

- (BOOL)useVeryShortWeekdaySymbols
{
    return (_caseOptions & (15<<4) ) == CFSCalendarCaseOptionsWeekdayUsesSingleUpperCase;
}

- (void)setAutoAdjustTitleSize:(BOOL)autoAdjustTitleSize
{
    self.adjustsFontSizeToFitContentSize = autoAdjustTitleSize;
}

- (BOOL)autoAdjustTitleSize
{
    return self.adjustsFontSizeToFitContentSize;
}

- (void)setTitleTextSize:(CGFloat)titleTextSize
{
    self.titleFont = [UIFont fontWithName:_titleFontName size:titleTextSize];
}

- (CGFloat)titleTextSize
{
    return _titleFontSize;
}

- (void)setSubtitleTextSize:(CGFloat)subtitleTextSize
{
    self.subtitleFont = [UIFont fontWithName:_subtitleFontName size:subtitleTextSize];
}

- (CGFloat)subtitleTextSize
{
    return _subtitleFontSize;
}

- (void)setWeekdayTextSize:(CGFloat)weekdayTextSize
{
    self.weekdayFont = [UIFont fontWithName:_weekdayFontName size:weekdayTextSize];
}

- (CGFloat)weekdayTextSize
{
    return _weekdayFontSize;
}

- (void)setHeaderTitleTextSize:(CGFloat)headerTitleTextSize
{
    self.headerTitleFont = [UIFont fontWithName:_headerTitleFontName size:headerTitleTextSize];
}

- (CGFloat)headerTitleTextSize
{
    return _headerTitleFontSize;
}

- (void)setAdjustsFontSizeToFitCellSize:(BOOL)adjustsFontSizeToFitCellSize
{
    self.adjustsFontSizeToFitContentSize = adjustsFontSizeToFitCellSize;
}

- (BOOL)adjustsFontSizeToFitCellSize
{
    return self.adjustsFontSizeToFitContentSize;
}

- (void)setTitleVerticalOffset:(CGFloat)titleVerticalOffset
{
    self.titleOffset = CGPointMake(0, titleVerticalOffset);
}

- (CGFloat)titleVerticalOffset
{
    return self.titleOffset.y;
}

- (void)setSubtitleVerticalOffset:(CGFloat)subtitleVerticalOffset
{
    self.subtitleOffset = CGPointMake(0, subtitleVerticalOffset);
}

- (CGFloat)subtitleVerticalOffset
{
    return self.subtitleOffset.y;
}

- (void)setEventColor:(UIColor *)eventColor
{
    self.eventDefaultColor = eventColor;
}

- (UIColor *)eventColor
{
    return self.eventDefaultColor;
}

- (void)setCellShape:(CFSCalendarCellShape)cellShape
{
    self.borderRadius = 1-cellShape;
}

- (CFSCalendarCellShape)cellShape
{
    return self.borderRadius==1.0?CFSCalendarCellShapeCircle:CFSCalendarCellShapeRectangle;
}

@end


