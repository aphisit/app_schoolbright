//
//  NewsMessageTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsMessageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconFile;
@property (weak, nonatomic) IBOutlet UILabel *statementreadLabel;
@property (weak, nonatomic) IBOutlet UIImageView *schoolLogo;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
