//
//  IDValueModel.m
//  JabjaiApp
//
//  Created by mac on 10/28/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "IDValueModel.h"

@implementation IDValueModel

-(NSNumber *)getID {
    return _ID;
}

-(NSString *)getVal {
    return _Value;
}

@end
