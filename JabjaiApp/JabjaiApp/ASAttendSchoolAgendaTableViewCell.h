//
//  ASAttendSchoolAgendaTableViewCell.h
//  JabjaiApp
//
//  Created by Mac on 14/9/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ASAttendSchoolAgendaTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusInLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusOutLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeInLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeOutLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeInStrLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeOutStrLabel;


@property (weak, nonatomic) IBOutlet UIView *inTimeView;
@property (weak, nonatomic) IBOutlet UIView *outTimeView;
@property (weak, nonatomic) IBOutlet UIView *ShadowContentView;
-(void) setColorStatusInView:(int)number;
-(void) setColorStatusOutView:(int)number;
@end

NS_ASSUME_NONNULL_END
