//
//  CallChackSignUpPin.h
//  JabjaiApp
//
//  Created by toffee on 6/13/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CallChackSignUpPin;

@protocol CallChackSignUpPinDelegate <NSObject>

- (void)callCheckSignUpPin:(NSString*)status success:(BOOL)success;

@end
@interface CallChackSignUpPin : NSObject
@property (nonatomic, weak) id<CallChackSignUpPinDelegate> delegate;
- (void)call:(long long)userId schoolid:(long long)schoolid;
@end
