//
//  ReportBehaviorScoreDetailViewController.m
//  JabjaiApp
//
//  Created by mac on 4/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportBehaviorScoreDetailViewController.h"
#import "ReportBehaviorScoreViewController.h"
#import "Utils.h"
#import "UserData.h"

@interface ReportBehaviorScoreDetailViewController () {
    UIColor *greenColor;
    UIColor *redColor;
    NSDateFormatter *timeFormatter;
}

@end

@implementation ReportBehaviorScoreDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLanguage];
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    [self.remainingScoreView layoutIfNeeded];
       CAGradientLayer  *remainingScoreGradient = [Utils getGradientColorStatus:@"green"];
       remainingScoreGradient.frame = self.remainingScoreView.bounds;
    [self.remainingScoreView.layer insertSublayer:remainingScoreGradient atIndex:0];
    
    greenColor = [UIColor colorWithRed:12/255.0 green:190/255.0 blue:64/255.0 alpha:1.0];
    redColor = [UIColor colorWithRed:255/255.0 green:57/255.0 blue:56/255.0 alpha:1.0];
    
    timeFormatter = [Utils getDateFormatter];
    //[timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [timeFormatter setDateFormat:@"HH:mm"];
    }
}

- (void) setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_BEHAVIOR_REP",nil,[Utils getLanguage],nil);
    self.headerScoreLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_BEHAVIOR_REP_SCORE",nil,[Utils getLanguage],nil);
    self.headerRecorderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_BEHAVIOR_REP_SAVE",nil,[Utils getLanguage],nil);
    self.headerNoteLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_BEHAVIOR_REP_MEAN",nil,[Utils getLanguage],nil);
    self.headerRemainingScoreLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_STU_BEHAVIOR_REP_REMAINING_SORE",nil,[Utils getLanguage],nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    CGFloat max = MAX(self.scoreLabel.frame.size.width, self.scoreLabel.frame.size.height);
    self.scoreLabel.layer.cornerRadius = max / 2.0;
    self.scoreLabel.layer.masksToBounds = YES;
    [self updateUI];
}

- (void)updateUI {
    if(self.selectedHistoryModel != nil) {
        self.behaviorNameLabel.text = [self.selectedHistoryModel getBehaviorName];
        self.datetimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:[self.selectedHistoryModel getDateTime]], [timeFormatter stringFromDate:[self.selectedHistoryModel getDateTime]]];
        
        NSNumber *myDoubleNumber = [NSNumber numberWithDouble:[self.selectedHistoryModel getBehaviorScore]];
        if([self.selectedHistoryModel getBehaviorType] == 0) { // type add
            self.scoreLabel.text = [NSString stringWithFormat:@"+%@", myDoubleNumber];
            self.scoreLabel.backgroundColor = greenColor;
        }
        else { // type reduce
            self.scoreLabel.text = [NSString stringWithFormat:@"-%@", myDoubleNumber];
            self.scoreLabel.backgroundColor = redColor;
        }
        
        if([self.selectedHistoryModel getRecorder] != nil && [[self.selectedHistoryModel getRecorder] length] > 0) {
            self.recorderLabel.text = [self.selectedHistoryModel getRecorder];
        }
        else {
            self.recorderLabel.text = @"-";
        }
        
        if([self.selectedHistoryModel getRemark] != nil && [[self.selectedHistoryModel getRemark] length] > 0) {
            self.remarkLabel.text = [self.selectedHistoryModel getRemark];
        }
        else {
            self.remarkLabel.text = @"-";
        }
        NSNumber *lastScore = [NSNumber numberWithDouble:self.remainingScore];
        self.remainingScoreLabel.text = [NSString stringWithFormat:@"%@", lastScore];
    }
    else {
        self.behaviorNameLabel.text = nil;
        self.datetimeLabel.text = nil;
        self.scoreLabel.text = @"+0";
        self.scoreLabel.backgroundColor = greenColor;
        self.recorderLabel.text = @"-";
        self.remarkLabel.text = @"-";
        self.remainingScoreLabel.text = @"0";
    }
}

- (IBAction)moveBack:(id)sender {
    
    ReportBehaviorScoreViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportBehaviorScoreStoryboard"];
    
    viewController.bsHistoryArray = _bsHistoryArray;
    viewController.remainingScore = self.remainingScore;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
