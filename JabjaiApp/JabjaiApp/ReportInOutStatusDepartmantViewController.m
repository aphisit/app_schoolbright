//
//  ReportInOutStatusDepartmantViewController.m
//  JabjaiApp
//
//  Created by toffee on 6/3/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutStatusDepartmantViewController.h"
#import "UserData.h"
#import "Utils.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ReportInOutStatusDepartmantViewController (){
    NSMutableArray<ReportInOutStatusDepartmentModel *>  *departmentStatusOnTimeArray, *departmentStatusLateArray, *departmentStatusAbsenArray, *departmentStatusSickArray, *departmentStatusPersonalArray, *departmentAllArray, *dataDepartmentArray,*departmentStatusAutherArray,*departmentStatusHoridayArray;
    
    NSInteger statusClickInterger;
}
@property (strong,nonatomic) CallReportInOutStatusDepartmantAPI *callReportInOutStatusDepartmantAPI;
@end
static NSString *identifier = @"Cell";
@implementation ReportInOutStatusDepartmantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ATTENDCLASS",nil,[Utils getLanguage],nil);
    [self.showNoStudent setText:NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_NO_STUDENT",nil,[Utils getLanguage],nil)];
    statusClickInterger = 0;
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportInOutStatusDepartmantCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:identifier];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportInOutStatusDepartmentTableViewCell class]) bundle:nil] forCellReuseIdentifier:identifier];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self doCallReportInOutStatusDepartmantAPI:[UserData getSchoolId] dayReports:[Utils datePunctuateStringFormat:self.dayReports] departmentId:self.departmentId];
    
    
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}

- (void)doCallReportInOutStatusDepartmantAPI:(long long)schoolId dayReports:(NSString*)dayReports departmentId:(long long)departmentId{
    [self showIndicator];
    if(self.callReportInOutStatusDepartmantAPI != nil) {
        self.callReportInOutStatusDepartmantAPI = nil;
    }
    self.callReportInOutStatusDepartmantAPI = [[CallReportInOutStatusDepartmantAPI alloc] init];
    self.callReportInOutStatusDepartmantAPI.delegate = self;
    [self.callReportInOutStatusDepartmantAPI call:schoolId dayReports:dayReports departmentId:departmentId] ;
}

- (void)callReportInOutStatusDepartmantAPI:(CallReportInOutStatusDepartmantAPI *)classObj dataDepartment:(NSMutableArray<ReportInOutStatusDepartmentModel *> *)dataDepartment success:(BOOL)success{
    if (success) {
        NSLog(@"xxxxx");
        [self stopIndicator];
        departmentAllArray = [[NSMutableArray alloc] init];
        departmentStatusOnTimeArray = [[NSMutableArray alloc] init];
        departmentStatusLateArray = [[NSMutableArray alloc] init];
        departmentStatusAbsenArray = [[NSMutableArray alloc] init];
        departmentStatusPersonalArray = [[NSMutableArray alloc] init];
        departmentStatusSickArray = [[NSMutableArray alloc] init];
        departmentStatusAutherArray = [[NSMutableArray alloc] init];
        departmentStatusHoridayArray = [[NSMutableArray alloc] init];
        dataDepartmentArray = dataDepartment;
        departmentAllArray = dataDepartment;
        
       
        
        for (int i = 0; i<departmentAllArray.count; i++) {
            ReportInOutStatusDepartmentModel *model = [[ReportInOutStatusDepartmentModel alloc] init];
            model = [departmentAllArray objectAtIndex:i];
            if ([[model getStatus] integerValue] == 0) {
                [departmentStatusOnTimeArray addObject:model];
            }
            else if ([[model getStatus] integerValue] == 1){
                [departmentStatusLateArray addObject:model];
                
            }
            else if ([[model getStatus] integerValue] == 3){
                [departmentStatusAbsenArray addObject:model];
                
            }
            else if ([[model getStatus] integerValue] == 10){
                [departmentStatusPersonalArray addObject:model];
                
            }
            else if ([[model getStatus] integerValue] == 11){
                [departmentStatusSickArray addObject:model];
                
            }
            else if ([[model getStatus] integerValue] == 9 || [[model getStatus] integerValue] == 8){
                [departmentStatusHoridayArray addObject:model];
            }
            else{
                [departmentStatusAutherArray addObject:model];
            }
            
        }
        
        self.amountAllLabel.text = [NSString stringWithFormat:@"%@ %d %@",NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_AMOUNT_PERSONNEL",nil,[Utils getLanguage],nil) ,dataDepartmentArray.count, NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.dateLabel.text = [Utils getThaiDateFormatWithDate:self.dayReports];
        self.departmentLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PERSONALTYPE",nil,[Utils getLanguage],nil),self.departmentName];
        
        [self.collectionView reloadData];
        [self.tableView reloadData];
        
    }
}


- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}

- (UICollectionViewCell *) collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
   
        ReportInOutStatusDepartmantCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (indexPath.row == 0) {
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ALL",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",dataDepartmentArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    }
    else if (indexPath.row == 1){
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ONTIME",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",departmentStatusOnTimeArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    }
    else if (indexPath.row == 2){
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_LATE",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",departmentStatusLateArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    }
    else if (indexPath.row == 3){
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ABSENCE",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",departmentStatusAbsenArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    }
    else if (indexPath.row == 4){
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_SICK",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",departmentStatusSickArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    }
    else if (indexPath.row == 5){
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PERSONAL",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",departmentStatusPersonalArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    }
    else if (indexPath.row == 6){
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_OTHER",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",departmentStatusAutherArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    }
    else if (indexPath.row == 7){
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_HOLIDAY",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",departmentStatusHoridayArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    }
    
    [cell setBackgroundClick:indexPath.row numberClick:statusClickInterger];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    statusClickInterger = indexPath.row;
    
    if (statusClickInterger == 0) {
        departmentAllArray =  dataDepartmentArray ;
    }else if (statusClickInterger == 1){
        departmentAllArray = departmentStatusOnTimeArray;
    }else if (statusClickInterger == 2){
        departmentAllArray = departmentStatusLateArray;
    }else if (statusClickInterger == 3){
        departmentAllArray = departmentStatusAbsenArray;
    }else if (statusClickInterger == 4){
        departmentAllArray = departmentStatusSickArray;
    }else if (statusClickInterger == 5){
        departmentAllArray = departmentStatusPersonalArray;
    }else if (statusClickInterger == 6){
        departmentAllArray = departmentStatusAutherArray;
    }else if (statusClickInterger == 7){
        departmentAllArray = departmentStatusHoridayArray;
    }
    
    [self.collectionView reloadData];
    [self.tableView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (departmentAllArray.count == 0) {
        self.showNoStudent.hidden = NO;
    }else{
        self.showNoStudent.hidden = YES;
    }
   
    return departmentAllArray.count;
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ReportInOutStatusDepartmentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    ReportInOutStatusDepartmentModel *model  = [departmentAllArray objectAtIndex:indexPath.row];
    cell.nameDepartmentLabel.text = [model getNameDepartment];
    cell.runNumberLabel.text = [NSString stringWithFormat:@"%d.",indexPath.row+1];
    [cell updateStatus:[[model getStatus]intValue]];

    if ([model getPictuer] == NULL || [[model getPictuer] isEqual:@""]) {
        cell.picDepartmentImage.image = [UIImage imageNamed:@"ic_user_info"];
    }
    else{
        [cell.picDepartmentImage sd_setImageWithURL:[NSURL URLWithString:[model getPictuer]]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}
#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)moveBack:(id)sender {
    ReportInOutAllDataSelectDepartmentViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutAllDataSelectDepartmentStoryboard"];
    viewController.date = self.dayReports;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
