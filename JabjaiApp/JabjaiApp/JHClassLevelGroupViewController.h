//
//  JHClassLevelGroupViewController.h
//  JabjaiApp
//
//  Created by toffee on 11/21/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SchoolClassroomModel.h"
#import "SchoolLevelModel.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "TableListDialog.h"
#import "SWRevealViewController.h"
#import "CallJHGetSubjectModeGroupAPI.h"

@interface JHClassLevelGroupViewController : UIViewController<UITextFieldDelegate,TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate,  CallJHGetSubjectModeGroupAPIDelegate>

// The dialog
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (strong, nonatomic) NSArray<JHSubjectModel *> *subjectArray;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long subjectId;

@property (nonatomic) NSMutableArray* levelId;
@property (nonatomic) NSInteger sendtype;



@property (weak, nonatomic) IBOutlet UITextField *classLavelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classRoomTextField;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
- (IBAction)actionNext:(id)sender;
- (IBAction)openDrawer:(id)sender;

@end
