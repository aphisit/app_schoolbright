//
//  SNDetailMessageBoxModel.h
//  JabjaiApp
//
//  Created by Mac on 27/4/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SNDetailMessageBoxModel : NSObject
@property (nonatomic) long long messageID;
@property (nonatomic) long long userID;
@property (nonatomic) int messageType; //1->Attendance, 2->Purchasing, 3->Topup, 4->AbsenceRequest, 5->News, 6->Homework
@property (nonatomic) int status; //0 -> unread, 1 -> read
@property (nonatomic) int statusRead; //0 -> unread, 1 -> read
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *replyResult;
@property (nonatomic) BOOL file;
@property (nonatomic) NSArray *replyButton;
@end

NS_ASSUME_NONNULL_END
