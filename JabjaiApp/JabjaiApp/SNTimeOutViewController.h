//
//  SNTimeOutViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWRevealViewController.h"
#import "CellUpdateSendNewsPOSTAPI.h"
#import "SNTimeOutImage.h"
#import "SNConfirmNewsViewController.h"
#import "SNIconFileCollectionViewCell.h"
#import "SlideMenuController.h"
#import "SelectOneDayCalendarViewController.h"
#import "SNPickerTimeViewController.h"
#import "PhotoAlbumLibary.h"

@interface SNTimeOutViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate,SlideMenuControllerDelegate, SelectOneDayCalendarDelegate,SNPickerTimeDelegate, PhotoAlbumLibaryDelegate, SNIconFileCollectionViewCellDelegate>{
    UIDatePicker *datePicker;
    NSInteger chackDateTime;
    BOOL chackRadioSendNow;
    BOOL chackRadioSetTime;

}

@property (nonatomic) NSInteger sendType;
@property (nonatomic) NSInteger newsType;
@property (nonatomic) NSInteger sendGroup;
@property (nonatomic) NSInteger sendTimeType;
@property (nonatomic) NSString* newsDetail;

@property (nonatomic) NSMutableArray* level2id;
@property (nonatomic) NSMutableArray* studentid;
@property (nonatomic) NSMutableArray* employeesid;
@property (nonatomic) NSMutableArray* userAcceptArray;
@property (nonatomic) NSMutableArray* imagesArray;
@property (nonatomic) NSMutableArray *attachmentArray;

//@property (nonatomic) NSString* sendDay;
@property (nonatomic) NSDate* dateSend; //ตั้งวันที่ส่ง
@property (nonatomic) NSString* timeSend; //ตั้งเวลาส่ง

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerSendNewsLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSendNowLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSetTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDescription;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachFileLabel;

@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;
@property (weak, nonatomic) IBOutlet UIView *shadowDetailView;
@property (weak, nonatomic) IBOutlet UIView *shadowImageView;
@property (weak, nonatomic) IBOutlet UIView *shadowFileView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UITextView *detail;
@property (weak, nonatomic) IBOutlet UITextField *dateTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *timeTextFiled;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpeceToSecond;


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *selectFileCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnSendNow;
@property (weak, nonatomic) IBOutlet UIButton *btnSettime;

@property (weak, nonatomic) IBOutlet UIView *sendImageView;
- (IBAction)selectImage:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (weak, nonatomic) IBOutlet UIButton *selectImageBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectFileBtn;

- (IBAction)sendNowRadio:(id)sender;
- (IBAction)setTimeRadio:(id)sender;
- (IBAction)selectFile:(id)sender;
- (IBAction)backToMenu:(id)sender;
- (IBAction)actionSubmit:(id)sender;



@end
