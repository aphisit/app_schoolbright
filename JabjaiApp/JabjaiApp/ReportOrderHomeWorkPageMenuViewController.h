//
//  ReportOrderHomeWorkPageMenuViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"
#import "CAPSPageMenu.h"

#import "ReportOrderHomeWorkDetailViewController.h"
#import "ReportOrderHomeworkDetailPersonViewController.h"
#import "ReportOrderHomeWorkRecieverViewController.h"

@interface ReportOrderHomeWorkPageMenuViewController : UIViewController <SlideMenuControllerDelegate, CAPSPageMenuDelegate>

@property (assign, nonatomic) long long homeworkID;
@property (nonatomic, strong) NSString *sortData;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;

@property (nonatomic, strong) ReportOrderHomeworkDetailPersonViewController *detailController;
@property (nonatomic, strong) ReportOrderHomeWorkRecieverViewController *recieverController;

- (IBAction)moveBack:(id)sender;

@end
