//
//  UserHealthViewController.m
//  JabjaiApp
//
//  Created by mac on 1/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "UserHealthViewController.h"
#import "UserInfoHealthModel.h"

#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"
#import "Constant.h"

@interface UserHealthViewController (){
    
    int heightInCms;
    int weightInKgs;
    
    float heightMs;  //Meters
    float weightKgs; //kilograms
    
    NSInteger connectCounter;
    
    NSMutableArray<UserInfoHealthModel *> *healthArray;
    
    UserInfoHealthModel *userinfoHealthModel;
    NSBundle *myLangBundle;
    
}

@end

@implementation UserHealthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setLanguage];
    [self clearDisplay];
    [self getHealthData];
    
}

-(void)viewDidLayoutSubviews{
    
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLanguage{
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource: [UserData getChangLanguage] ofType:@"lproj"]];
    self.genderHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_SEX",nil,myLangBundle,nil);
    self.ageHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_AGE",nil,myLangBundle,nil);
    self.heightHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_HIGHT",nil,myLangBundle,nil);
    self.weightHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_WEIGHT",nil,myLangBundle,nil);
    self.bloodTypeHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_BLOOD",nil,myLangBundle,nil);
    self.diseaseHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_CONGENITAL",nil,myLangBundle,nil);
    [self.temperatureBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_INFO_CHECK_TEMPERATURE",nil,myLangBundle,nil) forState:UIControlStateNormal];
 
}

- (void)getHealthData{
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getHealthDataWithUserID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getHealthData];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getHealthData];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getHealthData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if (healthArray != nil) {
                    [healthArray removeAllObjects];
                    healthArray = nil;
                }
                
                healthArray = [[NSMutableArray alloc] init];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];

                    NSMutableString *gender, *bloodtype, *disease;
                    
                    if (![[dataDict objectForKey:@"gender"] isKindOfClass:[NSNull class]]) {
                        gender = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"gender"]];
                    }
                    else{
                        gender = [[NSMutableString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil)];
                    }
                    
                    if (![[dataDict objectForKey:@"bloodType"] isKindOfClass:[NSNull class]]) {
                        bloodtype = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"bloodType"]];
                    }
                    
                    else{
                        bloodtype = [[NSMutableString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil)];
                    }
                    
                    if (![[dataDict objectForKey:@"disease"] isKindOfClass:[NSNull class]]) {
                        disease = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"disease"]];
                    }
                    
                    else{
                        disease = [[NSMutableString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil)];
                    }
                    
                    NSMutableString *ageText, *weightText, *heightText;
                    
                    if (![[dataDict objectForKey:@"age"] isKindOfClass:[NSNull class]]) {
                        ageText = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"age"]];
                    }
                    
                    else{
                        ageText = [[NSMutableString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil)];
                    }
                    
                    if (![[dataDict objectForKey:@"weight"] isKindOfClass:[NSNull class]]) {
                        weightText = [[NSMutableString alloc] initWithFormat:@"%@ ", [dataDict objectForKey:@"weight"]];
                        [weightText appendString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_HEALTH_KG",nil,[Utils getLanguage],nil)];
                    }
                    
                    else{
                        weightText = [[NSMutableString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil)];
                        
                    }
                    
                    if (![[dataDict objectForKey:@"height"] isKindOfClass:[NSNull class]]) {
                        heightText = [[NSMutableString alloc] initWithFormat:@"%@ ", [dataDict objectForKey:@"height"]];
                        [heightText appendString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_HEALTH_CM",nil,[Utils getLanguage],nil)];
                    }
                    
                    else{
                        heightText = [[NSMutableString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil)];
                        
                    }
                    
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) gender);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) bloodtype);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) disease);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) ageText);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) weightText);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) heightText);
                    
                    UserInfoHealthModel *model = [[UserInfoHealthModel alloc] init];
                    
                    model.sex = gender;
                    model.bloddType = bloodtype;
                    model.disease = disease;
                    model.ageText = ageText;
                    model.weightText = weightText;
                    model.heightText = heightText;
                    [self->healthArray addObject:model];
                    self->userinfoHealthModel = model;
                    
                    [self performData];
                }
                
                
            }
        }
        
    }
];
    
}

- (void)performData{
 
    
    if (userinfoHealthModel != nil) {
        
//        heightInCms = userinfoHealthModel.height;
//        weightInKgs = userinfoHealthModel.weight;
        
//        heightMs = (float) heightInCms *0.01;
//        weightKgs = (float) weightInKgs;
        
        heightMs = [userinfoHealthModel.heightText floatValue] *0.01;
        weightKgs = [userinfoHealthModel.weightText floatValue];
        
        float BMI = (weightKgs / (heightMs * heightMs));
        
        self.genderLabel.text = userinfoHealthModel.sex;
        self.ageLabel.text = userinfoHealthModel.ageText;
//        self.ageLabel.text = [NSString stringWithFormat:@"%d" , userinfoHealthModel.age];
        self.bloodTypeLabel.text = userinfoHealthModel.bloddType;
        
        self.heightLabel.text =  userinfoHealthModel.heightText ;
        self.weightLabel.text = userinfoHealthModel.weightText;
        
//        self.heightLabel.text = [NSString stringWithFormat:@"%d" , userinfoHealthModel.height];
//        self.weightLabel.text = [NSString stringWithFormat:@"%d" , userinfoHealthModel.weight];
        self.diseaseLabel.text = userinfoHealthModel.disease;
        
        if (BMI >= 30) {
            self.resultLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_VERY_OVERWEIGHT",nil,[Utils getLanguage],nil);
            self.levelLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_OBESE_CLASS_TREE",nil,[Utils getLanguage],nil);
            self.colorLevel.backgroundColor = [UIColor colorWithRed:1.00 green:0.00 blue:0.00 alpha:1.0];
        }
        else if (BMI >= 25){
            self.resultLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_OVERWEIGHT",nil,[Utils getLanguage],nil);
            self.levelLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_OBESE_CLASS_TWO",nil,[Utils getLanguage],nil);
            self.colorLevel.backgroundColor = [UIColor colorWithRed:0.96 green:0.42 blue:0.13 alpha:1.0];
        }
        else if (BMI >= 22){
            self.resultLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_OVERWEIGHTT",nil,[Utils getLanguage],nil);
            self.levelLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_OBESE_CLASS_ONE",nil,[Utils getLanguage],nil);
            self.colorLevel.backgroundColor = [UIColor colorWithRed:0.97 green:0.66 blue:0.16 alpha:1.0];
        }
        else if (BMI >= 18.6){
            self.resultLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_NORMAL",nil,[Utils getLanguage],nil);
            self.levelLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_HEALTHY",nil,[Utils getLanguage],nil);
            self.colorLevel.backgroundColor = [UIColor colorWithRed:0.05 green:0.78 blue:0.25 alpha:1.0];
        }
        else if (BMI <= 18.5){
            self.resultLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_UNDERWEIGHT",nil,[Utils getLanguage],nil);
            self.levelLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_INFO_SEVERELY_UNDERWEIGHT",nil,[Utils getLanguage],nil);
            self.colorLevel.backgroundColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.0];
            self.resultLabel.textColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:1.0];
            self.levelLabel.textColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:1.0];
        }
        else{
            self.resultLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil);
            self.colorLevel.backgroundColor = [UIColor colorWithRed: 0.40 green: 0.41 blue: 0.41 alpha: 1.00];
//            self.levelLabel.text = nil;
            [self.levelLabel removeFromSuperview];
        }
        
    }
    else{
        [self clearDisplay];
    }
    
}


- (void)clearDisplay {
    self.genderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil);
    self.ageLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil);
    self.heightLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil);
    self.weightLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil);
    self.bloodTypeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil);
    self.diseaseLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil);
    self.resultLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_NO_DATA",nil,[Utils getLanguage],nil);
//    [self.levelLabel removeFromSuperview];
}


- (IBAction)nextAction:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(responseUserHealthHitorysTemperatureViewController)]) {
        [self.delegate responseUserHealthHitorysTemperatureViewController];
    }
}
@end
