//
//  UserHealthHistoryModel.m
//  JabjaiApp
//
//  Created by Mac on 1/7/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "UserHealthHistoryModel.h"

@implementation UserHealthHistoryModel
@synthesize number = _number;
@synthesize time = _time;
@synthesize date = _date;
@synthesize temPerature = _temPerature;

-(void)setNumber:(NSInteger)number{
    _number = number;
}
-(void)setTime:(NSString *)time{
    _time = time;
}
-(void)setDate:(NSString *)date{
    _date = date;
}
-(void)setTemPerature:(NSString *)temPerature{
    _temPerature = temPerature;
}

- (NSInteger)getNumber{
    return _number;
}
- (NSString*)getTime{
    return _time;
}
- (NSString*)getDate{
    return _date;
}
- (NSString*)getTemperature{
    return _temPerature;
}


@end
