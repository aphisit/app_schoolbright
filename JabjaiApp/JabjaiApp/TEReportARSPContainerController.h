//
//  TEReportARSPContainerController.h
//  JabjaiApp
//
//  Created by toffee on 11/15/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//


@import ARSlidingPanel;
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"



@interface TEReportARSPContainerController : ARSPContainerController

//Global variables
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;

@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long selectedClassLevelId;
@property (nonatomic) long long selectedClassroomId;

@end
