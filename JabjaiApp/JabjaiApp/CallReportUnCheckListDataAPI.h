//
//  CallReportUnCheckListDataAPI.h
//  JabjaiApp
//
//  Created by toffee on 21/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReportUnCheckListDataModel.h"

NS_ASSUME_NONNULL_BEGIN
@class CallReportUnCheckListDataAPI;

@protocol CallReportUnCheckListDataAPIDelegate <NSObject>
@optional
- (void)callReportUnCheckListDataAPI:(CallReportUnCheckListDataAPI *)classObj dateTimeArray:(NSMutableArray<ReportUnCheckListDataModel *>*)dataArray success:(BOOL)success;
@end
@interface CallReportUnCheckListDataAPI : NSObject
@property (nonatomic, weak) id<CallReportUnCheckListDataAPIDelegate> delegate;
- (void)call:(long long)schoolId date:(NSString*)date;
@end

NS_ASSUME_NONNULL_END
