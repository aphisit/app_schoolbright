//
//  MenuReportTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 4/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "MenuReportTableViewCell.h"

@implementation MenuReportTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.iconReport.layer.cornerRadius = self.iconReport.frame.size.width / 2;
    self.iconReport.clipsToBounds = YES;
    self.iconReport.layer.shouldRasterize = YES;
    
    //UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    // Here pass new size you need
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
