//
//  ReportInOutAllDataSelectDepartmentViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportInOutAllDataSelectDepartmentViewController.h"
#import "ReportListTeacherViewController.h"
#import "UserData.h"
#import "Utils.h"

@interface ReportInOutAllDataSelectDepartmentViewController (){
    NSMutableArray *expandedSections;
    NSArray <ReportInOutAllDataSelectDepartmantModel*> *dataDepartmantArray;
}
@property (strong, nonatomic) CallReportInOutAllDataSelectDepartmantAPI *callReportInOutAllDataSelectDepartmantAPI;
@end

static NSString *cellIdentifier = @"Cell";
@implementation ReportInOutAllDataSelectDepartmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    [self setLanguage];
    expandedSections = [[NSMutableArray alloc] init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportInOutAllDataSelectDepartmantTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self doCallDataSelecDepartmant:[UserData getSchoolId] date:[Utils datePunctuateStringFormat:self.date]];
}
-(void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ATTENDCLASS",nil,[Utils getLanguage],nil);
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}

- (void)doCallDataSelecDepartmant:(long long)schoolId date:(NSString *)date {
    [self showIndicator];
    if(self.callReportInOutAllDataSelectDepartmantAPI != nil) {
        self.callReportInOutAllDataSelectDepartmantAPI = nil;
    }
    
    self.callReportInOutAllDataSelectDepartmantAPI = [[CallReportInOutAllDataSelectDepartmantAPI alloc] init];
    self.callReportInOutAllDataSelectDepartmantAPI.delegate = self;
    [self.callReportInOutAllDataSelectDepartmantAPI call:schoolId date:date];
    
}

- (void)callReportInOutAllDataSelectDepartmantAPI:(CallReportInOutAllDataSelectDepartmantAPI *)classObj dataDepaermantArray:(NSMutableArray<ReportInOutAllDataSelectDepartmantModel *> *)dataDepaermantArray success:(BOOL)success{
    if (success) {
        [self stopIndicator];
        dataDepartmantArray = dataDepaermantArray;
    }
    [self.tableView reloadData];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataDepartmantArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReportInOutAllDataSelectDepartmantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    ReportInOutAllDataSelectDepartmantModel *model = [[ReportInOutAllDataSelectDepartmantModel alloc] init];
    model = [dataDepartmantArray objectAtIndex:indexPath.row];
    cell.dayReport = self.date;
    cell.departmentId = [model getDepartmantId];
    cell.departmentName = [model getDepartmantName];
    cell.departmantNameLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PERSONALTYPE",nil,[Utils getLanguage],nil),[model getDepartmantName]];
    
    cell.amountLabel.text = [NSString stringWithFormat:@"%@ %d %@",NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_TOTAL_PERSONNEL",nil,[Utils getLanguage],nil),[model getAmountDepartmant],NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    cell.dateLabel.text = [Utils getThaiDateFormatWithDate:self.date];
    
    if([expandedSections containsObject:@(indexPath.row)]) {
        cell.graphView.hidden = false;
         [cell setStatusGraph:[model getStatus]];
    }else{
        cell.graphView.hidden = true;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([expandedSections containsObject:@(indexPath.row)]) {
        return 850;
    }else{
        return 101;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if([expandedSections containsObject:@(indexPath.row)]) {
        [expandedSections removeObject:@(indexPath.row)];
    }
    else {
        [expandedSections addObject:@(indexPath.row)];
    }
    [tableView reloadData];
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)moveBack:(id)sender {
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutAllDataStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
