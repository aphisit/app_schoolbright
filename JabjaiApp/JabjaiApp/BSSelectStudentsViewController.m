//
//  BSSelectStudentsViewController.m
//  JabjaiApp
//
//  Created by mac on 7/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSSelectStudentsViewController.h"
#import "BSSelectClassLevelViewController.h"
#import "BSSelectBehaviorScoreViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "Utils.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface BSSelectStudentsViewController () {
   
    UIColor *greenBGColor;
    UIColor *redBGColor;
    BOOL optionStatusSelectAll;
    // The dialog
    AlertDialog *alertDialog;
    NSBundle *myLangBundle;
    CAGradientLayer *gradient;
    
    //Search Bar
    BOOL searchActive;
    UIGestureRecognizer *searchBarCancelGesture;
    UIColor *searchBarBG;
    NSArray<BSSelectedStudent *> *searchStudentArray;
}

@property (strong, nonatomic) CallGetStudentInClassroomAPI *callGetStudentInClassroomAPI;

@end

static NSString *cellIdentifier = @"Cell";

@implementation BSSelectStudentsViewController
@synthesize mutableArray;
- (void)viewDidLoad {
    [super viewDidLoad];

     myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[UserData getChangLanguage] ofType:@"lproj"]];
    
    // Do any additional setup after loading the view.
    self.tableView.hidden = YES;
    self.noNumberStudent.hidden = YES;
    searchActive = NO;
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    searchBarBG = [UIColor colorWithRed:224/255.0 green:93/255.0 blue:37/255.0 alpha:1];
    self.searchBar.delegate = self;
    self.searchBar.translucent = NO;
    self.searchBar.opaque = NO;
    self.searchBar.barTintColor = searchBarBG;
    self.searchBar.backgroundImage = [[UIImage alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSStudentNameRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    greenBGColor = [UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0];
    redBGColor = [UIColor colorWithRed:220/255.0 green:43/255.0 blue:69/255.0 alpha:1.0];
    
    self.titleLabel.text = [[self.classroomArray objectAtIndex:self.selectedClassroomIndex] getClassroomName];
    
    [self getStudentInClassroom];
    [self checkOptionButton];
}
- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
    [self setLanguage];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    self.optSelectButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.25f] CGColor];
    self.optSelectButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.optSelectButton.layer.shadowOpacity = 1.0f;
    self.optSelectButton.layer.shadowRadius = 10.0f;
    self.optSelectButton.layer.masksToBounds = NO;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLanguage{
    NSBundle *myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[UserData getChangLanguage] ofType:@"lproj"]];
    [self.nextButton layoutIfNeeded];
    if(_mode == BS_MODE_ADD) {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_ADD",nil,myLangBundle,nil);
        
        gradient = [Utils getGradientColorStatus:@"green"];
        gradient.frame = self.nextButton.bounds;
        [self.nextButton.layer insertSublayer:gradient atIndex:0];
    }
    else {
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REDUCE",nil,myLangBundle,nil);;
        gradient = [Utils getGradientColorStatus:@"red"];
        gradient.frame = self.nextButton.bounds;
        [self.nextButton.layer insertSublayer:gradient atIndex:0];
    }
    
    [self.nextButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_BEHAVIOR_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
}

#pragma mark - UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBarCancelGesture = [UITapGestureRecognizer new];
    [searchBarCancelGesture addTarget:self action:@selector(backgroundTouched:)];
    [self.view addGestureRecognizer:searchBarCancelGesture];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if(searchBarCancelGesture != nil) {
        [self.view removeGestureRecognizer:searchBarCancelGesture];
        searchBarCancelGesture = nil;
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // called when text changes (including clear)
    searchActive = YES;
    if(searchText.length != 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"studentName contains[c] %@", searchText];
        searchStudentArray = [_selectedStudentArray filteredArrayUsingPredicate:predicate];
    }
    else {
        searchStudentArray = _selectedStudentArray;
    }
    [self.tableView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // called when keyboard search button pressed
    [self.searchBar resignFirstResponder];
}

#pragma mark - Utils
- (void)backgroundTouched:(id)sender {
    [self.view endEditing:YES];
    [self.searchBar resignFirstResponder];

    if(searchActive && self.searchBar.text.length == 0) {
        searchActive = NO;
        [self.tableView reloadData];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(searchActive) {
        if(searchStudentArray != nil) {
            return searchStudentArray.count;
        }
        else {
            return 0;
        }
    }else{
        if(self.selectedStudentArray != nil) {
            return self.selectedStudentArray.count;
        }
        else {
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BSStudentNameRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    NSLog(@"Row = %d",indexPath.row);
    BSSelectedStudent *model;
    
    if(searchActive) {
        model = [searchStudentArray objectAtIndex:indexPath.row];
    }
    else {
        model = [_selectedStudentArray objectAtIndex:indexPath.row];
    }
    
    cell.delegate = self;
    cell.index = indexPath.row;
    
    NSArray *array = [[NSString stringWithFormat:@"%@", [model getStudentName]] componentsSeparatedByString:@" "];
    NSMutableString *lastName = [[NSMutableString alloc] init];
    if (array.count>1) {
        for (int i = 0; i < array.count; i++) {
            if (i>0) {
                [lastName appendFormat:@"%@ ",array[i]];
            }
        }
        cell.titleLabel.text =  [NSString stringWithFormat:@"%@",array[0]] ;
        cell.titleLastname.text = lastName;
    }else{
        cell.titleLabel.text = [NSString stringWithFormat:@"%@",array[0]];
        cell.titleLastname.text = @"";
    }
    cell.runNumber.text = [NSString stringWithFormat:@"%d.",indexPath.row+1];
    [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@", [model getStudentPic]]]] placeholderImage:nil options:SDWebImageRefreshCached
        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        if (image == nil || error) {
                                          cell.userImageView.image = [UIImage imageNamed:@"ic_user_info"];
                                        }
        }];
    [cell.userImageView layoutIfNeeded];
    cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height /2;
    cell.userImageView.layer.masksToBounds = YES;
    cell.userImageView.layer.borderWidth = 0;
    if([model isSelected]) {
        if(self.mode == BS_MODE_ADD) {
            [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
        }
        else {
            [cell setRadioButtonSelectedWithType:RADIO_TYPE_RED];
        }
    }
    else {
        [cell setRadioButtonClearSelected];
    }
    [cell.shadowViewCell.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.shadowViewCell.layer setShadowOpacity:0.3];
    [cell.shadowViewCell.layer setShadowRadius:3.0];
    [cell.shadowViewCell.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 150; /* Device is iPad */
    }else{
        return 90;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isSelected;
    if(searchActive) {
        isSelected = [[searchStudentArray objectAtIndex:indexPath.row] isSelected];
        [[searchStudentArray objectAtIndex:indexPath.row] setSelected:!isSelected]; // toggle selected status
    }
    else {
        isSelected = [[self.selectedStudentArray objectAtIndex:indexPath.row] isSelected];
        [[self.selectedStudentArray objectAtIndex:indexPath.row] setSelected:!isSelected]; // toggle selected status
    }
    NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
    
}

#pragma mark - BSRadionTableViewCellDelegate

- (void)onPressRadioButton:(BSStudentNameRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index{
    
    
    BOOL isSelected;
    
    if(searchActive) {
        isSelected = [[searchStudentArray objectAtIndex:index] isSelected];
         [[searchStudentArray objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
        
    }
    else {
        isSelected = [[self.selectedStudentArray objectAtIndex:index] isSelected];
         [[self.selectedStudentArray objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
    }

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - API Caller

- (void)getStudentInClassroom {
    [self showIndicator];
    if(self.callGetStudentInClassroomAPI != nil) {
        self.callGetStudentInClassroomAPI = nil;
    }
    
    self.callGetStudentInClassroomAPI = [[CallGetStudentInClassroomAPI alloc] init];
    self.callGetStudentInClassroomAPI.delegate = self;
    [self.callGetStudentInClassroomAPI call:[UserData getSchoolId] classroomId:_classroomId];
}

#pragma mark - API Delegate

- (void)callGetStudentInClassrommAPI:(CallGetStudentInClassroomAPI *)classObj data:(NSArray<BSSelectedStudent *> *)data success:(BOOL)success {
     [self stopIndicator];
    NSManagedObjectContext *context = [self managedObjectContext];
    if(success && data != nil) {
        _selectedStudentArray = data;
        if(_selectedStudentArray.count == 0){
            self.tableView.hidden = YES;
            self.noNumberStudent.hidden = NO;
        }else{
            self.tableView.hidden = NO;
            self.noNumberStudent.hidden = YES;
        }
    }
    [self.tableView reloadData];
    [self checkOptionButton];
}

#pragma mark - Dialog
- (void)showAlertDialogWithMessage:(NSString *)message {
    if(alertDialog != nil) {
        if([alertDialog isDialogShowing]) {
            [alertDialog dismissDialog];
        }
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Utility

- (BOOL)validateData {
    
    if(_selectedStudentArray != nil) {
        for(BSSelectedStudent *model in _selectedStudentArray) {
            
            if([model isSelected]) {
                return YES;
            }
        }
    }
    
    return NO;
}

- (void)gotoBSSelectBehaviorScoreViewController {
    
    BSSelectBehaviorScoreViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectBehaviorScoreStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    viewController.status = self.status;
    viewController.selectedStudentArray = _selectedStudentArray;
    viewController.behaviorScoreArray = _behaviorScoreArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];

}

- (BOOL)isStudentSelectAll {
    
    BOOL selectAll = NO;
    
    if(_selectedStudentArray != nil) {
        for(BSSelectedStudent *model in _selectedStudentArray) {
            
            if(![model isSelected]) {
                return NO;
            }
            else {
                selectAll = YES;
            }
        }
    }
    
    return selectAll;
}

- (void)checkOptionButton {
    if(![self isStudentSelectAll]) {
        optionStatusSelectAll = YES;
        [self.optSelectButton setImage:[UIImage imageNamed:@"ic_select_all"] forState:UIControlStateNormal];
    }
    else {
        optionStatusSelectAll = NO;
        [self.optSelectButton setImage:[UIImage imageNamed:@"ic_deselect_all"] forState:UIControlStateNormal];
    }
}

- (void)selectAll {
    if(_selectedStudentArray != nil) {
        for(BSSelectedStudent *model in _selectedStudentArray) {
            [model setSelected:YES];
        }
    }
    [self.tableView reloadData];
}

- (void)deSelectAll {
    if(_selectedStudentArray != nil) {
        for(BSSelectedStudent *model in _selectedStudentArray) {
            [model setSelected:NO];
        }
    }
    [self.tableView reloadData];
}

#pragma mark - Action functions
- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        [self gotoBSSelectBehaviorScoreViewController];
    }
    else {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_BEHAVIOR_LEAST",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
    }
}

- (IBAction)moveBack:(id)sender {
    BSSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectClassLevelStoryboard"];
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    viewController.status = self.status;
    viewController.selectedStudentArray = _selectedStudentArray;
    viewController.behaviorScoreArray = _behaviorScoreArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)actionSelect:(id)sender {
    
    if(optionStatusSelectAll) {
        [self selectAll];
    }
    else {
        [self deSelectAll];
    }
    
    [self checkOptionButton];
}
@end
