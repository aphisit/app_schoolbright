//
//  CallGetSchoolClassroomJForobHomeAPI.m
//  JabjaiApp
//
//  Created by toffee on 1/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallGetSchoolClassroomJForobHomeAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetSchoolClassroomJForobHomeAPI () {
    NSInteger connectCounter;
    NSMutableArray<JHSelectRoomRadioModel *> *schoolClassrooms;
}

@end
@implementation CallGetSchoolClassroomJForobHomeAPI{
    
}
- (void)call:(long long)schoolId classLevelId:(long long)classLevelId {
    connectCounter = 0;
    //NSLog(@"classLevelId = %@",classLevelId);
    [self getSchoolClassroomWithSchoolId:schoolId classLevelId:classLevelId];
}
- (void)getSchoolClassroomWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId {
    NSString *URLString = [APIURL getSchoolClassroomWithSchoolId:schoolId classLevelId:classLevelId];
    NSURL *url = [NSURL URLWithString:URLString];

    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

        BOOL isFail = NO;

        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }

            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;

                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSchoolClassroomWithSchoolId:schoolId classLevelId:classLevelId];
            }
            else {

                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);

                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;

                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolClassroomWithSchoolId:schoolId classLevelId:classLevelId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }

            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {

                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;

                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolClassroomWithSchoolId:schoolId classLevelId:classLevelId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {

                NSArray *returnedArray = returnedData;
                connectCounter = 0;

                if(schoolClassrooms != nil) {
                    [schoolClassrooms removeAllObjects];
                    schoolClassrooms = nil;
                }

                schoolClassrooms = [[NSMutableArray alloc] init];

                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];

                    NSNumber *nClassroomID = [[NSNumber alloc] initWithInt:(int)[[dataDict objectForKey:@"ID"] integerValue]];
                    NSMutableString *classroomName = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"Value"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) classroomName);

                    JHSelectRoomRadioModel *model = [[JHSelectRoomRadioModel alloc] init];
                    model.classRoomID = nClassroomID;
                    model.classRoomName = classroomName;
                    model.selected = NO;

                    [schoolClassrooms addObject:model];
                }

                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetSchoolClassroomGroupAPI:data:success:)]) {

                    [self.delegate callGetSchoolClassroomGroupAPI:self data:schoolClassrooms success:YES ];
                }

                return ;
            }

        }

        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetSchoolClassroomGroupAPI:data:success:)]) {
                [self.delegate callGetSchoolClassroomGroupAPI:self data:nil success:NO];
            }
        }

    }];
}

@end
