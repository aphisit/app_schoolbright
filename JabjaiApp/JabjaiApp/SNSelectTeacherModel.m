//
//  SNSelectTeacherModel.m
//  JabjaiApp
//
//  Created by toffee on 9/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNSelectTeacherModel.h"

@interface SNSelectTeacherModel ()

@end

@implementation SNSelectTeacherModel

@synthesize teacherId = _teacherId;
@synthesize teacherName = _teacherName;
@synthesize selected = _selected;

- (void)setTeacherId:(long long)teacherId{
    _teacherId = teacherId;
}

- (void)setTeacherName:(NSString *)teacherName {
    _teacherName = teacherName;
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
}

- (long long)getTeacherId {
    return _teacherId;
}

- (NSString *)getTeacherName {
    return _teacherName;
}

- (BOOL)isSelected {
    return _selected;
}


@end
