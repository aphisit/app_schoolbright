//
//  NewsMessageDataProvider.m
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NewsMessageDataProvider.h"
#import "NewsMessageDataLoadingOperation.h"
#import <AWPagedArray/AWPagedArray.h>

const NSUInteger DataProviderDefaultPageSize = 20;
const NSUInteger DataProviderDataCount = 50;

@interface NewsMessageDataProvider() <AWPagedArrayDelegate>
@end

@implementation NewsMessageDataProvider {
    AWPagedArray *_pagedArray;
    NSOperationQueue *_operationQueue;
    NSMutableDictionary *_dataLoadingOperations;
}

#pragma mark - Cleanup
- (void)dealloc {
    [_operationQueue.operations makeObjectsPerformSelector:@selector(cancel)];
}

#pragma mark - Initialization
- (instancetype)init {
    return [self initWithPageSize:DataProviderDefaultPageSize];
}
- (instancetype)initWithPageSize:(NSUInteger)pageSize {
    
    self = [super init];
    if (self) {
        _pagedArray = [[AWPagedArray alloc] initWithCount:DataProviderDataCount objectsPerPage:pageSize];
        _pagedArray.delegate = self;
        _dataLoadingOperations = [NSMutableDictionary dictionary];
        _operationQueue = [NSOperationQueue new];
    }
    return self;
}

#pragma mark - Accessors
- (NSUInteger)loadedCount {
    return _pagedArray.pages.count*_pagedArray.objectsPerPage;
}
- (NSUInteger)pageSize {
    return _pagedArray.objectsPerPage;
}
- (NSArray *)dataObjects {
    return (NSArray *)_pagedArray;
}

#pragma mark - Other public methods
- (BOOL)isLoadingDataAtIndex:(NSUInteger)index {
    return _dataLoadingOperations[@([_pagedArray pageForIndex:index])] != nil;
}
- (void)loadDataForIndex:(NSUInteger)index {
    [self _setShouldLoadDataForPage:[_pagedArray pageForIndex:index]];
}

#pragma mark - Private methods
- (void)_setShouldLoadDataForPage:(NSUInteger)page {
    
    if (!_pagedArray.pages[@(page)] && !_dataLoadingOperations[@(page)]) {
        // Don't load data if there already is a loading operation in progress
        [self _loadDataForPage:page];
    }
}

- (void)_loadDataForPage:(NSUInteger)page {
    
    NSIndexSet *indexes = [_pagedArray indexSetForPage:page];
    
    NSOperation *loadingOperation = [self _loadingOperationForPage:page indexes:indexes];
    _dataLoadingOperations[@(page)] = loadingOperation;
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(dataProvider:willLoadDataAtPage:indexes:)]) {
        [self.delegate dataProvider:self willLoadDataAtPage:page indexes:indexes];
    }
    
    [_operationQueue addOperation:loadingOperation];
}

- (NSOperation *)_loadingOperationForPage:(NSUInteger)page indexes:(NSIndexSet *)indexes {
    
    NewsMessageDataLoadingOperation *operation = [[NewsMessageDataLoadingOperation alloc] initWithPage:page indexes:indexes];
    
    // Remember to not retain self in block since we store the operation
    __weak typeof(self) weakSelf = self;
    __weak typeof(operation) weakOperation = operation;
    operation.downloadCompletion = ^(NSUInteger page, NSIndexSet *indexes, NSArray *data) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [weakSelf _dataOperation:weakOperation finishedLoadingForPage:page indexes:indexes];
        }];
    };
    
    return operation;
}

- (void)_preloadNextPageIfNeededForIndex:(NSUInteger)index {
    
    if (!self.shouldLoadAutomatically) {
        return;
    }
    
    NSUInteger currentPage = [_pagedArray pageForIndex:index];
    NSUInteger preloadPage = [_pagedArray pageForIndex:index+self.automaticPreloadMargin];
    
    if (preloadPage > currentPage && preloadPage <= _pagedArray.numberOfPages) {
        [self _setShouldLoadDataForPage:preloadPage];
    }
}

- (void)_dataOperation:(NewsMessageDataLoadingOperation *)operation finishedLoadingForPage:(NSUInteger)page indexes:(NSIndexSet *)indexes {
    
    [_dataLoadingOperations removeObjectForKey:@(page)];
    
    [_pagedArray setObjects:operation.dataPage forPage:page];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(dataProvider:didLoadDataAtPage:indexes:)]) {
        [self.delegate dataProvider:self didLoadDataAtPage:page indexes:indexes];
    }
    
    
}

#pragma mark - Paged array delegate
- (void)pagedArray:(AWPagedArray *)pagedArray willAccessIndex:(NSUInteger)index returnObject:(__autoreleasing id *)returnObject {
    
    if ([*returnObject isKindOfClass:[NSNull class]] && self.shouldLoadAutomatically) {
        [self _setShouldLoadDataForPage:[pagedArray pageForIndex:index]];
    } else {
        [self _preloadNextPageIfNeededForIndex:index];
    }
}

@end
