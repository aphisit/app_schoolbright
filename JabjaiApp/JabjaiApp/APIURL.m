
#import "APIURL.h"
#import "Utils.h"
@interface APIURL ()
@end
@implementation APIURL
#pragma mark - School
+(NSString *)getLoginURL:(NSString *)username password:(NSString *)password schoolId:(long long)schoolId {
      NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/login?user=%@&pass=%@&schoolid=%lld"], username, password,schoolId];
    return url;
}
+(NSString*)getUpdatePassword:(NSString *)newPassword userId:(long long)userId token:(NSString *)token schoolid:(long long)schoolid{
     NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/user/password/change?password=%@&ClientToken=%@&user_id=%lld&schoolid=%lld"], newPassword, token, userId,schoolid];
    return url;
}
+(NSString *)getSchoolDataWithPasswordURL:(NSString *)password {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/schoolpass?password=%@"], password];
    return url;
}
+(NSString *)getListAllSchool{
    NSString *url = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school"];
    return url;
}

+(NSString *)getProvinceURL {
    NSString *url = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Province"];
    return url;
}
+(NSString *)getDistrictURL:(NSInteger)provinceID {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/amphur/%i"], (int)provinceID];
    return url;
}
+(NSString *)getSubDistrictURL:(NSInteger)districtID {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/district/%i"], (int)districtID];
    return url;
}
+(NSString *)getSignupURLWithSchoolID:(NSInteger)schoolID classroomID:(NSInteger)classroomID type:(NSInteger)type gender:(NSInteger)gender firstName:(NSString *)firstName lastName:(NSString *)lastName personalID:(NSString *)personalID studentID:(NSString *)studentID birthday:(NSDate *)birthday phoneNumber:(NSString *)phoneNumber email:(NSString *)email creditLimits:(NSInteger)creditLimits address:(NSString *)address provinceID:(NSInteger)provinceID districtID:(NSInteger)districtID subDistrictID:(NSInteger)subDistrictID {
    NSString *baseURL = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/User?"];
    NSDateFormatter *formatter = [Utils getDateFormatter];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *strBirthday = [formatter stringFromDate:birthday];
    NSString *query = [NSString stringWithFormat:@"nCompany=%i&nTermSubLevel2=%i&cType=%i&csex=%i&sname=%@&slastname=%@&sidentification=%@&studentID=%@&dBirth=%@&sPhone=%@&sEmail=%@&nMax=%i&address=%@&PROVINCE_ID=%i&AMPHUR_ID=%i&DISTRICT_ID=%i", (int)schoolID, (int)classroomID, (int)type, (int)gender, firstName, lastName, personalID, studentID, strBirthday, phoneNumber, email, (int)creditLimits, address, (int)provinceID, (int)districtID, (int)subDistrictID];
    
    NSString *encodedQuery = [Utils encodeString:query];
    NSString *url = [[NSString alloc] initWithFormat:@"%@%@", baseURL, encodedQuery];
    return url;
}
+(NSString *)getCreditBureauSummaryURL {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Reportmobile03?tremid=&userid=%i&schoolid=%lld"], (int)[UserData getUserID],[UserData getSchoolId]];
    return url;
}
+(NSString *)getYearAndTermURL {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/GetYear?userid=%i&schoolid=%lld"], (int)[UserData getUserID],[UserData getSchoolId]];
    return url;
}

//status 0 : On time, 1 : Late, 3 : Absence
+(NSString *)getAttendSchoolDataURLWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status schoolid:(long long)schoolid {
    NSString *serverStartDateStr = [Utils dateToServerDateFormat:startDate];
    NSString *serverEndDateStr = [Utils dateToServerDateFormat:endDate];
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/come2school?userid=%i&dstart=%@&dend=%@"], (int)[UserData getUserID], serverStartDateStr, serverEndDateStr];
    NSMutableString *mutableURL = [[NSMutableString alloc] initWithString:url];
    if(status >= 0) {
        [mutableURL appendString:[NSString stringWithFormat:@"&status=%i", (int)status]];
    }
    else {
        [mutableURL appendString:@"&status="];
    }
    [mutableURL appendString:[NSString stringWithFormat:@"&schoolid=%lld", schoolid]];
    return mutableURL;
}

+(NSString *)getMessageInboxURLWithLatestID:(NSInteger)latestID messageType:(NSInteger)messageType schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Message?userid=%i&latestid=%i&typeid=%i&schoolid=%lld"], (int)[UserData getUserID], (int)latestID, (int)messageType, schoolid];
    return url;
}
+ (NSString *)getAllMessageWithPage:(NSUInteger)page userID:(long long)userID status:(NSString *)status schoolId:(long long)schoolId {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/getmessage/%lld/%lu?status=%@&schoolid=%lld&lang=%@"], userID, page, status, schoolId,[UserData getChangLanguage]];
    return url;
}
+ (NSString *)getImageNews:(long long)userID idMessage:(long long)idMessage schoolid:(long long)schoolid {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/filemessage/%lld/%lld?schoolid=%lld"], userID, idMessage, schoolid];
    return url;
}
+ (NSString *)getNewsMessageWithPage:(NSUInteger)page userID:(long long)userID schoolid:(long long)schoolid {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/getMessage/news/%lld/%lu?schoolid=%lld"], userID, page,schoolid];
    return url;
}
+ (NSString *)getUpdateReadMessageURLWithMessageID:(long long)messageID userID:(long long)userID schoolid:(long long)schoolid {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/ReadMessag/%lld/%lld?schoolid=%lld"], userID, messageID,schoolid];
    return url;
}
+(NSString *)getChangeFingerprintRequestURL {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/repass?id=%ld&schoolid=%lld"], [UserData getUserID],[UserData getSchoolId]];
    return url;
}
+(NSString *)getUserCredentialURL {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/user/?userid=%ld&schoolid=%lld"], [UserData getUserID],[UserData getSchoolId]];
    return url;
}
+ (NSString *)getStudentScheduleListURLWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/Schedule/ScheduleList?studentid=%lld&day=%@&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
+ (NSString *)getStudentScheduleListDetailURLWithUserID:(long long)userID date:(NSDate *)date subjectID:(long long)subjectID schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/Schedule/ScheduleDetail?studentid=%lld&day=%@&ScheduleId=%lld&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date], subjectID, schoolid];
    return url;
}
+ (NSString *)getTeacherScheduleListDetailURLWithUserID:(long long)userID date:(NSDate *)date subjectID:(long long)subjectID schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/Schedule/ScheduleDetail?teacherid=%lld&day=%@&ScheduleId=%lld&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date], subjectID,schoolid];
    return url;
}
+ (NSString *)getBoughtItemWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/report/buyitem/v2?UserId=%lld&day=%@&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
+ (NSString *)getTopupMessageWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/getMessage/topup?userid=%lld&dmessage=%@&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
+ (NSString *)getUnreadMessageCountWithUserID:(long long)userID schoolid:(long long)schoolid {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/CountUnreadMessag/%lld?schoolid=%lld"], userID,schoolid];
    return url;
}
+ (NSString *)getSNNewsPOSTURL {
    NSString *url = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/News/sendfiles"];
    return url;
}
+ (NSString *)getHomeWorkPOSTURL {
    NSString *url = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/HomeWork/sendfiles"];
    return url;
}
+ (NSString *)getFeedBackPOSTURL {
    NSString *url =  [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/feedback/send"];
    return url;
}
+(NSString *)getStatusTakeEvent:(long long)schoolId {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/permission?school_id=%lld"], schoolId];
    return url;
}
+(NSString *)getAuthorizeMenu:(long long)userId clientToken:(NSString *)clientToken schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/user/Authentication?user_id=%lld&ClientToken=%@&schoolid=%lld"], userId, clientToken, schoolid];
        return url;
}
+(NSString *)getAmountUnreadLeave:(long long)userId schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/CountWaitingLetter?userid=%lld&schoolid=%lld"], userId, schoolid];
    return url;
}

+ (NSString *)getStatusInsertDataOfNotification{
    NSString *url = @"https://api.schoolbright.co/Notification/Insert";
    return url;
}

+ (NSString *)getStatusRemoveDataOfNotification{
    NSString *url = @"https://api.schoolbright.co/Notification/Remove";
    return url;
}

+ (NSString *)getStatusClearDataOfNotification:(NSString *)token{
    //NSString *url = @"https://api.schoolbright.co/Notification/Clear?Token=";
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/Notification/Clear?Token=%@"], token];
    return url;
}

// Take Class Attendance
+ (NSString *)getStudentClassLevelWithSchoolId:(long long)schoolId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/sublevel/getLevel/%lld"], schoolId];
    return url;
}
+ (NSString *)getSchoolClassroomWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/sublevel2?schoolid=%lld&sublevelid=%lld"], schoolId, classLevelId];
    return url;
}
+ (NSString *)getSubjectWithSchoolId:(long long)schoolId classroomId:(long long)classroomId date:(NSString *)date {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/School/getschedule/%lld/%lld?datetime=%@"], schoolId, classroomId, date];
    return url;
}
+ (NSString *)getStudentInClassWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId date:(NSString *)date {
     NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/School/getstudent/%lld/%lld/%lld/%lld?date=%@"], schoolId, classroomId, subjectId, teacherId,date];
    return url;
}
+ (NSString *)getPreviousAttendClassStatusWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/copystatus/%lld/%lld/%lld/%lld"], schoolId, classroomId, subjectId, teacherId];
    return url;
}
+ (NSString *)getUpdateStudentStatusPOSTWithSchoolId:(long long)schoolId subjectId:(long long)subjectId teacherId:(long long)teacherId date:(NSString *)date {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/School/updatestatus/%lld/%lld/%lld?date=%@"], schoolId, subjectId, teacherId,date];
    return url;
}
+ (NSString *)getTEUpdateStudentStatusPOSTWithSchoolId:(long long)schoolId  teacherId:(long long)teacherId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/School/updatestatusjobscan/%lld/%lld"], schoolId, teacherId];
    return url;
}
+ (NSString *)getTASubjectListWithSchoolId:(long long)schoolId classroomId:(long long)classroomId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/Schedule/report4level2/getplane/v1?schoolid=%lld&level2Id=%lld"], schoolId, classroomId];
    return url;
}
+ (NSString *)getTAHistoryStudentStatusListWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId date:(NSDate *)date {
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/Schedule/report4level2/getdaydetail/v1?schoolid=%lld&level2Id=%lld&day=%@&planeid=%@"], schoolId, classroomId, dateStr, subjectId];
    return url;
}
+ (NSString *)getTEHistoryStudentStatusListWithSchoolId:(long long)schoolId classroomId:(long long)classroomId  date:(NSDate *)date {
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/reports/jobscan/daily?schoolid=%lld&classid=%lld&day=%@"], schoolId, classroomId, dateStr];
    return url;
}
+ (NSString *)getTAHistoryDateStatusWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId startDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    NSString *startDateStr = [Utils dateToServerDateFormat:startDate];
    NSString *endDateStr= [Utils dateToServerDateFormat:endDate];
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/Schedule/report4level2/getdaychecked/v1?schoolid=%lld&level2Id=%lld&daystart=%@&dayend=%@&planeid=%@"], schoolId, classroomId, startDateStr, endDateStr, subjectId];
    return url;
}

+ (NSString *)getTAStatusCalendarOfReport:(long long)userID day:(NSString *)day subjectID:(long long)subjectID schoolID:(long long)schoolID classroomID:(long long)classroomID{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Reports/showschedule?userid=%lld&day=%@&planeid=%lld&schoolid=%lld&level2id=%lld"], userID, day, subjectID,schoolID,classroomID];
    return url;
}
/*
 ** Behavior Score
 */
+ (NSString *)getStudentInClassroomWithSchoolId:(long long)schoolId classroomId:(long long)classroomId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/studentinclass?SchoolId=%lld&level2=%lld"], schoolId, classroomId];
    return url;
}

//Take Even
+ (NSString *)getTakeEventStudentInClassroomWithSchoolId:(long long)schoolId classroomId:(long long)classroomId teacherId:(NSInteger)teacherId{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/School/getstudent/%lld/%lld?teacherId=%d"], schoolId, classroomId,teacherId];
    return url;
}
+ (NSString *)getTEGetDataStudentScanBarcode:(long long)schoolId idStudent:(NSString *)idStudent{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/user/qrcode?school_id=%lld&code=%@"], schoolId, idStudent];
    return url;
}
+ (NSString *)getTEConfirmScanerBarcode:(long long)schoolId userId:(long long)userId idCode:(NSString *)idCode{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Jobscan/checklate/qrcode?school_id=%lld&user_id=%lld&barcode=%@"], schoolId, userId,idCode];
    return url;
}
+ (NSString *)getTEScanerQROnTimeConfirm:(long long)schoolId userId:(long long)userId idCode:(NSString *)idCode{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Jobscan/checkontime/barcode?school_id=%lld&user_id=%lld&barcode=%@"], schoolId, userId,idCode];
    return url;
}
+ (NSString *)getTEFinishSchool:(long long)schoolId userId:(long long)userId idCode:(NSString *)idCode status:(NSString *)status{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/jobscan/checktimeout?school_id=%lld&user_id=%lld&user_barcode=%@&job_status=%@"], schoolId, userId,idCode,status];
    return url;
}

// Behavior Score
+ (NSString *)getAddBehaviorScoreListWithSchoolId:(long long)schoolId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Behaviors/list/add?SchoolId=%lld"], schoolId];
    return url;
}
+ (NSString *)getReduceBehaviorScoreListWithSchoolId:(long long)schoolId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Behaviors/list/reduce?SchoolId=%lld"], schoolId];
    return url;
}
+ (NSString *)getUpdateStudentBehaviorScorePOSTWithSchoolId:(long long)schoolId teacherId:(long long)teacherId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Behaviors/updatescore?SchoolId=%lld&TheacherId=%lld"], schoolId, teacherId];
    return url;
}

+ (NSString *)getBehaviorScoreHistoryInDateRangeWithSchoolId:(long long)schoolId classroomId:(long long)classroomId startDate:(NSDate *)startDate endDate:(NSDate *)endDate type:(NSInteger)type {
    NSString *startDateStr = [Utils dateToServerDateFormat:startDate];
    NSString *endDateStr= [Utils dateToServerDateFormat:endDate];
    NSString *url;
    if(type == 0 || type == 1) {
        url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Behaviors/reports?schoolid=%lld&classid=%lld&daystart=%@&dayend=%@&type=%i"], schoolId, classroomId, startDateStr, endDateStr, (int)type];
    } else {
        url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Behaviors/reports?schoolid=%lld&classid=%lld&daystart=%@&dayend=%@&type="], schoolId, classroomId, startDateStr, endDateStr];
    }
    return url;
}
+ (NSString *)getBehaviroScoreHistoryForStudentReportWithUserId:(long long)userId schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Behaviors/History?UserId=%lld&schoolid=%lld"], userId,schoolid];
    return url;
}

//Send News
+ (NSString *)getUpdateSendNewsPOST {
    NSString *url = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/news/send"];
    return url;
}
+ (NSString *)getStatusReplyCodeMessageBox:(long long)userID messageID:(long long)messageID replyCode:(int)replyCode schoolID:(long long)schoolID{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/ReplyMessage/%lld/%lld/%d?schoolid=%lld"], userID, messageID,replyCode,schoolID];
   // /api/messagebox/ReplyMessage/%lld/%lld/%d
    return url;
}

/*
 ** Executive report
 */
+ (NSString *)getEXReportSummaryAttendSchoolWithSchoolId:(long long)schoolId date:(NSDate *)date {
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/takeschoolattendancereports?dayreports=%@&schoolid=%lld"], dateStr, schoolId];
    return url;
}
+ (NSString *)getEXReportClassroomAttendSchoolWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId date:(NSDate *)date {
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/takeschoolattendancereports/SumStudent4class?dayreports=%@&schoolid=%lld&levelid=%lld"], dateStr, schoolId, classLevelId];
    return url;
}
+ (NSString *)getEXReportAttendSchoolTeacherListWithSchoolId:(long long)schoolId date:(NSDate *)date {
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/takeschoolattendancereports/Theacher?dayreports=%@&schoolid=%lld"], dateStr, schoolId];
    return url;
}
+ (NSString *)getEXReportAttendSchoolEmployeeListWithSchoolId:(long long)schoolId date:(NSDate *)date {
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/takeschoolattendancereports/Employees?dayreports=%@&schoolid=%lld"], dateStr, schoolId];
    return url;
}

#pragma mark - University URL
+(NSString *)getUniversityFacultyURL:(NSInteger)schoolID {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UniversityLevel?schoolid=%i"], (int)schoolID];
    return url;
}
+(NSString *)getUniversityDegreeURL:(NSInteger)schoolID facultyID:(NSInteger)facultyID {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UniversityLevelSub?schoolid=%i&levelid=%i"], (int)schoolID, (int)facultyID];
    return url;
}
+(NSString *)getUniversityMajorURL:(NSInteger)schoolID degreeID:(NSInteger)degreeID {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UniversityLevelSub2?schoolid=%i&sublevelid=%i"], (int)schoolID, (int)degreeID];
    return url;
}
+(NSString *)getStudentEnrollSubjectURL {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UniversityUserListSection?userid=%i&schoolid=%lld"], (int)[UserData getUserID],[UserData getSchoolId]];
    return url;
}
+(NSString *)getAddSectionUrl:(NSInteger)sectionID schoolid:(long long)schoolid {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UniversityUserInsertSection?Userid=%i&SectionId=%i&schoolid=%lld"], (int)[UserData getUserID], (int)sectionID,schoolid];
    return url;
}
+(NSString *)getDeleteSectionUrl:(NSInteger)sectionID schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UniversityUserDeleteSection?Userid=%i&SectionId=%i&schoolid=%lld"], (int)[UserData getUserID], (int)sectionID, schoolid];
    return url;
}
+(NSString *)getSubjectAddedUrl {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UniversityUserSelectedSection?Userid=%i&schoolid=%lld"], (int)[UserData getUserID],[UserData getSchoolId]];
    return url;
}
+(NSString *)getTeachingSubjectUrl {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UniversityPlane?UserId=%i&schoolid=%lld"], (int)[UserData getUserID],[UserData getSchoolId]];
    return url;
}
+(NSString *)getStudentsInSectionUrl:(NSInteger)sectionID schoolid:(long long)schoolid {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UniversitySectionListUser?userid=%i&SectionId=%i&schoolid=%lld"], (int)[UserData getUserID], (int)sectionID,schoolid];
    return url;
}
+(NSString *)getStudentSubjectListAllYearUrl {
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UniversityStudentSubjectList?UserId=%i&schoolid=%lld"], (int)[UserData getUserID],[UserData getSchoolId]];
    return url;
}
+(NSString *)getTheacherAllUrl:(long long)schoolID{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/user/getuser/Theacher?schoolid=%lld"],schoolID];
    return url;
}
+(NSString *)getPersonnelAllUrl:(long long)schoolID{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/user/getuser/employess?schoolid=%lld"],schoolID];
    return url;
}
+(NSString *)getAttendToSectionHistoryWithSectionID:(NSInteger)sectionID startDate:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status userType:(USERTYPE)userType schoolid:(long long)schoolid{
    NSString *serverStartDateStr = [Utils dateToServerDateFormat:startDate];
    NSString *serverEndDateStr = [Utils dateToServerDateFormat:endDate];
    NSString *url;
    if(userType == STUDENT) {
        url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Come2Section4Student?UserId=%i&SectionId=%i&dstart=%@&dend=%@"], (int)[UserData getUserID], (int)sectionID, serverStartDateStr, serverEndDateStr];
    }
    else {
        url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Come2Section4Teacher?UserId=%i&SectionId=%i&dstart=%@&dend=%@"], (int)[UserData getUserID], (int)sectionID, serverStartDateStr, serverEndDateStr];
    }
    NSMutableString *mutableURL = [[NSMutableString alloc] initWithString:url];
    if(status >= 0) {
        [mutableURL appendString:[NSString stringWithFormat:@"&status=%i", (int)status]];
    }
    else {
        [mutableURL appendString:@"&status="];
    }
    [mutableURL appendString:[NSString stringWithFormat:@"&schoolid=%lld", schoolid]];
    return mutableURL;
}
//JobHome
+ (NSString *)getJHSubjectWithSchoolId:(long long)schoolId classroomId:(NSString*)classroomId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/HomeWork/getplane?level2array=%@&school_id=%lld"], classroomId, schoolId];
    return url;
}
+ (NSString *)getJHSubjectModeGroupWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/homework/getplane?schoolid=%lld&levelid=%lld"], schoolId, classLevelId];
    return url;
}
+ (NSString *)getJHStudentInClassroomWithSchoolId:(long long)schoolId classroomId:(long long)classroomId {
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/studentinclass?SchoolId=%lld&level2=%lld"], schoolId, classroomId];
    return url;
}
+ (NSString *)getUpdateJobHomePOST {
    NSString *url = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/HomeWork/send"];
    return url;
}

//TeacherTimeTable
+ (NSString *)getEventCalendarWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/schoolCalendar?userid=%lld&day=%@&schoolid=%lld"], userID , [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
+ (NSString *)getTeacherTimeDetail:(long long)userId date:(NSDate *)date ScheduleId:(long long)ScheduleId schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/Schedule/ScheduleDetail?teacherid=%lld&day=%@&ScheduleId=%lld&schoolid=%lld"], userId , [Utils dateToServerDateFormat:date],ScheduleId,schoolid];
    return url;
}

// 25/10/2538
+ (NSString *)getEventCalendarListWithUserID:(long long)userID date:(NSDate *)date schoolId:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/schoolCalendarList?userid=%lld&day=%@&schoolid=%lld"], userID , [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
+ (NSString *)getTeacherScheduleListWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/Schedule/ScheduleList?teacherid=%lld&day=%@&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date],schoolid];
    return url;
}

// 26/12/2017
+ (NSString *)getTeacherScheduleMonthWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/Schedule/monthly?teacherid=%lld&day=%@&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
// 26/12/2017
+ (NSString *)getBoughtItemMonthWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Reports/buyproducts?userid=%lld&day=%@&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
// 26/12/2017
+ (NSString *)getTopupMonthWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Reports/topupmoney?userid=%lld&day=%@&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
// 26/12/2017
+ (NSString *)getStudentScheduleMonthWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/school/Schedule/monthly?studentid=%lld&day=%@&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date],schoolid];
    return url;
}

+ (NSString *)getLeaveInboxWithPage:(NSUInteger)page userID:(long long)userID schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/LeaveLetterList?userid=%lld/%lu&schoolid=%lld"] , userID , page,schoolid];
    return url;
}

+ (NSString *)getLeaveReportDailyWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/LeaveCalendar?userid=%lld&day=%@&schoolid=%lld"] , userID , [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
+ (NSString *)getLeaveReportMonthlyWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/LeaveCalendarList?userid=%lld&day=%@&schoolid=%lld"] , userID , [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
+ (NSString *)getHomeworkCalendarWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url= [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Reports/showhomework?userid=%lld&day=%@&schoolid=%lld"] , userID , [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
+ (NSString *)getHomeworkCalendarListWithUserID:(long long)userID date:(NSDate *)date schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/getmessage/homework?UserId=%lld&dMessage=%@&schoolid=%lld"] , userID , [Utils dateToServerDateFormat:date],schoolid];
    return url;
}
+ (NSString *)getHomeworkMessageWithPage:(NSUInteger)page userID:(long long)userID schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/getMessage/homework/%lld/%lu?schoolid=%lld"] , userID , page,schoolid];
    return url;
}

+ (NSString *)getCalendarReportBehaviorWithhUserID:(long long)userID date:(NSDate *)date classid:(long long)classid schoolid:(long long)schoolid{
     NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Reports/showbehaviors?userid=%lld&day=%@&classid=%lld&schoolid=%lld"], userID, [Utils dateToServerDateFormat:date] , classid, schoolid];
    return url;
}
+ (NSString *)getSNLeavePOSTURL{
    NSString *url = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/leave/sendfiles"];
    return url;
}
+ (NSString *)getSendLetterDataLeaveWithLeaveType:(NSString *)leaveType description:(NSString *)description dayStart:(NSDate *)dayStart dayEnd:(NSDate *)dayEnd userID:(long long)userID ampher:(NSString *)ampher homeNumber:(NSString *)homeNumber phone:(NSString *)phone province:(NSString *)province road:(NSString *)road tumbon:(NSString *)tumbon season:(NSInteger)season schoolid:(long long)schoolid{
    NSString *baseURL = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/studentleave?"];
    NSString *strfirstDate = [Utils dateToServerDateFormat:dayStart];
    NSString *strlastDate = [Utils dateToServerDateFormat:dayEnd];
    NSString *query = [[NSString alloc] initWithFormat:@"LeaveType=%@&Description=%@&DayStart=%@&DayEnd=%@&UserId=%lld&Aumpher=%@&HomeNumber=%@&Phone=%@&Province=%@&Road=%@&Tumbon=%@&Season=%i&schoolid=%lld", leaveType , description , strfirstDate , strlastDate , userID , ampher, homeNumber, phone, province, road, tumbon, (int)season, schoolid];
    NSString *encodedQuery = [Utils encodeString:query];
    NSString *url = [[NSString alloc] initWithFormat:@"%@%@", baseURL, encodedQuery];
    return url;
}
+ (NSString *)getWarningWithUserID:(long long)userID schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/detailwarning?userid=%lld&schoolid=%lld"] , userID,schoolid];
    return url;
}

//Fee / TuitionFee
+ (NSString *)getSchoolFeeListWithUserID:(long long)userID schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/invoices/list?student_id=%lld&schoolid=%lld"] , userID, schoolid];
    return url;
}
+ (NSString *)getSchoolFeeDetailWithUserID:(long long)userID InvoiceID:(long long)invoiceID schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/invoices/detail?student_id=%lld&invoices_Id=%lld&schoolid=%lld"] , userID, invoiceID,schoolid];
    return url;
}
+ (NSString *)getPaymentPrompayWebviewAPI:(long long)studentID amount:(double)amount invoiceID:(long long)invoiceID{
//    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/invoices/detail?student_id=%lld&invoices_Id=%lld&schoolid=%lld"] , userID, invoiceID,schoolid];
    NSString *url = [[NSString alloc] initWithString:[NSString stringWithFormat:@"https://system.schoolbright.co/PaymentGateway/KBank/TuitionFees/CheckoutQR.aspx?studentID=%lld&amount=%.2f&invoiceID=%lld&lang=%@",studentID,amount,invoiceID,[UserData getChangLanguage]]];
    return url;
}
+ (NSString *)getPaymentCardWebviewAPI:(long long)studentID amount:(double)amount invoiceID:(long long)invoiceID{
    NSString *url = [[NSString alloc] initWithString:[NSString stringWithFormat:@"https://system.schoolbright.co/PaymentGateway/KBank/TuitionFees/CheckoutCard.aspx?studentID=%lld&amount=%.2f&invoiceID=%lld&lang=%@",studentID,amount,invoiceID,[UserData getChangLanguage]]];
    return url;
}

+ (NSString *)getClassroomForStudentWithUserID:(long long)userID schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/findClassroom?userid=%lld&schoolid=%lld"], userID, schoolid];
    return url;
}
+ (NSString *)getPositionForTeacherWithUserID:(long long)userID schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/findJob?userid=%lld&schoolid=%lld"] , userID,schoolid];
    return url;
}

//Health
+ (NSString *)getHealthDataWithUserID:(long long)userID schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/healthProfile?userid=%lld&schoolid=%lld"] , userID, schoolid];
    return url;
}
+ (NSString *)getHealthHistory:(long long)userId schoolId:(long long)schoolId date:(NSString *)date{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/healthProfile/GetTemperatureLog?schoolid=%lld&sid=%lld&date=%@"] ,schoolId,userId,date];
       return url;
}

+ (NSString *)getRequestLetterWithLetterID:(long long)letterID userID:(long long)userID approve:(NSInteger)approve schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/confirmLeave?letterid=%lld&userid=%lld&approve=%i&schoolid=%lld"] , letterID , userID , (int)approve,schoolid];
    return url;
}
+ (NSString *)getUserLeaveDetailWithUserID:(long long)userID schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/UserLeaveDetail?userid=%lld&schoolid=%lld"], userID,schoolid];
    return url;
}
+ (NSString *)getLetterLeaveDetailWithLetterID:(long long)letterID userID:(long long)userID schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/LetterDetail?userid=%lld&letterid=%lld&schoolid=%lld"], userID , letterID, schoolid];
    return url;
}
+(NSString *)getUserIdOfMessage:(long long)schoolId messageId:(long long)messageId userId:(NSString *)userId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/getuser?messageid=%lld&userid=%@&school_id=%lld"], messageId,userId,schoolId];
    return url;
}
+(NSString *)getReportFeeWithUserID:(long long)userId termID:(NSString *)termId schoolid:(long long)schoolid{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/reports/invoices/list?userid=%lld&trem=%@&schoolid=%lld"] ,userId ,termId,schoolid];
    return url;
}

+(NSString *)getReportOrderHomeworkWithSchoolID:(long long)schoolId page:(NSUInteger)page{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/HomeWork/reports?school_id=%lld&page=%lu"], schoolId, page];
    return url;
}
+(NSString *)getReportOrderHomeworkDetailWithSchoolID:(long long)schoolId homeworkID:(long long)homeworkId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/homework/reportsdetail?school_id=%lld&homework_id=%lld"], schoolId, homeworkId];
    return url;
}
+(NSString *)getReportOrderHomeworkRecieverWithSchoolID:(long long)schoolId homeworkID:(long long)homeworkId page:(NSUInteger)page status:(NSString *)status{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/homework/reportsdetail/userlist?school_id=%lld&homework_id=%lld&page=%lu"], schoolId, homeworkId, page];
    NSMutableString *mutableURL = [[NSMutableString alloc] initWithString:url];
    if(status != nil) {
        [mutableURL appendString:[NSString stringWithFormat:@"&readstatus=%@", status]];
    }
    else {
        [mutableURL appendString:@"&readstatus="];
    }
    return mutableURL;
}
+(NSString *)getReportSendNewsListWithSchoolID:(long long)schoolId page:(NSUInteger)page sort:(NSString *)sort{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/news/reports?school_id=%lld&page=%lu"], schoolId, page];
    NSMutableString *mutableURL = [[NSMutableString alloc] initWithString:url];
    if(sort != nil) {
        [mutableURL appendString:[NSString stringWithFormat:@"&sort=%@", sort]];
    }
    else {
        [mutableURL appendString:@"&sort="];
    }
    return mutableURL;
}
+(NSString *)getReportSendNewsDetailWithSchoolID:(long long)schoolId newsID:(long long)newsId{
     NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/news/reportsdetail?school_id=%lld&news_id=%lld"], schoolId, newsId];
    return url;
}

+(NSString *)getReportSendNewsRecieverWithSchoolID:(long long)schoolId newsID:(long long)newsId page:(NSUInteger)page status:(NSString *)status{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/news/reportsdetail/userlist?school_id=%lld&news_id=%lld&page=%lu"], schoolId, newsId, page];
    NSMutableString *mutableURL = [[NSMutableString alloc] initWithString:url];
    if(status != nil) {
        [mutableURL appendString:[NSString stringWithFormat:@"&readstatus=%@", status]];
    }
    else {
        [mutableURL appendString:@"&readstatus="];
    }
    return mutableURL;
}

//Marget
+(NSString *)getMargetListName:(long long)schoolId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLSHOPAPI,@"/api/shop?school_id=%lld"],schoolId];
    return url;
}
+ (NSString *)getListMenuFood:(long long)schoolId shopId:(long long)shopId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLSHOPAPI,@"/api/shop/product?school_id=%lld&shop_id=%lld&product_name=&producttype_id="],schoolId,shopId];
    return url;
}
+ (NSString *)getUpdateOrder:(long long)schoolId shopId:(long long)shopId userId:(long long)userId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLSHOPAPI,@"/api/shop/payment?school_id=%lld&shop_id=%lld&user_id=%lld"],schoolId,shopId,userId];
    return url;
}
+ (NSString *)getCheckSignUpPin:(long long)userId schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLSHOPAPI,@"/api/shop/pin/status?user_id=%lld&schoolid=%lld"],userId,schoolid];
    return url;
}
+(NSString *) getAddPinCode:(long long)userId pinCode:(NSString*)pinCode schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLSHOPAPI,@"/api/shop/pin/register?user_id=%lld&pinCode=%@&schoolid=%lld"],userId,pinCode,schoolid];
    return url;
}
+(NSString*)getConfirmPinCodePOST{
    NSString *url = [NSString stringWithFormat:@"%@%@",CALLSHOPAPI,@"/api/shop/pin/check"];
    return url;
}
+(NSString*)getConfirmPinProductPOST:(long long)schoolId shopId:(long long)shopId{
    NSString *url = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@%@",CALLSHOPAPI,@"/api/shop/payment?school_id=%lld&shop_id=%lld"],schoolId,shopId];
    return url;
}

//UpdateLeavePost
+ (NSString *)getUpdateLeavePOST{
    NSString *url = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/studentleave"];
    return url;
}

//CancelLeave
+ (NSString *)getCancelLeaveWithLetterID:(long long)letterId UserID:(long long)userId schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/LeaveRequestCancel?letterid=%lld&userid=%lld&schoolid=%lld"], letterId, userId,schoolid];
    return url;
}

//UpdatePinCode
+ (NSString *)getUpdatePinCode{
    NSString *url = [NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/shop/pin/reset"];
    return url;
}

//ImageBarcode&ImageQrcode
+ (NSString *)getImageBarcodeString:(long long)high width:(long long)width userId:(long long)userId schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/User/barcode/xhdpi/%lld/%lld/%lld?schoolid=%lld"], high,width,userId,schoolid];
    return url;
}
+ (NSString *)getImageQrcodeString:(long long)high width:(long long)width userId:(long long)userId schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/User/qrcode/xhdpi/%lld/%lld/%lld?schoolid=%lld"], high,width,userId,schoolid];
    return url;
}
//Report(New)
+(NSString*) getDataRefillMoney:(long long)schoolId date:(NSString *)date{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/reports/shop/topup?school_id=%lld&daystart=%@"],schoolId,date];
    return url;
}
+(NSString*) getDataRefillMoneyWeek:(long long)schoolId dayStart:(NSString *)dayStart dayEnd:(NSString *)dayEnd{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/reports/shop/topup?school_id=%lld&daystart=%@&dayend=%@"],schoolId,dayStart,dayEnd];
    return url;
}
+(NSString*) getDataPurchasingDay:(long long)schoolId date:(NSString *)date{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/reports/shop/sell?school_id=%lld&daystart=%@&shop_id="],schoolId,date];
    return url;
}
+(NSString*) getDataPurchasingWeek:(long long)schoolId dayStart:(NSString *)dayStart dayEnd:(NSString *)dayEnd{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/reports/shop/sell?school_id=%lld&daystart=%@&dayend=%@&shop_id="],schoolId,dayStart,dayEnd];
    return url;
}
+(NSString*) getReportCheckFlagDateTime:(long long)shcoolId studentId:(NSString *)studentId date:(NSString *)date{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/reports/jobscan/daily/detail?schoolid=%lld&day=%@&studentid=%@"],shcoolId,date,studentId];
    return url;
}
+(NSString*)getDataReportCheckSubjectDateTime:(NSString *)studentId date:(NSString *)date subjectId:(NSString *)subjectId schoolId:(long long)schoolId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/reports/schedule/log?userid=%@&day=%@&planeid=%@&schoolId=%lld"],studentId,date,subjectId,schoolId];
    return url;
}
+(NSString*)getReportInOut:(long long)shcoolId date:(NSString *)date{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/takeschoolattendancereports/v2?schoolid=%lld&dayreports=%@"],shcoolId,date];
    return url;
}
+(NSString*)getReportDataLevel:(long long)shcoolId date:(NSString *)date{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/takeschoolattendancereports/v2/SumStudent4class?schoolid=%lld&dayreports=%@"],shcoolId,date];
    return url;
}
+(NSString*)getReportDataDepartmant:(long long)shcoolId date:(NSString *)date{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/takeschoolattendancereports/v2/Department?schoolid=%lld&dayreports=%@"],shcoolId,date];
    return url;
}
+ (NSString *)getReportUnCheck:(long long)shcoolId date:(NSString *)date{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/reports/jobscan/listday/uncheck?school_Id=%lld&day=%@"],shcoolId,date];
    return url;
}
+ (NSString *)getReportUnCheckListData:(long long)shcoolId date:(NSString *)date{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/reports/jobscan/uncheck?school_Id=%lld&day=%@"],shcoolId,date];
    return url;
}
+ (NSString *) getReportInOutStatus:(long long)shcoolId dayReports:(NSString *)dayReports levelId:(long long)levelId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/takeschoolattendancereports/v2/SumStudent4Rooms?schoolid=%lld&dayreports=%@&levelid=%lld"],shcoolId,dayReports,levelId];
    return url;
}
+ (NSString *) getReportInOutStatusDepaetmant:(long long)shcoolId dayReports:(NSString *)dayReports depaetmanId:(long long)depaetmanId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/takeschoolattendancereports/v2/Department/detail?schoolid=%lld&dayreports=%@&department_id=%lld"],shcoolId,dayReports,depaetmanId];
    return  url;
}

//UserInfo
+ (NSString *)getOpenMessageDataAll:(long long)userId schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/messagebox/Readall?userid=%lld&schoolid=%lld"],userId, schoolid];
    return url;
}

//Top up
+ (NSString *)getTopUpMonney:(long long)userId amount:(NSString *)amount schoolid:(long long)schoolid{
    NSString *url = [NSString stringWithFormat:@"https://system.schoolbright.co/PaymentGateway/KBank/Checkout.aspx?studentID=%lld&refno=JT620404002&amount=%@&schoolid=%lld&lang=%@",userId,amount,schoolid,[UserData getChangLanguage]];
    return url;
}

+ (NSString *)getMoveBackTopUp{
    NSString *url = [NSString stringWithFormat:@"https://system.schoolbright.co/closepage.html"];
    return url;
}
+ (NSString *)getQRCodePaymentOfKbank{
    NSString *url = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@%@",CALLAPI,@"/PaymentGateway/KBank/KBankPromptPay/GenerateQRCode.ashx"]];
    return  url;
}

//AcademicResults
+ (NSString*) getDetailOfAcademicResults:(long long)schoolId studentId:(long long)studentId year:(NSString *)year term:(NSString*)term{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/student/GetGradeInfo?schoolId=%lld&sid=%lld&nyear=%@&sterm=%@"],schoolId,studentId,year,term];
       return url;
}
+ (NSString *)getYearOfAcademicResults:(long long)schoolId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Common/GetYears?schoolId=%lld"],schoolId];
    return url;
}
+ (NSString *)getTermOfAcademicResults:(long long)schoolId nYear:(NSInteger)nYear studentId:(long long)studentId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/Common/GetTerms?schoolId=%lld&nYear=%d&sid=%lld"],schoolId,nYear,studentId];
    return url;
}
+ (NSString *)getStudentPermission:(long long)schoolId studentId:(long long)studentId{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/student/CheckStudentGradePermission?schoolId=%lld&sid=%lld"],schoolId,studentId];
    return url;
}

//AcceptStudentsMaps
+ (NSString *)getLocationSchool:(long long)schoolID studentID:(long long)studentID{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/StudentCall/GetSchoolLocation?schoolid=%lld&sid=%lld"],schoolID,studentID];
    return url;
}
+ (NSString *)getDistanceSatrtPointAndEndPoint:(NSString *)sourceLocation destinationLocation:(NSString *)destinationLocation{
    
    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&mode=driving&key=AIzaSyDANNqMxvkV0bigD2SHi2flWb3lpqd0o20",sourceLocation,destinationLocation];
    return url;
}

+ (NSString *)getStatusGetStudent:(long long)schoolID studentID:(long long)studentID{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/api/StudentCall/SetAnnouncement?schoolid=%lld&sid=%lld&serverType=1"],schoolID,studentID];
    return url;
}

//ClosedForRenovation
+ (NSString *)getStatusServer{
    NSString *url = [NSString stringWithFormat:@"%@/api/SeverStatus",CALLAPI];
    return url;
}

//SendTokenAndLanguage
+ (NSString *)getStatusSendTokenAndLanguage:(NSString *)token language:(NSString *)language{
    NSString *url = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@",CALLAPI,@"/Notification/languageUpdate?Token=%@&lang=%@"],token,language];
    return url;
}
@end






