//
//  ReportRefillMoneyAllDataViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MKDropdownMenu/MKDropdownMenu.h>
#import "SlideMenuController.h"
#import <Charts/Charts.h>
#import "CallReportRefillMoneyAPI.h"
#import "CallReportRefillMoneyWeekAPI.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface ReportRefillMoneyAllDataViewController : UIViewController <SlideMenuControllerDelegate,MKDropdownMenuDelegate,MKDropdownMenuDataSource,UITextFieldDelegate,ChartViewDelegate,IChartAxisValueFormatter,CallReportRefillMoneyAPIDelegate, CallReportRefillMoneyWeekAPIDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>


@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *detailRefillMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *dateTimeDropdown;
@property (weak, nonatomic) IBOutlet BarChartView *barCharView;

- (IBAction)moveBack:(id)sender;

@end
