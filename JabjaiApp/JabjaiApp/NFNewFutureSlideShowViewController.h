//
//  NFNewFutureSlideShowViewController.h
//  JabjaiApp
//
//  Created by toffee on 21/5/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NFNewFutureSlideShowCollectionViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface NFNewFutureSlideShowViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIView *contentView;
//- (IBAction)leftAction:(id)sender;
//- (IBAction)rightAction:(id)sender;

- (IBAction)closeAction:(id)sender;
- (IBAction)rightAction:(id)sender;
- (IBAction)leftAction:(id)sender;




-(void)showNewFutureSlideShow:(UIView*)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end

NS_ASSUME_NONNULL_END
