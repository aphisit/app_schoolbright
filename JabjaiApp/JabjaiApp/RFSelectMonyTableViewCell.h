//
//  RFSelectMonyTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 17/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RFSelectMonyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;
@property (weak, nonatomic) IBOutlet UIView *section1;

@end

NS_ASSUME_NONNULL_END
