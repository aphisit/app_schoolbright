//
//  reachabilityChangedDialog.m
//  JabjaiApp
//
//  Created by toffee on 5/21/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "reachabilityChangedDialog.h"
#import "Utils.h"
@interface reachabilityChangedDialog ()
@property (nonatomic, assign) BOOL isShowing;
@end

@implementation reachabilityChangedDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dialogView.layer.masksToBounds = YES;
    self.cancelBtn.layer.masksToBounds = YES;
    self.isShowing = NO;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientCancel;
    //set color header
    [self.cancelBtn layoutIfNeeded];
    gradientCancel = [Utils getGradientColorStatus:@"green"];
    gradientCancel.frame = self.cancelBtn.bounds;
    [self.cancelBtn.layer insertSublayer:gradientCancel atIndex:0];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

#pragma mark - Dialog Functions

- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

-(void)showDialogInView:(UIView *)targetView message:(NSString *)message{
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    
    [self.titleLabel setText:message];
    self.isShowing = YES;
}
-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogClose)]) {
        [self.delegate onAlertDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
