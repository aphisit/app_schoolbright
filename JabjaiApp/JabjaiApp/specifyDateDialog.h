//
//  specifyDateDialog.h
//  JabjaiApp
//
//  Created by toffee on 9/21/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol specifyDateDialogDelegate <NSObject>
@optional
- (void)onAlertDialogConfirm:(NSDate*)startDate endDate:(NSDate*)endDate;
@end
@interface specifyDateDialog : UIViewController
@property (retain, nonatomic) id<specifyDateDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *headerSpacifyTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerToLabel;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIDatePicker *startDatePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *endDatePicker;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)confirmAction:(id)sender;
- (IBAction)cancelAction:(id)sender;


- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
