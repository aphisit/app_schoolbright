//
//  ARSubjectScoreModel.h
//  JabjaiApp
//
//  Created by toffee on 19/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ARSubjectScoreModel : NSObject
@property (nonatomic) NSString* courseCode;
@property (nonatomic) NSString* courseName;
@property (nonatomic) NSString* status;
@property (nonatomic) NSString* score;
@property (nonatomic) NSString* grade;

-(void)setCourseCode:(NSString * _Nonnull)courseCode;
-(void)setCourseName:(NSString * _Nonnull)courseName;
-(void)setStatus:(NSString * _Nonnull)status;
-(void)setScore:(NSString*)score;
-(void)setGrade:(NSString*)grade;

-(NSString*)getCourseCode;
-(NSString*)getCourseName;
-(NSString*)getStatus;
-(NSString*)getScore;
-(NSString*)getGrade;
@end

NS_ASSUME_NONNULL_END
