//
//  UserData.h
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    STUDENT, TEACHER, USERTYPENULL
} USERTYPE;

typedef enum {
    SCHOOL, UNIVERSITY, ACADEMYTYPENULL
} ACADEMYTYPE;

typedef enum {
    MALE, FEMALE
} GENDERTYPE;

static NSString *MASTER_USERID = @"MASTER_USERID";
static NSString *USERID = @"USERID";
static NSString *USERLOGIN = @"USERLOGIN";
static NSString *USER_TYPE = @"USERTYPE";
static NSString *ACADEMY_TYPE = @"ACADEMYTYPE";
static NSString *SCHOOLID = @"SCHOOLID";
static NSString *USER_IMAGE = @"USER_IMAGE";
static NSString *FIRSTNAME = @"FIRSTNAME";
static NSString *LASTNAME = @"LASTNAME";
static NSString *GENDER = @"GENDER";
static NSString *APPVERSION = @"APPVERSION";
static NSString *SCHOOL_IMAGE = @"SCHOOL_IMAGE";
static NSString *SCHOOL_PIC = @"SCHOOL_PIC";
static NSString *LANGUAGE = @"LANGUAGE";
static NSString *TOKEN = @"TOKEN";
static NSString *SHOWSOMEID = @"SHOWSOMEID";
static NSString *USERNAME = @"USERNAME";
static NSString *PASSWORD = @"PASSWORD";


@interface UserData : NSObject

+ (void)setUserLogin:(BOOL)login;
+ (BOOL)isUserLogin;
+ (void)saveMasterUserID:(NSInteger)masterId;
+ (NSInteger)getMasterUserID;
+ (void)saveUserID:(NSInteger)userid;
+ (NSInteger)getUserID;
+ (void)saveSchoolId:(long long)schoolId;
+ (long long)getSchoolId;
+ (void)setUserImage:(NSString *)imageUrl;
+ (NSString *)getUserImage;
+ (void)setFirstName:(NSString *)firstName;
+ (NSString *)getFirstName;
+ (void)setLastName:(NSString *)lastName;
+ (NSString *)getLastName;
+ (void)setAppVersion:(NSString *)appVersion;
+ (NSString *)getAppVersion;
+(void)setSchoolImage:(NSData *)appVersion;
+ (NSData *)getSchoolImage;
+(void)setSchoolPic:(NSString *)schoolPic;
+ (NSString *)getSchoolPic;
+(void)setChangLanguage:(NSString *)changLanguage;
+ (NSString *)getChangLanguage;
+(void)setClientToken:(NSString*)clientToken;
+(NSString *)getClientToken;
+(void)setShowSomeId:(NSString*)switchSomeId;
+(NSString*)getShowSomeId;
+(void)setUsername:(NSString*)username;
+(NSString*)getUsername;
+(void)setPassword:(NSString*)password;
+(NSString*)getPassword;

+ (void)setUserType:(USERTYPE)userType;
+ (USERTYPE)getUserType;
+ (void)setAcademyType:(ACADEMYTYPE)academyType;
+ (ACADEMYTYPE)getAcademyType;
+ (void)setGender:(GENDERTYPE)genderType;
+ (GENDERTYPE)getGender;
@end
