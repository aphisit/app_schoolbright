//
//  SNReadImageViewController.h
//  JabjaiApp
//
//  Created by toffee on 5/8/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertDialogConfirm.h"
NS_ASSUME_NONNULL_BEGIN
@class SNReadImageViewController;

@protocol SNReadImageViewControllerDelegate <NSObject>
@end
@interface SNReadImageViewController : UIViewController<UIScrollViewDelegate, AlertDialogConfirmDelegate>

-(void)showPagesReadImageInView:(UIView *)targetView ImageArray:(NSArray*)imageArray;
@property (nonatomic, retain) id<SNReadImageViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *hideLoadView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *saveImageBtn;

- (IBAction)closeAction:(id)sender;
- (IBAction)saveFileAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
