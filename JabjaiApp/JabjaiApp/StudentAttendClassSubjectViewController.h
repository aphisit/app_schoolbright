//
//  StudentAttendClassSubjectViewController.h
//  JabjaiApp
//
//  Created by mac on 1/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeachingSubjectDialog.h"

@interface StudentAttendClassSubjectViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, TeachingSubjectDialogDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblView;

- (IBAction)openDrawer:(id)sender;
- (IBAction)showFilterDialog:(id)sender;

@end
