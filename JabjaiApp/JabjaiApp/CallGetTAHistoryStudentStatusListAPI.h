//
//  CallGetTAHistoryStudentStatusListAPI.h
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPDataStudentCheckSubjectModel.h"

@class CallGetTAHistoryStudentStatusListAPI;

@protocol CallGetTAHistoryStudentStatusListAPIDelegate <NSObject>

@optional
- (void)callGetTAHistoryStudentStatusListAPI:(CallGetTAHistoryStudentStatusListAPI *)classObj data:(NSArray<RPDataStudentCheckSubjectModel *> *)data success:(BOOL)success;

@end

@interface CallGetTAHistoryStudentStatusListAPI : NSObject

@property (nonatomic, weak) id<CallGetTAHistoryStudentStatusListAPIDelegate> delegate;

- (void)call:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId date:(NSDate *)date;

@end
