//
//  CallClearDataSystemNotificationPOSTAPI.h
//  JabjaiApp
//
//  Created by Mac on 5/3/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CallClearDataSystemNotificationPOSTAPIDelegate <NSObject>
@optional
- (void)callClearDataSystemNotificationPOSTAPI:(BOOL)success;
@end
@interface CallClearDataSystemNotificationPOSTAPI : NSObject
@property (nonatomic, weak)id<CallClearDataSystemNotificationPOSTAPIDelegate> delegate;
- (void)call:(NSString*)token;
@end

NS_ASSUME_NONNULL_END
