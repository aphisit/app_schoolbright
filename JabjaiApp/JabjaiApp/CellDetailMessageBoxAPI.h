//
//  CellDetailMessageBoxAPI.h
//  JabjaiApp
//
//  Created by Mac on 27/4/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNDetailMessageBoxModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CellDetailMessageBoxAPI;
@protocol CellDetailMessageBoxAPIDelegate <NSObject>
@optional
- (void)callDetailMessageBoxAPI:(CellDetailMessageBoxAPI *)classObj data:(SNDetailMessageBoxModel*)data success:(BOOL)succevss;
@end
@interface CellDetailMessageBoxAPI : NSObject
@property (nonatomic, weak) id<CellDetailMessageBoxAPIDelegate> delegate;
- (void)call:(long long)userID messageID:(long long)messageID schoolid:(long long)schoolid;
@end

NS_ASSUME_NONNULL_END
