//
//  CallBSGetDataStudentScanBehaviorAPI.h
//  JabjaiApp
//
//  Created by toffee on 24/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSDataStudentScanerModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallBSGetDataStudentScanBehaviorAPI;

@protocol CallBSGetDataStudentScanBehaviorAPIDelegate <NSObject>
@optional
- (void)callBSGetDataStudentScanBarcodeAPI:(CallBSGetDataStudentScanBehaviorAPI *)classObj data:(BSDataStudentScanerModel*)data resCode:(NSInteger)resCode success:(BOOL)success;
@end
@interface CallBSGetDataStudentScanBehaviorAPI : NSObject
@property (nonatomic, weak) id<CallBSGetDataStudentScanBehaviorAPIDelegate> delegate;
- (void)call:(long long)schoolId idStudent:(NSString *)idStudent;
@end

NS_ASSUME_NONNULL_END
