//
//  ARSuggestionViewController.h
//  JabjaiApp
//
//  Created by toffee on 4/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARDetailAcademicResultsViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface ARSuggestionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (nonatomic, strong) NSString *termStr;
@property (nonatomic, strong) NSString *yearId;
@property (nonatomic, strong) NSString *yearStr;
@property (nonatomic, strong) NSArray *termArray;
@property (nonatomic, strong) NSMutableArray *yearNumberArray;
@property (nonatomic, strong) NSMutableArray *yearIdArray;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *suggestionImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)moveBackAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
