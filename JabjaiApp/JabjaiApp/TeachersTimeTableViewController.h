//
//  TeachersTimeTableViewController.h
//  JabjaiApp
//
//  Created by toffee on 19/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeacherTimeTableSelectDayCollectionViewCell.h"
#import "CallTeacherTimeDayAPI.h"
#import "CallTeacherTimeTableDetailAPI.h"
#import "TeacherTimeTableListSubjectTableViewCell.h"
#import "TeacherTimeTableListSubjectDetailTableViewCell.h"
#import "SScheduleDetailModel.h"
#import "SlideMenuController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
NS_ASSUME_NONNULL_BEGIN

@interface TeachersTimeTableViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate ,UITableViewDataSource, CallTeacherTimeDayAPIDelegate,CallTeacherTimeTableDetailAPIDelegate,SlideMenuControllerDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *monthLable;
@property (weak, nonatomic) IBOutlet UILabel *headerTimeTableLabel;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *showHaveNotSubject;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)openDrawer:(id)sender;
@end

NS_ASSUME_NONNULL_END
