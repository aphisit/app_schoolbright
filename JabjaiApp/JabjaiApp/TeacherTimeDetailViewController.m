//
//  TeacherTimeDetailViewController.m
//  JabjaiApp
//
//  Created by toffee on 5/15/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "TeacherTimeDetailViewController.h"
#import "TeacherTimeTableViewController.h"
#import "Utils.h"
#import "UserData.h"
@interface TeacherTimeDetailViewController (){
    
}
@property (strong, nonatomic) CallTeacherTimeDetailAPI *callTeacherTimeDetailAPI;
@end

@implementation TeacherTimeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    [self getTeachTimeDetail];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getTeachTimeDetail{
    
    if(self.callTeacherTimeDetailAPI != nil) {
        self.callTeacherTimeDetailAPI = nil;
    }
    
    self.callTeacherTimeDetailAPI = [[CallTeacherTimeDetailAPI alloc] init];
    self.callTeacherTimeDetailAPI.delegate = self;
     [self.callTeacherTimeDetailAPI call:_ScheduleId date:_date schoolid:[UserData getSchoolId]];
}

-(void)callTeacherTimeDetailAPI:(CallTeacherTimeDetailAPI *)classObj subjectName:(NSString *)subjectName subjectID:(NSString *)subjectID classRoom:(NSString *)classRoom timeIn:(NSString *)timeIn timeOut:(NSString *)timeOut success:(BOOL)success{
    [_subjectName setText: [NSString stringWithFormat:@"วิชา %@",subjectName]];
    [_subjectID setText: [NSString stringWithFormat:@"รหัส %@",subjectID]];
    [_classRoom setText: [NSString stringWithFormat:@"ระดับชั้น %@",classRoom]];
    [_timeIn setText: [NSString stringWithFormat:@"%@ น.",timeIn]];
    [_timeOut setText: [NSString stringWithFormat:@"%@ น.",timeOut]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)moveBack:(id)sender {
    TeacherTimeTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TeacherTimeTableStoryBoard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
