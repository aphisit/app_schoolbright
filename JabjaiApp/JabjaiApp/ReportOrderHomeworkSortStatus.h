//
//  ReportOrderHomeworkSortStatus.h
//  JabjaiApp
//
//  Created by mac on 4/30/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportOrderHomeworkSortStatus : NSObject

@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) BOOL status;

@end
