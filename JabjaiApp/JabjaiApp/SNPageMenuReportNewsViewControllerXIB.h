//
//  SNPageMenuReportNewsViewControllerXIB.h
//  JabjaiApp
//
//  Created by Mac on 17/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "KxMenu.h"
#import "SNNewsDetailViewControllerXIB.h"
#import "SNReciverViewControllerXIB.h"
#import "SNReciverViewControllerXIB.h"
NS_ASSUME_NONNULL_BEGIN
@protocol SNPageMenuReportNewsViewControllerXIBDelegate <NSObject>

- (void) close;

@end
@interface SNPageMenuReportNewsViewControllerXIB : UIViewController <CAPSPageMenuDelegate>
@property (nonatomic, retain) id<SNPageMenuReportNewsViewControllerXIBDelegate> delegate;
@property (assign, nonatomic) long long newsID;
@property (nonatomic, strong) NSString *sortData;

//@property (nonatomic, strong) HWTDetailPersonViewControllerXIB *detailController;
@property (nonatomic, strong) SNReciverViewControllerXIB *recieverController;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *pageMenuContentView;

@property (weak, nonatomic) IBOutlet UIButton *sortButton;
- (IBAction)moveBack:(id)sender;
- (void)dismissNewsViewController;
- (BOOL)isDialogShowing;
- (void) showNewsDetailController:(UIView*)targetView newsID:(long long)newsID sortData:(NSString*)sortData;
@end

NS_ASSUME_NONNULL_END
