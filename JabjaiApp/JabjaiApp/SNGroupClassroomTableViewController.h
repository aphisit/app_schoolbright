//
//  SNGroupClassroomTableViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"

@interface SNGroupClassroomTableViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
