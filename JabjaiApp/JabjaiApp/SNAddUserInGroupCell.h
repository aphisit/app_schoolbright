//
//  SNAddUserInGroupCell.h
//  JabjaiApp
//
//  Created by toffee on 2/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
@class SNAddUserInGroupCell;
@protocol SNAddUserInGroupCellDelegate <NSObject>
- (void)onPressAddButton:(SNAddUserInGroupCell *)collectionViewCell atIndex:(NSInteger)index;
@end

@interface SNAddUserInGroupCell : UICollectionViewCell
@property (weak, nonatomic) id<SNAddUserInGroupCellDelegate> delegate;

@property (nonatomic) NSInteger index;

@property (weak, nonatomic) IBOutlet UIImageView *imageMembers;
@property (weak, nonatomic) IBOutlet UIButton *btnAddMembers;
@property (weak, nonatomic) IBOutlet UILabel *nameMembers;

- (IBAction)actionAddMembersButton:(id)sender;


@end
