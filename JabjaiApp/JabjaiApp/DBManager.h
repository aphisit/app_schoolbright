//
//  DBManager.h
//  JabjaiInboxMessageDBApp
//
//  Created by mac on 11/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "SlideMenuController.h"
@interface DBManager : NSObject 

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

@property (nonatomic, strong) NSMutableArray *arrColumnNames;
@property (nonatomic) int affectedRows;
@property (nonatomic) long lastInsertedRowID;

-(NSArray *)loadDataFromDB:(NSString *)query;
-(BOOL)executeQuery:(NSString *)query;
- (void)insertColumnQuery:(const char *)query;
- (void)checkColumnExists;
- (void)doCheckcolumnToken;
- (void)doCheckcolumnUsername;
- (void)doCheckcolumnPassword;


@end
