//
//  ReportInOutViewController.m
//  JabjaiApp
//
//  Created by mac on 4/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportInOutViewController.h"
#import "UserData.h"
#import "AttendSchoolCalendarViewController.h"
#import "AttendSchoolAgendaTableViewController.h"
#import "APIURL.h"
#import "Utils.h"
#import "DateUtility.h"
#import "YearTermModel.h"
#import "Attend2SchoolFilter.h"
#import "Constant.h"

#import "ReportListTeacherViewController.h"
#import "ReportListStudentViewController.h"

@interface ReportInOutViewController (){
    UIColor *pagerOrangeColor;
    UIColor *pagerGreenColor;
    UIColor *indicatorOrangeColor;
    UIColor *indicatorGreenColor;
    
    // Data Containers
    NSMutableDictionary *yearTermDict; //Key is yearNumber and data are array of YearTermModel
    
    NSInteger connectCounter;
    
    UIColor *OrangeColor;
    UIColor *GrayColor;
}

@property (nonatomic) CAPSPageMenu *pageMenu;

//
@property (strong, nonatomic) NSCalendar *gregorian;

@property (nonatomic, strong) CalendarDialog *calendarDialog;
@property (nonatomic, strong) Attend2SchoolReportFilterDialog *filterDialog;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@property (nonatomic, strong) AttendSchoolCalendarViewController *calendarController;
@property (nonatomic, strong) AttendSchoolAgendaTableViewController *agendaController;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation ReportInOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    [self setLanguage];
    pagerOrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1];
    pagerGreenColor = [UIColor colorWithRed:73/255.0 green:160/255.0 blue:152/255.0 alpha:1];
    indicatorOrangeColor = [UIColor colorWithRed:254/255.0 green:180/255.0 blue:57/255.0 alpha:1];
    indicatorGreenColor = [UIColor colorWithRed:31/255.0 green:124/255.0 blue:114/255.0 alpha:1];

    OrangeColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.40 alpha:1.0];
    GrayColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.gregorian = [Utils getGregorianCalendar];
    
    NSDate *minDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:[NSDate date] options:0];
    NSDate *maxDate = [NSDate date];
    self.calendarDialog = [[CalendarDialog alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_CALENDAR",nil,[Utils getLanguage],nil) minimumDate:minDate maximumDate:maxDate];
    
    connectCounter = 0;
    
    [self callAPIGetYearAndTerm];
    [self setupPageMenu];
    [self doDesignLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) setLanguage{
    if ([UserData getUserType] != STUDENT) {
           self.headerLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_WORKING",nil,[Utils getLanguage],nil);
    }else{
           self.headerLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ATTEND_CLASS",nil,[Utils getLanguage],nil);
    }
}

- (void) doDesignLayout{
    //set color header
    CAGradientLayer *gradientHeader;
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}

- (void)setupPageMenu {
    self.calendarController = [[AttendSchoolCalendarViewController alloc] init];
    self.calendarController.title = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_CALENDAR",nil,[Utils getLanguage],nil);
    
    self.agendaController = [[AttendSchoolAgendaTableViewController alloc] init];
    self.agendaController.title = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_HISTORY",nil,[Utils getLanguage],nil);
    self.agendaController.maximumDate = [NSDate date];
    self.agendaController.minimumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:[NSDate date] options:0];
    //    self.gregorian = [Utils getGregorianCalendar];
    //    self.maximumDate = [NSDate date];
    //    self.minimumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:[NSDate date] options:0];
    
    
    NSArray *controllerArray = @[self.calendarController, self.agendaController];
    
    // Color background for each controller tab
    NSDictionary *parameters;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(40),
                       CAPSPageMenuOptionMenuHeight: @(70),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:36.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    else{
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(20),
                       CAPSPageMenuOptionMenuHeight: @(40),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:26.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    [self.contentView layoutIfNeeded];
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    NSArray *menuItems = _pageMenu.menuItems;
    MenuItemView *menuItemView = [menuItems objectAtIndex:_pageMenu.currentPageIndex];
    menuItemView.backgroundColor = OrangeColor ;
    menuItemView.titleLabel.textColor = [UIColor whiteColor];
    
    //Optional delegate
    _pageMenu.delegate = self;
    
    
    
    [self.contentView addSubview:_pageMenu.view];
    
}

- (IBAction)showFilterDialog:(id)sender {
    
    //[self.calendarDialog showDialogInView:self.view];
    if(self.filterDialog != nil) {
        [self.filterDialog showDialogInView:self.view title:@"Filter"];
    }
    
}

#pragma CAPSPageMenuDelegate
- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    NSLog(@"WillMoveToPage");
    
//    if(index == 0) {
//        _pageMenu.selectionIndicatorView.backgroundColor = indicatorOrangeColor;
//    }
//    else {
//        _pageMenu.selectionIndicatorView.backgroundColor = indicatorGreenColor;
//
//    }
    
    NSArray *menuItems = _pageMenu.menuItems;
    
    int count = 0;
    
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = OrangeColor;
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        
        count++;
    }
    
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
    NSLog(@"DidMoveToPage");
    
//    if(index == 0) {
//        _pageMenu.selectionIndicatorView.backgroundColor = indicatorOrangeColor;
//    }
//    else {
//        _pageMenu.selectionIndicatorView.backgroundColor = indicatorGreenColor;
//
//    }
    
    NSArray *menuItems = _pageMenu.menuItems;
    
    int count = 0;
    
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = OrangeColor;
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        
        count++;
    }
    
}

#pragma mark - GetAPIData

-(void)callAPIGetYearAndTerm {
    [self getYearAndTermData];
}

-(void)getYearAndTermData {
    NSString *URLString = [APIURL getYearAndTermURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self callAPIGetYearAndTerm];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetYearAndTerm];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetYearAndTerm];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                yearTermDict = [[NSMutableDictionary alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSInteger yearID = [[dataDict objectForKey:@"Yearid"] integerValue];
                    NSInteger yearNumber;
                    if(![[dataDict objectForKey:@"YearNumber"] isKindOfClass:[NSNull class]]) {
                        yearNumber = [[dataDict objectForKey:@"YearNumber"] integerValue];
                    }
                    else {
                        yearNumber = 0;
                    }
                    NSMutableString *termID = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Termid"]];
                    NSMutableString *termName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TermName"]];
                    NSMutableString *startDateStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dStart"]];
                    NSMutableString *endDateStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dEnd"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) termID);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) termName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endDateStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *lStartDate = [formatter dateFromString:startDateStr];
                    NSDate *lEndDate = [formatter dateFromString:endDateStr];
                    
                    YearTermModel *model = [[YearTermModel alloc] init];
                    [model setYearID:[[NSNumber alloc] initWithInteger:yearID]];
                    [model setYearNumber:[[NSNumber alloc] initWithInteger:yearNumber]];
                    [model setTermID:termID];
                    [model setTermName:termName];
                    [model setTermBegin:lStartDate];
                    [model setTermEnd:lEndDate];
                    
                    //                    NSLog(@"%@", [NSString stringWithFormat:@"Term Start : %@", [Utils dateInStringFormat:lStartDate format:@"yyyy-MM-dd"]]);
                    //                    NSLog(@"%@", [NSString stringWithFormat:@"Term End : %@", [Utils dateInStringFormat:lEndDate format:@"yyyy-MM-dd"]]);
                    
                    if([yearTermDict objectForKey:model.yearNumber] != nil) {
                        NSMutableArray *termArr = [yearTermDict objectForKey:model.yearNumber];
                        [termArr addObject:model];
                        [yearTermDict setObject:termArr forKey:model.yearNumber];
                    }
                    else {
                        NSMutableArray *termArr = [[NSMutableArray alloc] init];
                        [termArr addObject:model];
                        [yearTermDict setObject:termArr forKey:model.yearNumber];
                    }
                }
                
                // Initialize filter dialog
                if(self.filterDialog != nil && [self.filterDialog isDialogShowing]) {
                    [self.filterDialog dismissDialog];
                    self.filterDialog = nil;
                }
                
                self.filterDialog = [[Attend2SchoolReportFilterDialog alloc] initWithYearTermDictionary:yearTermDict];
                self.filterDialog.delegate = self;
                
            }
        }
        
    }];
    
}

#pragma mark - Attend2SchoolReportFilterDialogDelegate

-(void)applyFilter:(NSNumber *)schoolYear semester:(NSString *)semester status:(NSString *)status startDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    
    Attend2SchoolFilter *filter = [[Attend2SchoolFilter alloc] init];
    filter.schoolYear = schoolYear;
    filter.semester = semester;
    
    if([status isEqualToString:@"ตรงเวลา"]) {
        filter.status = INTIME;
    }
    else if([status isEqualToString:@"สาย"]) {
        filter.status = LATE;
    }
    else if([status isEqualToString:@"ขาด"]) {
        filter.status = ABSENCE;
    }
    else if([status isEqualToString:@"ลา"]) {
        filter.status = ONLEAVE;
    }
    else if([status isEqualToString:@"วันหยุด"]) {
        filter.status = HOLIDAY;
    }
    else if([status isEqualToString:@"ลาป่วย"]) {
        filter.status = SICK;
    }
    else if([status isEqualToString:@"ลากิจ"]) {
        filter.status = PERSONAL;
    }
    else if([status isEqualToString:@"ไม่เช็คชื่อ"]) {
        filter.status = UNCHECK;
    }
    else {
        filter.status = ALL;
    }
    
    filter.startDate = startDate;
    filter.endDate = endDate;
    
    if(self.calendarController != nil) {
        [self.calendarController applyFilter:filter];
    }
    
    if(self.agendaController != nil) {
        [self.agendaController applyFilter:filter];
    }
    
    NSLog(@"%@", @"Apply Filter");
}




- (IBAction)moveBack:(id)sender {
    
    if ([UserData getUserType] == STUDENT) {
        ReportListStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListStudentStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
    }
    else {
        ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    
    
}
@end
