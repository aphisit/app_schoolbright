//
//  DatePickerDialog.h
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  DatePickerDialogDelegate

-(void)onSelectDate:(NSDate *)date;

@end

@interface DatePickerDialog : UIViewController <UIGestureRecognizerDelegate>

@property (nonatomic, retain) id<DatePickerDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;

- (IBAction)cancelAction:(id)sender;
- (IBAction)okAction:(id)sender;

- (instancetype)initWithDate:(NSDate *)date;
- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
