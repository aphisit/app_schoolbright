//
//  TEStatusDialogViewController.m
//  JabjaiApp
//
//  Created by toffee on 1/16/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "TEStatusDialogViewController.h"
#import "Utils.h"
#import "UserData.h"


@interface TEStatusDialogViewController (){
     BOOL isShowing;
    NSInteger chackmode;
    
    CAGradientLayer *greenGradient;
    CAGradientLayer *yellowGradient;
    CAGradientLayer *redGradient;
    CAGradientLayer *blueGradient;
    CAGradientLayer *purpleGradient;
    CAGradientLayer *pinkGradient;
    CAGradientLayer *grayGradient;
    
    NSBundle *myLangBundle;
}

@end

@implementation TEStatusDialogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    isShowing = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource: [UserData getChangLanguage] ofType:@"lproj"]];
    CGFloat max = MAX(self.statusOnTimeButton.frame.size.width, self.statusOnTimeButton.frame.size.height);
    CGFloat cornerRadius = max / 2.0;
    
    [self.statusOnTimeButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ONTIME",nil,myLangBundle,nil) forState:UIControlStateNormal];
    self.statusOnTimeButton.layer.cornerRadius = cornerRadius;
    self.statusOnTimeButton.layer.masksToBounds = YES;
    greenGradient = [Utils getGradientColorStatus:@"green"];
    greenGradient.frame = self.statusOnTimeButton.bounds;
    [self.statusOnTimeButton.layer insertSublayer:greenGradient atIndex:0];
    
    [self.statusLateButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_LATE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    self.statusLateButton.layer.cornerRadius = cornerRadius;
    self.statusLateButton.layer.masksToBounds = YES;
    yellowGradient = [Utils getGradientColorStatus:@"yellow"];
    yellowGradient.frame = self.statusLateButton.bounds;
    [self.statusLateButton.layer insertSublayer:yellowGradient atIndex:0];
    
    [self.statusAbsenceButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ABSENCE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    self.statusAbsenceButton.layer.cornerRadius = cornerRadius;
    self.statusAbsenceButton.layer.masksToBounds = YES;
    redGradient = [Utils getGradientColorStatus:@"red"];
    redGradient.frame = self.statusAbsenceButton.bounds;
    [self.statusAbsenceButton.layer insertSublayer:redGradient atIndex:0];
    
    [self.statusEventButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_EVENT_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    self.statusEventButton.layer.cornerRadius = cornerRadius;
    self.statusEventButton.layer.masksToBounds = YES;
    blueGradient = [Utils getGradientColorStatus:@"blue"];
    blueGradient.frame = self.statusEventButton.bounds;
    [self.statusEventButton.layer insertSublayer:blueGradient atIndex:0];
    
    [self.statusPersonalLeaveButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_PERSONAL_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    self.statusPersonalLeaveButton.titleLabel.numberOfLines = 0;
    self.statusPersonalLeaveButton.layer.cornerRadius = cornerRadius;
    self.statusPersonalLeaveButton.layer.masksToBounds = YES;
    pinkGradient = [Utils getGradientColorStatus:@"purple"];
    pinkGradient.frame = self.statusPersonalLeaveButton.bounds;
    [self.statusPersonalLeaveButton.layer insertSublayer:pinkGradient atIndex:0];
    
    [self.statusSickLeaveButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SICK_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    self.statusSickLeaveButton.titleLabel.numberOfLines = 0;
    self.statusSickLeaveButton.layer.cornerRadius = cornerRadius;
    self.statusSickLeaveButton.layer.masksToBounds = YES;
    purpleGradient = [Utils getGradientColorStatus:@"pink"];
    purpleGradient.frame = self.statusSickLeaveButton.bounds;
    [self.statusSickLeaveButton.layer insertSublayer:purpleGradient atIndex:0];
    
    [self.noStatus setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_UNDEFINED",nil,myLangBundle,nil) forState:UIControlStateNormal];
    self.noStatus.titleLabel.numberOfLines = 0;
    grayGradient = [Utils getGradientColorStatus:@"gray"];
    grayGradient.frame = self.noStatus.bounds;
    [self.noStatus.layer insertSublayer:grayGradient atIndex:0];
   
    if(chackmode == 2){
        self.noStatus.hidden = YES;
    }
    self.noStatus.layer.cornerRadius = cornerRadius;
    self.noStatus.layer.masksToBounds = YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)selectStatus:(id)sender {
    
    if(self.delegate != nil) {
        
        NSInteger tag = ((UIButton *)sender).tag;
        
        TA_SCAN_STATUS scanStatus = TA_NOTSCAN;
        
        
        switch (tag) {
//            case 1: scanStatus = TA_ONTIME; break;
//            case 2: scanStatus = TA_LATE; break;
//            case 3: scanStatus = TA_ABSENCE; break;
//            case 4: scanStatus = TA_NOTSCAN; break;
//            case 5: scanStatus = TA_PERSONAL_LEAVE; break;
//            case 6: scanStatus = TA_SICK_LEAVE; break;
                
            case 1: scanStatus = TA_ONTIME; break;
            case 2: scanStatus = TA_LATE; break;
            case 3: scanStatus = TA_ABSENCE; break;
            case 4: scanStatus = TA_EVENT; break;
            case 5: scanStatus = TA_PERSONAL_LEAVE; break;
            case 6: scanStatus = TA_SICK_LEAVE; break;
            case 7: scanStatus = TA_NOTSCAN; break;
                
        }
        
        [self.delegate taStatusDialog:self onSelectStatus:scanStatus];
        
    }
    
    [self dismissDialog];
}

#pragma mark - Dialog functions

- (void)showDialogInView:(UIView *)targetView mode:(NSInteger)mode {
    chackmode = mode;
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view by hide it
    //[self.dialogStackView setAlpha:0.0];
    
    // Animate the display of the dialog ivew
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    //[self.dialogStackView setAlpha:1.0];
    [UIView commitAnimations];
    
    isShowing = YES;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return isShowing;
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //[self.dialogStackView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    isShowing = NO;
    
}
@end
