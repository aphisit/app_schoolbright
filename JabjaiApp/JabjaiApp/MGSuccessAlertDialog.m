//
//  MGSuccessAlertDialog.m
//  JabjaiApp
//
//  Created by toffee on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "MGSuccessAlertDialog.h"
#import "UserData.h"
#import "Utils.h"

@interface MGSuccessAlertDialog ()
@property (nonatomic, assign) BOOL isShowing;
@end

@implementation MGSuccessAlertDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dialogView.layer.masksToBounds = YES;
    self.isShowing = NO;
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
}
 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
     
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}
-(void)showDialogInView:(UIView *)targetView data:(NSDictionary *)data{
    
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    NSLog(@"xxx = %@",[data objectForKey:@"name"]);
    [self.firstName setText:[data objectForKey:@"name"]];
    [self.lastName setText:[data objectForKey:@"lastname"]];
   
    if ([[data objectForKey:@"picture"] isEqualToString: @""] ) {

        self.senderImage.image = [UIImage imageNamed:@"ic_user_info"];
    }else{
        NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [data objectForKey:@"picture"]]];
        [self.senderImage setImage:[UIImage imageWithData:imageData]];
        self.senderImage.layer.cornerRadius = self.senderImage.frame.size.height /2;
        self.senderImage.layer.masksToBounds = YES;
        self.senderImage.layer.borderWidth = 0;
    }
   
    [self.nameShopLabel setText:[data objectForKey:@"shop_name"]];
    [self.totalMonny setText:[NSString stringWithFormat:@"%d",[[data objectForKey:@"payment"]integerValue]]];
    
    NSString *date = [data objectForKey:@"time"] ;
    NSArray *array1 = [[NSString stringWithFormat:@"%@", date] componentsSeparatedByString:@"."];
    NSArray *array2 = [[NSString stringWithFormat:@"%@", array1[0]] componentsSeparatedByString:@"T"];
     _dateTimeLabel.text = [NSString stringWithFormat:@"%@น. %@",array2[1],array2[0]];

    
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogClose)]) {
        [self.delegate onAlertDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
