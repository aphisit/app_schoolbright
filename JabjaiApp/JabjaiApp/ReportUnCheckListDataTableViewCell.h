//
//  ReportUnCheckListDataTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 21/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportUnCheckListDataTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *teacherNameLabel;


@end

NS_ASSUME_NONNULL_END
