//
//  CallMUListAllSchoolAPI.h
//  JabjaiApp
//
//  Created by toffee on 28/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MUAllSchoolModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallMUListAllSchoolAPI;

@protocol CallMUListAllSchoolAPIDelegate <NSObject>

- (void)callMUListAllSchoolNameAPI:(CallMUListAllSchoolAPI *)classObj data:(NSArray<MUAllSchoolModel *> *)data success:(BOOL)success;
@end

@interface CallMUListAllSchoolAPI : NSObject
@property (nonatomic, weak) id<CallMUListAllSchoolAPIDelegate> delegate;
- (void)call;
@end

NS_ASSUME_NONNULL_END
