//
//  ReportSendNewsRecieverViewController.h
//  JabjaiApp
//
//  Created by mac on 4/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportSendNewsRecieverViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (assign, nonatomic) long long newsID;

@property (nonatomic, strong) NSString *sortData;
@property (nonatomic, strong) NSString *sortChangeData;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (void)getRecieverListWithnewsID:(long long)newsId page:(NSInteger)page readStatus:(NSString *)readStatus;


@end
