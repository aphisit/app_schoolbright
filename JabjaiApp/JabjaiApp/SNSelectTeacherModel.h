//
//  SNSelectTeacherModel.h
//  JabjaiApp
//
//  Created by toffee on 9/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNSelectTeacherModel : UIViewController
@property (nonatomic) long long teacherId;
@property (nonatomic) NSString *teacherName;
@property (nonatomic) BOOL selected;

- (void)setTeacherId:(long long)teacherId;
- (void)setTeacherName:(NSString *)teacherName;
- (void)setSelected:(BOOL)selected;

- (long long)getTeacherId;
- (NSString *)getTeacherName;
- (BOOL)isSelected;

@end
