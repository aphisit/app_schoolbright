//
//  TeachingSubjectReportMainViewController.m
//  JabjaiApp
//
//  Created by mac on 12/30/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "TeachingSubjectReportMainViewController.h"
#import "TeachingSubjectStudentTableViewController.h"
#import "TeachingSubjectStatisticViewController.h"
#import "TeachingSubjectViewController.h"
#import "SWRevealViewController.h"

@interface TeachingSubjectReportMainViewController () {
    UIColor *pagerOrangeColor;
    UIColor *pagerGreenColor;
    UIColor *indicatorOrangeColor;
    UIColor *indicatorGreenColor;
}

@property (nonatomic) CAPSPageMenu *pageMenu;
@property (strong, nonatomic) TeachingSubjectStudentTableViewController *studentListViewController;
@property (strong, nonatomic) TeachingSubjectStatisticViewController *staticticViewController;

@end

@implementation TeachingSubjectReportMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pagerOrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1];
    pagerGreenColor = [UIColor colorWithRed:73/255.0 green:160/255.0 blue:152/255.0 alpha:1];
    indicatorOrangeColor = [UIColor colorWithRed:254/255.0 green:180/255.0 blue:57/255.0 alpha:1];
    indicatorGreenColor = [UIColor colorWithRed:31/255.0 green:124/255.0 blue:114/255.0 alpha:1];
    
    if(self.sectionName != nil) {
        self.headerTitleLabel.text = [NSString stringWithFormat:@"%@ (%@)", self.subjectName, self.sectionName];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    [self setupPageMenu];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)moveBack:(id)sender {
    
    TeachingSubjectViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TeachingSubjectReportStoryboard"];
    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}

#pragma mark - Class functions


- (void)setupPageMenu {
    
    self.studentListViewController = [[TeachingSubjectStudentTableViewController alloc] init];
    self.studentListViewController.title = @"นักเรียน";
    self.studentListViewController.sectionID = self.sectionID;
    
    self.staticticViewController = [[TeachingSubjectStatisticViewController alloc] init];
    self.staticticViewController.title = @"สถิติ";
    
    NSArray *controllerArray = @[self.studentListViewController, self.staticticViewController];
    
    // Color background for each controller tab
    NSArray *colorBackground = @[pagerOrangeColor, pagerGreenColor];
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionMenuItemSeparatorWidth: @(4.3),
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                                 CAPSPageMenuOptionSelectionIndicatorColor: indicatorOrangeColor,
                                 CAPSPageMenuOptionMenuMargin: @(20),
                                 CAPSPageMenuOptionMenuHeight: @(40),
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor: [UIColor whiteColor],                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"ThaiSansNeue-SemiBold" size:22.0],
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                                 CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                                 CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                 CAPSPageMenuOptionColorBackgroundArray: colorBackground
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    //Optional delegate
    _pageMenu.delegate = self;
    
    [self.contentView addSubview:_pageMenu.view];
    
}

#pragma CAPSPageMenuDelegate

- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    NSLog(@"WillMoveToPage");
    
    if(index == 0) {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorOrangeColor;
    }
    else {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorGreenColor;
        
    }
    
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
    NSLog(@"DidMoveToPage");
    
    if(index == 0) {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorOrangeColor;
    }
    else {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorGreenColor;
        
    }
    
}


@end
