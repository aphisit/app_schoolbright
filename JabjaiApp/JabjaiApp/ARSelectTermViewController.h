//
//  ARSelectTermViewController.h
//  JabjaiApp
//
//  Created by toffee on 20/2/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARDetailAcademicResultsViewController.h"
#import "SlideMenuController.h"
#import "TableListDialog.h"
#import "CallGetYearOfAcademicResultsAPI.h"
#import "CallGetTermOfAcademicResultsAPI.h"
#import "CallGetPermissionAcademicResultsAPI.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
#import "AlertDialog.h"
NS_ASSUME_NONNULL_BEGIN

@interface ARSelectTermViewController : UIViewController<UITextFieldDelegate, SlideMenuControllerDelegate, TableListDialogDelegate, CallGetYearOfAcademicResultsAPIDelegate, CallGetTermOfAcademicResultsAPIDelegate, CallGetPermissionAcademicResultsAPIDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

@property (nonatomic, strong) NSString *termStr;
@property (nonatomic, strong) NSString *yearStr;
@property (nonatomic, strong) NSString *yearId;
@property (nonatomic, strong) NSArray *termArray;
@property (nonatomic, strong) NSMutableArray *yearNumberArray;
@property (nonatomic, strong) NSMutableArray *yearIdArray;


@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSchoolYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSemesterLabel;



@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITextField *yearTextField;
@property (weak, nonatomic) IBOutlet UITextField *termTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

- (IBAction)openDarwerAction:(id)sender;
- (IBAction)nextAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
