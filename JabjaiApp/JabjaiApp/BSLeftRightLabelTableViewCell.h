//
//  BSLeftRightLabelTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLeftRightLabelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *LeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *RightLabel;

@end
