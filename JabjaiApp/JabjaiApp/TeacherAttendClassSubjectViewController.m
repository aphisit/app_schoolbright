//
//  TeacherAttendClassSubjectViewController.m
//  JabjaiApp
//
//  Created by mac on 1/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TeacherAttendClassSubjectViewController.h"
#import "StudentManageSubjectHeaderViewCell.h"
#import "StudentManageSubjectChildViewCell.h"
#import "UTeachingYearModel.h"
#import "APIURL.h"
#import "Utils.h"
#import "DateUtility.h"
#import "SWRevealViewController.h"
#import "AttendClassReportViewController.h"
#import "Constant.h"

@interface TeacherAttendClassSubjectViewController () {
    
    NSMutableArray *sectionKeys;
    NSMutableArray *expandedSections;
    NSMutableDictionary *subjectInTableSection; // key (sectionKeys) -> NSArray [USubjectModel]
    
    NSMutableArray *teachingYearArray;
    
    NSNumber *selected_year;
    NSString *selected_semester;
    
    int selected_sectionID;
    NSString *selected_sectionName;
    NSString *selected_subjectName;
    
    NSInteger connectCounter;
}

@property (strong, nonatomic) TeachingSubjectDialog *teachingSubjectDialog;

@end

@implementation TeacherAttendClassSubjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    // register table nib
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([StudentManageSubjectHeaderViewCell class]) bundle:nil] forCellReuseIdentifier:@"HeaderCell"];
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([StudentManageSubjectChildViewCell class]) bundle:nil] forCellReuseIdentifier:@"ChildCell"];
    
    self.tblView.tableFooterView = [[UIView alloc] init];
    self.tblView.dataSource = self;
    self.tblView.delegate = self;
    
    // Initial table expandable variables
    sectionKeys = [[NSMutableArray alloc] init];
    expandedSections = [[NSMutableArray alloc] init];
    subjectInTableSection = [[NSMutableDictionary alloc] init];
    
    connectCounter = 0;
    
    [self getTeachingSubjectData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)showFilterDialog:(id)sender {
    
    if(self.teachingSubjectDialog != nil) {
        
        [self.teachingSubjectDialog showDialogInView:self.view title:@"Filter"];
        
    }
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return subjectInTableSection.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([expandedSections containsObject:@(section)]) {
        
        NSString *key = [sectionKeys objectAtIndex:section];
        USubjectModel *subjectModel = [subjectInTableSection objectForKey:key];
        
        return subjectModel.sections.count + 1; // Plus 1 add header of each section
    }
    else {
        return 1; // If section is not expaned show only header
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *key = [sectionKeys objectAtIndex:indexPath.section];
    
    if(indexPath.row == 0) { // For the 1st row of each section show header
        
        StudentManageSubjectHeaderViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell" forIndexPath:indexPath];
        
        BOOL isExpanded = [expandedSections containsObject:@(indexPath.section)];
        
        if(isExpanded) {
            cell.arrowImage.image = [UIImage imageNamed:@"ic_sort_up"];
        }
        else {
            cell.arrowImage.image = [UIImage imageNamed:@"ic_sort_down"];
        }
        
        USubjectModel *subjectModel = [subjectInTableSection objectForKey:key];
        
        cell.titleLabel.text = subjectModel.subjectName;
        
        return cell;
    }
    else {
        
        StudentManageSubjectChildViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChildCell" forIndexPath:indexPath];
        
        USubjectModel *subjectModel = [subjectInTableSection objectForKey:key];
        NSArray *sections = subjectModel.sections;
        USectionModel *sectionModel = [sections objectAtIndex:indexPath.row - 1];
        
        cell.titleLabel.text = sectionModel.sectionName;
        [cell.actionImage removeFromSuperview];
        
        return cell;
    }
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([expandedSections containsObject:@(indexPath.section)]) {
        
        if(indexPath.row == 0) { // when select header of each section, then toggle up and down
            [expandedSections removeObject:@(indexPath.section)];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
        else {
            
            NSString *key = [sectionKeys objectAtIndex:indexPath.section];
            USubjectModel *subjectModel = [subjectInTableSection objectForKey:key];
            NSArray *sections = subjectModel.sections;
            USectionModel *sectionModel = [sections objectAtIndex:indexPath.row - 1];
            
            NSIndexPath *headerIndexPath = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
            StudentManageSubjectHeaderViewCell *cell = [tableView cellForRowAtIndexPath:headerIndexPath];
            
            selected_sectionID = sectionModel.sectionID;
            selected_sectionName = sectionModel.sectionName;
            selected_subjectName = cell.titleLabel.text;
            
            NSString *subjectTitle = [NSString stringWithFormat:@"%@ (SEC %@)", selected_subjectName, selected_sectionName];
            
            AttendClassReportViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AttendClassReportStoryboard"];
            viewController.sectionID = selected_sectionID;
            viewController.subjectName = subjectTitle;
            viewController.teachingYearArray = teachingYearArray;
            
            [self.revealViewController pushFrontViewController:viewController animated:YES];
            
        }
    }
    else {
        [expandedSections addObject:@(indexPath.section)];
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0) {
        return 60;
    }
    else {
        return 44;
    }
}

#pragma mark - GetAPIData

- (void)getTeachingSubjectData {
    NSString *URLString = [APIURL getTeachingSubjectUrl];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getTeachingSubjectData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getTeachingSubjectData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getTeachingSubjectData];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                teachingYearArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    
                    NSDictionary *yearDataDict = [returnedArray objectAtIndex:i];
                    
                    int yearID = [[yearDataDict objectForKey:@"YearId"] intValue];
                    NSMutableString *yearName = [[NSMutableString alloc] initWithFormat:@"%@", [yearDataDict objectForKey:@"Year"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) yearName);
                    
                    NSArray *termArray = [yearDataDict objectForKey:@"lTerm"];
                    
                    NSMutableArray<USemesterModel *> *semesters = [[NSMutableArray alloc] init];
                    
                    for(int j=0; j<termArray.count; j++) {
                        
                        NSDictionary *termDataDict = [termArray objectAtIndex:j];
                        
                        NSMutableString *semesterID = [[NSMutableString alloc] initWithFormat:@"%@", [termDataDict objectForKey:@"TermId"]];
                        NSMutableString *semesterName = [[NSMutableString alloc] initWithFormat:@"%@", [termDataDict objectForKey:@"Term"]];
                        NSMutableString *semesterStartDateXML = [[NSMutableString alloc] initWithFormat:@"%@", [termDataDict objectForKey:@"dStart"]];
                        NSMutableString *semesterEndDateXML = [[NSMutableString alloc] initWithFormat:@"%@", [termDataDict objectForKey:@"dEnd"]];
                        
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) semesterID);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) semesterName);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) semesterStartDateXML);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) semesterEndDateXML);
                        
                        NSDate *startDate = [Utils parseServerDateStringToDate:semesterStartDateXML];
                        NSDate *endDate = [Utils parseServerDateStringToDate:semesterEndDateXML];
                        
                        NSArray *subjectArray = [termDataDict objectForKey:@"lPlane"];
                        
                        NSMutableArray<USubjectModel *> *subjects = [[NSMutableArray alloc] init];
                        
                        for(int k=0; k<subjectArray.count; k++) {
                            
                            NSDictionary *subjectDataDict = [subjectArray objectAtIndex:k];
                            
                            NSMutableString *subjectID = [[NSMutableString alloc] initWithFormat:@"%@", [subjectDataDict objectForKey:@"PlaneId"]];
                            NSMutableString *subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [subjectDataDict objectForKey:@"Plane"]];
                            
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectID);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                            
                            NSArray *sectionArray = [subjectDataDict objectForKey:@"lSection"];
                            
                            NSMutableArray<USectionModel *> *sections = [[NSMutableArray alloc] init];
                            
                            for(int l=0; l<sectionArray.count; l++) {
                                NSDictionary *sectionDataDict = [sectionArray objectAtIndex:l];
                                
                                int sectionID = [[sectionDataDict objectForKey:@"SectionId"] intValue];
                                NSMutableString *sectionName = [[NSMutableString alloc] initWithFormat:@"%@", [sectionDataDict objectForKey:@"Section"]];
                                
                                CFStringTrimWhitespace((__bridge CFMutableStringRef) sectionName);
                                
                                USectionModel *sectionModel = [[USectionModel alloc] init];
                                sectionModel.sectionID = sectionID;
                                sectionModel.sectionName = sectionName;
                                
                                [sections addObject:sectionModel];
                            }
                            
                            USubjectModel *subjectModel = [[USubjectModel alloc] init];
                            subjectModel.subjectID = subjectID;
                            subjectModel.subjectName = subjectName;
                            subjectModel.sections = sections;
                            
                            [subjects addObject:subjectModel];
                            
                        }
                        
                        USemesterModel *semesterModel = [[USemesterModel alloc] init];
                        semesterModel.semesterID = semesterID;
                        semesterModel.semesterName = semesterName;
                        semesterModel.subjects = subjects;
                        semesterModel.startDate = startDate;
                        semesterModel.endDate = endDate;
                        
                        [semesters addObject:semesterModel];
                        
                    }
                    
                    UTeachingYearModel *teachingYearModel = [[UTeachingYearModel alloc] init];
                    teachingYearModel.yearID = yearID;
                    teachingYearModel.yearName = yearName;
                    teachingYearModel.semesters = semesters;
                    
                    [teachingYearArray addObject:teachingYearModel];
                    
                }
                
                //Find max year
                
                for(UTeachingYearModel *model in teachingYearArray) {
                    NSNumber *yearNumber = [[NSNumber alloc] initWithInt:[model.yearName intValue]];
                    
                    if(selected_year == nil || yearNumber > selected_year) {
                        selected_year = yearNumber;
                    }
                    
                }
                
                [self performTeachingYearData];
                
                if(self.teachingSubjectDialog != nil && [self.teachingSubjectDialog isDialogShowing]) {
                    [self.teachingSubjectDialog dismissDialog];
                    self.teachingSubjectDialog = nil;
                }
                
                self.teachingSubjectDialog = [[TeachingSubjectDialog alloc] initWithUTeachingYearModelArray:teachingYearArray];
                self.teachingSubjectDialog.delegate = self;
                [self.teachingSubjectDialog showDialogInView:self.view title:@"Filter"];
                
            }

        }

    }];
}

#pragma mark - Manage Data

- (void) performTeachingYearData {
    
    if(teachingYearArray == nil) {
        return;
    }
    
    if(sectionKeys != nil) {
        [sectionKeys removeAllObjects];
    }
    
    if(expandedSections != nil) {
        [expandedSections removeAllObjects];
    }
    
    if(subjectInTableSection != nil) {
        [subjectInTableSection removeAllObjects];
    }
    
    sectionKeys = [[NSMutableArray alloc] init];
    expandedSections = [[NSMutableArray alloc] init];
    subjectInTableSection = [[NSMutableDictionary alloc] init];
    
    for(UTeachingYearModel *teachingYearModel in teachingYearArray) {
        
        if(selected_year != nil) {
            
            int year = [teachingYearModel.yearName intValue];
            
            if(year == selected_year.intValue) {
                
                if(selected_semester == nil || [selected_semester isEqualToString:@"ทั้งหมด"]) {
                    
                    NSArray *semesters = teachingYearModel.semesters;
                    
                    for(USemesterModel *semesterModel in semesters) {
                        NSArray *subjects = semesterModel.subjects;
                        
                        for(USubjectModel *subjectModel in subjects) {
                            [subjectInTableSection setObject:subjectModel forKey:subjectModel.subjectID];
                            [sectionKeys addObject:subjectModel.subjectID];
                        }
                        
                    }
                    
                }
                else {
                    
                    NSArray *semesters = teachingYearModel.semesters;
                    
                    for(USemesterModel *semesterModel in semesters) {
                        
                        if([selected_semester isEqualToString:semesterModel.semesterName]) {
                            
                            NSArray *subjects = semesterModel.subjects;
                            
                            for(USubjectModel *subjectModel in subjects) {
                                [subjectInTableSection setObject:subjectModel forKey:subjectModel.subjectID];
                                [sectionKeys addObject:subjectModel.subjectID];
                            }
                            
                            break;
                        }
                        
                    }
                    
                }
                
                break;
                
            }
            
        }
        
    }
    
    [self.tblView reloadData];
}

#pragma mark - TeachingSubjectDialogDelegate

- (void)applyFilter:(NSNumber *)schoolYear semester:(NSString *)semester {
    
    selected_year = schoolYear;
    selected_semester = semester;
    
    [self performTeachingYearData];
}

@end
