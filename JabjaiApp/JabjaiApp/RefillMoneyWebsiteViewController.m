//
//  RefillMoneyWebsiteViewController.m
//  JabjaiApp
//
//  Created by mac on 11/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "RefillMoneyWebsiteViewController.h"
#import "SWRevealViewController.h"
#import "RefillMoneyViewController.h"
#import "UserData.h"
#import "UserInfoViewController.h"
#import "Utils.h"
#import "APIURL.h"


@interface RefillMoneyWebsiteViewController (){
    RFSaveBillTopUpDialog *rfSaveBillTopUpDialog;
}

@end

@implementation RefillMoneyWebsiteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_TOPUPMONEY",nil,[Utils getLanguage],nil);
    self.webView.delegate = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[APIURL getTopUpMonney:[UserData getUserID] amount:self.numberMoney schoolid:[UserData getSchoolId]]]];
    [self.webView loadRequest:request];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}

- (void) onAlertDialogClose{
    RefillMoneyViewController *viewController = [self.storyboard  instantiateViewControllerWithIdentifier:@"RefillMoneyStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self.indicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.indicator stopAnimating];
    self.indicator.hidesWhenStopped = YES;
    if ([self.webView.request.URL.description isEqualToString:[APIURL getMoveBackTopUp]]) {
        UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
}

- (IBAction)moveBack:(id)sender {
    RefillMoneyViewController *viewController = [self.storyboard  instantiateViewControllerWithIdentifier:@"RefillMoneyStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}


@end
