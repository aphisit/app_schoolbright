//
//  ReportLeaveDetailViewControllerXIB.m
//  JabjaiApp
//
//  Created by Mac on 24/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "ReportLeaveDetailViewControllerXIB.h"
#import "NoticeReportDateMessageDetailModel.h"
#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"

#import "NoticeReportDateViewController.h"
#import "NoticeReportDateMessageViewController.h"
#import "ReportLeaveViewController.h"

#import "MessageInboxScrollableTextDialogCollectionViewCell.h"

#import "AlertDialog.h"

#import <QBImagePickerController/QBImagePickerController.h>
#import "AppDelegate.h"
#import "ZMImageSliderViewController.h"
#import "NoticeReportDateMessageViewController.h"
#import "ERProgressHud.h"
#import "KxMenu.h"
#import "EZYGradientView.h"
@interface ReportLeaveDetailViewControllerXIB (){
    NoticeReportDateMessageDetailModel *noticeReportDateMessageDetailModel;
    NSMutableArray<NoticeReportDateMessageDetailModel *> *leaveReportArray;
    NSInteger connectCounter;
    NSArray *returnedArray;
    NSMutableArray *imageArray;
    NSString *address, *tumbon, *aumphur, *province, *phone;
    NSString *headtumbon, *headaumphur, *headprovince, *headphone;
    NSString *urlFile;
    EZYGradientView *acceptGradient, *declineGradient, *waitGradient, *cancelGradient;
    UIColor *jungleGreen, *downy, *salmon, *macaroniAndCheese, *mariner, *malibu, *heliotRope ,*melrose;
    UIColor *bitterSweet;
    CGFloat screenWidth;
    CancelLeaveDialog *cancelLeaveDialog;
    AlertDialog *alertDialog;
    NSDate *dateNow, *dateApprove;
    NSInteger imgConstant;
}
@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@end

@implementation ReportLeaveDetailViewControllerXIB

- (void)viewDidLoad {
    [super viewDidLoad];
    imgConstant = self.heightCollectionViewConstraint.constant;
    // Do any additional setup after loading the view from its nib.
}

- (void)showDetailLeaveViewController:(UIView *)targetView letterID:(long long)letterID{
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
        [targetView addSubview:self.view];
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height-20)];
        [targetView addSubview:self.view];
    }
    self.isShowing = YES;
    
    [self setLangage];
//    self.detailLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    self.detailLabel.numberOfLines = 0;
    self.letterID = letterID;
    self.addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.addressLabel.numberOfLines = 0;
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    
    self.timeFormatter = [Utils getDateFormatter];
    
    
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [self.timeFormatter setDateFormat:@"HH:mm"];
    }
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        self.heightContentConstraint.constant = 800;
//        self.heightMenuConstraint.constant = 120;
//    }
//    else{
//        self.heightContentConstraint.constant = 642;
//        self.heightMenuConstraint.constant = 70;
//    }
    imageArray = [[NSMutableArray alloc] init];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.moreButton addTarget:self action:@selector(showRightMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    screenWidth = [[UIScreen mainScreen] bounds].size.width;
//    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
//        
//        if (screenWidth > 320.0f) {
//            self.heightStatusConstraint.constant = 70;
//
//        }
//        else{
//            self.heightStatusConstraint.constant = 60;
//
//        }
//        
//    }
//    else{
//        self.heightStatusConstraint.constant = 120;
//
//    }
    
    // Color
    jungleGreen = [UIColor colorWithRed:0.18 green:0.76 blue:0.47 alpha:1.0]; //Accept
    downy = [UIColor colorWithRed:0.41 green:0.83 blue:0.63 alpha:1.0];
    salmon = [UIColor colorWithRed:0.99 green:0.56 blue:0.44 alpha:1.0]; // Decline
    macaroniAndCheese = [UIColor colorWithRed:0.99 green:0.76 blue:0.52 alpha:1.0];
    mariner = [UIColor colorWithRed:0.16 green:0.49 blue:0.75 alpha:1.0];  // Wait
    malibu = [UIColor colorWithRed:0.31 green:0.69 blue:0.98 alpha:1.0];
    heliotRope = [UIColor colorWithRed:0.56 green:0.40 blue:0.98 alpha:1.0]; // Cancel
    melrose = [UIColor colorWithRed:0.69 green:0.57 blue:0.99 alpha:1.0];
    
    bitterSweet = [UIColor colorWithRed:0.99 green:0.48 blue:0.40 alpha:1.0];
    
    EZYGradientView *gradientView = [[EZYGradientView alloc] init];
    gradientView.frame = self.view.frame;
    gradientView.firstColor = bitterSweet;
    gradientView.secondColor = macaroniAndCheese;
    gradientView.angleº = 90;
    gradientView.colorRatio = 0.5;
    gradientView.fadeIntensity = 1;
    gradientView.isBlur = YES;
    gradientView.blurOpacity = 0.5;
    [self.headMenuBar insertSubview:gradientView atIndex:0];
    
    [self clearDisplay];
    [self getLetterDetailWithLetterID:self.letterID];
    
    
}
-(void)removeDetailLeaveViewController {
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDetailLeaveViewController{
    [self removeDetailLeaveViewController];
}
- (BOOL)isDialogShowing{
    return self.isShowing;
}

- (void)viewDidLayoutSubviews{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

- (void) setLangage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_LEAVE",nil,[Utils getLanguage],nil);
    self.headerSenderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_SENDER",nil,[Utils getLanguage],nil);
    
    self.headerPrositionLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_POSITION",nil,[Utils getLanguage],nil);
    
    self.headerMeanLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_MEAN",nil,[Utils getLanguage],nil);
    self.headerStartDayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_START",nil,[Utils getLanguage],nil);
    self.headerEndDayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_END",nil,[Utils getLanguage],nil);
    self.headerApproveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_DAY_APPROVE",nil,[Utils getLanguage],nil);
    self.headerDetailLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_DETAIL",nil,[Utils getLanguage],nil);
    self.headerAddressLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_PLACE",nil,[Utils getLanguage],nil);
    self.headerImageLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_IMAGE",nil,[Utils getLanguage],nil);
}

- (void)showRightMenu:(UIButton *)sender{
    
    UIImage *unreadImage = [UIImage imageNamed:@"sort_with_unread"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [unreadImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *unreadImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSArray *menuItems =
    @[
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_CANCEL",nil,[Utils getLanguage],nil)
                     image:unreadImage2
                    target:self
                    action:@selector(cancelLeave:)],
      ];
    
    const CGFloat W = self.view.bounds.size.width;
    const CGFloat H = self.view.bounds.size.height;
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(W - 16, 18, 40, 40) menuItems:menuItems];
}

- (void)cancelLeave:(id)sender
{
    [self showCancelConfirm];
}

- (void)showCancelConfirm{
    NSString *detail = NSLocalizedStringFromTableInBundle(@"DIALOG_LEAVE_PRI_WANT_CANCEL_APPROVA",nil,[Utils getLanguage],nil);
    if(cancelLeaveDialog == nil) {
        cancelLeaveDialog = [[CancelLeaveDialog alloc] init];
        cancelLeaveDialog.delegate = self;
    }
    else if(cancelLeaveDialog != nil && [cancelLeaveDialog isDialogShowing]) {
        [cancelLeaveDialog dismissDialog];
    }
    [cancelLeaveDialog showDialogInView:self.view message:detail];
}

- (void)onCancelConfirmDialog:(CancelLeaveDialog *)cancelconfirmDialog confirm:(BOOL)confirm{
    if (confirm) {
        [self getCancelLeaveWithLetterID:self.letterID];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getLetterDetailWithLetterID:(long long)letterID{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getLetterLeaveDetailWithLetterID:letterID userID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    NSLog(@"%@", URLString);
    
    [[ERProgressHud sharedInstance] showDarkBackgroundViewWithTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LEAVE_PRI_PLEASE_WAIT",nil,[Utils getLanguage],nil)];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if( self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@( self->connectCounter) stringValue]);
                [self getLetterDetailWithLetterID:letterID];
            }
            else {
                self->connectCounter = 0;
            }
            
        }
        else {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[ERProgressHud sharedInstance] hide];
            });
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if( self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@( self->connectCounter) stringValue]);
                    [self getLetterDetailWithLetterID:letterID];
                }
                else {
                    self->connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if( self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@( self->connectCounter) stringValue]);
                    [self getLetterDetailWithLetterID:letterID];
                }
                else {
                    self->connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                self->connectCounter = 0;
                
                if ( self->leaveReportArray != nil) {
                    [ self->leaveReportArray removeAllObjects];
                    self->leaveReportArray = nil;
                }
                
                self->leaveReportArray = [[NSMutableArray alloc] init];
                
                self->imageArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *leaveName, *leavePosition, *leaveCause, *leaveStart, *leaveEnd, *leaveDetail, *leaveApprove;
                    
                    //leave name
                    if (![[dataDict objectForKey:@"senderName"] isKindOfClass:[NSNull class]]) {
                        leaveName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderName"]];
                    }
                    else{
                        leaveName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave position
                    if (![[dataDict objectForKey:@"senderJob"] isKindOfClass:[NSNull class]]) {
                        leavePosition = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderJob"]];
                    }
                    else{
                        leavePosition = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave cause
                    if (![[dataDict objectForKey:@"letterType"] isKindOfClass:[NSNull class]]) {
                        leaveCause = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"letterType"]];
                    }
                    else{
                        leaveCause = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave start
                    if (![[dataDict objectForKey:@"leaveStart"] isKindOfClass:[NSNull class]]) {
                        leaveStart = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"leaveStart"]];
                    }
                    else{
                        leaveStart = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave end
                    if (![[dataDict objectForKey:@"leaveEnd"] isKindOfClass:[NSNull class]]) {
                        leaveEnd = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"leaveEnd"]];
                    }
                    else{
                        leaveEnd = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave approve
                    if (![[dataDict objectForKey:@"sendApproveDate"] isKindOfClass:[NSNull class]]) {
                        leaveApprove = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sendApproveDate"]];
                    }
                    else{
                        leaveApprove = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave detail
                    if (![[dataDict objectForKey:@"letterDescription"] isKindOfClass:[NSNull class]]) {
                        leaveDetail = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"letterDescription"]];
                    }
                    else{
                        leaveDetail = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    int status = [[dataDict objectForKey:@"status"] intValue];
                    
                    NSMutableString *season;
                    
                    if (![[dataDict objectForKey:@"Season"] isKindOfClass:[NSNull class]]) {
                        season = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Season"]];
                    }
                    else{
                        season = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *address, *tumbon, *aumphur, *province, *phone;
                    
                    if (![[dataDict objectForKey:@"Aumpher"] isKindOfClass:[NSNull class]]) {
                        aumphur = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Aumpher"]];
                    }
                    else{
                        aumphur = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Tumbon"] isKindOfClass:[NSNull class]]) {
                        tumbon = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Tumbon"]];
                    }
                    else{
                        tumbon = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"HomeNumber"] isKindOfClass:[NSNull class]]) {
                        address = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"HomeNumber"]];
                    }
                    else{
                        address = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Province"] isKindOfClass:[NSNull class]]) {
                        province = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Province"]];
                    }
                    else{
                        province = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Phone"] isKindOfClass:[NSNull class]]) {
                        phone = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Phone"]];
                    }
                    else{
                        phone = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    int pageStatus = [[dataDict objectForKey:@"pageStatus"] intValue];
                    
                    NSMutableArray *fileImage;
                    fileImage = [dataDict objectForKey:@"file"];
                    NSLog(@"%@", fileImage);
                    
                    for (i=0; i < fileImage.count; i++) {
                        NSString *picture = fileImage[i];
                        NSLog(@"%@", picture);
                        self->urlFile = picture;
                        //                        urlFile = picture;
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leavePosition);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveCause);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveStart);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveEnd);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveDetail);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveApprove);
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) address);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) tumbon);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) aumphur);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) province);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) phone);
                    
                    NSDate *leaveStartDate;
                    leaveStartDate = [formatter2 dateFromString:leaveStart];
                    
                    NSDate *leaveEndDate;
                    leaveEndDate = [formatter2 dateFromString:leaveEnd];
                    
                    NSRange dotRange = [leaveApprove rangeOfString:@"."];
                    
                    NSDate *leaveApproveDate;
                    
                    if(dotRange.length != 0) {
                        leaveApproveDate = [formatter dateFromString:leaveApprove];
                    }
                    else {
                        leaveApproveDate = [formatter2 dateFromString:leaveApprove];
                    }
                    
                    NoticeReportDateMessageDetailModel *model = [[NoticeReportDateMessageDetailModel alloc] init];
                    
                    model.leaveName = leaveName;
                    model.leavePosition = leavePosition;
                    model.leaveCause = leaveCause;
                    model.leaveStart = leaveStartDate;
                    model.leaveEnd = leaveEndDate;
                    model.leaveDetail = leaveDetail;
                    
                    model.file = fileImage;
                    
                    model.status = status;
                    model.pageStatus = pageStatus;
                    
                    model.address = address;
                    model.tumbon = tumbon;
                    model.aumphur = aumphur;
                    model.province = province;
                    model.phone = phone;
                    
                    model.season = season;
                    
                    model.leaveDateApprove = leaveApproveDate;
                    
                    self->noticeReportDateMessageDetailModel = model;
                    [self performData];
                    
                    [ self->leaveReportArray addObject:model];
                    
                    [self.collectionView reloadData];
                    
                }
                
            }
        }
        
    }];
    
}

- (void)performData{
    
    if (noticeReportDateMessageDetailModel != nil) {
        self.leaveNameLabel.text = noticeReportDateMessageDetailModel.leaveName;
        self.positionLabel.text = noticeReportDateMessageDetailModel.leavePosition;
        self.causeLabel.text = noticeReportDateMessageDetailModel.leaveCause;
        
        self.startDateLabel.text = [Utils getThaiDateAcronymFormatWithDate:noticeReportDateMessageDetailModel.leaveStart];
        
        self.detailTextView.text = noticeReportDateMessageDetailModel.leaveDetail;
        
        // Accept Gradient
        acceptGradient = [[EZYGradientView alloc] init];
        acceptGradient.frame = CGRectMake(0, 0, self.statusBackGround.frame.size.width, self.statusBackGround.frame.size.height);
        acceptGradient.firstColor = jungleGreen;
        acceptGradient.secondColor = downy;
        acceptGradient.angleº = 90;
        acceptGradient.colorRatio = 0.5;
        acceptGradient.fadeIntensity = 1;
        acceptGradient.isBlur = YES;
        acceptGradient.blurOpacity = 0.5;
        
        // Decline Gradient
        declineGradient = [[EZYGradientView alloc] init];
        declineGradient.frame = CGRectMake(0, 0, self.statusBackGround.frame.size.width, self.statusBackGround.frame.size.height);
        declineGradient.firstColor = salmon;
        declineGradient.secondColor = macaroniAndCheese;
        declineGradient.angleº = 90;
        declineGradient.colorRatio = 0.5;
        declineGradient.fadeIntensity = 1;
        declineGradient.isBlur = YES;
        declineGradient.blurOpacity = 0.5;
        
        // Wait Gradient
        waitGradient = [[EZYGradientView alloc] init];
        waitGradient.frame = CGRectMake(0, 0, self.statusBackGround.frame.size.width, self.statusBackGround.frame.size.height);
        waitGradient.firstColor = mariner;
        waitGradient.secondColor = malibu;
        waitGradient.angleº = 90;
        waitGradient.colorRatio = 0.5;
        waitGradient.fadeIntensity = 1;
        waitGradient.isBlur = YES;
        waitGradient.blurOpacity = 0.5;
        
        
        // Cancel Gradient
        cancelGradient = [[EZYGradientView alloc] init];
        cancelGradient.frame = CGRectMake(0, 0, self.statusBackGround.frame.size.width, self.statusBackGround.frame.size.height);
        cancelGradient.firstColor = heliotRope;
        cancelGradient.secondColor = melrose;
        cancelGradient.angleº = 90;
        cancelGradient.colorRatio = 0.5;
        cancelGradient.fadeIntensity = 1;
        cancelGradient.isBlur = YES;
        cancelGradient.blurOpacity = 0.5;
        
        dateApprove = noticeReportDateMessageDetailModel.leaveDateApprove;
        dateNow = [NSDate date];
        
        if (noticeReportDateMessageDetailModel.status == 1) {
            if (noticeReportDateMessageDetailModel.pageStatus == 12) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_WAIT_CONFIRM_CANCEL",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
                self.moreButton.hidden = YES;
                self.moreButton.enabled = false;
            }
            else if (noticeReportDateMessageDetailModel.pageStatus == 9) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_CANCEL_REQUEST",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
                self.moreButton.hidden = YES;
                self.moreButton.enabled = false;
            }
            else{
                if (noticeReportDateMessageDetailModel.pageStatus == 3) {
                    self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_APPROVE",nil,[Utils getLanguage],nil);
                    [self.statusBackGround setBackgroundColor:jungleGreen];
                }
                else if (noticeReportDateMessageDetailModel.pageStatus == 4){
                    self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_NOT_ALLOW",nil,[Utils getLanguage],nil);
                    [self.statusBackGround setBackgroundColor:salmon];
                }
                else{
                    self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_WAIT_APPROVE",nil,[Utils getLanguage],nil);
                    [self.statusBackGround setBackgroundColor:mariner];
                }
            }
        }
        else if (noticeReportDateMessageDetailModel.status == 2) {
            if (noticeReportDateMessageDetailModel.pageStatus == 5 ||noticeReportDateMessageDetailModel.pageStatus == 6 || noticeReportDateMessageDetailModel.pageStatus == 7 || noticeReportDateMessageDetailModel.pageStatus == 8) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_WAIT_CONFIRM_CANCEL",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
                self.moreButton.hidden = YES;
                self.moreButton.enabled = false;
            }
            else if (noticeReportDateMessageDetailModel.pageStatus == 12) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_WAIT_CONFIRM_CANCEL",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
                self.moreButton.hidden = YES;
                self.moreButton.enabled = false;
            }
            else if (noticeReportDateMessageDetailModel.pageStatus == 9) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_CANCEL_REQUEST",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
                self.moreButton.hidden = YES;
                self.moreButton.enabled = false;
            }
            else{
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_APPROVE",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:jungleGreen];
            }
        }

        else if (noticeReportDateMessageDetailModel.status == 3) {
            
            if (noticeReportDateMessageDetailModel.pageStatus == 5 || noticeReportDateMessageDetailModel.pageStatus == 6 || noticeReportDateMessageDetailModel.pageStatus == 7 || noticeReportDateMessageDetailModel.pageStatus == 8) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_WAIT_CONFIRM_CANCEL",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
                self.moreButton.hidden = YES;
                self.moreButton.enabled = false;
            }
            else if (noticeReportDateMessageDetailModel.pageStatus == 9) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_CANCEL_REQUEST",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
                self.moreButton.hidden = YES;
                self.moreButton.enabled = false;
            }
            else if (noticeReportDateMessageDetailModel.pageStatus == 12) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_WAIT_CONFIRM_CANCEL",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
                self.moreButton.hidden = YES;
                self.moreButton.enabled = false;
            }
            else{
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_NOT_ALLOW",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:salmon];
//                self.moreButton.hidden = YES;
//                self.moreButton.enabled = false;
            }

        }
        if (noticeReportDateMessageDetailModel.status == 4) {
            
            if (noticeReportDateMessageDetailModel.pageStatus == 6 || noticeReportDateMessageDetailModel.pageStatus == 7 || noticeReportDateMessageDetailModel.pageStatus == 8) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_WAIT_CONFIRM_CANCEL",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
            }
            else if (noticeReportDateMessageDetailModel.pageStatus == 12) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_WAIT_CONFIRM_CANCEL",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
                self.moreButton.hidden = YES;
                self.moreButton.enabled = false;
            }
            else if (noticeReportDateMessageDetailModel.pageStatus == 9) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_CANCEL_REQUEST",nil,[Utils getLanguage],nil);
                [self.statusBackGround setBackgroundColor:heliotRope];
            }
            else{
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_CANCEL_REQUEST",nil,[Utils getLanguage],nil);
            }
            
            [self.statusBackGround setBackgroundColor:heliotRope];
            
            self.moreButton.hidden = YES;
            self.moreButton.enabled = false;
            
        }

        address = noticeReportDateMessageDetailModel.address;
        tumbon = noticeReportDateMessageDetailModel.tumbon;
        aumphur = noticeReportDateMessageDetailModel.aumphur;
        province = noticeReportDateMessageDetailModel.province;
        phone = noticeReportDateMessageDetailModel.phone;
        
        if ([address isEqual:@""] || [tumbon isEqual:@""] || [aumphur isEqual:@""] || [province isEqual:@""] || [phone isEqual:@""]) {
            self.addressLabel.text = @"-";
        }
        else{
            
            headtumbon = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_DISTRICT",nil,[Utils getLanguage],nil);
            headaumphur = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_AMPHUR",nil,[Utils getLanguage],nil);
            headprovince = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_PROVINCE",nil,[Utils getLanguage],nil);
            headphone = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_PHONENUMBER",nil,[Utils getLanguage],nil);
            self.addressLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@" , address, headtumbon, tumbon, headaumphur, aumphur, headprovince, province, headphone, phone];
            
        }
        
        if ([noticeReportDateMessageDetailModel.season isEqual:@""]) {
            //self.headChangeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_END",nil,[Utils getLanguage],nil);
            self.endDateLabel.text = [Utils getThaiDateAcronymFormatWithDate:noticeReportDateMessageDetailModel.leaveEnd];
        }
        else{
            if ([noticeReportDateMessageDetailModel.season isEqual:@"0"]) {
                //self.headChangeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_TIME_PERIOD",nil,[Utils getLanguage],nil);
                self.endDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_HALF_MORNING",nil,[Utils getLanguage],nil);
            }
            else if ([noticeReportDateMessageDetailModel.season isEqual:@"1"]) {
                //self.headChangeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_TIME_PERIOD",nil,[Utils getLanguage],nil);
                self.endDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_HALF_AFTERNOON",nil,[Utils getLanguage],nil);
            }
            else if ([noticeReportDateMessageDetailModel.season isEqual:@"-1"]) {
                //self.headChangeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_END",nil,[Utils getLanguage],nil);
                self.endDateLabel.text = [Utils getThaiDateAcronymFormatWithDate:noticeReportDateMessageDetailModel.leaveEnd];
            }
            else {
                self.endDateLabel.text = [Utils getThaiDateAcronymFormatWithDate:noticeReportDateMessageDetailModel.leaveEnd];
            }
        }
        
        if (noticeReportDateMessageDetailModel.leaveDateApprove == nil) {
            self.approveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_WAIT_ALLOW",nil,[Utils getLanguage],nil);
        }
        else{
            self.approveLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateAcronymFormatWithDate:noticeReportDateMessageDetailModel.leaveDateApprove] , [self.timeFormatter stringFromDate:noticeReportDateMessageDetailModel.leaveDateApprove]];
        }
        imageArray = noticeReportDateMessageDetailModel.file;
        
        float wCollection = self.collectionView.frame.size.width / ((self.collectionView.frame.size.width/4)-10);
        float amountRow = imageArray.count / (int)wCollection;
        NSLog(@"num = %d",(int)wCollection);
        NSInteger modImg = imageArray.count % (int)wCollection ;
        if (modImg > 0) {
            amountRow ++;
        }
        if (imageArray.count > 0) {
            self.heightCollectionViewConstraint.constant = self.heightCollectionViewConstraint.constant * amountRow+((amountRow*5)*2);//set hight image collectionview
            self.collectionView.hidden = NO;
            
        }else{
            self.heightCollectionViewConstraint.constant = imgConstant;
            self.collectionView.hidden = YES;
        }
        
    }
    else{
        [self clearDisplay];
        NSString *title = @"";
        NSString *message = NSLocalizedStringFromTableInBundle(@"DIALOG_LEAVE_PRI_CAN_NOT_LOAD_DATA",nil,[Utils getLanguage],nil);
        [self showFinishAlertDialogWithTitle:title message:message];
    }
    
}

- (void)clearDisplay {
    
    self.leaveNameLabel.text = @"-";
    self.positionLabel.text = @"-";
    self.causeLabel.text = @"-";
    self.startDateLabel.text = @"-";
    self.endDateLabel.text = @"-";
    self.approveLabel.text = @"-";
    self.detailTextView.text = @"-";
    self.addressLabel.text = @"-";
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MessageInboxScrollableTextDialogCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSData *imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [imageArray objectAtIndex:indexPath.row]]];

    cell.imageNews.tag = indexPath.row;
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageWithData:imageData];
    
    [cell.imageNews setBackgroundImage:imageView.image forState:UIControlStateNormal];
    [cell.imageNews addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:   (UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGSize defaultSize;
    defaultSize = CGSizeMake((self.collectionView.frame.size.width/4)-10, (imgConstant));
    return defaultSize;
}

- (void)okButtonTapped:(UIButton *)sender {
    
    NSLog(@"index = %d",sender.tag);
    ZMImageSliderViewController*controller = [[ZMImageSliderViewController alloc] initWithOptions:sender.tag imageUrls:imageArray];
    
    //    ZMImageSliderViewController*controller = [[ZMImageSliderViewController alloc] initWithOptions:sender.tag imageUrls:noticeReportDateMessageDetailModel.file];
    
    [self presentViewController:controller animated:YES completion:nil];
}

// Noti Cancel
- (void)getCancelLeaveWithLetterID:(long long)letterId {
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getCancelLeaveWithLetterID:letterId UserID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    //    [self showIndicator];
    
    [[ERProgressHud sharedInstance] showDarkBackgroundViewWithTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LEAVE_PRI_PLEASE_WAIT",nil,[Utils getLanguage],nil)];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getCancelLeaveWithLetterID:letterId];
            }
            else {
                connectCounter = 0;
            }
            
        }
        
        else{
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[ERProgressHud sharedInstance] hide];
            });
            NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            if ([str isEqualToString:@"\"SUCCESS\""]) {
//                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_CANCEL_REQUEST",nil,[Utils getLanguage],nil);
//                [self.statusBackGround setBackgroundColor:self->heliotRope];
//                self.approveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_WAIT_APPROVE",nil,[Utils getLanguage],nil);
//                self.moreButton.hidden = YES;
//                self.moreButton.enabled = false;
            [self getLetterDetailWithLetterID:self.letterID];
            }else{
                NSString *title = @"";
                NSString *message = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_NOCANCEL", nil, [Utils getLanguage], nil);
                [self showFinishAlertDialogWithTitle:title message:message];
            }
            
            
            
        }
    }];
}

//- (IBAction)moveBack:(id)sender {
//
//    if (self.page == 1) {
//        NoticeReportDateViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeReportStoryboard"];
//        [self.slideMenuController changeMainViewController:viewController close:YES];
//    }
//    else if (self.page == 2){
//        NoticeReportDateMessageViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeReportDateMessageStoryboard"];
//        [self.slideMenuController changeMainViewController:viewController close:YES];
//    }
//    else if (self.page == 3){
//        ReportLeaveViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportLeaveStoryboard"];
//        [self.slideMenuController changeMainViewController:viewController close:YES];
//    }
//
//}

- (void)showFinishAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else{
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    
    //alertDialog.heightContstraint.constant = 300.0f;
    
    [alertDialog showDialogInView:self.view title:title message:message];
    
}

- (void)onAlertDialogClose {
    
    ReportLeaveViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportLeaveStoryboard"];
    
    //    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}



- (IBAction)moveBack:(id)sender {
    [self dismissDetailLeaveViewController];
}
@end
