//
//  ConfirmDialog.m
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "ConfirmDialog.h"
#import "Utils.h"
@interface ConfirmDialog () {
    BOOL isShowing;
}

@end

@implementation ConfirmDialog

- (void)viewDidLoad {
    [super viewDidLoad];
 
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)].CGPath;
    self.headerView.layer.mask = maskLayer;
    self.headerView.layer.masksToBounds = YES;
    isShowing = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm,*gradientCancel;
    //set color header
    [self.btnConfirm layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.btnConfirm.bounds;
    [self.btnConfirm.layer insertSublayer:gradientConfirm atIndex:0];
    
    //set color button next
    [self.btnCancel layoutIfNeeded];
    gradientCancel = [Utils getGradientColorStatus:@"red"];
    gradientCancel.frame = self.btnCancel.bounds;
    [self.btnCancel.layer insertSublayer:gradientCancel atIndex:0];
    
    [self.btnCancel setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_WORKING_PRI_REP_CANCEL",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    [self.btnConfirm setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_WORKING_PRI_REP_CONFIRM",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

#pragma mark - Dialog functions
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message {
    
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
        [targetView addSubview:self.view];
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height-20)];
        [targetView addSubview:self.view];
    }
        
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    
    [self.headerTitleLabel setText:title];
    [self.messageLabel setText:message];
    
    isShowing = YES;
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [self.stackView setAlpha:0.0];
    [self.messageLabel setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onConfirmDialog:confirm:)]) {
        [self.delegate onConfirmDialog:self confirm:NO];
    }
}
- (BOOL)isDialogShowing {
    return isShowing;
}

#pragma mark - Action functions
- (IBAction)actionCancel:(id)sender {
    [self dismissDialog];
}

- (IBAction)actionConfirm:(id)sender {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onConfirmDialog:confirm:)]) {
        [self.delegate onConfirmDialog:self confirm:YES];
    }
}
@end
