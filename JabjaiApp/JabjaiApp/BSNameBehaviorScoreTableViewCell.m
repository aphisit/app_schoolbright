//
//  BSNameBehaviorScoreTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSNameBehaviorScoreTableViewCell.h"

@implementation BSNameBehaviorScoreTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat max = MAX(self.scoreLabel.frame.size.width, self.scoreLabel.frame.size.height);
    self.scoreLabel.layer.cornerRadius = max/2.0;
    self.scoreLabel.layer.masksToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
