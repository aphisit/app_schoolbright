//
//  CallJHGetSubjectAPI.h
//  JabjaiApp
//
//  Created by toffee on 11/22/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JHSubjectModel.h"
@class CallJHGetSubjectAPI;

@protocol CallJHGetSubjectAPIDelegate <NSObject>

@optional
- (void)callJHGetSubjectAPI:(CallJHGetSubjectAPI *)classObj data:(NSArray<JHSubjectModel *> *)data success:(BOOL)success;
- (void)notArrangeTimeTable:(CallJHGetSubjectAPI *)classObj;

@end


@interface CallJHGetSubjectAPI : NSObject
@property (nonatomic, weak) id<CallJHGetSubjectAPIDelegate> delegate;

- (void)call:(long long)schoolId classroomId:(NSString*)classroomId;
@end
