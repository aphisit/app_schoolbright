//
//  HWTReciverViewControllerXIB.h
//  JabjaiApp
//
//  Created by Mac on 10/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HWTReciverViewControllerXIB : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, assign) long long homeworkID;
@property (nonatomic, strong) NSString* sortData;
@property (nonatomic, strong) NSString *sortChangeData;
- (void)getRecieverListWithhomeworkID:(long long)homeworkId page:(NSInteger)page readStatus:(NSString *)readStatus;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;


@end

NS_ASSUME_NONNULL_END
