//
//  ReportCheckSubjectDateTimeOfStudentViewController.m
//  JabjaiApp
//
//  Created by toffee on 23/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportCheckSubjectDateTimeOfStudentViewController.h"
#import "UserData.h"
#import "Utils.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ReportCheckSubjectDateTimeOfStudentViewController (){
    int backgroundInterger;
    NSArray* dateTimeArray;
    NSArray<RPCheckNameSubjectDateTimeModel*>* dateTimeStatusAllArray;
}
@end
static NSString *cellIdentifier = @"Cell";

@implementation ReportCheckSubjectDateTimeOfStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    backgroundInterger = 0;
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportCheckFlagStatusCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
   
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportCheckNameSubjectDateTimeTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    dateTimeStatusAllArray = self.dateTimeAllArray;
    
    self.studentNameLabel.text = self.studentName;
    
    if (self.studentPic == NULL || [self.studentPic isEqual:@""]) {
        self.studentImage.image = [UIImage imageNamed:@"ic_user_info"];
    }
    else{
        [self.studentImage sd_setImageWithURL:[NSURL URLWithString:self.studentPic]];
    }
   
    self.studentImage.layer.cornerRadius = self.studentImage.frame.size.height /2;
    self.studentImage.layer.masksToBounds = YES;
    self.studentImage.layer.borderWidth = 0;
    

}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_REPORT_SUBJECT",nil,[Utils getLanguage],nil);
    self.showNoStudent.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_NOSTUDENT",nil,[Utils getLanguage],nil);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ReportCheckFlagStatusCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
  
    if (indexPath.row == 0) {
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ALL",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.dateTimeAllArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_allwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_allgray"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor grayColor];
            cell.amountLabel.textColor = [UIColor grayColor];
            
        }
        
    }
    else if (indexPath.row == 1) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ONTIME",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.dateTimeOnTimeArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockgreen"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:0.24 green:0.79 blue:0.53 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:0.24 green:0.79 blue:0.53 alpha:1.0];
        }
    }
    else if (indexPath.row == 2) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_LATE",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.dateTimeLateArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockyellow"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:1.00 green:0.66 blue:0.15 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:1.00 green:0.66 blue:0.15 alpha:1.0];
        }
    }
    else if (indexPath.row == 3) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ABSENCE",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.dateTimeAbsenArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockred"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.39 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.39 alpha:1.0];
        }
    }
    else if (indexPath.row == 4) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_SICK",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.dateTimeSickArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockviolet"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:0.88 green:0.40 blue:1.00 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:0.88 green:0.40 blue:1.00 alpha:1.0];
        }
    }
    else if (indexPath.row == 5) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PERSONAL",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.dateTimePersonalArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockpurple"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:0.60 green:0.00 blue:0.75 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:0.60 green:0.00 blue:0.75 alpha:1.0];
        }
    }
    else if (indexPath.row == 6) {
        
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_EVENT",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.dateTimeEventArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockblue"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:0.00 green:0.43 blue:0.82 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:0.00 green:0.43 blue:0.82 alpha:1.0];
        }
    }else{
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_UNDEFINED",nil,[Utils getLanguage],nil);
        cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",self.dateTimeUndefinedArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            cell.statusLabel.textColor = [UIColor whiteColor];
            cell.amountLabel.textColor = [UIColor whiteColor];
        }else{
            cell.clockImage.image = [UIImage imageNamed:@"ic_clockblack"];
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
            cell.statusLabel.textColor = [UIColor colorWithRed:0.36 green:0.36 blue:0.36 alpha:1.0];
            cell.amountLabel.textColor = [UIColor colorWithRed:0.36 green:0.36 blue:0.36 alpha:1.0];
        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    backgroundInterger = indexPath.row;
    
    if (backgroundInterger == 0) {
        dateTimeStatusAllArray = self.dateTimeAllArray;
    }
    else if (backgroundInterger == 1){
        dateTimeStatusAllArray = self.dateTimeOnTimeArray;
    }
    else if (backgroundInterger == 2){
        dateTimeStatusAllArray = self.dateTimeLateArray;
    }
    else if (backgroundInterger == 3){
        dateTimeStatusAllArray = self.dateTimeAbsenArray;
    }
    else if (backgroundInterger == 4){
        dateTimeStatusAllArray = self.dateTimeSickArray;
    }
    else if (backgroundInterger == 5){
        dateTimeStatusAllArray = self.dateTimePersonalArray;
    }
    else if (backgroundInterger == 6){
        dateTimeStatusAllArray = self.dateTimeEventArray;
    }
    else if (backgroundInterger == 7){
        dateTimeStatusAllArray = self.dateTimeUndefinedArray;
    }
 
    [self.collectionView reloadData];
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (dateTimeStatusAllArray.count == 0) {
        self.showNoStudent.hidden = NO;
    }else{
        self.showNoStudent.hidden = YES;
    }
    return dateTimeStatusAllArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     ReportCheckNameSubjectDateTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    RPCheckNameSubjectDateTimeModel *model;
    model = [dateTimeStatusAllArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateDateTimeLabel:[model getDate] status:[model getStatus]];
    [cell updateStatus:[[model getStatus]intValue]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}


- (IBAction)moveBack:(id)sender {
    ReportCheckSubjectHistoryStudentAllViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportCheckSubjectHistoryStudentAllStoryboard"];
    viewController.studentStatusOnTimeArray = self.studentStatusOnTimeArray;
    viewController.studentStatusLateArray = self.studentStatusLateArray;
    viewController.studentStatusPersonalArray = self.studentStatusPersonalArray;
    viewController.studentStatusSickArray = self.studentStatusSickArray;
    viewController.studentStatusEventArray = self.studentStatusEventArray;
    viewController.studentStatusAbsenArray = self.studentStatusAbsenArray;
    viewController.studentStatusAllArray = self.studentStatusAllArray;
    viewController.studentStatusUndefinedArray = self.studentStatusUndefinedArray;
    viewController.nameClassRoom = self.nameClassRoom;
    viewController.nameSubject = self.nameSubject;
    viewController.selectedClassroomId = self.selectedClassroomId;
    viewController.consideredDate = self.consideredDate;
    viewController.selectedSubjectId = self.selectedSubjectId;
    viewController.studentAllArray = self.studentAllArray;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}



@end
