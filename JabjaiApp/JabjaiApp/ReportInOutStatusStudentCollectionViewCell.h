//
//  ReportInOutStatusStudentCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 27/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutStatusStudentCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *clockImage;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundColor;

- (void) setBackgroundClick:(int)number numberClick:(int)numberClick;

@end

NS_ASSUME_NONNULL_END
