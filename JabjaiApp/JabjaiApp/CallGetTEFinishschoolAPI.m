//
//  CallGetTEFinishschoolAPI.m
//  JabjaiApp
//
//  Created by toffee on 14/11/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallGetTEFinishschoolAPI.h"
#import "APIURL.h"
#import "Utils.h"

@implementation CallGetTEFinishschoolAPI{
    NSInteger connectCounter, resCode;
}

-(void)call:(long long)schoolId userId:(long long)userId barcode:(NSString *)barcode status:(NSString *)status{
    connectCounter = 0;
    [self doCheckFinishSchool:schoolId userId:userId barcode:barcode status:status];
}

-(void)doCheckFinishSchool:(long long)schoolId userId:(long long)userId barcode:(NSString *)barcode status:(NSString *)status{
    
    NSString *urlString = [APIURL getTEFinishSchool:schoolId userId:userId idCode:barcode status:status];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

                  if(error != nil) {
                      NSLog(@"An error occured : %@" , [error localizedDescription]);
                  }
                  else if(data == nil) {
                      NSLog(@"Data is null");
                  }
                  else {
                      NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                      NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
                  }

                  if(connectCounter < TRY_CONNECT_MAX) {
                      connectCounter++;

                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self doCheckFinishSchool:schoolId userId:userId barcode:barcode status:status];

                  }
                  else {

                      isFail = YES;
                      connectCounter = 0;
                  }

              }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self doCheckFinishSchool:schoolId userId:userId barcode:barcode status:status];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSDictionary *dict = returnedData;
                resCode = [[dict objectForKey:@"resCode"] integerValue];
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTEFinishschoolAPI:resCode:success:)]) {
                               [self.delegate callGetTEFinishschoolAPI:self resCode:resCode success:YES];
                }
            }
            
        }
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTEFinishschoolAPI:resCode:success:)]) {
                [self.delegate callGetTEFinishschoolAPI:self resCode:resCode success:NO];
            }
        }
    }];
    
    
}

@end
