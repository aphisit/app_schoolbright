//
//  NoticeStopHalfViewController.m
//  JabjaiApp
//
//  Created by mac on 3/7/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "NoticeStopHalfViewController.h"
#import "NoticeStopHalfDetailViewController.h"
#import "SchoolCodeDialogViewController.h"
#import "AlertDialog.h"
#import "AlertDialogConfirm.h"
#import "DateUtility.h"
#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"
#import "UserInfoViewController.h"

@interface NoticeStopHalfViewController (){
    
    NSInteger selectStartDateTextField;
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    UnAuthorizeMenuDialog *unAuthorizeMenuDialog;
    NSString *dateString;
    SchoolCodeDialogViewController *otherCodeDialog;
    NSDate *dayStart, *dayEnd;
    NSArray *causelist, *peroidlist;
    NSInteger connectCounter;
    NSBundle *myLangBundle;
    NSMutableArray *authorizeSubMenuArray;
}
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (nonatomic, assign) BOOL isShowing;
@property (strong, nonatomic) CalendarDialog *calendarDialog;
@property (strong, nonatomic) SelectOneDayCalendarViewController *calendarSelectoneDialog;
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@property (strong, nonatomic) NSDate *minDate;
@property (strong, nonatomic) NSDate *maxDate;
@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) UIColor *orangeColor;
@end
static NSString *sickRequestCode = @"sick";
static NSString *periodRequestCode = @"period";
@implementation NoticeStopHalfViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    if ([UserData getUserType] != STUDENT) { [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//call to authorize menu
    }
    // Do any additional setup after loading the view.
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    
    otherCodeDialog = [[SchoolCodeDialogViewController alloc] init];
    otherCodeDialog.delegate = self;
    
    self.cause.delegate = self;
    self.leaveDate.delegate = self;
    self.period.delegate = self;
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    self.calendarSelectoneDialog = [[SelectOneDayCalendarViewController alloc] initWithTitle:@"เลือกวันที่" minimumDate:self.minDate maximumDate:self.maxDate];
    self.calendarSelectoneDialog.delegate = self;
    self.gregorian = [Utils getGregorianCalendar];
    self.maxDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:+1 toDate:[NSDate date] options:0]; // [NSDate date];
    self.minDate = [NSDate date];
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    [self setLanguage];
    [self doInit];
    [self getWarning];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section2.layer setShadowOpacity:0.3];
    [self.section2.layer setShadowRadius:3.0];
    [self.section2.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section3.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section3.layer setShadowOpacity:0.3];
    [self.section3.layer setShadowRadius:3.0];
    [self.section3.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.headerNextbtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.headerNextbtn.layer setShadowOpacity:0.3];
    [self.headerNextbtn.layer setShadowRadius:3.0];
    [self.headerNextbtn.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    
}

-(void)setLanguage{
    
    self.headerLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE",nil,myLangBundle,nil);
    self.headerReasonLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_LEAVEREASON",nil,myLangBundle,nil);
    self.headerDatefromLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_DATEFROM",nil,myLangBundle,nil);
    self.headerSeasonLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_SEASON",nil,myLangBundle,nil);
    self.cause.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEAVE_REASON",nil,myLangBundle,nil);
    self.leaveDate.placeholder = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_DATEFROM",nil,myLangBundle,nil);
    self.period.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEAVE_SEASON",nil,myLangBundle,nil);
    [self.headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

- (void)doInit{
    
    self.cause.text = self.leaveCause;
    self.leaveDate.text = self.leaveFirstDate;
    if (self.leavePeriod == nil) {
        self.period.text = @"";
    }
    else{
        self.period.text = [NSString stringWithFormat:@"%@ (%@)" , self.leaveFirstDate , self.leavePeriod];
    }
}

#pragma mark - CallGetMenuListAPIDelegate
///check authorize menu
- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
    if ((resCode == 300 || resCode == 200) && success ) {
        NSLog(@"xx");
        authorizeSubMenuArray = [[NSMutableArray alloc] init];
        if (data != NULL) {
            for (int i = 0;i < data.count; i++) {
                NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
                for (int j = 0; j < subMenuArray.count; j++) {
                    NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
                    [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
                }
            }
        }
        NSString* notPermissionText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_UNAUTHORRIZE",nil,[Utils getLanguage],nil);
        NSString* pleaseContactText = NSLocalizedStringFromTableInBundle(@"LABEL_AUTHORRIZE_MENU_PLEASECONTACT",nil,[Utils getLanguage],nil);
        NSString *alertMessage = [NSString stringWithFormat:@"%@\n%@",notPermissionText,pleaseContactText];
        
        if (![authorizeSubMenuArray containsObject: @"31"]) {
            [self showAlertDialogAuthorizeMenu:alertMessage];
            NSLog(@"ไม่มีสิทธิ์");
        }
    }else{
        
        self.dbHelper = [[UserInfoDBHelper alloc] init];
        [self.dbHelper deleteUserAllWithMasterId:[UserData getMasterUserID]];
        // Logout
        [UserData setUserLogin:NO];
        [UserData saveMasterUserID:0];
        [UserData saveUserID:0];
        [UserData setUserType:USERTYPENULL];
        [UserData setAcademyType:ACADEMYTYPENULL];
        [UserData setFirstName:@""];
        [UserData setLastName:@""];
        [UserData setUserImage:@""];
        [UserData setGender:MALE];
        [UserData setClientToken:@""];
        LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    
  
}

- (void)onAlertDialogClose {
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if ([UserData getUserType] != STUDENT) {
        causelist = [NSArray arrayWithObjects:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_SICK",nil,myLangBundle,nil)
                     ,NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PERSONAL",nil,myLangBundle,nil)
                     ,NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_GOVERNMENT",nil,myLangBundle,nil)
                     ,NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_STUDY",nil,myLangBundle,nil)
                     ,NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_MATERNITY",nil,myLangBundle,nil)
                     ,NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_VACATION",nil,myLangBundle,nil)
                     ,NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_ORDINATION",nil,myLangBundle,nil)
                     ,NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_MILITARY",nil,myLangBundle,nil)
                     ,nil];
    }else{
        causelist = [NSArray arrayWithObjects:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_SICK",nil,myLangBundle,nil)
                     ,NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PERSONAL",nil,myLangBundle,nil)
                     ,nil];
    }
    
//    causelist = [NSArray arrayWithObjects:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_SICK",nil,myLangBundle,nil),NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PERSONAL",nil,myLangBundle,nil),NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_OTHER",nil,myLangBundle,nil),nil];
    
    peroidlist = [NSArray arrayWithObjects:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_HALF_MORNING",nil,myLangBundle,nil),NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_HALF_AFTERNOON",nil,myLangBundle,nil), nil];
    
    self.calendarSelectoneDialog = [[SelectOneDayCalendarViewController alloc] initWithTitle:@"เลือกวันที่" minimumDate:self.minDate maximumDate:self.maxDate];
    self.calendarSelectoneDialog.delegate = self;
    
    if (textField.tag == 1) {
        NSLog(@"Cause = %@", causelist);
        if (causelist != nil && causelist.count > 0) {
            
            [self showTableListDialogWithRequestCode:sickRequestCode data:causelist];
            
        }
        
        return NO;
    }
    
    else if (textField.tag == 2){
        
        if([self.calendarSelectoneDialog isDialogShowing]) {
            [self.calendarSelectoneDialog dismissDialog];
        }
        self.calendarSelectoneDialog = [[SelectOneDayCalendarViewController alloc] initWithTitle:@"เลือกวันที่เริ่มลาหยุด" minimumDate:self.minDate maximumDate:self.maxDate];
        self.calendarSelectoneDialog.delegate = self;
        [self.calendarSelectoneDialog showDialogInView:self.view];
        selectStartDateTextField = textField.tag;
        //  selectStartDateTextField = YES;
        
        return NO;
    }
    
    else if (textField.tag == 3){
        
        if (self.leaveDate.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_LEAVE_PLEASE_DATE",nil,myLangBundle,nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else{
            
            NSLog(@"Cause = %@", causelist);
            if (peroidlist != nil && peroidlist.count > 0) {
                
                [self showTableListDialogWithRequestCode:periodRequestCode data:peroidlist];
                
            }
            
            
        }
        
        return NO;
    }
    
    
    return YES;
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showAlertDialogAuthorizeMenu:(NSString *)message {
    if(unAuthorizeMenuDialog != nil && [unAuthorizeMenuDialog isDialogShowing]) {
        [unAuthorizeMenuDialog dismissDialog];
    }
    else {
        unAuthorizeMenuDialog = [[UnAuthorizeMenuDialog alloc] init];
        unAuthorizeMenuDialog.delegate = self;
    }
    [unAuthorizeMenuDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index{
    
    if ([requestCode isEqualToString:sickRequestCode]) {
        self.cause.text = [causelist objectAtIndex:index];
        self.leaveCause = [causelist objectAtIndex:index];
    }
    else if ([requestCode isEqualToString:periodRequestCode]){
        self.period.text = [NSString stringWithFormat:@"%@ (%@)", self.leaveFirstDate , [peroidlist objectAtIndex:index]];
        self.leavePeriod = [peroidlist objectAtIndex:index];
    }
}

- (void)getOtherCauseList{
    
    NSString *title = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REASON",nil,myLangBundle,nil);
    NSString *hint = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_SPECIFY",nil,myLangBundle,nil);
    
    [otherCodeDialog showDialogInView:self.view title:title hint:hint];
}

#pragma mark - SchoolCodeDialogViewControllerDelegate
-(void)onCancelButtonPress {
    
}

-(void)onOkButtonPress {
    
    NSString *password = otherCodeDialog.passwordTextField.text;
    NSMutableString *pwd = [[NSMutableString alloc] initWithString:password];
    CFStringTrimWhitespace((__bridge CFMutableStringRef) pwd);
    
    self.cause.text = password;
    self.leaveCause = password;
    
    [otherCodeDialog dismissDialog];
    
    
}

#pragma mark - CalendarDialogDelegate
-(void)calendarDialogPressOK:(NSArray<NSDate *> *)selectedDates {
    
    if(selectedDates.count >= 2) {
        NSDate *minDate = [DateUtility minDate:selectedDates];
        NSDate *maxDate = [DateUtility maxDate:selectedDates];
        
        self.leaveDate.text = [self.dateFormatter stringFromDate:minDate];
        
        self.leaveStartdate = minDate;
    }
    else
        if(selectedDates.count > 0) {
            
            NSDate *selectDate = [selectedDates objectAtIndex:0];
            
            //        if(selectStartDateTextField) {
            //            self.firstdate.text = [self.dateFormatter stringFromDate:selectDate];
            //            self.lastdate.text = @"";
            //
            //            self.leaveFirstDate = self.firstdate.text;
            //            self.leaveLastDate = self.lastdate.text;
            //        }
            //        else {
            //            self.firstdate.text = [self.dateFormatter stringFromDate:selectDate];
            //            self.lastdate.text = [self.dateFormatter stringFromDate:selectDate];
            //
            //            self.leaveFirstDate = self.firstdate.text;
            //            self.leaveLastDate = self.lastdate.text;
            //        }
            
            if (selectStartDateTextField == 2) {
                self.leaveDate.text = [self.dateFormatter stringFromDate:selectDate];
                self.leaveFirstDate = self.leaveDate.text;
                
                self.period.text = @"";
                
                self.leaveStartdate = selectDate;
                
            }
            else {
               
            }
            
        }
        else {
            self.leaveDate.text = @"";
            
            self.leaveFirstDate = self.leaveDate.text;

        }
    
    
}

-(void)calendarDialogPressCancel {
    NSLog(@"%@", @"Cancel calendar dialog");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [self registerForKeyboardNotification];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [self unRegisterForKeyboardNotification];
}

-(void)viewDidAppear:(BOOL)animated {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
//    [self.revealViewController revealToggle:self.revealViewController];
    
}

- (IBAction)nextPage:(id)sender {
    
    if ([self validateData]) {
        [self gotoNextPage];
    }
    
}

-(BOOL)validateData{
    
    
    if (self.cause.text.length == 0 || self.leaveDate.text.length == 0 || self.period.text.length == 0) {
        
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_LEAVE_FIL_DETAILS",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    else {
        
    }
    
    return YES;
}

-(void)gotoNextPage{
    
    dayStart = self.leaveStartdate;
    
    NoticeStopHalfDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeStopHalfDetailStoryboard"];
    
    viewController.leaveCause = self.leaveCause;
    viewController.leavePeriod = self.leavePeriod;
    viewController.leaveFirstDate = self.leaveFirstDate;
    
    viewController.leaveStartdate = self.leaveStartdate;
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}



#pragma mark - Keyboard Listener

- (void)registerForKeyboardNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)unRegisterForKeyboardNotification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (screenSize.height == 812.0f) {
            
            otherCodeDialog.textFieldDailogTopConstraint.constant = 167.5;
            otherCodeDialog.textFieldDailogBottomConstraint.constant = 367.5;
            
        }
        
        else if (screenSize.height == 736.0f){
            
            otherCodeDialog.textFieldDailogTopConstraint.constant = 129.5;
            otherCodeDialog.textFieldDailogBottomConstraint.constant = 329.5;
            
        }
        
        else if (screenSize.height == 667.0f){
            
            otherCodeDialog.textFieldDailogTopConstraint.constant = 95;
            otherCodeDialog.textFieldDailogBottomConstraint.constant = 295;
            
        }
        else if (screenSize.height == 568.0f){
            
            otherCodeDialog.textFieldDailogTopConstraint.constant = 15.5;
            otherCodeDialog.textFieldDailogBottomConstraint.constant = 275.5;
        }
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (screenSize.height == 812.0f) {
            otherCodeDialog.textFieldDailogTopConstraint.constant = 267.5;
            otherCodeDialog.textFieldDailogBottomConstraint.constant = 267.5;
        }
        else if (screenSize.height == 736.0f){
            otherCodeDialog.textFieldDailogTopConstraint.constant = 229.5;
            otherCodeDialog.textFieldDailogBottomConstraint.constant = 229.5;
        }
        else if (screenSize.height == 667.0f){
            otherCodeDialog.textFieldDailogTopConstraint.constant = 195;
            otherCodeDialog.textFieldDailogBottomConstraint.constant = 195;
        }
        else if (screenSize.height == 568.0f){
            otherCodeDialog.textFieldDailogTopConstraint.constant = 145.5;
            otherCodeDialog.textFieldDailogBottomConstraint.constant = 145.5;
        }
    }
}

- (void)getWarning{
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getWarningWithUserID:userID schoolid:schoolid];
    
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getWarning];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            connectCounter = 0;
            NSString *dialogTitle = @"แจ้งเตือน";
            NSString *position = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSMutableString *strResult = [[NSMutableString alloc] initWithString:position];
            [strResult replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [strResult length])];
            CFStringTrimWhitespace((__bridge CFMutableStringRef) strResult);
            
            if ([strResult isEqualToString:@"SUCCESS"]) {
                [alertDialog dismissDialog];
            }
            else{
                [self showFinishAlertDialogWithTitle:dialogTitle message:strResult];
            }
        }
        
    }];
}


- (void)showFinishAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else{
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    [alertDialog showDialogInView:self.view title:title message:message];
    
}
@end
