//
//  CallGetStudentInClassAPI.m
//  JabjaiApp
//
//  Created by mac on 7/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetStudentInClassAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetStudentInClassAPI () {
    NSInteger connectCounter;
    NSMutableArray<TAStudentStatusModel *> *studentStatusArray;
}

@end

@implementation CallGetStudentInClassAPI

- (void)call:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId date:(NSString *)date {
    connectCounter = 0;
    [self getStudentInClass:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId date:date];
}

#pragma mark - Get API Data
- (void)getStudentInClass:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId date:(NSString *)date{
    
    NSString *URLString = [APIURL getStudentInClassWithSchoolId:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getStudentInClass:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId date:date];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentInClass:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentInClass:schoolId classroomId:classroomId subjectId:subjectId teacherId:teacherId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(studentStatusArray != nil) {
                    [studentStatusArray removeAllObjects];
                    studentStatusArray = nil;
                }
                
                studentStatusArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long userId = [[dataDict objectForKey:@"UserId"] longLongValue];
                    BOOL authorized = [[dataDict objectForKey:@"authorized"] boolValue];
                    
                    NSMutableString *studentId, *studentName, *teacherName , *studentPic;
                    long long teacherId = 0;
                    NSInteger studentState = 0;
                    
                    if(![[dataDict objectForKey:@"studentId"] isKindOfClass:[NSNull class]]) {
                        studentId = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentId"]];
                    }
                    else {
                        studentId = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    
                    if ([[UserData getChangLanguage] isEqualToString:@"th"]) {
                        if(![[dataDict objectForKey:@"studentName"] isKindOfClass:[NSNull class]]) {
                            studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentName"]];
                        }
                        else {
                            studentName = [[NSMutableString alloc] initWithString:@""];
                        }
                    }else{
                        if(![[dataDict objectForKey:@"studentNameEN"] isKindOfClass:[NSNull class]]) {
                            studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentNameEN"]];
                            if ([studentName isEqualToString:@" "]) {
                                 studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentName"]];
                            }
                        }
                        else {
                            studentName = [[NSMutableString alloc] initWithString:@""];
                        }
                        
                    }
                    
                   
                    
                    if(![[dataDict objectForKey:@"teachername"] isKindOfClass:[NSNull class]]) {
                        teacherName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"teachername"]];
                    }
                    else {
                        teacherName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"pic"] isKindOfClass:[NSNull class]]) {
                        studentPic = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"pic"]];
                    }
                    else {
                        studentPic = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"Student_State"] isKindOfClass:[NSNull class]]) {
                        studentState = [ [dataDict objectForKey:@"Student_State"]integerValue];
                    }
                   
                    if(![[dataDict objectForKey:@"teacherId"] isKindOfClass:[NSNull class]]) {
                        teacherId = [[dataDict objectForKey:@"teacherId"] longLongValue];
                    }
                    
                    int scanStatus = -1;
                    
                    if(![[dataDict objectForKey:@"scanstatus"] isKindOfClass:[NSNull class]]) {
                        int statusCode = [[dataDict objectForKey:@"scanstatus"] intValue];
                        
                        switch (statusCode) {
                            case 0:
                                scanStatus = 0; // ontime
                                break;
                            case 1:
                                scanStatus = 1; // late
                                break;
                            case 3:
                                scanStatus = 3; // absence
                                break;
                            case 4:
                                scanStatus = 4; // personal leave
                                break;
                            case 5:
                                scanStatus = 5; // sick leave
                                break;
                            case 6:
                                scanStatus = 6; // event
                                break;
                            default:
                                scanStatus = -1; // not scan
                                break;
                        }
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentId);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) teacherName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentPic);
                    
                    TAStudentStatusModel *model = [[TAStudentStatusModel alloc] init];
                    [model setUserId:userId];
                    [model setScanStatus:scanStatus];
                    [model setStudentId:studentId];
                    [model setStudentName:studentName];
                    [model setTeacherId:teacherId];
                    [model setCheckedTeacherName:teacherName];
                    [model setAuthorized:authorized];
                    [model setStudentPic:studentPic];
                    [model setStudentState:studentState];
                    
                    [studentStatusArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetStudentInClassAPI:data:success:)]) {
                    [self.delegate callGetStudentInClassAPI:self data:studentStatusArray success:YES];
                }
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetStudentInClassAPI:data:success:)]) {
                [self.delegate callGetStudentInClassAPI:self data:nil success:NO];
            }
        }

    }];
}

@end
