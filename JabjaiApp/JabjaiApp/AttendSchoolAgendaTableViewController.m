//
//  AttendSchoolAgendaTableViewController.m
//  JabjaiApp
//
//  Created by mac on 10/24/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "AttendSchoolAgendaTableViewController.h"
#import "AgendaHeaderTableViewCell.h"
#import "AgendaTableViewCell.h"
#import "APIURL.h"
#import "AttendToSchoolModel.h"
#import "Utils.h"
#import "DateUtility.h"
#import "Constant.h"

@interface AttendSchoolAgendaTableViewController () {
    NSArray *titleArr;
    NSMutableArray *attendToSchoolArray;
    NSMutableArray *dateStatus;
    
    NSInteger connectCounter;
}

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSCalendar *gregorian;
  

@end

static NSString *tableHeaderCellIdentifier = @"HeaderCell";
static NSString *tableCellIdentifier = @"Cell";

@implementation AttendSchoolAgendaTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //[self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AgendaHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableHeaderCellIdentifier];
    //[self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AgendaTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ASAttendSchoolAgendaTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableCellIdentifier];
    
    
    titleArr = @[NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_DATE",nil,[Utils getLanguage],nil),NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_STATUS",nil,[Utils getLanguage],nil)];
    dateStatus = [[NSMutableArray alloc] init];
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
//    self.gregorian = [Utils getGregorianCalendar];
//    self.maximumDate = [NSDate date];
//    self.minimumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:[NSDate date] options:0];

    NSLog(@"minimumDate = %@",self.minimumDate);
    NSLog(@"maximumDate = %@",self.maximumDate);
    connectCounter = 0;
    [self callAPIGetAttendSchoolData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if(section == 0) {
//        return 1;
//    } else {
//
        if(attendToSchoolArray == nil) {
            return 0;
        }
        else {
            return attendToSchoolArray.count;
        }
    //}
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     ASAttendSchoolAgendaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableCellIdentifier forIndexPath:indexPath];
    
        if(attendToSchoolArray != nil) {
            AttendToSchoolModel *model = [attendToSchoolArray objectAtIndex:indexPath.row];
            NSLog(@"index = %ld",(long)indexPath.row);
            NSLog(@"status = %d",[model.status intValue]);
            NSString *dateString = [self.dateFormatter stringFromDate:model.scanDate];
            NSString *statusInString, *statusOutString;
         
            if([model.status intValue] == 0) { // ON TIME
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ONTIME",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 1) { // LATE
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_LATE",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 3) { // ABSENCE
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ABSENCE",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 6) { // ON LEAVE
                if ([UserData getUserType] == STUDENT) {
                    statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_SCHOOL_OUT",nil,[Utils getLanguage],nil);
                }else{
                    statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_HOLIDAY",nil,[Utils getLanguage],nil);
                }
                
            }
            else if([model.status intValue] == 8) { // HOLIDAY
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_HOLIDAY",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 9) { // TERMOFF
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_HOLIDAY",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 10) { //PERSONAL
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_PERSONAL",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 11) { //SICK
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_SICK",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 12) { //EVENT
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_EVENT",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 21) {
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_MATERNITY",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 22) {
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_VACATION",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 23) {
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ORDINATION",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 24) {
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_MILITARY",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 25) {
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_STUDY",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 26) {
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_GOVERNMENT",nil,[Utils getLanguage],nil);
            }
            else if([model.status intValue] == 99) { //UNCHECK
                statusInString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_UNDEFINED",nil,[Utils getLanguage],nil);
            }
            else {
                statusInString = @"";
            }
            
 
            if([model.statusOut intValue] == 0 ) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_OUTONTIME",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 2) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_OUTADVANCE",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 3) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_OUTLATE",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == -3) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ABSENCE",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 6) { // HOLIDAY
                if ([UserData getUserType] == STUDENT) {
                    statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_SCHOOL_OUT",nil,[Utils getLanguage],nil);
                }else{
                    statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_HOLIDAY",nil,[Utils getLanguage],nil);
                }
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_HOLIDAY",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 8) { // HOLIDAY
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_HOLIDAY",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 9) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_HOLIDAY",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 10) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_PERSONAL",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 11) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_SICK",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 12) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_EVENT",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 21) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_MATERNITY",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 22) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_VACATION",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 23) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ORDINATION",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 24) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_MILITARY",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 25) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_STUDY",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 26) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_GOVERNMENT",nil,[Utils getLanguage],nil);
            }
            else if([model.statusOut intValue] == 99) {
                statusOutString = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_UNDEFINED",nil,[Utils getLanguage],nil);
            }
            else {
                statusOutString = @"";
            }
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
            timeFormatter.dateFormat = @"HH:mm:ss";
            [cell setColorStatusInView:[model.status intValue]];
            [cell setColorStatusOutView:[model.statusOut intValue]];
            cell.dateLabel.text = [NSString stringWithFormat:@"%@",dateString];
            cell.statusInLabel.text = statusInString;
            cell.statusOutLabel.text = statusOutString;
           
            if ([[timeFormatter stringFromDate:model.timeIN] isEqual:@"00:00:00"]) {
                cell.timeInLabel.text = @"-";
            }else{
                cell.timeInLabel.text = [timeFormatter stringFromDate:model.timeIN];
            }
            
            if ([[timeFormatter stringFromDate:model.timeOut] isEqual:@"00:00:00"]) {
                cell.timeOutLabel.text = @"-";
            }
            else{
                cell.timeOutLabel.text = [timeFormatter stringFromDate:model.timeOut];
            }
            
            cell.timeInStrLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_TIMEIN",nil,[Utils getLanguage],nil);
            cell.timeOutStrLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_TIMEOUT",nil,[Utils getLanguage],nil);
        }

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return 112;
    }else{
        return 150;
    }
    
}

#pragma mark - GetAPIData

-(void)callAPIGetAttendSchoolData {
    [self getAttendSchoolData:self.minimumDate endDate:self.maximumDate status:-1];
}

-(void)getAttendSchoolData:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status {
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getAttendSchoolDataURLWithStartDate:startDate endDate:endDate status:status schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self callAPIGetAttendSchoolData];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetAttendSchoolData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetAttendSchoolData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                attendToSchoolArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSString *timeIn, *timeOut;
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSString *statusInStr = [dataDict objectForKey:@"StatusIN"];
                    NSString *statusOutStr = [dataDict objectForKey:@"StatusOut"];
                    NSMutableString *scanDateTimeStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dScan"]];
                    
                    if (![[dataDict objectForKey:@"TimeIn"]isKindOfClass:[NSNull class]]) {
                        timeIn = [dataDict objectForKey:@"TimeIn"];
                    }else{
                        timeIn = @"";
                    }
                    
                    if (![[dataDict objectForKey:@"TimeOut"]isKindOfClass:[NSNull class]]) {
                        timeOut = [dataDict objectForKey:@"TimeOut"];
                    }else{
                        timeOut = @"";
                    }
                    
                    
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) scanDateTimeStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDateFormatter *timeInForMatter = [Utils getDateFormatter];
                    [timeInForMatter setDateFormat:@"HH:mm:ss"];
                    
                    NSDateFormatter *timeOutForMatter = [Utils getDateFormatter];
                    [timeOutForMatter setDateFormat:@"HH:mm:ss"];
                    
                    
                    
                    NSDate *scanDate = [formatter dateFromString:scanDateTimeStr];
                    NSDate *timeIntDate = [formatter dateFromString:timeIn];
                    NSDate *timeOutDate = [formatter dateFromString:timeOut];
                    
                    NSInteger statusIn,statusOut;
                    if([statusInStr isKindOfClass:[NSNull class]] || [statusInStr length] == 0) {
                        statusIn = 3; //Absence
                    }
                    else {
                        if ([statusInStr isEqual:@"-"]) {
                            statusIn = 99;
                        }else{
                            statusIn = [statusInStr integerValue];
                        }
                        
                    }
                    
                    if([statusOutStr isKindOfClass:[NSNull class]] || [statusOutStr length] == 0) {
                        statusOut = 3; //Absence
                    }
                    else {
                        if ([statusOutStr isEqual:@"-"]) {
                            statusOut = 99;
                        }else{
                            statusOut = [statusOutStr integerValue];
                        }
                        
                    }
                    
                    if(statusIn != 9 || statusOut != 9) { // store the day except weekend
                        AttendToSchoolModel *model = [[AttendToSchoolModel alloc] init];
                        [model setStatus:[[NSNumber alloc] initWithInteger:statusIn]];
                        [model setStatusOut:[[NSNumber alloc] initWithInteger:statusOut]];
                        [model setTimeIN:timeIntDate];
                        [model setTimeOut:timeOutDate];
                        [model setScanDate:scanDate];
                    
                        [attendToSchoolArray addObject:model];
                    }
                    
                }
                
                // reverse date descending
                NSArray *reverse =  [[attendToSchoolArray reverseObjectEnumerator] allObjects];
                attendToSchoolArray = [[NSMutableArray alloc] initWithArray:reverse];
                
                [self.tableView reloadData];
            }
            
            
        }
        
    }];
    
}

#pragma mark - Filter

-(void)applyFilter:(Attend2SchoolFilter *)filter {
    
    NSInteger status = -1;
    
    if(filter.status == INTIME) {
        status = 0;
    }
    else if(filter.status == LATE) {
        status = 1;
    }
    else if(filter.status == ABSENCE) {
        status = 3;
    }
    else if(filter.status == ONLEAVE) {
        status = 7;
    }
    else if(filter.status == HOLIDAY) {
        status = 8;
    }
    
    self.minimumDate = filter.startDate;
    self.maximumDate = filter.endDate;
    [self getAttendSchoolData:filter.startDate endDate:filter.endDate status:status];
    
}


@end
