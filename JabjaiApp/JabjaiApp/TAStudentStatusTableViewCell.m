//
//  TAStudentStatusTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 7/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TAStudentStatusTableViewCell.h"
#import "Utils.h"
#import "UserData.h"


@interface TAStudentStatusTableViewCell () {
    UIColor *greenColor;
    UIColor *yellowColor;
    UIColor *redColor;
    UIColor *pinkColor;
    UIColor *lightBluecolor;
    UIColor *blueColor;
    UIColor *grayColor;
    
    CAGradientLayer *gradient;
    NSBundle *myLangBundle;
}
@end

@implementation TAStudentStatusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)selectStatus:(id)sender {
    
    if(self.delegate != nil) {
        [self.delegate onSelectStatusButton:self atIndex:self.index];
    }
}

- (void)updateStatus:(int)status {
    gradient = [CAGradientLayer layer];
    NSLog(@"numberStatusButton = %d",status);
    UIImage * backgroundColorImage;
     myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource: [UserData getChangLanguage] ofType:@"lproj"]];
    [self.statusButton layoutIfNeeded];
    switch (status) {
        case 0:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ONTIME",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"green"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            
            break;
        case 1:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_LATE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"yellow"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 3:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ABSENCE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"red"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 12:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_EVENT_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"blue"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 10:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_PERSONAL_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 11:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SICK_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"pink"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 6:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_EVENT_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"blue"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 4:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_PERSONAL_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"purple"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 5:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SICK_LEAVE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"pink"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 21:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_DISTRIBUTE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 22:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_RESING",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 23:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SUSPENDED",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 24:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_GRADUATE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 25:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_LOST_CONTRACT",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        case 26:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_RETIRE",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"brown"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
        default:
            [self.statusButton setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_UNDEFINED",nil,myLangBundle,nil) forState:UIControlStateNormal];
            gradient = [Utils getGradientColorStatus:@"gray"];
            gradient.frame = self.statusButton.bounds;
            UIGraphicsBeginImageContext(gradient.bounds.size);
            [gradient renderInContext:UIGraphicsGetCurrentContext()];
            backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [self.statusButton setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
            break;
    }

}
@end
