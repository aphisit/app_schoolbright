//
//  CallGetTermOfAcademicResultsAPI.h
//  JabjaiApp
//
//  Created by toffee on 21/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CallGetTermOfAcademicResultsAPI;
@protocol CallGetTermOfAcademicResultsAPIDelegate <NSObject>
@optional
- (void)callGetTermOfAcademicResultsAPI:( CallGetTermOfAcademicResultsAPI *)classObj data:(NSArray*)data success:(BOOL)success;
@end
@interface CallGetTermOfAcademicResultsAPI : NSObject
@property (nonatomic, weak) id<CallGetTermOfAcademicResultsAPIDelegate> delegate;
- (void)call:(long long)schoolId nYear:(NSInteger)nYear studentId:(long long)studentId;
@end

NS_ASSUME_NONNULL_END
