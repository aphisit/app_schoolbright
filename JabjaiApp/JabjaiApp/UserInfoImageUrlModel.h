//
//  UserInfoImageUrlModel.h
//  JabjaiApp
//
//  Created by mac on 1/31/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfoImageUrlModel : NSObject

@property (strong, nonatomic) NSString *schoolName;
@property (strong, nonatomic) NSString *imageUrl;

@end
