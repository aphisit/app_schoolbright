//
//  CreditBureauViewController.h
//  JabjaiApp
//
//  Created by mac on 10/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "JabjaiApp-Swift.h"
#import "CAPSPageMenu.h"

@interface CreditBureauViewController : UIViewController <CAPSPageMenuDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;

- (IBAction)openDrawer:(id)sender;

@end
