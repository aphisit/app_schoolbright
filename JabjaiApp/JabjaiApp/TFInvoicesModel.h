//
//  TFInvoicesModel.h
//  JabjaiApp
//
//  Created by Mac on 5/10/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TFInvoicesModel : NSObject

@property (nonatomic) double amount;
@property (nonatomic) int paymentGroupId;
@property (nonatomic, strong) NSString *paymentDate;





//@property (assign, nonatomic) long long invoiceID;
//@property (nonatomic) int status;
//@property (nonatomic) float totalPrice;






//@property (nonatomic) float price;
//@property (strong, nonatomic) NSMutableArray *product;
//@property (strong, nonatomic) NSDate *dateIssue;
//@property (strong, nonatomic) NSDate *dateDue;
//@property (strong, nonatomic) NSString *schoolName;

@end

NS_ASSUME_NONNULL_END
