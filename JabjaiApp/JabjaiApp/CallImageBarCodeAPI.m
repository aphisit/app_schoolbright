//
//  CallImageBarCodeAPI.m
//  JabjaiApp
//
//  Created by toffee on 7/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CallImageBarCodeAPI.h"
#import "APIURL.h"
#import "Utils.h"

@implementation CallImageBarCodeAPI{
    NSInteger connectCounter;
    NSString *URLString;
}
-(void)call:(long long)high width:(long long)width userId:(long long)userId generateCode:(NSString *)generateCode schoolid:(long long)schoolid{
    connectCounter = 0;
    [self getImageBarcode:high width:width userId:userId generateCode:generateCode schoolid:schoolid];
}


//BarCode
- (void)getImageBarcode:(long long)high width:(long long)width userId:(long long)userId  generateCode:(NSString *)generateCode schoolid:(long long)schoolid{
    if ([generateCode isEqualToString:@"barcode"]) {
         URLString = [APIURL getImageBarcodeString:high width:width userId:userId schoolid:schoolid];
    }else{
         URLString = [APIURL getImageQrcodeString:high width:width userId:userId schoolid:schoolid];
    }
   
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getImageBarcode:high width:width userId:userId generateCode:generateCode schoolid:schoolid];
                
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            
            NSString *imageString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
          
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getImageBarcode:high width:width userId:userId generateCode:generateCode schoolid:schoolid];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
           
            else {
              
                connectCounter = 0;
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callImageBarCodeAPI:data:generateCode:success:)]) {
                    [self.delegate callImageBarCodeAPI:self data:imageString generateCode:generateCode success:YES];
                }
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callImageBarCodeAPI:data:generateCode:success:)]) {
                [self.delegate callImageBarCodeAPI:self data:nil generateCode:nil success:NO];
            }
        }
        
        
    }
     ];
}

@end
