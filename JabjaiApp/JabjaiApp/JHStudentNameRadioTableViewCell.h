//
//  JHStudentNameRadioTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 11/21/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@class JHStudentNameRadioTableViewCell;

@protocol JHStudentNameRadioTableViewCellDelegate <NSObject>

- (void)onPressRadioButton:(JHStudentNameRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index;

@end
@interface JHStudentNameRadioTableViewCell : UITableViewCell
@property (weak, nonatomic) id<JHStudentNameRadioTableViewCellDelegate> delegate;

@property (nonatomic) NSInteger index;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLastname;
@property (weak, nonatomic) IBOutlet UILabel *runNumber;
@property (weak, nonatomic) IBOutlet UIView *shadowViewCell;

@property (weak, nonatomic) IBOutlet UIButton *radioButton;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;

- (void)setRadioButtonSelected;
- (void)setRadioButtonSelectedWithType:(int) type;
- (void)setRadioButtonClearSelected;

- (IBAction)actionPressRadioButton:(id)sender;
@end
