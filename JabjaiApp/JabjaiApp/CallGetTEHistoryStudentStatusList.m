//
//  CallGetTEHistoryStudentStatusList.m
//  JabjaiApp
//
//  Created by toffee on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetTEHistoryStudentStatusList.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetTEHistoryStudentStatusList () {
    NSInteger connectCounter;
    NSMutableArray<RPCheckNameFlagAttendanceModel *> *studentStatusModelArray;
}

@end

@implementation CallGetTEHistoryStudentStatusList
- (instancetype)init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(long long)schoolId classroomId:(long long)classroomId  date:(NSDate *)date {
    [self getDataFromServer:schoolId classroomId:classroomId  date:date];
}

- (void)getDataFromServer:(long long)schoolId classroomId:(long long)classroomId  date:(NSDate *)date {
    
    NSString *urlString = [APIURL getTEHistoryStudentStatusListWithSchoolId:schoolId classroomId:classroomId  date:date];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataFromServer:schoolId classroomId:classroomId  date:date];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId classroomId:classroomId  date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId classroomId:classroomId  date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(studentStatusModelArray != nil) {
                    [studentStatusModelArray removeAllObjects];
                    studentStatusModelArray = nil;
                }
                
                studentStatusModelArray = [[NSMutableArray alloc] init];
                
                if(returnedArray.count > 0) {
                    //NSDictionary *dict = [returnedArray objectAtIndex:0];
                    NSArray *dataArray = returnedArray;
                    //NSArray *dataArray = [dict objectForKey:@"ScheduleScan4Level2"];
                    
                    if(dataArray != nil) {
                        for(int i=0; i<dataArray.count; i++) {
                            NSDictionary *dataDict = [dataArray objectAtIndex:i];
                            NSMutableString *studentPic,*studentId;
                            NSString *studentName ;
                            NSInteger studentState = 0;
                            if ([[UserData getChangLanguage] isEqualToString:@"th"]) {
                                if(![[dataDict objectForKey:@"studentname"] isKindOfClass:[NSNull class]]) {
                                    studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentname"]];
                                }
                                else {
                                    studentName = [[NSMutableString alloc] initWithString:@""];
                                }
                            }else{
                                if(![[dataDict objectForKey:@"studentnameEN"] isKindOfClass:[NSNull class]]) {
                                    studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentnameEN"]];
                                    if ([studentName isEqualToString:@" "]) {
                                        studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentname"]];
                                    }
                                }
                                else {
                                    studentName = [[NSMutableString alloc] initWithString:@""];
                                }
                                
                            }
                            
                            
                            
                            NSInteger status = [[dataDict objectForKey:@"status"] integerValue];
                            
                            if(![[dataDict objectForKey:@"pic"] isKindOfClass:[NSNull class]]) {
                                studentPic = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"pic"]];
                            }
                            else {
                                studentPic = [[NSMutableString alloc] initWithString:@""];
                            }
                            
                            if(![[dataDict objectForKey:@"studentid"] isKindOfClass:[NSNull class]]) {
                                studentId = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentid"]];
                                NSLog(@"xxxx1 %@",studentId);
                                
                            }
                            else {
                                studentId = [[NSMutableString alloc] initWithString:@""];
                                NSLog(@"xxxx2 %@",studentId);
                            }
                            
                            if(![[dataDict objectForKey:@"Student_State"] isKindOfClass:[NSNull class]]) {
                                studentState = [ [dataDict objectForKey:@"Student_State"]integerValue];
                            }
                            
                            
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) studentPic);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) studentId);
                            
                            RPCheckNameFlagAttendanceModel *model = [[RPCheckNameFlagAttendanceModel alloc] init];
//
                            [model setStudentName:studentName];
                            [model setStatus:status];
                            [model setStudentPic:studentPic];
                            [model setStudentId:studentId];
                            [model setStudentState:studentState];
                            
                            [studentStatusModelArray addObject:model];
                        }
                    }
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTEHistoryStudentStatusListAPI:data:success:)]) {
                    [self.delegate callGetTEHistoryStudentStatusListAPI:self data:studentStatusModelArray success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTEHistoryStudentStatusListAPI:data:success:)]) {
                [self.delegate callGetTEHistoryStudentStatusListAPI:self data:nil success:NO];
            }
        }
    }];
}

@end
