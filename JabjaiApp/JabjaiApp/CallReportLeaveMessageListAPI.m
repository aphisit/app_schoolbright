//
//  CallReportLeaveMessageListAPI.m
//  JabjaiApp
//
//  Created by toffee on 29/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallReportLeaveMessageListAPI.h"
#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"
@implementation CallReportLeaveMessageListAPI{
    NSInteger connectCounter;
    NSString *countLeave;
    NSMutableArray<NoticeReportDateMessageModel *> *leaveArray;
}
-(void)call:(long long)schoolId userId:(long long)userId{
    countLeave = @"0";
    [self getLeaveMessageList:schoolId userId:userId];
}
-(void)getLeaveMessageList:(long long)schoolId userId:(long long)userId{
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getUserLeaveDetailWithUserID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
       
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
           BOOL isFail = NO;
           if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
               
               if(error != nil) {
                   NSLog(@"An error occured : %@" , [error localizedDescription]);
               }
               else if(data == nil) {
                   NSLog(@"Data is null");
               }
               else {
                   NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                   NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
               }
               
               if(connectCounter < TRY_CONNECT_MAX) {
                   connectCounter++;
                   
                   NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                   [self getLeaveMessageList:schoolId userId:userId];
               }
               else {
                    isFail = YES;
                   connectCounter = 0;
               }
               
           }
           else {
               id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
               
               if(error != nil) {
                   NSLog(@"An error occured : %@", [error localizedDescription]);
                   
                   if(connectCounter < TRY_CONNECT_MAX) {
                       connectCounter++;
                       
                       NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                       [self getLeaveMessageList:schoolId userId:userId];
                   }
                   else {
                        isFail = YES;
                       connectCounter = 0;
                   }
               }
               else if(![returnedData isKindOfClass:[NSArray class]]) {
                   if(connectCounter < TRY_CONNECT_MAX) {
                       connectCounter++;
                       NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                       [self getLeaveMessageList:schoolId userId:userId];
                   }
                   else {
                        isFail = YES;
                       connectCounter = 0;
                   }
               }
               else{
                   NSArray *returnedArray = returnedData;
                   connectCounter = 0;
                   leaveArray = [[NSMutableArray alloc] init];
                   for (int i=0; i<returnedArray.count; i++) {
                       NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                       
                       long long letterID = [[dataDict objectForKey:@"letterId"]longLongValue];
                       
                       int status = [[dataDict objectForKey:@"status"] intValue];
                       
                       NSMutableString *countDate, *leaveType;
                       
                       if (![[dataDict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                           leaveType = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"type"]];
                       }
                       else{
                           leaveType = [[NSMutableString alloc] initWithString:@""];
                       }
                       
                       if (![[dataDict objectForKey:@"totalLeaveDay"] isKindOfClass:[NSNull class]]) {
                           countDate = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"totalLeaveDay"]];
                       }
                       
                       else{
                           countDate = [[NSMutableString alloc] initWithString:@""];
                       }
                       
                       NSDateFormatter *formatter = [Utils getDateFormatter];
                       [formatter setDateFormat:[Utils getXMLDateFormat]];
                       
                       NSDateFormatter *formatter2 = [Utils getDateFormatter];
                       [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                       
                       NSMutableString *leaveDate = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Date"]];
                       
                       CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveType);
                       CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveDate);
                       CFStringTrimWhitespace((__bridge CFMutableStringRef) countDate);
                       
                       NSRange dotRange = [leaveDate rangeOfString:@"."];
                       
                       NSDate *messageDate;
                       
                       if(dotRange.length != 0) {
                           messageDate = [formatter dateFromString:leaveDate];
                       }
                       else {
                           messageDate = [formatter2 dateFromString:leaveDate];
                       }
                       NoticeReportDateMessageModel *model = [[NoticeReportDateMessageModel alloc] init];
                       model.letterID = letterID;
                       model.leaveType = leaveType;
                       model.date = messageDate;
                       model.countLeave = countDate;
                       model.status = status;
                       countLeave = countDate;
                       [leaveArray addObject:model];
                   }
                   if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportLeaveMessageListAPI:data:countLeave:success:)]) {
                        [self.delegate callReportLeaveMessageListAPI:self data:leaveArray countLeave:countLeave success:YES];
                    }
               }
           }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportLeaveMessageListAPI:data:countLeave:success:)]) {
                           [self.delegate callReportLeaveMessageListAPI:self data:leaveArray countLeave:countLeave success:NO];
            }
        }
       }];
}
@end
