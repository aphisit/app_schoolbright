//
//  PurchasingProductModel.h
//  JabjaiApp
//
//  Created by mac on 4/20/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PurchasingListMenuModel.h"

@interface PurchasingProductModel : NSObject

@property (strong, nonatomic) NSString *paymentDate;
@property (assign, nonatomic) long long paymentId;
@property (assign, nonatomic) long long userId;
@property (strong, nonatomic) NSString *userType;
@property (strong, nonatomic) NSString *shopName;
@property (assign, nonatomic) NSInteger totalPrice;
@property (strong, nonatomic) NSMutableArray<PurchasingListMenuModel *>* listProduct;

@end
