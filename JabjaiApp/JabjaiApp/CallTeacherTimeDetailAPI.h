//
//  CallTeacherTimeDetailAPI.h
//  JabjaiApp
//
//  Created by toffee on 5/15/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CallTeacherTimeDetailAPI;

@protocol CallTeacherTimeDetailAPIDelegate <NSObject>

@optional
- (void)callTeacherTimeDetailAPI:(CallTeacherTimeDetailAPI *)classObj subjectName:(NSString*)subjectName subjectID:(NSString*)subjectID classRoom:(NSString*)classRoom timeIn:(NSString*)timeIn timeOut:(NSString*)timeOut success:(BOOL)success;


@end
@interface CallTeacherTimeDetailAPI : NSObject
@property (nonatomic, weak) id<CallTeacherTimeDetailAPIDelegate> delegate;
- (void)call:(long long)ScheduleId date:(NSDate*)date schoolid:(long long)schoolid;
@end
