//
//  ReportUnCheckListDataModel.m
//  JabjaiApp
//
//  Created by toffee on 21/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportUnCheckListDataModel.h"

@implementation ReportUnCheckListDataModel
@synthesize level = _level;
@synthesize teacherName = _teacherName;

- (void) setLevel:(NSString *)level{
    _level = level;
}

- (void) setTeacherName:(NSString *)teacherName{
    _teacherName = teacherName;
}

- (NSString*)getLevel{
    return _level;
}

- (NSString*)getTeacher{
    return _teacherName;
}

@end
