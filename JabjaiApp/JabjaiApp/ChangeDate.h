//
//  ChangeDate.h
//  JabjaiApp
//
//  Created by toffee on 12/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChangeDate : NSObject
+ (NSString *)changeDate:(NSString*)date;
@end
