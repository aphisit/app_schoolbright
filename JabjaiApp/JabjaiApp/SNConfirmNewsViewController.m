//
//  SNConfirmNewsViewController.m
//  JabjaiApp
//
//  Created by toffee on 10/6/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNConfirmNewsViewController.h"
#import "UserData.h"
//#import "AlertDialog.h"//
#import "AlertDialogConfirm.h"
#import "SNLevelStudentViewController.h"
#import "SNTimeOutViewController.h"
#import "APIURL.h"
#import "Utils.h"
#import <AFNetworking/AFNetworking.h>
#import "DBAttachment.h"
#import "UserInfoViewController.h"

@interface SNConfirmNewsViewController (){
    NSString *setTimeType;
    AlertDialogConfirm *alertDialog;
    NSString *jsonString;
    NSString *dateStr;
    NSInteger newsid;
    UIImage *ima;
    NSInteger num;
    NSBundle *myLangBundle;
}

@end
static NSString  *cellIdentifier = @"Cell";
@implementation SNConfirmNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
   
    NSString * resultConditions = [self.userAcceptArray componentsJoinedByString:@"\n"];
    self.userAccept.text = resultConditions;
    self.detail.text = self.newsDetail;
    
    dateStr = [NSString stringWithFormat: @"\" %@ \"", [Utils datePunctuateStringFormat:[NSDate date]]];
    //dateStr = [NSString stringWithFormat: @" %@ ", [Utils datePunctuateStringFormat:[NSDate date]]];

    if(self.sendTimeType == 0){
        self.daySends.text = [Utils dateInStringFormat:[NSDate date]];
    }else{
        self.daySends.text = [NSString stringWithFormat:@"%@ เวลา %@ น.",[Utils dateInStringFormat:self.dateSend],self.timesend];
    }
    NSLog(@"%@",[Utils dateInStringFormat:_dateSend]);

   
    self.collectionview.delegate = self;
    self.collectionview.dataSource = self;
    [self.fileCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SNIconFileCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.fileCollectionView.delegate = self;
    self.fileCollectionView.dataSource = self;
    
    //hidden collectionview
    if (self.imageNews.count < 1) {
        self.collectionview.hidden = YES;
    }
    if (self.fileArray.count < 1) {
        self.fileCollectionView.hidden = YES;
    }
    
    [self setLanguage];
    [self doSetDetailConfirm];
    
    // Do any additional setup after loading the view.
}
-(void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.contentview.frame.size.width, self.contentview.frame.size.height);
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.submitBtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.submitBtn.bounds;
    [self.submitBtn.layer insertSublayer:gradientNext atIndex:0];
}

-(void)setLanguage{
   
    self.headerSenderLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS",nil,myLangBundle,nil)];
    self.headerDeliveryLabel.text = [NSString stringWithFormat:@"%@ :",NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_DELIVERY",nil,myLangBundle,nil)];
    self.headerRecipientLabel.text =[NSString stringWithFormat:@"%@ :",NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_RECIPIENT",nil,myLangBundle,nil)];
    self.headerSendDateLabel.text = [NSString stringWithFormat:@"%@ :",NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_SENDDATE",nil,myLangBundle,nil)];
    self.headerDesciptionLabel.text = [NSString stringWithFormat:@"%@ :",NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_DESCIPTION",nil,myLangBundle,nil)];
    self.headerAttachFileLabel.text = [NSString stringWithFormat:@"%@ :",NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_ATTACH_FILE",nil,myLangBundle,nil)];
    self.headerAttachImageLabel.text = [NSString stringWithFormat:@"%@ :",NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_ATTACH_IMAGE",nil,myLangBundle,nil)];
    
    
    
    
    [self.submitBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_SENDNEWS_SUBMIT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialogConfirm alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.collectionview) {
        return [_imageNews count];
    }else{
        return self.fileArray.count;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    static NSString *cellIdentifier = @"Cell";
    if (collectionView == self.collectionview) {
        SNConfrimCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        cell.imageNews.tag = 200;
        cell.imageNews.image = [self.imageNews objectAtIndex:indexPath.row];
        
        return cell;
    }else{
        SNIconFileCollectionViewCell *fileCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        DBAttachment *attachment = self.fileArray[indexPath.row];
        CGFloat scale = [UIScreen mainScreen].scale;
        CGSize scaledThumbnailSize = CGSizeMake( 80.f * scale, 80.f * scale );
        [attachment loadThumbnailImageWithTargetSize:scaledThumbnailSize completion:^(UIImage *resultImage) {
            fileCell.fileimage.image = resultImage;
        }];
        
       
        return fileCell;
    }
   
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize defaultSize = CGSizeMake((self.collectionview.frame.size.width/3)-10, (self.collectionview.frame.size.height));
    return defaultSize;
}

- (void)doSetDetailConfirm{
    if(self.sendTimeType == 0){
        setTimeType = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_SENDNOW",nil,myLangBundle,nil);
    }else{
        setTimeType = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_SETTIMES",nil,myLangBundle,nil);
    }
    self.sendTimeTypes.text = setTimeType;
}

- (void)doSetParameter{
    NSLog(@"%@",self.newsDetail);
    self.setnewsDetail = self.newsDetail;
//    self.newsDetail = [self.newsDetail stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
//    self.newsDetail = [NSString stringWithFormat: @"\" %@ \"", self.newsDetail];
    
    NSMutableString *employeesid = [[NSMutableString alloc] initWithString:@"["];
    for(int i=0; i<self.employeesid.count; i++) {
        [employeesid appendFormat:@"%@", [self.employeesid objectAtIndex:i]];
        
        if(i+1 != self.employeesid.count) {
            [employeesid appendString:@","];
        }
    }
    [employeesid appendString:@"]"];
    //เก็บ id room
    NSMutableString *level2Id = [[NSMutableString alloc] initWithString:@"["];
    for(int i=0; i<self.level2id.count; i++) {
        [level2Id appendFormat:@"%@", [self.level2id objectAtIndex:i]];
        
        if(i+1 != self.level2id.count) {
            [level2Id appendString:@","];
        }
    }
    [level2Id appendString:@"]"];
    
    //เก็บ id studen
    NSMutableString *studenId = [[NSMutableString alloc] initWithString:@"["];
    for(int i=0; i<self.studentid.count; i++) {
        [studenId appendFormat:@"%@", [self.studentid objectAtIndex:i]];
        
        if(i+1 != self.studentid.count) {
            [studenId appendString:@","];
        }
    }
    [studenId appendString:@"]"];
    
    if (self.sendTimeType == 1) {
        self.scheduledTime = [NSString stringWithFormat: @"\" %@ %@\"", [Utils datePunctuateStringFormat:self.dateSend], self.timesend];
        self.time = [NSString stringWithFormat:@"\"%@\"",self.timesend];
    }else{
        self.scheduledTime = @"\"\"";
        self.time =  @"\"\"";
    }
   
    long long shcoolID = [UserData getSchoolId];
    NSInteger userID = [UserData getUserID] ;
   
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    jsonString = [[NSString alloc] initWithFormat:@"{\"schoolid\" : %lld , \"sendtimetype\" : %d , \"sendtype\" : %d , \"newstype\" : %d,\"sendgroup\" : %d,\"level2id\" : %@,\"studentid\" : %@,\"employeesid\" : %@,\"daysend\" : %@,\"scheduled_time\" : %@,\"timesend\" : %@,\"useradd\" : %d}", shcoolID, _sendTimeType , _sendType,_newsType, _sendGroup , level2Id, studenId, employeesid, dateStr, self.scheduledTime, self.time, userID];
     [self updateSNSendNews:jsonString imageArray:self.imageNews];
}

- (void)validateData {
      [self stopIndicator];
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_COMPLETED",nil,myLangBundle,nil);
        [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    [alertDialog  showDialogInView:self.view title:title message:message];
}

#pragma mark - insert data to server
- (void)updateSNSendNews:(NSString *)jsonString imageArray:(NSMutableArray *)imageArray{
    NSString *URLString = [APIURL getUpdateSendNewsPOST];
    NSError* error = nil;

    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *json = [[NSMutableDictionary alloc] init];
    json = [[NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableLeaves error:&error] mutableCopy];
    [json setObject:self.newsDetail forKey:@"newsdetail" ];
    NSLog(@"%@",error);
    static NSString *KEY_NEWSID = @"newsid";
    static NSString *KEY_SCHOOLID = @"schoolid";
    if(error != nil) {
        NSLog(@"%@", @"Parse JSON data failed");
    }
    else {
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        
        [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"responseObject = %@", responseObject);
            self->newsid = [[responseObject objectForKey:@"newsid"] integerValue];
            NSString *urlString = [APIURL getSNNewsPOSTURL];
            NSURL *url = [NSURL URLWithString:urlString];
            NSString *schoolID = [[NSString alloc] initWithFormat:@"%lld", (long long)[UserData getSchoolId]];
            NSString *newsID = [[NSString alloc] initWithFormat:@"%lld", (long long)self->newsid];
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            [parameters setObject:schoolID forKey:KEY_SCHOOLID];
            [parameters setObject:newsID forKey:KEY_NEWSID];
            
            if(imageArray.count > 0){
                /////send image News
                for (UIImage *ima in imageArray) {
                    if (ima != nil) {
                        NSData *adjestIma;
                        if (((unsigned long)[UIImageJPEGRepresentation(ima, 1.0) length]) > 500000) {
                            adjestIma = UIImageJPEGRepresentation(ima, 0.4);
                        }else{
                            adjestIma = UIImageJPEGRepresentation(ima, 1.0);
                        }
                        [Utils uploadImageFromURL:url data:adjestIma imageparameterName:@"image" imageFileName:[NSString stringWithFormat:@"%d",num++] mimeType:JPEG parameters:parameters withCompletionHandler:^(id responseObject, NSError *error) {
                        }];
                    }
                }
            }
            if(self.fileArray.count > 0){
                for (DBAttachment *model in self.fileArray) {
                    NSString *path = model.originalFileResource;
                    NSError* error = nil;
                    NSData *fileData = [NSData dataWithContentsOfFile:path options: 0 error: &error];
                    NSData * data;
                    if ([[NSFileManager defaultManager] fileExistsAtPath:path]){
                         data = [[NSFileManager defaultManager] contentsAtPath:path];
                    }
                    NSArray *typeArray = [model.fileName componentsSeparatedByString:@"."];
                    NSString *type = [typeArray objectAtIndex:typeArray.count-1];
                    [Utils uploadFileFromURL:url data:fileData imageparameterName:[typeArray objectAtIndex:0] imageFileName:[NSString stringWithFormat:@"%d",num++] mimeType:type parameters:parameters withCompletionHandler:^(id responseObject, NSError *error) {
                    }];
                }
            }
 
            if(error != nil) {
                NSLog(@"error");
            }
            else {
                NSLog(@"response = %@", responseObject);
                [self validateData];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error");
        }];
        
    }
    
}
#pragma mark - AlertDialogDelegate
- (void)onAlertDialogClose {
    
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
    

}
#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        self.submitBtn.enabled = NO;
        [self.indicator startAnimating];
    }
}
- (void)stopIndicator {
    self.submitBtn.enabled = YES;
    [self.indicator stopAnimating];
    
}
- (IBAction)moveBack:(id)sender {
    
    SNTimeOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
    viewController.sendTimeType = self.sendTimeType;
    viewController.sendType = self.sendType;
    viewController.newsType = self.newsType;
    viewController.sendGroup = self.sendGroup;
    viewController.newsDetail = self.setnewsDetail;
    viewController.level2id = self.level2id;
    viewController.studentid = self.studentid;
    viewController.employeesid = self.employeesid;
    viewController.dateSend = self.dateSend;
    viewController.timeSend = self.timesend;
    viewController.userAcceptArray = self.userAcceptArray;
    viewController.imagesArray = _imageNews;
    viewController.attachmentArray = self.fileArray;
    

    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (IBAction)confirmAction:(id)sender {
    _submitBtn.enabled = NO;
    [self showIndicator];
    
     [self doSetParameter];
    
    }
@end
