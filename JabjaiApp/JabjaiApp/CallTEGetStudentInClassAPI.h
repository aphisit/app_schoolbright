//
//  CallTEGetStudentInClassAPI.h
//  JabjaiApp
//
//  Created by toffee on 10/30/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAStudentStatusModel.h"
@class CallTEGetStudentInClassAPI;

@protocol CallTEGetStudentInClassAPIDelegate <NSObject>

@optional
- (void)callTEGetStudentInClassAPI:(CallTEGetStudentInClassAPI *)classObj data:(NSArray<TAStudentStatusModel *> *)data success:(BOOL)success;

@end
@interface CallTEGetStudentInClassAPI : NSObject
@property (nonatomic, weak) id<CallTEGetStudentInClassAPIDelegate> delegate;

- (void)call:(long long)schoolId classroomId:(long long)classroomId teacherId:(NSInteger)teacherId;



@end
