//
//  TECheckLateWithIdStudentViewController.h
//  JabjaiApp
//
//  Created by toffee on 1/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//
#import "TELevelClassViewController.h"
#import <UIKit/UIKit.h>
#import "CallTEGetDataStudentScanBarcodeAPI.h"
#import "CallTEConfirmScanerBarcode.h"
#import "ConfirmScanerLateViewController.h"
#import "AlertDialogConfirm.h"
#import "AlertDialog.h"
NS_ASSUME_NONNULL_BEGIN

@interface TECheckLateWithIdStudentViewController : UIViewController <CallTEGetDataStudentScanBarcodeAPIDelegate,ConfirmScanerLateViewControllerDelegate, CallTEConfirmScanerBarcodeDelegate, AlertDialogConfirmDelegate,AlertDialogDelegate>

@property (nonatomic,assign) NSInteger mode;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerCheckLateLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerClearBtn;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *moveBackBtn;

@property (weak, nonatomic) IBOutlet UITextField *idStudentTextField;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)moveBack:(id)sender;
- (IBAction)nextAction:(id)sender;
- (IBAction)numberButton:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
