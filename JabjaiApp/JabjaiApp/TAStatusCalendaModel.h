//
//  TAStatusCalendaModel.h
//  JabjaiApp
//
//  Created by Mac on 18/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TAStatusCalendaModel : NSObject
@property (nonatomic) long long schoolID;
@property (nonatomic) NSString *day;

- (void)setSchoolID:(long long)schoolID;
- (void)setDay:(NSString * _Nonnull)day;

- (long long)getSchoolID;
- (NSString *)getDay;

@end

NS_ASSUME_NONNULL_END
