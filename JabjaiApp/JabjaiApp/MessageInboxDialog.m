//
//  MessageInboxDialog.m
//  JabjaiApp
//
//  Created by mac on 11/24/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "MessageInboxDialog.h"
#import "Utils.h"
#import "UserData.h"
@interface MessageInboxDialog () {
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic) BOOL isShowing;

@end

@implementation MessageInboxDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dialogView.layer.masksToBounds = YES;
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10,10)].CGPath;
    self.headerView.layer.mask = maskLayer;
    self.headerView.layer.masksToBounds = YES;
    self.closeBtn.layer.masksToBounds = YES;
    self.dateFormatter = [Utils getDateFormatter];
    //[self.dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm 'น.'"];
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm 'น.'"];
        //[self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [self.dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
        //[self.timeFormatter setDateFormat:@"HH:mm"];
    }
    self.isShowing = NO;
    [self.closeBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_INFO_CLOSE",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm;
    //set color header
    [self.closeBtn layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.closeBtn.bounds;
    [self.closeBtn.layer insertSublayer:gradientConfirm atIndex:0];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

- (void)dismissDialog {
    
    [self removeDialogFromView];
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(messageInboxDialogClose)]) {
        [self.delegate messageInboxDialogClose];
    }
}

- (IBAction)closeDialog:(id)sender {
    
    [self dismissDialog];
}

-(void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message messageDate:(NSDate *)messageDate {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    [self.titleLabel setText:title];
    [self.messageLabel setText:message];
    
    NSArray *array = [[NSString stringWithFormat:@"%@", [self.dateFormatter stringFromDate:messageDate]] componentsSeparatedByString:@" "];
    [self.dateLabel setText:array[0]];
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.timeLabel setText:[array[1] stringByAppendingString:@" น."]];
    }else{
        [self.timeLabel setText:array[1]];
    }
    
    
//    [self.dateLabel setText:[self.dateFormatter stringFromDate:messageDate]];
    
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

-(BOOL)isDialogShowing {
    return self.isShowing;
}

@end
