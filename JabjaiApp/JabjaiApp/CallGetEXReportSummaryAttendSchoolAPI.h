//
//  CallGetEXReportSummaryAttendSchoolAPI.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXReportClassStatusModel.h"

@class CallGetEXReportSummaryAttendSchoolAPI;

@protocol CallGetEXReportSummaryAttendSchoolAPIDelegate <NSObject>

- (void)callGetEXReportSummaryAttendSchoolAPI:(CallGetEXReportSummaryAttendSchoolAPI *)classObj data:(NSArray<EXReportClassStatusModel *> *)data success:(BOOL)success;

@end

@interface CallGetEXReportSummaryAttendSchoolAPI : NSObject

@property (nonatomic, weak) id<CallGetEXReportSummaryAttendSchoolAPIDelegate> delegate;

- (void)call:(long long)schoolId date:(NSDate *)date;

@end
