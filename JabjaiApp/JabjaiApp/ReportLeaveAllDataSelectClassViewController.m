//
//  ReportLeaveAllDataSelectClassViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportLeaveAllDataSelectClassViewController.h"
#import "ReportListTeacherViewController.h"

#import "AlertDialog.h"
#import "UserData.h"
#import "Utils.h"

@interface ReportLeaveAllDataSelectClassViewController (){
   
    AlertDialog *alertDialog;
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classLevelIdArray;
    NSMutableArray *classroomStringArrray;
    TableListDialog *tableListDialog;
    
    
}
@property (weak, nonatomic) UIButton *nextButton;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) CFSCalendar *calendar;
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;
@end
static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";

@implementation ReportLeaveAllDataSelectClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    NSString *alertTitle = @"แจ้งเตือน";
    NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LEABEL_REPORT_NO_SERVICE",nil,[Utils getLanguage],nil);
    [self showFinishAlertDialogWithTitle:alertTitle message:alertMessage];
//    self.classLevelTextField.delegate = self;
//    self.classRoomTextField.delegate = self;
//     [self getSchoolClassLevel];

    [self createCalendar];
}

- (void)createCalendar {
    UIColor *grayColor;
    UIColor *headerTitleColor;
    UIColor *dodgerBlue;
    [self.calendarContentView layoutIfNeeded];
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    dodgerBlue = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0]; // Now Full
    self.calendarContentView.backgroundColor = grayColor;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    NSLog(@"H = %d",(int)self.calendarContentView.frame.size.height);
    [self.calendarContentView layoutIfNeeded];
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0,self.calendarContentView.frame.size.width, height)];
    calendar.dataSource = self;
    calendar.delegate = self;
    //calendar.swipeToChooseGesture.enabled = YES;
    calendar.backgroundColor = [UIColor whiteColor];
    //    calendar.appearance.titleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:22.0f];
    //    calendar.appearance.subtitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:22.0f];
    //    calendar.appearance.weekdayFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:22.0f];
    //calendar.appearance.headerTitleFont = [UIFont fontWithName:@"THSarabunNew-Bold" size:40.0f];
    
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
//    calendar.appearance.selectionColor = selectionColor;
//    calendar.appearance.todayColor = todayColor;
    // 04/05/2018
    calendar.appearance.todayColor = dodgerBlue;
    calendar.appearance.titleTodayColor = [UIColor whiteColor];
    calendar.appearance.todaySelectionColor = dodgerBlue;
    calendar.appearance.selectionColor = [UIColor clearColor];
    calendar.appearance.titleSelectionColor = [UIColor blackColor];
    calendar.appearance.borderSelectionColor = dodgerBlue;
    [self.calendarContentView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 34, 34);
    //previousButton.backgroundColor = [UIColor orangeColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    //[previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.calendarContentView addSubview:previousButton];
    self.previousButton = previousButton;
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    // nextButton.frame = CGRectMake(CGRectGetWidth(self.containerView.frame) - 95, 5, 95, 34);
    nextButton.frame = CGRectMake(self.view.frame.size.width - nextButton.frame.size.width - 50, 5, 34, 34);
    //nextButton.backgroundColor = [UIColor greenColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    //[nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.calendarContentView addSubview:nextButton];
    self.nextButton = nextButton;
   // [self getTopupMonthly:calendar.currentPage];
}
//- (void)getSchoolClassLevel {
//
//
//    if(self.callGetSchoolClassLevelAPI != nil) {
//        self.callGetSchoolClassLevelAPI = nil;
//    }
//    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
//    self.callGetSchoolClassLevelAPI.delegate = self;
//    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
//
//
//}
//
//#pragma mark - CallGetSchoolClassLevelAPIDelegate
//
//- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
//
//    if(success && data != nil) {
//       // [self stopIndicator];
//        _classLevelArray = data;
//        // clear array
//        if(classLevelStringArray != nil) {
//            [classLevelStringArray removeAllObjects];
//            classLevelStringArray = nil;
//        }
//        classLevelStringArray = [[NSMutableArray alloc] init];
//        for(int i=0; i<data.count; i++) {
//            SchoolLevelModel *model = [data objectAtIndex:i];
//            [classLevelStringArray addObject:[model getClassLevelName]];
//        }
//        NSLog(@"CLASSLAVEL = %@",classLevelStringArray);
//    }
//}
//
//- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
//    //[self showIndicator];
//    if(self.callGetSchoolClassroomAPI != nil) {
//        self.callGetSchoolClassroomAPI = nil;
//    }
//
//    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
//    self.callGetSchoolClassroomAPI.delegate = self;
//    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
//}
//#pragma mark - CallGetStudentClassroomAPIDelegate
//
//- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
//
//    if(success && data != nil) {
//        //[self stopIndicator];
//        _classroomArray = data;
//
//        // clear array
//        if(classroomStringArrray != nil) {
//            [classroomStringArrray removeAllObjects];
//            classroomStringArrray = nil;
//        }
//
//        classroomStringArrray = [[NSMutableArray alloc] init];
//
//        for(int i=0; i<data.count; i++) {
//            SchoolClassroomModel *model = [data objectAtIndex:i];
//            [classroomStringArrray addObject:[model getClassroomName]];
//        }
//    }
//}
//
//#pragma mark - Dialog
//- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
//
//    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
//        [tableListDialog dismissDialog];
//        tableListDialog = nil;
//    }
//
//    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
//    tableListDialog.delegate = self;
//    [tableListDialog showDialogInView:self.view dataArr:data];
//}
//- (void)showAlertDialogWithMessage:(NSString *)message {
//
//    if(alertDialog != nil && [alertDialog isDialogShowing]) {
//        [alertDialog dismissDialog];
//    }
//    else {
//        alertDialog = [[AlertDialog alloc] init];
//    }
//    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
//}
//
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//
//    if(textField.tag == 1) { // User press class level textfield
//
//        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
//            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
//        }
//        return NO;
//    }
//    else if(textField.tag == 2) { // User press classroom textfield
//
//        if(self.classLevelTextField.text.length == 0) {
//            NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
//            [self showAlertDialogWithMessage:alertMessage];
//        }
//        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
//
//            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
//        }
//
//        return NO;
//    }
//
//    return YES;
//}
//- (BOOL)validateData {
//
//    if(self.classLevelTextField.text.length == 0) {
//        NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
//        [self showAlertDialogWithMessage:alertMessage];
//
//        return NO;
//    }
//    else if(self.classRoomTextField.text.length == 0) {
//        NSString *alertMessage = @"กรุณาเลือกชั้นเรียน";
//        [self showAlertDialogWithMessage:alertMessage];
//
//        return NO;
//    }
//
//    return YES;
//}
//#pragma mark - TableListDialogDelegate
//- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
//
//    if([requestCode isEqualToString:classLevelRequestCode]) {
//
//        //        if(index == _selectedClassLevelIndex) {
//        //            return;
//        //        }
//        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
//        _selectedClassLevelIndex = index;
//
//        // Clear dependency text field values
//        self.classRoomTextField.text = @"";
//        _selectedClassroomIndex = -1;
//        _classLevelId = 0;
//
//        // get classroom data with class level
//        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
//        [self getSchoolClassroomWithClassLevelId:_classLevelId];
//
//    }
//    else if([requestCode isEqualToString:classroomRequestCode]) {
//        // self.level2Id = [[NSMutableArray alloc] init];
//
//        if(index == _selectedClassroomIndex) {
//            return;
//        }
//
//        self.classRoomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
//        _selectedClassroomIndex = index;
//        _classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
//        // addID Room
//        // [self.level2Id addObject:@(_classroomId)];
//
//    }
//
//}
//
//
//- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
//
//    if(alertDialog == nil) {
//        alertDialog = [[AlertDialog alloc] init];
//    }
//    else if(alertDialog != nil && [alertDialog isDialogShowing]) {
//        [alertDialog dismissDialog];
//    }
//
//    [alertDialog showDialogInView:self.view title:title message:message];
//}
//
- (void)showFinishAlertDialogWithTitle:(NSString *)title message:(NSString *)message {

    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else{
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}


- (void)onAlertDialogClose {
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}



- (IBAction)moveBack:(id)sender {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
