//
//  CallGMMenuAPI.h
//  JabjaiApp
//
//  Created by toffee on 4/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuListModel.h"


@class CallGMMenuAPI;
@protocol CallGMMenuAPIDelegate <NSObject>

- (void)callGMMenuAPI:(CallGMMenuAPI *)classObj data:(NSArray<MenuListModel *> *)data success:(BOOL)success;
@end
@interface CallGMMenuAPI : NSObject

@property (nonatomic, weak) id<CallGMMenuAPIDelegate> delegate;
- (void)call:(long long)schoolId shopId:(long long)shopId;

@end
