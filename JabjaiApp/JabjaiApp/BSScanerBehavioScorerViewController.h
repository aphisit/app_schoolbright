//
//  BSScanerBehavioScorerViewController.h
//  JabjaiApp
//
//  Created by toffee on 24/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Utils.h"
#import "CallBSGetDataStudentScanBehaviorAPI.h"
#import "AlertDialogConfirm.h"
#import "AlertDialog.h"
#import "ConfirmScanerLateViewController.h"
#import "BSSelectClassLevelViewController.h"
#import "BSSelectBehaviorScoreViewController.h"
#import "BSScanerStudentDataDialog.h"
NS_ASSUME_NONNULL_BEGIN

@interface BSScanerBehavioScorerViewController : UIViewController<AVCaptureMetadataOutputObjectsDelegate,CallBSGetDataStudentScanBehaviorAPIDelegate,AlertDialogConfirmDelegate,AlertDialogDelegate,ConfirmScanerLateViewControllerDelegate,BSScanerStudentDataDialogDelegate>

//Global variables
@property (nonatomic) int status;
@property (assign,nonatomic) int  mode;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;

// BSSelectStudentsViewController
@property (strong, nonatomic) NSArray<BSSelectedStudent *> *selectedStudentArray;
// BSSelectBehaviorScoreViewController
@property (strong, nonatomic) NSArray<BehaviorScoreModel *> *behaviorScoreArray;

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (strong, nonatomic) IBOutlet UIView *viewforCamera;
@property (weak, nonatomic) IBOutlet UIView *headerView;

-(BOOL)startReading;
-(void)stopReading;
-(IBAction)moveAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
