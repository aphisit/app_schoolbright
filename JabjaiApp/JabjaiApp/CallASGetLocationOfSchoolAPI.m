//
//  CallASGetLocationOfSchoolAPI.m
//  JabjaiApp
//
//  Created by Mac on 11/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallASGetLocationOfSchoolAPI.h"
#import "APIURL.h"
#import "Utils.h"
@implementation CallASGetLocationOfSchoolAPI{
    NSInteger connectCounter;
    ACLocationModel *model;
}

- (void)call:(long long)schoolID studentID:(long long)studentID{
    connectCounter = 0;
    [self getLocation:schoolID studentID:studentID];
}
- (void)getLocation:(long long)schoolID studentID:(long long)studentID{
    NSString *URLString = [APIURL getLocationSchool:schoolID studentID:studentID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }

            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;

                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getLocation:schoolID studentID:studentID];

            }
            else {
                isFail = YES;
                self->connectCounter = 0;
            }

        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getLocation:schoolID studentID:studentID];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
                
            }
            else {
                NSDictionary *dic = returnedData;
                double laticude = 0.0,longticude = 0.0;
                int statusAccept = 0, statusLocation = 0, statusIsActive = 0;
                
                
                if (![[dic objectForKey:@"data"] isKindOfClass:[NSNull class]]) {
                    
                    
                    if (![dic[@"data"][@"latitude"] isKindOfClass:[NSNull class]]) {
                       // term = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"trem"]];
                        laticude = [returnedData[@"data"][@"latitude"] doubleValue];
                    }
                    else{
                        laticude = 0.0;
                    }
                    
                    if (![dic[@"data"][@"latitude"] isKindOfClass:[NSNull class]]) {
                        longticude = [returnedData[@"data"][@"longitude"] doubleValue];
                    }else{
                        longticude = 0.0;
                    }
                    
                    if (![dic[@"data"][@"isCall"] isKindOfClass:[NSNull class]]) {
                        statusAccept = [dic[@"data"][@"isCall"] intValue];
                    }else{
                        statusAccept = 0;
                    }
                    
                    if (![dic[@"data"][@"isLocation"] isKindOfClass:[NSNull class]]) {
                        statusLocation = [dic[@"data"][@"isLocation"] intValue];
                    }else{
                        statusLocation = 0;
                    }
                    
                    if (![dic[@"data"][@"isActive"] isKindOfClass:[NSNull class]]) {
                        statusIsActive = [dic[@"data"][@"isActive"] intValue];
                    }else{
                        statusIsActive = 0;
                    }
                }
                
                self->model = [[ACLocationModel alloc] init];
                self->model.latitude = laticude;
                self->model.longitude = longticude;
                self->model.statusAccept = statusAccept;
                self->model.statusLacation = statusLocation;
                self->model.statusIsActive = statusIsActive;
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callASGetLocationOfSchoolAPI:data:success:)]) {
                    [self.delegate callASGetLocationOfSchoolAPI:self data:self->model success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callASGetLocationOfSchoolAPI:data:success:)]) {
                [self.delegate callASGetLocationOfSchoolAPI:self data:self->model success:NO];
            }
        }
     
        
    }
     ];
}
@end
