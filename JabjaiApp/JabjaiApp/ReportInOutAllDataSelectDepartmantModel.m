//
//  ReportInOutAllDataSelectDepartmantModel.m
//  JabjaiApp
//
//  Created by toffee on 15/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutAllDataSelectDepartmantModel.h"

@implementation ReportInOutAllDataSelectDepartmantModel
@synthesize departmantName = _departmantName;
@synthesize departmantId = _departmantId;
@synthesize amountDepartmant = _amountDepartmant;
@synthesize status = _status;

- (void) setDepartmantId:(long long)departmantId{
    _departmantId = departmantId;
}

- (void) setDepartmantName:(NSString *)departmantName{
    _departmantName = departmantName;
}

- (void) setAmountStudent:(NSInteger)amountStudent{
    _amountDepartmant = amountStudent;
}

- (void) setStatus:(NSDictionary *)status{
    _status = status;
}

- (NSString*)getDepartmantName{
    return _departmantName;
}

- (long long)getDepartmantId{
    return _departmantId;
}

- (NSInteger)getAmountDepartmant{
    return _amountDepartmant;
}

- (NSDictionary*)getStatus{
    return _status;
}
@end
