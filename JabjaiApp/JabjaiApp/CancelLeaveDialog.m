//
//  CancelLeaveDialog.m
//  JabjaiApp
//
//  Created by mac on 5/18/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CancelLeaveDialog.h"
#import "EZYGradientView.h"
#import "Utils.h"

@interface CancelLeaveDialog (){
    
    BOOL isShowing;
    EZYGradientView *acceptGradient, *declineGradient;
    
    UIColor *jungleGreen, *downy, *salmon, *macaroniAndCheese;
    
}

@end

@implementation CancelLeaveDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    // Color
    jungleGreen = [UIColor colorWithRed:0.18 green:0.76 blue:0.47 alpha:1.0]; //Accept
    downy = [UIColor colorWithRed:0.41 green:0.83 blue:0.63 alpha:1.0];
    salmon = [UIColor colorWithRed:0.99 green:0.56 blue:0.44 alpha:1.0]; // Decline
    macaroniAndCheese = [UIColor colorWithRed:0.99 green:0.76 blue:0.52 alpha:1.0];

    
    /// Accept Gradient
    acceptGradient = [[EZYGradientView alloc] init];
    acceptGradient.frame = CGRectMake(0, 0, self.acceptBackGround.frame.size.width, self.acceptBackGround.frame.size.height);
    acceptGradient.firstColor = jungleGreen;
    acceptGradient.secondColor = downy;
    acceptGradient.angleº = 90;
    acceptGradient.colorRatio = 0.5;
    acceptGradient.fadeIntensity = 1;
    acceptGradient.isBlur = YES;
    acceptGradient.blurOpacity = 0.5;
    [self.acceptBackGround insertSubview:acceptGradient atIndex:0];
    
    // Decline Gradient
    declineGradient = [[EZYGradientView alloc] init];
    declineGradient.frame = CGRectMake(0, 0, self.declineBackGround.frame.size.width, self.declineBackGround.frame.size.height);
    declineGradient.firstColor = salmon;
    declineGradient.secondColor = macaroniAndCheese;
    declineGradient.angleº = 90;
    declineGradient.fadeIntensity = 1;
    declineGradient.isBlur = YES;
    declineGradient.blurOpacity = 0.5;
    [self.declineBackGround insertSubview:declineGradient atIndex:0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm,*gradientCancel;
    //set color header
    [self.btnConfirm layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.btnConfirm.bounds;
    [self.btnConfirm.layer insertSublayer:gradientConfirm atIndex:0];
    
    //set color button next
    [self.btnCancel layoutIfNeeded];
    gradientCancel = [Utils getGradientColorStatus:@"red"];
    gradientCancel.frame = self.btnCancel.bounds;
    [self.btnCancel.layer insertSublayer:gradientCancel atIndex:0];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

- (BOOL)isDialogShowing {
    return isShowing;
}

- (void)showDialogInView:(UIView *)targetView message:(NSString *)message{
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    [self.messageLabel setText:message];
    
    isShowing = YES;
    
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onCancelConfirmDialog:confirm:)]) {
        [self.delegate onCancelConfirmDialog:self confirm:NO];
    }
}

- (IBAction)actionConfirm:(id)sender {
    
    [self removeDialogFromView];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onCancelConfirmDialog:confirm:)]) {
        [self.delegate onCancelConfirmDialog:self confirm:YES];
    }
    
}

- (IBAction)actionCancel:(id)sender {
    
    [self dismissDialog];
    
}
@end
