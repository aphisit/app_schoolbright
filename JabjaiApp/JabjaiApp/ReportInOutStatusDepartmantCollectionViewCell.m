//
//  ReportInOutStatusDepartmantCollectionViewCell.m
//  JabjaiApp
//
//  Created by toffee on 6/3/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutStatusDepartmantCollectionViewCell.h"

@implementation ReportInOutStatusDepartmantCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) setBackgroundClick:(int)numberCell numberClick:(int)numberClick{
    if (numberCell == 0) {
        
        if (numberClick == numberCell) {
            self.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            self.clockImage.image = [UIImage imageNamed:@"ic_allwhite"];
            self.statusLabel.textColor = [UIColor whiteColor];
            self.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            self.clockImage.image = [UIImage imageNamed:@"ic_allgray"];
            self.backgroundColor.backgroundColor = [UIColor clearColor];
            self.statusLabel.textColor = [UIColor grayColor];
            self.amountLabel.textColor = [UIColor grayColor];
            
        }
        
        
    }
    else if (numberCell == 1) {
        
        if (numberClick == numberCell) {
            self.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            self.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            self.statusLabel.textColor = [UIColor whiteColor];
            self.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            self.clockImage.image = [UIImage imageNamed:@"ic_clockgreen"];
            self.backgroundColor.backgroundColor = [UIColor clearColor];
            self.statusLabel.textColor = [UIColor colorWithRed:0.24 green:0.79 blue:0.53 alpha:1.0];
            self.amountLabel.textColor = [UIColor colorWithRed:0.24 green:0.79 blue:0.53 alpha:1.0];
        }
    }
    else if (numberCell == 2) {
        
        
        if (numberClick == numberCell) {
            self.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            self.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            self.statusLabel.textColor = [UIColor whiteColor];
            self.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            self.clockImage.image = [UIImage imageNamed:@"ic_clockyellow"];
            self.backgroundColor.backgroundColor = [UIColor clearColor];
            self.statusLabel.textColor = [UIColor colorWithRed:1.00 green:0.66 blue:0.15 alpha:1.0];
            self.amountLabel.textColor = [UIColor colorWithRed:1.00 green:0.66 blue:0.15 alpha:1.0];
        }
    }
    else if (numberCell == 3) {
        if (numberClick == numberCell) {
            self.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            self.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            self.statusLabel.textColor = [UIColor whiteColor];
            self.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            self.clockImage.image = [UIImage imageNamed:@"ic_clockred"];
            self.backgroundColor.backgroundColor = [UIColor clearColor];
            self.statusLabel.textColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.39 alpha:1.0];
            self.amountLabel.textColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.39 alpha:1.0];
        }
    }
    else if (numberCell == 4) {
        
        if (numberClick == numberCell) {
            self.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            self.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            self.statusLabel.textColor = [UIColor whiteColor];
            self.amountLabel.textColor = [UIColor whiteColor];
            
        }else{
            self.clockImage.image = [UIImage imageNamed:@"ic_clockviolet"];
            self.backgroundColor.backgroundColor = [UIColor clearColor];
            self.statusLabel.textColor = [UIColor colorWithRed:0.88 green:0.40 blue:1.00 alpha:1.0];
            self.amountLabel.textColor = [UIColor colorWithRed:0.88 green:0.40 blue:1.00 alpha:1.0];
        }
    }
    else if (numberCell == 5) {
        
        if (numberClick == numberCell) {
            self.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            self.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            self.statusLabel.textColor = [UIColor whiteColor];
            self.amountLabel.textColor = [UIColor whiteColor];
        }else{
            self.clockImage.image = [UIImage imageNamed:@"ic_clockpurple"];
            self.backgroundColor.backgroundColor = [UIColor clearColor];
            self.statusLabel.textColor = [UIColor colorWithRed:0.60 green:0.00 blue:0.75 alpha:1.0];
            self.amountLabel.textColor = [UIColor colorWithRed:0.60 green:0.00 blue:0.75 alpha:1.0];
        }
    }
    else if (numberCell == 6){
        
        if (numberClick == numberCell) {
            self.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            self.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            self.statusLabel.textColor = [UIColor whiteColor];
            self.amountLabel.textColor = [UIColor whiteColor];
        }else{
            self.clockImage.image = [UIImage imageNamed:@"ic_clockother"];
            self.backgroundColor.backgroundColor = [UIColor clearColor];
            self.statusLabel.textColor = [UIColor colorWithRed:0.49 green:0.32 blue:0.63 alpha:1.0];
            self.amountLabel.textColor = [UIColor colorWithRed:0.49 green:0.32 blue:0.63 alpha:1.0];
        }
    }
    else if (numberCell == 7){
        
        if (numberClick == numberCell) {
            self.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
            self.clockImage.image = [UIImage imageNamed:@"ic_clockwhite"];
            self.statusLabel.textColor = [UIColor whiteColor];
            self.amountLabel.textColor = [UIColor whiteColor];
        }else{
            self.clockImage.image = [UIImage imageNamed:@"ic_clockorange"];
            self.backgroundColor.backgroundColor = [UIColor clearColor];
            self.statusLabel.textColor = [UIColor colorWithRed:0.97 green:0.58 blue:0.11 alpha:1.0];
            self.amountLabel.textColor = [UIColor colorWithRed:0.97 green:0.58 blue:0.11 alpha:1.0];
        }
    }

}

@end
