//
//  TEReportCalendarViewController.h
//  JabjaiApp
//
//  Created by toffee on 11/15/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//
@import ARSlidingPanel;
#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CFSCalendar.h"

#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"

#import "CallGetTEHistoryStudentStatusList.h"

#import "SlideMenuController.h"

//@class TEReportCalendarViewController;
//@protocol TEReportCalendarViewControllerDelegate <NSObject>
//
//@optional
//- (void)onSelectDate:(TEReportCalendarViewController *)classObj date:(NSDate *)date;
//
//@end

@interface TEReportCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, ARSPDragDelegate, ARSPVisibilityStateDelegate , UITableViewDataSource, UITableViewDelegate, CallGetTEHistoryStudentStatusListAPIDelegate, SlideMenuControllerDelegate>

//@property (weak, nonatomic) id<TEReportCalendarViewControllerDelegate> delegate;

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;

@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long selectedClassLevelId;
@property (nonatomic) long long selectedClassroomId;

@property (weak ,nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;


@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)moveBack:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailContraint;

@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;

- (IBAction)openDetail:(id)sender;

@end
