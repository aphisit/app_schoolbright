//
//  BSScanerStudentDataDialog.h
//  JabjaiApp
//
//  Created by toffee on 21/11/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "Utils.h"
#import "BSDataStudentScanerModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol  BSScanerStudentDataDialogDelegate<NSObject>
@optional
- (void) confirmResponse:(BSDataStudentScanerModel*)userData;
- (void) cancelResponse;

@end

@interface BSScanerStudentDataDialog : UIViewController
- (void)showDialogInView:(UIView *)targetView data:(BSDataStudentScanerModel *)data mode:(int)mode;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@property (retain, nonatomic) id<BSScanerStudentDataDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *studentPictureImg;
@property (weak, nonatomic) IBOutlet UILabel *headerStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerNameLable;
@property (weak, nonatomic) IBOutlet UILabel *headerLevelLable;

@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *classRoomLabel;
@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBnt;
@property (weak, nonatomic) IBOutlet UIButton *confirmBnt;

- (IBAction)confirmAction:(id)sender;
- (IBAction)cancelAction:(id)sender;


@end
NS_ASSUME_NONNULL_END
