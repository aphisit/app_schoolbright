//
//  CallRemoveDataSystemNotificationPOSTAPI.h
//  JabjaiApp
//
//  Created by Mac on 5/3/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CallRemoveDataSystemNotificationPOSTAPIDelegate <NSObject>
@optional
- (void)callRemoveDataSystemNotificationPOSTAPI:(BOOL)success;

@end
@interface CallRemoveDataSystemNotificationPOSTAPI : NSObject
@property (nonatomic, weak)id<CallRemoveDataSystemNotificationPOSTAPIDelegate> delegate;
- (void)call:(NSString *)jsonString;
@end

NS_ASSUME_NONNULL_END
