//
//  Utils.m
//  JabjaiApp
//
//  Created by mac on 10/28/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "Utils.h"
#import <AFNetworking/AFNetworking.h>
#import "UserData.h"

@interface Utils () {
}

@end

@implementation Utils

+ (void)downloadDataFromURL:(NSURL *)url withCompletionHandler:(void(^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
        [[NSOperationQueue mainQueue] addOperationWithBlock:^(void) {
            completionHandler(data, response, error);
        }];
        
    }];
    
    [task resume];
    
}

+ (void)uploadImageFromURL:(NSURL *)url data:(NSData *)data imageparameterName:(NSString *)imageParameterName imageFileName:(NSString *)imageFileName mimeType:(MIME_TYPE)mimeType parameters:(NSDictionary<NSString *, NSString *> *)parameters withCompletionHandler:(void(^)(id responseObject, NSError *error))completionHandler {
    
    NSString *mimeTypeStr;
    
    if(mimeType == JPEG) {
        mimeTypeStr = @"image/jpeg";
        
    }
    else {
        mimeTypeStr = @"image/png";
    }
    
    NSError *error;
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url.absoluteString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if(data != nil) {
            
            [formData appendPartWithFileData:data name:imageParameterName fileName:imageFileName mimeType:mimeTypeStr];
            //[formData appendPartWithFileURL:imageURL name:imageParameterName fileName:[imageURL lastPathComponent]  mimeType:mimeTypeStr error:nil];
        }
    } error:&error];
    
    if(error != nil) {
        completionHandler(nil, error);
        return;
    }

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager uploadTaskWithStreamedRequest:request progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        NSLog(@"Response %@", responseObject);
        completionHandler(responseObject, error);
        
    }];
    
    [uploadTask resume];
}

+ (void)uploadFileFromURL:(NSURL *)url data:(NSData *)data imageparameterName:(NSString *)imageParameterName imageFileName:(NSString *)imageFileName mimeType:(NSString *)mimeType parameters:(NSDictionary<NSString *, NSString *> *)parameters withCompletionHandler:(void(^)(id responseObject, NSError *error))completionHandler {
    
    NSString *mimeTypeStr;
    if([mimeType isEqualToString:@"pdf"]) {
        mimeTypeStr = @"application/pdf";
    }
    else if ([mimeType isEqualToString:@"docx"]){
        mimeTypeStr = @"application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    }
    else if ([mimeType isEqualToString:@"doc"]){
        mimeTypeStr = @"application/msword";
    }
    else if ([mimeType isEqualToString:@"xlsx"]){
        mimeTypeStr = @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    }
    else if ([mimeType isEqualToString:@"xls"]){
        mimeTypeStr = @"application/vnd.ms-excel";
    }
  

    NSError *error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url.absoluteString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if(data != nil) {
            [formData appendPartWithFileData:data name:imageParameterName fileName:imageFileName mimeType:mimeTypeStr];
        }
    } error:&error];
    
    if(error != nil) {
        completionHandler(nil, error);
        return;
    }
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager uploadTaskWithStreamedRequest:request progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        NSLog(@"Response %@", responseObject);
        completionHandler(responseObject, error);
        
    }];
    
    [uploadTask resume];
}

+(BOOL)isValidEmail:(NSString *)email {
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
}

+(BOOL)stringIsNumeric:(NSString *)str {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *number = [formatter numberFromString:str];
    return !!number; // If the string is not numeric, number will be nil
}

+(BOOL)isInteger:(NSString *)str {
    
    BOOL isNumeric = [Utils stringIsNumeric:str];
    
    if(isNumeric) {
        double number = [str doubleValue];
        double d = 1;
        
        if(modf(number, &d) == 0.0) {
            return YES;
        }
        else {
            return NO;
        }
    }
    else {
        return NO;
    }
    
}

+(BOOL)isIntegerWithDouble:(double)number {
    double d = 1;
    
    if(modf(number, &d) == 0.0) {
        return YES;
    }
    else {
        return NO;
    }
}

+(NSString *)encodeString:(NSString *)str {
    NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
    NSString *strEncoded = [str stringByAddingPercentEncodingWithAllowedCharacters:set];
    return strEncoded;
}

+(NSString *)getSeverDateTimeFormat {
    
    //2016-08-01T00:00:00
    NSString *datetimeformat = @"yyyy-MM-dd'T'HH:mm:ss";
    return datetimeformat;
    
}

+(NSString *)dateToServerDateFormat:(NSDate *)date {
    NSString *serverFormat = @"MM/dd/yyyy";
    NSString *serverDateStr = [self dateInStringFormat:date format:serverFormat];
    
    return serverDateStr;
}

+(NSString *)getXMLDateFormat {
    NSString *xmlDateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    return xmlDateFormat;
}

+(NSArray *)getThaiMonthRangeWithStartMonth:(NSInteger)startMonth endMonth:(NSInteger)endMonth {
    
   NSBundle *myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    if(startMonth < 1 || endMonth > 12) {
        return nil;
    }
    
    NSArray *thaiMonths = @[NSLocalizedStringFromTableInBundle(@"JANUARY",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"FEBRUARY",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"MARCH",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"APRIL",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"MAY",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"JUNE",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"JULY",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"AUGUST",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"SEPTEMBER",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"OCTOBER",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"NOVEMBER",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"DECEMBER",nil,myLangBundle,nil)];
    
    NSMutableArray *monthsRange = [[NSMutableArray alloc] init];
    
    NSInteger counter = startMonth - 1;
    
    do {
        [monthsRange addObject:[thaiMonths objectAtIndex:counter]];
        
        counter = (counter + 1) % 12;
        if (counter == 0) {
            counter = endMonth;
        }
    } while (counter != endMonth);
    
    return monthsRange;
}

+(NSInteger)getThaiMonthNumberWithName:(NSString *)month {
    NSArray *thaiMonths = @[@"มกราคม", @"กุมภาพันธ์", @"มีนาคม", @"เมษายน", @"พฤษภาคม", @"มิถุนายน", @"กรกฎาคม", @"สิงหาคม", @"กันยายน", @"ตุลาคม", @"พฤศจิกายน", @"ธันวาคม"];
    
    for(int i=0; i<thaiMonths.count; i++) {
        if([[thaiMonths objectAtIndex:i] isEqualToString:month]) {
            return i+1;
        }
    }
    
    return -1;
}

+(NSString *)dateInStringFormat:(NSDate *)date {
    
//    NSDateFormatter *dateFormatter = [self getDateFormatter];
//    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
//    NSString *strDate = [dateFormatter stringFromDate:date];
    NSString *strDate;
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    if ([[[UserData getChangLanguage] lowercaseString] isEqualToString:@"th"]) {
            strDate = [[NSString alloc] initWithFormat:@"%02i/%02i/%li", day, month, (long)year + 543];
    }else{
            strDate = [[NSString alloc] initWithFormat:@"%02i/%02i/%li", day, month, (long)year];
    }
    return strDate;
}

+(NSString *)datePunctuateStringFormat:(NSDate *)date{
    NSDateFormatter *dateFormatter = [self getDateFormatter];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    
    return strDate;
}

+(NSString *)dateInStringFormat:(NSDate *)date format:(NSString *)format {
    NSDateFormatter *dateFormatter = [self getDateFormatter];
    [dateFormatter setDateFormat:format];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}

+(NSDate *)parseServerDateStringToDate:(NSString *)dateStr {
    
    NSDateFormatter *formatter = [self getDateFormatter];
    [formatter setDateFormat:[Utils getXMLDateFormat]];
    
    NSDateFormatter *formatter2 = [self getDateFormatter];
    [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
    
    NSDate *parseDate;
    
    NSRange dotRange = [dateStr rangeOfString:@"."];
    
    if(dotRange.length != 0) {
        parseDate = [formatter dateFromString:dateStr];
    }
    else {
        parseDate = [formatter2 dateFromString:dateStr];
    }
    
    return parseDate;
}

+ (NSDateFormatter *)getDateFormatter {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    formatter.locale = locale;
    return formatter;
}

+ (NSCalendar *)getGregorianCalendar {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    return calendar;
}

+ (NSString *)getThaiDateFormatWithDate:(NSDate *)date {
    NSBundle *myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    NSString *dateStr;
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSArray *thaiMonths = @[NSLocalizedStringFromTableInBundle(@"JANUARY",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"FEBRUARY",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"MARCH",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"APRIL",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"MAY",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"JUNE",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"JULY",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"AUGUST",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"SEPTEMBER",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"OCTOBER",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"NOVEMBER",nil,myLangBundle,nil),
                            NSLocalizedStringFromTableInBundle(@"DECEMBER",nil,myLangBundle,nil)];
   
    
//    [[UserData getChangLanguage] lowercaseString];
    if ([[[UserData getChangLanguage] lowercaseString] isEqualToString:@"th"]) {
         dateStr = [[NSString alloc] initWithFormat:@"%02li %@ %li", day, thaiMonths[month - 1], year + 543];
    }else{
         dateStr = [[NSString alloc] initWithFormat:@"%02li %@ %li", day, thaiMonths[month - 1], year];
    }
    return dateStr;
}

+ (NSString *)getBuddhistEraFormatWithDate:(NSDate *)date {
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSString *dateStr = [[NSString alloc] initWithFormat:@"%02li/%02li/%li", day, month, year + 543];

    return dateStr;
}

+ (unsigned)parseIntFromData:(NSData *)data {
    NSString *dataDescription = [data description];
    NSString *dataAsString = [dataDescription substringWithRange:NSMakeRange(1, [dataDescription length] - 2)];
    
    unsigned intData = 0;
    NSScanner *scanner = [NSScanner scannerWithString:dataAsString];
    [scanner scanHexInt:&intData];
    
    return intData;
}

+ (NSString *)getStudentStatusJSON:(NSArray<TAStudentStatusModel *> *)studentStatusArray {
    
    if(studentStatusArray == nil || studentStatusArray.count == 0) {
        return @"{\"rootobject\":[]}";
    }
    
    NSMutableString *jsonString = [[NSMutableString alloc] initWithString:@"{\"rootobject\":["];
    
    BOOL hasData = NO;
    
    for(TAStudentStatusModel *model in studentStatusArray) {
        
        if([model getScanStatus] != -1) {
            long long userId = [model getUserId];
            int scanStatus = [model getScanStatus];
            
            NSString *jsonObjStr = [NSString stringWithFormat:@"{\"UserId\":%lld,\"scanstatus\":%i},", userId, scanStatus];
            
            [jsonString appendString:jsonObjStr];
            hasData = YES;
        }
        
    }
    
    if(hasData) {
        [jsonString deleteCharactersInRange:[jsonString rangeOfString:@"," options:NSBackwardsSearch]];
    }
    
    [jsonString appendString:@"]}"];
    
    return jsonString;
}

+ (NSString *)getStudentsBehaviorsScoreJSONWithSelectedStudentArray:(NSArray<BSSelectedStudent *> *)selectedStudentsArray selectedBehaviorsScoreArray:(NSArray<BehaviorScoreModel *> *)selectedBehaviorsScoreArray {
    
    NSMutableString *behaviorIds = [[NSMutableString alloc] initWithString:@"["];
    
    if(selectedBehaviorsScoreArray != nil) {
        
        for(int i=0; i<selectedBehaviorsScoreArray.count; i++) {
            
            BehaviorScoreModel *model = [selectedBehaviorsScoreArray objectAtIndex:i];
            
            [behaviorIds appendFormat:@"%lld", [model getBehaviorScoreId]];
            
            if(i+1 != selectedBehaviorsScoreArray.count) {
                [behaviorIds appendString:@","];
            }
        }
    }
    
    [behaviorIds appendString:@"]"];
    
    NSMutableString *studentIds = [[NSMutableString alloc] initWithString:@"["];
    
    if(selectedStudentsArray != nil) {
        
        for(int i=0; i<selectedStudentsArray.count; i++) {
            
            BSSelectedStudent *model = [selectedStudentsArray objectAtIndex:i];
            
            [studentIds appendFormat:@"%lld", [model getStudentId]];
            
            if(i+1 != selectedStudentsArray.count) {
                [studentIds appendString:@","];
            }
        }
    }
    
    [studentIds appendString:@"]"];
    
    NSString *jsonString = [[NSString alloc] initWithFormat:@"{\"studentId\" : %@, \"BehaviorsId\" : %@}", studentIds, behaviorIds];
    
    return jsonString;
}

+ (NSInteger)compareVersion:(NSString *)version1 version2:(NSString *)version2 {
    NSInteger compareResult = 0;
    BOOL isEqual = YES;
    
    NSArray *v1 = [version1 componentsSeparatedByString:@"."];
    NSArray *v2 = [version2 componentsSeparatedByString:@"."];
    
    NSInteger min = MIN(v1.count, v2.count);
    
    for(int i=0; i<min; i++) {
        NSInteger n1 = [[v1 objectAtIndex:i] integerValue];
        NSInteger n2 = [[v2 objectAtIndex:i] integerValue];
        
        if(n1 > n2) {
            compareResult = 1;
            isEqual = NO;
            break;
        }
        else if(n1 < n2) {
            compareResult = -1;
            isEqual = NO;
            break;
        }
    }
    
    if(isEqual) {
        if(v1.count > v2.count) {
            compareResult = 1;
        }
        else if(v1.count < v2.count) {
            compareResult = -1;
        }
        else {
            compareResult = 0;
        }
    }
    
    return compareResult;
}
+ (NSString *)getSendNewsJSONWithSelectedStudentArray:(NSMutableArray *)selectedTeacherArray  {
    
    NSMutableString *studentIds = [[NSMutableString alloc] initWithString:@"["];
    for(int i=0; i<selectedTeacherArray.count; i++) {
        [studentIds appendFormat:@"%@", [selectedTeacherArray objectAtIndex:i]];
        
        if(i+1 != selectedTeacherArray.count) {
            [studentIds appendString:@","];
        }
    }
    [studentIds appendString:@"]"];
    NSString *jsonString = [[NSString alloc] initWithFormat:@"{\"studentId\" : %@}", studentIds];
    return jsonString;
}


+ (NSString *)getThaiDateAcronymFormatWithDate:(NSDate *)date{
    NSString *dateStr;
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
  
    if ([[UserData getChangLanguage] isEqualToString:@"th"]) {
        NSArray *thaiMonthTh = @[@"ม.ค.", @"ก.พ.", @"มี.ค.", @"เม.ย.", @"พ.ค.", @"มิ.ย.", @"ก.ค.", @"ส.ค.", @"ก.ย.", @"ต.ค.", @"พ.ย.", @"ธ.ค."];
        dateStr = [[NSString alloc] initWithFormat:@"%02li %@ %li", day, thaiMonthTh[month - 1], year + 543];
    }else{
        NSArray *thaiMonthEn = @[@"Jan.", @"Feb.", @"Mar.", @"Apr.", @"May.", @"Jun.", @"Jul.", @"Aug.", @"Sep.", @"Oct.", @"Nov.", @"Dec."];
        dateStr = [[NSString alloc] initWithFormat:@"%02li %@ %li", day, thaiMonthEn[month - 1], year];
    }
    return dateStr;
}

+(CAGradientLayer*)getGradientColorHeader{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    UIColor *bitterSweet, *MacaroniAndCheese;
    bitterSweet = [UIColor colorWithRed:1.00 green:0.44 blue:0.37 alpha:1.0];
    MacaroniAndCheese = [UIColor colorWithRed:1.00 green:0.73 blue:0.48 alpha:1.0];
    gradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
    gradient.startPoint = CGPointMake(1.0, 0.5);
    gradient.endPoint = CGPointMake(0.0, 0.5);
    return gradient;
}

+(CAGradientLayer*)getGradientColorNextAtion{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    UIColor *bitterSweet, *MacaroniAndCheese;
    
    bitterSweet = [UIColor colorWithRed:0.16 green:0.72 blue:0.46 alpha:1.0];
    MacaroniAndCheese = [UIColor colorWithRed:0.44 green:0.82 blue:0.64 alpha:1.0];
    gradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
    gradient.startPoint = CGPointMake(1.0, 0.5);
    gradient.endPoint = CGPointMake(0.0, 0.5);
    return gradient;
}



+(CAGradientLayer*)getGradientColorStatus:(NSString *)stringColor{
   
     CAGradientLayer *gradient = [CAGradientLayer layer];
    if ([stringColor isEqualToString:@"green"]) {
        CAGradientLayer *greenGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        bitterSweet = [UIColor colorWithRed:0.44 green:0.82 blue:0.64 alpha:1.0];
        MacaroniAndCheese = [UIColor colorWithRed:0.16 green:0.72 blue:0.46 alpha:1.0];
       
        greenGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        greenGradient.startPoint = CGPointMake(0.0, 0.5);
        greenGradient.endPoint = CGPointMake(0.5, 0.0);
       
        gradient = greenGradient;
    }
    else if ([stringColor isEqualToString:@"yellow"]){
        CAGradientLayer *yellowGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        bitterSweet = [UIColor colorWithRed:1.00 green:0.72 blue:0.49 alpha:1.0];
        MacaroniAndCheese = [UIColor colorWithRed:1.00 green:0.53 blue:0.40 alpha:1.0];
        yellowGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        yellowGradient.startPoint = CGPointMake(0.0, 0.5);
        yellowGradient.endPoint = CGPointMake(1.0, 0.0);
        gradient = yellowGradient;
    }
    else if ([stringColor isEqualToString:@"red"]){
        CAGradientLayer *redGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        bitterSweet =  [UIColor colorWithRed:1.00 green:0.40 blue:0.40 alpha:1.0];
        MacaroniAndCheese = [UIColor colorWithRed:1.00 green:0.21 blue:0.21 alpha:1.0];
        redGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        redGradient.startPoint = CGPointMake(0.0, 0.5);
        redGradient.endPoint = CGPointMake(0.5, 0.0);
        gradient = redGradient;
    }
    else if ([stringColor isEqualToString:@"blue"]){
        CAGradientLayer *blueGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        bitterSweet =  [UIColor colorWithRed:0.14 green:0.66 blue:0.91 alpha:1.0];
        MacaroniAndCheese = [UIColor colorWithRed:0.01 green:0.41 blue:0.72 alpha:1.0];
        blueGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        blueGradient.startPoint = CGPointMake(0.0, 0.5);
        blueGradient.endPoint = CGPointMake(0.5, 0.0);
        gradient = blueGradient;
    }
    else if ([stringColor isEqualToString:@"purple"]){
        CAGradientLayer *purpleGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        bitterSweet =  [UIColor colorWithRed:0.68 green:0.31 blue:0.93 alpha:1.0];
        MacaroniAndCheese = [UIColor colorWithRed:0.45 green:0.07 blue:0.60 alpha:1.0];
        purpleGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        purpleGradient.startPoint = CGPointMake(0.0, 0.5);
        purpleGradient.endPoint = CGPointMake(0.5, 0.0);
        gradient = purpleGradient;
    }
    else if ([stringColor isEqualToString:@"pink"]){
        CAGradientLayer *pinkGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        bitterSweet =  [UIColor colorWithRed:0.86 green:0.47 blue:0.77 alpha:1.0];
        MacaroniAndCheese = [UIColor colorWithRed:0.71 green:0.20 blue:0.92 alpha:1.0];
        pinkGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        pinkGradient.startPoint = CGPointMake(0.0, 0.5);
        pinkGradient.endPoint = CGPointMake(0.5, 0.0);
        gradient = pinkGradient;
    }
    else if ([stringColor isEqualToString:@"gray"]){
        CAGradientLayer *grayGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        bitterSweet = [UIColor colorWithRed:0.55 green:0.55 blue:0.55 alpha:1.0];
        MacaroniAndCheese = [UIColor colorWithRed:0.37 green:0.37 blue:0.37 alpha:1.0];
        grayGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        grayGradient.startPoint = CGPointMake(0.0, 0.5);
        grayGradient.endPoint = CGPointMake(0.5, 0.0);
        gradient = grayGradient;
    }
    else if ([stringColor isEqualToString:@"brown"]){
        CAGradientLayer *grayGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        MacaroniAndCheese = [UIColor colorWithRed:0.66 green:0.25 blue:0.25 alpha:1.0];
        bitterSweet = [UIColor colorWithRed:0.65 green:0.38 blue:0.38 alpha:1.0];
        grayGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        grayGradient.startPoint = CGPointMake(0.0, 0.5);
        grayGradient.endPoint = CGPointMake(0.5, 0.0);
        gradient = grayGradient;
    }
    else if ([stringColor isEqualToString:@"purplesoft"]){
        CAGradientLayer *grayGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        MacaroniAndCheese = [UIColor colorWithRed:0.25 green:0.02 blue:0.71 alpha:1.0];
        bitterSweet = [UIColor colorWithRed:0.50 green:0.22 blue:0.87 alpha:1.0];
        grayGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        grayGradient.startPoint = CGPointMake(0.0, 0.5);
        grayGradient.endPoint = CGPointMake(0.5, 0.0);
        gradient = grayGradient;
    }
    else if ([stringColor isEqualToString:@"orange"]){
        CAGradientLayer *grayGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        MacaroniAndCheese = [UIColor colorWithRed:0.81 green:0.57 blue:0.00 alpha:1.0];
        bitterSweet = [UIColor colorWithRed:0.98 green:0.81 blue:0.39 alpha:1.0];
        grayGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        grayGradient.startPoint = CGPointMake(0.0, 0.5);
        grayGradient.endPoint = CGPointMake(0.5, 0.0);
        gradient = grayGradient;
    }
    else if ([stringColor isEqualToString:@"PurpleNeedle"]){
        CAGradientLayer *grayGradient = [CAGradientLayer layer];
        UIColor *bitterSweet, *MacaroniAndCheese;
        MacaroniAndCheese = [UIColor colorWithRed: 0.25 green: 0.02 blue: 0.71 alpha: 1.00];
        bitterSweet = [UIColor colorWithRed: 0.50 green: 0.22 blue: 0.87 alpha: 1.00];
        grayGradient.colors = @[(id)MacaroniAndCheese.CGColor, (id)bitterSweet.CGColor];
        grayGradient.startPoint = CGPointMake(0.0, 0.5);
        grayGradient.endPoint = CGPointMake(0.5, 0.0);
        gradient = grayGradient;
    }

    return gradient;
    
}
+ (NSBundle *)getLanguage{
    
   NSBundle  *myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    return myLangBundle;
}

+ (NSString *)getStatus:(NSInteger)number{
    NSString *status;
    NSBundle *myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    if (number == 0) {
        status = NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_ONTIME",nil,myLangBundle,nil);
    }else if (number == 3){
        status = NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_ABSENCE",nil,myLangBundle,nil);
    }else if (number == 4){
        status = NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_PERSONAL",nil,myLangBundle,nil);
    }else if (number == 5){
        status = NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_SICK",nil,myLangBundle,nil);
    }else if (number == 6){
        status = NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_EVENT",nil,myLangBundle,nil);
    }else if (number == 7){
        status =  NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_LEAVE",nil,myLangBundle,nil);
    }else if (number == 8){
        status = NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_HOLIDAY",nil,myLangBundle,nil);
    }
    return status;
}

+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}



@end
