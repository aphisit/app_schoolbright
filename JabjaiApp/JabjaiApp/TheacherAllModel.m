//
//  TheacherAllModel.m
//  JabjaiApp
//
//  Created by toffee on 9/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TheacherAllModel.h"

@interface TheacherAllModel ()

@end

@implementation TheacherAllModel

- (long long)getTheaCherId {
    return self.theaCherId;
}

- (NSString *)getTheaCherName {
    return self.theaCherName;
}

@end
