//
//  CallGetSchoolClassLevel.m
//  JabjaiApp
//
//  Created by mac on 7/4/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetSchoolClassLevelAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetSchoolClassLevelAPI () {
    NSInteger connectCounter;
    NSMutableArray<SchoolLevelModel *> *schoolLevels;
}
@end
@implementation CallGetSchoolClassLevelAPI
- (void)call:(long long)schoolId {
    connectCounter = 0;
    [self getStudentClassLevelWithSchoolId:schoolId];
}
#pragma mark - Get API Data
- (void)getStudentClassLevelWithSchoolId:(long long)schoolId {
    NSString *URLString = [APIURL getStudentClassLevelWithSchoolId:schoolId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            if(error != nil) {
                NSLog(@"An error occured : %@",[error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getStudentClassLevelWithSchoolId:schoolId];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }

        } else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentClassLevelWithSchoolId:schoolId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentClassLevelWithSchoolId:schoolId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(schoolLevels != nil) {
                    [schoolLevels removeAllObjects];
                    schoolLevels = nil;
                }
                
                schoolLevels = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long classLevelId = [[dataDict objectForKey:@"ID"] longLongValue];
                    
                    NSMutableString *classLevelName;
                    
                    
                    
                    
                    if ([[UserData getChangLanguage] isEqualToString:@"th"]) {
                        if(![[dataDict objectForKey:@"NameTH"] isKindOfClass:[NSNull class]]) {
                            classLevelName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"NameTH"]];
                        }
                        else {
                            classLevelName = [[NSMutableString alloc] initWithString:@""];
                        }
                    }else{
                        if(![[dataDict objectForKey:@"NameEN"] isKindOfClass:[NSNull class]]) {
                            classLevelName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"NameEN"]];
                        }
                        else {
                            classLevelName = [[NSMutableString alloc] initWithString:@""];
                        }
                    }
                    
                    
                    
                    
//                    if(![[dataDict objectForKey:@"NameTH"] isKindOfClass:[NSNull class]]) {
//                        classLevelName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"NameTH"]];
//                    }
//                    else {
//                        classLevelName = [[NSMutableString alloc] initWithString:@""];
//                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) classLevelName);
                    
                    SchoolLevelModel *model = [[SchoolLevelModel alloc] init];
                    model.classLevelId = classLevelId;
                    model.classLevelName = classLevelName;
                    
                    [schoolLevels addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetSchoolClassLevelAPI:data:success:)]) {
                    [self.delegate callGetSchoolClassLevelAPI:self data:schoolLevels success:YES];
                }
                
                return ;
                
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetSchoolClassLevelAPI:data:success:)]) {
                [self.delegate callGetSchoolClassLevelAPI:self data:nil success:NO];
            }
        }
        
    }];
}

@end
