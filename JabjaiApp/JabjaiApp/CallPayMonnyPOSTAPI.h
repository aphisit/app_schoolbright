//
//  CallPayMonnyPOSTAPI.h
//  JabjaiApp
//
//  Created by toffee on 7/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CallPayMonnyPOSTAPI;
@protocol CallPayMonnyPOSTAPIDelegate <NSObject>

- (void)callPayMonnyPOSTAPI:(CallPayMonnyPOSTAPI *)classObj status:(NSString*)status success:(BOOL)success;
@end
@interface CallPayMonnyPOSTAPI : NSObject
@property (nonatomic, weak) id<CallPayMonnyPOSTAPIDelegate> delegate;
- (void)call:(NSString*)jsonString shopId:(long long)shopId;

@end
