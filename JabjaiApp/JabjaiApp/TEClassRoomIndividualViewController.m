//
//  TEClassRoomIndividualViewController.m
//  JabjaiApp
//
//  Created by toffee on 10/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TEClassRoomIndividualViewController.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "MultilineAlertDialog.h"
#import "TELevelClassViewController.h"
#import "TESummaryViewController.h"
#import "Utils.h"

#import <SDWebImage/UIImageView+WebCache.h>

static NSInteger TA_MODE_CHECK = 1;
static NSInteger TA_MODE_EDIT = 2;
@interface TEClassRoomIndividualViewController (){
    NSArray<TEStatusModel *> *studentStatusHistoryForCopyArray;
    BOOL searchActive;
    NSInteger selectedStudentIndex;
    NSArray<TAStudentStatusModel *> *searchStudentStatusArray;
      // The dialog
    TEStatusDialogViewController *taStatusDialog;
    AlertDialog *alertDialog;
    MultilineAlertDialog *multilineAlertDialog;
    UIGestureRecognizer *searchBarCancelGesture;
    NSBundle *myLangBundle;
    UIColor *searchBarBG;
    CAGradientLayer *gradient;
}

@property (strong, nonatomic) CallTEGetStudentInClassAPI *callTEGetStudentInClassAPI;
@property (strong, nonatomic) CallGetPreviousAttendClassStatusAPI *callGetPreviousAttendClassStatusAPI;
@end
static NSString *cellIdentifier = @"Cell";
@implementation TEClassRoomIndividualViewController
@synthesize studentStatusArray = _studentStatusArray;
@synthesize studentImageArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.noStudents.hidden = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TAStudentStatusTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    searchBarBG = [UIColor colorWithRed:224/255.0 green:93/255.0 blue:37/255.0 alpha:1];
    self.searchBar.delegate = self;
    self.searchBar.translucent = NO;
    self.searchBar.opaque = NO;
    //self.searchBar.barTintColor = searchBarBG;
    self.searchBar.backgroundImage = [[UIImage alloc] init];
    selectedStudentIndex = -1;
    searchActive = NO;
    [self setLanguage];
    
    UIView *view = [self.searchBar.subviews objectAtIndex:0];
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *cancelButton = (UIButton *)subView;
            [cancelButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        }
    }
    NSLog(@"id = %lld",self.classroomId);
    if(_studentStatusArray == nil) {
         [self getStudentInClassroom];
    }
    
 
}
- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    //set color button next
    [self.nextBtn layoutIfNeeded];
    gradient = [Utils getGradientColorNextAtion];
    gradient.frame = self.nextBtn.bounds;
    [self.nextBtn.layer insertSublayer:gradient atIndex:0];
    
    [self.nextBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.nextBtn.layer setShadowOpacity:0.3];
    [self.nextBtn.layer setShadowRadius:10.0];
    [self.nextBtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setLanguage{
     myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[UserData getChangLanguage] ofType:@"lproj"]];
    _headTakeEvent.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ATTENDANCE",nil,myLangBundle,nil);
     _noStudentLabel.text = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_NOSTUDENT",nil,myLangBundle,nil);
    [_nextBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_FLAG_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];
    
}

#pragma mark - API Caller

- (void)getStudentInClassroom {
    [self showIndicator];
    if(self.callTEGetStudentInClassAPI != nil) {
        self.callTEGetStudentInClassAPI = nil;
    }
    self.callTEGetStudentInClassAPI = [[CallTEGetStudentInClassAPI alloc] init];
    self.callTEGetStudentInClassAPI.delegate = self;
    [self.callTEGetStudentInClassAPI call:[UserData getSchoolId] classroomId:_classroomId teacherId:[UserData getUserID]];
}
#pragma mark - API Delegate

- (void)callTEGetStudentInClassAPI:(CallTEGetStudentInClassAPI *)classObj data:(NSArray<TAStudentStatusModel *> *)data success:(BOOL)success{
    [self stopIndicator];
    if(success && data != nil) {
        _studentStatusArray = data;
        
        if(_studentStatusArray.count == 0){
            self.tableView.hidden = YES;
            self.noStudents.hidden = NO;

        }else{
            self.tableView.hidden = NO;
            self.noStudents.hidden = YES;
        }
        
        [self.tableView reloadData];
    }

}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"studenArray = %@",self.selectedStudentArray);
    if(searchActive) {
        if(searchStudentStatusArray != nil) {
            return searchStudentStatusArray.count;
        }
        else {
            return 0;
        }
    }
    if(self.studentStatusArray != nil) {
        NSLog(@"Count = %d",self.studentStatusArray.count);
        return self.studentStatusArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TAStudentStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    TAStudentStatusModel *model;
    cell.delegate = self;
    cell.index = indexPath.row;
    
    if(searchActive) {
        model = [searchStudentStatusArray objectAtIndex:indexPath.row];
    }
    else {
        model = [_studentStatusArray objectAtIndex:indexPath.row];
    }
    NSArray *array = [[NSString stringWithFormat:@"%@", [model getStudentName]] componentsSeparatedByString:@" "];
    NSMutableString *lastName = [[NSMutableString alloc] init];
    if (array.count>1) {
        for (int i = 0; i < array.count; i++) {
            if (i>0) {
                [lastName appendFormat:@"%@ ",array[i]];
            }
        }
        cell.titleLabel.text = array[0];
        cell.lastName.text = lastName;
    }else{
        cell.titleLabel.text = array[0];
        cell.lastName.text = @"";
    }
    
    [cell.imageStuden sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@", [model getStudentPic]]]] placeholderImage:nil options:SDWebImageRefreshCached
     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     if (image == nil || error) {
                                       cell.imageStuden.image = [UIImage imageNamed:@"ic_user_info"];
                                     }
     }];
    
    
   
    [cell.imageStuden layoutIfNeeded];
    cell.imageStuden.layer.cornerRadius = cell.imageStuden.frame.size.height /2;
    cell.imageStuden.layer.masksToBounds = YES;
    cell.imageStuden.layer.borderWidth = 0;
    
    cell.runNumber.text = [NSString stringWithFormat:@"%d.",indexPath.row+1];
    [cell updateStatus:[model scanStatus]];
    
    [cell.statusButton layoutIfNeeded];
    cell.statusButton.layer.cornerRadius = (NSInteger)cell.statusButton.frame.size.width/2;
    cell.statusButton.titleLabel.numberOfLines = 0;

    if([model authorized]) {
       
        
        //check status student
        if ([model getStudentState] == 1 ) {//Distribute
            [cell updateStatus:21];
            cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
            //cell.userInteractionEnabled = NO;
        }
        else if ([model getStudentState] == 2 ){//Resing
            [cell updateStatus:22];
            cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
           // cell.userInteractionEnabled = NO;
        }
        else if ([model getStudentState] == 3 ){//Suspended
            [cell updateStatus:23];
            cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
            //cell.userInteractionEnabled = NO;
        }
        else if ([model getStudentState] == 4 ){//Graduate
            [cell updateStatus:24];
            cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
            //cell.userInteractionEnabled = NO;
        }
        else if ([model getStudentState] == 5 ){//Lost contract
            [cell updateStatus:25];
            cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
            //cell.userInteractionEnabled = NO;
        }
        else if ([model getStudentState] == 6 ){//Retire
            [cell updateStatus:26];
            cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
            //cell.userInteractionEnabled = NO;
        }else{
            cell.backgroundColorView.backgroundColor = [UIColor whiteColor];
            //cell.userInteractionEnabled = YES;
        }
    }
    else {
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        //cell.userInteractionEnabled = NO;
    }
    
   
    [cell.backgroundColorView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.backgroundColorView.layer setShadowOpacity:0.3];
    [cell.backgroundColorView.layer setShadowRadius:3.0];
    [cell.backgroundColorView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 120; /* Device is iPad */
    }else{
        return 90;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedStudentIndex = indexPath.row;
    
    TAStudentStatusModel *model;
    if(searchActive) {
        model = [searchStudentStatusArray objectAtIndex:indexPath.row];
    }
    else {
        model = [_studentStatusArray objectAtIndex:indexPath.row];
    }
    
    if ([model getAuthorized]) {
        if ([model getStudentState] == 1 || [model getStudentState] == 2 || [model getStudentState] == 3 || [model getStudentState] == 4 || [model getStudentState] == 5 || [model getStudentState] == 6) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_CAN_NOT_CHANGE",nil,myLangBundle,nil);;
            [self showAlertDialogWithMessage:alertMessage];
        }else{
            [self showSelectStatusDialog];
        }
    }else{
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_CAN_NOT_CHANGE",nil,myLangBundle,nil);;
        [self showAlertDialogWithMessage:alertMessage];
    }
}

#pragma mark - TAStudentTableViewCellDelegate

- (void)onSelectStatusButton:(TAStudentStatusTableViewCell *)tableViewCell atIndex:(NSInteger)index {
    selectedStudentIndex = index;
    
    [self showSelectStatusDialog];
}
- (void)showSelectStatusDialog {
    
    if(taStatusDialog != nil) {
        
        if([taStatusDialog isDialogShowing]) {
            [taStatusDialog dismissDialog];
        }
        
        taStatusDialog = nil;
    }
    
    taStatusDialog = [[TEStatusDialogViewController alloc] init];
    taStatusDialog.delegate = self;
    [taStatusDialog showDialogInView:self.view mode:_mode];
    
}

- (void)taStatusDialog:(TEStatusDialogViewController *)dialog onSelectStatus:(TA_SCAN_STATUS)scanStatus {
    
    if(selectedStudentIndex >= 0) {
        TEStatusModel *model;
        
        if(searchActive) {
            model = [searchStudentStatusArray objectAtIndex:selectedStudentIndex];
        }
        else {
            model = [_studentStatusArray objectAtIndex:selectedStudentIndex];
        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedStudentIndex inSection:0];
        
        switch (scanStatus) {
            case TA_ONTIME:
                [model setScanStatus:0];
                break;
            case TA_LATE:
                [model setScanStatus:1];
                break;
            case TA_ABSENCE:
                [model setScanStatus:3];
                break;
            case TA_EVENT:
                [model setScanStatus:12];
                break;
            case TA_PERSONAL_LEAVE:
                [model setScanStatus:10];
                break;
            case TA_SICK_LEAVE:
                [model setScanStatus:11];
                break;
            default:
                [model setScanStatus:-1];
                break;
        }
        
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if(_studentStatusArray != nil) { // if select all
        
        BOOL hasChange = NO;
        
        for(int i=0; i<_studentStatusArray.count; i++) {
            TEStatusModel *model = [_studentStatusArray objectAtIndex:i];
            
            if([model getAuthorized]) {
                
                hasChange = YES;
                
                switch (scanStatus) {
                    case TA_ONTIME:
                        [model setScanStatus:0];
                        break;
                    case TA_LATE:
                        [model setScanStatus:1];
                        break;
                    case TA_ABSENCE:
                        [model setScanStatus:3];
                        break;
                    case TA_PERSONAL_LEAVE:
                        [model setScanStatus:10];
                        break;
                    case TA_SICK_LEAVE:
                        [model setScanStatus:11];
                        break;
                    case TA_EVENT:
                        [model setScanStatus:12];
                        break;
                    default:
                        [model setScanStatus:-1];
                        break;
                }
                
            }
        }
        
        if(hasChange) {
            [self.tableView reloadData];
        }
    }
    
}
#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBarCancelGesture = [UITapGestureRecognizer new];
    [searchBarCancelGesture addTarget:self action:@selector(backgroundTouched:)];
    [self.view addGestureRecognizer:searchBarCancelGesture];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if(searchBarCancelGesture != nil) {
        [self.view removeGestureRecognizer:searchBarCancelGesture];
        searchBarCancelGesture = nil;
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // called when text changes (including clear)
    
    searchActive = YES;
    
    if(searchText.length != 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"studentName contains[c] %@", searchText];
        searchStudentStatusArray = [_studentStatusArray filteredArrayUsingPredicate:predicate];
    }
    else {
        searchStudentStatusArray = _studentStatusArray;
    }
    
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // called when keyboard search button pressed
    
    [self.searchBar resignFirstResponder];
}
#pragma mark - Utils

- (void)backgroundTouched:(id)sender {
    [self.view endEditing:YES];
    [self.searchBar resignFirstResponder];
    
    if(searchActive && self.searchBar.text.length == 0) {
        searchActive = NO;
        [self.tableView reloadData];
    }
}
- (BOOL)checkHavePermission {
    if(_studentStatusArray == nil) {
        return YES;
    }
    
    for(TEStatusModel *model in _studentStatusArray) {
        if(![model getAuthorized]) {
            return NO;
        }
    }
    
    return YES;
}

- (void)copyStatus {
    
    if(_studentStatusArray != nil && studentStatusHistoryForCopyArray != nil) {
        NSMutableDictionary<NSNumber *, NSNumber *> *studentIdStatusDict = [[NSMutableDictionary alloc] init];
        
        for(int i=0; i<studentStatusHistoryForCopyArray.count; i++) {
            TEStatusModel *model = [studentStatusHistoryForCopyArray objectAtIndex:i];
            
            NSNumber *userId = [[NSNumber alloc] initWithLongLong:[model getUserId]];
            NSNumber *scanStatus = [[NSNumber alloc] initWithInt:[model getScanStatus]];
            
            [studentIdStatusDict setObject:scanStatus forKey:userId];
        }
        
        for(int i=0; i<_studentStatusArray.count; i++) {
            TEStatusModel *model = [_studentStatusArray objectAtIndex:i];
            
            NSNumber *userId = [[NSNumber alloc] initWithLongLong:[model getUserId]];
            
            if([studentIdStatusDict objectForKey:userId] != nil) {
                int scanStatus = [[studentIdStatusDict objectForKey:userId] intValue];
                [model setScanStatus:scanStatus];
            }
        }
    }
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}
-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (BOOL)validateData {
    
    if(_studentStatusArray != nil) {
        
        for(TAStudentStatusModel *model in _studentStatusArray) {
            if([model getScanStatus] > -1) {
                return YES;
            }
        }
    }
    return NO;
}

- (IBAction)back:(id)sender {
    TELevelClassViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"takeeventAttendanceStoryboard"];
    viewController.classroomId = self.classroomId;
    viewController.mode = self.mode;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (IBAction)actionSelectAll:(id)sender {
     selectedStudentIndex = -1;
     [self showSelectStatusDialog];
}

- (IBAction)actionNext:(id)sender {
    
    if (![self validateData]) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_PLEASE_SELECT_STATUS",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
    }else{
        TESummaryViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TESummaryStoryboard"];
        viewController.studentStatusArray = _studentStatusArray;
        viewController.mode = _mode;
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    
}
@end
