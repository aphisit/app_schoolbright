//
//  MGSuccessAlertDialog.h
//  JabjaiApp
//
//  Created by toffee on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MGSuccessAlertDialogDelegate <NSObject>
@optional
- (void)onAlertDialogClose;

@end
@interface MGSuccessAlertDialog : UIViewController

@property (retain, nonatomic) id<MGSuccessAlertDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *senderImage;
@property (weak, nonatomic) IBOutlet UIImageView *recipientImage;

@property (weak, nonatomic) IBOutlet UILabel *firstName;
@property (weak, nonatomic) IBOutlet UILabel *lastName;
@property (weak, nonatomic) IBOutlet UILabel *nameShopLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalMonny;
@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UIButton *bnt;


- (void)showDialogInView:(UIView *)targetView data:(NSDictionary *)data;

- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
