//
//  ReportOrderHomeWorkRecieverViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportOrderHomeWorkRecieverViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "ReportOrderHomeworkSortStatus.h"
#import "ReportOrderHomeworkPersonRecieverTableViewCell.h"

#import "Utils.h"
#import "UserData.h"
#import "Constant.h"
#import "APIURL.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface ReportOrderHomeWorkRecieverViewController (){
    
    NSMutableDictionary<NSNumber *, NSArray<ReportOrderHomeworkSortStatus *> *> *reportSections;
    NSMutableArray<ReportOrderHomeworkSortStatus *> *messages;
    
    UIColor *grayColor;
    
    NSManagedObject *personalImage;
    
    NSInteger connectCounter;
   
    NSString *changePageStatus;
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;


@end

@implementation ReportOrderHomeWorkRecieverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    reportSections = [[NSMutableDictionary alloc] init];
    messages = [[NSMutableArray alloc] init];
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportOrderHomeworkPersonRecieverTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    
    // set up formatter
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    self.sortData = nil;
    [self getRecieverListWithhomeworkID:self.homeworkID page:1 readStatus:self.sortData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return reportSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger page = section +1;
    
    return [[reportSections objectForKey:@(page)] count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ReportOrderHomeworkPersonRecieverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSInteger page = indexPath.section + 1;
    ReportOrderHomeworkSortStatus *model = [[reportSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
//    cell.imagePerson.image = [UIImage imageNamed:@"ic_user_info"]
    cell.imagePerson.layer.cornerRadius = cell.imagePerson.frame.size.height /2;
    cell.imagePerson.layer.masksToBounds = YES;

    if ([model.image isEqual:@""] || model.image == nil) {
        cell.imagePerson.image = [UIImage imageNamed:@"ic_user_info"];
    }
    else{
        [cell.imagePerson sd_setImageWithURL:[NSURL URLWithString:model.image]];
    }
    
    cell.namePerson.text = model.name;
    
    if (model.status == YES) {
        cell.statusPerson.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_READ",nil,[Utils getLanguage],nil);
    }
    else if (model.status == NO){
        cell.statusPerson.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_UNREND",nil,[Utils getLanguage],nil);
    }
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    return cell;
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getRecieverListWithhomeworkID:(long long)homeworkId page:(NSInteger)page readStatus:(NSString *)readStatus{
    
    [self showIndicator];
    
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getReportOrderHomeworkRecieverWithSchoolID:schoolID homeworkID:homeworkId page:page status:readStatus];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getRecieverListWithhomeworkID:homeworkId page:page readStatus:readStatus];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getRecieverListWithhomeworkID:homeworkId page:page readStatus:readStatus];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getRecieverListWithhomeworkID:homeworkId page:page readStatus:readStatus];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                if (page == 1) {
                    reportSections = [[NSMutableDictionary alloc] init];
                }
                //reportSections = [[NSMutableDictionary alloc] init];
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *recievedname;
                    
                    if(![[dataDict objectForKey:@"student_name"] isKindOfClass:[NSNull class]]) {
                        recievedname = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"student_name"]];
                    }
                    else {
                        recievedname = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *recievedimage;
                    
                    if(![[dataDict objectForKey:@"picture"] isKindOfClass:[NSNull class]]) {
                        recievedimage = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"picture"]];
                    }
                    else {
                        recievedimage = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    BOOL status = [[dataDict objectForKey:@"read_status"] boolValue];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) recievedname);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) recievedimage);
                    
                    ReportOrderHomeworkSortStatus *model = [[ReportOrderHomeworkSortStatus alloc] init];
                    
                    model.name = recievedname;
                    model.status = status;
                    model.image = recievedimage;
                    
                    [newDataArr addObject:model];
                    
                }
                
                [messages removeAllObjects];
                messages = [[NSMutableArray alloc] initWithArray:newDataArr];
                [reportSections setObject:newDataArr forKey:@(page)];
                [self.tableView reloadData];
            }
            
        }
    }];
    
}

- (void)fetchMoreData{
    
    if (messages.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *lastRowIndexPath = [visiblePaths lastObject];
        
        NSInteger numberOfSections = [self.tableView numberOfSections];
        NSInteger lastRowSection = [lastRowIndexPath section];
        NSInteger lastRowRow = [lastRowIndexPath row];
        NSInteger numberOfRowInsection = [self.tableView numberOfRowsInSection:lastRowSection];
        
        if (lastRowSection == numberOfSections -1 && lastRowRow == numberOfRowInsection - 1) {
            if (messages.count % 20 == 0) {
                NSInteger nextpage = lastRowSection + 2;
                changePageStatus = self.sortChangeData;
                if ([changePageStatus isEqual:@"read"]) {
                    [self getRecieverListWithhomeworkID:self.homeworkID page:nextpage readStatus:changePageStatus];
                }
                else if([changePageStatus isEqual:@"unread"]){
                    [self getRecieverListWithhomeworkID:self.homeworkID page:nextpage readStatus:changePageStatus];
                }
                else{
                    [self getRecieverListWithhomeworkID:self.homeworkID page:nextpage readStatus:changePageStatus];
                }

                
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self fetchMoreData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self fetchMoreData];
}

- (void)showIndicator{
    if (![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator{
    [self.indicator stopAnimating];
    
}



@end
