//
//  CallCheckFlagDateTimeOfStudentAPI.h
//  JabjaiApp
//
//  Created by toffee on 18/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPCheckFlagDateTimeModel.h"

NS_ASSUME_NONNULL_BEGIN
@class CallCheckFlagDateTimeOfStudentAPI;
@protocol CallCheckFlagDateTimeOfStudentAPIDelegate <NSObject>

@optional
- (void)callCheckFlagDateTimeOfStudentAPI:(CallCheckFlagDateTimeOfStudentAPI *)classObj data:(NSMutableArray <RPCheckFlagDateTimeModel *>*)data success:(BOOL)success;

@end

@interface CallCheckFlagDateTimeOfStudentAPI : NSObject

-(void)call:(long long)shcoolId studentId:(NSString*)studentId date:(NSString*)date;
@property (nonatomic, weak) id<CallCheckFlagDateTimeOfStudentAPIDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
