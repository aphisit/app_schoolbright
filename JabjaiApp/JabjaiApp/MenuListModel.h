//
//  MenuListModel.h
//  JabjaiApp
//
//  Created by toffee on 4/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuListModel : NSObject
@property (nonatomic) long long productId;
@property (nonatomic) NSString  *productName;
@property (nonatomic) NSInteger *price;
@property (nonatomic) NSInteger *numberProduct;
@property (nonatomic) NSString  *barcode;
@property (nonatomic) NSString  *picture;


- (void)setNumberProduct:(NSInteger *)numberProduct;

- (long long)getProductId;
- (NSString *)getProductName;
- (NSInteger *)getPrice;
- (NSString *)getBarcode;
- (NSString *)getPicture;
- (NSInteger *)getNumberProduct;

@end
