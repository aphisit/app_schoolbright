//
//  CallFileMessageAPI.h
//  JabjaiApp
//
//  Created by toffee on 2/8/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class CallFileMessageAPI;

@protocol CallFileMessageAPIDelegate <NSObject>

@optional
- (void)callFileMessageAPI:(CallFileMessageAPI *)classObj data:(NSArray*)data success:(BOOL)success;
@end
@interface CallFileMessageAPI : NSObject

@property (nonatomic, weak) id<CallFileMessageAPIDelegate> delegate;

- (void)call:(long long)userID messageID:(long long)messageID schoolid:(long long)schoolid;

@end

NS_ASSUME_NONNULL_END
