//
//  RPOriginPinCodeViewController.m
//  JabjaiApp
//
//  Created by toffee on 6/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "RPOriginPinCodeViewController.h"
#import <CommonCrypto/CommonHMAC.h>
#import "UserData.h"
#import "UIView+Shake.h"
#import "SettingViewController.h"
#import "Utils.h"

@interface RPOriginPinCodeViewController (){
    
    NSMutableArray  *numberArray;
    NSMutableString *numberCode;
    ResetPinCodeViewController *resetPinCodeViewController;
}
@property (strong, nonatomic) CallConfirmPinAPI *callConfirmPinAPI;

@end

@implementation RPOriginPinCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    numberArray = [[NSMutableArray alloc] init];
    numberCode = [[NSMutableString alloc] init];
    _confirmPinUnfair.hidden = YES;
   
    _passView1.layer.cornerRadius = _passView1.bounds.size.width/2;
    _passView1.layer.masksToBounds = YES;
    _passView1.layer.borderWidth = 1;
    
    _passView2.layer.cornerRadius = _passView2.bounds.size.width/2;
    _passView2.layer.masksToBounds = YES;
    _passView2.layer.borderWidth = 1;
    
    _passView3.layer.cornerRadius = _passView3.bounds.size.width/2;
    _passView3.layer.masksToBounds = YES;
    _passView3.layer.borderWidth = 1;
    
    _passView4.layer.cornerRadius = _passView4.bounds.size.width/2;
    _passView4.layer.masksToBounds = YES;
    _passView4.layer.borderWidth = 1;
    
    _passView5.layer.cornerRadius = _passView5.bounds.size.width/2;
    _passView5.layer.masksToBounds = YES;
    _passView5.layer.borderWidth = 1;
    
    _passView6.layer.cornerRadius = _passView6.bounds.size.width/2;
    _passView6.layer.masksToBounds = YES;
    _passView6.layer.borderWidth = 1;
    
    [self clearPin];
    [self setLanguage];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

-(void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHANGE_PIN",nil,[Utils getLanguage],nil);
    self.headerPinLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHANGE_PIN_ORIGINAL_CODE",nil,[Utils getLanguage],nil);
    self.headerIncorrectPinLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHANGE_PIN_INCORRECT",nil,[Utils getLanguage],nil);
}

//check dupicate pin
- (void)doConfirmPinCode:(NSString*)jsonCode {
    
    if(self.callConfirmPinAPI != nil) {
        self.callConfirmPinAPI = nil;
    }
    self.callConfirmPinAPI = [[CallConfirmPinAPI alloc] init];
    self.callConfirmPinAPI.delegate = self;
     [self.callConfirmPinAPI call:jsonCode ] ;
}
- (void)callConfirmPinAPI:(CallConfirmPinAPI *)classObj status:(NSString *)status success:(BOOL)success{
    if ([status isEqualToString:@"Success"]) {
        NSLog(@"%@",status);
        [self doResetPincode:numberCode];
       
       
        
    }else{
        
        _confirmPinUnfair.hidden = NO;
        [self.passView1 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView2 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView3 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView4 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView5 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView6 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        
        [self clearPin];
    }
    
}


//CreateJson
-(NSString *)createJsonString:(NSString*)pinCode{
    NSString  *jsonString = [[NSString alloc] initWithFormat:@"{ \"Pin\" : \"%@\", \"User_Id\" : %d, \"schoolid\" : %lld }", pinCode, [UserData getUserID], [UserData getSchoolId]];
    return jsonString;
}
//hash Pin sha256
- (NSMutableString *)hmacForKey1:(NSString *)key andStringData:(NSString *)data
{
    
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    
    NSMutableString *stringOut = [NSMutableString stringWithCapacity:HMAC.length];
    const unsigned char *macOutBytes = HMAC.bytes;
    
    for (NSInteger i=0; i<HMAC.length; ++i) {
        [stringOut appendFormat:@"%02x", macOutBytes[i]];
    }
    return stringOut;
    
}

- (void)doResetPincode:(NSString*)pinOld {
    
    if(resetPinCodeViewController != nil) {
        if([resetPinCodeViewController isDialogShowing]) {
            [resetPinCodeViewController dismissDialog];
        }
    }
    else {
        resetPinCodeViewController = [[ResetPinCodeViewController alloc] init];
        resetPinCodeViewController.delegate = self;
    }
    [resetPinCodeViewController showDialogInView:self.view pinOld:pinOld];
}
//back to settingviewcontroller after confirm PIN
-(void)successConfirmPin{
    SettingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
-(void)onMoveBack{
    [self clearPin];
}

-(void)clearPin{
  
    [_passView1 setBackgroundColor:[UIColor clearColor]];
    [_passView2 setBackgroundColor:[UIColor clearColor]];
    [_passView3 setBackgroundColor:[UIColor clearColor]];
    [_passView4 setBackgroundColor:[UIColor clearColor]];
    [_passView5 setBackgroundColor:[UIColor clearColor]];
    [_passView6 setBackgroundColor:[UIColor clearColor]];
   
    numberArray = [[NSMutableArray alloc] init];
    numberCode = [[NSMutableString alloc] init];

}
- (IBAction)actionButtonNumber:(UIControl*)sender {
    
    if (sender.tag != 10) {
        if (numberArray.count < 6) {
            [numberCode appendString:[NSString stringWithFormat:@"%d",sender.tag]];
            [numberArray addObject:[NSString stringWithFormat:@"%d",sender.tag]];
            
            if (numberArray.count == 1) {
                
                [_passView1 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 2) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 3) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 4) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 5) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 6) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
                [_passView6 setBackgroundColor:[UIColor grayColor]];
                
                NSString *key = @"ra7$K:L.]%";
                NSMutableString *hmac1 = [self hmacForKey1:key andStringData:numberCode];
                NSString *jsonString = [self createJsonString:hmac1];
                [self doConfirmPinCode:jsonString];
            }
            
        }
    }else{
        _confirmPinUnfair.hidden = YES;
        if (numberArray.count > 0) {
            [numberCode deleteCharactersInRange:NSMakeRange(numberArray.count-1, 1)];
            [numberArray removeObjectAtIndex:numberArray.count-1];
            [_passView1 setBackgroundColor:[UIColor clearColor]];
            [_passView2 setBackgroundColor:[UIColor clearColor]];
            [_passView3 setBackgroundColor:[UIColor clearColor]];
            [_passView4 setBackgroundColor:[UIColor clearColor]];
            [_passView5 setBackgroundColor:[UIColor clearColor]];
            [_passView6 setBackgroundColor:[UIColor clearColor]];
            if (numberArray.count == 1) {
                
                [_passView1 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 2) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 3) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 4) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 5) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 6) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
                [_passView6 setBackgroundColor:[UIColor grayColor]];
            }
            
        }
    }
    
    
    
}

- (IBAction)moveBack:(id)sender {
    SettingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
