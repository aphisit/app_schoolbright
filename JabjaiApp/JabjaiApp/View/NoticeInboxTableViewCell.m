//
//  NoticeInboxTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 12/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NoticeInboxTableViewCell.h"

@implementation NoticeInboxTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.noticeStatusColor.backgroundColor = [UIColor greenColor];
    self.dateInbox.text = nil;
    self.leaveName.text = nil;
    self.leaveCause.text = nil;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse{
    
    self.noticeStatusColor.backgroundColor = [UIColor greenColor];
    self.leaveName.text = nil;
    self.dateInbox.text = nil;
    self.leaveCause.text = nil;
    
}

@end
