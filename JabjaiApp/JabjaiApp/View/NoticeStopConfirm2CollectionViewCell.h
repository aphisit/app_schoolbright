//
//  NoticeStopConfirm2CollectionViewCell.h
//  JabjaiApp
//
//  Created by mac on 11/30/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeStopConfirm2CollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageNotice;

@end
