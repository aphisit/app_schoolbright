//
//  NoticeImageCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 7/8/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoticeImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;


@end

NS_ASSUME_NONNULL_END
