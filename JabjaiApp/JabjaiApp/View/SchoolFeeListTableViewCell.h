//
//  SchoolFeeListTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 3/13/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SchoolFeeListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerPaymentLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusColor;
@property (weak, nonatomic) IBOutlet UILabel *statusFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *termLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *issueDateLabel;

@end
