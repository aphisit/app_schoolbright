//
//  NoticeStopConfirmCollectionViewCell.h
//  JabjaiApp
//
//  Created by mac on 11/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeStopConfirmCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageNotice;

@end
