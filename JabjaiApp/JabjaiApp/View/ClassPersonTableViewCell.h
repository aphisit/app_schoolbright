//
//  ClassPersonTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 3/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassPersonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *classLabel;

@end
