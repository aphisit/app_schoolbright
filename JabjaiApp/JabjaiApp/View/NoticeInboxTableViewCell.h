//
//  NoticeInboxTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 12/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeInboxTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *noticeStatusColor;
@property (weak, nonatomic) IBOutlet UIView *readStatusColor;

@property (weak, nonatomic) IBOutlet UILabel *dateInbox;
@property (weak, nonatomic) IBOutlet UILabel *leaveCause;
@property (weak, nonatomic) IBOutlet UILabel *leaveName;



@end
