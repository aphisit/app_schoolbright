//
//  LIDetailLeaveViewControllerXIB.h
//  JabjaiApp
//
//  Created by Mac on 22/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoticeInboxDetailModel.h"
#import "AlertDialogConfirm.h"
#import "MessageInboxScrollableTextDialogCollectionViewCell.h"
#import "ZMImageSliderViewController.h"
NS_ASSUME_NONNULL_BEGIN
@protocol LIDetailLeaveViewControllerXIBDelegate <NSObject>

- (void) close;

@end

@interface LIDetailLeaveViewControllerXIB : UIViewController <AlertDialogConfirmDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic,retain) id<LIDetailLeaveViewControllerXIBDelegate > delegate;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassroomLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPositionLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerReasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerStarDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEndDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSubmitDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttactImageLabel;

@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailName;
@property (weak, nonatomic) IBOutlet UILabel *leaveInClassRoomLabel;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailPosiion;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailCause;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailFirstDate;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailLastDate;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailRequestDate;
@property (weak, nonatomic) IBOutlet UITextView *leaveDetailTextView;
//@property (weak, nonatomic) IBOutlet UILabel *coronClassroomLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconClassroomImg;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailAddress;
@property (weak, nonatomic) IBOutlet UIView *declineBackGround;
@property (weak, nonatomic) IBOutlet UIView *acceptBackGround;
@property (weak, nonatomic) IBOutlet UIView *statusBackGround;
@property (weak, nonatomic) IBOutlet UILabel *statusLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *acceptLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *declineLeaveLabel;

@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIButton *declineButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightButtonMenuConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highImageConstraint;

@property (weak, nonatomic) IBOutlet UIStackView *statusStackView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonStatusViewConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pointClassroomConstraint;


- (void)dismissDetailLeaveViewController;
- (BOOL)isDialogShowing;
- (void)showDetailLeaveViewController:(UIView*)targetView letterID:(long long)letterID;

- (IBAction)moveBack:(id)sender;

@end

NS_ASSUME_NONNULL_END
