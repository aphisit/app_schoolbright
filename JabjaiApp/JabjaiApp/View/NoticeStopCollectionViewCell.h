//
//  NoticeStopCollectionViewCell.h
//  JabjaiApp
//
//  Created by mac on 11/28/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeStopCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageNotice;

@end
