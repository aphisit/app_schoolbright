//
//  LIDetailLeaveViewControllerXIB.m
//  JabjaiApp
//
//  Created by Mac on 22/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "LIDetailLeaveViewControllerXIB.h"
#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"
#import "KxMenu.h"
@interface LIDetailLeaveViewControllerXIB (){
    NoticeInboxDetailModel *noticeInboxDetailModel;
    NSMutableArray<NoticeInboxDetailModel *> *noticeLeaveArray;
    NSInteger connectCounter;
    NSInteger approve;
    AlertDialogConfirm *alertDialog;
    NSString *address, *tumbon, *aumphur, *province, *phone;
    NSString *headtumbon, *headaumphur, *headprovince, *headphone;
    NSArray *returnedArray;
    NSMutableArray *imageArray;
    NSString *urlFile;
    CAGradientLayer *greenGradient ,*redGradient ,*blueGradient,*purpleGradient;
    UIColor *jungleGreen, *downy, *salmon, *macaroniAndCheese, *mariner, *malibu, *heliotRope ,*melrose;
    UIColor *bitterSweet;
    CGFloat screenWidth;
    long long letterId;
    NSInteger imgConstant;
}
@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@end

@implementation LIDetailLeaveViewControllerXIB

- (void)viewDidLoad {
    [super viewDidLoad];
    imgConstant = self.highImageConstraint.constant;
    // Do any additional setup after loading the view from its nib.
}

//- (void)viewDidLayoutSubviews{
//
//    [self doDesignLayout];
//}

- (void) doDesignLayout{
    CAGradientLayer  *headerGradient,*acceptGradient,*cancelGradient;
    //set color header
    [self.headerView layoutIfNeeded];
    headerGradient = [Utils getGradientColorHeader];
    headerGradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:headerGradient atIndex:0];
    
    [self.acceptBackGround layoutIfNeeded];
    acceptGradient = [Utils getGradientColorStatus:@"green"];
    acceptGradient.frame = self.acceptBackGround.bounds;
    [self.acceptBackGround.layer insertSublayer:acceptGradient atIndex:0];
    
    [self.declineBackGround layoutIfNeeded];
    cancelGradient = [Utils getGradientColorStatus:@"red"];
    cancelGradient.frame = self.declineBackGround.bounds;
    [self.declineBackGround.layer insertSublayer:cancelGradient atIndex:0];
    
    //set color stutus
    [self.statusBackGround layoutIfNeeded];
    
    greenGradient = [Utils getGradientColorStatus:@"green"];
    greenGradient.frame = self.statusBackGround.bounds;
    
    redGradient = [Utils getGradientColorHeader];
    redGradient.frame = self.statusBackGround.bounds;
    
    blueGradient = [Utils getGradientColorStatus:@"blue"];
    blueGradient.frame = self.statusBackGround.bounds;
    
    purpleGradient = [Utils getGradientColorStatus:@"purple"];
    purpleGradient.frame = self.statusBackGround.bounds;
}
//[NSString stringWithFormat:@"%@:"]
- (void) setLanguage{
    self.headerTitleLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_LEAVE_REQUEST",nil,[Utils getLanguage],nil)] ;
    self.headerSenderLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_SENDER",nil,[Utils getLanguage],nil)];
    self.headerClassroomLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_CLASSROOM",nil,[Utils getLanguage],nil)] ;
    self.headerPositionLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_POSITION",nil,[Utils getLanguage],nil)] ;
    self.headerReasonLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_REASON",nil,[Utils getLanguage],nil)] ;
    self.headerStarDayLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_START_DAY",nil,[Utils getLanguage],nil)] ;
    self.headerEndDayLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_END_DAY",nil,[Utils getLanguage],nil)] ;
    self.headerSubmitDateLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_SUBMITDATE",nil,[Utils getLanguage],nil)];
    self.headerDetailLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_DETAIL",nil,[Utils getLanguage],nil)];
    self.headerAddressLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_ADDRESS",nil,[Utils getLanguage],nil)] ;
    self.headerAttactImageLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_ATTACT_IMAGE",nil,[Utils getLanguage],nil)];
    self.acceptLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_LEAVE_REPORT_ALLOW",nil,[Utils getLanguage],nil);
    self.declineLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"BTN_LEAVE_REPORT_NOT_ALLOW",nil,[Utils getLanguage],nil);
}

- (void)showDetailLeaveViewController:(UIView *)targetView letterID:(long long)letterID{
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
        [targetView addSubview:self.view];
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height-20)];
        [targetView addSubview:self.view];
    }
    self.isShowing = YES;
    
    letterId = letterID;
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    imageArray = [[NSMutableArray alloc] init];

    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
//    self.heightButtonMenuConstraint.constant = 0;
//    self.buttonStatusViewConstraint.constant = 0;
//    self.statusStackView.hidden = YES;
    
    [self setLanguage];
    [self getLetterConfirmDetailWithLetterID:letterID];
    

}

-(void)removeDetailLeaveViewController {
    
//    [UIView beginAnimations:@"" context:nil];
//    [UIView setAnimationDuration:0.25];
//    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
//    [self.contentView setAlpha:0.0];
//    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDetailLeaveViewController{
    [self removeDetailLeaveViewController];
    
//    if(self.delegate && [self.delegate respondsToSelector:@selector(closePageHomeWork)]) {
//        [self.delegate closePageHomeWork];
//    }
}
- (BOOL)isDialogShowing{
    return self.isShowing;
}


- (void)getLetterConfirmDetailWithLetterID:(long long)letterID{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getLetterLeaveDetailWithLetterID:letterID userID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    NSLog(@"%@", URLString);
    
//    [[ERProgressHud sharedInstance] showDarkBackgroundViewWithTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LEAVE_REPORT_PLEASE_WAIT",nil,[Utils getLanguage],nil)];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getLetterConfirmDetailWithLetterID:letterID];
            }
            else {
                self->connectCounter = 0;
            }
            
        }
        else {
            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [[ERProgressHud sharedInstance] hide];
//            });
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getLetterConfirmDetailWithLetterID:letterID];
                }
                else {
                    self->connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getLetterConfirmDetailWithLetterID:letterID];
                }
                else {
                    self->connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                self->connectCounter = 0;
                
                if (self->noticeLeaveArray != nil) {
                    [self->noticeLeaveArray removeAllObjects];
                    self->noticeLeaveArray = nil;
                }
                
                self->noticeLeaveArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *leaveName, *leavePosition, *leaveCause, *leaveStart, *leaveEnd, *leaveDetail, *leaveRequest,*leaveClassRoom,*leaveStudentCode;
                    
                    //leave name
                    if (![[dataDict objectForKey:@"senderName"] isKindOfClass:[NSNull class]]) {
                        leaveName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderName"]];
                    }
                    else{
                        leaveName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave position
                    if (![[dataDict objectForKey:@"senderJob"] isKindOfClass:[NSNull class]]) {
                        leavePosition = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderJob"]];
                    }
                    else{
                        leavePosition = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave cause
                    if (![[dataDict objectForKey:@"letterType"] isKindOfClass:[NSNull class]]) {
                        leaveCause = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"letterType"]];
                    }
                    else{
                        leaveCause = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave start
                    if (![[dataDict objectForKey:@"leaveStart"] isKindOfClass:[NSNull class]]) {
                        leaveStart = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"leaveStart"]];
                    }
                    else{
                        leaveStart = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave end
                    if (![[dataDict objectForKey:@"leaveEnd"] isKindOfClass:[NSNull class]]) {
                        leaveEnd = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"leaveEnd"]];
                    }
                    else{
                        leaveEnd = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave request
                    if (![[dataDict objectForKey:@"sendRequestDate"] isKindOfClass:[NSNull class]]) {
                        leaveRequest = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sendRequestDate"]];
                    }
                    else{
                        leaveRequest = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave detail
                    if (![[dataDict objectForKey:@"letterDescription"] isKindOfClass:[NSNull class]]) {
                        leaveDetail = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"letterDescription"]];
                    }
                    else{
                        leaveDetail = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    int status = [[dataDict objectForKey:@"status"] intValue];
                    int pageStatus = [[dataDict objectForKey:@"pageStatus"] intValue];
                    
                    NSMutableString *season;
                    
                    if (![[dataDict objectForKey:@"Season"] isKindOfClass:[NSNull class]]) {
                        season = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Season"]];
                    }
                    else{
                        season = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave Classroom
                    if (![[dataDict objectForKey:@"senderClassroom"] isKindOfClass:[NSNull class]]) {
                        leaveClassRoom = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderClassroom"]];
                    }
                    else{
                        leaveClassRoom = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave StudentCode
                    if (![[dataDict objectForKey:@"Code"] isKindOfClass:[NSNull class]]) {
                        leaveStudentCode = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Code"]];
                    }
                    else{
                        leaveStudentCode = [[NSMutableString alloc] initWithString:@""];
                    }
         
                    NSMutableString *address, *tumbon, *aumphur, *province, *phone;
                    
                    if (![[dataDict objectForKey:@"Aumpher"] isKindOfClass:[NSNull class]]) {
                        aumphur = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Aumpher"]];
                    }
                    else{
                        aumphur = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Tumbon"] isKindOfClass:[NSNull class]]) {
                        tumbon = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Tumbon"]];
                    }
                    else{
                        tumbon = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"HomeNumber"] isKindOfClass:[NSNull class]]) {
                        address = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"HomeNumber"]];
                    }
                    else{
                        address = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Province"] isKindOfClass:[NSNull class]]) {
                        province = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Province"]];
                    }
                    else{
                        province = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Phone"] isKindOfClass:[NSNull class]]) {
                        phone = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Phone"]];
                    }
                    else{
                        phone = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableArray *fileImage;
                    fileImage = [dataDict objectForKey:@"file"];
                    NSLog(@"%@", fileImage);
                    
//                    if (fileImage.count == 0) {
//                        self.heightCollectionViewConstraint.constant = 0;
//                        self.heightContentConstraint.constant = 500;
//                    }
//
//                    for (i=0; i < fileImage.count; i++) {
//                        NSString *picture = fileImage[i];
//
//                        NSLog(@"%@", picture);
//
//                        urlFile = picture;
//
//                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leavePosition);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveCause);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveStart);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveEnd);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveDetail);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveRequest);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveStudentCode);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveClassRoom);
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) address);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) tumbon);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) aumphur);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) province);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) phone);
                    
                    NSDate *leaveStartDate;
                    leaveStartDate = [formatter2 dateFromString:leaveStart];
                    
                    NSDate *leaveEndDate;
                    leaveEndDate = [formatter2 dateFromString:leaveEnd];
                    
                    NSRange dotRange = [leaveRequest rangeOfString:@"."];
                    
                    NSDate *leaveRequestDate;
                    
                    if(dotRange.length != 0) {
                        leaveRequestDate = [formatter dateFromString:leaveRequest];
                    }
                    else {
                        leaveRequestDate = [formatter2 dateFromString:leaveRequest];
                    }
                    
                    NoticeInboxDetailModel *model = [[NoticeInboxDetailModel alloc] init];
                    
                    model.leaveName = leaveName;
                    model.leavePosition = leavePosition;
                    model.leaveCause = leaveCause;
                    model.leaveStart = leaveStartDate;
                    model.leaveEnd = leaveEndDate;
                    model.leaveDetail = leaveDetail;
                    model.leaveDateRequest = leaveRequestDate;
                    model.classRoom = leaveClassRoom;
                    model.studentCode = leaveStudentCode;
                    
                    model.address = address;
                    model.tumbon = tumbon;
                    model.aumphur = aumphur;
                    model.province = province;
                    model.phone = phone;
                    
                    model.file = fileImage;
                    
                    model.status = status;
                    model.season = season;
                    model.pageStatus = pageStatus;
                    
                    self->noticeInboxDetailModel = model;
                    [self performData];
                    
                    [self->noticeLeaveArray addObject:model];
                    
                    //[self.collectionView reloadData];
                    
                }
                
            }
        }
        
    }];
    
}

- (void)performData{
    
    if (noticeInboxDetailModel != nil) {
        
        if ([noticeInboxDetailModel.classRoom isEqualToString:@""]) {
            self.iconClassroomImg.hidden = YES;
            self.headerClassroomLabel.hidden = YES;
            self.leaveInClassRoomLabel.hidden = YES;
            self.pointClassroomConstraint.constant = 10;
        }
        
        self.leaveInboxDetailName.text = [NSString stringWithFormat:@"%@ %@",noticeInboxDetailModel.studentCode,noticeInboxDetailModel.leaveName];
        self.leaveInboxDetailPosiion.text = noticeInboxDetailModel.leavePosition;
        self.leaveInboxDetailCause.text = noticeInboxDetailModel.leaveCause;
        self.leaveInboxDetailFirstDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveStart];
        self.leaveInboxDetailRequestDate.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveDateRequest] , [self.timeFormatter stringFromDate:noticeInboxDetailModel.leaveDateRequest]];
        self.leaveDetailTextView.text = noticeInboxDetailModel.leaveDetail;
        self.leaveInClassRoomLabel.text = noticeInboxDetailModel.classRoom;
        
        address = noticeInboxDetailModel.address;
        tumbon = noticeInboxDetailModel.tumbon;
        aumphur = noticeInboxDetailModel.aumphur;
        province = noticeInboxDetailModel.province;
        phone = noticeInboxDetailModel.phone;
        
        //Address
        if ([address isEqual:@""] || [tumbon isEqual:@""] || [aumphur isEqual:@""] || [province isEqual:@""] || [phone isEqual:@""]) {
            self.leaveInboxDetailAddress.text = @"-";
            //self.heightContentConstraint.constant = 525;
        }
        else{
            
            headtumbon = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_SUBDISTRICT",nil,[Utils getLanguage],nil);
            headaumphur = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_DISTRICT",nil,[Utils getLanguage],nil);
            headprovince = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_PROVINCE",nil,[Utils getLanguage],nil);
            headphone = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_CONTACT_NUMBER",nil,[Utils getLanguage],nil);
            
            self.leaveInboxDetailAddress.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@" , address, headtumbon, tumbon, headaumphur, aumphur, headprovince, province, headphone, phone];
            
        }
        
        if ([noticeInboxDetailModel.season isEqual:@""]) {
            self.headerEndDayLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_END_DAY",nil,[Utils getLanguage],nil)];
            self.leaveInboxDetailLastDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveEnd];
        }
        else{
            if ([noticeInboxDetailModel.season isEqual:@"0"]){
                self.headerEndDayLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_TIME_PERIOD",nil,[Utils getLanguage],nil)] ;
                self.leaveInboxDetailLastDate.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_HALF_MORNING",nil,[Utils getLanguage],nil);
            }
            else if ([noticeInboxDetailModel.season isEqual:@"1"]) {
                self.headerEndDayLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_TIME_PERIOD",nil,[Utils getLanguage],nil)];
                self.leaveInboxDetailLastDate.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_HALF_AFTERNOON",nil,[Utils getLanguage],nil);
            }
            else if ([noticeInboxDetailModel.season isEqual:@"-1"]) {
                self.headerEndDayLabel.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_END_DAY",nil,[Utils getLanguage],nil)];
                self.leaveInboxDetailLastDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveEnd];
            }
            else {
                self.leaveInboxDetailLastDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveEnd];
            }
        }
        if (noticeInboxDetailModel.file.count == 0) {
            self.collectionView.hidden = YES;
        }else{
            self.collectionView.hidden = NO;
        }
        
        // set image collectionview
        imageArray = noticeInboxDetailModel.file;
        float wCollection = self.collectionView.frame.size.width / ((self.collectionView.frame.size.width/4)-10);
        float amountRow = self->imageArray.count / (int)wCollection;
        NSInteger modImg = self->imageArray.count % (int)wCollection ;
        if (modImg > 0) {
             amountRow ++;
        }
        self.highImageConstraint.constant = self->imgConstant * amountRow+((amountRow*5)*2);
        [self doDesignLayout];
        [self.collectionView reloadData];
        
        //set status
        [self.statusBackGround layoutIfNeeded];
        switch (noticeInboxDetailModel.pageStatus) {
            case 0:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVALTREE",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineLeave:) forControlEvents:UIControlEventTouchUpInside];
                //[self.statusBackGround insertSubview:blueGradient atIndex:0];
                [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
            break;
            case 1:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVALTWO",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineLeave:) forControlEvents:UIControlEventTouchUpInside];
               [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
                break;
            case 2:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVALONE",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
                break;
            case 3:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVAL",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.buttonStatusViewConstraint.constant = 0;
                self.statusStackView.hidden = YES;
                [self.statusBackGround.layer insertSublayer:greenGradient atIndex:0];
                break;
            case 4:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_REJECRED",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.buttonStatusViewConstraint.constant = 0;
                self.statusStackView.hidden = YES;
                [self.statusBackGround.layer insertSublayer:redGradient atIndex:0];
                break;
            case 5:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_WAIT_APPROVAL",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.buttonStatusViewConstraint.constant = 0;
                self.statusStackView.hidden = YES;
               [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
                break;
            case 6:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_TREE_CANCEL",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.statusBackGround.layer insertSublayer:purpleGradient atIndex:0];
                break;
            case 7:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_TWO_CANCEL",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.statusBackGround.layer insertSublayer:purpleGradient atIndex:0];
                break;
            case 8:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_ONE_CANCEL",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.statusBackGround.layer insertSublayer:purpleGradient atIndex:0];
                break;
            case 9:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_CANCEL_REQUEST",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.buttonStatusViewConstraint.constant = 0;
                self.statusStackView.hidden = YES;
                [self.statusBackGround.layer insertSublayer:purpleGradient atIndex:0];
                break;
            case 10:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVALTWO",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.buttonStatusViewConstraint.constant = 0;
                self.statusStackView.hidden = YES;
               [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
                break;
            case 11:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVALONE",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.buttonStatusViewConstraint.constant = 0;
                self.statusStackView.hidden = YES;
                [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
                break;
            default:
                break;
        }
    }
    else{
        
        [self clearDisplay];
    }
    
}

- (void)clearDisplay {
    
    self.leaveInboxDetailName.text = @"";
    self.leaveInboxDetailPosiion.text = @"";
    self.leaveInboxDetailCause.text = @"";
    self.leaveInboxDetailFirstDate.text = @"";
    self.leaveInboxDetailLastDate.text = @"";
    self.leaveInboxDetailRequestDate.text = @"";
    self.leaveDetailTextView.text = @"";

}

#pragma mark - CollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return imageArray.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MessageInboxScrollableTextDialogCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSData *imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [imageArray objectAtIndex:indexPath.row]]];
    cell.imageNews.tag = indexPath.row;
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageWithData:imageData];
    
    [cell.imageNews setBackgroundImage:imageView.image forState:UIControlStateNormal];
    [cell.imageNews addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize defaultSize;
    defaultSize = CGSizeMake((self.collectionView.frame.size.width/4)-10, (imgConstant));
    return defaultSize;
}

- (void)okButtonTapped:(UIButton *)sender {
    
    NSLog(@"index = %d",sender.tag);
    ZMImageSliderViewController*controller = [[ZMImageSliderViewController alloc] initWithOptions:sender.tag imageUrls:imageArray];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)acceptLeave:(UIButton *)sender{
    
    approve = 1;
    [self getRequestLetterWithLetterID:letterId approve:approve];
    
}

- (void)declineLeave:(UIButton *)sender{
    
    approve = 0;
    [self getRequestLetterWithLetterID:letterId approve:approve];
    
}

- (void)acceptCancelLeave:(UIButton *)sender{
    
    approve = 3;
    [self getRequestLetterWithLetterID:letterId approve:approve];
    
}

- (void)declineCancelLeave:(UIButton *)sender{
    
    approve = 4;
    [self getRequestLetterWithLetterID:letterId approve:approve];
    
}

- (void)getRequestLetterWithLetterID:(long long)letterID approve:(NSInteger)approve{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getRequestLetterWithLetterID:letterID userID:userID approve:approve schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    //    [self showIndicator];
    
//    [[ERProgressHud sharedInstance] showDarkBackgroundViewWithTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LEAVE_REPORT_PLEASE_WAIT",nil,[Utils getLanguage],nil)];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            //            [self stopIndicator];
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(self-> connectCounter < TRY_CONNECT_MAX) {
                self-> connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(self-> connectCounter) stringValue]);
                [self getRequestLetterWithLetterID:letterID approve:approve];
            }
            else {
                self-> connectCounter = 0;
            }
        }
        else{
            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [[ERProgressHud sharedInstance] hide];
//            });
            
            [self validateData];
        }
    }];
}

- (void)validateData{
    NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LEAVE_REPORT_SUCCESS",nil,[Utils getLanguage],nil);
    [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)onAlertDialogClose{
    [self dismissDetailLeaveViewController];
}

- (IBAction)moveBack:(id)sender {
    [self dismissDetailLeaveViewController];
}
@end
