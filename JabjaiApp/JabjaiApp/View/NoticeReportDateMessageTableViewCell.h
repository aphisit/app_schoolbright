//
//  NoticeReportDateMessageTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 1/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeReportDateMessageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *statusColor;
@property (weak, nonatomic) IBOutlet UILabel *leaveDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *leaveNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *leaveCauseLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;


@end
