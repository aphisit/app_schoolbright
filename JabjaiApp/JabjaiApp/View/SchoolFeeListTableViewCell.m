//
//  SchoolFeeListTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 3/13/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "SchoolFeeListTableViewCell.h"
#import "Utils.h"
@implementation SchoolFeeListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.headerPaymentLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE",nil,[Utils getLanguage],nil);
//    self.headerYearLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_YEAR",nil,[Utils getLanguage],nil);
//    self.headerSemesterLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_SEMESTER",nil,[Utils getLanguage],nil);
//    self.headerBalanceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_INFROM_PAYMENT",nil,[Utils getLanguage],nil);
//    self.headerBathLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_BATH",nil,[Utils getLanguage],nil);
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
