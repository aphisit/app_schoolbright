//
//  SchoolFeePriceListTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 3/13/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SchoolFeePriceListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *priceListLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *bahtLabel;


@end
