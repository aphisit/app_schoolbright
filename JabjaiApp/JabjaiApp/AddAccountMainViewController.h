//
//  AddAccountMainViewController.h
//  JabjaiApp

//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DelUserAccountTableViewCell.h"
#import "ConfirmDialog.h"
#import "SlideMenuController.h"
#import "CallRemoveDataSystemNotificationPOSTAPI.h"
extern NSString *tokenStr;
@interface AddAccountMainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, DelUserAccountTableViewCellDelegate, ConfirmDialogDelegate, SlideMenuControllerDelegate, CallRemoveDataSystemNotificationPOSTAPIDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerManageAccountLabel;

@property (weak, nonatomic) IBOutlet UIButton *addUserButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)moveBack:(id)sender;
- (IBAction)actionAddAccount:(id)sender;

@end
