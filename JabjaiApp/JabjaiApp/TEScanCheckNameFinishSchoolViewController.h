//
//  TEScanCheckNameFinishSchoolViewController.h
//  JabjaiApp
//
//  Created by toffee on 30/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "CallGetTEFinishschoolAPI.h"
#import "CallTEGetDataStudentScanBarcodeAPI.h"
#import "AlertDialog.h"
NS_ASSUME_NONNULL_BEGIN

@protocol TEScanCheckNameFinishSchoolDelegate <NSObject>

- (void) replyTEScanCheckNameFinishSchoolViewController;

//- (void)responseScannerCheckNameFinishSchool:(TEDataStudentScanerModel*) data studentCode:(NSString*)studentCode;
- (void)replyIdStudent:(TEDataStudentScanerModel*) data studentCode:(NSString*)studentCode mode:(int)mode success:(BOOL)success;
- (void) responseAleatMessage:(NSString*)message mode:(int)mode;

@end

@interface TEScanCheckNameFinishSchoolViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate,CallGetTEFinishschoolAPIDelegate,CallTEGetDataStudentScanBarcodeAPIDelegate>

@property (nonatomic,retain) id<TEScanCheckNameFinishSchoolDelegate>delegate;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, assign) int mode;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) IBOutlet UIView *viewforCamera;
@end

NS_ASSUME_NONNULL_END
