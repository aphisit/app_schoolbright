//
//  JHClassRoomGroupViewController.m
//  JabjaiApp
//
//  Created by toffee on 12/18/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "JHClassRoomGroupViewController.h"
#import "EXReportClassHeaderTableViewCell.h"
#import "SNClassRoomview.h"

@interface JHClassRoomGroupViewController ()

@end
static NSString *headerCellIdentifier = @"HeaderCell";
static NSString *childCellIdentifier = @"Cell";

@implementation JHClassRoomGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EXReportClassHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:headerCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SNClassRoomview class]) bundle:nil] forCellReuseIdentifier:childCellIdentifier];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionNext:(id)sender {
}

- (IBAction)actionBack:(id)sender {
}

- (IBAction)back:(id)sender {
}
@end
