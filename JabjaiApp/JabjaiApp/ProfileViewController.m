//
//  ProfileViewController.m
//  JabjaiApp
//
//  Created by mac on 12/21/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "ProfileViewController.h"
#import "SWRevealViewController.h"
#import "Utils.h"
#import "UserInfoModel.h"
#import "APIURL.h"
#import "Constant.h"
#import "ProfileCardXIB.h"

@interface ProfileViewController () {
    NSInteger connectCounter;
}

@property (strong, nonatomic) UserInfoModel *userInfoModel;
//@property (strong, nonatomic) CardProfile *cardProfile;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) CardPfileXIBViewController *cardPfileXIBViewController;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"UserBefore = %@",[UserData getShowSomeId]);
    if ([UserData getShowSomeId] == NULL) {
           [UserData setShowSomeId:@"on"];
    }
    NSLog(@"UserAfter = %@",[UserData getShowSomeId]);
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController){
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(singleTapping:)];
    tapGesture1.numberOfTapsRequired = 1;
    [tapGesture1 setDelegate:self];
    [_backgroundCardprofile addGestureRecognizer:tapGesture1];
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    self.schoolValueLabel.text = @"";
    self.nameValueLabel.text = @"";
    self.lastNameValueLabel.text = @"";
    self.classValueLabel.text = @"";
    self.studentIDValueLabel.text = @"";
    self.genderValueLabel.text = @"";
    self.birthdayValueLabel.text = @"";
    self.phoneNumberValueLabel.text = @"";
    self.emailValueLabel.text = @"";
    self.remainingValueLabel.text = @"";
    self.creditLimitsValueLabel.text = @"";
    self.cardView.layer.cornerRadius = 9.0;
    self.cardView.layer.masksToBounds = YES;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        CGFloat screenHeight = [[UIScreen mainScreen]bounds].size.height;
        
        if (screenHeight == 812.0f) {//iphone x
            self.heightBackgroundCardConstraint.constant = 240.0f;
        }
        if (screenHeight == 667.0f) {//iphone 6
            self.heightBackgroundCardConstraint.constant = 240.0f;
        }
        if (screenHeight == 736.0f) {//iphone 6+
            self.heightBackgroundCardConstraint.constant = 250.0f;
        }
        if (screenHeight == 568.0f) {//iphone 5
            self.heightBackgroundCardConstraint.constant = 213.0f;
        }
    }else{
         self.heightBackgroundCardConstraint.constant = 400.0f;
    }
    connectCounter = 0;
    [self setLanguage];
    [self getUserInfoData];
}
    
- (void)viewDidLayoutSubviews{
    
    [self doDesignLayout];
}
    
- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
}

- (void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PROFILE",nil,[Utils getLanguage],nil);
    self.headerSchoolLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PROFILE_SCHOOLNAME",nil,[Utils getLanguage],nil);
    self.headerNameLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PROFILE_NAME",nil,[Utils getLanguage],nil);
    self.headerSexLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PROFILE_SEX",nil,[Utils getLanguage],nil);
    self.headerBirthdayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PROFILE_BIRTHDAY",nil,[Utils getLanguage],nil);
    self.headerPhoneNumberLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PROFILE_PHONENUMBER",nil,[Utils getLanguage],nil);
    self.headerEmailLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PROFILE_EMAIL",nil,[Utils getLanguage],nil);
    self.headerClassroomLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PROFILE_CLASSROOM",nil,[Utils getLanguage],nil);
    self.headerStudentIDLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_PROFILE_STUDENTID",nil,[Utils getLanguage],nil);
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    NSLog(@"xxxx");
        if(self.cardPfileXIBViewController != nil) {
            if([self.cardPfileXIBViewController isDialogShowing]) {
                [self.cardPfileXIBViewController dismissDialog];
            }
        }
        else {
            self.cardPfileXIBViewController = [[CardPfileXIBViewController alloc] init];
            self.cardPfileXIBViewController.delegate = self;
        }
    
            [ self.cardPfileXIBViewController showDialogInView: self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - GetAPIData

- (void)getUserInfoData {
    NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUserInfoData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(returnedArray.count != 0) {
                    
                    self.userInfoModel = [[UserInfoModel alloc] init];
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    NSMutableString *firstName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sName"]];
                    NSMutableString *lastName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sLastname"]];
                    NSInteger sex = [[dataDict objectForKey:@"cSex"] integerValue];
                    NSMutableString *birthdayStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dBirth"]];
                    NSMutableString *phoneNumber = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sPhone"]];
                    NSMutableString *email = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sEmail"]];
                    float money = [[dataDict objectForKey:@"nMoney"] floatValue];
                    float creditLimits = [[dataDict objectForKey:@"nMax"] floatValue];
                    NSNumber *score = [NSNumber numberWithDouble:[[dataDict objectForKey:@"Score"] doubleValue]];
                    
                    NSString *imageUrl;
                    
                    if(![[dataDict objectForKey:@"image"] isKindOfClass:[NSNull class]]) {
                        imageUrl = [dataDict objectForKey:@"image"];
                    }
                    else {
                        imageUrl = @"";
                    }
                    
                    NSMutableString *studentCode, *studentClass, *schoolName;
                    
                    if(![[dataDict objectForKey:@"StudentId"] isKindOfClass:[NSNull class]]) {
                        studentCode = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"StudentId"]];
                    }
                    else {
                        studentCode = [[NSMutableString alloc] init];
                    }
                    
                    if(![[dataDict objectForKey:@"StudentClass"] isKindOfClass:[NSNull class]]) {
                        studentClass = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"StudentClass"]];
                    }
                    else {
                        studentClass = [[NSMutableString alloc] init];
                    }
                    
                    if(![[dataDict objectForKey:@"SchoolName"] isKindOfClass:[NSNull class]]) {
                        schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolName"]];
                    }
                    else {
                        schoolName = [[NSMutableString alloc] init];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) firstName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lastName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) birthdayStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) phoneNumber);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) email);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentCode);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentClass);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) schoolName);
                    
                    NSDate *birtdayDate = [Utils parseServerDateStringToDate:birthdayStr];
                    
                    self.userInfoModel.firstName = firstName;
                    self.userInfoModel.lastName = lastName;
                    self.userInfoModel.sex = sex;
                    self.userInfoModel.birthday = birtdayDate;
                    self.userInfoModel.studentCode = studentCode;
                    self.userInfoModel.studentClass = studentClass;
                    self.userInfoModel.schoolName = schoolName;
                    self.userInfoModel.email = email;
                    self.userInfoModel.phoneNumber = phoneNumber;
                    self.userInfoModel.money = money;
                    self.userInfoModel.creditLimits = creditLimits;
                    self.userInfoModel.score = score;
                    self.userInfoModel.imageUrl = imageUrl;
                    
                    [self updateUserProfile];
                    
                }

            }

        }

    }];
    
}

#pragma mark - Update Userprofile

- (void)updateUserProfile {
    
    if(![UserData getSchoolImage] == nil){
        CGFloat imageWidth = self.backgroundCardprofile.frame.size.width;
        CGFloat imageHeight = self.backgroundCardprofile.frame.size.height;
        UIImage *schoolImage = [UIImage imageWithData:[UserData getSchoolImage]];
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(imageWidth, imageHeight), YES, 5.0);
        [schoolImage drawInRect:CGRectMake(0, 0, imageWidth, imageHeight)];
        UIImage *schoolImage2 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
         self.backgroundCardprofile.backgroundColor = [UIColor colorWithPatternImage:schoolImage2];
    }else{
        self.backgroundCardprofile.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"profile_box_bg"]];
    }
    
    
    if(self.userInfoModel != nil) {
        
        if(self.userInfoModel.imageUrl == nil || self.userInfoModel.imageUrl.length == 0) {
            if(self.userInfoModel.sex == 0) {
                self.userImageView.image = [UIImage imageNamed:@"ic_user_info"];
            }
            else {
                self.userImageView.image = [UIImage imageNamed:@"ic_user_women"];
            }
        }
        else {
            [self.userImageView loadImageFromURL:self.userImageView.imageURL];
        }
        
        self.nameValueLabel.text = self.userInfoModel.firstName;
        self.lastNameValueLabel.text = self.userInfoModel.lastName;
        self.schoolValueLabel.text = self.userInfoModel.schoolName;
        
        if(self.userInfoModel.sex == 0) {
            self.genderValueLabel.text = @"ชาย";
        }
        else {
            self.genderValueLabel.text = @"หญิง";
        }
        
        self.birthdayValueLabel.text = [self.dateFormatter stringFromDate:self.userInfoModel.birthday];
        self.phoneNumberValueLabel.text = self.userInfoModel.phoneNumber;
        self.emailValueLabel.text = self.userInfoModel.email;
        
        if([Utils isIntegerWithDouble:self.userInfoModel.money]) {
            self.remainingValueLabel.text = [[NSString alloc] initWithFormat:@"%.0f บาท", self.userInfoModel.money];
        }
        else {
            self.remainingValueLabel.text = [[NSString alloc] initWithFormat:@"%.2f บาท", self.userInfoModel.money];
        }
        
        
        if(self.userInfoModel.creditLimits == 0) {
            self.creditLimitsValueLabel.text = @"ไม่จำกัด";
        }
        else {
            self.creditLimitsValueLabel.text = [[NSString alloc] initWithFormat:@"%.02f บาท/วัน", self.userInfoModel.creditLimits];
        }
        
       
        
        if([UserData getUserType] == STUDENT) {
            self.classValueLabel.text = self.userInfoModel.studentClass;
            self.studentIDValueLabel.text = self.userInfoModel.studentCode;
        }
        else {
            
            if(self.classStackView != nil) {
                [self.classStackView removeFromSuperview];
            }
            
            if(self.studentIDStackView != nil) {
                [self.studentIDStackView removeFromSuperview];
            }
        }
    }
}

- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
    
}
@end
