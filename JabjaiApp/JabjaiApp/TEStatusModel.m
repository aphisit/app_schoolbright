//
//  TEStatusModel.m
//  JabjaiApp
//
//  Created by toffee on 1/16/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "TEStatusModel.h"

@implementation TEStatusModel

@synthesize userId = _userId;
@synthesize scanStatus = _scanStatus;
@synthesize studentId = _studentId;
@synthesize studentName = _studentName;
@synthesize teacherId = _teacherId;
@synthesize checkedTeacherName = _checkedTeacherName;
@synthesize authorized = _authorized;
@synthesize pic = _pic;
@synthesize statusCheck = _statusCheck;

- (void)setUserId:(long long)userId {
    _userId = userId;
}

- (void)setScanStatus:(int)scanStatus {
    _scanStatus = scanStatus;
}

- (void)setStudentId:(NSString *)studentId {
    _studentId = studentId;
}

- (void)setStudentName:(NSString *)studentName {
    _studentName = studentName;
}

- (void)setTeacherId:(long long)teacherId {
    _teacherId = teacherId;
}

- (void)setCheckedTeacherName:(NSString *)checkedTeacherName {
    _checkedTeacherName = checkedTeacherName;
}

- (void)setAuthorized:(BOOL)authorized {
    _authorized = authorized;
}

- (void)setStudentPic:(NSString *)studentPic {
    _pic = studentPic;
}

- (void)setStatusCheck:(BOOL)statusCheck{
    _statusCheck = statusCheck;
}

- (long long)getUserId {
    return _userId;
}

- (int)getScanStatus {
    return _scanStatus;
}

- (NSString *)getStudentId {
    return _studentId;
}

- (NSString *)getStudentName {
    return _studentName;
}

- (long long)getTeacherId {
    return _teacherId;
}

- (NSString *)getCheckedTeacherName {
    return _checkedTeacherName;
}

- (BOOL)getAuthorized {
    return _authorized;
}
- (NSString *)getStudentPic {
    return _pic;
}

- (BOOL)getstatusCheck{
    return _statusCheck;
}


@end
