//
//  SScheduleDetailModel.h
//  JabjaiApp
//
//  Created by mac on 5/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SScheduleDetailModel : NSObject

/*
 * status code
 * 0 : on time
 * 1 : late
 * 3 : absence
 * 7 : on leave
 * 8 : holiday
 */

@property (assign, nonatomic) long long subjectID;
@property (strong, nonatomic) NSString *subjectName;
@property (strong, nonatomic) NSString *subjectCode;
@property (strong, nonatomic) NSString *roomNumber;
@property (strong, nonatomic) NSString *classRoom;
@property (strong, nonatomic) NSString *lecturer;
@property (strong, nonatomic) NSString *courseStartTime;
@property (strong, nonatomic) NSString *courseEndTime;
@property (strong, nonatomic) NSString *clockInTime;
@property (assign, nonatomic) NSInteger clockInStatus;
@property (strong, nonatomic) NSString *clockOutTime;
@property (assign, nonatomic) NSInteger clockOutStatus;

@end
