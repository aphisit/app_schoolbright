//
//  UTeachingYearModel.h
//  JabjaiApp
//
//  Created by mac on 12/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "USemesterModel.h"

@interface UTeachingYearModel : NSObject

@property (nonatomic) int yearID;
@property (strong, nonatomic) NSString *yearName;
@property (strong, nonatomic) NSArray<USemesterModel *> *semesters;

@end
