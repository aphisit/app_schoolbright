//
//  ReportInOutAllDataSelectDepartmantTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 15/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Charts/Charts.h>
#import "ReportInOutStatusDepartmantViewController.h"
#import "NavigationViewController.h"
#import "SlideMenuController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutAllDataSelectDepartmantTableViewCell : UITableViewCell <SlideMenuControllerDelegate>

@property (nonatomic) long long departmentId;
@property (strong, nonatomic) NSDate *dayReport;
@property (strong, nonatomic) NSString *departmentName;

@property (weak, nonatomic) IBOutlet UILabel *headerOnTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAbsenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSickLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPersanalLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerOtherLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerHolidayLabel;


@property (weak, nonatomic) IBOutlet UIView *graphView;
@property (weak, nonatomic) IBOutlet UILabel *departmantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet BarChartView *barCharView;
@property (weak, nonatomic) IBOutlet UILabel *amountOnTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountAbsenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountSickLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountPersonalLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountOtherLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountHolidayLabel;

@property (weak, nonatomic) IBOutlet UILabel *onTimePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *latePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *absencePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *sickPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *otherPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayPercentLabel;

@property (weak, nonatomic) IBOutlet UIView *onTimeColorView;
@property (weak, nonatomic) IBOutlet UIView *lateColorView;
@property (weak, nonatomic) IBOutlet UIView *absenceColorView;
@property (weak, nonatomic) IBOutlet UIView *sickColorView;
@property (weak, nonatomic) IBOutlet UIView *personalColorView;
@property (weak, nonatomic) IBOutlet UIView *otherColorView;
@property (weak, nonatomic) IBOutlet UIView *holidayColorView;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *lookDetailBtn;

- (IBAction)showDetailAction:(id)sender;


- (void) setStatusGraph:(NSDictionary*)statusArray;


@end

NS_ASSUME_NONNULL_END
