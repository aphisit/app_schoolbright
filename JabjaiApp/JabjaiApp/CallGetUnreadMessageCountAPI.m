//
//  CallGetUnreadMessageCountAPI.m
//  JabjaiApp
//
//  Created by mac on 5/18/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetUnreadMessageCountAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetUnreadMessageCountAPI () {
    NSInteger connectCounter;
}

@end

@implementation CallGetUnreadMessageCountAPI

- (instancetype)init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(long long)userID schoolid:(long long)schoolid{
    [self getUnreadCountWithUserID:userID schoolid:schoolid];
}

- (void)call:(long long)userID requestCode:(NSString *)requestCode schoolid:(long long)schoolid{
    self.requestCode = requestCode;
    
    [self getUnreadCountWithUserID:userID schoolid:schoolid];
}

#pragma mark - Get API Data
- (void)getUnreadCountWithUserID:(long long)userID schoolid:(long long)schoolid {
    
    NSString *URLString = [APIURL getUnreadMessageCountWithUserID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFailed = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUnreadCountWithUserID:userID schoolid:schoolid];
            }
            else {
                
                isFailed = YES;
                connectCounter = 0;
            }
        }
        else {
            
            NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if(![Utils isInteger:responseString]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUnreadCountWithUserID:userID schoolid:schoolid];
                }
                else {
                    
                    isFailed = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                connectCounter = 0;
                
                NSInteger unreadCount = [responseString integerValue];
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetUnreadMessageCountAPI:unreadCount:success:)]) {
                    
                    [self.delegate callGetUnreadMessageCountAPI:self unreadCount:unreadCount success:YES];
                    
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetUnreadMessageCountAPI:unreadCount:requestCode:success:)]) {
                    [self.delegate callGetUnreadMessageCountAPI:self unreadCount:unreadCount requestCode:_requestCode success:YES];
                }
                
                return;
                
            }
            
        }
        
        // When get data failed call the delegate to send failed
        if(isFailed) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetUnreadMessageCountAPI:unreadCount:success:)]) {
                [self.delegate callGetUnreadMessageCountAPI:self unreadCount:0 success:NO];
            }
            
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetUnreadMessageCountAPI:unreadCount:requestCode:success:)]) {
                [self.delegate callGetUnreadMessageCountAPI:self unreadCount:0 requestCode:_requestCode success:NO];
            }
        }
        
    }];
}

@end
