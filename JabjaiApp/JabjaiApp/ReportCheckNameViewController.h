//
//  ReportCheckNameViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "CAPSPageMenu.h"
#import "ReportCheckFlagPoleViewController.h"
#import "ReportCheckSubjectViewController.h"
#import "ReportCheckFlagpoleSelectViewController.h"
#import "ReportCheckSubjectSelectViewController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface ReportCheckNameViewController : UIViewController <SlideMenuControllerDelegate, CAPSPageMenuDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>

@property (nonatomic) CAPSPageMenu *pageMenu;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (nonatomic, strong) ReportCheckFlagPoleViewController *flagPoleController;
@property (nonatomic, strong) ReportCheckSubjectViewController *subjectController;

- (IBAction)moveBack:(id)sender;

@end
