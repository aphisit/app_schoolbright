//
//  CallUpdateMargetPOSTAPI.h
//  JabjaiApp
//
//  Created by toffee on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CallUpdateMargetPOSTAPI;

@protocol CallUpdateMargetPOSTAPIDelegate <NSObject>

@optional
- (void)callUpdateMargetPOSTAPI:(CallUpdateMargetPOSTAPI *)classObj response:(NSDictionary *)response chackMonny:(NSString *)chackMonny success:(BOOL)success;

@end
@interface CallUpdateMargetPOSTAPI : NSObject
@property (nonatomic, weak) id<CallUpdateMargetPOSTAPIDelegate> delegate;

- (void)call:(long long)schoolId shopId:(long long)shopId userId:(long long)userId jsonString:(NSString *)jsonString;

@end
