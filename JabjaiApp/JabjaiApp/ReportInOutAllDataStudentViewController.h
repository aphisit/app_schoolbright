//
//  ReportInOutAllDataStudentViewController.h
//  JabjaiApp
//
//  Created by mac on 4/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Charts/Charts.h>
#import "CallReportInOutAPI.h"
#import "ReportInOutAllDataSelectLevelViewController.h"
#import "SlideMenuController.h"
#import "NavigationViewController.h"

@interface ReportInOutAllDataStudentViewController : UIViewController <CallReportInOutAPIDelegate, SlideMenuControllerDelegate>

@property (nonatomic) NSDate* date;

@property (weak, nonatomic) IBOutlet UILabel *headerOnTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAbsenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEventLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSickLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPersonalLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerUnCheckLabel;


@property (weak, nonatomic) IBOutlet BarChartView *barCharView;

@property (weak, nonatomic) IBOutlet UILabel *amountOnTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountAbsenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountEventLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountSickLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountPersonalLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountUnCheckLabel;

@property (weak, nonatomic) IBOutlet UILabel *onTimePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *latePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *absencePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *sickPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *unCheckPercentLabel;

@property (weak, nonatomic) IBOutlet UIView *onTimeColorView;
@property (weak, nonatomic) IBOutlet UIView *lateColorView;
@property (weak, nonatomic) IBOutlet UIView *absenceColorView;
@property (weak, nonatomic) IBOutlet UIView *eventColorView;
@property (weak, nonatomic) IBOutlet UIView *sickColorView;
@property (weak, nonatomic) IBOutlet UIView *personalColorView;
@property (weak, nonatomic) IBOutlet UIView *unCheckColorView;


@property (weak, nonatomic) IBOutlet UILabel *dateThaiLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountStudentAllLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UIButton *seeDetailBtn;

- (IBAction)nextAction:(id)sender;

@end
