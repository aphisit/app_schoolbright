//
//  JHClassRoomGroupViewController.h
//  JabjaiApp
//
//  Created by toffee on 12/18/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JHClassRoomGroupViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)actionNext:(id)sender;
- (IBAction)actionBack:(id)sender;



@end
