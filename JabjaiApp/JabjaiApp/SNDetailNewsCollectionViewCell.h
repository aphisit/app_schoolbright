//
//  SNDetailNewsCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 19/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SNDetailNewsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *fileimage;
@property (weak, nonatomic) IBOutlet UIButton *fileBtn;
@end

NS_ASSUME_NONNULL_END
