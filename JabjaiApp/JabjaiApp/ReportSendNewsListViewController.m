//
//  ReportSendNewsListViewController.m
//  JabjaiApp
//
//  Created by mac on 4/4/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportSendNewsListViewController.h"
#import "NewsMessageTableViewCell.h"
#import "ProgressTableViewCell.h"
#import "ReportSendNewsSortModel.h"
#import "ReportSendNewsListTableViewCell.h"
#import "ReportSendNewsDetailPagemenuViewController.h"
#import "NavigationViewController.h"
#import "ReportListTeacherViewController.h"
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"
#import "AppDelegate.h"
#import "AlertDialog.h"
#import "KxMenu.h"

#import <SDWebImage/UIImageView+WebCache.h>

static NSString *dataCellIdentifier = @"Cell";

@interface ReportSendNewsListViewController (){
    
    NSMutableDictionary<NSNumber *, NSArray<ReportSendNewsSortModel *> *> *reportSections;
    NSMutableArray<ReportSendNewsSortModel *> *messages;
    NSInteger connectCounter;
    UIColor *grayColor;
    UIRefreshControl *refreshControl;
    NSString *sortData;
    AlertDialog *alertDialog;
    NSManagedObject *schoolImage;
}

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property (nonatomic, strong) SNPageMenuReportNewsViewControllerXIB *sNPageMenuReportNewsViewControllerXIB;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation ReportSendNewsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    reportSections = [[NSMutableDictionary alloc] init];
    messages = [[NSMutableArray alloc] init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportSendNewsListTableViewCell class]) bundle:nil] forCellReuseIdentifier:dataCellIdentifier];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    
    self.timeFormatter = [Utils getDateFormatter];
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [self.timeFormatter setDateFormat:@"HH:mm"];
    }
    [self.moreButton addTarget:self action:@selector(showRightMenu:) forControlEvents:UIControlEventTouchUpInside];
    sortData = nil;
    [self getReportSendNewsDataWithPage:1 sort:sortData];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_SENDNEWS",nil,[Utils getLanguage],nil);
}

- (void)viewWillAppear:(BOOL)animated{
    
}

- (void)showRightMenu:(UIButton *)sender{
    
    UIImage *clockImage = [UIImage imageNamed:@"sort_with_clock"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [clockImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *clockImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *peopleImage = [UIImage imageNamed:@"sort_with_name"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [peopleImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *peopleImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSArray *menuItems =
    @[
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_SORTBY_NAME",nil,[Utils getLanguage],nil)
                     image:peopleImage2
                    target:self
                    action:@selector(sortNameItem:)],
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"LABEL_SENDNEWS_REP_SORTBY_DATE",nil,[Utils getLanguage],nil)
                     image:clockImage2
                    target:self
                    action:@selector(sortDateItem:)],
      ];
    
//    KxMenuItem *first = menuItems[0];
//    first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
//    first.alignment = NSTextAlignmentLeft;
    
    const CGFloat W = self.view.bounds.size.width;
    const CGFloat H = self.view.bounds.size.height;
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(W - 16, 18, 40, 40) menuItems:menuItems];
    
}

- (void) sortNameItem:(id)sender
{
    sortData = @"name";
    
    NSIndexPath *topPath = [NSIndexPath indexPathForRow:0 inSection:0];
   // [self.tableView scrollToRowAtIndexPath:topPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    double delayInsecond = 1;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        
        [self getReportSendNewsDataWithPage:1 sort:sortData];
        [self.tableView reloadData];
        
    });
    
    NSLog(@"%@", sortData);
}

- (void) sortDateItem:(id)sender
{
    sortData = @"day";
    
    NSIndexPath *topPath = [NSIndexPath indexPathForRow:0 inSection:0];
   // [self.tableView scrollToRowAtIndexPath:topPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    double delayInsecond = 1 ;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        
        [self getReportSendNewsDataWithPage:1 sort:sortData];
        [self.tableView reloadData];
        
    });
    
    NSLog(@"%@", sortData);
}


- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialog alloc] init];
    }
    else if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showFinishAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else{
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
    
}

- (void)onAlertDialogClose {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return reportSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger page = section +1;
    
    return [[reportSections objectForKey:@(page)] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReportSendNewsListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
    
    NSInteger page = indexPath.section + 1;
    ReportSendNewsSortModel *model = [[reportSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
    cell.imageIconSendNews.layer.cornerRadius = cell.imageIconSendNews.frame.size.height /2;
    cell.imageIconSendNews.layer.masksToBounds = YES;
    
    if ([model.imageNewsIcon isEqual:@""] || model.imageNewsIcon == nil) {
        cell.imageIconSendNews.image = [UIImage imageNamed:@"school"];
    }
    else{
        [cell.imageIconSendNews sd_setImageWithURL:[NSURL URLWithString:model.imageNewsIcon]];
    }
    cell.headTitle.text = model.username;
    cell.dateTitle.text = [NSString stringWithFormat:@"%@ %@", [self.timeFormatter stringFromDate:model.dateSend], [self.dateFormatter stringFromDate:model.dateSend]];
    cell.detailText.text = model.message;
    [cell.sectionView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.sectionView.layer setShadowOpacity:0.3];
    [cell.sectionView.layer setShadowRadius:3.0];
    [cell.sectionView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    return cell;
}


#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        
    NSInteger page = indexPath.section + 1;
    
    ReportSendNewsSortModel *model = [[reportSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    if (self.sNPageMenuReportNewsViewControllerXIB != nil) {
        self.sNPageMenuReportNewsViewControllerXIB = nil;
    }
    self.sNPageMenuReportNewsViewControllerXIB = [[SNPageMenuReportNewsViewControllerXIB alloc] init];
    [self.sNPageMenuReportNewsViewControllerXIB showNewsDetailController:self.view newsID:model.newsID sortData:@""];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)getReportSendNewsDataWithPage:(NSUInteger)page sort:(NSString *)sort{
    
    [self showIndicator];
    
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getReportSendNewsListWithSchoolID:schoolID page:page sort:sort];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getReportSendNewsDataWithPage:page sort:sort];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportSendNewsDataWithPage:page sort:sort];
                    
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportSendNewsDataWithPage:page sort:sort];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                
                if (page == 1) {
                    reportSections = [[NSMutableDictionary alloc] init];
                }
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int newsID = [[dataDict objectForKey:@"news_id"] intValue];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"message"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"message"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"title"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"title"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *newsDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"daysend"]];
                    
                    NSMutableString *sender;
                    
                    if(![[dataDict objectForKey:@"user_name"] isKindOfClass:[NSNull class]]) {
                        sender = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"user_name"]];
                    }
                    else {
                        sender = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSString *imageSchoolIconUrl;
                    
                    if(![[dataDict objectForKey:@"picture"] isKindOfClass:[NSNull class]]){
                        imageSchoolIconUrl = [dataDict objectForKey:@"picture"];
                    }
                    else{
                        imageSchoolIconUrl = @"";
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) newsDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    
                    NSRange dotRange = [newsDateStr rangeOfString:@"."];
                    
                    NSDate *newsDate;
                    
                    if(dotRange.length != 0) {
                        newsDate = [formatter dateFromString:newsDateStr];
                    }
                    else {
                        newsDate = [formatter2 dateFromString:newsDateStr];
                    }
                    
                    ReportSendNewsSortModel *model = [[ReportSendNewsSortModel alloc] init];
                    
                    model.newsID = newsID;
                    model.message = message;
                    model.username = sender;
                    model.dateSend = newsDate;
                    model.title = title;
                    model.imageNewsIcon = imageSchoolIconUrl;
                
                    
                    [newDataArr addObject:model];
                    
                }
                [messages removeAllObjects];
                messages = [[NSMutableArray alloc] initWithArray:newDataArr];
                [reportSections setObject:newDataArr forKey:@(page)];
                [self.tableView reloadData];
            }
        }
    }];
    
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)fetchMoreData{
    if (messages.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *lastRowIndexPath = [visiblePaths lastObject];
        NSInteger numberOfSections = [self.tableView numberOfSections];
        NSInteger lastRowSection = [lastRowIndexPath section];
        NSInteger lastRowRow = [lastRowIndexPath row];
        NSInteger numberOfRowInsection = [self.tableView numberOfRowsInSection:lastRowSection];
        
        if (lastRowSection == numberOfSections -1 && lastRowRow == numberOfRowInsection - 1) {
            if (messages.count % 20 == 0) {
                NSInteger nextpage = lastRowSection + 2;

                [self getReportSendNewsDataWithPage:nextpage sort:sortData];
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self fetchMoreData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self fetchMoreData];
}

- (void)showIndicator{
    if (![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator{
    [self.indicator stopAnimating];
}

- (IBAction)moveBack:(id)sender {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
