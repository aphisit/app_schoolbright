//
//  LoginListAllSchoolNameModel.m
//  JabjaiApp
//
//  Created by toffee on 26/4/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "LoginListAllSchoolNameModel.h"

@implementation LoginListAllSchoolNameModel
@synthesize schoolID = _schoolID;
@synthesize schoolName = _schoolName;

-(void)setSchoolID:(long long)schoolID{
    _schoolID = schoolID;
}
-(void)setSchoolName:(NSString *)schoolName{
    _schoolName = schoolName;
}

-(long long)getSchoolID{
    return _schoolID;
}
-(NSString*)getSchoolName{
    return _schoolName;
}
@end
