//
//  HWTDetailPersonViewControllerXIB.h
//  JabjaiApp
//
//  Created by Mac on 10/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageInboxScrollableTextDialogCollectionViewCell.h"
#import "saveImageXIBViewController.h"
#import "ReportOrderHomeWorkReadFileViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface HWTDetailPersonViewControllerXIB : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, assign) long long homeworkID;

@property (weak, nonatomic) IBOutlet UILabel *headerSubjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRecipientLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerReadLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerUnReadLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDateSendLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDateAlertLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachFileLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;

@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet UILabel *allCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *unreadCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateNotiLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

//@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *loadBackgroundView;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *fileCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highImaCollectionConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highFileCollectionConstraint;

- (void)getReportOrderHomeworkDetailWithHomeworkID:(long long)homeworkId;

@end

NS_ASSUME_NONNULL_END
