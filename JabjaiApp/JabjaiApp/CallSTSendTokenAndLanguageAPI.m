//
//  CallSTSendTokenAndLanguageAPI.m
//  JabjaiApp
//
//  Created by Mac on 16/2/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import "CallSTSendTokenAndLanguageAPI.h"
#import "APIURL.h"
#import "Utils.h"
@implementation CallSTSendTokenAndLanguageAPI{
    NSInteger connectCounter,statusCode;
    
}
- (void)call:(NSString *)token language:(NSString *)language{
    connectCounter = 0;
    
    [self getStatusSendTokenAndLanguage:token language:language];
}
- (void) getStatusSendTokenAndLanguage:(NSString *)token language:(NSString *)language{
    NSString *URLString = [APIURL getStatusSendTokenAndLanguage:token language:language];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }

            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;

                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getStatusSendTokenAndLanguage:token language:token];;
;

            }
            else {
                isFail = YES;
                self->connectCounter = 0;
            }

        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getStatusSendTokenAndLanguage:token language:token];
;
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
                
            }
            else {
                NSDictionary *dic = returnedData;
                
                self->statusCode = [[dic objectForKey:@"StatusCode"] integerValue];
               
                NSLog(@"xxxx");
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callSTSendTokenAndLanguageAPI:statusCode:success:)]) {
                    [self.delegate callSTSendTokenAndLanguageAPI:self statusCode: self->statusCode success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callSTSendTokenAndLanguageAPI:statusCode:success:)]) {
                [self.delegate callSTSendTokenAndLanguageAPI:self statusCode:self->statusCode success:NO];
            }
        }
     
        
    }
     ];
}
@end
