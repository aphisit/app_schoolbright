//
//  ReportHomeWorkMonthlyModel.h
//  JabjaiApp
//
//  Created by mac on 1/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportHomeWorkMonthlyModel : NSObject

@property (strong , nonatomic) NSDate *homeworkList;

@end
