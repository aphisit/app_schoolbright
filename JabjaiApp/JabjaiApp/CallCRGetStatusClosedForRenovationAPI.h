//
//  CallCRGetStatusClosedForRenovationAPI.h
//  JabjaiApp
//
//  Created by Mac on 9/2/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CallCRGetStatusClosedForRenovationAPI;
@protocol CallCRGetStatusClosedForRenovationAPIDelegate <NSObject>

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString*)message success:(BOOL)success;

@end
@interface CallCRGetStatusClosedForRenovationAPI : NSObject
@property (nonatomic,weak) id<CallCRGetStatusClosedForRenovationAPIDelegate> delegate;

-(void)callStatusServer;

@end

NS_ASSUME_NONNULL_END
