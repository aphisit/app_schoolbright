//
//  PhotoAlbumLibary.m
//  JabjaiApp
//
//  Created by toffee on 3/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "PhotoAlbumLibary.h"
//#include <AssetsLibrary/AssetsLibrary.h>
//#import <AssetsLibrary/AssetsLibrary.h>

#import <SDWebImage/UIImageView+WebCache.h>
#import "Utils.h"
@interface PhotoAlbumLibary (){
    //ALAssetsLibrary *library;
    NSMutableArray *imageArray;
    NSMutableArray *selectedIndexArr,*selectImage;
    PHImageRequestOptions *option;
    BOOL sizeLimit;
}
@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic, copy) NSArray *fetchResults;
@property (nonatomic, copy) NSArray *assetCollections;
@property (nonatomic, strong) PHFetchResult *userAlbum;
@property (nonatomic, strong) PHAssetCollection *assetCollection;
@property (nonatomic, strong) PHFetchResult *assets;
@property (nonatomic, assign) CGSize cellPortraitSize;
@property (nonatomic, assign) CGSize cellLandscapeSize;
@end
static int count=0;
static const NSUInteger photoColumns = 3;
static NSString  *cellIdentifier = @"Cell";
@implementation PhotoAlbumLibary

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.contentView.layer.masksToBounds = YES;
    self.isShowing = NO;
    [self.albumCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([PhotoAlbumCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
   
    self.cellPortraitSize = self.cellLandscapeSize = CGSizeZero;
    selectedIndexArr = [[NSMutableArray alloc] init];
   // option = [PHImageRequestOptions new];
    selectedIndexArr = [[NSMutableArray alloc] init];
    selectImage = [[NSMutableArray alloc] init];
    self.albumCollectionView.allowsMultipleSelection = YES;
    self.albumCollectionView.delegate = self;
    self.albumCollectionView.dataSource = self;
    [self setLanguage];
    
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradient;
    //set color header
    [self.headerView layoutIfNeeded];
    gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

- (void)setLanguage{
    self.headerAllpictureLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ALBUM_ALL_PICTURE",nil,[Utils getLanguage],nil);
    [self.headerDoneLabel setTitle:NSLocalizedStringFromTableInBundle(@"LABEL_ALBUM_DONE",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

- (void)setupCellSize
{
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.albumCollectionView.collectionViewLayout;
    
    // Fetch shorter length
    CGFloat arrangementLength = MIN(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    
    CGFloat minimumInteritemSpacing = layout.minimumInteritemSpacing;
    UIEdgeInsets sectionInset = layout.sectionInset;
    
    CGFloat totalInteritemSpacing = MAX((photoColumns - 1), 0) * minimumInteritemSpacing;
    CGFloat totalHorizontalSpacing = totalInteritemSpacing + sectionInset.left + sectionInset.right;
    
    // Caculate size for portrait mode
    CGFloat size = (CGFloat)floor((arrangementLength - totalHorizontalSpacing) / photoColumns);
    self.cellPortraitSize = CGSizeMake(size, size);
    
    // Caculate size for landsacpe mode
    arrangementLength = MAX(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    NSUInteger numberOfPhotoColumnsInLandscape = (arrangementLength - sectionInset.left + sectionInset.right)/size;
    totalInteritemSpacing = MAX((numberOfPhotoColumnsInLandscape - 1), 0) * minimumInteritemSpacing;
    totalHorizontalSpacing = totalInteritemSpacing + sectionInset.left + sectionInset.right;
    size = (CGFloat)floor((arrangementLength - totalHorizontalSpacing) / numberOfPhotoColumnsInLandscape);
    self.cellLandscapeSize = CGSizeMake(size, size);
}



- (void) showPhotoAlbumView:(UIView *)targetView{
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height-20)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
     // Initialize dialog view hide it
    [self.contentView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentView setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
    [self getAllPhotosFromCamera];
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
    
}

- (void)dismissPhotoAlbum {
    [self removeDialogFromView];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(replyPhotoAlbum:imageArray:)]) {
        [self.delegate replyPhotoAlbum:self imageArray:selectImage];
    }
}

- (BOOL)isPhotoAlbumShowing {
    return self.isShowing;
}

//get photo
-(void)getAllPhotosFromCamera
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (status == PHAuthorizationStatusAuthorized) {
                 // Access has been granted.
            PHFetchResult *photosResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
            imageArray = [[NSMutableArray alloc] init];
            imageArray = [@[] mutableCopy];
            for (int i = [photosResult count]-1; i >= 0; i--){
                PHAsset *asset = [photosResult objectAtIndex:i];
                if (asset != nil) {
                    [imageArray addObject:asset];
                }
            }
            NSLog(@"x3");
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.albumCollectionView reloadData];// You can direct use NSMutuable Array images
            });
         }
     ];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
     PhotoAlbumCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    UIImageView *checked = (UIImageView*)[cell viewWithTag:44];
    if (checked==nil) {
        checked = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        checked.tag = 44;
        checked.center = CGPointMake(cell.frame.size.width/2, cell.frame.size.height/2);
        [cell.image addSubview:checked];
    }
    NSLog(@"xxxxx = %d",cell.selected);
    
    PHAsset *img = imageArray[indexPath.row];
    if (cell.selected && [selectedIndexArr containsObject:img]) {
        [checked setImage:[UIImage imageNamed:@"pa_icon_check"]];
        [cell setBackgroundColor:[UIColor redColor]];
    }else{
        [checked setImage:nil];
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    
    CGFloat retina = [UIScreen mainScreen].scale;
    CGSize square = CGSizeMake(cell.bounds.size.width * retina, cell.bounds.size.height * retina);
    option.synchronous = YES;
    [[PHImageManager defaultManager] requestImageForAsset:imageArray[indexPath.row]
                                               targetSize:square contentMode:PHImageContentModeAspectFill options:option resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                                                   cell.image.image = result;
                                                   // cell.image.image = [self compressImage:result];
        
                                               }];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //return CGSizeMake(70, 70);
    if (CGSizeEqualToSize(CGSizeZero, self.cellPortraitSize)
        || CGSizeEqualToSize(CGSizeZero, self.cellLandscapeSize)) {
        [self setupCellSize];
    }
    
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft
        || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeRight) {
        return self.cellLandscapeSize;
    }
    return self.cellPortraitSize;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2.5;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(320, 10);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}
 

#pragma mark -  Collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoAlbumCollectionViewCell *cell = (PhotoAlbumCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if ([selectedIndexArr count]==10) {
        cell.selected = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Limit Exceed" message:@"You cannot select more than 10 images." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }else{
        cell.backgroundColor = [UIColor redColor];
        cell.selected = YES;
        UIImageView *checked = (UIImageView*)[cell viewWithTag:44];
        [checked setImage:[UIImage imageNamed:@"pa_icon_check"]];
        if (imageArray > 0 ) {
            PHAsset *img = imageArray[indexPath.row];
            [selectedIndexArr addObject:img];
        }
        CGFloat retina = [UIScreen mainScreen].scale;
        CGSize square = CGSizeMake(cell.bounds.size.width * retina, cell.bounds.size.height * retina);
        option = [[PHImageRequestOptions alloc] init];
        option.resizeMode   = PHImageRequestOptionsResizeModeExact;
        option.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        option.synchronous = YES;
        [[PHImageManager defaultManager] requestImageForAsset:imageArray[indexPath.row]
                                                     targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeDefault options:option resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                                                         if (result != nil) {
                                                             [self->selectImage addObject:[self compressImage:result]];
                                                             //[self compressImage:result];
                                                         }
                                                     }
        ];
        
    }
}

- (UIImage*)compressImage:(UIImage *)image{

    NSData *imgData = UIImageJPEGRepresentation(image, 1); //1 it represents the quality of the image.
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imgData length]);
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 1.0;//50 percent compression
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }

    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imageData length]);
    return [UIImage imageWithData:imageData];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoAlbumCollectionViewCell *cell = (PhotoAlbumCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    UIImageView *checked = (UIImageView*)[cell viewWithTag:44];
    [checked setImage:nil];
    NSInteger numIndex = [selectedIndexArr indexOfObject:imageArray[indexPath.row]];
    
    if (imageArray.count>0) {
        PHAsset *img = imageArray[indexPath.row];
        if ([selectedIndexArr containsObject:img]) {
            NSLog(@"YES");
            [selectedIndexArr removeObject:img];
            [selectImage removeObjectAtIndex:numIndex];
        }
    }
}

- (IBAction)moveAction:(id)sender {
    [self dismissPhotoAlbum];
}

- (IBAction)chooseAction:(id)sender {
    [self dismissPhotoAlbum];
}
@end
