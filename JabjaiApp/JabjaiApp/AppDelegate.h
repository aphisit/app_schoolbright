//
//  AppDelegate.h
//  JabjaiApp
//
//  Created by mac on 10/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <JabjaiApp-Swift.h>
#import "reachabilityChangedDialog.h"
#import "CallSTSendTokenAndLanguageAPI.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,reachabilityChangedDialogDelegate, CallSTSendTokenAndLanguageAPIDelegate>

//@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (strong, nonatomic) NSString *deeplink;
@property (nonatomic) NSInteger deeplink_message_id;



//coredata
@property (strong, nonatomic) NSManagedObjectContext
*managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel
*managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator
*persistentStoreCoordinator;
//Reachability
@property (strong, nonatomic) reachabilityChangedDialog *viewController;

- (void)saveContext;

+ (void)downloadDataFromURL:(NSURL *)url withCompletionHandler:(void(^)(NSData *data))completionHandler;

@end

