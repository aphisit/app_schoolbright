//
//  CallReportUnCheckListDataAPI.m
//  JabjaiApp
//
//  Created by toffee on 21/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallReportUnCheckListDataAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallReportUnCheckListDataAPI () {
    NSInteger connectCounter;
    NSMutableArray <ReportUnCheckListDataModel*>* dataArray;
  
}
@end
@implementation CallReportUnCheckListDataAPI

- (void)call:(long long)schoolId date:(NSString*)date{
    connectCounter = 0;
    [self doGetDataUnCheckListdata:schoolId date:date];
}

-(void)doGetDataUnCheckListdata:(long long)schoolId date:(NSString *)date {
    NSString *URLString = [APIURL getReportUnCheckListData:schoolId date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self doGetDataUnCheckListdata:schoolId date:date];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                     [self doGetDataUnCheckListdata:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self doGetDataUnCheckListdata:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                NSString *classLevel, *teacherName;
                connectCounter = 0;
                dataArray = [[NSMutableArray alloc] init];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    if(![[dataDict objectForKey:@"class_name"] isKindOfClass:[NSNull class]]) {
                        
                        classLevel = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"class_name"]];
                    }
                    else {
                        classLevel = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"teacher_name"] isKindOfClass:[NSNull class]]) {
                        
                        teacherName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"teacher_name"]];
                    }
                    else {
                        teacherName = [[NSMutableString alloc] initWithString:@""];
                    }
                   
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) classLevel);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) teacherName);
                    ReportUnCheckListDataModel *model = [[ReportUnCheckListDataModel alloc] init];

                    [model setLevel:classLevel];
                    [model setTeacherName:teacherName];

                    [dataArray addObject:model];
                }
                
                
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportUnCheckListDataAPI:dateTimeArray:success:)]) {
                    [self.delegate callReportUnCheckListDataAPI:self dateTimeArray:dataArray success:YES];
                }
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportUnCheckListDataAPI:dateTimeArray:success:)]) {
                    [self.delegate callReportUnCheckListDataAPI:self dateTimeArray:dataArray success:NO];
                }
            }
            
        }
        
    }];
}

@end
