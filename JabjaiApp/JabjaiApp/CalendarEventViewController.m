//
//  CalendarEventViewController.m
//  JabjaiApp
//
//  Created by mac on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CalendarEventViewController.h"
#import "CalendarEventAllModel.h"
#import "CalendarEventModel.h"
#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"
#import "DateUtility.h"

#import "CalendarEventTableViewCell.h"

#import "EventCalendarDetailDialog.h"

@interface CalendarEventViewController (){
    UIColor *grayColor;
    UIColor *headerTitleColor;
    UIColor *todayColor;
    UIColor *selectionColor;
    
    NSMutableDictionary *fillDefaultColors;
    NSMutableDictionary *fillSelectionColors;
    NSMutableDictionary *borderSelectionColors;
    NSMutableDictionary *titleDefaultColors;
    NSMutableDictionary *titleSelectionColors;
    
    NSMutableArray *weekendDays;
    NSMutableArray *holidayArray;
    NSInteger connectCounter;
    NSMutableArray<CalendarEventModel *> *eventArray;
    UIColor *brightTurquoise, *sulu, *starShip, *blushPink;
    UIColor *dodgerBlue, *aqua, *monaLisa, *bitterSweet, *ripTide, *mountainMeadow, *perfume, *heliotRope, *alto, *dustyGray, *peachOrange, *texasRose;
    
    UIScrollView  *myScroll;
    
    float start;
    float end;

    NSDate *methodStart;
    NSDate *methodEnd;
     
    
}

@property (nonatomic) CGFloat shadowRadius;
@property (nonatomic) CGFloat shadowOpacity;
@property (nonatomic) CGFloat animationDuration;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (nonatomic) NSCalendar *gregorian;
@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;
@property (strong, nonatomic) UIColor *eventGreenColor;
@property (strong, nonatomic) UIColor *todayColor;
@property (strong, nonatomic) UIColor *customTextSelectedColor;
@property (strong, nonatomic) UIColor *customTextDeselectedColor;
@property (nonatomic, strong) EventCalendarDetailDialog *eventCalendarDialog;
@property (nonatomic) NSDate *considerDate;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation CalendarEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    SWRevealViewController *revealViewController = self.revealViewController;
    eventArray = [[NSMutableArray alloc] init];
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    selectionColor = [UIColor colorWithRed:212/255.0 green:44/255.0 blue:31/255.0 alpha:1.0];
    
    //04/05/2017

    dodgerBlue = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0]; // Now Full
    aqua = [UIColor colorWithRed:0.21 green:0.89 blue:1.00 alpha:1.0]; // Now Select
    monaLisa = [UIColor colorWithRed:1.00 green:0.68 blue:0.62 alpha:1.0]; // Color status 0 Before Select
    bitterSweet = [UIColor colorWithRed:1.00 green:0.48 blue:0.38 alpha:1.0]; // Color status 0 After Select
    ripTide = [UIColor colorWithRed:0.47 green:0.93 blue:0.71 alpha:1.0]; // Color status 1 Before Select
    mountainMeadow = [UIColor colorWithRed:0.13 green:0.76 blue:0.46 alpha:1.0]; // Color status 1 After Select
    perfume = [UIColor colorWithRed:0.80 green:0.62 blue:0.98 alpha:1.0]; // Color status 2 Before Select
    heliotRope = [UIColor colorWithRed:0.75 green:0.51 blue:1.00 alpha:1.0]; // Color status 2 After Select
    alto = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1.0]; // Color status 3 Before Select
    dustyGray = [UIColor colorWithRed:0.60 green:0.60 blue:0.60 alpha:1.0]; // Color status 3 After Select
    peachOrange = [UIColor colorWithRed:1.00 green:0.85 blue:0.59 alpha:1.0]; // Color status 4 Before Select
    texasRose = [UIColor colorWithRed:1.00 green:0.75 blue:0.31 alpha:1.0]; // Color status 4 After Select
    
    
    
    // 25/10/2017
    self.eventGreenColor = [UIColor colorWithRed:62/255.0 green:194/255.0 blue:45/255.0 alpha:1.0];
    self.todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    self.customTextSelectedColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.customTextDeselectedColor = [UIColor colorWithRed:190/255.0 green:194/255.0 blue:197/255.0 alpha:1.0];
    
    self.gregorian = [Utils getGregorianCalendar];
    
    self.minimumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
    self.maximumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:1 toDate:[NSDate date] options:0];
    
    // 25/10/2017
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.shadowRadius = 5.0f;
    self.shadowOpacity = 0.5f;
    self.animationDuration = 0.55f;
    

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height >= 1024.0f) {
            self.heightCalendarConstraint.constant = 450.0f;
        }
    }
    else{
        self.heightCalendarConstraint.constant = 300.0f;
    }
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = grayColor;
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_up"] forState:UIControlStateNormal];
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_down"] forState:UIControlStateSelected];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CalendarEventTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    self.considerDate = [NSDate date];
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:self.considerDate];
    [self getCalendarEventListWithDate:self.considerDate];
    [self setlanguage];
    
  
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(upSwipe:)];
    [swipeLeft setDirection: UISwipeGestureRecognizerDirectionUp ];
    [self.contentTableView addGestureRecognizer:swipeLeft];
    swipeLeft=nil;

    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(downSwipe:)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.contentTableView addGestureRecognizer:swipeRight];
}

- (void)viewWillAppear:(BOOL)animated {
    [self getCalendarEventListWithDate:[NSDate date]];
    [self createCalendar];
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    [self.contentTableView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.contentTableView.layer setShadowOpacity:0.3];
    [self.contentTableView.layer setShadowRadius:10.0];
    [self.contentTableView.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    
}

-(void)setlanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CALENDA_SCHOOL",nil,[Utils getLanguage],nil);
}

- (void)createCalendar {
    
    // Remove all subviews from container
    NSArray *viewsToRemove = self.containerView.subviews;
    for(UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    //self.containerView.backgroundColor = grayColor;    
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, height)];
    calendar.dataSource = self;
    calendar.delegate = self;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: [UserData getChangLanguage]];
    calendar.locale = locale;
    calendar.backgroundColor = [UIColor whiteColor];
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
    // 04/05/2018
    calendar.appearance.todayColor = dodgerBlue;
    calendar.appearance.titleTodayColor = [UIColor whiteColor];
    calendar.appearance.todaySelectionColor = dodgerBlue;
    
    calendar.appearance.selectionColor = [UIColor clearColor];
    calendar.appearance.titleSelectionColor = [UIColor blackColor];
    calendar.appearance.borderSelectionColor = dodgerBlue;
    
    [self.containerView addSubview:calendar];
    self.calendar = calendar;
   
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 95, 34);
    previousButton.backgroundColor = [UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:previousButton];
    self.previousButton = previousButton;
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(self.view.frame.size.width - nextButton.frame.size.width - 95, 5, 95, 34);
    nextButton.backgroundColor = [UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:nextButton];
    self.nextButton = nextButton;
    [self getHolidayList:calendar.currentPage];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - FSCalendarDataSource

- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

#pragma mark - FSCalendarDelegate

- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
    if([DateUtility sameMonthOfYear:date date2:calendar.currentPage]) {
        return YES;
    }
    
    return NO;
}



- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:date];
    self.considerDate = date;
    [self getCalendarEventListWithDate:date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd/MM/yyyy";
    NSLog(@"%@", [formatter stringFromDate:date]);
}

#pragma mark - Class buttons event

- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
    
    [self getHolidayList:previousMonth];
}

- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
    
    [self getHolidayList:nextMonth];
}
#pragma mark - upSwipe, downSwipe
-(void)upSwipe:(UISwipeGestureRecognizer *)gesture
{
    self.arrowStatement.selected = !self.arrowStatement.selected;
       
       if (self.arrowStatement.isSelected == YES) {
           //        self.topDetailConstraint.constant = -300.0f;
           
           if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
               CGSize screenSize = [[UIScreen mainScreen] bounds].size;
               if (screenSize.height >= 1024.0f) {
                   [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                       self.topDetailConstraint.constant = 0.0f;
                       [self.view layoutIfNeeded];
                   } completion:^(BOOL finished) {
                       [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                           self.topDetailConstraint.constant = -450.0f;
                           [self.view layoutIfNeeded];
                       } completion:^(BOOL finished) {
                           
                       }];
                   }];
               }
           }
           else{
               [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                   self.topDetailConstraint.constant = 0.0f;
                   [self.view layoutIfNeeded];
               } completion:^(BOOL finished) {
                   [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                       self.topDetailConstraint.constant = -300.0f;
                       [self.view layoutIfNeeded];
                   } completion:^(BOOL finished) {
                       
                   }];
               }];
           }
           
       }else{
           //        self.topDetailConstraint.constant = 0.0f;
           if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
               CGSize screenSize = [[UIScreen mainScreen] bounds].size;
               if (screenSize.height >= 1024.0f) {
                   [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                       self.topDetailConstraint.constant = -450.0f;
                       [self.view layoutIfNeeded];
                   } completion:^(BOOL finished) {
                       [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                           self.topDetailConstraint.constant = 0.0f;
                           [self.view layoutIfNeeded];
                       } completion:^(BOOL finished) {
                           
                       }];
                   }];
               }
           }
           else{
               [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                   self.topDetailConstraint.constant = -300.0f;
                   [self.view layoutIfNeeded];
               } completion:^(BOOL finished) {
                   [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                       self.topDetailConstraint.constant = 0.0f;
                       [self.view layoutIfNeeded];
                   } completion:^(BOOL finished) {
                       
                   }];
               }];
           }
       }
       
}

-(void)downSwipe:(UISwipeGestureRecognizer *)gesture
{
   self.arrowStatement.selected = !self.arrowStatement.selected;
      
      if (self.arrowStatement.isSelected == YES) {
          //        self.topDetailConstraint.constant = -300.0f;
          
          if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
              CGSize screenSize = [[UIScreen mainScreen] bounds].size;
              if (screenSize.height >= 1024.0f) {
                  [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                      self.topDetailConstraint.constant = 0.0f;
                      [self.view layoutIfNeeded];
                  } completion:^(BOOL finished) {
                      [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                          self.topDetailConstraint.constant = -450.0f;
                          [self.view layoutIfNeeded];
                      } completion:^(BOOL finished) {
                          
                      }];
                  }];
              }
          }
          else{
              [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                  self.topDetailConstraint.constant = 0.0f;
                  [self.view layoutIfNeeded];
              } completion:^(BOOL finished) {
                  [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                      self.topDetailConstraint.constant = -300.0f;
                      [self.view layoutIfNeeded];
                  } completion:^(BOOL finished) {
                      
                  }];
              }];
          }
          
      }else{
          //        self.topDetailConstraint.constant = 0.0f;
          if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
              CGSize screenSize = [[UIScreen mainScreen] bounds].size;
              if (screenSize.height >= 1024.0f) {
                  [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                      self.topDetailConstraint.constant = -450.0f;
                      [self.view layoutIfNeeded];
                  } completion:^(BOOL finished) {
                      [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                          self.topDetailConstraint.constant = 0.0f;
                          [self.view layoutIfNeeded];
                      } completion:^(BOOL finished) {
                          
                      }];
                  }];
              }
          }
          else{
              [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                  self.topDetailConstraint.constant = -300.0f;
                  [self.view layoutIfNeeded];
              } completion:^(BOOL finished) {
                  [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                      self.topDetailConstraint.constant = 0.0f;
                      [self.view layoutIfNeeded];
                  } completion:^(BOOL finished) {
                      
                  }];
              }];
          }
      }
}

- (IBAction)openDetail:(id)sender {
    
    self.arrowStatement.selected = !self.arrowStatement.selected;
    
    if (self.arrowStatement.isSelected == YES) {
        //        self.topDetailConstraint.constant = -300.0f;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailConstraint.constant = -450.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailConstraint.constant = 0.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = -300.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
        
    }else{
        //        self.topDetailConstraint.constant = 0.0f;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = -450.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailConstraint.constant = 0.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailConstraint.constant = -300.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
    }
    
    
}


- (void)calendarCurrentPageDidChange:(CFSCalendar *)calendar {
    
    NSString *dateAsString = [self.dateFormatter stringFromDate:calendar.currentPage];
    NSLog(@"%@", [NSString stringWithFormat:@"Did change page %@", dateAsString]);
    
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date {
    
    if(fillDefaultColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if([fillDefaultColors objectForKey:key] != nil) {
            return [fillDefaultColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date{
    
    if (fillSelectionColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([fillSelectionColors objectForKey:key] != nil) {
            return [fillSelectionColors objectForKey:key];
        }
    }
    
    return nil;
    
}
- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance borderSelectionColorForDate:(NSDate *)date{
    
    if (borderSelectionColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([borderSelectionColors objectForKey:key] != nil) {
            return [borderSelectionColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date{
    
    if (titleDefaultColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([titleDefaultColors objectForKey:key] != nil) {
            return [titleDefaultColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleSelectionColorForDate:(NSDate *)date{
    
    if (titleSelectionColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if ([titleSelectionColors objectForKey:key] != nil) {
            return [titleSelectionColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (void)getHolidayList:(NSDate *)date{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getEventCalendarListWithUserID:userID date:date schoolId:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            //            [self stopIndicator];
            
            if (error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if (data == nil){
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if ((connectCounter < TRY_CONNECT_MAX)) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getHolidayList:date];
            }
            else{
                connectCounter = 0;
            }
        }
        
        else{
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getHolidayList:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getHolidayList:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                holidayArray = [[NSMutableArray alloc] init];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *datadict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *holidayStr = [[NSMutableString alloc] initWithFormat:@"%@", [datadict objectForKey:@"date"]];
                    
//                    int *colorDate = [[datadict objectForKey:@"color"]intValue];
                    
//                    NSString *colorDate;
//
//                    if(![[datadict objectForKey:@"color"] isKindOfClass:[NSNull class]]) {
//                        colorDate = [[NSMutableString alloc] initWithString:[datadict objectForKey:@"color"]];
//                    }
//                    else {
//                        colorDate = [[NSMutableString alloc] initWithString:@""];
//                    }
                    
                    NSString *colorDate = [datadict objectForKey:@"color"];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)holidayStr);
                    
                    NSInteger colorDay;
                    
                    if([colorDate isKindOfClass:[NSNull class]]) {
                        colorDay = 1;
                    }
                    else {
                        colorDay = [colorDate integerValue];
                    }
                    
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *holiday = [formatter dateFromString:holidayStr];
                    
                    CalendarEventAllModel *model = [[CalendarEventAllModel alloc] init];
                    
                    [model setHolidayList:holiday];
                    [model setColor:[[NSNumber alloc] initWithInteger:colorDay]];
                    
                    [holidayArray addObject:model];
                }
                
                [self updateEventHoliday];
                
            }
            
            
        }
    }];
    
}

- (void)updateEventHoliday{
    
    
    if (holidayArray == nil) {
        return;
    }
    
    if (fillDefaultColors != nil) {
        [fillDefaultColors removeAllObjects];
    }
    
    if (fillSelectionColors != nil) {
        [fillSelectionColors removeAllObjects];
    }
    
    if (borderSelectionColors != nil) {
        [borderSelectionColors removeAllObjects];
    }
    
    if (titleDefaultColors != nil) {
        [titleDefaultColors removeAllObjects];
    }
    
    if (titleSelectionColors != nil) {
        [titleSelectionColors removeAllObjects];
    }
    
    fillDefaultColors = [[NSMutableDictionary alloc] init];
    fillSelectionColors = [[NSMutableDictionary alloc] init];
    borderSelectionColors = [[NSMutableDictionary alloc] init];
    titleDefaultColors = [[NSMutableDictionary alloc] init];
    titleSelectionColors = [[NSMutableDictionary alloc] init];
    
    for (CalendarEventAllModel *model in holidayArray) {
        
        NSString *dateKey = [self.dateFormatter stringFromDate:model.holidayList];
//        [fillDefaultColors setObject:self.eventGreenColor forKey:dateKey];
        
        NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:model.holidayList];
        NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
        if([today day] == [otherDay day] &&
           [today month] == [otherDay month] &&
           [today year] == [otherDay year] &&
           [today era] == [otherDay era]) {
            
            [fillDefaultColors setObject:dodgerBlue forKey:dateKey];
            [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
            [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
        }
        else{
            
            if ([model.color intValue] == 0) {
                
                [fillDefaultColors setObject:monaLisa forKey:dateKey];
                [fillSelectionColors setObject:bitterSweet forKey:dateKey];
                [borderSelectionColors setObject:[UIColor clearColor] forKey:dateKey];
                [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
                [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
                
            }
            else if ([model.color intValue] == 1){
                
                [fillDefaultColors setObject:ripTide forKey:dateKey];
                [fillSelectionColors setObject:mountainMeadow forKey:dateKey];
                [borderSelectionColors setObject:[UIColor clearColor] forKey:dateKey];
                [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
                [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
                
            }
            else if ([model.color intValue] == 2){
                
                [fillDefaultColors setObject:perfume forKey:dateKey];
                [fillSelectionColors setObject:heliotRope forKey:dateKey];
                [borderSelectionColors setObject:[UIColor clearColor] forKey:dateKey];
                [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
                [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
                
            }
            else if ([model.color intValue] == 3){
                
                [fillDefaultColors setObject:alto forKey:dateKey];
                [fillSelectionColors setObject:dustyGray forKey:dateKey];
                [borderSelectionColors setObject:[UIColor clearColor] forKey:dateKey];
                [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
                [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
                
            }
            else if ([model.color intValue] == 4){
                
                [fillDefaultColors setObject:peachOrange forKey:dateKey];
                [fillSelectionColors setObject:texasRose forKey:dateKey];
                [borderSelectionColors setObject:[UIColor clearColor] forKey:dateKey];
                [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
                [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
                
            }
            
        }

        
    }
    
    [self.calendar reloadData];
    
}


- (IBAction)openDrawer:(id)sender {
    
//    [self.revealViewController revealToggle:self.revealViewController];
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(eventArray == nil) {
        return 0;
    }
    else {
        return eventArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CalendarEventModel *modelEvent = [eventArray objectAtIndex:indexPath.row];
    CalendarEventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.eventLabel.text = modelEvent.eventName;
    [cell.colorStatusView layoutIfNeeded];
    cell.colorStatusView.layer.cornerRadius = cell.colorStatusView.frame.size.height /2;
    cell.colorStatusView.layer.borderWidth = 0;
    cell.colorStatusView.layer.masksToBounds = YES;
    
    if (modelEvent.statusColor == 0) {
        cell.colorStatusView.backgroundColor = bitterSweet;
    }else if (modelEvent.statusColor == 1){
        cell.colorStatusView.backgroundColor = mountainMeadow;
    }
    else if (modelEvent.statusColor == 2){
        cell.colorStatusView.backgroundColor = heliotRope;
    }
    else if (modelEvent.statusColor == 3){
        cell.colorStatusView.backgroundColor = dustyGray;
    }
    else if (modelEvent.statusColor == 4){
        cell.colorStatusView.backgroundColor = texasRose;
    }
    
   
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CalendarEventModel *model = [eventArray objectAtIndex:indexPath.row];
    [self showMessage:model];
}

- (void)showMessage:(CalendarEventModel *)model{
    
    NSString *event = model.eventName;
    NSString *fullEvent = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"DIALOG_CALENDA_SCHOOL_DAY",nil,[Utils getLanguage],nil),[Utils getThaiDateFormatWithDate:self.considerDate]];
    NSString *detailDate = [NSString stringWithFormat:@"%@",event];
    if (self.eventCalendarDialog == nil) {
        self.eventCalendarDialog = [[EventCalendarDetailDialog alloc] init];
    }
    if (self.eventCalendarDialog != nil && [self.eventCalendarDialog isDialogShowing]) {
        [self.eventCalendarDialog dismissDialog];
    }
    [self.eventCalendarDialog showDialogInView:self.view message:fullEvent holiday:detailDate messageDate:self.considerDate];
}

#pragma mark - Get API Data
- (void)getCalendarEventListWithDate:(NSDate *)date {
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getEventCalendarWithUserID:userID date:date schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getCalendarEventListWithDate:date];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getCalendarEventListWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getCalendarEventListWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(eventArray != nil) {
                    [eventArray removeAllObjects];
                    eventArray = nil;
                }
                
                eventArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    
                    NSMutableString *title;
                    
            
                    NSInteger  status = [[dataDict objectForKey:@"color"] integerValue];

                    if(![[dataDict objectForKey:@"title"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"title"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    CalendarEventModel *model = [[CalendarEventModel alloc] init];
                    model.eventName = title;
                    model.statusColor = status;
                    
                    [eventArray addObject:model];
                    
                }
                
                [self.tableView reloadData];
            }
        }
        
    }];
}

#pragma mark - DialogDelegate
-(void)eventDialogClose {
    NSLog(@"MessageInboxDialog Close");
}





@end
