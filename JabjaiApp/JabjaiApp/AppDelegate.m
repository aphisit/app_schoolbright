//
//  AppDelegate.m
//  JabjaiApp
//
//  Created by mac on 10/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

@import AirshipKit;
@import GoogleMaps;
@import Firebase;
#import "AppDelegate.h"
#import "PushHandler.h"

#import "LoginViewController.h"
#import "UserInfoViewController.h"
#import "NavigationViewController.h"
#import "CallGetUnreadMessageCountAPI.h"

#import "UserData.h"
#import "Utils.h"
#import "SlideMenuController.h"
#import "Reachability.h"
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>
#import <SDWebImage/UIImageView+WebCache.h>


@interface AppDelegate ()<CallGetUnreadMessageCountAPIDelegate>{
    reachabilityChangedDialog *alertDialog;
}
-(void)reachabilityChanged:(NSNotification*)note;

@property(strong) Reachability * googleReach;
@property (nonatomic, strong) PushHandler *pushHandler;
@property (nonatomic, strong) CallGetUnreadMessageCountAPI *callGetUnreadMessageCountAPI;
@property (nonatomic, strong) CallSTSendTokenAndLanguageAPI *callSTSendTokenAndLanguageAPI;
@end
NSString *tokenStr = @"";
@implementation AppDelegate

//@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
//@synthesize managedObjectModel = _managedObjectModel;
//@synthesize managedObjectContext = _managedObjectContext;
@synthesize window = _window;
@synthesize viewController = _viewController;
NSString *const kGCMMessageIDKey = @"gcm.message_id";
- (void)createMenu{
    NSLog(@"xxxxx");
    if ([UserData isUserLogin]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UserInfoViewController *mainViewController = (UserInfoViewController *)[storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
        NavigationViewController *leftViewController = (NavigationViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NavigationStoryboard"];

        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:mainViewController];

        leftViewController.mainViewControler = nvc;

        SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:mainViewController leftMenuViewController:leftViewController];

        slideMenuController.automaticallyAdjustsScrollViewInsets = YES;
        slideMenuController.delegate = mainViewController;

        self.window.rootViewController = slideMenuController;
        [self.window makeKeyWindow];
    }
    else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *mainViewController = (LoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
        NavigationViewController *leftViewController = (NavigationViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NavigationStoryboard"];
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:mainViewController];
        leftViewController.mainViewControler = nvc;
        SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:mainViewController leftMenuViewController:leftViewController];
        slideMenuController.automaticallyAdjustsScrollViewInsets = YES;
        slideMenuController.delegate = mainViewController;
        self.window.rootViewController = slideMenuController;
        [self.window makeKeyWindow];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //[Fabric with:@[[Crashlytics class]]];
    // TODO: Move this to where you establish a user session
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                                object:nil];
    __weak __block typeof(self) weakself = self;

    NSString *UDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];  //UDID Device
   
    
    self.googleReach = [Reachability reachabilityWithHostname:@"www.google.com"];
    [self.googleReach startNotifier];
    
    
    if ([self needsUpdate]) {
        [self showAlertUpdate];
    }
    
    // Set log level for debugging config loading (optional)
    // It will be set to the value in the loaded config upon takeOff
    [UAirship setLogLevel:UALogLevelTrace];
    
    // Populate AirshipConfig.plist with your app's info from https://go.urbanairship.com
    // or set runtime properties here.
    UAConfig *config = [UAConfig defaultConfig];
    
    // Call takeOff (which creates the UAirship singleton)
    [UAirship takeOff:config];
    
    // Print out the application configuration for debugging (optional)
    UA_LDEBUG(@"Config:\n%@", [config description]);
    
    //[UAirship push].autobadgeEnabled = YES;
    //[[UAirship push] setAutobadgeEnabled:YES];
    
    // Set the icon badge to zero on startup (optional)
    [[UAirship push] resetBadge];
    
    // Set a custom delegate for handling notification
    self.pushHandler = [[PushHandler alloc] init];
    [UAirship push].pushNotificationDelegate = self.pushHandler;
    [UAirship push].userPushNotificationsEnabled = YES;
//    [[SDImageCache sharedImageCache]clearMemory];
//    [[SDImageCache sharedImageCache]clearDisk];
    [self createMenu];
    [GMSServices provideAPIKey:@"AIzaSyDANNqMxvkV0bigD2SHi2flWb3lpqd0o20"];
    
    //setup firebase//
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
            UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
            requestAuthorizationWithOptions:authOptions
            completionHandler:^(BOOL granted, NSError * _Nullable error) {
              // ...
            }];
      } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
      }
      [application registerForRemoteNotifications];
    
    
    return YES;
}

-(BOOL) needsUpdate{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/th/lookup?bundleId=%@", appID]];
    NSData* data = [NSData dataWithContentsOfURL:url];
    //data = nil;
    if (data != nil) {
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if ([lookup[@"resultCount"] integerValue] == 1){
            NSString* appStoreVersion = lookup[@"results"][0][@"version"];
            NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
            if (![appStoreVersion isEqualToString:currentVersion]){
                NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
                return YES;
            }
        }

    }
        return NO;
}

-(void)showAlertUpdate
{
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"please update app"  message:nil  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Update" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
            {
                @try
                {
                    NSLog(@"tapped ok");
                    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
                    if (canOpenSettings)
                        {
                            NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/in/app/jabjai/id1171077392?mt=8"];
                                            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                        }
                }
                @catch (NSException *exception)
                {}
            }]
     ];
    UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    topWindow.rootViewController = [UIViewController new];
    topWindow.windowLevel = UIWindowLevelAlert + 1;
    [topWindow makeKeyAndVisible];
    [topWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
    
}

-(void)reachabilityChanged:(NSNotification*)note
{
    Reachability * reach = [note object];
    if(reach == self.googleReach)
    {
        if([reach isReachable])
        {
            NSLog(@"wwww");
             [alertDialog dismissDialog];
        }
        else
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTableInBundle(@"LABEL_CONNECT_INTERNET_ALERT",nil,[Utils getLanguage],nil)message:NSLocalizedStringFromTableInBundle(@"LABEL_CONNECT_INTERNET",nil,[Utils getLanguage],nil)preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedStringFromTableInBundle(@"BTN_CONNECT_INTERNET_OK",nil,[Utils getLanguage],nil) style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {}];
            [alert addAction:defaultAction];
            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
        }
    }
}
//- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
//{
//    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
//    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
//    tokenStr = token;
//
//    if (![[UserData getChangLanguage]isEqual:@""] || ![[UserData getChangLanguage]isEqual:nil]) {
//        [self doSendTokenAndLanguage:token language:[UserData getChangLanguage]];
//    }
//
//    NSLog(@"content---%@", token);
//}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [ self getUnreadMessageCount];
   
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
   // [[UIApplication sharedApplication] setApplicationIconBadgeNumber:10];
     [[UAirship push] resetBadge];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

////Firebase cloud messaging
// [START refresh_token]
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSLog(@"nn = %ld",(long)[UserData getUserID]);
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    tokenStr = fcmToken;
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}

// With "FirebaseAppDelegateProxyEnabled": NO
- (void)application:(UIApplication *)application
    didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"content---%@", token);
    //tokenStr = token;
    [FIRMessaging messaging].APNSToken = deviceToken;
//    if (![[UserData getChangLanguage]isEqual:@""] || ![[UserData getChangLanguage]isEqual:nil]) {
//        [self doSendTokenAndLanguage:token language:[UserData getChangLanguage]];
//    }

}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
    fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
  // If you are receiving a notification message while your app is in the background,
  // this callback will not be fired till the user taps on the notification launching the application.
  // TODO: Handle data of notification

  // With swizzling disabled you must let Messaging know about the message, for Analytics
  // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];

  // Print message ID.
  if (userInfo[kGCMMessageIDKey]) {
    NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
  }
  // Print full message.
  NSLog(@"%@", userInfo);
  completionHandler(UIBackgroundFetchResultNewData);
}

// Receive displayed notifications for iOS 10 devices.
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
  NSDictionary *userInfo = notification.request.content.userInfo;

  // With swizzling disabled you must let Messaging know about the message, for Analytics
  // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];

  // Print message ID.
  if (userInfo[kGCMMessageIDKey]) {
    NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
  }

  // Print full message.
  NSLog(@"%@", userInfo);

  // Change this to your preferred presentation option
  completionHandler(UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionAlert);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
  NSDictionary *userInfo = response.notification.request.content.userInfo;
  if (userInfo[kGCMMessageIDKey]) {
    NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
  }

  // With swizzling disabled you must let Messaging know about the message, for Analytics
  // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];

  // Print full message.
  NSLog(@"%@", userInfo);

  completionHandler();
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"JabjaiApp"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

+ (void)downloadDataFromURL:(NSURL *)url withCompletionHandler:(void(^)(NSData *data))completionHandler {
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        //NSLog(@"Request data");
        
        if(error != nil) {
            // If any error occurs then just display its description on the console.
            NSLog(@"%@", [error localizedDescription]);
        }
        else {
            // If no error occurs, check the HTTP status code.
            NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
            
            // If it's other than 200, then show it on the console.
            if(HTTPStatusCode != 200) {
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            // Call the completion handler with the returned data on the main thread.
            [[NSOperationQueue mainQueue] addOperationWithBlock:^(void) {
                completionHandler(data);
            }];

        }
        
    }];
    
    [task resume];
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}
///////
#pragma mark -
//Core Data stack

@synthesize
managedObjectContext = _managedObjectContext;
@synthesize
managedObjectModel = _managedObjectModel;
@synthesize
persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL
   *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.mindstick.CoreDataSample" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel
   *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
        if (_managedObjectModel != nil) {
            return _managedObjectModel;
        }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"JabjaiApp" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator*)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
            if (_persistentStoreCoordinator != nil) {
                return _persistentStoreCoordinator;
            }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"JabjaiApp.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] =
        failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort()causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
              abort();
    }
              
              return _persistentStoreCoordinator;
}

- (NSManagedObjectContext
   *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
        if (_managedObjectContext != nil) {
            return _managedObjectContext;
        }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

- (void)getUnreadMessageCount {
    
    if(self.callGetUnreadMessageCountAPI == nil) {
        self.callGetUnreadMessageCountAPI = [[CallGetUnreadMessageCountAPI alloc] init];
        self.callGetUnreadMessageCountAPI.delegate = self;
    }
    [self.callGetUnreadMessageCountAPI call:[UserData getUserID] schoolid:[UserData getSchoolId]];
}

- (void)callGetUnreadMessageCountAPI:(CallGetUnreadMessageCountAPI *)classObj unreadCount:(NSInteger)unreadCount success:(BOOL)success {
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:unreadCount];
}

#pragma mark - CallSTSendTokenAndLanguageAPI
- (void) doSendTokenAndLanguage:(NSString*)token language:(NSString*)language{
    if ( self.callSTSendTokenAndLanguageAPI != nil) {
        self.callSTSendTokenAndLanguageAPI = nil;
    }
    self.callSTSendTokenAndLanguageAPI = [[CallSTSendTokenAndLanguageAPI alloc] init];
    self.callSTSendTokenAndLanguageAPI.delegate = self;
    [self.callSTSendTokenAndLanguageAPI call:token language:language];
}
- (void)callSTSendTokenAndLanguageAPI:(CallSTSendTokenAndLanguageAPI *)classObj statusCode:(NSInteger)statusCode success:(BOOL)success{
    NSLog(@"%d",statusCode);
    
}

@end
