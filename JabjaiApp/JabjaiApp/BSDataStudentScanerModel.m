//
//  BSDataStudentScanerModel.m
//  JabjaiApp
//
//  Created by toffee on 24/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "BSDataStudentScanerModel.h"

@implementation BSDataStudentScanerModel
@synthesize studentFirstName = _studentFirstName;
@synthesize studentLastName = _studentLastName;
@synthesize studentPicture = _studentPicture;
@synthesize studentClass = _studentClass;
@synthesize studentCode = _studentCode;
@synthesize studentID = _studentID;

- (void)setStudentFirstName:(NSString *)studentFirstName{
    _studentFirstName = studentFirstName;
}
- (void)setStudentLastName:(NSString *)studentLastName{
    _studentLastName = studentLastName;
}
- (void)setStudentPicture:(NSString *)studentPicture{
    _studentPicture = studentPicture;
}
- (void)setStudentClass:(NSString *)studentClass{
    _studentClass = studentClass;
}
- (void)setStudentCode:(NSString *)studentCode{
    _studentCode = studentCode;
}
- (void)setStudentID:(long long)studentID{
    _studentID = studentID;
}


- (NSString*)getStudentFirstName{
    return _studentFirstName;
}
- (NSString*)getStudentLastName{
    return _studentLastName;
}
- (NSString*)getStudentPicture{
    return _studentPicture;
}
- (NSString*)getStudentClass{
    return _studentClass;
}
- (NSString*)getStudentCode{
    return _studentCode;
}
- (long long)getStudentID{
    return _studentID;
}
@end
