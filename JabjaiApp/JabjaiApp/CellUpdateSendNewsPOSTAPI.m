//
//  CellUpdateSendNewsPOSTAPI.m
//  JabjaiApp
//
//  Created by toffee on 9/15/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CellUpdateSendNewsPOSTAPI.h"
#import "APIURL.h"
#import "Constant.h"
#import "Utils.h"
#import <AFNetworking/AFNetworking.h>
#import <QBImagePickerController/QBImagePickerController.h>
#import "AlertDialog.h"

@interface CellUpdateSendNewsPOSTAPI (){
    UIImage *ima;
    PHImageRequestOptions *requestOptions;
    NSData *dataImage;
    NSInteger newsid;
    AlertDialog *alertDialog;
    
}

@end

@implementation CellUpdateSendNewsPOSTAPI

- (void)call:(NSString *)jsonString  imageArray:(NSArray *)imageArray{
    [self updateStudentBehaviorScore:jsonString imageArray:(NSArray*)imageArray];
}

- (void)updateStudentBehaviorScore:(NSString *)jsonString imageArray:(NSArray *)imageArray{
    NSString *URLString = [APIURL getUpdateSendNewsPOST];
    NSError* error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableLeaves error:&error];
    static NSString *KEY_NEWSID = @"newsid";
    static NSString *KEY_SCHOOLID = @"schoolid";
    if(error != nil) {
        NSLog(@"%@", @"Parse JSON data failed");
    }
    else {
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"responseObject = %@", responseObject);
            newsid = [[responseObject objectForKey:@"newsid"] integerValue];
            
            if(imageArray.count != 0){
                /////send image News
                NSInteger i;
                NSData *imageData = nil;
                NSMutableArray *addImage = [[NSMutableArray alloc] init];
                NSString *urlString = [APIURL getSNNewsPOSTURL];
                NSURL *url = [NSURL URLWithString:urlString];
                NSString *schoolID = [[NSString alloc] initWithFormat:@"%lld", (long long)[UserData getSchoolId]];
                NSString *newsID = [[NSString alloc] initWithFormat:@"%lld", (long long)newsid];
                NSLog(@"num = %d",  newsid);
                NSMutableString *feedBackStr = [[NSMutableString alloc] initWithFormat:@"%@", @"432"];
                NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
                [parameters setObject:schoolID forKey:KEY_SCHOOLID];
                [parameters setObject:newsID forKey:KEY_NEWSID];
                
                PHImageManager *managers = [PHImageManager defaultManager];
                NSMutableArray *images = [[NSMutableArray alloc] init];
                NSString *orgFilename;
                requestOptions = [[PHImageRequestOptions alloc] init];
                requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
                requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
                requestOptions.synchronous = true;
                for (PHAsset *asset in imageArray) {
                    NSArray *resources = [PHAssetResource assetResourcesForAsset:asset];
                    [addImage addObject:resources];
                    imageData = resources;
                    orgFilename = ((PHAssetResource*)resources[0]).originalFilename;
                    
                    [managers requestImageForAsset:asset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeDefault options:requestOptions resultHandler:^void (UIImage *image, NSDictionary *info){
                        ima = image;
                        [images addObject:ima];
                    }];
                }
                dataImage = [NSKeyedArchiver archivedDataWithRootObject:images];
                [Utils uploadImageFromURL:url data:dataImage imageparameterName:orgFilename imageFileName:[NSString stringWithFormat:@"%ld",i++] mimeType:JPEG parameters:parameters withCompletionHandler:^(id responseObject, NSError *error) {
                    NSLog(@"response = %@", responseObject);
                    
                }];
        
            }
                      

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callUpdateBehaviorScorePOSTAPI:response:success:)]) {
                [self.delegate callUpdateBehaviorScorePOSTAPI:self response:nil success:NO];
            }
            
        }];
    
    }

}



@end
