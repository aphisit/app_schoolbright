//
//  CallSTSendTokenAndLanguageAPI.h
//  JabjaiApp
//
//  Created by Mac on 16/2/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CallSTSendTokenAndLanguageAPI;
@protocol CallSTSendTokenAndLanguageAPIDelegate <NSObject>

- (void)callSTSendTokenAndLanguageAPI:(CallSTSendTokenAndLanguageAPI *)classObj statusCode:(NSInteger)statusCode success:(BOOL)success;

@end
@interface CallSTSendTokenAndLanguageAPI : NSObject
@property (nonatomic,weak)id<CallSTSendTokenAndLanguageAPIDelegate> delegate;
- (void) call:(NSString*)token language:(NSString*)language ;

@end

NS_ASSUME_NONNULL_END
