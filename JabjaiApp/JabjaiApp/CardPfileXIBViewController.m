//
//  CardPfileXIBViewController.m
//  JabjaiApp
//
//  Created by toffee on 7/9/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CardPfileXIBViewController.h"
#import "UserData.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Utils.h"


@interface CardPfileXIBViewController ()
@property (nonatomic, assign) BOOL isShowing;
@property (strong, nonatomic) CallGMGetUserInfoDataAPI *callGMGetUserInfoDataAPI;
@property (strong, nonatomic) CallImageBarCodeAPI *callImageBarCodeAPI;
@property (strong, nonatomic) UserInfoModel *userInfoModel;
@end

@implementation CardPfileXIBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _isFlib = YES;
    self.imageUser.layer.masksToBounds = YES;
    _contentView.transform = CGAffineTransformMakeRotation(M_PI_2);
    [self.imageBarcode setAlpha:0.0];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showDialogInView:(UIView *)targetView{
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [self.contentView setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.0]];
    [targetView addSubview:self.view];
    
    
    // Initialize dialog view hide it
    [self.contentView setAlpha:0.0];
    //[self.closeButton setAlpha:0.0];
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentView setAlpha:1.0];
    //[self.closeButton setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
    
    self.contentViewCard.layer.cornerRadius = 9.0;
    self.contentViewCard.layer.masksToBounds = YES;
    
    self.contentViewCardBack.layer.cornerRadius = 9.0;
    self.contentViewCardBack.layer.masksToBounds = YES;
    
    NSString *formatQRcodeStr = [NSString stringWithFormat:@"%@@%ld@%@",[UserData getUsername],(long)[UserData getUserID],[Utils dateInStringFormat:[NSDate date] format:@"dd-MM-yyyy:HH:mm:ss"]];
    CIImage *qrImage = [self createQRForString:formatQRcodeStr];
    float scaleX = self.qrImageQrCode.frame.size.width / qrImage.extent.size.width;
    float scaleY = self.qrImageQrCode.frame.size.height / qrImage.extent.size.height;
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    self.qrImageQrCode.image = [UIImage imageWithCIImage:qrImage
                                                 scale:[UIScreen mainScreen].scale
                                           orientation:UIImageOrientationUp];
    [NSTimer scheduledTimerWithTimeInterval:600.0f
    target:self selector:@selector(generateQRCode:) userInfo:nil repeats:YES];
    
    NSString *formatBarcodeStr = [NSString stringWithFormat:@"%@@%ld@%@",[UserData getUsername],(long)[UserData getUserID],[Utils dateInStringFormat:[NSDate date] format:@"dd-MM-yyyy:HH:mm:ss"]];
    [self generateBarCodeWithString:formatBarcodeStr];
    [NSTimer scheduledTimerWithTimeInterval:600.0f
    target:self selector:@selector(generateBarcode:) userInfo:nil repeats:YES];
    [self getUserInfoData];
    
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentView setAlpha:0.0];
    //[self.closeButton setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}
- (IBAction)closeAction:(id)sender {
    [self dismissDialog];
}

- (IBAction)flibButtonAction:(id)sender {
    [UIView transitionWithView:self.contentView duration:1.0 options:(_isFlib ? UIViewAnimationOptionTransitionFlipFromRight : UIViewAnimationOptionTransitionFlipFromRight) animations:^{
        
        if (_isFlib) {
            self.contentViewCard.hidden = true;
            self.contentViewCardBack.hidden = false;
        }
        else{
            self.contentViewCard.hidden = false;
            self.contentViewCardBack.hidden = true;
        }
        
        
    } completion:^(BOOL finished){
        if (finished) {
            _isFlib = !_isFlib;
        }
    }];
}

- (void)dismissDialog{
    
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogClose)]) {
        [self.delegate onAlertDialogClose];
    }

}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

//Call To API Info Card
- (void)getUserInfoData {
    
    if(self.callGMGetUserInfoDataAPI != nil) {
        self.callGMGetUserInfoDataAPI = nil;
    }
    
    self.callGMGetUserInfoDataAPI = [[CallGMGetUserInfoDataAPI alloc] init];
    self.callGMGetUserInfoDataAPI.delegate = self;
    [self.callGMGetUserInfoDataAPI getUserInfoData] ;
}

-(void)callGMGetUserInfoDataAPI:(CallGMGetUserInfoDataAPI *)classObj data:(UserInfoModel *)data success:(BOOL)success{
    self.userInfoModel = [[UserInfoModel alloc] init];
    self.userInfoModel = data;
    NSString *newIdCard;
    if (data != nil) {
        //[self getImageBarcode:@"barcode"];
        //[self getImageBarcode:@"qrcode"];
        if([UserData getUserType] == STUDENT) {
            //Barcode
            [self.nameHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_NAME",nil,[Utils getLanguage],nil)];
            [self.idHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_STUDENTID",nil,[Utils getLanguage],nil)];
            [self.departmentHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_CLASSROOM",nil,[Utils getLanguage],nil)];
            [self.positionHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_CARD_ID_CARD",nil,[Utils getLanguage],nil)];
        
            if ((NSInteger)self.userInfoModel.sIdentification.length>3) {
                if ([[UserData getShowSomeId] isEqualToString:@"on"]){
                    newIdCard = self.userInfoModel.sIdentification;
                    for (int i = 0; i <  (NSInteger)self.userInfoModel.sIdentification.length - 3;i++) {
                        newIdCard = [newIdCard stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@"X"];
                    }
                }else{
                    newIdCard = self.userInfoModel.sIdentification;
                }
            }else{
                newIdCard = self.userInfoModel.sIdentification;
            }

            [self.nameSchoolLable setText:self.userInfoModel.schoolName];
            [self.nameLabel setText:[NSString stringWithFormat:@"%@ %@",self.userInfoModel.firstName,self.userInfoModel.lastName]];
            [self.idCardLabel setText:self.userInfoModel.studentId];
            [self.departmentLabel setText:self.userInfoModel.studentClass];
            [self.positionLabel setText:newIdCard];
            if ([self.userInfoModel.imageUrl isEqualToString:@""]) {
                self.imageUser.image = [UIImage imageNamed:@"ic_user_default"];
                self.qrImageUser.image = [UIImage imageNamed:@"ic_user_default"];
            }else{
                [self.imageUser sd_setImageWithURL:self.userInfoModel.imageUrl];
                [self.qrImageUser sd_setImageWithURL:self.userInfoModel.imageUrl];
            }
            
            if ([[UserData getSchoolPic] isEqualToString:@""]) {
                self.imageSchool.image = [UIImage imageNamed:@"school"];
                self.qrImageSchool.image = [UIImage imageNamed:@"school"];
            }else{
                [self.imageSchool sd_setImageWithURL:[UserData getSchoolPic]];
                [self.qrImageSchool sd_setImageWithURL:[UserData getSchoolPic]];
            }
            
            
            //QR code
            
            [self.qrNameHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_NAMESURNAME",nil,[Utils getLanguage],nil)];
            [self.qrClassroomHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_CLASS_ROOM",nil,[Utils getLanguage],nil)];
            
            [self.qrNameLabel setText:[NSString stringWithFormat:@"%@ %@",self.userInfoModel.firstName,self.userInfoModel.lastName]];
            [self.qrClassroomLabel setText:self.userInfoModel.studentClass];
            [self.qrNameSchoolLabel setText:self.userInfoModel.schoolName];
 
        }
        else {
            [self.nameHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_NAME",nil,[Utils getLanguage],nil)];
            [self.idHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_CARD_ID_CARD",nil,[Utils getLanguage],nil)];
            [self.departmentHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_DEPARTMENT",nil,[Utils getLanguage],nil)];
            [self.positionHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_POSITION",nil,[Utils getLanguage],nil)];
            
            if ((NSInteger)self.userInfoModel.sIdentification.length>3) {
                if ([[UserData getShowSomeId] isEqualToString:@"on"]){
                newIdCard = self.userInfoModel.sIdentification;
                    for (int i = 0; i <  (NSInteger)self.userInfoModel.sIdentification.length - 3;i++) {
                        newIdCard = [newIdCard stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@"X"];
                    }
                }else{
                    newIdCard = self.userInfoModel.sIdentification;
                }
            }else{
                newIdCard = self.userInfoModel.sIdentification;
            }
            
            [self.nameSchoolLable setText:self.userInfoModel.schoolName];
            [self.nameLabel setText:[NSString stringWithFormat:@"%@ %@",self.userInfoModel.firstName,self.userInfoModel.lastName]];
            [self.idCardLabel setText:newIdCard];
            [self.departmentLabel setText:self.userInfoModel.department];
            [self.positionLabel setText:self.userInfoModel.position];
            
            if ([self.userInfoModel.imageUrl isEqualToString:@""]) {
                self.imageUser.image = [UIImage imageNamed:@"ic_user_default"];
                self.qrImageUser.image = [UIImage imageNamed:@"ic_user_default"];
            }else{
                [self.imageUser sd_setImageWithURL:self.userInfoModel.imageUrl];
                [self.qrImageUser sd_setImageWithURL:self.userInfoModel.imageUrl];
            }
//            self.imageUser.contentMode = UIViewContentModeScaleAspectFill;
//            self.imageUser.clipsToBounds = YES;
            
            if ([[UserData getSchoolPic] isEqualToString:@""]) {
                self.imageSchool.image = [UIImage imageNamed:@"school"];
                self.qrImageSchool.image = [UIImage imageNamed:@"school"];
            }else{
                [self.imageSchool sd_setImageWithURL:[UserData getSchoolPic]];
                [self.qrImageSchool sd_setImageWithURL:[UserData getSchoolPic]];
            }
            
            //QR code
            [self.qrNameHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_NAMESURNAME",nil,[Utils getLanguage],nil)];
            [self.qrClassroomHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_POSITION",nil,[Utils getLanguage],nil)];
            
            [self.qrNameLabel setText:[NSString stringWithFormat:@"%@ %@",self.userInfoModel.firstName,self.userInfoModel.lastName]];
           
            [self.qrNameSchoolLabel setText:self.userInfoModel.schoolName];
            
            if ([self.userInfoModel.department isEqualToString:@""]) {
                [self.qrClassroomLabel setText:@"-"];
            }else{
                [self.qrClassroomLabel setText:self.userInfoModel.department];
            }
        }
        
    }
    
}


- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}
//generate qrcode
- (void)generateQRCode:(NSTimer *)timer
{
    NSString *formatQRcodeStr = [NSString stringWithFormat:@"%@@%ld@%@",[UserData getUsername],(long)[UserData getUserID],[Utils dateInStringFormat:timer.fireDate format:@"dd-MM-yyyy:HH:mm:ss"]];
    CIImage *qrImage = [self createQRForString:formatQRcodeStr];
    float scaleX = self.qrImageQrCode.frame.size.width / qrImage.extent.size.width;
    float scaleY = self.qrImageQrCode.frame.size.height / qrImage.extent.size.height;
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    self.qrImageQrCode.image = [UIImage imageWithCIImage:qrImage
                                                 scale:[UIScreen mainScreen].scale
                                           orientation:UIImageOrientationUp];
}


- (CIImage *)createQRForString:(NSString *)qrString {
    NSData *stringData = [qrString dataUsingEncoding: NSISOLatin1StringEncoding];
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    return qrFilter.outputImage;
}

//generate barcode
- (void)generateBarcode:(NSTimer *)timer
{
    NSString *formatBarcodeStr = [NSString stringWithFormat:@"%@@%ld@%@",[UserData getUsername],(long)[UserData getUserID],[Utils dateInStringFormat:timer.fireDate format:@"dd-MM-yyyy:HH:mm:ss"]];
    [self generateBarCodeWithString:formatBarcodeStr];
}

-(void)generateBarCodeWithString:(NSString *)string
{
CIFilter *barCodeFilter = [CIFilter filterWithName:@"CICode128BarcodeGenerator"];

[barCodeFilter setDefaults];
NSData *barCodeData = [string dataUsingEncoding:NSASCIIStringEncoding];//13 digit number
[barCodeFilter setValue:barCodeData forKey:@"inputMessage"];
[barCodeFilter setValue:[NSNumber numberWithFloat:0] forKey:@"inputQuietSpace"];

CIImage *outputImage = [barCodeFilter outputImage];
CIContext *context = [CIContext contextWithOptions:nil];
CGImageRef cgImage = [context createCGImage:outputImage
fromRect:[outputImage extent]];

UIImage *image = [UIImage imageWithCGImage:cgImage
scale:1.
orientation:UIImageOrientationUp];
[self.imageBarcode layoutIfNeeded];
UIImage *resized = [self QRresizeImage:image
withQuality:kCGInterpolationNone
rate:5.0];

self.imageBarcode.image = resized;

CGImageRelease(cgImage);
}

- (UIImage *)QRresizeImage:(UIImage *)image
withQuality:(CGInterpolationQuality)quality
rate:(CGFloat)rate
{
UIImage *resized = nil;
CGFloat width = image.size.width * rate;
CGFloat height = image.size.height * rate;
UIGraphicsBeginImageContext(CGSizeMake(width, height));
CGContextRef context = UIGraphicsGetCurrentContext();
CGContextSetInterpolationQuality(context, quality);
[image drawInRect:CGRectMake(0, 0, width, height)];
resized = UIGraphicsGetImageFromCurrentImageContext();
UIGraphicsEndImageContext();

return resized;
}


@end
