
//  ReportLeaveViewController.m
//  JabjaiApp
//
//  Created by mac on 4/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportLeaveViewController.h"
#import "ReportListTeacherViewController.h"
#import "ReportListStudentViewController.h"
#import "UserData.h"
#import "Utils.h"

@interface ReportLeaveViewController (){
    UIColor *grayColor;
    UIColor *acceptLeave;
    UIColor *declineLeave;
    UIColor *waitLeave;
    UIColor *dodgerBlue, *aqua, *monaLisa, *bitterSweet, *ripTide, *mountainMeadow, *perfume, *heliotRope, *alto, *dustyGray, *peachOrange, *texasRose;
    UIColor *cornFlower, *mariner;
    NSArray<NoticeReportDateMessageModel *> *leaveArray;
}
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) CallReportLeaveMessageListAPI *callReportLeaveMessageListAPI;
@property (nonatomic, strong) ReportLeaveDetailViewControllerXIB *reportLeaveDetailViewControllerXIB;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation ReportLeaveViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLanguage];
    [self callGetStatusServer];
    
       //grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
       
       self.tableView.delegate = self;
       self.tableView.dataSource = self;
       //self.tableView.backgroundColor = grayColor;
       self.tableView.tableFooterView = [[UIView alloc] init];
       //[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
       [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NoticeReportDateMessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
       
       self.dateFormatter = [Utils getDateFormatter];
       self.dateFormatter.dateFormat = @"dd/MM/yyyy";
       
       acceptLeave = [UIColor colorWithRed:0.13 green:0.86 blue:0.55 alpha:1.0];
       declineLeave = [UIColor colorWithRed:1.00 green:0.32 blue:0.32 alpha:1.0];
       waitLeave = [UIColor colorWithRed:0.26 green:0.78 blue:0.96 alpha:1.0];
       
       // 08/05/2561
       dodgerBlue = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0]; // Now Full
       aqua = [UIColor colorWithRed:0.21 green:0.89 blue:1.00 alpha:1.0]; // Now Select
       monaLisa = [UIColor colorWithRed:1.00 green:0.68 blue:0.62 alpha:1.0]; // Color status 0 Before Select
       bitterSweet = [UIColor colorWithRed:1.00 green:0.48 blue:0.38 alpha:1.0]; // Color status 0 After Select
       ripTide = [UIColor colorWithRed:0.47 green:0.93 blue:0.71 alpha:1.0]; // Color status 1 Before Select
       mountainMeadow = [UIColor colorWithRed:0.13 green:0.76 blue:0.46 alpha:1.0]; // Color status 1 After Select
       perfume = [UIColor colorWithRed:0.80 green:0.62 blue:0.98 alpha:1.0]; // Color status 2 Before Select
       heliotRope = [UIColor colorWithRed:0.75 green:0.51 blue:1.00 alpha:1.0]; // Color status 2 After Select
       alto = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1.0]; // Color status 3 Before Select
       dustyGray = [UIColor colorWithRed:0.60 green:0.60 blue:0.60 alpha:1.0]; // Color status 3 After Select
       peachOrange = [UIColor colorWithRed:1.00 green:0.85 blue:0.59 alpha:1.0]; // Color status 4 Before Select
       texasRose = [UIColor colorWithRed:1.00 green:0.75 blue:0.31 alpha:1.0]; // Color status 4 After Select
       
       cornFlower = [UIColor colorWithRed:0.58 green:0.80 blue:0.92 alpha:1.0]; // Color status 0 Before Select
       mariner = [UIColor colorWithRed:0.13 green:0.49 blue:0.76 alpha:1.0]; // Color status 1 After Select
       [self getUserLeaveDetail:[UserData getSchoolId] userId:[UserData getUserID]];
}

- (void) setLanguage{
    self.headerLeaveReportLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_LEAVE",nil,[Utils getLanguage],nil);
    self.headerLeaveTotalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_LEAVE_ALL",nil,[Utils getLanguage],nil);
    self.headerDayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_DAY",nil,[Utils getLanguage],nil);
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
   // [self setupPageMenu];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader,*gradientDay;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    [self.dayView layoutIfNeeded];
    gradientDay = [Utils getGradientColorStatus:@"blue"];
    gradientDay.frame = self.dayView.bounds;
    [self.dayView.layer insertSublayer:gradientDay atIndex:0];
}

#pragma mark - CallReportLeaveMessageListAPI
-(void) getUserLeaveDetail:(long long)schoolId userId:(long long)userId{
    self.callReportLeaveMessageListAPI = nil;
    self.callReportLeaveMessageListAPI = [[ CallReportLeaveMessageListAPI alloc] init];
    self.callReportLeaveMessageListAPI.delegate = self;
    [self.callReportLeaveMessageListAPI call:schoolId userId:userId];
}
-(void)callReportLeaveMessageListAPI:(CallReportLeaveMessageListAPI *)classObj data:(NSArray<NoticeReportDateMessageModel *> *)data countLeave:(NSString *)countLeave success:(BOOL)success{
    if (data != nil && success) {
        leaveArray = data;
        self.dateCount.text = countLeave;
    }
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(leaveArray == nil) {
        return 0;
    }
    else {
        return leaveArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NoticeReportDateMessageModel *model = [leaveArray objectAtIndex:indexPath.row];
    NoticeReportDateMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.statusColor.layer.cornerRadius = cell.statusColor.frame.size.width / 2;
    cell.statusColor.clipsToBounds = YES;
    cell.statusColor.layer.shouldRasterize = YES;
    
    if (model.status == 0) {
        cell.statusColor.backgroundColor = bitterSweet;
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_NO_ALLOW",nil,[Utils getLanguage],nil);
    }
    else if (model.status == 1){
        cell.statusColor.backgroundColor = mountainMeadow;
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_ALLOW",nil,[Utils getLanguage],nil);
    }
    else if (model.status == 3){
        cell.statusColor.backgroundColor = mariner;
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_WAIT_ALLOW",nil,[Utils getLanguage],nil);
    }
    else if (model.status == 4){
        cell.statusColor.backgroundColor = heliotRope;
        cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PRI_REP_CANCEL",nil,[Utils getLanguage],nil);
    }
    cell.leaveNameLabel.text = [model leaveType];
    cell.leaveDateLabel.text = [Utils dateInStringFormat:model.date];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NoticeReportDateMessageModel *model = [leaveArray objectAtIndex:indexPath.row];
    if (self.reportLeaveDetailViewControllerXIB != nil) {
        self.reportLeaveDetailViewControllerXIB = nil;
    }
    self.reportLeaveDetailViewControllerXIB = [[ReportLeaveDetailViewControllerXIB alloc] init];
    [self.reportLeaveDetailViewControllerXIB showDetailLeaveViewController:self.view letterID:model.letterID];
//    ReportLeaveDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportLeaveDetailStoryboard"];
//    viewController.letterID = model.letterID;
//    viewController.page = 3;
//    [self.slideMenuController changeMainViewController:viewController close:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (IBAction)moveBack:(id)sender {
    
    if ([UserData getUserType] == TEACHER) {
        ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else{
        ReportListStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListStudentStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
}
@end

