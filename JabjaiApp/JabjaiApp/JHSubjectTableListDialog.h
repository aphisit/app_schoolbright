//
//  JHSubjectTableListDialog.h
//  JabjaiApp
//
//  Created by toffee on 11/22/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JHSubjectModel.h"
#import "TableJHSubjectListViewCell.h"

@class JHSubjectTableListDialog;

@protocol JHSubjectTableListDialogDelegate <NSObject>

- (void)subjectTableListDialog:(JHSubjectTableListDialog *)dialog onItemSelectWithRequestCode:(NSString *)requestCode atIndex:(NSInteger)index;

@end
@interface JHSubjectTableListDialog : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) id<JHSubjectTableListDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (instancetype)initWithRequestCode:(NSString *)requestCode;
- (void)showDialogInView:(UIView *)targetView subjectArray:(NSArray<JHSubjectModel *> *)subjectArray;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
