//
//  BSBehaviorTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 12/19/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSBehaviorTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *statementreadLabel;
@property (weak, nonatomic) IBOutlet UIView *bgView;



@end
