//
//  TEDataStudentScanerModel.h
//  JabjaiApp
//
//  Created by toffee on 2/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TEDataStudentScanerModel : NSObject
@property (nonatomic) NSString *studentFirstName;
@property (nonatomic) NSString *studentLastName;
@property (nonatomic) NSString *studentPicture;
@property (nonatomic) NSString *studentClass;

- (void)setStudentFirstName:(NSString *)studentFirstName;
- (void)setStudentLastName:(NSString *)studentLastName;
- (void)setStudentPicture:(NSString *)studentPicture;
- (void)setStudentClass:(NSString *)studentClass;

- (NSString*)getStudentFirstName;
- (NSString*)getStudentLastName;
- (NSString*)getStudentPicture;
- (NSString*)getStudentClass;

@end

NS_ASSUME_NONNULL_END
