//
//  SSubjectViewControllerDelegate.h
//  JabjaiApp
//
//  Created by mac on 5/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SSubjectViewControllerDelegate <NSObject>

@optional
- (void)onSelectSubject:(long long)subjectID subjectName:(NSString *)subjectName date:(NSDate *)date;

@end

