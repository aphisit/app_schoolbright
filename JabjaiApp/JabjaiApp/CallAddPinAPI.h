//
//  CallAddPinAPI.h
//  JabjaiApp
//
//  Created by toffee on 6/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CallAddPinAPI;
@protocol CallAddPinAPIDelegate <NSObject>

- (void)callAddPinAPI:(CallAddPinAPI *)classObj status:(NSString*)status success:(BOOL)success;
@end
@interface CallAddPinAPI : NSObject
@property (nonatomic, weak) id<CallAddPinAPIDelegate> delegate;
- (void)call:(long long)userId pinCode:(NSString*)pinCode schoolid:(long long)schoolid;
@end
