//
//  CallUHGetHistoryHealthAPI.h
//  JabjaiApp
//
//  Created by Mac on 1/7/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserHealthHistoryModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol CallUHGetHistoryHealthAPIDelegate <NSObject>

-(void)callUHGetHistoryHealthAPI:(NSArray<UserHealthHistoryModel*>*)temperatureArray sucess:(BOOL)sucess;

@end

@interface CallUHGetHistoryHealthAPI : NSObject
@property (nonatomic ,weak) id<CallUHGetHistoryHealthAPIDelegate>delegate;
- (void)call:(long long)userId schoolId:(long long)schoolId date:(NSString*)date;

@end

NS_ASSUME_NONNULL_END
