//
//  TopupItemListViewController.h
//  JabjaiApp
//
//  Created by mac on 5/14/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopupReportViewController.h"

@interface TopupItemListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,TopupReportViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
