//
//  SNDetailNewsViewController.m
//  JabjaiApp
//
//  Created by Mac on 2/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "SNDetailNewsViewController.h"
#import "UserData.h"
#import "Utils.h"
@interface SNDetailNewsViewController (){
    NSMutableArray *imageArray,*fileArray,*typeFileArray;
    NSInteger typySend;
    UIView *view;
    NSInteger imgConstant,fileConstant;
    UIButton *notAllowBtn, *allowBtn, *acknowledgeBtn;
    double allowHeight;
    SNDetailMessageBoxModel *modelData;
    long long messageID;
    int replyCode;
}
@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic, strong) SNReadFileViewController *openFileView;
@property (nonatomic, strong) SNReadImageViewController *openImageView;
@property (nonatomic, strong) ConfirmDialog *confirmDialog;
@property (nonatomic, strong) AlertDialog *notSuccessDialog;
@property (nonatomic, strong) CallFileMessageAPI *callFileMessageAPI;
@property (nonatomic, strong) CellDetailMessageBoxAPI *cellDetailMessageBoxAPI;
@property (nonatomic, strong) CallSNSendReplyCodeMessageBoxAPI *callSNSendReplyCodeMessageBoxAPI;
@end

@implementation SNDetailNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    imgConstant = self.highImaCollectionConstraint.constant;
    fileConstant = self.highFileCollectionConstraint.constant;
    allowHeight = self.allowHeightConstraint.constant;
    
   
   
    [self setLanguage];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidLayoutSubviews{
    //close allow
    self.allowStack.hidden = YES;
    self.acknowledgeView.hidden = YES;
    self.statusView.hidden = YES;
    self.acknowledgeConstraint.constant = 0.0;
    self.statusHeightConstraint.constant = 0.0;
    self.allowHeightConstraint.constant = 0.0;
}

-(void)setLanguage{
    
    self.headerLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFORM_BROADCAST",nil,[Utils getLanguage],nil);
    self.headerNewsLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFORM_INFORM_BROADCAST",nil,[Utils getLanguage],nil);
    self.headerAttactImageLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFORM_ATTACT_IMAGE",nil,[Utils getLanguage],nil);
    self.headerAttactFileLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFORM_ATTACT_FILE",nil,[Utils getLanguage],nil);
    
}

- (void)showDetailNewsViewController:(UIView *)targetView model:(MessageInboxDataModel *)model typeSend:(NSInteger)typeSend{
    view = targetView;
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
        [targetView addSubview:self.view];
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height-20)];
        [targetView addSubview:self.view];
    }

    // Initialize dialog view hide it
    [self.contentView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentView setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
    
    [self.headerView layoutIfNeeded];
    CAGradientLayer *gradient;
    gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
//    self.allowStack.hidden = YES;
//    self.acknowledgeView.hidden = YES;
//    self.statusView.hidden = YES;
//    self.acknowledgeConstraint.constant = allowHeight;
//    self.statusHeightConstraint.constant = allowHeight;
//    self.allowHeightConstraint.constant = allowHeight;
    
    [self.imageCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SNDetailNewsCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.imageCollectionView.delegate = self;
    self.imageCollectionView.dataSource = self;
    
    [self.fileCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SNDetailNewsCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.fileCollectionView.delegate = self;
    self.fileCollectionView.dataSource = self;
    self.imageCollectionView.hidden = NO;
    self.fileCollectionView.hidden = NO;
    
    [self callGetDetailMessage:[UserData getUserID] schoolID:[UserData getSchoolId] messageID:model.messageID];
    messageID = model.messageID;
    replyCode = 0;
    imageArray = [[NSMutableArray alloc] init];
    fileArray = [[NSMutableArray alloc] init];
    typeFileArray = [[NSMutableArray alloc] init];
    typySend = typeSend;

    self.highImaCollectionConstraint.constant = imgConstant;
    self.highFileCollectionConstraint.constant = fileConstant;
    [self.imageCollectionView reloadData];
    [self.fileCollectionView reloadData];
    ;
}
#pragma  mark - ConfirmDialog
- (void) showConfirmDialog:(NSString*)message{
    if (self.confirmDialog != nil) {
        self.confirmDialog = nil;
    }
    self.confirmDialog = [[ConfirmDialog alloc] init];
    self.confirmDialog.delegate = self;
    [self.confirmDialog showDialogInView:view title:@"แจ้งเตือน" message:message];
}

- (void)onConfirmDialog:(ConfirmDialog *)confirmDialog confirm:(BOOL)confirm{
    if (confirm == YES) {
        [self callSendReplyCode:[UserData getUserID] messageID:messageID replyCode:replyCode schoolID:[UserData getSchoolId]];
    }
}

#pragma  mark - AlertDialog
- (void) showNotSuccessDialog:(NSString*)message{
    if (self.notSuccessDialog != nil) {
        self.notSuccessDialog = nil;
    }
    self.notSuccessDialog = [[AlertDialog alloc] init];
    [self.notSuccessDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

//Action when hitting submit
- (void) notAllowAction:(UIButton*)sender{
    replyCode = (int)[sender tag];
    [self showConfirmDialog:sender.titleLabel.text];
}

- (void) allowAction:(UIButton*)sender{
    replyCode = (int)[sender tag];
    [self showConfirmDialog:sender.titleLabel.text];
}
 
- (void) acknowledgeAction:(UIButton*)sender{
    replyCode = (int)[sender tag];
    [self showConfirmDialog:sender.titleLabel.text];
}

#pragma mark - CallSNSendReplyCodeMessageBoxAPI
- (void)callSendReplyCode:(long long)userID messageID:(long long)messageID replyCode:(int)replyCode schoolID:(long long)schoolID{
    if (self.callSNSendReplyCodeMessageBoxAPI != nil) {
        self.callSNSendReplyCodeMessageBoxAPI = nil;
    }
    self.callSNSendReplyCodeMessageBoxAPI = [[CallSNSendReplyCodeMessageBoxAPI alloc] init];
    self.callSNSendReplyCodeMessageBoxAPI.delegate = self;
    [self.callSNSendReplyCodeMessageBoxAPI call:userID messageID:messageID replyCode:replyCode schoolID:schoolID];
}
- (void)callSNSendReplyCodeMessageBoxAPI:(CallSNSendReplyCodeMessageBoxAPI *)classObj data:(SNStatusSendReplyCodeModel *)data success:(BOOL)success{
    if (success == YES) {
        [self callGetDetailMessage:[UserData getUserID] schoolID:[UserData getSchoolId] messageID:messageID];
    }else{
        [self showNotSuccessDialog:@"ไม่สำเร็จ ลองใหม่อีกครั้ง"];
    }
}

#pragma mark - CellDetailMessageBoxAPI
- (void)callGetDetailMessage:(long long)userID schoolID:(long long)schoolID messageID:(long long)messageID{
    if (self.cellDetailMessageBoxAPI != nil) {
        self.cellDetailMessageBoxAPI = nil;
    }
    self.cellDetailMessageBoxAPI = [[CellDetailMessageBoxAPI alloc] init];
    self.cellDetailMessageBoxAPI.delegate = self;
    [self.cellDetailMessageBoxAPI call:userID messageID:messageID schoolid:schoolID];
}

- (void)callDetailMessageBoxAPI:(CellDetailMessageBoxAPI *)classObj data:(SNDetailMessageBoxModel *)data success:(BOOL)success{
    if (success) {
        NSLog(@"");
        modelData = data;
        
        [self.detailTextView setEditable:NO];
        [self.detailTextView setDataDetectorTypes:UIDataDetectorTypeAll];
        [self.detailTextView setText:data.message];
        
        NSDateFormatter *formatter = [Utils getDateFormatter];
        [formatter setDateFormat:[Utils getXMLDateFormat]];
        
        NSDateFormatter *formatter2 = [Utils getDateFormatter];
        [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
        
        NSRange dotRange = [data.date rangeOfString:@"."];
        
        NSDate *messageDate;
        
        if(dotRange.length != 0) {
            messageDate = [formatter dateFromString:data.date];
        }
        else {
            messageDate = [formatter2 dateFromString:data.date];
        }
        [formatter setDateFormat:@"HH:mm"];
        self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ : %@ %@ : %@ ",NSLocalizedStringFromTableInBundle(@"LABEL_INFORM_DATE",nil,[Utils getLanguage],nil),[Utils getThaiDateFormatWithDate:messageDate],NSLocalizedStringFromTableInBundle(@"LABEL_INFORM_TIME",nil,[Utils getLanguage],nil),[formatter stringFromDate:messageDate]];
        
        
//        if (![data.replyResult isEqual:@""]) {
//
//            self.allowStack.hidden = YES;
//            self.acknowledgeView.hidden = YES;
//            self.statusView.hidden = NO;
//
//            [self.statusView layoutIfNeeded];
//            self.statusView.backgroundColor = [UIColor clearColor];
//            self.statusView.layer.shadowColor = [UIColor blackColor].CGColor;
//            self.statusView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
//            self.statusView.layer.shadowOpacity = 0.8;
//            self.statusView.layer.shadowRadius = 3.0;
//
//            // Create View and UILabel
//            UIView *statusContentView = [[UIView alloc] initWithFrame:self.statusView.bounds];
//            statusContentView.backgroundColor = [UIColor colorWithRed: 0.99 green: 0.65 blue: 0.42 alpha: 1.00];
//            statusContentView.layer.masksToBounds = YES;
//            statusContentView.layer.cornerRadius = 10;
//
//            [self.statusView addSubview:statusContentView];
//
//            UILabel *yourLabel = [[UILabel alloc] initWithFrame:self.statusView.bounds];
//            yourLabel.text =[NSString stringWithFormat:@"สถานะ : %@",data.replyResult];
//            yourLabel.textAlignment = NSTextAlignmentCenter;
//            [yourLabel setTextColor:[UIColor whiteColor]];
//            [yourLabel setBackgroundColor:[UIColor clearColor]];
//            [yourLabel setFont:[UIFont fontWithName: @"THSarabunNew-Bold" size: 25.0f]];
//            [statusContentView addSubview:yourLabel];
//
//        }else{
//            if (data.replyButton.count == 2) {
//
//                self.allowStack.hidden = NO;
//                self.acknowledgeView.hidden = YES;
//                self.statusView.hidden = YES;
//                self.notAllowedView.backgroundColor = [UIColor clearColor];
//                self.notAllowedView.layer.shadowColor = [UIColor blackColor].CGColor;
//                self.notAllowedView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
//                self.notAllowedView.layer.shadowOpacity = 0.8;
//                self.notAllowedView.layer.shadowRadius = 3.0;
//                [self.notAllowedView layoutIfNeeded];
//                notAllowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//                [notAllowBtn addTarget:self action:@selector(notAllowAction:) forControlEvents:UIControlEventTouchUpInside];
//                [notAllowBtn setTitle:[[data.replyButton objectAtIndex:1] objectForKey:@"MessageButton"] forState:UIControlStateNormal];
//                [notAllowBtn setTag:[[[data.replyButton objectAtIndex:1] objectForKey:@"ReplyCode"]intValue]];
//                [notAllowBtn.titleLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:25.0]];
//                [notAllowBtn setTitleColor:[UIColor colorWithRed: 0.99 green: 0.65 blue: 0.42 alpha: 1.00] forState:UIControlStateNormal];
//                notAllowBtn.layer.masksToBounds = YES;
//                notAllowBtn.layer.cornerRadius = 10;
//                notAllowBtn.layer.borderWidth = 2.0f;
//                notAllowBtn.layer.borderColor = [UIColor colorWithRed: 0.99 green: 0.65 blue: 0.42 alpha: 1.00].CGColor;
//                notAllowBtn.frame = self.notAllowedView.bounds;
//                notAllowBtn.backgroundColor = [UIColor whiteColor];
//                [self.notAllowedView addSubview:notAllowBtn];
//                self.allowView.backgroundColor = [UIColor clearColor];
//                self.allowView.layer.shadowColor = [UIColor blackColor].CGColor;
//                self.allowView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
//                self.allowView.layer.shadowOpacity = 0.8;
//                self.allowView.layer.shadowRadius = 3.0;
//                [self.allowView layoutIfNeeded];
//                allowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//                [allowBtn addTarget:self action:@selector(allowAction:) forControlEvents:UIControlEventTouchUpInside];
//                [allowBtn setTitle:[[data.replyButton objectAtIndex:0] objectForKey:@"MessageButton"] forState:UIControlStateNormal];
//                [allowBtn setTag:[[[data.replyButton objectAtIndex:0] objectForKey:@"ReplyCode"]intValue]];
//                [allowBtn.titleLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:25.0]];
//                [allowBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                allowBtn.layer.masksToBounds = YES;
//                allowBtn.layer.cornerRadius = 10;
//                //allowBtn.backgroundColor = [UIColor greenColor];
//                allowBtn.frame = self.allowView.bounds;
//                allowBtn.backgroundColor = [UIColor colorWithRed: 0.00 green: 0.55 blue: 0.01 alpha: 1.00];
//                [self.allowView addSubview:allowBtn];
//
//            }else if (data.replyButton.count == 1){
//
//                self.allowStack.hidden = YES;
//                self.acknowledgeView.hidden = NO;
//                self.statusView.hidden = YES;
//
//               // self.acknowledgeView.hidden = NO;
//                self.acknowledgeView.backgroundColor = [UIColor clearColor];
//                self.acknowledgeView.layer.shadowColor = [UIColor blackColor].CGColor;
//                self.acknowledgeView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
//                self.acknowledgeView.layer.shadowOpacity = 0.8;
//                self.acknowledgeView.layer.shadowRadius = 3.0;
//                [self.acknowledgeView layoutIfNeeded];
//                acknowledgeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//                [acknowledgeBtn addTarget:self action:@selector(acknowledgeAction:) forControlEvents:UIControlEventTouchUpInside];
//                [acknowledgeBtn setTitle:[[data.replyButton objectAtIndex:0] objectForKey:@"MessageButton"] forState:UIControlStateNormal];
//                [acknowledgeBtn setTag:[[[data.replyButton objectAtIndex:0] objectForKey:@"ReplyCode"]intValue]];
//                [acknowledgeBtn.titleLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:25.0]];
//                [acknowledgeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                acknowledgeBtn.layer.masksToBounds = YES;
//                acknowledgeBtn.layer.cornerRadius = 10;
//                //acknowledgeBtn.backgroundColor = [UIColor greenColor];
//                acknowledgeBtn.frame = self.acknowledgeView.bounds;
//                acknowledgeBtn.backgroundColor = [UIColor colorWithRed: 0.00 green: 0.55 blue: 0.01 alpha: 1.00];
//                [self.acknowledgeView addSubview:acknowledgeBtn];
//
//
//            }else{
//                self.allowStack.hidden = YES;
//                self.acknowledgeView.hidden = YES;
//                self.statusView.hidden = YES;
//
//                self.acknowledgeConstraint.constant = 0.0;
//                self.statusHeightConstraint.constant = 0.0;
//                self.allowHeightConstraint.constant = 0.0;
//            }
       // }
        
       
        
        
        [self doCallFileMessageAPI:[UserData getUserID] messageID:data.messageID schoolid:[UserData getSchoolId]];
    }
}

#pragma mark - CallFileMessageAPI
- (void)doCallFileMessageAPI:(long long)userID messageID:(long long)messageID schoolid:(long long)schoolid{
    [self showIndicatorImg];
    [self showIndicatorFile];
    self.callFileMessageAPI = nil;
    self.callFileMessageAPI = [[ CallFileMessageAPI alloc] init];
    self.callFileMessageAPI.delegate = self;
    [self.callFileMessageAPI call:userID messageID:messageID schoolid:schoolid];
}

- (void)callFileMessageAPI:(CallFileMessageAPI *)classObj data:(NSArray *)data success:(BOOL)success{
    [self stopIndicatorImg];
    [self stopIndicatorFile];
    if (success) {
        if (data != nil || data != NULL) {
            for (int i=0; i<data.count; i++) {
                NSString *fileType = [[data objectAtIndex:i]objectForKey:@"filetype"];
                if ([fileType isEqualToString:@"image/jpeg"] || [fileType isEqualToString:@"image/jpg"]|| [fileType isEqualToString:@"image/png"]) {
                    //[imageArray addObject:[[data objectAtIndex:i]objectForKey:@"filename"] ];
                    
                    NSURL *url = [NSURL URLWithString:[[data objectAtIndex:i]objectForKey:@"filename"]];
                    NSData *data = [NSData dataWithContentsOfURL:url];
                    UIImage *img = [[UIImage alloc] initWithData:data];
                    if (img != nil || img != NULL) {
                        [imageArray addObject:img];
                    }
                    
                }else{
                    NSString *fileType = [[data objectAtIndex:i]objectForKey:@"filetype"];
                    NSString *fileName = [[data objectAtIndex:i]objectForKey:@"filename"];
                    
                    if (fileType != nil || fileType != NULL) {
                        [typeFileArray addObject:fileType];
                    }
                    
                    if (fileName != nil || fileName != NULL) {
                        [fileArray addObject:fileName];
                    }
                   
                }
            }
            
            //[self.imageCollectionView layoutIfNeeded];
            float wCollection = self.imageCollectionView.frame.size.width / ((self.imageCollectionView.frame.size.width/4)-10);
            float amountRow = imageArray.count / (int)wCollection;
            NSLog(@"num = %d",(int)wCollection);
            NSInteger modImg = imageArray.count % (int)wCollection ;
            
            if (modImg > 0) {
                 amountRow ++;
            }
            self.highImaCollectionConstraint.constant = self->imgConstant * amountRow+((amountRow*5)*2);

            //[self.fileCollectionView layoutIfNeeded];
            float wFileCollection = self.fileCollectionView.frame.size.width / ((self.fileCollectionView.frame.size.width/4)-10);
            float amountFileRow = fileArray.count / (int)wFileCollection;
            NSLog(@"num = %d",(int)wFileCollection);
            NSInteger modFile = fileArray.count % (int)wFileCollection ;
            
            if (modFile > 0) {
                amountFileRow ++;
            }
            
            self.highFileCollectionConstraint.constant = self->fileConstant * amountFileRow+((amountFileRow*5)*2);
            
            if (imageArray.count == 0) {
                self.imageCollectionView.hidden = YES;
            }
            if (fileArray.count == 0) {
                self.fileCollectionView.hidden = YES;
            }
            
            [self.imageCollectionView reloadData];
            [self.fileCollectionView reloadData];
        }
    }
}


- (void)removeDetailNewsViewController {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDetailNewsViewController {
    
    [self removeDetailNewsViewController];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(closeShowDetailNews)]) {
        [self.delegate closeShowDetailNews];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}




- (IBAction)backAction:(id)sender {
    if (typySend == 0) {
        [self dismissDetailNewsViewController];
    }
    
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (collectionView == self.imageCollectionView) {
        
        return imageArray.count;
    }else{
        return typeFileArray.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SNDetailNewsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    if (collectionView == self.imageCollectionView) {
        cell.fileBtn.tag = indexPath.row;
        //[cell.fileimage sd_setImageWithURL:[NSURL URLWithString: [imageArray objectAtIndex:indexPath.row]]];
        [cell.fileimage setImage:[imageArray objectAtIndex:indexPath.row]];
        [cell.fileBtn addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
         return cell;
    }else{
        cell.fileBtn.tag = indexPath.row;
        if ([[typeFileArray objectAtIndex:indexPath.row] isEqualToString:@"PDF"]) {
            [cell.fileimage setImage:[UIImage imageNamed:@"ic_pdf"]];
        }else if ([[typeFileArray objectAtIndex:indexPath.row] isEqualToString:@"MS Word"]){
            [cell.fileimage setImage:[UIImage imageNamed:@"ic_word"]];
        }else{
            [cell.fileimage setImage:[UIImage imageNamed:@"ic_excel"]];
        }
        [cell.fileBtn addTarget:self action:@selector(selectFileTapped:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    //[self.imageCollectionView layoutIfNeeded];
    CGSize defaultSize;
    if (collectionView == self.imageCollectionView) {
          defaultSize = CGSizeMake((self.imageCollectionView.frame.size.width/4)-10, (imgConstant));
    }else{
          defaultSize = CGSizeMake((self.fileCollectionView.frame.size.width/4)-10, (fileConstant));
    }
    return defaultSize;
}

- (void)selectFileTapped:(UIButton *)sender  {
    self.openFileView = [[SNReadFileViewController alloc] init];
    [self.openFileView showPagesReadFileInView:view partFile:[fileArray objectAtIndex:sender.tag]];
}

- (void)okButtonTapped:(UIButton *)sender{
    self.openImageView = [[SNReadImageViewController alloc]init];
    [self.openImageView showPagesReadImageInView:view ImageArray:imageArray];
}

#pragma mark - Indicator
- (void)showIndicatorImg {
    // Show the indicator
    if(![self.indicatorIma isAnimating]) {
        [self.indicatorIma startAnimating];
    }
}

- (void)stopIndicatorImg {
    [self.indicatorIma stopAnimating];
}

- (void)showIndicatorFile {
    // Show the indicator
    if(![self.indicatorFile isAnimating]) {
        [self.indicatorFile startAnimating];
    }
}

- (void)stopIndicatorFile {
    [self.indicatorFile stopAnimating];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
