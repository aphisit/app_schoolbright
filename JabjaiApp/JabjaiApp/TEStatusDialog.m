//
//  TEStatusDialog.m
//  JabjaiApp
//
//  Created by toffee on 11/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TEStatusDialog.h"

@interface TEStatusDialog (){
    BOOL isShowing;
}

@end

@implementation TEStatusDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     isShowing = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLayoutSubviews {
    
    CGFloat max = MAX(self.statusOnTimeButton.frame.size.width, self.statusOnTimeButton.frame.size.height);
    CGFloat cornerRadius = max / 2.0;
    
    self.statusOnTimeButton.layer.cornerRadius = cornerRadius;
    self.statusOnTimeButton.layer.masksToBounds = YES;
    
    self.statusLateButton.layer.cornerRadius = cornerRadius;
    self.statusLateButton.layer.masksToBounds = YES;
    
    self.statusAbsenceButton.layer.cornerRadius = cornerRadius;
    self.statusAbsenceButton.layer.masksToBounds = YES;
    
    self.statusEventButton.layer.cornerRadius = cornerRadius;
    self.statusEventButton.layer.masksToBounds = YES;
    
    self.statusPersonalLeaveButton.layer.cornerRadius = cornerRadius;
    self.statusPersonalLeaveButton.layer.masksToBounds = YES;
    
    self.statusSickLeaveButton.layer.cornerRadius = cornerRadius;
    self.statusSickLeaveButton.layer.masksToBounds = YES;
}

- (IBAction)selectStatus:(id)sender {
    
    if(self.delegate != nil) {
        
        NSInteger tag = ((UIButton *)sender).tag;
        
        TA_SCAN_STATUS scanStatus = TA_NOTSCAN;
        
        switch (tag) {
            case 1: scanStatus = TA_ONTIME; break;
            case 2: scanStatus = TA_LATE; break;
            case 3: scanStatus = TA_ABSENCE; break;
            case 4: scanStatus = TA_EVENT; break;
            case 5: scanStatus = TA_PERSONAL_LEAVE; break;
            case 6: scanStatus = TA_SICK_LEAVE; break;
        }
        
        [self.delegate taStatusDialog:self onSelectStatus:scanStatus];
        
    }
    
    [self dismissDialog];
}

#pragma mark - Dialog functions

- (void)showDialogInView:(UIView *)targetView {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view by hide it
    [self.dialogStackView setAlpha:0.0];
    
    // Animate the display of the dialog ivew
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogStackView setAlpha:1.0];
    [UIView commitAnimations];
    
    isShowing = YES;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return isShowing;
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogStackView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    isShowing = NO;
    
}



@end
