//
//  CallTEGetDataStudentScanBarcodeAPI.h
//  JabjaiApp
//
//  Created by toffee on 1/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TEDataStudentScanerModel.h"

@class CallTEGetDataStudentScanBarcodeAPI;

@protocol CallTEGetDataStudentScanBarcodeAPIDelegate <NSObject>
@optional
- (void)callTEGetDataStudentScanBarcodeAPI:(CallTEGetDataStudentScanBarcodeAPI *)classObj data:(TEDataStudentScanerModel*)data resCode:(NSInteger)resCode success:(BOOL)success;
@end

@interface CallTEGetDataStudentScanBarcodeAPI : NSObject
@property (nonatomic, weak) id<CallTEGetDataStudentScanBarcodeAPIDelegate> delegate;
- (void)call:(long long)schoolId idStudent:(NSString *)idStudent;
@end

