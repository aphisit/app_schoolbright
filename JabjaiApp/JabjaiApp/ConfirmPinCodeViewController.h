//
//  ConfirmPinCodeViewController.h
//  JabjaiApp
//
//  Created by toffee on 6/18/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallConfirmPinAPI.h"
#import "CallPayMonnyPOSTAPI.h"
#import "AlertDialog.h"
#import "SlideMenuController.h"
@protocol ConfirmPinCodeViewControllerDelegate <NSObject>

@optional
//- (void)onCancelButtonPress;
//- (void)onOkButtonPress;
- (void)successConfirmPin:(NSString*)status;
- (void)onAlertPinMoveBack;
- (void)addPinCode;
@end
@interface ConfirmPinCodeViewController : UIViewController <CallConfirmPinAPIDelegate, CallPayMonnyPOSTAPIDelegate>

@property (nonatomic, retain) id<ConfirmPinCodeViewControllerDelegate> delegate;


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerShopLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerConfirmPinLabel;


@property (nonatomic) long long shopId;
@property (nonatomic) NSString* status;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *confirmPinUnfair;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *passView1;
@property (weak, nonatomic) IBOutlet UIView *passView2;
@property (weak, nonatomic) IBOutlet UIView *passView3;
@property (weak, nonatomic) IBOutlet UIView *passView4;
@property (weak, nonatomic) IBOutlet UIView *passView5;
@property (weak, nonatomic) IBOutlet UIView *passView6;

- (IBAction)actionButtonNumber:(UIControl*)sender;
- (IBAction)moveBack:(id)sender;

- (void)dismissDialog:(NSString*)status;
- (BOOL)isDialogShowing;
- (void)showDialogInView:(UIView *)targetView productJsonString:(NSString*)productJsonString shopId:(long long)shopId ;

@end
