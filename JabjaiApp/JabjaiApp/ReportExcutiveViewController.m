//
//  ReportExcutiveViewController.m
//  JabjaiApp
//
//  Created by mac on 4/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportExcutiveViewController.h"

#import "MenuReportTableViewCell.h"

#import "ReportInOutAllDataViewController.h"
#import "ReportRefillMoneyAllDataViewController.h"
#import "ReportPurchasingAllDataViewController.h"
#import "ReportFeeViewController.h"
#import "ReportUnCheckNameViewController.h"

//#import "ExecutiveReportMainViewController.h"
#import "ReportInOutAllDataViewController.h"
#import "NavigationViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "Utils.h"

@interface ReportExcutiveViewController (){
    
    UIColor *grayColor;
    
    AlertDialog *alertDialog;
    NSMutableArray *authorizeMenuArray; // authorize Menu user
    NSMutableArray *authorizeSubMenuArray;
    
}
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;

@end

@implementation ReportExcutiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    authorizeMenuArray = [[NSMutableArray alloc] init];
    authorizeSubMenuArray = [[NSMutableArray alloc] init];
    [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//list authorize menu
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MenuReportTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    
    
//    self.tableView.scrollEnabled = false;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
    //[self showIndicator];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
    // [self stopIndicator];
    
    for (int i = 0;i < data.count; i++) {
        NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
        
        NSInteger menuId = [[[data objectAtIndex:i] objectForKey:@"menuGroup_Id"] integerValue];
        [authorizeMenuArray addObject: [NSString stringWithFormat:@"%d",menuId]];
        for (int j = 0; j < subMenuArray.count; j++) {
            NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
            [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
        }
    }
    [self.tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    UIImage *ima;
    
//    UIImage *myImage =[UIImage imageNamed:@"send_news"];
//    cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
    
    if(indexPath.row == 0) {
        if (![authorizeSubMenuArray containsObject: @"26"]) {
            cell.userInteractionEnabled = false;
            //cell.iconReport.image = [UIImage imageNamed:@"ic_in-out"];
            ima = [UIImage imageNamed:@"ic_in-out"];
            cell.BackgroundView.backgroundColor = [UIColor colorWithRed:0.74 green:0.74 blue:0.74 alpha:1.0];
            cell.nameReport.textColor = [UIColor whiteColor];
        }else{
            cell.userInteractionEnabled = true;
            //cell.iconReport.image = [UIImage imageNamed:@"in_out"];
            ima = [UIImage imageNamed:@"in_out"];
            cell.BackgroundView.backgroundColor = [UIColor whiteColor];
            cell.nameReport.textColor = [UIColor blackColor];
        }
       
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_ATTEND_CLASS",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 1) {
        if (![authorizeSubMenuArray containsObject: @"30"]) {
            cell.userInteractionEnabled = false;
            //cell.iconReport.image = [UIImage imageNamed:@"ic_top-up"];
            ima = [UIImage imageNamed:@"ic_top-up"];
            cell.BackgroundView.backgroundColor = [UIColor colorWithRed:0.74 green:0.74 blue:0.74 alpha:1.0];
            cell.nameReport.textColor = [UIColor whiteColor];
        }else{
            cell.userInteractionEnabled = true;
            //cell.iconReport.image = [UIImage imageNamed:@"edit_report"];
            ima = [UIImage imageNamed:@"edit_report"];
            cell.BackgroundView.backgroundColor = [UIColor whiteColor];
            cell.nameReport.textColor = [UIColor blackColor];
        }
        
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_UNCHECKNAME_REPORT",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 2) {
        if (![authorizeSubMenuArray containsObject: @"27"]) {
            cell.userInteractionEnabled = false;
            //cell.iconReport.image = [UIImage imageNamed:@"ic_top-up"];
            ima = [UIImage imageNamed:@"ic_top-up"];
            cell.BackgroundView.backgroundColor = [UIColor colorWithRed:0.74 green:0.74 blue:0.74 alpha:1.0];
            cell.nameReport.textColor = [UIColor whiteColor];
        }else{
            cell.userInteractionEnabled = true;
            //cell.iconReport.image = [UIImage imageNamed:@"coin"];
            ima = [UIImage imageNamed:@"coin"];
            cell.BackgroundView.backgroundColor = [UIColor whiteColor];
            cell.nameReport.textColor = [UIColor blackColor];
        }
        
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_TOPUP_REPORT",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 3) {
        
        if (![authorizeSubMenuArray containsObject: @"28"]) {
            cell.userInteractionEnabled = false;
            //cell.iconReport.image = [UIImage imageNamed:@"ic.shopping"];
            ima = [UIImage imageNamed:@"ic.shopping"];
            cell.BackgroundView.backgroundColor = [UIColor colorWithRed:0.74 green:0.74 blue:0.74 alpha:1.0];
            cell.nameReport.textColor = [UIColor whiteColor];
        }else{
            cell.userInteractionEnabled = true;
            //cell.iconReport.image = [UIImage imageNamed:@"purchase"];
            ima = [UIImage imageNamed:@"purchase"];
            cell.BackgroundView.backgroundColor = [UIColor whiteColor];
            cell.nameReport.textColor = [UIColor blackColor];
        }
        
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_PURCHASING_REPORT",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 4) {
        if (![authorizeSubMenuArray containsObject: @"29"]) {
            cell.userInteractionEnabled = false;
            //cell.iconReport.image = [UIImage imageNamed:@"ic_Tuitionfee"];
            ima = [UIImage imageNamed:@"ic_Tuitionfee"];
            cell.BackgroundView.backgroundColor = [UIColor colorWithRed:0.74 green:0.74 blue:0.74 alpha:1.0];
            cell.nameReport.textColor = [UIColor whiteColor];
        }else{
            cell.userInteractionEnabled = true;
            //cell.iconReport.image = [UIImage imageNamed:@"report_bank"];
            ima = [UIImage imageNamed:@"report_bank"];
            cell.BackgroundView.backgroundColor = [UIColor whiteColor];
            cell.nameReport.textColor = [UIColor blackColor];
        }
        
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_REPORTPAT_TUITION_FEES",nil,[Utils getLanguage],nil);
    }
       // UIImage *myImage =[UIImage imageNamed:@"send_news"];
    cell.iconReport.image =[self imageWithImage:ima scaledToSize:CGSizeMake(cell.iconReport.frame.size.width,cell.iconReport.frame.size.height)];
    [cell.BackgroundView layoutIfNeeded];
    [cell.BackgroundView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.BackgroundView.layer setShadowOpacity:0.3];
    [cell.BackgroundView.layer setShadowRadius:3.0];
    [cell.BackgroundView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if( (indexPath.row == 0)) {
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportExcutiveViewController:numberPage:)]) {
            [self.delegate changePageReportExcutiveViewController:self numberPage:0];
        }
    }
    else if (indexPath.row == 1) {
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportExcutiveViewController:numberPage:)]) {
            [self.delegate changePageReportExcutiveViewController:self numberPage:1];
        }
    }
    else if (indexPath.row == 2) {
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportExcutiveViewController:numberPage:)]) {
            [self.delegate changePageReportExcutiveViewController:self numberPage:2];
        }
    }
    else if (indexPath.row == 3) {
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportExcutiveViewController:numberPage:)]) {
            [self.delegate changePageReportExcutiveViewController:self numberPage:3];
        }
    }
    else if (indexPath.row == 4) {
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportExcutiveViewController:numberPage:)]) {
            [self.delegate changePageReportExcutiveViewController:self numberPage:4];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    // Here pass new size you need
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
