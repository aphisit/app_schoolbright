//
//  ReportCheckFlagPoleCalendarViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportCheckFlagPoleCalendarViewController.h"

#import "TAReportStudentStatusTableViewCell.h"
#import "Utils.h"
#import "DateUtility.h"
#import "UserData.h"
#import "ReportCheckFlagHistoryStudentAllViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

//#import "ReportCheckFlagPoleViewController.h"

//#import "ReportCheckNameViewController.h"

@interface ReportCheckFlagPoleCalendarViewController (){
    
    NSMutableArray<RPCheckNameFlagAttendanceModel *>  *studentStatusArray,*studentStatusOnTimeArray, *studentStatusLateArray, *studentStatusAbsenArray, *studentStatusSickArray, *studentStatusPersonalArray, *studentStatusEventArray,*studentStatusUndefinedArray,*studentStatusAllArray;
    
   
    
    
    NSDate *consideredDate;
    NSDateFormatter *dateFormatter;
    
    UIColor *grayColor;
    
    long long classroomId;
    NSString *subjectId;
    
    UIColor *headerTitleColor;
    UIColor *todayColor;
    UIColor *selectionColor;
    
    NSCalendar *gregorian;
    
    UIColor *dodgerBlue, *aqua, *monaLisa, *bitterSweet, *ripTide, *mountainMeadow, *perfume, *heliotRope, *alto, *dustyGray, *peachOrange, *texasRose;
    
    NSInteger onTimeSum, lateSum, absenceSum, sickSum, personalSum, eventSum, studentSum, undefinedSum;
    
}

@property (nonatomic) CGFloat shadowRadius;
@property (nonatomic) CGFloat shadowOpacity;
@property (nonatomic) CGFloat animationDuration;

@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;

@property (strong, nonatomic) CallGetTEHistoryStudentStatusList *callGetTEHistoryStudentStatusListAPI;

@end

static NSString *cellIdentifier = @"Cell";

@implementation ReportCheckFlagPoleCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    selectionColor = [UIColor colorWithRed:212/255.0 green:44/255.0 blue:31/255.0 alpha:1.0];
    
    //04/05/2017
    
    dodgerBlue = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0]; // Now Full
    aqua = [UIColor colorWithRed:0.21 green:0.89 blue:1.00 alpha:1.0]; // Now Select
    monaLisa = [UIColor colorWithRed:1.00 green:0.68 blue:0.62 alpha:1.0]; // Color status 0 Before Select
    bitterSweet = [UIColor colorWithRed:1.00 green:0.48 blue:0.38 alpha:1.0]; // Color status 0 After Select
    ripTide = [UIColor colorWithRed:0.47 green:0.93 blue:0.71 alpha:1.0]; // Color status 1 Before Select
    mountainMeadow = [UIColor colorWithRed:0.13 green:0.76 blue:0.46 alpha:1.0]; // Color status 1 After Select
    perfume = [UIColor colorWithRed:0.80 green:0.62 blue:0.98 alpha:1.0]; // Color status 2 Before Select
    heliotRope = [UIColor colorWithRed:0.75 green:0.51 blue:1.00 alpha:1.0]; // Color status 2 After Select
    alto = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1.0]; // Color status 3 Before Select
    dustyGray = [UIColor colorWithRed:0.60 green:0.60 blue:0.60 alpha:1.0]; // Color status 3 After Select
    peachOrange = [UIColor colorWithRed:1.00 green:0.85 blue:0.59 alpha:1.0]; // Color status 4 Before Select
    texasRose = [UIColor colorWithRed:1.00 green:0.75 blue:0.31 alpha:1.0]; // Color status 4 After Select
    gregorian = [Utils getGregorianCalendar];
    self.minimumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
    self.maximumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:1 toDate:[NSDate date] options:0];
    self.shadowRadius = 5.0f;
    self.shadowOpacity = 0.5f;
    self.animationDuration = 0.55f;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height >= 1024.0f) {
            self.heightCalendarConstraint.constant = 450.0f;
        }
    }
    else{
        self.heightCalendarConstraint.constant = 300.0f;
    }
    
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_up"] forState:UIControlStateNormal];
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_down"] forState:UIControlStateSelected];
    
    classroomId = self.selectedClassroomId;
    dateFormatter = [Utils getDateFormatter];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    if(consideredDate == nil) {
        consideredDate = [NSDate date];
    }
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:consideredDate];

    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionNextView:)];
    [self.viewNextAction addGestureRecognizer:singleFingerTap];
    [self getHistoryStudentStatus];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_REPORT_PLAGPOLE",nil,[Utils getLanguage],nil);
    
    self.headerOntimeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ONTIME",nil,[Utils getLanguage],nil);
    self.headerLastLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_LATE",nil,[Utils getLanguage],nil);
    self.headerAbsenceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ABSENCE",nil,[Utils getLanguage],nil);
    self.headerSickLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_SICK",nil,[Utils getLanguage],nil);
    self.headerEventLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_EVENT",nil,[Utils getLanguage],nil);
    self.headerPersonalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PERSONAL",nil,[Utils getLanguage],nil);
    self.headerUndefinedLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_UNDEFINED",nil,[Utils getLanguage],nil);
}

- (void)createCalendar {
    
    // Remove all subviews from container
    // Remove all subviews from container
    //    NSArray *viewsToRemove = self.containerView.subviews;
    //    for(UIView *v in viewsToRemove) {
    //        [v removeFromSuperview];
    //    }
    
    self.containerView.backgroundColor = grayColor;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, height)];
    
    calendar.dataSource = self;
    calendar.delegate = self;
    //calendar.swipeToChooseGesture.enabled = YES;
    calendar.backgroundColor = grayColor;//[UIColor whiteColor];
    //    calendar.appearance.titleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:22.0f];
    //    calendar.appearance.subtitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:22.0f];
    //    calendar.appearance.weekdayFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:22.0f];
    //    calendar.appearance.headerTitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:26.0f];
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
//    calendar.appearance.selectionColor = selectionColor;
//    calendar.appearance.todayColor = todayColor;
    
    // 04/05/2018
    calendar.appearance.todayColor = dodgerBlue;
    calendar.appearance.titleTodayColor = [UIColor whiteColor];
    calendar.appearance.todaySelectionColor = dodgerBlue;
    
    calendar.appearance.selectionColor = [UIColor clearColor];
    calendar.appearance.titleSelectionColor = [UIColor blackColor];
    calendar.appearance.borderSelectionColor = dodgerBlue;
    
    [self.containerView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 95, 34);
    previousButton.backgroundColor = grayColor;//[UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    nextButton.frame = CGRectMake(CGRectGetWidth(self.containerView.frame) - 95, 5, 95, 34);
    nextButton.frame = CGRectMake(self.view.frame.size.width - nextButton.frame.size.width - 95, 5, 95, 34);
    nextButton.backgroundColor = grayColor; //[UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:nextButton];
    self.nextButton = nextButton;
    
    //    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    //
    //    self.panelControllerContainer.visibleZoneHeight = screenHeight - (self.calendar.frame.size.height + self.calendar.frame.origin.y) - (self.headerView.frame.origin.y + self.headerView.frame.size.height);
    //    self.panelControllerContainer.swipableZoneHeight = 0;
    //    self.panelControllerContainer.draggingEnabled = YES;
    //    self.panelControllerContainer.shouldOverlapMainViewController = YES;
    //    self.panelControllerContainer.shouldShiftMainViewController = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self createCalendar];
}

#pragma mark - CFSCalendarDataSource

- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

#pragma mark - CFSCalendarDelegate

- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
    if([DateUtility sameMonthOfYear:date date2:calendar.currentPage]) {
        return YES;
    }
    
    return NO;
}

- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
    //    if(self.delegate && [self.delegate respondsToSelector:@selector(onSelectDate:date:)]) {
    //        [self.delegate onSelectDate:self date:date];
    //    }
    
    self.dateTitle.text = [Utils getThaiDateFormatWithDate: date];
    
    if(![DateUtility sameDate:consideredDate date2:date] ) {
        consideredDate = date;
        [self getHistoryStudentStatus];
    }
}

#pragma mark - Class buttons event

- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
}

- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)moveBack:(id)sender {
    
    ReportCheckNameViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportCheckNameStoryboard"];
        
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}

- (IBAction)openDetail:(id)sender {
    
    self.arrowStatement.selected = !self.arrowStatement.selected;
    
    if (self.arrowStatement.isSelected == YES) {
        //        self.topDetailConstraint.constant = -300.0f;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailContraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailContraint.constant = -450.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailContraint.constant = 0.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailContraint.constant = -300.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
        
    }else{
        //        self.topDetailConstraint.constant = 0.0f;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailContraint.constant = -450.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailContraint.constant = 0.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailContraint.constant = -300.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailContraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
    }
    
    
}

- (void)getHistoryStudentStatus {
    
    self.callGetTEHistoryStudentStatusListAPI = nil;
    self.callGetTEHistoryStudentStatusListAPI = [[CallGetTEHistoryStudentStatusList alloc] init];
    self.callGetTEHistoryStudentStatusListAPI.delegate = self;
    [self.callGetTEHistoryStudentStatusListAPI call:[UserData getSchoolId] classroomId:classroomId date:consideredDate];
    
    [self showIndicator];
}

#pragma mark - CallGetTAHistoryStudentStatusListAPIDelegate
- (void)callGetTEHistoryStudentStatusListAPI:(CallGetTEHistoryStudentStatusList *)classObj data:(NSMutableArray<RPCheckNameFlagAttendanceModel *> *)data success:(BOOL)success{
    
    [self stopIndicator];
    
    if(success && data != nil) {
        studentStatusArray = data;
        onTimeSum = 0;
        lateSum = 0;
        absenceSum = 0;
        personalSum = 0;
        sickSum = 0;
        eventSum = 0;
        studentSum = 0;
        undefinedSum = 0;
        
        studentStatusOnTimeArray = [[NSMutableArray alloc] init];
        studentStatusLateArray = [[NSMutableArray alloc] init];
        studentStatusAbsenArray = [[NSMutableArray alloc] init];
        studentStatusPersonalArray = [[NSMutableArray alloc] init];
        studentStatusSickArray = [[NSMutableArray alloc] init];
        studentStatusEventArray = [[NSMutableArray alloc] init];
        studentStatusUndefinedArray = [[NSMutableArray alloc] init];
        studentStatusAllArray = [[NSMutableArray alloc] init];
        
        for (int i = 0; i<data.count; i++) {
            
            RPCheckNameFlagAttendanceModel *model = [data objectAtIndex:i];
            if ([model getStudentState] == 0) {
                [studentStatusAllArray addObject:model];
                if ([model getStatus] == 0) {
                    [studentStatusOnTimeArray addObject:model];
                    onTimeSum ++;
                }
                else if ([model getStatus] == 1){
                    [studentStatusLateArray addObject:model];
                    lateSum ++;
                }
                else if ([model getStatus] == 3){
                    [studentStatusAbsenArray addObject:model];
                    absenceSum ++;
                }
                else if ([model getStatus] == 10){
                    [studentStatusPersonalArray addObject:model];
                    personalSum ++;
                }
                else if ([model getStatus] == 11){
                    [studentStatusSickArray addObject:model];
                    sickSum ++;
                }
                else if ([model getStatus] == 12){
                    [studentStatusEventArray addObject:model];
                    eventSum ++;
                }else{
                    [studentStatusUndefinedArray addObject:model];
                    undefinedSum ++;
                }
            }
            
            
            
        }
        studentSum = onTimeSum + lateSum + absenceSum + personalSum + sickSum + eventSum + undefinedSum;
        
        self.onTimeLabel.text = [NSString stringWithFormat:@"%d %@",onTimeSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.lateLabel.text = [NSString stringWithFormat:@"%d %@",lateSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.absenceLabel.text = [NSString stringWithFormat:@"%d %@",absenceSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.sickLeaveLabel.text = [NSString stringWithFormat:@"%d %@",sickSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.personalLeaveLabel.text = [NSString stringWithFormat:@"%d %@",personalSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.eventLabel.text = [NSString stringWithFormat:@"%d %@",eventSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.studentTotalLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %d %@",NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ROOM",nil,[Utils getLanguage],nil), self.nameClassRoom, NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_AMOUNT_STUDENT",nil,[Utils getLanguage],nil), studentSum, NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.undefinedLabel.text = [NSString stringWithFormat:@"%d %@",undefinedSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
      
    }
}


#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (void)actionNextView:(UITapGestureRecognizer *)recognizer
{
   // CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    ReportCheckFlagHistoryStudentAllViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportCheckFlagHistoryStudentAllStoryboard"];
    viewController.studentStatusOnTimeArray = studentStatusOnTimeArray;
    viewController.studentStatusLateArray = studentStatusLateArray;
    viewController.studentStatusPersonalArray = studentStatusPersonalArray;
    viewController.studentStatusSickArray = studentStatusSickArray;
    viewController.studentStatusEventArray = studentStatusEventArray;
    viewController.studentStatusAbsenArray = studentStatusAbsenArray;
    viewController.studentStatusAllArray = studentStatusAllArray;
    viewController.studentAllArray = studentStatusArray;
    viewController.studentSum = studentSum;
    viewController.studentStatusUndefinedArray = studentStatusUndefinedArray;
    viewController.nameClassRoom = self.nameClassRoom;
    viewController.selectedClassroomId = self.selectedClassroomId;
    viewController.consideredDate = consideredDate;
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    //Do stuff here...
}



@end
