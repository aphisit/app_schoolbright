  //
//  DBManager.m
//  JabjaiInboxMessageDBApp
//
//  Created by mac on 11/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "DBManager.h"

@interface DBManager()

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;
@property (nonatomic, strong) NSMutableArray *arrResults;

-(void)copyDatabaseIntoDocumentsDirectory;
-(BOOL)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;

@end

@implementation DBManager

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename {
    self = [super init];
    if(self) {
        
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        
        // Keep the database filename.
        self.databaseFilename = dbFilename;
        
        // Copy the database file into the documents directory if necessary.
        [self copyDatabaseIntoDocumentsDirectory];
    }
    
    return self;
}

-(void)copyDatabaseIntoDocumentsDirectory {
    
    // Check if the database file exists in the documents directory.
    NSString *destinationPath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
        
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        // Check if any error occured during copying and display it.
        if(error != nil) {
            NSLog(@"An error occured during copying the database file : %@", [error localizedDescription]);
        }
    }
}

- (BOOL)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable {
    
       
    
    
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
   
    // Initialize the results array
    if(self.arrResults != nil) {
        [self.arrResults removeAllObjects];
        self.arrResults = nil;
    }
    
    self.arrResults = [[NSMutableArray alloc] init];
    
    // Initialize the column names array.
    if(self.arrColumnNames != nil) {
        [self.arrColumnNames removeAllObjects];
        self.arrColumnNames = nil;
    }
    
    self.arrColumnNames = [[NSMutableArray alloc] init];
    
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL);
        
        if(prepareStatementResult == SQLITE_OK) {
            
            // Check if the query is non-executable.
            if(!queryExecutable) {
                // In this case data must be loaded from the database.
                
                // Declare an array to keep the data for each fetched row.
                NSMutableArray *arrDataRow;
                
                // Loop through the results and add them to the results array row by row.
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    
                    // Initialize the mutable array that will contain the data of a fetched row.
                    arrDataRow = [[NSMutableArray alloc] init];
                    
                    // Get the total number of columns.
                    int totalColumns = sqlite3_column_count(compiledStatement);
                    
                    // Go through all columns and fetch each column data.
                    for(int i=0; i<totalColumns; i++) {
                        // Convert the column data to text (characters).
                        char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
                        
                        // If there are contents in the current column (field) then add them to the current row array.
                        if(dbDataAsChars != NULL) {
                            // Convert the characters to string.
                            [arrDataRow addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                        }else{
                            [arrDataRow addObject:[NSNull null]];
                        }
                        
                        // Keep the current column name.
                        if(self.arrColumnNames.count != totalColumns) {
                            dbDataAsChars = (char *)sqlite3_column_name(compiledStatement, i);
                            [self.arrColumnNames addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                            
                        }
                    }
                    
                    // Store each fetched data row in the results array, but first check if there is actually data.
                    if(arrDataRow.count > 0) {
                        [self.arrResults addObject:arrDataRow];
                    }

                }
                
                return YES;
                
            }
            else {
                
                // This is the case of an executable query (insert, update, ...).
                
                // Execute the query.
                int excuteQueryResults = sqlite3_step(compiledStatement);
                if(excuteQueryResults == SQLITE_DONE) {
                    
                    // Keep the affected rows.
                    self.affectedRows = sqlite3_changes(sqlite3Database);
                    
                    // Keep the last inserted row ID.
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
                    
                    return YES;
                }
                else {
                    // If could not execute the query show the error message on the debugger.
                    NSLog(@"%s", sqlite3_errmsg(sqlite3Database));
                    
                    return NO;
                }
                
            }
            
        }
        
    }
    else {
        // In the database cannot be opened then show the error message on the debugger.
        NSLog(@"%s", sqlite3_errmsg(sqlite3Database));
        
        return NO;
    }
    
    // Close the database.
    sqlite3_close(sqlite3Database);
    
    return YES;
}


-(NSArray *)loadDataFromDB:(NSString *)query {
    // Run the query and indicate that is not executable.
    // The query string is converted to a char * object.
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    // Returned the loaded results.
    return (NSArray *)self.arrResults;
}

-(BOOL)executeQuery:(NSString *)query {
    // Run the query and indicate that is executable.
    return [self runQuery:[query UTF8String] isQueryExecutable:YES];
}

- (void)insertColumnQuery:(const char *)query{
    
                sqlite3 *database;
                sqlite3_stmt *statement;
                // Set the database file path.
                NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
                if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
                {
//                    NSString *updateSQL = [NSString stringWithFormat: @"ALTER TABLE user_info ADD COLUMN token VARCHAR"];
//                    const char *update_stmt = [updateSQL UTF8String];
                    sqlite3_prepare_v2(database, query, -1, &statement, NULL);
                    if(sqlite3_step(statement)==SQLITE_DONE)
                    {
                       // [self checkColumnExists];
                        NSLog(@"DB Altered");
                    }
                    else
                    {
                        NSLog(@"DB not Altered");
                    }
    
                    sqlite3_finalize(statement);
                    sqlite3_close(database);
                }
    
 
}

-(void)checkColumnExists
{
    const char *query = "select token from user_info";
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL);
        
        if(prepareStatementResult == SQLITE_OK) {
            NSLog(@"DB altered");
        }else{
            NSString *addColomnQuery = [NSString stringWithFormat:@"ALTER TABLE user_info ADD COLUMN token VARCHAR"];
            [self insertColumnQuery:[addColomnQuery UTF8String]];
            
            NSString *deleteAllQuery = [NSString stringWithFormat:@"DELETE FROM user_info"];
            [self insertColumnQuery:[deleteAllQuery UTF8String]];

        }
        // Close the database.
        sqlite3_finalize(compiledStatement);
        sqlite3_close(sqlite3Database);
    }
  
}

-(void) doCheckcolumnToken{
    
    const char *query = "select token from user_info";
    // Create a sqlite object.
    sqlite3 *sqlite3Database;

    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];

    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);

    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;

        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL);

        if(prepareStatementResult == SQLITE_OK) {
            NSLog(@"DB altered");
            
        }else{
            NSString *addColomnQuery = [NSString stringWithFormat:@"ALTER TABLE user_info ADD COLUMN token VARCHAR"];
            [self insertColumnQuery:[addColomnQuery UTF8String]];

//            NSString *deleteAllQuery = [NSString stringWithFormat:@"DELETE FROM user_info"];
//            [self insertColumnQuery:[deleteAllQuery UTF8String]];
        
        }
        // Close the database.
        sqlite3_finalize(compiledStatement);
        sqlite3_close(sqlite3Database);
    }
}

- (void) doCheckcolumnUsername{
    const char *query = "select username from user_info";
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;

        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL);

        if(prepareStatementResult == SQLITE_OK) {
            NSLog(@"DB altered");
            
        }else{
            NSString *addColomnQuery = [NSString stringWithFormat:@"ALTER TABLE user_info ADD COLUMN username VARCHAR"];
            [self insertColumnQuery:[addColomnQuery UTF8String]];
            
//            NSString *deleteAllQuery = [NSString stringWithFormat:@"DELETE FROM user_info"];
//            [self insertColumnQuery:[deleteAllQuery UTF8String]];
        }
        // Close the database.
        sqlite3_finalize(compiledStatement);
        sqlite3_close(sqlite3Database);
    }
}

- (void) doCheckcolumnPassword{
    const char *query = "select password from user_info";
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;

        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL);

        if(prepareStatementResult == SQLITE_OK) {
            NSLog(@"DB altered");
        }else{
            NSString *addColomnQuery = [NSString stringWithFormat:@"ALTER TABLE user_info ADD COLUMN password VARCHAR"];
            [self insertColumnQuery:[addColomnQuery UTF8String]];
            
//            NSString *deleteAllQuery = [NSString stringWithFormat:@"DELETE FROM user_info"];
//            [self insertColumnQuery:[deleteAllQuery UTF8String]];
        }
        // Close the database.
        sqlite3_finalize(compiledStatement);
        sqlite3_close(sqlite3Database);
    }
}

@end
