//
//  ReportCheckFlagDateTimeOfStudentTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 3/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportCheckFlagDateTimeOfStudentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *statusBtn;
- (void)updateStatus:(int)status;

@end

NS_ASSUME_NONNULL_END
