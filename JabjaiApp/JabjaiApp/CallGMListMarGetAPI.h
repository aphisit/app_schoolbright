//
//  CallGMListMarGetAPI.h
//  JabjaiApp
//
//  Created by toffee on 4/10/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MargetListModel.h"



@class CallGMListMarGetAPI;

@protocol CallGMListMarGetAPIDelegate <NSObject>

- (void)callGMListMarGetAPI:(CallGMListMarGetAPI *)classObj data:(NSArray<MargetListModel *> *)data success:(BOOL)success;

@end

@interface CallGMListMarGetAPI : NSObject
@property (nonatomic, weak) id<CallGMListMarGetAPIDelegate> delegate;
- (void)call:(long long)schoolId;

@end
