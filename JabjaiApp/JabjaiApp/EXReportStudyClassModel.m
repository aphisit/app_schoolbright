//
//  EXReportStudyClassModel.m
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "EXReportStudyClassModel.h"

@implementation EXReportStudyClassModel

@synthesize iid = _iid;
@synthesize title = _title;
@synthesize reportStatusDetailArray = _reportStatusDetailArray;

- (void)setIId:(long long)iid {
    _iid = iid;
}

- (void)setTitle:(NSString *)title {
    _title = title;
}

- (void)setReportStatusDetailArray:(NSMutableArray<EXReportStatusDetailModel *> *)reportStatusDetailArray {
    _reportStatusDetailArray = reportStatusDetailArray;
}

- (long long)getIId {
    return _iid;
}

- (NSString *)getTitle {
    return _title;
}

- (NSMutableArray<EXReportStatusDetailModel *> *)getReportStatusDetailArray {
    return _reportStatusDetailArray;
}

@end
