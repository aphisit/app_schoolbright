//
//  NewsMessageDataLoadingOperation.m
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NewsMessageDataLoadingOperation.h"
#import "MessageInboxDataModel.h"
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"

@interface NewsMessageDataLoadingOperation() {
    NSInteger connectCounter;
}

@end

@implementation NewsMessageDataLoadingOperation

- (instancetype)initWithPage:(NSUInteger)page indexes:(NSIndexSet *)indexes {
    self = [super init];
    
    if(self) {
        _page = page;
        _indexes = indexes;
        connectCounter = 0;
        
        // Establish the weak self reference
        __weak typeof(self) weakSelf = self;
        
        [self addExecutionBlock:^{
            
            // Establish the strong self reference
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf getNewsMessageDataWithPage:page strongSelf:strongSelf];
            
        }];
    }
    
    return self;
}

- (void)downloadSuccess:(void (^)(NSUInteger, NSIndexSet *, NSArray *))finishBlock {
    self.downloadCompletion = finishBlock;
}

#pragma mark - Get API Data
- (void)getNewsMessageDataWithPage:(NSUInteger)page strongSelf:(NewsMessageDataLoadingOperation *)strongSelf {
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getNewsMessageWithPage:page userID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getNewsMessageDataWithPage:page strongSelf:strongSelf];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getNewsMessageDataWithPage:page strongSelf:strongSelf];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getNewsMessageDataWithPage:page strongSelf:strongSelf];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    int status = [[dataDict objectForKey:@"nStatus"] intValue];
                    int type = [[dataDict objectForKey:@"nType"] intValue];
                    NSMutableString *messageDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dSend"]];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sTitle"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    NSRange dotRange = [messageDateStr rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:messageDateStr];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:messageDateStr];
                    }
                    
                    MessageInboxDataModel *model = [[MessageInboxDataModel alloc] init];
                    model.messageID = messageID;
                    model.userID = (int)[UserData getUserID];
                    model.messageType = type;
                    model.status =status;
                    model.title = title;
                    model.message = message;
                    model.date = messageDate;
                    
                    [newDataArr addObject:model];
                    
                }
                
                strongSelf->_dataPage = newDataArr;
                
                if(self.downloadCompletion) {
                    self.downloadCompletion(_page, _indexes, _dataPage);
                }
            }
            
        }
    }];
    
}

@end
