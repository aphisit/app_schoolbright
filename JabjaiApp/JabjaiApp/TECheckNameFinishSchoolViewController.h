//
//  TECheckNameFinishSchoolViewController.h
//  JabjaiApp
//
//  Created by toffee on 30/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"

#import "TEPadNumberCheckNameFinishSchoolViewController.h"
#import "TEScanCheckNameFinishSchoolViewController.h"
#import "SlideMenuController.h"
#import "ConfirmScanerLateViewController.h"
#import "CallGetTEFinishschoolAPI.h"
#import "AlertDialogConfirm.h"
#import "AlertDialog.h"
NS_ASSUME_NONNULL_BEGIN

@interface TECheckNameFinishSchoolViewController : UIViewController<CAPSPageMenuDelegate, TEScanCheckNameFinishSchoolDelegate,TEPadNumberCheckNameFinishSchoolDelegate, SlideMenuControllerDelegate, ConfirmScanerLateViewControllerDelegate, CallGetTEFinishschoolAPIDelegate, AlertDialogConfirmDelegate,AlertDialogDelegate>

@property (strong, nonatomic) TEPadNumberCheckNameFinishSchoolViewController *tEPadNumberCheckNameFinishSchoolViewController;
@property (strong, nonatomic) TEScanCheckNameFinishSchoolViewController *tEScanCheckNameFinishSchoolViewController;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
- (IBAction)openDrawer:(id)sender;
@end

NS_ASSUME_NONNULL_END
