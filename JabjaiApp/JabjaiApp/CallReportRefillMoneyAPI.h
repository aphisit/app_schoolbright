//
//  CallReportRefillMoneyAPI.h
//  JabjaiApp
//
//  Created by toffee on 10/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CallReportRefillMoneyAPI;

@protocol CallReportRefillMoneyAPIDelegate <NSObject>

@optional

- (void)callReportRefillMoneyAPI:(CallReportRefillMoneyAPI *)classObj timeArray:(NSArray *)timeArray totalMoneyArray:(NSArray *)totalMoneyArray success:(BOOL)success;


@end
@interface CallReportRefillMoneyAPI : NSObject
@property (nonatomic, weak) id<CallReportRefillMoneyAPIDelegate> delegate;

- (void)call:(long long)schoolId date:(NSString*)date;

@end
