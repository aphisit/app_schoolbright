//
//  CallGetTEFinishschoolAPI.h
//  JabjaiApp
//
//  Created by toffee on 14/11/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CallGetTEFinishschoolAPI;
@protocol CallGetTEFinishschoolAPIDelegate <NSObject>
- (void)callGetTEFinishschoolAPI:(CallGetTEFinishschoolAPI *)classObj resCode:(NSInteger)resCode success:(BOOL)success;
@end
@interface CallGetTEFinishschoolAPI : NSObject
@property (nonatomic, weak) id<CallGetTEFinishschoolAPIDelegate> delegate;
- (void)call:(long long)schoolId userId:(long long)userId barcode:(NSString*)barcode status:(NSString*)status ;
@end

NS_ASSUME_NONNULL_END
