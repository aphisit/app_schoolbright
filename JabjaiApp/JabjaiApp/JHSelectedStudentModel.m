//
//  JHSelectedStudentModel.m
//  JabjaiApp
//
//  Created by toffee on 11/22/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "JHSelectedStudentModel.h"

@implementation JHSelectedStudentModel

@synthesize studentId = _studentId;
@synthesize studentName = _studentName;
@synthesize pic = _pic;
@synthesize picverion = _picverion;
@synthesize selected = _selected;

- (void)setStudentId:(long long)studentId {
    _studentId = studentId;
}

- (void)setStudentName:(NSString *)studentName {
    _studentName = studentName;
}
- (void)setStudentPic:(NSString *)pic {
    _pic = pic;
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
}
- (void)setPicverion:(long long)picverion {
    _picverion = picverion;
}

- (long long)getStudentId {
    return _studentId;
}

- (NSString *)getStudentName {
    return _studentName;
}
- (NSString *)getStudentPic {
    return _pic;
}

- (BOOL)isSelected {
    return _selected;
}
- (long long)getPicverion {
    return _picverion;
}

@end
