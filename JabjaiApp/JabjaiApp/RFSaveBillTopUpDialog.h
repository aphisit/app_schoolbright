//
//  RFSaveBillTopUpDialog.h
//  JabjaiApp
//
//  Created by toffee on 19/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@protocol RFSaveBillTopUpDialogDelegate <NSObject>
@optional
- (void)onAlertDialogClose;
@end
@interface RFSaveBillTopUpDialog : UIViewController
@property (retain, nonatomic) id<RFSaveBillTopUpDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;


- (void)showDialogInView:(UIView *)targetView ;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end

NS_ASSUME_NONNULL_END
