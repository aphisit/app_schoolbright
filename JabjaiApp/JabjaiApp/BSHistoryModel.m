//
//  BSHistoryModel.m
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSHistoryModel.h"

@implementation BSHistoryModel

@synthesize behaviorType = _behaviorType;
@synthesize behaviorName = _behaviorName;
@synthesize behaviorScore = _behaviorScore;
@synthesize dateTime = _dateTime;
@synthesize studentName = _studentName;
@synthesize recorder = _recorder;
@synthesize remark = _remark;
@synthesize picStudent = _picStudent;

- (void)setBehaviorType:(NSInteger)behaviorType {
    _behaviorType = behaviorType;
}

- (void)setBehaviorName:(NSString *)behaviorName {
    _behaviorName = behaviorName;
}

- (void)setBehaviorScore:(double)behaviorScore {
    _behaviorScore = behaviorScore;
}

- (void)setDateTime:(NSDate *)dateTime {
    _dateTime = dateTime;
}

- (void)setStudentName:(NSString *)studentName {
    _studentName = studentName;
}

- (void)setRecorder:(NSString *)recorder {
    _recorder = recorder;
}

- (void)setRemark:(NSString *)remark {
    _remark = remark;
}

- (void)setPicStudent:(NSString *)picStudent{
    _picStudent = picStudent;
}



- (NSInteger)getBehaviorType {
    return _behaviorType;
}

- (NSString *)getBehaviorName {
    return _behaviorName;
}

- (double)getBehaviorScore {
    return _behaviorScore;
}

- (NSDate *)getDateTime {
    return _dateTime;
}

- (NSString *)getStudentName {
    return _studentName;
}

- (NSString *)getRecorder {
    return _recorder;
}

- (NSString *)getRemark {
    return _remark;
}
- (NSString *)getPicStudent {
    return _picStudent;
}

@end
