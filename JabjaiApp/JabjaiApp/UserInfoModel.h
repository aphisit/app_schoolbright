//
//  UserInfoModel.h
//  JabjaiApp
//
//  Created by mac on 12/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfoModel : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (nonatomic) NSInteger sex; //0: male, 1: female
@property (strong, nonatomic) NSDate *birthday;
@property (strong, nonatomic) NSString *studentCode;
@property (strong, nonatomic) NSString *studentClass;
@property (strong, nonatomic) NSString *schoolName;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *department;
@property (strong, nonatomic) NSString *position;
@property (strong, nonatomic) NSString *sIdentification;
@property (strong, nonatomic) NSString *studentId;
@property (nonatomic) float money;
@property (nonatomic) float creditLimits;
@property (nonatomic) NSNumber *score;
@property (strong, nonatomic) NSString *imageUrl;

@end
