//
//  ReportListTeacherViewController.m
//  JabjaiApp
//
//  Created by mac on 4/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportListTeacherViewController.h"
#import "AlertDialog.h"
#import "Utils.h"
#import "UserData.h"
@interface ReportListTeacherViewController (){
    CAPSPageMenu *pageMenu;
    UIColor *OrangeColor;
    UIColor *GrayColor;
    AlertDialog *alertDialog;
}
@end

@implementation ReportListTeacherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    OrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1.0];
    GrayColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_REPORT",nil,[Utils getLanguage],nil);
    [self doDesignLayout];
    [self setupPagemenu];
}

//- (void)viewDidLayoutSubviews{
//    [self doDesignLayout];
//    [self setupPagemenu];
//}

- (void) doDesignLayout{
    
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
  
}


- (void)changePageReportStudentViewController:(ReportStudentViewController *)classObj numberPage:(int)numberPage{
    if (numberPage == 0) {
        ReportCheckNameViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportCheckNameStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 1){
        
        [self showAlertDialogWithTitle:@"" message:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_COMING_SOON",nil,[Utils getLanguage],nil)];
        
//        ReportLeaveAllDataSelectClassViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportLeaveAllDataSelectClassStoryboard"];
//        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 2){
        ReportBehaviorAllDataSelectViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportBehaviorAllDataSelectStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 3){
        ReportOrderHomeWorkViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportOrderHomeWorkStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 4){
        ReportSendNewsListViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportSendNewsListStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
}

- (void)changePageReportExcutiveViewController:(ReportExcutiveViewController *)classObj numberPage:(int)numberPage{
    
    if (numberPage == 0) {
        ReportInOutAllDataViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutAllDataStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 1){
        ReportUnCheckNameViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportUnCheckNameStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 2){
        ReportRefillMoneyAllDataViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportRefillMoneyAllDataStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 3){
        ReportPurchasingAllDataViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportPurchasingAllDataStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 4){
        [self showAlertDialogWithTitle:@"" message:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_COMING_SOON",nil,[Utils getLanguage],nil)];
//        ReportFeeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportFeeStoryboard"];
//        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    
}

- (void)changePageReportPersonViewController:(ReportPersonViewController *)classObj numberPage:(int)numberPage{
    if (numberPage == 0) {
        ReportInOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 1){
        ReportLeaveViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportLeaveStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 2){
        ReportRefillMoneyViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportRefillMoneyStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (numberPage == 3){
        ReportPurchasingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportPurchasingStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
   
}

- (void)setupPagemenu{
    
    self.studentViewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportStudentStoryboard"];
    self.studentViewcontroller.title = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_REPORT_STUDENT",nil,[Utils getLanguage],nil);
    self.studentViewcontroller.delegate = self;
    
    self.excutiveViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportExcutiveStoryboard"];
    self.excutiveViewController.title = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_EXECUTIVE_REPORT",nil,[Utils getLanguage],nil);
    self.excutiveViewController.delegate = self;

    self.personViewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportPersonStoryboard"];
    self.personViewcontroller.title = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_PERSONAL_REPORT",nil,[Utils getLanguage],nil);
    self.personViewcontroller.delegate = self;
    
    NSArray *controllerArray = @[self.studentViewcontroller, self.excutiveViewController, self.personViewcontroller];
    
    NSDictionary *parameters;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(40),
                       CAPSPageMenuOptionMenuHeight: @(70),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:36.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    else{
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(20),
                       CAPSPageMenuOptionMenuHeight: @(40),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:22.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    
    [self.contentView layoutIfNeeded];
    
    pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    // For the first time set first tab background to orange and label color to white
    NSArray *menuItems = pageMenu.menuItems;
    MenuItemView *menuItemView = [menuItems objectAtIndex:pageMenu.currentPageIndex];
    menuItemView.backgroundColor = OrangeColor;
    menuItemView.titleLabel.textColor = [UIColor whiteColor];
    
    //Optional delegate
    pageMenu.delegate = self;
    
    [self.contentView addSubview:pageMenu.view];
    
    
}

#pragma CAPSPageMenuDelegate

- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index{
    
    NSArray *menuItems = pageMenu.menuItems;
    int count = 0;
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = OrangeColor;
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        count++;
    }
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {

    NSArray *menuItems = pageMenu.menuItems;
    
    int count = 0;

    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = OrangeColor;
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }

        count++;

    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialog alloc] init];
    }
    else if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

//- (void)showFinishAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
//
//    if(alertDialog != nil && [alertDialog isDialogShowing]) {
//        [alertDialog dismissDialog];
//    }
//    else{
//
//        //        alertDialog = [[AlertDialog alloc] initWithNibName:@"AlertDialog" bundle:nil];
//
//        alertDialog = [[AlertDialog alloc] init];
//        alertDialog.delegate = self;
//    }
//
//    [alertDialog showDialogInView:self.view title:title message:message];
//
//}

//- (void)onAlertDialogClose {
//
//    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
//
//    [self.slideMenuController changeMainViewController:viewController close:YES];
//
//}



- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}
@end
