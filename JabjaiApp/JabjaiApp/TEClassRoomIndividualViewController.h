//
//  TEClassRoomIndividualViewController.h
//  JabjaiApp
//
//  Created by toffee on 10/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "TEStatusModel.h"
//#import "CallGetStudentInClassAPI.h"
#import "CallGetPreviousAttendClassStatusAPI.h"
#import "TAStudentStatusTableViewCell.h"
#import "CallGetStudentInClassroomAPI.h"
#import "BSSelectedStudent.h"
//#import "TAStatusDialog.h"
#import "TEStatusDialogViewController.h"
#import "CallTEGetStudentInClassAPI.h"
#import <CoreData/CoreData.h>

#import "SlideMenuController.h"

@interface TEClassRoomIndividualViewController : UIViewController< TAStudentStatusTableViewCellDelegate, UITableViewDataSource, UITableViewDelegate, CallTEGetStudentInClassAPIDelegate, CallGetPreviousAttendClassStatusAPIDelegate, TEStatusDialogViewControllerDelegate, UISearchBarDelegate, SlideMenuControllerDelegate>

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger mode;
@property (nonatomic) NSMutableArray* studentImageArray;

// TAStudentListViewController
@property (strong, nonatomic) NSArray<TAStudentStatusModel *> *studentStatusArray;
// BSSelectStudentsViewController
@property (strong, nonatomic) NSArray<TEStatusModel *> *selectedStudentArray;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UILabel *noStudents;
@property (weak, nonatomic) IBOutlet UILabel *headTakeEvent;
@property (weak, nonatomic) IBOutlet UILabel *noStudentLabel;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIView *headerView;



- (IBAction)back:(id)sender;
- (IBAction)actionSelectAll:(id)sender;
- (IBAction)actionNext:(id)sender;


@end
