//
//  CallGetPermissionAcademicResultsAPI.m
//  JabjaiApp
//
//  Created by toffee on 25/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallGetPermissionAcademicResultsAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallGetPermissionAcademicResultsAPI () {
    NSInteger connectCounter;
    BOOL permission;
}
@end
@implementation CallGetPermissionAcademicResultsAPI
-(void)call:(long long)schoolId studentId:(long long)studentId{
    connectCounter = 0;
    [self getPermission:schoolId studentId:studentId];
}
-(void)getPermission:(long long)schoolId studentId:(long long)studentId{
    NSString *URLString = [APIURL getStudentPermission:schoolId studentId:studentId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
           
           BOOL isFail = NO;
           
           if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
               if(error != nil) {
                   NSLog(@"An error occured : %@" , [error localizedDescription]);
               }
               else if(data == nil) {
                   NSLog(@"Data is null");
               }
               else {
                   NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                   NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
               }
               
               if(connectCounter < TRY_CONNECT_MAX) {
                   connectCounter++;
                   
                   NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                   [self getPermission:schoolId studentId:studentId];
;
               }
               else {
                   
                   isFail = YES;
                   connectCounter = 0;
               }

           } else {
               NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
               permission = [responseString boolValue];
               NSLog(@"xx");
               if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetPermissionAcademicResultsAPI:data:success:)]) {
                                      [self.delegate callGetPermissionAcademicResultsAPI:self data:permission success:YES];
                }
           }
           if(isFail) {
               if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetPermissionAcademicResultsAPI:data:success:)]) {
                   [self.delegate callGetPermissionAcademicResultsAPI:self data:permission success:NO];
               }
           }
       }];
}
@end
