//
//  PasscodeViewXIB.h
//  JabjaiApp
//
//  Created by toffee on 6/7/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallAddPinAPI.h"
#import "CallConfirmPinAPI.h"
#import "MGListMargetViewController.h"
#import "MGPasscodeViewController.h"
#import "SlideMenuController.h"



@class PasscodeViewXIB;
@protocol PasscodeViewXIBDelegate <NSObject>

- (void)passcodeViewXIB:(PasscodeViewXIB *)classObj statuss:(NSString*)statuss successs:(BOOL)successs;
@end

@interface PasscodeViewXIB : UIView <CallAddPinAPIDelegate, CallConfirmPinAPIDelegate, SlideMenuControllerDelegate, MGPasscodeViewControllerDelegate >

@property (nonatomic, weak) id<PasscodeViewXIBDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *headerPinLabel;
@property (strong, nonatomic) IBOutlet UILabel *confirmPinUnfair;



@property (weak, nonatomic) IBOutlet UIView *passView1;
@property (weak, nonatomic) IBOutlet UIView *passView2;
@property (weak, nonatomic) IBOutlet UIView *passView3;
@property (weak, nonatomic) IBOutlet UIView *passView4;
@property (weak, nonatomic) IBOutlet UIView *passView5;
@property (weak, nonatomic) IBOutlet UIView *passView6;

@property (weak, nonatomic) IBOutlet UIButton *numberButton;

//- (void)showDialogInView:(UIView *)targetView data:(NSDictionary *)data;

- (IBAction)actionButtonNumber:(UIControl*)sender;


@end
