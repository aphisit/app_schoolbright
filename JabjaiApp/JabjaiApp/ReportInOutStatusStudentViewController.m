//
//  ReportInOutStatusStudentViewController.m
//  JabjaiApp
//
//  Created by toffee on 27/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutStatusStudentViewController.h"
#import "UserData.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Utils.h"
@interface ReportInOutStatusStudentViewController (){
    
    NSInteger classroomClickInterger;
    NSInteger statusClickInterger;
    
    NSArray<ReportInOutStatusClassroomModel*> *dataClassroomArray;
    NSArray<ReportInOutStatusStudentModel*> *studentStatusAllArray;
    
    NSMutableArray<ReportInOutStatusStudentModel *>  *studentStatusOnTimeArray, *studentStatusLateArray, *studentStatusAbsenArray, *studentStatusSickArray, *studentStatusPersonalArray, *studentStatusEventArray,*studentStatusUndefinedArray, *studentArray;
    
}
@property (strong, nonatomic) CallReportInOutStatusStudentAPI *callReportInOutStatusStudentAPI;

@end
static NSString *statusIdentifier = @"StatusCell";
static NSString *classroomIdentifier = @"ClassroomCell";
static NSString *cellIdentifier = @"Cell";

@implementation ReportInOutStatusStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setLanguage];
    classroomClickInterger = 0;
    statusClickInterger = 0;
    [self.classroomCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportInOutSelectClassRoomCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:classroomIdentifier];
    self.classroomCollectionView.delegate = self;
    self.classroomCollectionView.dataSource = self;
    
    [self.statusCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportInOutStatusStudentCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:statusIdentifier];
    self.statusCollectionView.delegate = self;
    self.statusCollectionView.dataSource = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportInOutStatusStudentTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self doCallReportInOutStatusAPI:[UserData getSchoolId] dayReports:[Utils datePunctuateStringFormat:self.dayReports] levelId:self.levelId];
    
}

-(void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ATTENDCLASS",nil,[Utils getLanguage],nil);
    self.headerNoStudentLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_NO_STUDENT",nil,[Utils getLanguage],nil);
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    
    
}

- (void) doCallReportInOutStatusAPI :(long long) schoolId dayReports:(NSString*)dayReport levelId:(long long)levelId{
    [self showIndicator];
    if(self.callReportInOutStatusStudentAPI != nil) {
        self.callReportInOutStatusStudentAPI = nil;
    }
    self.callReportInOutStatusStudentAPI = [[CallReportInOutStatusStudentAPI alloc] init];
    self.callReportInOutStatusStudentAPI.delegate = self;
    [self.callReportInOutStatusStudentAPI call:schoolId dayReports:dayReport levelId:levelId];
    
}

- (void) callReportInOutStatusStudentAPI:(CallReportInOutStatusStudentAPI *)classObj dataClassroom:(NSMutableArray<ReportInOutStatusClassroomModel *> *)dataClassroom success:(BOOL)success{
    if (success && dataClassroom.count >0) {
        [self stopIndicator];
        dataClassroomArray = dataClassroom;
        ReportInOutStatusClassroomModel *model = [[ReportInOutStatusClassroomModel alloc] init];
       
        model = [dataClassroomArray objectAtIndex:0]; //get room frist
        self.classRoomLable.text = [model getSubLevelName];
        self.amountLabel.text = [NSString stringWithFormat:@"%@ %d คน",NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_AMOUNT_STUTENTALL",nil,[Utils getLanguage],nil),[model getLogData].count];
        self.dateLabel.text = [Utils getThaiDateFormatWithDate:self.dayReports];
        [self doSeparateStatusStudent:model];
        
        
        
    }
    
    
}

- (void) doSeparateStatusStudent:(ReportInOutStatusClassroomModel*)model{
    ReportInOutStatusStudentModel *studentModel = [[ReportInOutStatusStudentModel alloc] init];
    studentStatusAllArray = [model getLogData];

    studentArray = [[NSMutableArray alloc] init];
    studentStatusOnTimeArray = [[NSMutableArray alloc] init];
    studentStatusLateArray = [[NSMutableArray alloc] init];
    studentStatusAbsenArray = [[NSMutableArray alloc] init];
    studentStatusPersonalArray = [[NSMutableArray alloc] init];
    studentStatusSickArray = [[NSMutableArray alloc] init];
    studentStatusEventArray = [[NSMutableArray alloc] init];
    studentStatusUndefinedArray = [[NSMutableArray alloc] init];
    
    studentArray = [NSMutableArray arrayWithArray:studentStatusAllArray];
    
    for (int i = 0; i < studentStatusAllArray.count; i++) {
        studentModel = [studentStatusAllArray objectAtIndex:i];
        
        if ([[studentModel getStatus] integerValue] == 0) {
            [studentStatusOnTimeArray addObject:studentModel];
            
        }
        else if ([[studentModel getStatus] integerValue] == 1){
            [studentStatusLateArray addObject:studentModel];
            
        }
        else if ([[studentModel getStatus] integerValue] == 3){
            [studentStatusAbsenArray addObject:studentModel];
            
        }
        else if ([[studentModel getStatus] integerValue] == 10){
            [studentStatusPersonalArray addObject:studentModel];
            
        }
        else if ([[studentModel getStatus] integerValue] == 11){
            [studentStatusSickArray addObject:studentModel];
            
        }
        else if ([[studentModel getStatus] integerValue] == 12){
            [studentStatusEventArray addObject:studentModel];
            
        }else{
            [studentStatusUndefinedArray addObject:studentModel];
            
        }
    }
    [self.classroomCollectionView reloadData];
    [self.statusCollectionView reloadData];
    [self.tableView reloadData];
}



- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if(collectionView == self.classroomCollectionView){
        return dataClassroomArray.count;
    }else{
        return 8;
    }
}

- (UICollectionViewCell *) collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if(collectionView == self.classroomCollectionView){
         ReportInOutSelectClassRoomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:classroomIdentifier forIndexPath:indexPath];
        ReportInOutStatusClassroomModel *model = [[ReportInOutStatusClassroomModel alloc] init];
        
        [cell setBackgroundClick:indexPath.row numberClick:classroomClickInterger];
        
        model = [dataClassroomArray objectAtIndex:indexPath.row];
        cell.classroomLabel.text = [model getSubLevelName];
        return cell;
    }
    else{
        ReportInOutStatusStudentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:statusIdentifier forIndexPath:indexPath];
      
        if (indexPath.row == 0) {
            cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ALL",nil,[Utils getLanguage],nil);
            cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",studentStatusAllArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        }
        else if (indexPath.row == 1){
            cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ONTIME",nil,[Utils getLanguage],nil);
            cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",studentStatusOnTimeArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        }
        else if (indexPath.row == 2){
            cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_LATE",nil,[Utils getLanguage],nil);
            cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",studentStatusLateArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        }
        else if (indexPath.row == 3){
            cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ABSENCE",nil,[Utils getLanguage],nil);
            cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",studentStatusAbsenArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        }
        else if (indexPath.row == 4){
            cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_SICK",nil,[Utils getLanguage],nil);
            cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",studentStatusSickArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        }
        else if (indexPath.row == 5){
            cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PERSONAL",nil,[Utils getLanguage],nil);
            cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",studentStatusPersonalArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        }
        else if (indexPath.row == 6){
            cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_EVENT",nil,[Utils getLanguage],nil);
            cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",studentStatusEventArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        }
        else if (indexPath.row == 7){
            cell.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_UNDEFINED",nil,[Utils getLanguage],nil);
            cell.amountLabel.text = [NSString stringWithFormat:@"%d %@",studentStatusUndefinedArray.count,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        }
        [cell setBackgroundClick:indexPath.row numberClick:statusClickInterger];
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    if(collectionView == self.classroomCollectionView){
        statusClickInterger = 0;
        classroomClickInterger = indexPath.row;
        ReportInOutStatusClassroomModel *model = [[ReportInOutStatusClassroomModel alloc] init];
        model = [dataClassroomArray objectAtIndex:classroomClickInterger];
        self.classRoomLable.text = [model getSubLevelName];
        self.amountLabel.text = [NSString stringWithFormat:@"%@ %d",NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_AMOUNT_STUTENTALL",nil,[Utils getLanguage],nil),[model getLogData].count];
        [self doSeparateStatusStudent:model];
    }
    else{
        statusClickInterger = indexPath.row;
        if (statusClickInterger == 0) {
            studentArray = [NSMutableArray arrayWithArray: studentStatusAllArray ];
        }else if (statusClickInterger == 1){
            studentArray = studentStatusOnTimeArray;
        }else if (statusClickInterger == 2){
            studentArray = studentStatusLateArray;
        }else if (statusClickInterger == 3){
            studentArray = studentStatusAbsenArray;
        }else if (statusClickInterger == 4){
            studentArray = studentStatusSickArray;
        }else if (statusClickInterger == 5){
            studentArray = studentStatusPersonalArray;
        }else if (statusClickInterger == 6){
            studentArray = studentStatusEventArray;
        }else if (statusClickInterger == 7){
            studentArray = studentStatusUndefinedArray;
        }
        
        [self.statusCollectionView reloadData];
        [self.tableView reloadData];
    }

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize defaultSizee;
    if (collectionView == self.classroomCollectionView) {
          defaultSizee = CGSizeMake((self.classroomCollectionView.frame.size.height+35), (self.classroomCollectionView.frame.size.height));
    }else if (collectionView == self.statusCollectionView){
         defaultSizee = CGSizeMake((self.statusCollectionView.frame.size.height+15), (self.statusCollectionView.frame.size.height));
    }
    return defaultSizee;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (studentArray.count == 0) {
        self.showNoStudent.hidden = NO;
    }else{
        self.showNoStudent.hidden = YES;
    }
    return studentArray.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ReportInOutStatusStudentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    ReportInOutStatusStudentModel *model  = [studentArray objectAtIndex:indexPath.row];
    cell.nameStudentLabel.text = [model getNameStudent];
    cell.runNumberLabel.text = [NSString stringWithFormat:@"%d.",indexPath.row+1];
    //[cell updateStatus:[[model getStatus]intValue]];
    
    if ([model getPictuer] == NULL || [[model getPictuer] isEqual:@""]) {
        cell.picStudentImage.image = [UIImage imageNamed:@"ic_user_info"];
    }
    else{
        [cell.picStudentImage sd_setImageWithURL:[NSURL URLWithString:[model getPictuer]]];
    }
    
    
    if ([model getStudentState] == 0) {
        [cell updateStatus:[[model getStatus]intValue]];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.0];
        cell.userInteractionEnabled = YES;
    }
    else if ([model getStudentState] == 1){
        [cell updateStatus:21];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    else if ([model getStudentState] == 2 ){//Resing
        [cell updateStatus:22];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    else if ([model getStudentState] == 3 ){//Suspended
        [cell updateStatus:23];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    else if ([model getStudentState] == 4 ){//Graduate
        [cell updateStatus:24];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    else if ([model getStudentState] == 5 ){//Lost contract
        [cell updateStatus:25];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    else if ([model getStudentState] == 6 ){//Retire
        [cell updateStatus:26];
        cell.backgroundColorView.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
        cell.userInteractionEnabled = NO;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)moveBack:(id)sender {
    ReportInOutAllDataSelectLevelViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportInOutAllDataSelectLevelStoryboard"];
    viewController.date = self.dayReports;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
