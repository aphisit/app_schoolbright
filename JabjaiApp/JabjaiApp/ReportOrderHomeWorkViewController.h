//
//  ReportOrderHomeWorkViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "HWTDetailOrderHomeworkViewControllerXIB.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface ReportOrderHomeWorkViewController : UIViewController <SlideMenuControllerDelegate, UITableViewDelegate, UITableViewDataSource, CallCRGetStatusClosedForRenovationAPIDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;


- (IBAction)moveBack:(id)sender;

@end
