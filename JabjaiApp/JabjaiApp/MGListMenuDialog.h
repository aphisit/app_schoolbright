//
//  MGListMenuDialog.h
//  JabjaiApp
//
//  Created by toffee on 5/17/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol  MGListMenuDialogDelegate
@end
@interface MGListMenuDialog : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) id<MGListMenuDialogDelegate> delegate;
@property (nonatomic, strong) NSArray *menuArr;
@property (nonatomic, strong) NSArray *priceArr;
@property (nonatomic, strong) NSArray *amountArr;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIView *content;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

- (id)initWithRequestCode:(NSString *)requestCode;
- (void)showDialogInView:(UIView *)targetView listMenu:(NSArray *)listMenu lisiPrice:(NSArray *)listPrice listAmount:(NSArray *)listAmount;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
- (IBAction)actionNext:(id)sender;

@end
