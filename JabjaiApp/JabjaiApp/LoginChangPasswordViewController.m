//
//  LoginChangPasswordViewController.m
//  JabjaiApp
//
//  Created by toffee on 8/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "LoginChangPasswordViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "UserInfoDBHelper.h"
#import "Utils.h"

@interface LoginChangPasswordViewController (){
     AlertDialog *alertDialog;
    AlertDialogConfirm *alertDialogConfirm;
    
    NSMutableArray<MultipleUserModel *> *userArray;
    MultipleUserModel *userModel;
}
@property (nonatomic, strong) CallChangPasswordLoginAPI *callChangPasswordLoginAPI;
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;

@end

@implementation LoginChangPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.passwordNewLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CPW_NEW_PASSWORD",nil,[Utils getLanguage],nil);
    self.passwordNewTextfield.placeholder = NSLocalizedStringFromTableInBundle(@"LABEL_CPW_NEW_PASSWORD",nil,[Utils getLanguage],nil);
    self.confirmPasswordLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CPW_CONFIRM_PASSWORD",nil,[Utils getLanguage],nil);
    self.confirmPasswordTextfield.placeholder = NSLocalizedStringFromTableInBundle(@"LABEL_CPW_CONFIRM_PASSWORD",nil,[Utils getLanguage],nil);
    [self.confirmBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_CPW_CONFIRM",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    [self.cancelBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_CPW_CANCEL",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    
    self.passwordNewTextfield.delegate = self;
    self.confirmPasswordTextfield.delegate = self;
    self.dbHelper = [[UserInfoDBHelper alloc] init];

    [self hidekeyboard]; // hide keyboard
    // Do any additional setup after loading the view.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
CAGradientLayer *gradienConfirm,*gradientCancel;
//set color header
[self.confirmBtn layoutIfNeeded];
gradienConfirm = [Utils getGradientColorStatus:@"green"];
gradienConfirm.frame = self.confirmBtn.bounds;
[self.confirmBtn.layer insertSublayer:gradienConfirm atIndex:0];

//set color button next
[self.cancelBtn layoutIfNeeded];
gradientCancel = [Utils getGradientColorStatus:@"red"];
gradientCancel.frame = self.cancelBtn.bounds;
[self.cancelBtn.layer insertSublayer:gradientCancel atIndex:0];
    
}

- (void)hidekeyboard{
    UITapGestureRecognizer *hideKeyboardRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(switchDatePickerHidden)];
    [self.view addGestureRecognizer:hideKeyboardRecognizer];

    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(switchDatePickerHidden)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    [self.passwordNewTextfield setInputAccessoryView:toolBar];
    [self.confirmPasswordTextfield setInputAccessoryView:toolBar];
}

- (void)switchDatePickerHidden
{
    [self.passwordNewTextfield resignFirstResponder];
    [self.confirmPasswordTextfield resignFirstResponder];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)confirmAction:(id)sender {
    NSString *passwordNew = self.passwordNewTextfield.text;
    NSString *confirmPassword = self.confirmPasswordTextfield.text;
    
    if(passwordNew.length == 0) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CPW_FILL_NEW_PASSWORD",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else if(confirmPassword.length == 0) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CPW_FILL_CONFIRM_PASSWORD",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else {
        if (passwordNew.length < 6) {
            NSString *alertTitle = @"แจ้งเตือน";
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CPW_SIX_CHAR",nil,[Utils getLanguage],nil);
            [self showAlertDialogWithTitle:alertTitle message:alertMessage];
        }else if(confirmPassword.length < 6){
            NSString *alertTitle = @"แจ้งเตือน";
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CPW_SIX_CHAR",nil,[Utils getLanguage],nil);
            [self showAlertDialogWithTitle:alertTitle message:alertMessage];
        }else{
            if ([passwordNew isEqualToString:confirmPassword]) {
                [self callUpdatePassword:confirmPassword userId:[UserData getUserID] clientToken:[UserData getClientToken] schoolid:[UserData getSchoolId]];
            }
            else{
                NSString *alertTitle = @"แจ้งเตือน";
                NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CPW_PASS_NOT_CORRECT",nil,[Utils getLanguage],nil);
                [self showAlertDialogWithTitle:alertTitle message:alertMessage];
            }
        }
        
       
    }
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
   
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
     alertDialog = [[AlertDialog alloc] init];
    
    [alertDialog showDialogInView:self.view title:title message:message];
}



- (void)showAlertConfirmDialogWithTitle:(NSString *)title message:(NSString *)message{
    
    if (alertDialogConfirm != nil && [alertDialogConfirm isDialogShowing]) {
        [alertDialogConfirm dismissDialog];
    }
    
    alertDialogConfirm = [[AlertDialogConfirm alloc] init];
    alertDialogConfirm.delegate = self;
    [alertDialogConfirm showDialogInView:self.view title:title message:message];
    
}

-(void)onAlertDialogClose{
    
    if ([UserData getMasterUserID] != [UserData getUserID]) {
        [self updateUserList];
        //userModel = [userArray objectAtIndex:userArray.count-1];
        [UserData saveUserID:[userModel getSlaveId]];
        [UserData setUserLogin:YES];
        [UserData saveSchoolId:[userModel getSchoolId]];
        [UserData setFirstName:[userModel getFirstName]];
        [UserData setLastName:[userModel getLastName]];
        [UserData setUserImage:[userModel getImageUrl]];
        [UserData setClientToken:[userModel getClientToken]];
        
        if([userModel getAcademyType] == 1) {
            [UserData setAcademyType:SCHOOL];
        }
        else {
            [UserData setAcademyType:UNIVERSITY];
        }
        
        if([userModel getUserType] == 0) {
            [UserData setUserType:STUDENT];
        }
        else {
            [UserData setUserType:TEACHER];
        }
        
        if([userModel getGender] == 0) {
            [UserData setGender:MALE];
        }
        else {
            [UserData setGender:FEMALE];
        }
        
        UserInfoViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
        
    }else{
        [UserData setUserLogin:NO];
        [UserData saveMasterUserID:0];
        [UserData saveUserID:0];
        [UserData setUserType:USERTYPENULL];
        [UserData setAcademyType:ACADEMYTYPENULL];
        [UserData setFirstName:@""];
        [UserData setLastName:@""];
        [UserData setUserImage:@""];
        [UserData setGender:MALE];
        [UserData setClientToken:@""];
        
        LoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    
    
}


- (void) callUpdatePassword :(NSString *)newPassword  userId:(long long)userId clientToken:(NSString *)clientToken schoolid:(long long)schoolid{ //Authorize Menu For User
    [self showIndicator];
    self.callChangPasswordLoginAPI = nil;
    self.callChangPasswordLoginAPI = [[ CallChangPasswordLoginAPI alloc] init];
    self.callChangPasswordLoginAPI.delegate = self;
    [self.callChangPasswordLoginAPI call:newPassword userId:userId token:clientToken schoolid:schoolid];
}
-(void) callChangPasswordLoginAPI:(CallChangPasswordLoginAPI *)classObj resCode:(NSInteger)resCode newToken:(NSString *)newToken success:(BOOL)success{
    [self stopIndicator];
    if (success) {
        
        if (resCode == 200) {
            [self.dbHelper updateToken:[UserData getUserID] token:newToken];
            NSLog(@"xxxx");
            NSString *alertTitle = @"แจ้งเตือน";
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CPW_SUCCESS",nil,[Utils getLanguage],nil);
            [self showAlertConfirmDialogWithTitle:alertTitle message:alertMessage];
        }
        else if (resCode == 301){
            NSString *alertTitle = @"แจ้งเตือน";
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CPW_NOT_OLD_PASSWORD",nil,[Utils getLanguage],nil);
            [self showAlertDialogWithTitle:alertTitle message:alertMessage];
        }
        
        
      
    }
   
    

}
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (void)updateUserList {
    
    NSArray *users = [self.dbHelper getUserDataWithMasterId:[UserData getMasterUserID]];
    
    if(userArray != nil) {
        [userArray removeAllObjects];
        userArray = nil;
    }
    userArray = [[NSMutableArray alloc] init];
    
    if(users != nil) {
    
        for (int i = 0; i<users.count; i++) {
            MultipleUserModel *model = [[MultipleUserModel alloc] init];
            model = [users objectAtIndex:i];
            if (model.getSlaveId == [UserData getUserID]) {
                userModel = model;
                break;
            }
        }
    }
}


- (IBAction)cancelAction:(id)sender {
    UserInfoViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
@end
