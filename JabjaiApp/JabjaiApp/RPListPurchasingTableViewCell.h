//
//  RPListPurchasingTableViewCell.h
//  JabjaiApp
//
//  Created by Mac on 11/8/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RPListPurchasingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceTotalLabel;

@end

NS_ASSUME_NONNULL_END
