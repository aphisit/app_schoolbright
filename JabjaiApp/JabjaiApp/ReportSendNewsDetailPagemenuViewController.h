//
//  ReportSendNewsDetailPagemenuViewController.h
//  JabjaiApp
//
//  Created by mac on 4/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "CAPSPageMenu.h"

#import "ReportSendNewsDetailViewController.h"
#import "ReportSendNewsRecieverViewController.h"

@interface ReportSendNewsDetailPagemenuViewController : UIViewController <SlideMenuControllerDelegate, CAPSPageMenuDelegate>

@property (assign, nonatomic) long long newsID;
@property (nonatomic, strong) NSString *sortData;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;

@property (nonatomic, strong) ReportSendNewsDetailViewController *detailController;
@property (nonatomic, strong) ReportSendNewsRecieverViewController *recieverController;

- (IBAction)moveBack:(id)sender;

@end
