//
//  CallSNSendReplyCodeMessageBoxAPI.h
//  JabjaiApp
//
//  Created by Mac on 30/4/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNStatusSendReplyCodeModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallSNSendReplyCodeMessageBoxAPI;
@protocol CallSNSendReplyCodeMessageBoxAPIDelegate <NSObject>

- (void)callSNSendReplyCodeMessageBoxAPI:(CallSNSendReplyCodeMessageBoxAPI*)classObj data:(SNStatusSendReplyCodeModel*)data success:(BOOL)success;

@end
@interface CallSNSendReplyCodeMessageBoxAPI : NSObject
@property (nonatomic, weak) id<CallSNSendReplyCodeMessageBoxAPIDelegate>delegate;
- (void)call:(long long)userID messageID:(long long)messageID replyCode:(int)replyCode schoolID:(long long)schoolID;
@end

NS_ASSUME_NONNULL_END
