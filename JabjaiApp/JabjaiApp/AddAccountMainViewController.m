//
//  AddAccountMainViewController.m
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import AirshipKit;

#import "AddAccountMainViewController.h"
#import "SWRevealViewController.h"
#import "UserInfoDBHelper.h"
#import "MultipleUserModel.h"
#import "SettingViewController.h"
#import "AddAccountLoginViewController.h"
#import "UserData.h"
#import "Utils.h"

@interface AddAccountMainViewController () {
    NSMutableArray<MultipleUserModel *> *userArray;
    
    NSInteger selectedIndex;
    
    //The dialog
    ConfirmDialog *confirmDialog;
}

@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (nonatomic, strong) CallRemoveDataSystemNotificationPOSTAPI *callRemoveDataSystemNotificationPOSTAPI;
@end

static NSString *cellIdentifier = @"Cell";

@implementation AddAccountMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DelUserAccountTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    selectedIndex = -1;
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    [self updateUserList];
    [self setLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews{
    
    [self doDesignLayout];
}

- (void)setLanguage{
    self.headerManageAccountLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MANAGE_ACC",nil,[Utils getLanguage],nil);
    [self.addUserButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_MANAGE_ADD_ACC",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

- (void) doDesignLayout{
    CAGradientLayer  *gradientHeaderView,*gradientBtnLogin;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeaderView = [Utils getGradientColorHeader];
    gradientHeaderView.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeaderView atIndex:0];
    
    //set color button next
    [self.addUserButton layoutIfNeeded];
    gradientBtnLogin = [Utils getGradientColorNextAtion];
    gradientBtnLogin.frame = self.addUserButton.bounds;
    [self.addUserButton.layer insertSublayer:gradientBtnLogin atIndex:0];
}

#pragma mark - CallRemoveDataSystemNotificationPOSTAPI
- (void)callRemoveDataSystemNotification:(NSString*)jsonString{
    self.callRemoveDataSystemNotificationPOSTAPI = nil;
    self.callRemoveDataSystemNotificationPOSTAPI = [[CallRemoveDataSystemNotificationPOSTAPI alloc] init];
    self.callRemoveDataSystemNotificationPOSTAPI.delegate = self;
    [self.callRemoveDataSystemNotificationPOSTAPI call:jsonString];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(userArray != nil) {
        return userArray.count;
    }
    else {
        return 0;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DelUserAccountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    MultipleUserModel *model = [userArray objectAtIndex:indexPath.row];
    
    if([model getImageUrl] == nil || [[model getImageUrl] length] == 0) {
        
        if([model getGender] == 0) { // Male
            cell.imgUser.image = [UIImage imageNamed:@"ic_user_info"];
        }
        else { // Female
            cell.imgUser.image = [UIImage imageNamed:@"ic_user_women"];
        }
    }
    else {
        [cell.imgUser loadImageFromURL:[model getImageUrl]];
    }
    
    if([model getSlaveId] == [UserData getMasterUserID]) {
        cell.btnDel.hidden = YES;
    }
    else {
        cell.btnDel.hidden = NO;
    }
    
    NSString *name = [[NSString alloc] initWithFormat:@"%@ %@", [model getFirstName], [model getLastName]];
    cell.titleLabel.text = name;
    cell.index = indexPath.row;
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

#pragma mark - DelUserAccountTableViewCellDelegate

- (void)onDeleteUser:(DelUserAccountTableViewCell *)tableViewCell atIndex:(NSInteger)index {
    selectedIndex = index;
    [self showConfirmDialog];
}

#pragma mark - ConfirmDialogDelegate

- (void)onConfirmDialog:(ConfirmDialog *)confirmDialog confirm:(BOOL)confirm {
    if(confirm && selectedIndex >= 0) {
        MultipleUserModel *model = [userArray objectAtIndex:selectedIndex];
        
        [self.dbHelper deleteUserWithMasterId:[UserData getMasterUserID] slaveId:[model getSlaveId]];
        
        NSString *tag = [NSString stringWithFormat:@"%lld", [model getSlaveId]];
        [[UAirship push] removeTag:tag];
        [[UAirship push] updateRegistration];
        
        NSString *UDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];  //UDID Device
        [self callRemoveDataSystemNotification:[NSString stringWithFormat:@"{\"Token\":\"%@\",\"UserID\":%d,\"SchoolID\":%lld,\"Imei\":\"%@\",\"System\":\"IOS\"}",tokenStr,[UserData getUserID],[UserData getSchoolId],UDID]];
        
        [self updateUserList];
        [self.tableView reloadData];
    }
}

#pragma mark - Dialog

- (void)showConfirmDialog {
    
    
    if(confirmDialog != nil && [confirmDialog isDialogShowing]) {
        [confirmDialog dismissDialog];
    }
    else {
        confirmDialog = [[ConfirmDialog alloc] init];
        confirmDialog.delegate = self;
    }
    [confirmDialog showDialogInView:self.view title:@"แจ้งเตือน" message:NSLocalizedStringFromTableInBundle(@"DIALOG_MANAGE_ACC_DELETE_ACCOUNT",nil,[Utils getLanguage],nil)];
}

#pragma mark - Utility

- (void)updateUserList {
    NSArray *users = [self.dbHelper getUserDataWithMasterId:[UserData getMasterUserID]];
    
    if(userArray != nil) {
        [userArray removeAllObjects];
        userArray = nil;
    }
    userArray = [[NSMutableArray alloc] init];
    
    if(users != nil) {
        for(MultipleUserModel *model in users) {
            if([model getSlaveId] != [UserData getUserID]) {
                [userArray addObject:model];
            }
        }
    }

}

#pragma mark - Action functions

- (IBAction)moveBack:(id)sender {
    SettingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];

}

- (IBAction)actionAddAccount:(id)sender {
    
    AddAccountLoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAccountLoginStoryboard"];
    viewController.userArray = userArray;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
@end
