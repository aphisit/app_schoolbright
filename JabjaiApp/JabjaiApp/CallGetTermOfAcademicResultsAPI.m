//
//  CallGetTermOfAcademicResultsAPI.m
//  JabjaiApp
//
//  Created by toffee on 21/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallGetTermOfAcademicResultsAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallGetTermOfAcademicResultsAPI () {
    NSInteger connectCounter;
    NSArray *termArray;
    //NSMutableArray *yearArray;
}
@end
@implementation CallGetTermOfAcademicResultsAPI
- (void)call:(long long)schoolId nYear:(NSInteger)nYear studentId:(long long)studentId{
    connectCounter = 0;
    [self getTermOfAcademicResults:schoolId nYear:nYear studentId:studentId];
}
-(void)getTermOfAcademicResults:(long long)schoolId nYear:(NSInteger)nYear studentId:(long long)studentId{
    NSString *URLString = [APIURL getTermOfAcademicResults:schoolId nYear:nYear studentId:studentId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getTermOfAcademicResults:schoolId nYear:nYear studentId:studentId];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }

        } else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getTermOfAcademicResults:schoolId nYear:nYear studentId:studentId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                connectCounter = 0;
                termArray = returnedData;
                NSLog(@"xxx");
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTermOfAcademicResultsAPI:data:success:)]) {
                                   [self.delegate callGetTermOfAcademicResultsAPI:self data:termArray success:YES];
                }
            }
        }
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTermOfAcademicResultsAPI:data:success:)]) {
                [self.delegate callGetTermOfAcademicResultsAPI:self data:termArray success:NO];
            }
        }
    }];
}
@end
