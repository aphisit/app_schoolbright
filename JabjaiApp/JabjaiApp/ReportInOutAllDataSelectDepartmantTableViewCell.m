//
//  ReportInOutAllDataSelectDepartmantTableViewCell.m
//  JabjaiApp
//
//  Created by toffee on 15/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutAllDataSelectDepartmantTableViewCell.h"
#import "UserData.h"
#import "Utils.h"
#import "AppDelegate.h"

@interface ReportInOutAllDataSelectDepartmantTableViewCell (){
    NSArray *statusXaxisArray;
    NSMutableArray *statusYaxisArray;
    NSInteger statusOnTime,statusLate,statusAbsence,statusPersonal,statusSick,statusOther,statusHoliday,totalStatus;
    
    float onTimePercent, latePercent, absencePercent, personalPercent, sickPercent,otherPercent, holidayPercent;
    CAGradientLayer *gradient;
    UIColor *greenColor, *orangeColor, *redColor, *pinkColor, *purpleColor, *holidayColor, *otherColor;
    UIImage * backgroundColorImage;
}
@end

@implementation ReportInOutAllDataSelectDepartmantTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
   
    
    [self setLanguage];
    CGFloat max = MAX(self.onTimeColorView.frame.size.width, self.onTimeColorView.frame.size.height);
    
    gradient = [CAGradientLayer layer];
    gradient = [Utils getGradientColorStatus:@"green"];
    gradient.frame = self.onTimeColorView.bounds;
    UIGraphicsBeginImageContext(gradient.bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.onTimeColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
    self.onTimeColorView.layer.cornerRadius = max/2.0;
    self.onTimeColorView.layer.masksToBounds = YES;
    
    gradient = [Utils getGradientColorStatus:@"yellow"];
    gradient.frame = self.lateColorView.bounds;
    UIGraphicsBeginImageContext(gradient.bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.lateColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
    self.lateColorView.layer.cornerRadius = max/2.0;
    self.lateColorView.layer.masksToBounds = YES;
    
    gradient = [Utils getGradientColorStatus:@"red"];
    gradient.frame = self.absenceColorView.bounds;
    UIGraphicsBeginImageContext(gradient.bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.absenceColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
    self.absenceColorView.layer.cornerRadius = max/2.0;
    self.absenceColorView.layer.masksToBounds = YES;
    
    gradient = [Utils getGradientColorStatus:@"pink"];
    gradient.frame = self.sickColorView.bounds;
    UIGraphicsBeginImageContext(gradient.bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.sickColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
    self.sickColorView.layer.cornerRadius = max/2.0;
    self.sickColorView.layer.masksToBounds = YES;
    
    gradient = [Utils getGradientColorStatus:@"purple"];
    gradient.frame = self.personalColorView.bounds;
    UIGraphicsBeginImageContext(gradient.bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.personalColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
    self.personalColorView.layer.cornerRadius = max/2.0;
    self.personalColorView.layer.masksToBounds = YES;
    
   
    
    gradient = [Utils getGradientColorStatus:@"purplesoft"];
    gradient.frame = self.otherColorView.bounds;
    UIGraphicsBeginImageContext(gradient.bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.otherColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
    self.otherColorView.layer.cornerRadius = max/2.0;
    self.otherColorView.layer.masksToBounds = YES;
    
    gradient = [Utils getGradientColorStatus:@"orange"];
    gradient.frame = self.holidayColorView.bounds;
    UIGraphicsBeginImageContext(gradient.bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.holidayColorView setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
    self.holidayColorView.layer.cornerRadius = max/2.0;
    self.holidayColorView.layer.masksToBounds = YES;
}

- (void) setLanguage{
     self.headerOnTimeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ONTIME",nil,[Utils getLanguage],nil);
    self.headerLateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_LATE",nil,[Utils getLanguage],nil);
    self.headerAbsenceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ABSENCE",nil,[Utils getLanguage],nil);
    self.headerSickLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_SICK",nil,[Utils getLanguage],nil);
    self.headerPersanalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PERSONAL",nil,[Utils getLanguage],nil);
    self.headerOtherLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_OTHER",nil,[Utils getLanguage],nil);
    self.headerHolidayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_HOLIDAY",nil,[Utils getLanguage],nil);
    [self.lookDetailBtn setTitle:NSLocalizedStringFromTableInBundle(@"BNT_ATTEND_CLASS_REP_DETAIL",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setStatusGraph:(NSDictionary *)statusArray{
    //set color look detail
    [self.lookDetailBtn layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorStatus:@"green"];
    gradient.frame = self.lookDetailBtn.bounds;
    [self.lookDetailBtn.layer insertSublayer:gradient atIndex:0];
    statusYaxisArray = [[NSMutableArray alloc] init];
    statusOnTime = [[statusArray objectForKey:@"status_0"] integerValue];
    statusLate = [[statusArray objectForKey:@"status_1"] integerValue];
    statusAbsence = [[statusArray objectForKey:@"status_2"] integerValue];
    statusPersonal = [[statusArray objectForKey:@"status_3"] integerValue];
    statusSick = [[statusArray objectForKey:@"status_4"] integerValue];
    statusOther = [[statusArray objectForKey:@"status_5"] integerValue];
    statusHoliday = [[statusArray objectForKey:@"status_6"] integerValue];
    totalStatus = statusOnTime + statusLate + statusAbsence + statusPersonal + statusSick + statusOther + statusHoliday;
    
    onTimePercent = (float)statusOnTime/totalStatus * 100;
    latePercent = (float)statusLate/totalStatus * 100;
    absencePercent = (float)statusAbsence/totalStatus * 100;
    personalPercent = (float)statusPersonal/totalStatus * 100;
    sickPercent = (float)statusSick/totalStatus * 100;
    otherPercent = (float)statusOther/totalStatus * 100;
    holidayPercent = (float)statusHoliday/totalStatus * 100;
    
    [statusYaxisArray addObject:[NSString stringWithFormat:@"%d",statusOnTime]];
    [statusYaxisArray addObject:[NSString stringWithFormat:@"%d",statusLate]];
    [statusYaxisArray addObject:[NSString stringWithFormat:@"%d",statusAbsence]];
    [statusYaxisArray addObject:[NSString stringWithFormat:@"%d",statusSick]];
    [statusYaxisArray addObject:[NSString stringWithFormat:@"%d",statusPersonal]];
    [statusYaxisArray addObject:[NSString stringWithFormat:@"%d",statusOther]];
    [statusYaxisArray addObject:[NSString stringWithFormat:@"%d",statusHoliday]];
    NSLog(@"xxxx");
    statusXaxisArray = [NSArray arrayWithObjects: NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ONTIME",nil,[Utils getLanguage],nil),
                        NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_LATE",nil,[Utils getLanguage],nil),
                        NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_ABSENCE",nil,[Utils getLanguage],nil),
                        NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_SICK",nil,[Utils getLanguage],nil),
                        NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PERSONAL",nil,[Utils getLanguage],nil),
                        NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_OTHER",nil,[Utils getLanguage],nil),
                        NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_HOLIDAY",nil,[Utils getLanguage],nil),
                        nil];
    [self setBarChar];
    [self setGraphBarChart:statusXaxisArray values:statusYaxisArray];
    
    self.amountOnTimeLabel.text = [NSString stringWithFormat:@"%d %@",statusOnTime,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    self.amountLateLabel.text = [NSString stringWithFormat:@"%d %@",statusLate,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    self.amountAbsenceLabel.text = [NSString stringWithFormat:@"%d %@",statusAbsence,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    self.amountSickLabel.text = [NSString stringWithFormat:@"%d %@",statusSick,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    self.amountPersonalLabel.text = [NSString stringWithFormat:@"%d %@",statusPersonal,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    self.amountOtherLabel.text = [NSString stringWithFormat:@"%d %@",statusOther,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
    self.amountHolidayLabel.text = [NSString stringWithFormat:@"%d %@",statusHoliday,NSLocalizedStringFromTableInBundle(@"LABEL_ATTEND_CLASS_REP_PEOPLE",nil,[Utils getLanguage],nil)];
   
    if (!isnan(onTimePercent)) {
        self.onTimePercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",onTimePercent];
    }else{
        self.onTimePercentLabel.text = [NSString stringWithFormat:@"(0.0%%)"];
    }
    
    if (!isnan(latePercent)) {
        self.latePercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",latePercent];
        
    }else{
        self.latePercentLabel.text = [NSString stringWithFormat:@"(0.0%%)"];
    }
    
    if (!isnan(absencePercent)) {
        self.absencePercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",absencePercent];
    }else{
        self.absencePercentLabel.text = [NSString stringWithFormat:@"(0.0%%)"];
    }
    
    if (!isnan(sickPercent)) {
        self.sickPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",sickPercent];
    }else{
        self.sickPercentLabel.text = [NSString stringWithFormat:@"(0.0%%)"];
    }
    
    if (!isnan(personalPercent)) {
        self.personalPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",personalPercent];
    }else{
        self.personalPercentLabel.text = [NSString stringWithFormat:@"(0.0%%)"];
    }
    if (!isnan(otherPercent)) {
        self.otherPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",otherPercent];
    }else{
        self.otherPercentLabel.text = [NSString stringWithFormat:@"(0.0%%)"];
    }
    
    if (!isnan(holidayPercent)) {
        self.holidayPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)",holidayPercent];
    }else{
        self.holidayPercentLabel.text = [NSString stringWithFormat:@"(0.0%%)"];
    }
    
    
}

- (void) setBarChar{
    
    _barCharView.delegate = self;
    _barCharView.drawBarShadowEnabled = NO;
    _barCharView.drawValueAboveBarEnabled = YES;
    _barCharView.maxVisibleCount = 60;
    _barCharView.xAxis.drawGridLinesEnabled = NO;
    _barCharView.leftAxis.drawLabelsEnabled = NO;
    _barCharView.legend.enabled = NO;
    _barCharView.chartDescription.text = @"";
    
    _barCharView.leftAxis.drawAxisLineEnabled = false;
    _barCharView.leftAxis.drawGridLinesEnabled = false;
    _barCharView.leftAxis.gridColor = [UIColor clearColor];
    _barCharView.xAxis.drawGridLinesEnabled = false;
    _barCharView.backgroundColor = [UIColor whiteColor];
    _barCharView.translatesAutoresizingMaskIntoConstraints = false;
    [_barCharView animateWithYAxisDuration:3.0];
    
    ChartXAxis *xAxis = _barCharView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.granularity = 1.0; // only intervals of 1 day
    xAxis.valueFormatter = self;
    //xAxis.labelRotationAngle = 270.0f;
    
    NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
    
    ChartYAxis *leftAxis = _barCharView.leftAxis;
    leftAxis.labelFont = [UIFont systemFontOfSize:0.f];
    leftAxis.labelCount = 8;
    leftAxis.valueFormatter = [[ChartDefaultAxisValueFormatter alloc] initWithFormatter:leftAxisFormatter];
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.spaceTop = 0.15;
    leftAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
    leftAxis.drawAxisLineEnabled = NO;
    
    ChartYAxis *rightAxis = _barCharView.rightAxis;
    rightAxis.enabled = YES;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.labelFont = [UIFont systemFontOfSize:0.f];
    rightAxis.labelCount = 8;
    rightAxis.valueFormatter = leftAxis.valueFormatter;
    rightAxis.spaceTop = 0.15;
    rightAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
    rightAxis.drawAxisLineEnabled = NO;
    
}

-(void)setGraphBarChart:(NSArray*)dataPoints values:(NSArray*)values{
    self.barCharView.noDataText = @"No Data Available kingly add some";
    NSMutableArray *dataEntries = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<dataPoints.count; i++) {
        double x1 = [values[i]doubleValue];
        [dataEntries addObject:[[BarChartDataEntry alloc] initWithX:i y:x1]];
        
        BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithEntries:dataEntries];
        BarChartData *chartData = [[BarChartData alloc] initWithDataSet:set1];
        self.barCharView.data = chartData;
        [set1 setColor:[UIColor colorWithRed:0.16 green:0.81 blue:0.65 alpha:1.0]];
    }
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithEntries:dataEntries];
    BarChartData *chartData = [[BarChartData alloc] initWithDataSet:set1];
    self.barCharView.data = chartData;
    //[set1 setColor:[UIColor colorWithRed:0.16 green:0.81 blue:0.65 alpha:1.0]];
    greenColor = [UIColor colorWithRed:0.09 green:0.72 blue:0.12 alpha:1.0];
    orangeColor = [UIColor colorWithRed:0.92 green:0.58 blue:0.16 alpha:1.0];
    redColor = [UIColor colorWithRed:0.92 green:0.16 blue:0.16 alpha:1.0];
    pinkColor = [UIColor colorWithRed:0.88 green:0.14 blue:0.94 alpha:1.0];
    purpleColor = [UIColor colorWithRed:0.58 green:0.12 blue:0.86 alpha:1.0];
    otherColor = [UIColor colorWithRed:0.16 green:0.14 blue:0.94 alpha:1.0];
    holidayColor = [UIColor colorWithRed:0.81 green:0.57 blue:0.00 alpha:1.0];
    
    [set1 setColors:@[greenColor,orangeColor,redColor,pinkColor,purpleColor,otherColor,holidayColor]];
    // [self.barCharView animateWithXAxisDuration:2.0 easingOption:ChartEasingOptionEaseInBounce];
    
}

- (CAGradientLayer*)setColorGradient:(NSString*)nameColor
{
    gradient = [Utils getGradientColorStatus:nameColor];
    
    return gradient;
}
#pragma mark - IAxisValueFormatter
- (NSString * _Nonnull)stringForValue:(double)value axis:(ChartAxisBase * _Nullable)axis
{
    NSString *xAxisStringValue = @"";
    int myInt = (int)value;
    if(statusXaxisArray.count > myInt)
        xAxisStringValue = [statusXaxisArray objectAtIndex:myInt];
    
    return xAxisStringValue;
}


#pragma mark - ChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight
{
    NSLog(@"chartValueSelected");
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
    NSLog(@"chartValueNothingSelected");
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)showDetailAction:(id)sender {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ReportInOutStatusDepartmantViewController *viewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"ReportInOutStatusDepartmantStoryboard"];
        viewController.departmentId = self.departmentId;
        viewController.dayReports = self.dayReport;
        viewController.departmentName = self.departmentName;
        NavigationViewController *leftViewController = (NavigationViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"NavigationStoryboard"];
        SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:viewController leftMenuViewController:leftViewController];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [[appDelegate window] setRootViewController:slideMenuController];
}
@end
