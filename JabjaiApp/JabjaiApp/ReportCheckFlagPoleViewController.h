//
//  ReportCheckFlagPoleViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TAStudentStatusModel.h"
#import "SchoolClassroomModel.h"
#import "SchoolLevelModel.h"
#import "TableListDialog.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "SWRevealViewController.h"
#import "TAStudentStatusModel.h"
#import "TAStudentStatusTableViewCell.h"
#import "AlertDialog.h"
#import "SlideMenuController.h"

@interface ReportCheckFlagPoleViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate,TAStudentStatusTableViewCellDelegate, SlideMenuControllerDelegate,AlertDialogDelegate >{}
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (strong, nonatomic) NSArray<TAStudentStatusModel *> *selectedStudentArray;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLabel;


@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classRoomTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIView *section2;

@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;

- (IBAction)actionNext:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
