//
//  CallGetReduceBehaviorScoreListAPI.h
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BehaviorScoreModel.h"

@class CallGetReduceBehaviorScoreListAPI;

@protocol CallGetReduceBehaviorScoreListAPIDelegate <NSObject>

- (void)callGetReduceBehaviorScoreListAPI:(CallGetReduceBehaviorScoreListAPI *)classObj data:(NSArray<BehaviorScoreModel *> *)data success:(BOOL)success;

@end

@interface CallGetReduceBehaviorScoreListAPI : NSObject

@property (nonatomic, weak) id<CallGetReduceBehaviorScoreListAPIDelegate> delegate;

- (void)call:(long long)schoolId;

@end
