//
//  CallGetBSHistoryInDateRangeAPI.m
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetBSHistoryInDateRangeAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetBSHistoryInDateRangeAPI () {
    NSInteger connectCounter;
    NSMutableArray<BSHistoryModel *> *bsHistoryModelArray;
}

@end

@implementation CallGetBSHistoryInDateRangeAPI

- (id)init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(long long)schoolId classroomId:(long long)classroomId startDate:(NSDate *)startDate endDate:(NSDate *)endDate type:(NSInteger)type {
    
    [self getBSHistoryData:schoolId classroomId:classroomId startDate:startDate endDate:endDate type:type];
}

#pragma mark - Get API Data

- (void)getBSHistoryData:(long long)schoolId classroomId:(long long)classroomId startDate:(NSDate *)startDate endDate:(NSDate *)endDate type:(NSInteger)type {
    
    NSString *URLString = [APIURL getBehaviorScoreHistoryInDateRangeWithSchoolId:schoolId classroomId:classroomId startDate:startDate endDate:endDate type:type];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getBSHistoryData:schoolId classroomId:classroomId startDate:startDate endDate:endDate type:type];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getBSHistoryData:schoolId classroomId:classroomId startDate:startDate endDate:endDate type:type];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getBSHistoryData:schoolId classroomId:classroomId startDate:startDate endDate:endDate type:type];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(bsHistoryModelArray != nil) {
                    [bsHistoryModelArray removeAllObjects];
                    bsHistoryModelArray = nil;
                }
                
                bsHistoryModelArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *behaviorName, *studentName, *recorder, *remark;
                    
                    if(![[dataDict objectForKey:@"behaviorsname"] isKindOfClass:[NSNull class]]) {
                        behaviorName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"behaviorsname"]];
                    }
                    else {
                        behaviorName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"studentname"] isKindOfClass:[NSNull class]]) {
                        studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentname"]];
                    }
                    else {
                        studentName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"theachername"] isKindOfClass:[NSNull class]]) {
                        recorder = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"theachername"]];
                    }
                    else {
                        recorder = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"note"] isKindOfClass:[NSNull class]]) {
                        remark = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"note"]];
                    }
                    else {
                        remark = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) behaviorName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) recorder);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) remark);
                    
                    NSInteger type = [[dataDict objectForKey:@"behaviorstype"] integerValue];
                    NSInteger score = [[dataDict objectForKey:@"score"] integerValue];
                    NSString *dateStr = [dataDict objectForKey:@"day"];
                    NSDate *dateTime = [Utils parseServerDateStringToDate:dateStr];
                    
                    BSHistoryModel *model = [[BSHistoryModel alloc] init];
                    [model setBehaviorType:type];
                    [model setBehaviorName:behaviorName];
                    [model setBehaviorScore:score];
                    [model setDateTime:dateTime];
                    [model setStudentName:studentName];
                    [model setRecorder:recorder];
                    [model setRemark:remark];
                    
                    [bsHistoryModelArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetBSHistoryInDateRangeAPI:data:success:)]) {
                    [self.delegate callGetBSHistoryInDateRangeAPI:self data:bsHistoryModelArray success:YES];
                }
                
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetBSHistoryInDateRangeAPI:data:success:)]) {
                [self.delegate callGetBSHistoryInDateRangeAPI:self data:nil success:NO];
            }
        }

    }];
}

@end
