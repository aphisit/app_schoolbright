//
//  SignupPage4ViewController.h
//  JabjaiApp
//
//  Created by mac on 10/28/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupDataModel.h"
#import "TableListDialog.h"
#import "CaptureScreenCodeDialog.h"

@interface SignupPage4ViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, CaptureScreenCodeDialogDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic,strong) SignupDataModel *signupDataModel;
@property (nonatomic, strong) NSString *previousSegue;

@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *provinceTextField;
@property (weak, nonatomic) IBOutlet UITextField *districtTextField;
@property (weak, nonatomic) IBOutlet UITextField *subDistrictTextField;
@property (weak, nonatomic) IBOutlet UIButton *signupBtn;

- (IBAction)moveBack:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)signupAction:(id)sender;

@end
