//
//  TAReportHistoryStudentStatusViewController.m
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TAReportHistoryStudentStatusViewController.h"
#import "TAReportARSPContainerController.h"
#import "TAReportStudentStatusTableViewCell.h"
#import "Utils.h"
#import "DateUtility.h"
#import "UserData.h"
#import "TAReportStudentStatusModel.h"

@interface TAReportHistoryStudentStatusViewController () {
    NSArray<TAReportStudentStatusModel *> *studentStatusArray;
    
    NSDate *consideredDate;
    NSDateFormatter *dateFormatter;
    
    UIColor *grayColor;
    
    long long classroomId;
    NSString *subjectId;
}

@property (strong, nonatomic) TAReportARSPContainerController *arspContainerController;
@property (strong, nonatomic) TAReportCalendarViewController *taReportCalendarViewController;

@property (strong, nonatomic) CallGetTAHistoryStudentStatusListAPI *callGetTAHistoryStudentStatusListAPI;

@end

static NSString *cellIdentifier = @"Cell";

@implementation TAReportHistoryStudentStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    grayColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.backgroundColor = grayColor;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TAReportStudentStatusTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    self.arspContainerController = (TAReportARSPContainerController *)self.parentViewController;
    self.taReportCalendarViewController = (TAReportCalendarViewController *)self.arspContainerController.mainViewController;
    self.taReportCalendarViewController.delegate = self;
    
    classroomId = self.arspContainerController.selectedClassroomId;
    subjectId = self.arspContainerController.selectedSubjectId;
    
    dateFormatter = [Utils getDateFormatter];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    if(consideredDate == nil) {
        consideredDate = [NSDate date];
    }
    
    self.dateTitleLabel.text = [Utils getThaiDateFormatWithDate:consideredDate];
    
    [self getHistoryStudentStatus];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(studentStatusArray != nil) {
        return studentStatusArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TAReportStudentStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    TAReportStudentStatusModel *model = [studentStatusArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = [NSString stringWithFormat:@"%li. %@", (indexPath.row + 1), [model getStudentName]];
    [cell updateStatus:[model getStatus]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

#pragma mark - API Caller

- (void)getHistoryStudentStatus {
    
    self.callGetTAHistoryStudentStatusListAPI = nil;
    self.callGetTAHistoryStudentStatusListAPI = [[CallGetTAHistoryStudentStatusListAPI alloc] init];
    self.callGetTAHistoryStudentStatusListAPI.delegate = self;
    [self.callGetTAHistoryStudentStatusListAPI call:[UserData getSchoolId] classroomId:classroomId subjectId:subjectId date:consideredDate];
    
    [self showIndicator];
}

#pragma mark - CallGetTAHistoryStudentStatusListAPIDelegate
- (void)callGetTAHistoryStudentStatusListAPI:(CallGetTAHistoryStudentStatusListAPI *)classObj data:(NSArray<TAReportStudentStatusModel *> *)data success:(BOOL)success {
    
    [self stopIndicator];
    
    if(success && data != nil) {
        studentStatusArray = data;
        
        [self.tableView reloadData];
    }
}

#pragma mark - TAReportCalendarViewControllerDelegate
- (void)onSelectDate:(TAReportCalendarViewController *)classObj date:(NSDate *)date {
    
    self.dateTitleLabel.text = [Utils getThaiDateFormatWithDate: date];
    
    if(![DateUtility sameDate:consideredDate date2:date] ) {
        consideredDate = date;
        [self getHistoryStudentStatus];
    }
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

@end
