//
//  InboxMessageDBManager.m
//  JabjaiInboxMessageDBApp
//
//  Created by mac on 11/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "InboxMessageDBManager.h"
#import "DBManager.h"
#import "Utils.h"

@interface InboxMessageDBManager() {
    
}

@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSDateFormatter *formatter;

@end

@implementation InboxMessageDBManager

-(instancetype)init {
    self = [super init];
    
    if(self) {
        self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"appdatabase.sqlite"];
        self.formatter = [Utils getDateFormatter];
        [self.formatter setDateFormat:[Utils getXMLDateFormat]];
    }
    
    return self;
}

-(BOOL)insertMessage:(MessageInboxDataModel *)dataModel {
    
    if([self isMessageIDExistsInDatabase:dataModel.messageID]) {
        NSLog(@"%@", [NSString stringWithFormat:@"Attemp to add duplicate message id : %d, message : %@", dataModel.messageID, dataModel.message]);
        
        return NO;
    }
    
    NSString *dateStr = [self.formatter stringFromDate:dataModel.date];
    
    NSString *query = [NSString stringWithFormat:@"insert into message values(%d, %d, %d, %d, '%@', '%@', '%@')", dataModel.messageID, dataModel.userID, dataModel.messageType, dataModel.status, dataModel.title, dataModel.message, dateStr];
    
    [self.dbManager executeQuery:query];
    
    if([self.dbManager affectedRows] != 0) {
        return YES;
    }
    else {
        return NO;
    }

}

-(BOOL)insertMultipleMessage:(NSArray *)dataArr {
    
    NSMutableString *multipleQuery = [[NSMutableString alloc] init];
    
    if(dataArr.count > 0) {
        
        MessageInboxDataModel *model = [dataArr objectAtIndex:0];
        NSString *dateStr = [self.formatter stringFromDate:model.date];
        
        [multipleQuery appendFormat:@"insert into message values (%d, %d, %d, %d, '%@', '%@', '%@')", model.messageID, model.userID, model.messageType, model.status, model.title, model.message, dateStr];
        
        for(int i=1; i<dataArr.count; i++) {
            
            model = [dataArr objectAtIndex:i];
            dateStr = [self.formatter stringFromDate:model.date];
            
            [multipleQuery appendFormat:@" ,(%d, %d, %d, %d, '%@', '%@', '%@')", model.messageID, model.userID, model.messageType, model.status, model.title, model.message, dateStr];
        }
        
    }
    
    return [self.dbManager executeQuery:multipleQuery];
    
}

-(void)deleteMessageWithID:(int)messageID userID:(int)userID {
    NSString *query = [NSString stringWithFormat:@"delete from message where id=%d and user_id=%d", messageID, userID];
    
    [self.dbManager executeQuery:query];
}

-(void)updateMessageStatusWithID:(int)messageID status:(int)status userID:(int)userID {
    NSString *query = [NSString stringWithFormat:@"update message set status=%d where id=%d and user_id=%d", status, messageID, userID];
    
    [self.dbManager executeQuery:query];
}

-(BOOL)isMessageIDExistsInDatabase:(int)messageID {
    NSString *query = [NSString stringWithFormat:@"select count(*) from message where id=%d", messageID];
    
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    int count = [[[results objectAtIndex:0] objectAtIndex:0] intValue];
    
    if(count == 0) {
        return NO;
    }
    else {
        return YES;
    }
}

-(NSArray *)getMessageWithType:(int)type userID:(int)userID {
    
    NSString *query = [NSString stringWithFormat:@"select * from message where user_id=%d and type=%d order by id desc", userID, type];
    
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    //Get columns index
    NSInteger idIndex = [self.dbManager.arrColumnNames indexOfObject:@"id"];
    NSInteger userIDIndex = [self.dbManager.arrColumnNames indexOfObject:@"user_id"];
    NSInteger typeIndex = [self.dbManager.arrColumnNames indexOfObject:@"type"];
    NSInteger statusIndex = [self.dbManager.arrColumnNames indexOfObject:@"status"];
    NSInteger titleIndex = [self.dbManager.arrColumnNames indexOfObject:@"title"];
    NSInteger messageIndex = [self.dbManager.arrColumnNames indexOfObject:@"message"];
    NSInteger sendDateIndex = [self.dbManager.arrColumnNames indexOfObject:@"send_date"];
    
    NSMutableArray *returnedArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<results.count; i++) {
        NSArray *rowArray = [results objectAtIndex:i];
        
        int messageID = [[rowArray objectAtIndex:idIndex] intValue];
        int userID = [[rowArray objectAtIndex:userIDIndex] intValue];
        int type = [[rowArray objectAtIndex:typeIndex] intValue];
        int status = [[rowArray objectAtIndex:statusIndex] intValue];
        NSString *title = [rowArray objectAtIndex:titleIndex];
        NSString *message = [rowArray objectAtIndex:messageIndex];
        NSString *sendDateStr = [rowArray objectAtIndex:sendDateIndex];
        NSDate *date = [self.formatter dateFromString:sendDateStr];
        
        MessageInboxDataModel *model = [[MessageInboxDataModel alloc] init];
        model.messageID = messageID;
        model.userID = userID;
        model.messageType = type;
        model.status = status;
        model.title = title;
        model.message = message;
        model.date = date;
        
        [returnedArray addObject:model];
        
    }
    
    return returnedArray;
}

-(NSArray *)getAllMessageWithUserID:(int)userID {
    
    NSString *query = [NSString stringWithFormat:@"select * from message where user_id=%d order by id desc", userID];
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    //Get columns index
    NSInteger idIndex = [self.dbManager.arrColumnNames indexOfObject:@"id"];
    NSInteger userIDIndex = [self.dbManager.arrColumnNames indexOfObject:@"user_id"];
    NSInteger typeIndex = [self.dbManager.arrColumnNames indexOfObject:@"type"];
    NSInteger statusIndex = [self.dbManager.arrColumnNames indexOfObject:@"status"];
    NSInteger titleIndex = [self.dbManager.arrColumnNames indexOfObject:@"title"];
    NSInteger messageIndex = [self.dbManager.arrColumnNames indexOfObject:@"message"];
    NSInteger sendDateIndex = [self.dbManager.arrColumnNames indexOfObject:@"send_date"];
    
    NSMutableArray *returnedArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<results.count; i++) {
        NSArray *rowArray = [results objectAtIndex:i];
        
        int messageID = [[rowArray objectAtIndex:idIndex] intValue];
        int userID = [[rowArray objectAtIndex:userIDIndex] intValue];
        int type = [[rowArray objectAtIndex:typeIndex] intValue];
        int status = [[rowArray objectAtIndex:statusIndex] intValue];
        NSString *title = [rowArray objectAtIndex:titleIndex];
        NSString *message = [rowArray objectAtIndex:messageIndex];
        NSString *sendDateStr = [rowArray objectAtIndex:sendDateIndex];
        NSDate *date = [self.formatter dateFromString:sendDateStr];
        
        MessageInboxDataModel *model = [[MessageInboxDataModel alloc] init];
        model.messageID = messageID;
        model.userID = userID;
        model.messageType = type;
        model.status = status;
        model.title = title;
        model.message = message;
        model.date = date;
        
        [returnedArray addObject:model];
        
    }
    
    return returnedArray;

}

-(MessageInboxDataModel *)getMessageWithID:(int)messageID userID:(int)userID {
    NSString *query = [NSString stringWithFormat:@"select * from message where user_id=%d and id=%d", userID, messageID];
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    if(results.count != 0) {
        
        //Get columns index
        NSInteger idIndex = [self.dbManager.arrColumnNames indexOfObject:@"id"];
        NSInteger userIDIndex = [self.dbManager.arrColumnNames indexOfObject:@"user_id"];
        NSInteger typeIndex = [self.dbManager.arrColumnNames indexOfObject:@"type"];
        NSInteger statusIndex = [self.dbManager.arrColumnNames indexOfObject:@"status"];
        NSInteger titleIndex = [self.dbManager.arrColumnNames indexOfObject:@"title"];
        NSInteger messageIndex = [self.dbManager.arrColumnNames indexOfObject:@"message"];
        NSInteger sendDateIndex = [self.dbManager.arrColumnNames indexOfObject:@"send_date"];
        
        NSArray *rowArray = [results objectAtIndex:0];
        
        int messageID = [[rowArray objectAtIndex:idIndex] intValue];
        int userID = [[rowArray objectAtIndex:userIDIndex] intValue];
        int type = [[rowArray objectAtIndex:typeIndex] intValue];
        int status = [[rowArray objectAtIndex:statusIndex] intValue];
        NSString *title = [rowArray objectAtIndex:titleIndex];
        NSString *message = [rowArray objectAtIndex:messageIndex];
        NSString *sendDateStr = [rowArray objectAtIndex:sendDateIndex];
        NSDate *date = [self.formatter dateFromString:sendDateStr];
        
        MessageInboxDataModel *model = [[MessageInboxDataModel alloc] init];
        model.messageID = messageID;
        model.userID = userID;
        model.messageType = type;
        model.status = status;
        model.title = title;
        model.message = message;
        model.date = date;
        
        return model;

    }
    
    return nil;
}

-(int)getLatestMessageIDWithUserID:(int)userID {
    NSString *query = [NSString stringWithFormat:@"select id from message where user_id=%d order by id desc limit 1", userID];
    
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    if(results.count != 0) {
        int latestID = [[[results objectAtIndex:0] objectAtIndex:0] intValue];
        return latestID;
    }
    else {
        return 0;
    }
}

-(void)deleteAllDataInTable {
    NSString *query = @"delete from message";
    
    [self.dbManager executeQuery:query];
}

-(int)getNumberOfUnreadMessageWithUserID:(int)userID {
    
    NSString *query = [NSString stringWithFormat:@"select count(*) from message where user_id=%d and status=0", userID];
    
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    if(results.count != 0) {
        int number = [[[results objectAtIndex:0] objectAtIndex:0] intValue];
        return number;
    }
    else {
        return 0;
    }
}

//TypeID 1 = Attendance, 2 = Purchasing, 3 = Topup, 4 = AbsenceRequest, 5 = News
-(int)getNumberOfUnreadMessageWithUserID:(int)userID typeID:(int)typeID {
    
    NSString *query = [NSString stringWithFormat:@"select count(*) from message where user_id=%d and type=%d and status=0", userID, typeID];
    
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    if(results.count != 0) {
        int number = [[[results objectAtIndex:0] objectAtIndex:0] intValue];
        return number;
    }
    else {
        return 0;
    }
}

@end
