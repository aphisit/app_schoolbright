//
//  SchoolLevelModel.h
//  JabjaiApp
//
//  Created by mac on 7/4/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SchoolLevelModel : NSObject

@property (nonatomic) long long classLevelId;
@property (nonatomic) NSString *classLevelName;

- (long long)getClassLevelId;
- (NSString *)getClassLevelName;

@end
