//
//  CallGetLoginAPI.m
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetLoginAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetLoginAPI () {
    NSInteger connectCounter;
}

@end
@implementation CallGetLoginAPI

- (instancetype) init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(NSString *)userName password:(NSString *)password {
    [self getDataFromServer:userName password:password];
}

- (void)getDataFromServer:(NSString *)userName password:(NSString *)password {
    NSString *urlString = [APIURL getLoginURL:userName password:password];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataFromServer:userName password:password];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:userName password:password];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSDictionary class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:userName password:password];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                NSDictionary *returnedDict = returnedData;
                connectCounter = 0;
                
                long long userID = [[returnedDict objectForKey:@"ID"] intValue];
                int academyType = [[returnedDict objectForKey:@"Value"] intValue];
                int userType = [[returnedDict objectForKey:@"Type"] intValue];
                long long schoolId = [[returnedData objectForKey:@"SchoolId"] longLongValue];
                int gender = [[returnedData objectForKey:@"sex"] intValue];
                
                NSString *firstName, *lastName, *imageUrl;
                
                if(![[returnedData objectForKey:@"image"] isKindOfClass:[NSNull class]]) {
                    imageUrl = [returnedData objectForKey:@"image"];
                }
                else {
                    imageUrl = @"";
                }
                
                if(![[returnedData objectForKey:@"name"] isKindOfClass:[NSNull class]]) {
                    firstName = [returnedData objectForKey:@"name"];
                }
                else {
                    firstName = @"";
                }
                
                if(![[returnedData objectForKey:@"lastname"] isKindOfClass:[NSNull class]]) {
                    lastName = [returnedData objectForKey:@"lastname"];
                }
                else {
                    lastName = @"";
                }

                if(userID > 0) {
                    LoginUserModel *model = [[LoginUserModel alloc] init];
                    [model setUserId:userID];
                    [model setSchoolId:schoolId];
                    [model setUserType:userType];
                    [model setAcademyType:academyType];
                    [model setImageUrl:imageUrl];
                    [model setFirstName:firstName];
                    [model setLastName:lastName];
                    [model setGender:gender];
                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetLoginAPI:data:success:)]) {
                        [self.delegate callGetLoginAPI:self data:model success:YES];
                    }

                }
                else if(userID < 0) {
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onUnRegisterFinger:confirmCode:)]) {
                        NSString *confirmCode = [NSString stringWithFormat:@"%lld", (userID * -1)];
                        
                        [self.delegate onUnRegisterFinger:self confirmCode:confirmCode];
                    }
                }
                else {
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(incorrectUserNameOrPassword:)]) {
                        [self.delegate incorrectUserNameOrPassword:self];
                    }
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetLoginAPI:data:success:)]) {
                [self.delegate callGetLoginAPI:self data:nil success:NO];
            }
        }
    }];
}

@end
