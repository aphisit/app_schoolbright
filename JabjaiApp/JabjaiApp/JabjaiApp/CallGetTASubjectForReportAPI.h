//
//  CallGetTASubjectForReportAPI.h
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TASubjectModel.h"

@class CallGetTASubjectForReportAPI;

@protocol CallGetTASubjectForReportAPIDelegate <NSObject>

@optional
- (void)callGetTASubjectForReportAPI:(CallGetTASubjectForReportAPI *)classObj data:(NSArray<TASubjectModel *> *)data success:(BOOL)success;
- (void)notArrangeTimeTable:(CallGetTASubjectForReportAPI *)classObj;

@end

@interface CallGetTASubjectForReportAPI : NSObject

@property (nonatomic, weak) id<CallGetTASubjectForReportAPIDelegate> delegate;

- (void)call:(long long)schoolId classroomId:(long long)classroomId;

@end
