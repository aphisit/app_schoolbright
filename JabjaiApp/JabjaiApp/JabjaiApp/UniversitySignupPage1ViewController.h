//
//  UniversitySignupPage1ViewController.h
//  JabjaiApp
//
//  Created by mac on 11/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupDataModel.h"
#import "TableListDialog.h"

@interface UniversitySignupPage1ViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate>

@property (strong, nonatomic) SignupDataModel *signupDataModel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UITextField *universityTextField;
@property (weak, nonatomic) IBOutlet UITextField *facultyTextField;
@property (weak, nonatomic) IBOutlet UITextField *degreeTextField;
@property (weak, nonatomic) IBOutlet UITextField *majorTextField;
@property (weak, nonatomic) IBOutlet UITextField *studentIDTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

- (IBAction)moveBack:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)nextStep:(id)sender;

@end
