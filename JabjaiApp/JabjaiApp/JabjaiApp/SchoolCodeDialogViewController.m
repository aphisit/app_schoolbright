//
//  SchoolCodeAlertViewController.m
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SchoolCodeDialogViewController.h"

@interface SchoolCodeDialogViewController ()

@property (nonatomic, assign) BOOL isShowing;

@end

@implementation SchoolCodeDialogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Decorate view
    self.dialogView.layer.cornerRadius = 10;
    self.dialogView.layer.masksToBounds = YES;
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)].CGPath;
    self.headerView.layer.mask = maskLayer;
    self.headerView.layer.masksToBounds = YES;
    
    self.cancelBtn.layer.cornerRadius = 20;
    self.cancelBtn.layer.masksToBounds = YES;
    self.okBtn.layer.cornerRadius = 20;
    self.okBtn.layer.masksToBounds = YES;
    
    // Initialize variables
    self.isShowing = NO;
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showDialogInView:(UIView *)targetView {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    // Clear value of field to default values
    self.passwordTextField.text = @"";
    self.hintLabel.text = @"กรุณากรอกรหัสโรงเรียน";
    self.hintLabel.textColor = [UIColor blackColor];
    
    self.isShowing = YES;
    
}

-(void)showDialogInView:(UIView *)targetView title:(NSString *)title hint:(NSString *)hint {
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    // Clear value of field to default values
    self.titleLabel.text = title;
    self.hintLabel.text = hint;
    self.hintLabel.textColor = [UIColor blackColor];
    self.passwordTextField.text = @"";
    
    self.isShowing = YES;
}


-(void)removeDialogFromView {
  
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dismissDialog {
    
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onCancelButtonPress)]) {
        [self.delegate onCancelButtonPress];
    }
 
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

- (IBAction)cancelAction:(id)sender {
    [self dismissDialog];
}

- (IBAction)okAction:(id)sender {
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onOkButtonPress)]) {
        [self.delegate onOkButtonPress];
    }
}

- (IBAction)dismissKeyboard:(id)sender {
    [self resignFirstResponder];
}
@end
