//
//  CreditBureauViewController.m
//  JabjaiApp
//
//  Created by mac on 10/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "CreditBureauViewController.h"
#import "ScoreViewController.h"
#import "FinanceViewController.h"
#import "StudyViewController.h"
#import "AppDelegate.h"
#import "MessageInboxViewController.h"

@interface CreditBureauViewController () {
    UIColor *OrangeColor;
    UIColor *GrayColor;
}

@property (nonatomic) CAPSPageMenu *pageMenu;

@end

@implementation CreditBureauViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    OrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1.0];
    GrayColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    
    // Because credit bureau is default front view of SWRevealViewController, thus check in its if it has deep link send from notification go to MessageInboxViewController;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.deeplink != nil) {
        
        MessageInboxViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageInboxStoryboard"];
        viewController.message_id = appDelegate.deeplink_message_id;
        
        if([appDelegate.deeplink isEqualToString:@"come_to_school"]) {
            viewController.inboxType = ATTENDANCE;
        }
        else if([appDelegate.deeplink isEqualToString:@"buy_product"]) {
            viewController.inboxType = PURCHASING;
        }
        else if([appDelegate.deeplink isEqualToString:@"topup"]) {
            viewController.inboxType = TOPUP;
        }
        else if([appDelegate.deeplink isEqualToString:@"absence_request"]) {
            viewController.inboxType = ABSENCEREQUEST;
        }
        else if([appDelegate.deeplink isEqualToString:@"news"]) {
            viewController.inboxType = NEWS;
        }
        
        // Clear deep link
        appDelegate.deeplink = nil;
        appDelegate.deeplink_message_id = -1;
        
        [self.revealViewController pushFrontViewController:viewController animated:NO];
    }

    
}

- (void)viewDidLayoutSubviews {
    [self setupPageMenu];
}

- (void)viewDidAppear:(BOOL)animated {

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setupPageMenu {
    ScoreViewController *controller1 = [[ScoreViewController alloc] init];
    controller1.title = @"เครดิตบูโร";
    
    StudyViewController *controller2 = [[StudyViewController alloc] init];
    controller2.title = @"การเรียน";
    
    FinanceViewController *controller3 = [[FinanceViewController alloc] init];
    controller3.title = @"การเงิน";
    
    NSArray *controllerArray = @[controller1, controller2, controller3];
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionMenuItemSeparatorWidth: @(4.3),
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                                 CAPSPageMenuOptionMenuMargin: @(20),
                                 CAPSPageMenuOptionMenuHeight: @(40),
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"ThaiSansNeue-SemiBold" size:22.0],
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                                 CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                                 CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    // For the first time set first tab background to orange and label color to white
    NSArray *menuItems = _pageMenu.menuItems;
    MenuItemView *menuItemView = [menuItems objectAtIndex:_pageMenu.currentPageIndex];
    menuItemView.backgroundColor = OrangeColor;
    menuItemView.titleLabel.textColor = [UIColor whiteColor];

    //Optional delegate
    _pageMenu.delegate = self;
    
    [self.contentView addSubview:_pageMenu.view];
    
}

#pragma CAPSPageMenuDelegate

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    NSLog(@"DidMoveToPage");
    
    NSArray *menuItems = _pageMenu.menuItems;
    
    int count = 0;
    
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = OrangeColor;
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        
        count++;
    }
    
}


- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

@end
