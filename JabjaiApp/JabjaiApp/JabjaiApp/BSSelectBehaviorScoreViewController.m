//
//  BSSelectBehaviorScoreViewController.m
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSSelectBehaviorScoreViewController.h"
#import "BSSelectStudentsViewController.h"
#import "BSSummaryViewController.h"
#import "AlertDialog.h"
#import "MessageInBoxScrollableTextDialog.h"
#import "UserData.h"
#import "Constant.h"

@interface BSSelectBehaviorScoreViewController () {
    
    UIColor *greenBGColor;
    UIColor *redBGColor;
    
    // The dialog
    AlertDialog *alertDialog;
    MessageInBoxScrollableTextDialog *alertMessge;
}

@property (strong, nonatomic) CallGetAddBehaviorScoreListAPI *callGetAddBehaviorScoreListAPI;
@property (strong, nonatomic) CallGetReduceBehaviorScoreListAPI *callGetReduceBehaviorScoreListAPI;

@end

static NSString *cellIdentifier = @"Cell";

@implementation BSSelectBehaviorScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSBehaviorScoreRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    greenBGColor = [UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0];
    redBGColor = [UIColor colorWithRed:220/255.0 green:43/255.0 blue:69/255.0 alpha:1.0];
    
    if(_mode == BS_MODE_ADD) {
        self.headerTitleLabel.text = @"เพิ่มคะแนนความประพฤติ";
        [self.nextButton setBackgroundColor:greenBGColor];
    }
    else {
        self.headerTitleLabel.text = @"ตัดคะแนนความประพฤติ";
        [self.nextButton setBackgroundColor:redBGColor];
    }

    if(self.behaviorScoreArray == nil) {
        
        if(_mode == BS_MODE_ADD) {
            [self getAddBehaviorScoreData];
        }
        else {
            [self getReduceBehaviorScoreData];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.behaviorScoreArray != nil) {
        return self.behaviorScoreArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BSBehaviorScoreRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    BehaviorScoreModel *model = [self.behaviorScoreArray objectAtIndex:indexPath.row];
    
    cell.delegate = self;
    cell.index = indexPath.row;
    cell.titleLabel.text = [model getBehaviorScoreName];
   

    if([model getBehaviorScoreType] == 0) { // type add
        cell.scoreLabel.text = [NSString stringWithFormat:@"+%i", (int)[model getScore]];
    }
    else {
        cell.scoreLabel.text = [NSString stringWithFormat:@"-%i", (int)[model getScore]];
    }
    
    if([model isSelected]) {
        
        if(self.mode == BS_MODE_ADD) {
            [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
        }
        else {
            [cell setRadioButtonSelectedWithType:RADIO_TYPE_RED];
        }
    }
    else {
        [cell setRadioButtonClearSelected];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BehaviorScoreModel *model = [self.behaviorScoreArray objectAtIndex:indexPath.row];
    NSString *alertMessage = [model getBehaviorScoreName];
    [self showAlertDialogMessagelBox:alertMessage];
    NSLog(@"index2 = %d",indexPath);
    
}

#pragma mark - BSBehaviorScoreTableViewCellDelegate

- (void)onPressRadioButton:(BSBehaviorScoreRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index {
    
    BOOL isSelected = [[self.behaviorScoreArray objectAtIndex:index] isSelected];
    [[self.behaviorScoreArray objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - API Caller

- (void)getAddBehaviorScoreData {
    
    if(self.callGetAddBehaviorScoreListAPI != nil) {
        self.callGetAddBehaviorScoreListAPI = nil;
    }
    
    self.callGetAddBehaviorScoreListAPI = [[CallGetAddBehaviorScoreListAPI alloc] init];
    self.callGetAddBehaviorScoreListAPI.delegate = self;
    [self.callGetAddBehaviorScoreListAPI call:[UserData getSchoolId]];
}

- (void)getReduceBehaviorScoreData {
    
    if(self.callGetReduceBehaviorScoreListAPI != nil) {
        self.callGetReduceBehaviorScoreListAPI = nil;
    }
    
    self.callGetReduceBehaviorScoreListAPI = [[CallGetReduceBehaviorScoreListAPI alloc] init];
    self.callGetReduceBehaviorScoreListAPI.delegate = self;
    [self.callGetReduceBehaviorScoreListAPI call:[UserData getSchoolId]];
}

#pragma mark - API Delegate

- (void)callGetAddBehaviorScoreListAPI:(CallGetAddBehaviorScoreListAPI *)classObj data:(NSArray<BehaviorScoreModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _behaviorScoreArray = data;
        
        [self.tableView reloadData];
    }
}

- (void)callGetReduceBehaviorScoreListAPI:(CallGetReduceBehaviorScoreListAPI *)classObj data:(NSArray<BehaviorScoreModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _behaviorScoreArray = data;
        
        [self.tableView reloadData];
    }
}

#pragma mark - Dialog

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil) {
        if([alertDialog isDialogShowing]) {
            [alertDialog dismissDialog];
        }
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void) showAlertDialogMessagelBox:(NSString *)message {
//    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
//    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
   
    if (alertMessge != nil) {
        if([alertMessge isDialogShowing]){
            [alertMessge dismissDialog];
        }
    }
    else{
        alertMessge = [[MessageInBoxScrollableTextDialog alloc] init];
    }
    [alertMessge showDialogInView:self.view title:@"พฤติกรรม" message:message messageDate:nil];

}

#pragma mark - Utility

- (BOOL)validateData {
    if(_behaviorScoreArray != nil) {
        for(BehaviorScoreModel *model in _behaviorScoreArray) {
            
            if([model isSelected]) {
                return YES;
            }
        }
    }
    
    return NO;
}

- (void)gotoBSSummaryViewController {
    
    BSSummaryViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSummaryStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    
    viewController.selectedStudentArray = _selectedStudentArray;
    
    viewController.behaviorScoreArray = _behaviorScoreArray;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

#pragma mark - Action functions

- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        [self gotoBSSummaryViewController];
    }
    else {
        NSString *alertMessage = @"กรุณาเลือกอย่างน้อย 1 รายการ";
        [self showAlertDialogWithMessage:alertMessage];
    }
    
}

- (IBAction)moveBack:(id)sender {
    
    BSSelectStudentsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectStudentsStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    
    viewController.selectedStudentArray = _selectedStudentArray;
    
    viewController.behaviorScoreArray = _behaviorScoreArray;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
@end
