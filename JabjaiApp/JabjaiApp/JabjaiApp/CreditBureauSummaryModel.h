//
//  CreditBureauSummaryModel.h
//  JabjaiApp
//
//  Created by mac on 11/4/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreditBureauSummaryModel : NSObject {

}

@property (nonatomic) NSNumber *status;
@property (nonatomic) NSNumber *times;
@property (nonatomic) NSNumber *creditLimits;
@property (nonatomic) NSNumber *money;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;

@end
