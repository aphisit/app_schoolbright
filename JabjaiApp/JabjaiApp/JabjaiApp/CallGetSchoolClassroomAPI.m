//
//  CallGetSchoolClassroomAPI.m
//  JabjaiApp
//
//  Created by mac on 7/5/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetSchoolClassroomAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetSchoolClassroomAPI () {
    NSInteger connectCounter;
    NSMutableArray<SchoolClassroomModel *> *schoolClassrooms;
}

@end

@implementation CallGetSchoolClassroomAPI

- (void)call:(long long)schoolId classLevelId:(long long)classLevelId {
    connectCounter = 0;
    [self getSchoolClassroomWithSchoolId:schoolId classLevelId:classLevelId];
}

#pragma mark - Get API Data

- (void)getSchoolClassroomWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId {
    NSString *URLString = [APIURL getSchoolClassroomWithSchoolId:schoolId classLevelId:classLevelId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSchoolClassroomWithSchoolId:schoolId classLevelId:classLevelId];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolClassroomWithSchoolId:schoolId classLevelId:classLevelId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolClassroomWithSchoolId:schoolId classLevelId:classLevelId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(schoolClassrooms != nil) {
                    [schoolClassrooms removeAllObjects];
                    schoolClassrooms = nil;
                }
                
                schoolClassrooms = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSNumber *nClassroomID = [[NSNumber alloc] initWithInt:(int)[[dataDict objectForKey:@"ID"] integerValue]];
                    NSMutableString *classroomName = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"Value"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) classroomName);
                    
                    SchoolClassroomModel *model = [[SchoolClassroomModel alloc] init];
                    model.classroomID = nClassroomID;
                    model.classroomName = classroomName;
                    
                    [schoolClassrooms addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetSchoolClassroomAPI:data:success:)]) {
                    
                    [self.delegate callGetSchoolClassroomAPI:self data:schoolClassrooms success:YES];
                }
                
                return ;
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetSchoolClassroomAPI:data:success:)]) {
                [self.delegate callGetSchoolClassroomAPI:self data:nil success:NO];
            }
        }
        
    }];
}


@end
