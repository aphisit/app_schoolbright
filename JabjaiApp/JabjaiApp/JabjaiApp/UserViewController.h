//
//  UserViewController.h
//  JabjaiApp
//
//  Created by mac on 5/17/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUIImageView.h"

@interface UserViewController : UIViewController

@property (weak, nonatomic) IBOutlet CustomUIImageView *userImageView;

@property (weak, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *classLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentIDLabel;
@property (weak, nonatomic) IBOutlet UIStackView *classStackView;
@property (weak, nonatomic) IBOutlet UIStackView *studentIDStackView;

@property (weak, nonatomic) IBOutlet UIView *studentFooterView;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentBalanceLabel;

@property (weak, nonatomic) IBOutlet UIView *teacherFooterView;
@property (weak, nonatomic) IBOutlet UILabel *teacherBalanceLabel;

@end
