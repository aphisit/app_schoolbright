//
//  TableListWithHeaderDialog.m
//  JabjaiApp
//
//  Created by mac on 11/5/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "TableListWithHeaderDialog.h"

@interface TableListWithHeaderDialog () {
    NSArray *dataDictKeys;
    NSDictionary *stringDataDict; //This dictionary contains NSArray of months
    NSMutableArray *dataPerSection;
    
    NSInteger totalRow;
    
    UITapGestureRecognizer *tapGesture;
}

@property (nonatomic, strong) NSString *requestCode;
@property (nonatomic, assign) BOOL isShowing;

@end

static NSString *tableCellID = @"Cell";
static NSString *tableHeaderCellID = @"HeaderCell";
static CGFloat cellHeight = 50;

@implementation TableListWithHeaderDialog

-(id)initWithRequestCode:(NSString *)requestCode {
    
    self = [[TableListWithHeaderDialog alloc] init];
    
    if(self) {
        self.requestCode = requestCode;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeDialogFromView)];
//    tapGesture.delegate = self;
//    tapGesture.cancelsTouchesInView = NO;
//    [self.view addGestureRecognizer:tapGesture];
    
    self.tblView.dataSource = self;
    self.tblView.delegate = self;
    
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([TableListViewCell class]) bundle:nil] forCellReuseIdentifier:tableCellID];
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([TableListHeaderViewCell class]) bundle:nil] forCellReuseIdentifier:tableHeaderCellID];
    
    self.tblView.layer.cornerRadius = 10.0;
    self.tblView.layer.masksToBounds = YES;
    self.tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.isShowing = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.tblView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(dataPerSection != nil) {
        return dataPerSection.count;
    }
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(dataPerSection != nil) {
        return [[dataPerSection objectAtIndex:section] count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *dataRow = [dataPerSection objectAtIndex:indexPath.section];
    NSString *month = [dataRow objectAtIndex:indexPath.row];
    
    if(indexPath.row == 0) { // row equals zero show table header cell
        TableListHeaderViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableHeaderCellID forIndexPath:indexPath];
        
        cell.label.text = month;
        cell.userInteractionEnabled = NO;
        
        return cell;
    }
    else {
        TableListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableCellID forIndexPath:indexPath];
        cell.label.text = month;
        cell.userInteractionEnabled = YES;
        
        return cell;
    }
}

#pragma TableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row != 0) {
        NSArray *dataRow = [dataPerSection objectAtIndex:indexPath.section];
        NSString *month = [dataRow objectAtIndex:indexPath.row];
        NSString *termName = [dataRow objectAtIndex:0];
        
        [self.delegate onItemSelectWithRequestCode:_requestCode headerSectionString:termName selectedString:month];
        [self removeDialogFromView];
    }
}

#pragma mark - Dialog Functions

-(void)showDialogInView:(UIView *)targetView stringDataDict:(NSDictionary *)dataDict dataDictKeys:(NSArray *)dataKeys {
    
    dataDictKeys = dataKeys;
    stringDataDict = dataDict;
    [self synthesisDataArr];
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.tblView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.tblView setAlpha:1.0];
    [UIView commitAnimations];
    
    
    // Adjust table view frame size;
    
    int cellNumber = 6;
    if(totalRow < 7) {
        cellNumber = (int)totalRow;
    }
    
    // Calcalate position of new table view dialog
    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat viewHeight = self.view.frame.size.height;
    
    CGFloat tableViewWidth = self.view.frame.size.width - 32; //self.tblView.frame.size.width;
    CGFloat tableViewHeight = cellHeight * cellNumber;
    
    CGFloat posX = (viewWidth - tableViewWidth)/2.0;
    CGFloat posY = (viewHeight - tableViewHeight)/2.0;
    
    self.tblView.frame = CGRectMake(posX, posY, tableViewWidth, cellHeight * cellNumber);
    
    [self.tblView reloadData];
    
    self.isShowing = YES;;
    
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.tblView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

#pragma mark ManagingDataToShow
-(void)synthesisDataArr {
    
    dataPerSection = [[NSMutableArray alloc] init];
    
    NSArray *keys = dataDictKeys;
    
    if(keys == nil || keys.count == 0) {
        keys = [stringDataDict allKeys];
    }
    
    totalRow = 0;
    
    for(int i=0; i<keys.count; i++) {
        
        NSMutableArray *dataRow = [[NSMutableArray alloc] init];
        
        NSString *termStr = [keys objectAtIndex:i];
        NSArray *monthsArr = [stringDataDict objectForKey:termStr];
        
        [dataRow addObject:termStr];
        totalRow++;
        
        for(int j=0; j<monthsArr.count; j++) {
            NSString *month = [monthsArr objectAtIndex:j];
            [dataRow addObject:month];
            totalRow++;
        }
        
        [dataPerSection addObject:dataRow];
    }
}


@end
