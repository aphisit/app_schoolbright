//
//  SchoolGradModel.h
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SchoolGradModel : NSObject

@property (nonatomic, strong) NSNumber *gradID;
@property (nonatomic, strong) NSString *gradName;

-(NSNumber *)getGradID;
-(NSString *)getGradName;

@end
