//
//  StudyViewController.h
//  JabjaiApp
//
//  Created by mac on 10/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JabjaiApp-Swift.h>
#import <Charts/Charts.h>
#import "TableListWithHeaderDialog.h"
#import "TableListDialog.h"

@interface StudyViewController : UIViewController <TableListWithHeaderDialogDelegate, TableListDialogDelegate, ChartViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *yearlyBtn;
@property (weak, nonatomic) IBOutlet UIButton *monthlyBtn;
@property (weak, nonatomic) IBOutlet UIButton *weeklyBtn;

@property (weak, nonatomic) IBOutlet PieChartView *pieChart;

@property (weak, nonatomic) IBOutlet UILabel *onTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *lateLabel;
@property (weak, nonatomic) IBOutlet UILabel *absenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

@property (weak, nonatomic) IBOutlet UIStackView *graphLegendView;
@property (weak, nonatomic) IBOutlet UIView *viewOnTime;
@property (weak, nonatomic) IBOutlet UIView *viewLate;
@property (weak, nonatomic) IBOutlet UIView *viewAbsence;

@property (weak, nonatomic) IBOutlet UIStackView *onTimeStackView;
@property (weak, nonatomic) IBOutlet UIStackView *lateStackView;
@property (weak, nonatomic) IBOutlet UIStackView *absenceStackView;

- (IBAction)yearlyButtonTap:(id)sender;
- (IBAction)monthlyButtonTap:(id)sender;
- (IBAction)weeklyButtonTap:(id)sender;

@end
