//
//  SNClassLevelGroupViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "TableListDialog.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "SWRevealViewController.h"

@interface SNClassLevelGroupViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate>{
}
// The dialog
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger mode;

@property (weak, nonatomic) IBOutlet UITextField *classLavelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classRoomTextField;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
- (IBAction)actionNext:(id)sender;
- (IBAction)backToMenu:(id)sender;


@end
