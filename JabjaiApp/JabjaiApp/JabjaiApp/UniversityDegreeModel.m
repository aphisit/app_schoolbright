//
//  UniversityDegreeModel.m
//  JabjaiApp
//
//  Created by mac on 11/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "UniversityDegreeModel.h"

@implementation UniversityDegreeModel

-(NSNumber *)getDegreeID {
    return self.degreeID;
}
-(NSString *)getDegreeName {
    return self.degreeName;
}

@end
