//
//  BSNameBehaviorScoreTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSNameBehaviorScoreTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *behaviorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@end
