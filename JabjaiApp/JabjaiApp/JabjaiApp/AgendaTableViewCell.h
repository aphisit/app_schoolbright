//
//  AgendaTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 10/24/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelLeft;
@property (weak, nonatomic) IBOutlet UILabel *labelRight;

@end
