//
//  TopupItemListViewController.m
//  JabjaiApp
//
//  Created by mac on 5/14/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TopupItemListViewController.h"
#import "TopupTableViewCell.h"
#import "MessageInboxDataModel.h"
#import "Constant.h"
#import "APIURL.h"
#import "Utils.h"

@interface TopupItemListViewController () {
    NSMutableArray<MessageInboxDataModel *> *messageArray;
    
    NSInteger connectCounter;
    UIColor *grayColor;
}

@property (strong, nonatomic) ARSPContainerController *panelControllerContainer;
@property (strong, nonatomic) TopupReportViewController *topupReportViewController;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;

@end

@implementation TopupItemListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.panelControllerContainer = (ARSPContainerController *)self.parentViewController;
    self.topupReportViewController = (TopupReportViewController *)self.panelControllerContainer.mainViewController;
    self.topupReportViewController.delegate = self;
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = grayColor;
    
    connectCounter = 0;

    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TopupTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:[NSDate date]];
    [self getTopupItemListWithDate:[NSDate date]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(messageArray == nil) {
        return 0;
    }
    else {
        return messageArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MessageInboxDataModel *model = [messageArray objectAtIndex:indexPath.row];
    
    TopupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.titleLabel.text = model.title;
    cell.timeLabel.text = [self.timeFormatter stringFromDate:model.date];
    cell.messageLabel.text = model.message;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

#pragma mark - TopupReportViewControllerDelegate
- (void)onSelectDate:(NSDate *)date {
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:date];
    [self getTopupItemListWithDate:date];
}

#pragma mark - Get API Data
- (void)getTopupItemListWithDate:(NSDate *)date {
    
    long long userID = [UserData getUserID];
    NSString *URLString = [APIURL getTopupMessageWithUserID:userID date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getTopupItemListWithDate:date];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getTopupItemListWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getTopupItemListWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(messageArray != nil) {
                    [messageArray removeAllObjects];
                    messageArray = nil;
                }
                
                messageArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    int status = [[dataDict objectForKey:@"nStatus"] intValue];
                    int type = [[dataDict objectForKey:@"nType"] intValue];
                    NSMutableString *messageDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dSend"]];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sTitle"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    NSRange dotRange = [messageDateStr rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:messageDateStr];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:messageDateStr];
                    }
                    
                    MessageInboxDataModel *model = [[MessageInboxDataModel alloc] init];
                    model.messageID = messageID;
                    model.userID = userID;
                    model.messageType = type;
                    model.status =status;
                    model.title = title;
                    model.message = message;
                    model.date = messageDate;

                    [messageArray addObject:model];
                    
                }
                
                [self.tableView reloadData];
            }
        }
        
    }];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

@end
