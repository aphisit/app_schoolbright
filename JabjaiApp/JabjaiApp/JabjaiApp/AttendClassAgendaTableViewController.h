//
//  AttendClassAgendaTableViewController.h
//  JabjaiApp
//
//  Created by mac on 1/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Attend2SchoolFilter.h"

@interface AttendClassAgendaTableViewController : UITableViewController

@property (nonatomic) int sectionID;

-(void)applyFilter:(Attend2SchoolFilter *)filter;

@end
