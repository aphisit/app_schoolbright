//
//  SettingViewController.h
//  JabjaiApp
//
//  Created by mac on 12/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangeFingerprintAlertDialog.h"
#import "ChangeFingerprintConfirmDialog.h"

@interface SettingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, ChangeFingerprintConfirmDialogDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblView;

- (IBAction)openDrawer:(id)sender;

@end
