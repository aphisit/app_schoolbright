//
//  CallUpdateBehaviorScorePOSTAPI.h
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CallUpdateBehaviorScorePOSTAPI;

@protocol CallUpdateBehaviorScorePOSTAPIDelegate <NSObject>

@optional
- (void)callUpdateBehaviorScorePOSTAPI:(CallUpdateBehaviorScorePOSTAPI *)classObj response:(NSString *)response success:(BOOL)success;

@end
@interface CallUpdateBehaviorScorePOSTAPI : NSObject

@property (nonatomic, weak) id<CallUpdateBehaviorScorePOSTAPIDelegate> delegate;

- (void)call:(long long)schoolId teacherId:(long long)teacherId jsonString:(NSString *)jsonString;

@end
