//
//  USectionModel.h
//  JabjaiApp
//
//  Created by mac on 12/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface USectionModel : NSObject

@property (nonatomic) int sectionID;
@property (strong, nonatomic) NSString *sectionName;

@end
