//
//  SNClassRoomIndividualViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "BSSelectedStudent.h"
#import "BehaviorScoreModel.h"
#import "BSStudentNameRadioTableViewCell.h"
#import "CallGetStudentInClassroomAPI.h"

@interface SNClassRoomIndividualViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, BSStudentNameRadioTableViewCellDelegate, CallGetStudentInClassroomAPIDelegate>
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger mode;
// BSSelectStudentsViewController
@property (strong, nonatomic) NSArray<BSSelectedStudent *> *selectedStudentArray;

// BSSelectBehaviorScoreViewController
@property (strong, nonatomic) NSArray<BehaviorScoreModel *> *behaviorScoreArray;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

- (IBAction)actionNext:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
