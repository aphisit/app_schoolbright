//
//  testViewController.h
//  JabjaiApp
//
//  Created by toffee on 8/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface testViewController : UIViewController{}

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *lastName;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;



@end
