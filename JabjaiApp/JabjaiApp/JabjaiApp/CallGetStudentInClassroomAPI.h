//
//  CallGetStudentInClassroomAPI.h
//  JabjaiApp
//
//  Created by mac on 7/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSSelectedStudent.h"

@class CallGetStudentInClassroomAPI;

@protocol CallGetStudentInClassroomAPIDelegate <NSObject>

- (void)callGetStudentInClassrommAPI:(CallGetStudentInClassroomAPI *)classObj data:(NSArray<BSSelectedStudent *> *)data success:(BOOL)success;

@end

@interface CallGetStudentInClassroomAPI : NSObject

@property (nonatomic, weak) id<CallGetStudentInClassroomAPIDelegate> delegate;

- (void)call:(long long)schoolId classroomId:(long long)classroomId;

@end
