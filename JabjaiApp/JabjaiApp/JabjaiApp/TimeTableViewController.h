//
//  TimeTableViewController.h
//  JabjaiApp
//
//  Created by mac on 5/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import ARSlidingPanel;

@protocol TimeTableViewControllerDelegate <NSObject>

@optional
- (void)onSelectDate:(NSDate *)date;

@end

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "SSubjectViewControllerDelegate.h"
#import "CFSCalendar.h"

@interface TimeTableViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, ARSPDragDelegate, ARSPVisibilityStateDelegate, SSubjectViewControllerDelegate>

@property (retain, nonatomic) id<TimeTableViewControllerDelegate> delegate;

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) NSCalendar *gregorian;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)openDrawer:(id)sender;


@end
