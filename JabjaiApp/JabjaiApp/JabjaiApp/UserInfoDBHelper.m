//
//  UserInfoDBHelper.m
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "UserInfoDBHelper.h"
#import "DBManager.h"

@interface UserInfoDBHelper () {
    
}

@property (nonatomic, strong) DBManager *dbManager;

@end

@implementation UserInfoDBHelper

- (instancetype)init {
    self = [super init];
    
    if(self) {
        self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"appdatabase.sqlite"];
    }
    
    return self;
}

- (BOOL)insertData:(MultipleUserModel *)dataModel {
    
    if([self isMasterIdExistsInDBWithMasterId:dataModel.masterId slaveId:dataModel.slaveId]) {
        NSLog(@"%@", [NSString stringWithFormat:@"Attempted to add duplicate master_id=%lld, slave_id=%lld", dataModel.masterId, dataModel.slaveId]);
        
        return NO;
    }
    
    long long masterId = [dataModel getMasterId];
    long long slaveId = [dataModel getSlaveId];
    NSInteger userType = [dataModel getUserType];
    NSInteger academyType = [dataModel getAcademyType];
    long long schoolId = [dataModel getSchoolId];
    NSString *imageUrl = [dataModel getImageUrl];
    NSString *firstName = [dataModel getFirstName];
    NSString *lastName = [dataModel getLastName];
    NSInteger gender = [dataModel getGender];
    
    if(imageUrl == nil) {
        imageUrl = @"";
    }
    
    if(firstName == nil) {
        firstName = @"";
    }
    
    if(lastName == nil) {
        lastName = @"";
    }
    
    NSString *query = [NSString stringWithFormat:@"insert into user_info (master_id, slave_id, user_type, academy_type, school_id, image_url, firstname, lastname, gender) values (%lld, %lld, %ld, %ld, %lld, \'%@\', \'%@\', \'%@\', %ld)", masterId, slaveId, userType, academyType, schoolId, imageUrl, firstName, lastName, gender];
    
    [self.dbManager executeQuery:query];
    
    if([self.dbManager affectedRows] != 0) {
        return YES;
    }
    else {
        return NO;
    }
}

- (BOOL)updateImageUrlWithSlaveId:(long long)slaveId imageUrl:(NSString *)imageUrl {
    NSString *query = [NSString stringWithFormat:@"update user_info set image_url=%@ where slave_id=%lld", imageUrl, slaveId];
    
    return [self.dbManager executeQuery:query];
}

- (void)deleteUserWithMasterId:(long long)masterId slaveId:(long long)slaveId {
    NSString *query = [NSString stringWithFormat:@"delete from user_info where master_id=%lld and slave_id=%lld", masterId, slaveId];
    
    [self.dbManager executeQuery:query];
}

- (NSArray<MultipleUserModel *> *)getUserDataWithMasterId:(long long)masterId {
    
    NSString *query = [NSString stringWithFormat:@"select * from user_info where master_id=%lld", masterId];
    NSArray *results = [self.dbManager loadDataFromDB:query];
    
    // Get column index
    NSInteger masterIdIndex = [self.dbManager.arrColumnNames indexOfObject:@"master_id"];
    NSInteger slaveIdIndex = [self.dbManager.arrColumnNames indexOfObject:@"slave_id"];
    NSInteger userTypeIndex = [self.dbManager.arrColumnNames indexOfObject:@"user_type"];
    NSInteger academyTypeIndex = [self.dbManager.arrColumnNames indexOfObject:@"academy_type"];
    NSInteger schoolIdIndex = [self.dbManager.arrColumnNames indexOfObject:@"school_id"];
    NSInteger imageUrlIndex = [self.dbManager.arrColumnNames indexOfObject:@"image_url"];
    NSInteger firstNameIndex = [self.dbManager.arrColumnNames indexOfObject:@"firstname"];
    NSInteger lastNameIndex = [self.dbManager.arrColumnNames indexOfObject:@"lastname"];
    NSInteger genderIndex = [self.dbManager.arrColumnNames indexOfObject:@"gender"];
    
    NSMutableArray *returnedArray = [[NSMutableArray alloc] init];
    
    if(results != nil) {
        for(int i=0; i<results.count; i++) {
            NSArray *rowArray = [results objectAtIndex:i];
            
            long long masterId = [[rowArray objectAtIndex:masterIdIndex] longLongValue];
            long long slaveId = [[rowArray objectAtIndex:slaveIdIndex] longLongValue];
            NSInteger userType = [[rowArray objectAtIndex:userTypeIndex] integerValue];
            NSInteger academyType = [[rowArray objectAtIndex:academyTypeIndex] integerValue];
            long long schoolId = [[rowArray objectAtIndex:schoolIdIndex] longLongValue];
            NSString *imageUrl = [rowArray objectAtIndex:imageUrlIndex];
            NSString *firstName = [rowArray objectAtIndex:firstNameIndex];
            NSString *lastName = [rowArray objectAtIndex:lastNameIndex];
            NSInteger gender = [[rowArray objectAtIndex:genderIndex] integerValue];
            
            MultipleUserModel *model = [[MultipleUserModel alloc] init];
            [model setMasterId:masterId];
            [model setSlaveId:slaveId];
            [model setUserType:userType];
            [model setAcademyType:academyType];
            [model setSchoolId:schoolId];
            [model setImageUrl:imageUrl];
            [model setFirstName:firstName];
            [model setLastName:lastName];
            [model setGender:gender];
            
            [returnedArray addObject:model];
        }
    }
    
    return returnedArray;
}

- (BOOL)isMasterIdExistsInDBWithMasterId:(long long)masterId slaveId:(long long)slaveId {
    NSString *query = [[NSString alloc] initWithFormat:@"select count(*) from user_info where master_id=%lld and slave_id=%lld", masterId, slaveId];
    
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    int count = [[[results objectAtIndex:0] objectAtIndex:0] intValue];
    
    if(count == 0) {
        return NO;
    } else {
        return YES;
    }
}

@end
