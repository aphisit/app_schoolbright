//
//  EXReportClassStatusModel.m
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "EXReportClassStatusModel.h"

@implementation EXReportClassStatusModel

@synthesize iid = _iid;
@synthesize title = _title;
@synthesize numberOfOnTimeStatus = _numberOfOnTimeStatus;
@synthesize numberOfLateStatus = _numberOfLateStatus;
@synthesize numberOfAbsenceStatus = _numberOfAbsenceStatus;

- (void)setIId:(long long)iid {
    _iid = iid;
}

- (void)setTitle:(NSString *)title {
    _title = title;
}

- (void)setNumberOfOnTimeStatus:(NSInteger)numberOfOnTimeStatus {
    _numberOfOnTimeStatus = numberOfOnTimeStatus;
}

- (void)setNumberOfLateStatus:(NSInteger)numberOfLateStatus {
    _numberOfLateStatus = numberOfLateStatus;
}

- (void)setNumberOfAbsenceStatus:(NSInteger)numberOfAbsenceStatus {
    _numberOfAbsenceStatus = numberOfAbsenceStatus;
}

- (long long)getIId {
    return _iid;
}

- (NSString *)getTitle {
    return _title;
}

- (NSInteger)getNumberOfOnTimeStatus {
    return _numberOfOnTimeStatus;
}

- (NSInteger)getNumberOfLateStatus {
    return _numberOfLateStatus;
}

- (NSInteger)getNumberOfAbsenceStatus {
    return _numberOfAbsenceStatus;
}

@end
