//
//  SNClassRoomIndividualViewController.m
//  JabjaiApp
//
//  Created by toffee on 9/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNClassRoomIndividualViewController.h"
#import "BSSelectStudentsViewController.h"
#import "SNClassLevelGroupViewController.h"
#import "BSSelectBehaviorScoreViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "Constant.h"


@interface SNClassRoomIndividualViewController (){
    UIColor *greenBGColor;
    UIColor *redBGColor;
    
    BOOL optionStatusSelectAll;
    
    // The dialog
    AlertDialog *alertDialog;
}
@property (strong, nonatomic) CallGetStudentInClassroomAPI *callGetStudentInClassroomAPI;
@end
static NSString *cellIdentifier = @"Cell";
@implementation SNClassRoomIndividualViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    // Do any additional setup after loading the view.
    
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSStudentNameRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    greenBGColor = [UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0];
     self.titleLabel.text = [[self.classroomArray objectAtIndex:self.selectedClassroomIndex] getClassroomName];
    
    if(self.selectedStudentArray == nil) {
        [self getStudentInClassroom];
    }
    
    //[self checkOptionButton];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - API Caller

- (void)getStudentInClassroom {
    
    if(self.callGetStudentInClassroomAPI != nil) {
        self.callGetStudentInClassroomAPI = nil;
    }
    
    self.callGetStudentInClassroomAPI = [[CallGetStudentInClassroomAPI alloc] init];
    self.callGetStudentInClassroomAPI.delegate = self;
    [self.callGetStudentInClassroomAPI call:[UserData getSchoolId] classroomId:_classroomId];
}
#pragma mark - API Delegate

- (void)callGetStudentInClassrommAPI:(CallGetStudentInClassroomAPI *)classObj data:(NSArray<BSSelectedStudent *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _selectedStudentArray = data;
        
        [self.tableView reloadData];
        //[self checkOptionButton];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.selectedStudentArray != nil) {
        return self.selectedStudentArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BSStudentNameRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    BSSelectedStudent *model = [self.selectedStudentArray objectAtIndex:indexPath.row];
    
    cell.delegate = self;
    cell.index = indexPath.row;
    cell.titleLabel.text = [NSString stringWithFormat:@"%li. %@", (indexPath.row + 1), [model getStudentName]];
    
    if([model isSelected]) {
        NSLog(@"Mode = %d",self.mode);
        [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
    }
    else {
        [cell setRadioButtonClearSelected];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}


#pragma mark - BSRadionTableViewCellDelegate

- (void)onPressRadioButton:(BSStudentNameRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index{
    
    
    BOOL isSelected = [[self.selectedStudentArray objectAtIndex:index] isSelected];
    [[self.selectedStudentArray objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}





- (IBAction)actionNext:(id)sender {
}

- (IBAction)moveBack:(id)sender {

     SNClassLevelGroupViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNClassLevelGroupStoryboard"];
     [self.revealViewController pushFrontViewController:viewController animated:YES];
}
@end
