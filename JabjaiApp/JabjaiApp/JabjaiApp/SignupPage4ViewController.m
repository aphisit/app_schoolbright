//
//  SignupPage4ViewController.m
//  JabjaiApp
//
//  Created by mac on 10/28/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SignupPage4ViewController.h"
#import "SignupPage3ViewController.h"
#import "LoginViewController.h"
#import "Utils.h"
#import "APIURL.h"
#import "IDValueModel.h"
#import "AlertDialog.h"
#import "Constant.h"

@interface SignupPage4ViewController () {
    NSMutableArray *provinceArr;
    NSMutableArray *districtArr;
    NSMutableArray *subDistrictArr;
    
    // The dialog
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    CaptureScreenCodeDialog *captureScreenCodeDialog;
    
    NSInteger connectCounter;
}

@property (assign, nonatomic) NSInteger provinceID;
@property (assign, nonatomic) NSInteger districtID;
@property (assign, nonatomic) NSInteger subDistrictID;

@end

static NSString *provinceDialogRequestCode = @"provinceDialogRequestCode";
static NSString *districtDialogRequestCode = @"districtDialogRequestCode";
static NSString *subDistrictDialogRequestCode = @"subDistrictDialogRequestCode";

@implementation SignupPage4ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.addressTextField.delegate = self;
    self.provinceTextField.delegate = self;
    self.districtTextField.delegate = self;
    self.subDistrictTextField.delegate = self;
    
    alertDialog = [[AlertDialog alloc] init];
    captureScreenCodeDialog = [[CaptureScreenCodeDialog alloc] init];
    captureScreenCodeDialog.delegate = self;
    
    // Restore data to all text fields
    if(_signupDataModel.address != nil) {
        self.addressTextField.text = _signupDataModel.address;
    }
    
    if(_signupDataModel.province != nil) {
        self.provinceTextField.text = _signupDataModel.province;
        self.provinceID = [_signupDataModel.provinceID integerValue];
    }
    
    if(_signupDataModel.district != nil) {
        self.districtTextField.text = _signupDataModel.district;
        self.districtID = [_signupDataModel.districtID integerValue];
    }
    
    if(_signupDataModel.subDistrict != nil) {
        self.subDistrictTextField.text = _signupDataModel.subDistrict;
        self.subDistrictID = [_signupDataModel.subDistrictID integerValue];
    }
    
    connectCounter = 0;
    
    // Docorate the views
    self.signupBtn.layer.cornerRadius = 20;
    self.signupBtn.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"signupPage3Segue"]) {
        SignupPage3ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
        controller.previousSegue = _previousSegue;
    }
}

- (IBAction)moveBack:(id)sender {
    
    [self performSegueWithIdentifier:@"signupPage3Segue" sender:self];
    
}

- (IBAction)dismissKeyboard:(id)sender {
    [self resignFirstResponder];
}

- (IBAction)signupAction:(id)sender {
    
    BOOL isDataValidate = [self validateData];
    
    if(isDataValidate) {
        NSMutableString *address = [[NSMutableString alloc] initWithString:self.addressTextField.text];
        CFStringTrimWhitespace((__bridge CFMutableStringRef) address);
        
        NSString *province = self.provinceTextField.text;
        NSString *district = self.districtTextField.text;
        NSString *subDistrict = self.subDistrictTextField.text;
        
        self.signupDataModel.address = address;
        self.signupDataModel.province = province;
        self.signupDataModel.provinceID = [[NSNumber alloc] initWithInteger:self.provinceID];
        self.signupDataModel.district = district;
        self.signupDataModel.districtID = [[NSNumber alloc] initWithInteger:self.districtID];
        self.signupDataModel.subDistrict = subDistrict;
        self.signupDataModel.subDistrictID = [[NSNumber alloc] initWithInteger:self.subDistrictID];
        
        [self applyData];
    }
    
}

#pragma mark - Individual Functions
-(BOOL)validateData {
    
    NSString *dialogTitle = @"แจ้งเตือน";
    
    if([self.addressTextField.text length] == 0 || [self.provinceTextField.text length] == 0 || [self.districtTextField.text length] == 0 || [self.subDistrictTextField.text length] == 0) {
        
        NSString *alertMessage = @"กรุณากรอกข้อมูลให้ครบถ้วน";
        [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        
        return NO;
        
    }
    return YES;
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    NSString *dialogTitle = @"แจ้งเตือน";
    
    if(textField.tag == 2) { //province text field
        
        if(tableListDialog == nil) {
            [self getProvinceInfo];
        }
        else if(tableListDialog != nil && ![tableListDialog isDialogShowing]) {
            [self getProvinceInfo];
        }
        
        return NO;
    }
    else if(textField.tag == 3) {
        
        if([self.provinceTextField.text length] == 0) {
            NSString *alertMessage = @"กรุณาเลือกจังหวัด";
            [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        }
        else {
            
            if(tableListDialog == nil) {
                [self getDistrictInfo];
            }
            else if(tableListDialog != nil && ![tableListDialog isDialogShowing]) {
                [self getDistrictInfo];
            }
            
        }
        
        return NO;
    }
    else if(textField.tag == 4) {
        
        if([self.provinceTextField.text length] == 0) {
            NSString *alertMessage = @"กรุณาเลือกจังหวัด";
            [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        }
        else if([self.districtTextField.text length] == 0) {
            NSString *alertMessage = @"กรุณาเลือกอำเภอ";
            [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        }
        else {
            
            if(tableListDialog == nil) {
                [self getSubDistrictInfo];
            }
            else if(tableListDialog != nil && ![tableListDialog isDialogShowing]) {
                [self getSubDistrictInfo];
            }
            
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - GetAPIData

- (void)getProvinceInfo {
    NSString *URLString = [APIURL getProvinceURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getProvinceInfo];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getProvinceInfo];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getProvinceInfo];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                provinceArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSNumber *ID = [[NSNumber alloc] initWithInt:(int)[[dataDict objectForKey:@"PROVINCE_ID"] integerValue]];
                    NSMutableString *Value = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"PROVINCE_NAME"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) Value);
                    
                    IDValueModel *model = [[IDValueModel alloc] init];
                    model.ID = ID;
                    model.Value = Value;
                    
                    [provinceArr addObject:model];
                    [dataArr addObject:Value];
                }
                
                // Create NSArray data to show in dialog
                [self showTableListDialogWithRequestCode:provinceDialogRequestCode data:dataArr];
                
            }
        }
        
    }];
    
}

- (void)getDistrictInfo {
    NSString *URLString = [APIURL getDistrictURL:self.provinceID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDistrictInfo];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDistrictInfo];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDistrictInfo];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                districtArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSNumber *ID = [[NSNumber alloc] initWithInt:(int)[[dataDict objectForKey:@"AMPHUR_ID"] integerValue]];
                    NSMutableString *Value = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"AMPHUR_NAME"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) Value);
                    
                    IDValueModel *model = [[IDValueModel alloc] init];
                    model.ID = ID;
                    model.Value = Value;
                    
                    [districtArr addObject:model];
                    [dataArr addObject:Value];
                }
                
                // Create NSArray data to show in dialog
                [self showTableListDialogWithRequestCode:districtDialogRequestCode data:dataArr];
            
            }
            
        }
    }];
    
}

- (void)getSubDistrictInfo {
    NSString *URLString = [APIURL getSubDistrictURL:self.districtID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSubDistrictInfo];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubDistrictInfo];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubDistrictInfo];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                subDistrictArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSNumber *ID = [[NSNumber alloc] initWithInt:(int)[[dataDict objectForKey:@"DISTRICT_ID"] integerValue]];
                    NSMutableString *Value = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"DISTRICT_NAME"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) Value);
                    
                    IDValueModel *model = [[IDValueModel alloc] init];
                    model.ID = ID;
                    model.Value = Value;
                    
                    [subDistrictArr addObject:model];
                    [dataArr addObject:Value];
                }
                
                // Create NSArray data to show in dialog
                [self showTableListDialogWithRequestCode:subDistrictDialogRequestCode data:dataArr];
                
            }
            
        }
    }];
    
}

-(void)applyData {
    
    if(_signupDataModel.creditLimit == nil || _signupDataModel.creditLimit.length == 0) {
        _signupDataModel.creditLimit = @"0";
    }
    
    NSInteger schoolID = [_signupDataModel.schoolID integerValue];
    NSInteger type = [_signupDataModel.type integerValue];
    NSInteger gender = [_signupDataModel.gender integerValue];
    NSString *firstName = _signupDataModel.firstName;
    NSString *lastName = _signupDataModel.lastName;
    NSString *studentID = _signupDataModel.studentID;
    NSDate *birthday = _signupDataModel.birthDay;
    NSString *phoneNumber = _signupDataModel.phoneNumber;
    NSString *email = _signupDataModel.email;
    NSInteger creditLimits = [_signupDataModel.creditLimit integerValue];
    NSString *address = _signupDataModel.address;
    NSInteger provinceID = [_signupDataModel.provinceID integerValue];
    NSInteger districtID = [_signupDataModel.districtID integerValue];
    NSInteger subDistrictID = [_signupDataModel.subDistrictID integerValue];
    NSString *personalID;
    
    if(self.signupDataModel.type.intValue != 0) { // Not a student
        personalID = self.signupDataModel.phoneNumber;
    }
    else {
        personalID = self.signupDataModel.studentID;
    }
    
    // For School
    NSInteger classroomID = [_signupDataModel.classroomID integerValue];
    
    // For University
    NSInteger majorID = [_signupDataModel.majorID integerValue];
    
    if([_signupDataModel.type intValue] != 0) { //If user is not student
        studentID = @"";
    }
    
    if(type != 0) { //If type not equals zero it's a teacher or staff.
        studentID = @"";
        classroomID = 0;
    }
    
    NSString *URLString;
    
    if([_signupDataModel.academyType intValue] == 1) { // School
        
        URLString = [APIURL getSignupURLWithSchoolID:schoolID classroomID:classroomID type:type gender:gender firstName:firstName lastName:lastName personalID:personalID studentID:studentID birthday:birthday phoneNumber:phoneNumber email:email creditLimits:creditLimits address:address provinceID:provinceID districtID:districtID subDistrictID:subDistrictID];
 
    }
    else if([_signupDataModel.academyType intValue] == 2 ) { // University
        URLString = [APIURL getSignupURLWithSchoolID:schoolID classroomID:majorID type:type gender:gender firstName:firstName lastName:lastName personalID:personalID studentID:studentID birthday:birthday phoneNumber:phoneNumber email:email creditLimits:creditLimits address:address provinceID:provinceID districtID:districtID subDistrictID:subDistrictID];
    }
    
    NSURL *url = [[NSURL alloc] initWithString:URLString];
    
    NSLog(@"%@", URLString);
    
    [self signupToServerWithURL:url];
    
}

- (void)signupToServerWithURL:(NSURL *)url {
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self signupToServerWithURL:url];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            connectCounter = 0;
            
            NSString *dialogTitle = @"แจ้งเตือน";
            NSString *resultString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSMutableString *strResult = [[NSMutableString alloc] initWithString:resultString];
            [strResult replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [strResult length])];
            CFStringTrimWhitespace((__bridge CFMutableStringRef) strResult);
            
            NSLog(@"Signup Result: %@", strResult);
            
            if(![Utils isInteger:strResult]) {
                NSString *alertMessage = @"เกิดข้อผิดพลาดในการทำรายการ โปรดลองใหม่";
                [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
            }
            else if([strResult intValue] == 0) {
                NSString *alertMessage = @"เลขประจำตัวประชาชนนี้ถูกใช้งานแล้ว";
                [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
            }
            else {
                [self showCaptureScreenCodeDialogWithTitle:dialogTitle verifyCode:strResult];
            }
            
        }
    }];
    
}

#pragma mark - TableListDialogDelegate

-(void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:provinceDialogRequestCode]) {
        
        self.provinceTextField.text = [[provinceArr objectAtIndex:index] getVal];
        self.provinceID = [[[provinceArr objectAtIndex:index] getID] integerValue];
        
        // Clear dependency text field value
        self.districtTextField.text = @"";
        self.subDistrictTextField.text = @"";
        
    }
    else if([requestCode isEqualToString:districtDialogRequestCode]) {
        
        self.districtTextField.text = [[districtArr objectAtIndex:index] getVal];
        self.districtID = [[[districtArr objectAtIndex:index] getID] integerValue];
        
        // Clear dependency text field value
        self.subDistrictTextField.text = @"";
        
    }
    else if([requestCode isEqualToString:subDistrictDialogRequestCode]) {
        
        self.subDistrictTextField.text = [[subDistrictArr objectAtIndex:index] getVal];
        self.subDistrictID = [[[subDistrictArr objectAtIndex:index] getID] integerValue];
    }
}

#pragma mark - CaptureScreenCodeDialogDelegate
-(void)onPressCaptureScreen {
    //[self performSegueWithIdentifier:@"loginSegue" sender:self];
    [self performSegueWithIdentifier:@"loginSegue" sender:self];
}

-(void)onDismissCaptureDialog {
    [self performSegueWithIdentifier:@"loginSegue" sender:self];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showCaptureScreenCodeDialogWithTitle:(NSString *)title verifyCode:(NSString *)verifyCode {
    
    if(captureScreenCodeDialog != nil && [captureScreenCodeDialog isDialogShowing]) {
        [captureScreenCodeDialog dismissDialog];
    }
    
    [captureScreenCodeDialog showDialogInView:self.view title:title verifyCode:verifyCode];
}

@end
