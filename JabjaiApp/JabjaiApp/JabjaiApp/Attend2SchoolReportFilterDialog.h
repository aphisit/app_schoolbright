//
//  Attend2SchoolReportFilterDialog.h
//  JabjaiApp
//
//  Created by mac on 12/16/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MKDropdownMenu/MKDropdownMenu.h>
#import "CalendarDialog.h"
#import "YearTermModel.h"

@protocol Attend2SchoolReportFilterDialogDelegate

- (void)applyFilter:(NSNumber *)schoolYear semester:(NSString *)semester status:(NSString *)status startDate:(NSDate *)startDate endDate:(NSDate *)endDate;

@end

@interface Attend2SchoolReportFilterDialog : UIViewController <MKDropdownMenuDataSource, MKDropdownMenuDelegate, UITextFieldDelegate, CalendarDialogDelegate>

@property (nonatomic, retain) id<Attend2SchoolReportFilterDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet MKDropdownMenu *schoolYearDropdown;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *semesterDropdown;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *statusDropdown;
@property (weak, nonatomic) IBOutlet UITextField *startDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *endDateTextField;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

- (IBAction)onPressCancel:(id)sender;
- (IBAction)onPressOK:(id)sender;

- (id)initWithYearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict;
- (void)YearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
