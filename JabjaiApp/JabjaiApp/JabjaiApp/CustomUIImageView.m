//
//  CustomUIImageView.m
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CustomUIImageView.h"
#import <objc/runtime.h>

static char URL_KEY;

@implementation CustomUIImageView

@dynamic imageURL;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)loadImageFromURL:(NSString *)url {
    self.imageURL = url;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        NSURL *contentUrl = [NSURL URLWithString:self.imageURL];
        NSData *data = [NSData dataWithContentsOfURL:contentUrl];
        UIImage *imageFromData = [UIImage imageWithData:data];
        
        if(imageFromData) {
            if([self.imageURL isEqualToString:url]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.image = imageFromData;
                });
            }
        }
    });
}

- (void)setImageURL:(NSString *)imageURL {
    objc_setAssociatedObject(self, &URL_KEY, imageURL, OBJC_ASSOCIATION_COPY);
}

- (NSString *)imageURL {
    return objc_getAssociatedObject(self, &URL_KEY);
}

@end
