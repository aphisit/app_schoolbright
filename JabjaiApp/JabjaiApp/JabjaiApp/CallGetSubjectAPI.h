//
//  CallGetSubjectAPI.h
//  JabjaiApp
//
//  Created by mac on 7/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSubjectModel.h"

@class CallGetSubjectAPI;

@protocol CallGetSubjectAPIDelegate <NSObject>

@optional
- (void)callGetSubjectAPI:(CallGetSubjectAPI *)classObj data:(NSArray<SSubjectModel *> *)data success:(BOOL)success;
- (void)notArrangeTimeTable:(CallGetSubjectAPI *)classObj;

@end

@interface CallGetSubjectAPI : NSObject

@property (nonatomic, weak) id<CallGetSubjectAPIDelegate> delegate;

- (void)call:(long long)schoolId classroomId:(long long)classroomId;

@end
