//
//  StaffSignupPage1ViewController.h
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupDataModel.h"
#import "DLRadioButton.h"

@interface StaffSignupPage1ViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) SignupDataModel *signupDataModel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *schoolTextField;
@property (weak, nonatomic) IBOutlet DLRadioButton *typeRadioButton;
@property (weak, nonatomic) IBOutlet DLRadioButton *genderRadioButton;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@property (weak, nonatomic) IBOutlet UILabel *academyLabel;

- (IBAction)moveBack:(id)sender;
- (IBAction)nextStep:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;

@end
