//
//  APIURL.h
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserData.h"

@interface APIURL : NSObject

+(NSString *)getLoginURL:(NSString *)username password:(NSString *)password;
+(NSString *)getSchoolDataWithPasswordURL:(NSString *)password;
+(NSString *)getSchoolDegree:(NSInteger)schoolID;
+(NSString *)getSchoolGrad:(NSInteger)schoolID degreeID:(NSInteger)degreeID;
+(NSString *)getSchoolClassroom:(NSInteger)schoolID gradID:(NSInteger)gradID;
+(NSString *)getProvinceURL;
+(NSString *)getDistrictURL:(NSInteger)provinceID;
+(NSString *)getSubDistrictURL:(NSInteger)districtID;
+(NSString *)getSignupURLWithSchoolID:(NSInteger)schoolID classroomID:(NSInteger)classroomID type:(NSInteger)type gender:(NSInteger)gender firstName:(NSString *)firstName lastName:(NSString *)lastName personalID:(NSString *)personalID studentID:(NSString *)studentID birthday:(NSDate *)birthday phoneNumber:(NSString *)phoneNumber email:(NSString *)email creditLimits:(NSInteger)creditLimits address:(NSString *)address provinceID:(NSInteger)provinceID districtID:(NSInteger)districtID subDistrictID:(NSInteger)subDistrictID;
+(NSString *)getCreditBureauSummaryURL;
+(NSString *)getYearAndTermURL;
+(NSString *)getAttendSchoolDataURLWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status;
+(NSString *)getMessageInboxURLWithLatestID:(NSInteger)latestID;
+ (NSString *)getMessageInboxURLWithLatestID:(NSInteger)latestID messageType:(NSInteger)messageType;
+ (NSString *)getAllMessageWithPage:(NSUInteger)page userID:(long long)userID;
+ (NSString *)getNewsMessageWithPage:(NSUInteger)page userID:(long long)userID;
+(NSString *)getUpdateReadMessageURLWithMessageID:(long long)messageID userID:(long long)userID;
+(NSString *)getChangeFingerprintRequestURL;
+(NSString *)getUserCredentialURL;
+ (NSString *)getStudentScheduleListURLWithUserID:(long long)userID date:(NSDate *)date;
+ (NSString *)getStudentScheduleListDetailURLWithUserID:(long long)userID date:(NSDate *)date subjectID:(long long)subjectID;
+ (NSString *)getBoughtItemWithUserID:(long long)userID date:(NSDate *)date;
+ (NSString *)getTopupMessageWithUserID:(long long)userID date:(NSDate *)date;
+ (NSString *)getUnreadMessageCountWithUserID:(long long)userID;
+ (NSString *)getFeedBackPOSTURL;

// Take Class Attendance
+ (NSString *)getStudentClassLevelWithSchoolId:(long long)schoolId;
+ (NSString *)getSchoolClassroomWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId;
+ (NSString *)getSubjectWithSchoolId:(long long)schoolId classroomId:(long long)classroomId;
+ (NSString *)getStudentInClassWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId;
+ (NSString *)getPreviousAttendClassStatusWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId;
+ (NSString *)getUpdateStudentStatusPOSTWithSchoolId:(long long)schoolId subjectId:(long long)subjectId teacherId:(long long)teacherId;
+ (NSString *)getTASubjectListWithSchoolId:(long long)schoolId classroomId:(long long)classroomId;
+ (NSString *)getTAHistoryStudentStatusListWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId date:(NSDate *)date;
+ (NSString *)getTAHistoryDateStatusWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId startDate:(NSDate *)startDate endDate:(NSDate *)endDate;

// Behavior Score
+ (NSString *)getStudentInClassroomWithSchoolId:(long long)schoolId classroomId:(long long)classroomId;
+ (NSString *)getAddBehaviorScoreListWithSchoolId:(long long)schoolId;
+ (NSString *)getReduceBehaviorScoreListWithSchoolId:(long long)schoolId;
+ (NSString *)getUpdateStudentBehaviorScorePOSTWithSchoolId:(long long)schoolId teacherId:(long long)teacherId;
+ (NSString *)getBehaviorScoreHistoryInDateRangeWithSchoolId:(long long)schoolId classroomId:(long long)classroomId startDate:(NSDate *)startDate endDate:(NSDate *)endDate type:(NSInteger)type;
+ (NSString *)getBehaviroScoreHistoryForStudentReportWithUserId:(long long)userId;

// Executive report
+ (NSString *)getEXReportSummaryAttendSchoolWithSchoolId:(long long)schoolId date:(NSDate *)date;
+ (NSString *)getEXReportClassroomAttendSchoolWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId date:(NSDate *)date;
+ (NSString *)getEXReportAttendSchoolTeacherListWithSchoolId:(long long)schoolId date:(NSDate *)date;
+ (NSString *)getEXReportAttendSchoolEmployeeListWithSchoolId:(long long)schoolId date:(NSDate *)date;

+(NSString *)getUniversityFacultyURL:(NSInteger)schoolID;
+(NSString *)getUniversityDegreeURL:(NSInteger)schoolID facultyID:(NSInteger)facultyID;
+(NSString *)getUniversityMajorURL:(NSInteger)schoolID degreeID:(NSInteger)degreeID;
+(NSString *)getStudentEnrollSubjectURL;
+(NSString *)getAddSectionUrl:(NSInteger)sectionID;
+(NSString *)getDeleteSectionUrl:(NSInteger)sectionID;
+(NSString *)getSubjectAddedUrl;
+(NSString *)getTeachingSubjectUrl;
+(NSString *)getStudentsInSectionUrl:(NSInteger)sectionID;
+(NSString *)getStudentSubjectListAllYearUrl;
+(NSString *)getAttendToSectionHistoryWithSectionID:(NSInteger)sectionID startDate:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status userType:(USERTYPE)userType;
+(NSString *)getTheacherAllUrl:(NSInteger)schoolId;

@end
