//
//  CallGetSchoolClassLevel.h
//  JabjaiApp
//
//  Created by mac on 7/4/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SchoolLevelModel.h"

@class CallGetSchoolClassLevelAPI;

@protocol CallGetSchoolClassLevelAPIDelegate <NSObject>

@optional
- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success;

@end

@interface CallGetSchoolClassLevelAPI : NSObject

@property (nonatomic, weak) id<CallGetSchoolClassLevelAPIDelegate> delegate;

- (void)call:(long long)schoolId;

@end
