//
//  SNSelectTheaCherViewController.m
//  JabjaiApp
//
//  Created by toffee on 9/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNSelectTheaCherViewController.h"
#import "APIURL.h"
#import "UserData.h"
#import "Utils.h"

@interface SNSelectTheaCherViewController (){
    NSMutableArray<TheacherAllModel *> *theacherAll;
}

@end
static NSString *cellIdentifier = @"Cell";
NSMutableArray *addTheacherArrray;
@implementation SNSelectTheaCherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view.
    
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSStudentNameRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];

    self.doTheacher;
  
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    // Do any additional setup after loading the view.
}

-(void)doTheacher{
    
    NSInteger schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getTheacherAllUrl:schoolID] ;
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        NSArray *returnedArray = returnedData;
        addTheacherArrray = [[NSMutableArray alloc] init];
        for(int i=0; i<returnedArray.count; i++) {
            NSDictionary *dataDict = [returnedArray objectAtIndex:i];
            
            long long theacherId = [[dataDict objectForKey:@"emplyoeesid"] longLongValue];
            
            NSMutableString *theacherName;
            
            if(![[dataDict objectForKey:@"emplyoeesname"] isKindOfClass:[NSNull class]]) {
                theacherName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"emplyoeesname"]];
            }
            else {
                theacherName = [[NSMutableString alloc] initWithString:@""];
            }
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) theacherName);
            
            TheacherAllModel *model = [[TheacherAllModel alloc] init];
            model.theaCherId = theacherId;
            model.theaCherName = theacherName;
            NSLog(@"NAME = %@",theacherName);
            //TheacherAllModel *model1 = [returnedArray objectAtIndex:i];
            [addTheacherArrray addObject:theacherName ];
            
        }
        
        [self.tableView reloadData];
    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"count = %d",addTheacherArrray.count);
    if(addTheacherArrray != nil) {
        
        return addTheacherArrray.count;
    }
    else {
        self.doTheacher;
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BSStudentNameRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    //BSSelectedStudent *model = [theacherAll objectAtIndex:indexPath.row];
    
    cell.delegate = self;
    cell.index = indexPath.row;
    cell.titleLabel.text = @"xx";
    
//    if([model isSelected]) {
//       // NSLog(@"Mode = %d",self.mode);
//        [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
//    }
//    else {
//        [cell setRadioButtonClearSelected];
//    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}


#pragma mark - BSRadionTableViewCellDelegate

//- (void)onPressRadioButton:(BSStudentNameRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index{
//    
//    
//    BOOL isSelected = [[theacherAll objectAtIndex:index] isSelected];
//    [[theacherAll objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
//    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
//    
//    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
