//
//  FeedbackViewController.h
//  JabjaiApp
//
//  Created by mac on 5/18/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertDialog.h"

@interface FeedbackViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, AlertDialogDelegate>

@property (weak, nonatomic) IBOutlet UITextView *feedbackTextView;
@property (weak, nonatomic) IBOutlet UIButton *uploadPictureButton;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

- (IBAction)moveBack:(id)sender;
- (IBAction)uploadPicture:(id)sender;
- (IBAction)submitFeedback:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;

@end
