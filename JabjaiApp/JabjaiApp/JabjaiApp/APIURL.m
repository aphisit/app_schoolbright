//
//  APIURL.m
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "APIURL.h"
#import "Utils.h"

@interface APIURL ()

@end

@implementation APIURL

#pragma mark - School

+(NSString *)getLoginURL:(NSString *)username password:(NSString *)password {
    //NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/login?user=%@&pass=%@", username, password];
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/login?user=%@&pass=%@", username, password];
    return url;
}

+(NSString *)getSchoolDataWithPasswordURL:(NSString *)password {
    //NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/schoolpass?password=%@", password];
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/schoolpass?password=%@", password];
    return url;
}

+(NSString *)getSchoolDegree:(NSInteger)schoolID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/level?schoolid=%i", (int)schoolID];
    return url;
}

+(NSString *)getSchoolGrad:(NSInteger)schoolID degreeID:(NSInteger)degreeID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/sublevel?schoolid=%i&levelid=%i", (int)schoolID, (int)degreeID];
    return url;
}

+(NSString *)getSchoolClassroom:(NSInteger)schoolID gradID:(NSInteger)gradID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/sublevel2?schoolid=%i&sublevelid=%i", (int)schoolID, (int)gradID];
    return url;
}

+(NSString *)getProvinceURL {
    NSString *url = @"http://beta02api.jabjai.school/api/Province";
    return url;
}

+(NSString *)getDistrictURL:(NSInteger)provinceID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/amphur/%i", (int)provinceID];
    return url;
}

+(NSString *)getSubDistrictURL:(NSInteger)districtID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/district/%i", (int)districtID];
    return url;
}

+(NSString *)getSignupURLWithSchoolID:(NSInteger)schoolID classroomID:(NSInteger)classroomID type:(NSInteger)type gender:(NSInteger)gender firstName:(NSString *)firstName lastName:(NSString *)lastName personalID:(NSString *)personalID studentID:(NSString *)studentID birthday:(NSDate *)birthday phoneNumber:(NSString *)phoneNumber email:(NSString *)email creditLimits:(NSInteger)creditLimits address:(NSString *)address provinceID:(NSInteger)provinceID districtID:(NSInteger)districtID subDistrictID:(NSInteger)subDistrictID {
    
    NSString *baseURL = @"http://beta02api.jabjai.school/api/User?";
    
    NSDateFormatter *formatter = [Utils getDateFormatter];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *strBirthday = [formatter stringFromDate:birthday];
    
    NSString *query = [NSString stringWithFormat:@"nCompany=%i&nTermSubLevel2=%i&cType=%i&csex=%i&sname=%@&slastname=%@&sidentification=%@&studentID=%@&dBirth=%@&sPhone=%@&sEmail=%@&nMax=%i&address=%@&PROVINCE_ID=%i&AMPHUR_ID=%i&DISTRICT_ID=%i", (int)schoolID, (int)classroomID, (int)type, (int)gender, firstName, lastName, personalID, studentID, strBirthday, phoneNumber, email, (int)creditLimits, address, (int)provinceID, (int)districtID, (int)subDistrictID];
    
    NSString *encodedQuery = [Utils encodeString:query];
    NSString *url = [[NSString alloc] initWithFormat:@"%@%@", baseURL, encodedQuery];
    
    return url;
}

+(NSString *)getCreditBureauSummaryURL {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/Reportmobile03?tremid=&userid=%i", (int)[UserData getUserID]];
    return url;
}

+(NSString *)getYearAndTermURL {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/GetYear?userid=%i", (int)[UserData getUserID]];
    return url;
}

//status 0 : On time, 1 : Late, 3 : Absence
+(NSString *)getAttendSchoolDataURLWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status {
    
    NSString *serverStartDateStr = [Utils dateToServerDateFormat:startDate];
    NSString *serverEndDateStr = [Utils dateToServerDateFormat:endDate];
    
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/come2school?userid=%i&dstart=%@&dend=%@", (int)[UserData getUserID], serverStartDateStr, serverEndDateStr];
    NSMutableString *mutableURL = [[NSMutableString alloc] initWithString:url];
    
    if(status >= 0) {
        [mutableURL appendString:[NSString stringWithFormat:@"&status=%i", (int)status]];
    }
    else {
        [mutableURL appendString:@"&status="];
    }
    
    return mutableURL;
}

+(NSString *)getMessageInboxURLWithLatestID:(NSInteger)latestID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/message?userid=%i&&latestid=%i", (int)[UserData getUserID], (int)latestID];
    return url;
}

+ (NSString *)getMessageInboxURLWithLatestID:(NSInteger)latestID messageType:(NSInteger)messageType {
    //http://beta02api.azurewebsites.net/api/Message?userid=85&latestid=0&typeid=1
    NSString *url = [NSString stringWithFormat:@"http://beta02api.azurewebsites.net/api/Message?userid=%i&latestid=%i&typeid=%i", (int)[UserData getUserID], (int)latestID, (int)messageType];
    
    return url;
}

+ (NSString *)getAllMessageWithPage:(NSUInteger)page userID:(long long)userID {
    //http://beta02api.jabjai.school/api/messagebox/getMessage/85/3
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/messagebox/getMessage/%lld/%lu", userID, page];
    
    return url;
}
+ (NSString *)getNewsMessageWithPage:(NSUInteger)page userID:(long long)userID {
    //http://beta02api.jabjai.school/api/messagebox/getMessage/news/85/1
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/messagebox/getMessage/news/%lld/%lu", userID, page];
    
    return url;
}

+ (NSString *)getUpdateReadMessageURLWithMessageID:(long long)messageID userID:(long long)userID {
    //NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/message?userid=%i&messageid=%i", (int)[UserData getUserID], (int)messageID];

    //http://beta02api.azurewebsites.net/api/messagebox/ReadMessag/85/35233
    NSString *url = [NSString stringWithFormat:@"http://beta02api.azurewebsites.net/api/messagebox/ReadMessag/%lld/%lld", userID, messageID];
    return url;
}

+(NSString *)getChangeFingerprintRequestURL {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/repass?id=%ld", [UserData getUserID]];
    return url;
}

+(NSString *)getUserCredentialURL {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/user/?userid=79", [UserData getUserID]];
    return url;
}

+ (NSString *)getStudentScheduleListURLWithUserID:(long long)userID date:(NSDate *)date {
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.jabjai.school/api/school/Schedule/ScheduleList?studentid=%lld&day=%@", userID, [Utils dateToServerDateFormat:date]];
    
    return url;

}

+ (NSString *)getStudentScheduleListDetailURLWithUserID:(long long)userID date:(NSDate *)date subjectID:(long long)subjectID {
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/school/Schedule/ScheduleDetail?studentid=%lld&day=%@&ScheduleId=%lld", userID, [Utils dateToServerDateFormat:date], subjectID];
    
    return url;
}

+ (NSString *)getBoughtItemWithUserID:(long long)userID date:(NSDate *)date {
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/report/buyitem?userid=%lld&day=%@", userID, [Utils dateToServerDateFormat:date]];
    return url;
}

+ (NSString *)getTopupMessageWithUserID:(long long)userID date:(NSDate *)date {
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/messagebox/getMessage/topup?userid=%lld&dmessage=%@", userID, [Utils dateToServerDateFormat:date]];
    return url;
}

+ (NSString *)getUnreadMessageCountWithUserID:(long long)userID {
    //http://beta02api.azurewebsites.net/api/messagebox/CountUnreadMessag/85
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/messagebox/CountUnreadMessag/%lld", userID];
    return url;
}

+ (NSString *)getFeedBackPOSTURL {
    // parameters : message,phone,userid
    NSString *url = @"http://apifeedback20170505022614.azurewebsites.net/api/feedback/send";
    return url;
}

// Take Class Attendance
+ (NSString *)getStudentClassLevelWithSchoolId:(long long)schoolId {
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/sublevel/getLevel/%lld", schoolId];
    
    return url;
}

+ (NSString *)getSchoolClassroomWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/sublevel2?schoolid=%lld&sublevelid=%lld", schoolId, classLevelId];
    
    return url;
}

+ (NSString *)getSubjectWithSchoolId:(long long)schoolId classroomId:(long long)classroomId {
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/School/getschedule/%lld/%lld", schoolId, classroomId];
    
    return url;
}

+ (NSString *)getStudentInClassWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId {
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/School/getstudent/%lld/%lld/%lld/%lld", schoolId, classroomId, subjectId, teacherId];
    
    return url;
}

+ (NSString *)getPreviousAttendClassStatusWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId {
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/school/copystatus/%lld/%lld/%lld/%lld", schoolId, classroomId, subjectId, teacherId];
    
    return url;
}

+ (NSString *)getUpdateStudentStatusPOSTWithSchoolId:(long long)schoolId subjectId:(long long)subjectId teacherId:(long long)teacherId {
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/School/updatestatus/%lld/%lld/%lld", schoolId, subjectId, teacherId];
    
    return url;
}

+ (NSString *)getTASubjectListWithSchoolId:(long long)schoolId classroomId:(long long)classroomId {
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/school/Schedule/report4level2/getplane/v1?schoolid=%lld&level2Id=%lld", schoolId, classroomId];
    
    return url;
}

+ (NSString *)getTAHistoryStudentStatusListWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId date:(NSDate *)date {
    
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/school/Schedule/report4level2/getdaydetail/v1?schoolid=%lld&level2Id=%lld&day=%@&planeid=%@", schoolId, classroomId, dateStr, subjectId];
    
    return url;
}

+ (NSString *)getTAHistoryDateStatusWithSchoolId:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId startDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    
    NSString *startDateStr = [Utils dateToServerDateFormat:startDate];
    NSString *endDateStr= [Utils dateToServerDateFormat:endDate];
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/school/Schedule/report4level2/getdaychecked/v1?schoolid=%lld&level2Id=%lld&daystart=%@&dayend=%@&planeid=%@", schoolId, classroomId, startDateStr, endDateStr, subjectId];
    
    return url;
}

/*
 ** Behavior Score
 */

+ (NSString *)getStudentInClassroomWithSchoolId:(long long)schoolId classroomId:(long long)classroomId {
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.jabjai.school/api/studentinclass?SchoolId=%lld&level2=%lld", schoolId, classroomId];
    
    return url;
}

+ (NSString *)getAddBehaviorScoreListWithSchoolId:(long long)schoolId {
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.jabjai.school/api/Behaviors/list/add?SchoolId=%lld", schoolId];
    
    return url;
}

+ (NSString *)getReduceBehaviorScoreListWithSchoolId:(long long)schoolId {
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.jabjai.school/api/Behaviors/list/reduce?SchoolId=%lld", schoolId];
    
    return url;
    
}

+ (NSString *)getUpdateStudentBehaviorScorePOSTWithSchoolId:(long long)schoolId teacherId:(long long)teacherId {
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.jabjai.school/api/Behaviors/updatescore?SchoolId=%lld&TheacherId=%lld", schoolId, teacherId];
    return url;
    
}

+ (NSString *)getBehaviorScoreHistoryInDateRangeWithSchoolId:(long long)schoolId classroomId:(long long)classroomId startDate:(NSDate *)startDate endDate:(NSDate *)endDate type:(NSInteger)type {
    
    NSString *startDateStr = [Utils dateToServerDateFormat:startDate];
    NSString *endDateStr= [Utils dateToServerDateFormat:endDate];
    
    NSString *url;
    
    if(type == 0 || type == 1) {
        
        url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/Behaviors/reports?schoolid=%lld&classid=%lld&daystart=%@&dayend=%@&type=%i", schoolId, classroomId, startDateStr, endDateStr, (int)type];
        
    } else {
        url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/Behaviors/reports?schoolid=%lld&classid=%lld&daystart=%@&dayend=%@&type=", schoolId, classroomId, startDateStr, endDateStr];
    }
    
    return url;
}

+ (NSString *)getBehaviroScoreHistoryForStudentReportWithUserId:(long long)userId {
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/Behaviors/History?UserId=%lld", userId];
    
    return url;
}

/*
 ** Executive report
 */

+ (NSString *)getEXReportSummaryAttendSchoolWithSchoolId:(long long)schoolId date:(NSDate *)date {
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/takeschoolattendancereports?dayreports=%@&schoolid=%lld", dateStr, schoolId];
    return url;
}

+ (NSString *)getEXReportClassroomAttendSchoolWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId date:(NSDate *)date {
    
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/takeschoolattendancereports/SumStudent4class?dayreports=%@&schoolid=%lld&levelid=%lld", dateStr, schoolId, classLevelId];
    return url;
}

+ (NSString *)getEXReportAttendSchoolTeacherListWithSchoolId:(long long)schoolId date:(NSDate *)date {
    
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/takeschoolattendancereports/Theacher?dayreports=%@&schoolid=%lld", dateStr, schoolId];
    return url;
}

+ (NSString *)getEXReportAttendSchoolEmployeeListWithSchoolId:(long long)schoolId date:(NSDate *)date {
    NSString *dateStr = [Utils dateToServerDateFormat:date];
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://beta02api.azurewebsites.net/api/takeschoolattendancereports/Employees?dayreports=%@&schoolid=%lld", dateStr, schoolId];
    return url;
}

#pragma mark - University URL

+(NSString *)getUniversityFacultyURL:(NSInteger)schoolID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/UniversityLevel?schoolid=%i", (int)schoolID];
    return url;
}

+(NSString *)getUniversityDegreeURL:(NSInteger)schoolID facultyID:(NSInteger)facultyID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/UniversityLevelSub?schoolid=%i&levelid=%i", (int)schoolID, (int)facultyID];
    return url;
}

+(NSString *)getUniversityMajorURL:(NSInteger)schoolID degreeID:(NSInteger)degreeID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/UniversityLevelSub2?schoolid=%i&sublevelid=%i", (int)schoolID, (int)degreeID];
    return url;
}

+(NSString *)getStudentEnrollSubjectURL {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/UniversityUserListSection?userid=%i", (int)[UserData getUserID]];
    return url;
}

+(NSString *)getAddSectionUrl:(NSInteger)sectionID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/UniversityUserInsertSection?Userid=%i&SectionId=%i", (int)[UserData getUserID], (int)sectionID];
    return url;
}

+(NSString *)getDeleteSectionUrl:(NSInteger)sectionID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/UniversityUserDeleteSection?Userid=%i&SectionId=%i", (int)[UserData getUserID], (int)sectionID];
    return url;
}

+(NSString *)getSubjectAddedUrl {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/UniversityUserSelectedSection?Userid=%i", (int)[UserData getUserID]];
    return url;
}

+(NSString *)getTeachingSubjectUrl {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/UniversityPlane?UserId=%i", (int)[UserData getUserID]];
    return url;
}

+(NSString *)getStudentsInSectionUrl:(NSInteger)sectionID {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/UniversitySectionListUser?userid=%i&SectionId=%i", (int)[UserData getUserID], (int)sectionID];
    return url;
}

+(NSString *)getStudentSubjectListAllYearUrl {
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/UniversityStudentSubjectList?UserId=%i", (int)[UserData getUserID]];
    return url;
}
+(NSString *)getTheacherAllUrl:(NSInteger)schoolID{
    NSString *url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/user/getuser/Theacher?schoolid=%i", (int)schoolID];
    return url;
}


+(NSString *)getAttendToSectionHistoryWithSectionID:(NSInteger)sectionID startDate:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status userType:(USERTYPE)userType {
    
    NSString *serverStartDateStr = [Utils dateToServerDateFormat:startDate];
    NSString *serverEndDateStr = [Utils dateToServerDateFormat:endDate];
    
    NSString *url;
    
    if(userType == STUDENT) {
        url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/Come2Section4Student?UserId=%i&SectionId=%i&dstart=%@&dend=%@", (int)[UserData getUserID], (int)sectionID, serverStartDateStr, serverEndDateStr];
    }
    else {
        url = [NSString stringWithFormat:@"http://beta02api.jabjai.school/api/Come2Section4Teacher?UserId=%i&SectionId=%i&dstart=%@&dend=%@", (int)[UserData getUserID], (int)sectionID, serverStartDateStr, serverEndDateStr];
    }
    
    NSMutableString *mutableURL = [[NSMutableString alloc] initWithString:url];
    
    if(status >= 0) {
        [mutableURL appendString:[NSString stringWithFormat:@"&status=%i", (int)status]];
    }
    else {
        [mutableURL appendString:@"&status="];
    }
    
    return mutableURL;
}

@end
