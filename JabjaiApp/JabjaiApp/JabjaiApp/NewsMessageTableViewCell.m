//
//  NewsMessageTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NewsMessageTableViewCell.h"

@implementation NewsMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.dateTimeLabel.text = nil;
    self.titleLabel.text = nil;
    self.messageLabel.text = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    self.dateTimeLabel.text = nil;
    self.titleLabel.text = nil;
    self.messageLabel.text = nil;
}

@end
