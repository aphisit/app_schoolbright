//
//  SignupPage1ViewController.h
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupDataModel.h"
#import "TableListDialog.h"

@interface SignupPage1ViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate>

@property (nonatomic, strong) SignupDataModel *signupDataModel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *schoolTextField;
@property (weak, nonatomic) IBOutlet UITextField *degreeTextField;
@property (weak, nonatomic) IBOutlet UITextField *gradTextField;
@property (weak, nonatomic) IBOutlet UITextField *classroomTextField;
@property (weak, nonatomic) IBOutlet UITextField *studentIDTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

- (IBAction)moveBack:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)nextStep:(id)sender;

@end
