//
//  SSubjectViewController.m
//  JabjaiApp
//
//  Created by mac on 5/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//



#import "SSubjectViewController.h"
#import "SSubjectTableViewCell.h"
#import "SSubjectModel.h"
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"

@interface SSubjectViewController () {
    
    NSMutableArray<SSubjectModel *> *subjectArray;
    NSInteger connectCounter;
    
    UIColor *grayColor;
}

@property (strong, nonatomic) TimeTableViewController *timeTableViewController;
@property (strong, nonatomic) ARSPContainerController *panelControllerContainer;

@property (nonatomic) NSDate *consideredDate;

@end

@implementation SSubjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.panelControllerContainer = (ARSPContainerController *)self.parentViewController;
    self.timeTableViewController = (TimeTableViewController *)self.panelControllerContainer.mainViewController;
    self.timeTableViewController.delegate = self;
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = grayColor;
    
    connectCounter = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SSubjectTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.consideredDate = [NSDate date];
    self.dateTitle.text = [Utils getThaiDateFormatWithDate: self.consideredDate];
    [self getScheduleListDataWithDate: self.consideredDate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(subjectArray == nil) {
        return 0;
    }
    else {
        return subjectArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SSubjectModel *model = [subjectArray objectAtIndex:indexPath.row];
    
    SSubjectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.labelSubject.text = model.subjectName;
    cell.labelTimeStart.text = model.startTime;
    cell.labelTimeEnd.text = model.endTime;
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onSelectSubject:subjectName:date:)]) {
        SSubjectModel *model = [subjectArray objectAtIndex:indexPath.row];
        [self.delegate onSelectSubject:model.subjectID subjectName:model.subjectName date:self.consideredDate];
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

#pragma mark - TimeTableViewControllerDelegate

- (void)onSelectDate:(NSDate *)date {
    
    self.consideredDate = date;
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:date];
    [self getScheduleListDataWithDate:date];
    
}

#pragma mark - Get API Data
- (void)getScheduleListDataWithDate:(NSDate *)date {
    
    long long userID = [UserData getUserID];
    NSString *URLString = [APIURL getStudentScheduleListURLWithUserID:userID date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getScheduleListDataWithDate:date];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getScheduleListDataWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getScheduleListDataWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(subjectArray != nil) {
                    [subjectArray removeAllObjects];
                    subjectArray = nil;
                }
                
                subjectArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long subjectID = [[dataDict objectForKey:@"ScheduleId"] longLongValue];
                    
                    NSMutableString *subjectName, *startTime, *endTime;
                    
                    if(![[dataDict objectForKey:@"ScheduleName"] isKindOfClass:[NSNull class]]) {
                        subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"ScheduleName"]];
                    }
                    else {
                        subjectName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TimeIn"] isKindOfClass:[NSNull class]]) {
                        startTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeIn"]];
                    }
                    else {
                        startTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if(![[dataDict objectForKey:@"TimeOut"] isKindOfClass:[NSNull class]]) {
                        endTime = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TimeOut"]];
                    }
                    else {
                        endTime = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startTime);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endTime);
                    
                    SSubjectModel *model = [[SSubjectModel alloc] init];
                    model.subjectID = subjectID;
                    model.subjectName = subjectName;
                    model.startTime = startTime;
                    model.endTime = endTime;
                    
                    [subjectArray addObject:model];
                    
                }
                
                [self.tableView reloadData];
            }
        }
        
    }];
    
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

@end
