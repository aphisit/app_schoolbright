//
//  AddAccountMainViewController.h
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DelUserAccountTableViewCell.h"
#import "ConfirmDialog.h"

@interface AddAccountMainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, DelUserAccountTableViewCellDelegate, ConfirmDialogDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)moveBack:(id)sender;
- (IBAction)actionAddAccount:(id)sender;

@end
