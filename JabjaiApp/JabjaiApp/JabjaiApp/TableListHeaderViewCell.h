//
//  TableListHeaderViewCell.h
//  JabjaiApp
//
//  Created by mac on 11/5/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableListHeaderViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
