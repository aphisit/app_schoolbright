//
//  PurchasingItemListViewController.h
//  JabjaiApp
//
//  Created by mac on 5/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PurchasingReportViewController.h"

@interface PurchasingItemListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, PurchasingReportViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;


@end
