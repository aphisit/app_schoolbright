//
//  CallGetAddBehaviorScoreListAPI.h
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BehaviorScoreModel.h"

@class CallGetAddBehaviorScoreListAPI;

@protocol CallGetAddBehaviorScoreListAPIDelegate <NSObject>

- (void)callGetAddBehaviorScoreListAPI:(CallGetAddBehaviorScoreListAPI *)classObj data:(NSArray<BehaviorScoreModel *> *)data success:(BOOL)success;

@end

@interface CallGetAddBehaviorScoreListAPI : NSObject

@property (nonatomic, weak) id<CallGetAddBehaviorScoreListAPIDelegate> delegate;

- (void)call:(long long)schoolId;

@end
