//
//  AddAccountLoginViewController.m
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import AirshipKit;

#import "AddAccountLoginViewController.h"
#import "AddAccountMainViewController.h"
#import "SWRevealViewController.h"
#import "LoginUserModel.h"
#import "VerifyCodeDialog.h"
#import "AlertDialog.h"
#import "MultilineAlertDialog.h"
#import "UserData.h"
#import "UserInfoDBHelper.h"

@interface AddAccountLoginViewController () {
    
    VerifyCodeDialog *verifyCodeDialog;
    AlertDialog *alertDialog;
    MultilineAlertDialog *multilineAlertDialog;
    
}

@property (nonatomic, strong) CallGetLoginAPI *callGetLoginAPI;

@property (nonatomic, strong) UserInfoDBHelper *dbHelper;

@end

@implementation AddAccountLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    
    if(self.userArray == nil) {
        self.userArray = [self.dbHelper getUserDataWithMasterId:[UserData getMasterUserID]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Call API
- (void) getLoginWithUserName:(NSString *)userName password:(NSString *)password {
    
    self.callGetLoginAPI = nil;
    self.callGetLoginAPI = [[CallGetLoginAPI alloc] init];
    self.callGetLoginAPI.delegate = self;
    [self.callGetLoginAPI call:userName password:password];
    
    [self showIndicator];
}

#pragma mark - CallGetLoginAPIDelegate
- (void)callGetLoginAPI:(CallGetLoginAPI *)classObj data:(LoginUserModel *)data success:(BOOL)success {
    
    [self stopIndicator];
    self.btnLogin.userInteractionEnabled = YES;
    
    if(success && data != nil) {
        MultipleUserModel *userModel = [[MultipleUserModel alloc] init];
        
        userModel.masterId = [UserData getMasterUserID];
        userModel.slaveId = data.userId;
        userModel.schoolId = data.schoolId;
        userModel.userType = data.userType;
        userModel.academyType = data.academyType;
        userModel.imageUrl = data.imageUrl;
        userModel.firstName = data.firstName;
        userModel.lastName = data.lastName;
        userModel.gender = data.gender;
        
        [self insertUserIntoDB:userModel];
    }
    else {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *message1 = @"Username / Password ไม่ถูกต้อง";
        NSString *message2 = @"*สำหรับนักเรียนให้ใช้เลขบัตรนักเรียน";
        NSString *message3 = @"*สำหรับอาจารย์ให้ใช้เบอร์โทรศัพท์";
        
        NSString *textMessage = [NSString stringWithFormat:@"%@\n\n%@\n%@", message1, message2, message3];
        [self showMultilineAlertDialogWithTitle:alertTitle message:textMessage];
    }
}

- (void)onUnRegisterFinger:(CallGetLoginAPI *)classObj confirmCode:(NSString *)confirmCode {
    
    [self stopIndicator];
    self.btnLogin.userInteractionEnabled = YES;
    
    NSString *title = @"ท่านยังไม่ได้ยืนยันลายนิ้วมือ";
    [self showVerifyCodeDialogWithTitle:title verifyCode:confirmCode];
}

- (void)incorrectUserNameOrPassword:(CallGetLoginAPI *)classObj {
    
    [self stopIndicator];
    self.btnLogin.userInteractionEnabled = YES;
    
    NSString *alertTitle = @"แจ้งเตือน";
    NSString *message1 = @"Username / Password ไม่ถูกต้อง";
    NSString *message2 = @"*สำหรับนักเรียนให้ใช้เลขบัตรนักเรียน";
    NSString *message3 = @"*สำหรับอาจารย์ให้ใช้เบอร์โทรศัพท์";
    
    NSString *textMessage = [NSString stringWithFormat:@"%@\n\n%@\n%@", message1, message2, message3];
    [self showMultilineAlertDialogWithTitle:alertTitle message:textMessage];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialog alloc] init];
    }
    else if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showMultilineAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(multilineAlertDialog == nil) {
        multilineAlertDialog = [[MultilineAlertDialog alloc] init];
    }
    else if(multilineAlertDialog != nil && [multilineAlertDialog isDialogShowing]) {
        [multilineAlertDialog dismissDialog];
    }
    
    [multilineAlertDialog showDialogInView:self.view title:title message:message];
}

- (void)showVerifyCodeDialogWithTitle:(NSString *)title verifyCode:(NSString *)verifyCode {
    
    if(verifyCodeDialog == nil) {
        verifyCodeDialog = [[VerifyCodeDialog alloc] init];
    }
    else if(verifyCodeDialog != nil && [verifyCodeDialog isDialogShowing]) {
        [verifyCodeDialog dismissDialog];
    }
    
    [verifyCodeDialog showDialogInView:self.view title:title verifyCode:verifyCode];
}


#pragma mark - Utility

- (void)insertUserIntoDB:(MultipleUserModel *)userModel {
    
    BOOL isDuplicated = NO;
    
    if(self.userArray != nil) {
        for(MultipleUserModel *model in self.userArray) {
            
            if([model getMasterId] == [userModel getMasterId] && [model getSlaveId] == [userModel getSlaveId]) {
                isDuplicated = YES;
                break;
            }
        }
    }
    
    if(isDuplicated) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = @"ชื่อผู้ใช้นี้ถูกเพิ่มไว้ก่อนแล้ว";
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else if([userModel getSlaveId] == [UserData getMasterUserID]) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = @"ไม่สามารถเพิ่มผู้ใช้ตรงกับผู้ใช้หลักได้";
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else {
        BOOL success = [self.dbHelper insertData:userModel];
        
        if(success) {
            
            NSString *tag = [NSString stringWithFormat:@"%lld", [userModel getSlaveId]];
            [[UAirship push] addTag:tag];
            [[UAirship push] updateRegistration];
            
            [self backToPreviousPage];
        }
        else {
            NSString *alertTitle = @"แจ้งเตือน";
            NSString *alertMessage = @"ขออภัยไม่สามารถเพิ่มผู้ใช้ได้ โปรดลองใหม่!";
            [self showAlertDialogWithTitle:alertTitle message:alertMessage];
        }
    }
    
}

- (void)backToPreviousPage {
    //AddAccountMainStoryboard
    AddAccountMainViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAccountMainStoryboard"];
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Action functions
- (IBAction)dismissKeyboard:(id)sender {
    [self resignFirstResponder];
}

- (IBAction)actionLogin:(id)sender {

    NSString *userName = self.usernameTextField.text;
    NSString *password = self.passwordTextField.text;
    
    if(userName.length == 0) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = @"กรุณาระบุชื่อผู้ใช้";
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else if(password.length == 0) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = @"กรุณาระบุรหัสผ่าน";
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else {
        self.btnLogin.userInteractionEnabled = NO;
        [self getLoginWithUserName:userName password:password];
    }
}

- (IBAction)moveBack:(id)sender {
    [self backToPreviousPage];
}
@end
