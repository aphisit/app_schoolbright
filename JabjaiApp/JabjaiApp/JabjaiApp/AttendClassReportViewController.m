//
//  AttendClassReportViewController.m
//  JabjaiApp
//
//  Created by mac on 1/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "AttendClassReportViewController.h"
#import "AttendClassCalendarViewController.h"
#import "AttendClassAgendaTableViewController.h"
#import "APIURL.h"
#import "Utils.h"
#import "DateUtility.h"
#import "YearTermModel.h"
#import "Attend2SchoolFilter.h"
#import "StudentAttendClassSubjectViewController.h"
#import "TeacherAttendClassSubjectViewController.h"

@interface AttendClassReportViewController () {
    
    UIColor *pagerOrangeColor;
    UIColor *pagerGreenColor;
    UIColor *indicatorOrangeColor;
    UIColor *indicatorGreenColor;
    
    // Data Containers
    NSMutableDictionary *yearTermDict; //Key is yearNumber and data are array of YearTermModel
}

@property (nonatomic) CAPSPageMenu *pageMenu;

//
@property (strong, nonatomic) NSCalendar *gregorian;

@property (nonatomic, strong) CalendarDialog *calendarDialog;
@property (nonatomic, strong) Attend2SchoolReportFilterDialog *filterDialog;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@property (nonatomic, strong) AttendClassCalendarViewController *calendarController;
@property (nonatomic, strong) AttendClassAgendaTableViewController *agendaController;

@end

@implementation AttendClassReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pagerOrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1];
    pagerGreenColor = [UIColor colorWithRed:73/255.0 green:160/255.0 blue:152/255.0 alpha:1];
    indicatorOrangeColor = [UIColor colorWithRed:254/255.0 green:180/255.0 blue:57/255.0 alpha:1];
    indicatorGreenColor = [UIColor colorWithRed:31/255.0 green:124/255.0 blue:114/255.0 alpha:1];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.gregorian = [Utils getGregorianCalendar];
    
    NSDate *minDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:[NSDate date] options:0];
    NSDate *maxDate = [NSDate date];
    self.calendarDialog = [[CalendarDialog alloc] initWithTitle:@"ปฏิทิน" minimumDate:minDate maximumDate:maxDate];
    
    if(self.subjectName != nil) {
        self.headerTitleLabel.text = self.subjectName;
    }
    
    [self setupFilterDialog];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    [self setupPageMenu];
}

- (void)setupPageMenu {
    self.calendarController = [[AttendClassCalendarViewController alloc] init];
    self.calendarController.title = @"ปฏิทิน";
    self.calendarController.sectionID = self.sectionID;
    
    self.agendaController = [[AttendClassAgendaTableViewController alloc] init];
    self.agendaController.title = @"ประวัติ";
    self.agendaController.sectionID = self.sectionID;
    
    NSArray *controllerArray = @[self.calendarController, self.agendaController];
    
    // Color background for each controller tab
    NSArray *colorBackground = @[pagerOrangeColor, pagerGreenColor];
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionMenuItemSeparatorWidth: @(4.3),
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                                 CAPSPageMenuOptionSelectionIndicatorColor: indicatorOrangeColor,
                                 CAPSPageMenuOptionMenuMargin: @(20),
                                 CAPSPageMenuOptionMenuHeight: @(40),
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor: [UIColor whiteColor],                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"ThaiSansNeue-SemiBold" size:22.0],
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                                 CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                                 CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                 CAPSPageMenuOptionColorBackgroundArray: colorBackground
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    //Optional delegate
    _pageMenu.delegate = self;
    
    [self.contentView addSubview:_pageMenu.view];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)moveBack:(id)sender {
    
    if([UserData getUserType] == STUDENT) {
        StudentAttendClassSubjectViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"StudentAttendClassSubjectStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else {
        TeacherAttendClassSubjectViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TeacherAttendClassSubjectStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    
}

- (IBAction)showFilterDialog:(id)sender {
    
    if(self.filterDialog != nil) {
        [self.filterDialog showDialogInView:self.view title:@"Filter"];
    }
}

#pragma CAPSPageMenuDelegate
- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    //NSLog(@"WillMoveToPage");
    
    if(index == 0) {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorOrangeColor;
    }
    else {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorGreenColor;
        
    }
    
    
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
    //NSLog(@"DidMoveToPage");
    
    if(index == 0) {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorOrangeColor;
    }
    else {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorGreenColor;
        
    }
    
}

#pragma mark - Attend2SchoolReportFilterDialogDelegate

-(void)applyFilter:(NSNumber *)schoolYear semester:(NSString *)semester status:(NSString *)status startDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    
    Attend2SchoolFilter *filter = [[Attend2SchoolFilter alloc] init];
    filter.schoolYear = schoolYear;
    filter.semester = semester;
    
    if([status isEqualToString:@"ตรงเวลา"]) {
        filter.status = INTIME;
    }
    else if([status isEqualToString:@"สาย"]) {
        filter.status = LATE;
    }
    else if([status isEqualToString:@"ขาด"]) {
        filter.status = ABSENCE;
    }
    else if([status isEqualToString:@"ลา"]) {
        filter.status = ONLEAVE;
    }
    else if([status isEqualToString:@"วันหยุด"]) {
        filter.status = HOLIDAY;
    }
    else {
        filter.status = ALL;
    }
    
    filter.startDate = startDate;
    filter.endDate = endDate;
    
    if(self.calendarController != nil) {
        [self.calendarController applyFilter:filter];
    }
    
    if(self.agendaController != nil) {
        [self.agendaController applyFilter:filter];
    }
    
    NSLog(@"%@", @"Apply Filter");
}

#pragma - Dialog setup

- (void)setupFilterDialog {
    
    if(self.teachingYearArray == nil) {
        return ;
    }
    
    if(yearTermDict != nil) {
        [yearTermDict removeAllObjects];
    }
    
    yearTermDict = [[NSMutableDictionary alloc] init];
    
    for(UTeachingYearModel *teachingYearModel in self.teachingYearArray) {
        NSNumber *yearID = [[NSNumber alloc] initWithInt:teachingYearModel.yearID];
        NSNumber *yearNumber = [[NSNumber alloc] initWithInt:[teachingYearModel.yearName intValue]];
        
        NSArray *semesters = teachingYearModel.semesters;
        
        for(USemesterModel *semesterModel in semesters) {
            
            YearTermModel *model = [[YearTermModel alloc] init];
            [model setYearID:yearID];
            [model setYearNumber:yearNumber];
            [model setTermID:semesterModel.semesterID];
            [model setTermName:semesterModel.semesterName];
            [model setTermBegin:semesterModel.startDate];
            [model setTermEnd:semesterModel.endDate];
            
            if([yearTermDict objectForKey:model.yearNumber] != nil) {
                NSMutableArray *termArr = [yearTermDict objectForKey:model.yearNumber];
                [termArr addObject:model];
                [yearTermDict setObject:termArr forKey:model.yearNumber];
            }
            else {
                NSMutableArray *termArr = [[NSMutableArray alloc] init];
                [termArr addObject:model];
                [yearTermDict setObject:termArr forKey:model.yearNumber];
            }
        }
    }
    
    // Initialize filter dialog
    if(self.filterDialog != nil && [self.filterDialog isDialogShowing]) {
        [self.filterDialog dismissDialog];
        self.filterDialog = nil;
    }
    
    self.filterDialog = [[Attend2SchoolReportFilterDialog alloc] initWithYearTermDictionary:yearTermDict];
    self.filterDialog.delegate = self;
}


@end
