//
//  testViewController.m
//  JabjaiApp
//
//  Created by toffee on 8/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "testViewController.h"
#import "Utils.h"
#import "APIURL.h"

@interface testViewController ()

@end

@implementation testViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self doListData];
    // Do any additional setup after loading the view.
}

- (void)doListData {
       NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
    NSLog(@"%@",URLString);
    NSLog(@"%@",url);
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        NSArray *returnedArray = returnedData;
        NSDictionary *dataDict = [returnedArray objectAtIndex:0];
        NSLog(@"dataDict = %@", dataDict);
        NSMutableString *firstName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sName"]];
        NSMutableString *lastName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sLastname"]];
        self.name.text = firstName;
        self.lastName.text = lastName;
        self.nameLabel.text = firstName;

    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
