//
//  CreditBureauGraphDialog.h
//  JabjaiApp
//
//  Created by mac on 11/19/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    ONTIME,
    LATE,
    ABSENCE
} ATTENDSTATUS;

@interface CreditBureauGraphDialog : UIViewController

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *emoImage;
@property (weak, nonatomic) IBOutlet UILabel *attendStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusTimesLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalStatusTimesLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

- (IBAction)closeDialog:(id)sender;

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title status:(ATTENDSTATUS)status times:(NSInteger)times totalTimes:(NSInteger)totalTimes;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
