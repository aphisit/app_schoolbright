//
//  SNTimeOutViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface SNTimeOutViewController : UIViewController <UITextFieldDelegate>{
    UIDatePicker *datePicker;
    NSInteger chackDateTime;
    BOOL chackRadioSendNow;
    BOOL chackRadioSetTime;
}
@property (weak, nonatomic) IBOutlet UITextView *detail;
@property (weak, nonatomic) IBOutlet UITextField *dateTextFiled;


@property (weak, nonatomic) IBOutlet UIButton *btnSendNow;
@property (weak, nonatomic) IBOutlet UIButton *btnSettime;
@property (weak, nonatomic) IBOutlet UIView *sendFileView;
@property (weak, nonatomic) IBOutlet UIView *sendImageView;
- (IBAction)selectImage:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *timeTextFiled;
- (IBAction)sendNowRadio:(id)sender;
- (IBAction)setTimeRadio:(id)sender;
- (IBAction)selectFile:(id)sender;
- (IBAction)backToMenu:(id)sender;


@end
