//
//  AboutViewController.m
//  JabjaiApp
//
//  Created by mac on 12/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "AboutViewController.h"
#import "SWRevealViewController.h"
#import "SettingViewController.h"
#import "Utils.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.borderView.layer.cornerRadius = 10;
    self.borderView.layer.masksToBounds = YES;
    self.borderView.layer.borderColor = UIColorWithHexString(@"#F06B20").CGColor;
    self.borderView.layer.borderWidth = 2.0;
    self.borderView.backgroundColor = [UIColor clearColor];
    
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *appVersion = [infoDict objectForKey:@"CFBundleShortVersionString"];
    self.versionLabel.text = [[NSString alloc] initWithFormat:@"เวอร์ชัน : %@", appVersion];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)moveBack:(id)sender {
    
    SettingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingStoryboard"];
    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}
@end
