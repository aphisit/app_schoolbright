//
//  BSHistoryModel.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSHistoryModel : NSObject

@property (nonatomic) NSInteger behaviorType; // type 0: add, 1: reduce
@property (nonatomic) NSString *behaviorName;
@property (nonatomic) NSInteger behaviorScore;
@property (nonatomic) NSDate *dateTime;
@property (nonatomic) NSString *studentName;
@property (nonatomic) NSString *recorder;
@property (nonatomic) NSString *remark;

- (void)setBehaviorType:(NSInteger)behaviorType;
- (void)setBehaviorName:(NSString *)behaviorName;
- (void)setBehaviorScore:(NSInteger)behaviorScore;
- (void)setDateTime:(NSDate *)dateTime;
- (void)setStudentName:(NSString *)studentName;
- (void)setRecorder:(NSString *)recorder;
- (void)setRemark:(NSString *)remark;

- (NSInteger)getBehaviorType;
- (NSString *)getBehaviorName;
- (NSInteger)getBehaviorScore;
- (NSDate *)getDateTime;
- (NSString *)getStudentName;
- (NSString *)getRecorder;
- (NSString *)getRemark;

@end
