//
//  BSSelectClassLevelViewController.m
//  JabjaiApp
//
//  Created by mac on 7/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSSelectClassLevelViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "Constant.h"
#import "BSSelectStudentsViewController.h"

@interface BSSelectClassLevelViewController () {
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    
    UIColor *greenBGColor;
    UIColor *redBGColor;
    
    // The dialog
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
}

@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;

@end

static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";

@implementation BSSelectClassLevelViewController

@synthesize classLevelId = _classLevelId;
@synthesize classroomId = _classroomId;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    greenBGColor = [UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0];
    redBGColor = [UIColor colorWithRed:220/255.0 green:43/255.0 blue:69/255.0 alpha:1.0];
    
    self.classLevelTextField.delegate = self;
    self.classroomTextField.delegate = self;
    
    if(_classLevelArray != nil && _classroomArray != nil) {
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classLevelArray.count; i++) {
            SchoolLevelModel *model = [_classLevelArray objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classroomArray.count; i++) {
            SchoolClassroomModel *model = [_classroomArray objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:_selectedClassLevelIndex] getClassLevelName];
        self.classroomTextField.text = [[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomName];
        
    }
    else {
        _selectedClassLevelIndex = -1;
        _selectedClassroomIndex = -1;
        
        // Call api
        [self getSchoolClassLevel];
    }
    
    if(_mode == BS_MODE_ADD) {
        self.headerTitleLabel.text = @"เพิ่มคะแนนความประพฤติ";
        [self.nextButton setBackgroundColor:greenBGColor];
    }
    else {
        self.headerTitleLabel.text = @"ตัดคะแนนความประพฤติ";
        [self.nextButton setBackgroundColor:redBGColor];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // User press class level textfield
        
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - API Caller

- (void)getSchoolClassLevel {
    
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    //NSLog(@"getSchoolig = %@",[UserData getSchoolId] );
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}

#pragma mark - CallGetSchoolClassLevelAPIDelegate

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
    }
}

#pragma mark - CallGetStudentClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _classroomArray = data;
        
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
        
        if(index == _selectedClassLevelIndex) {
            return;
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classroomTextField.text = @"";
        _selectedClassroomIndex = -1;
        _classLevelId = 0;
        
        // get classroom data with class level
        _classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        NSLog(@"classID = %d",_classLevelId);
        [self getSchoolClassroomWithClassLevelId:_classLevelId];
        
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        
        if(index == _selectedClassroomIndex) {
            return;
        }
        
        self.classroomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        
        _classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        
    }
    
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classroomTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกชั้นเรียน";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}

- (void)gotoSelectStudentsPage {
    
    BSSelectStudentsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectStudentsStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    
    viewController.selectedStudentArray = _selectedStudentArray;
    
    viewController.behaviorScoreArray = _behaviorScoreArray;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

#pragma mark - Action fuctions

- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        [self gotoSelectStudentsPage];
    }
}
@end
