//
//  CallGetBSHistoryInDateRangeAPI.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSHistoryModel.h"

@class CallGetBSHistoryInDateRangeAPI;

@protocol CallGetBSHistoryInDateRangeAPIDelegate <NSObject>

- (void)callGetBSHistoryInDateRangeAPI:(CallGetBSHistoryInDateRangeAPI *)classObj data:(NSArray<BSHistoryModel *> *)data success:(BOOL)success;

@end

@interface CallGetBSHistoryInDateRangeAPI : NSObject

@property (nonatomic, weak) id<CallGetBSHistoryInDateRangeAPIDelegate> delegate;

- (void)call:(long long)schoolId classroomId:(long long)classroomId startDate:(NSDate *)startDate endDate:(NSDate *)endDate type:(NSInteger)type;

@end
