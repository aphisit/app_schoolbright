//
//  SchoolDegreeModel.h
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SchoolDegreeModel : NSObject

@property (nonatomic, strong) NSNumber *degreeID;
@property (nonatomic, strong) NSString *degreeName;

-(NSNumber *)getDegreeID;
-(NSString *)getDegreeName;

@end
