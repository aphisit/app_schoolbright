//
//  AttendClassAgendaTableViewController.m
//  JabjaiApp
//
//  Created by mac on 1/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "AttendClassAgendaTableViewController.h"
#import "AgendaHeaderTableViewCell.h"
#import "AgendaTableViewCell.h"
#import "APIURL.h"
#import "Utils.h"
#import "DateUtility.h"
#import "AttendToSectionHistoryModel.h"
#import "Constant.h"

@interface AttendClassAgendaTableViewController () {
    NSArray *titleArr;
    NSMutableArray *attendToSectionHistoryArray;
    NSMutableArray *dateStatus;
    
    NSInteger connectCounter;
}

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) NSDate *minimumDate;
@property (strong, nonatomic) NSDate *maximumDate;

@end

static NSString *tableHeaderCellIdentifier = @"HeaderCell";
static NSString *tableCellIdentifier = @"Cell";

@implementation AttendClassAgendaTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AgendaHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableHeaderCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AgendaTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableCellIdentifier];
    
    titleArr = @[@"วันที่", @"สถานะ"];
    dateStatus = [[NSMutableArray alloc] init];
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.gregorian = [Utils getGregorianCalendar];
    self.maximumDate = [NSDate date];
    self.minimumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:[NSDate date] options:0];
    
    connectCounter = 0;
    
    [self getAttendToSectionHistoryData:self.minimumDate endDate:self.maximumDate status:-1];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else {
        
        if(attendToSectionHistoryArray == nil) {
            return 0;
        }
        else {
            return attendToSectionHistoryArray.count;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        AgendaHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableHeaderCellIdentifier forIndexPath:indexPath];
        
        cell.headerLeft.text = titleArr[0];
        cell.headerRight.text = titleArr[1];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        
        return cell;
    }
    else {
        AgendaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableCellIdentifier forIndexPath:indexPath];
        
        if(attendToSectionHistoryArray != nil) {
            AttendToSectionHistoryModel *model = [attendToSectionHistoryArray objectAtIndex:indexPath.row];
            
            NSString *dateString = [self.dateFormatter stringFromDate:model.scanDate];
            NSString *statusString;
            
            if([model.status intValue] == 0) { // ON TIME (ATTEND)
                statusString = @"เข้าเรียน";
            }
            else if([model.status intValue] == 1) { // LATE
                statusString = @"สาย";
            }
            else if([model.status intValue] == 3) { // ABSENCE
                statusString = @"ขาด";
            }
            else if([model.status intValue] == 7) { // ON LEAVE
                statusString = @"ลา";
            }
            else if([model.status intValue] == 8) { // HOLIDAY
                statusString = @"วันหยุด";
            }
            else {
                statusString = @"";
            }
            
            cell.labelLeft.text = dateString;
            cell.labelRight.text = statusString;
        }
        else {
            cell.labelLeft.text = @"";
            cell.labelRight.text = @"";
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        
        return cell;
    }
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - GetAPIData
-(void)getAttendToSectionHistoryData:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status {
    
    NSString *URLString = [APIURL getAttendToSectionHistoryWithSectionID:self.sectionID startDate:startDate endDate:endDate status:status userType:[UserData getUserType]];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getAttendToSectionHistoryData:startDate endDate:endDate status:status];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getAttendToSectionHistoryData:startDate endDate:endDate status:status];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getAttendToSectionHistoryData:startDate endDate:endDate status:status];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                attendToSectionHistoryArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSString *statusStr = [dataDict objectForKey:@"LogLearnScanStatusIn"];
                    NSMutableString *scanDateTimeStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"LogLearnDate"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) scanDateTimeStr);
                    
                    NSDate *scanDate = [Utils parseServerDateStringToDate:scanDateTimeStr];
                    
                    NSInteger status;
                    
                    if([statusStr isKindOfClass:[NSNull class]] || [statusStr length] == 0) {
                        status = 3; //Absence
                    }
                    else {
                        status = [statusStr integerValue];
                    }
                    
                    if(status != 9) { // store the date except weekend
                        
                        AttendToSectionHistoryModel *model = [[AttendToSectionHistoryModel alloc] init];
                        [model setStatus:[[NSNumber alloc] initWithInteger:status]];
                        [model setScanDate:scanDate];
                        
                        [attendToSectionHistoryArray addObject:model];
                    }
                    
                }
                
                // reverse date descending
                //NSArray *reverse =  [[attendToSectionHistoryArray reverseObjectEnumerator] allObjects];
                //attendToSectionHistoryArray = [[NSMutableArray alloc] initWithArray:reverse];
                
                [self.tableView reloadData];
            }

        }

    }];
   
}

#pragma mark - Filter

-(void)applyFilter:(Attend2SchoolFilter *)filter {
    
    NSInteger status = -1;
    
    if(filter.status == INTIME) {
        status = 0;
    }
    else if(filter.status == LATE) {
        status = 1;
    }
    else if(filter.status == ABSENCE) {
        status = 3;
    }
    else if(filter.status == ONLEAVE) {
        status = 7;
    }
    else if(filter.status == HOLIDAY) {
        status = 8;
    }
    
    self.minimumDate = filter.startDate;
    self.maximumDate = filter.endDate;
    [self getAttendToSectionHistoryData:filter.startDate endDate:filter.endDate status:status];
}
@end
