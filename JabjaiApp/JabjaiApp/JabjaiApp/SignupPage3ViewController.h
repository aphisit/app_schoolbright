//
//  SignupPage3ViewController.h
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupDataModel.h"
#import "DatePickerDialog.h"

@interface SignupPage3ViewController : UIViewController <UITextFieldDelegate, DatePickerDialogDelegate>

@property (nonatomic, strong) NSString *previousSegue;
@property (nonatomic,strong) SignupDataModel *signupDataModel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property (weak, nonatomic) IBOutlet UITextField *telephoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *creditLimitsTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@property (weak, nonatomic) IBOutlet UIStackView *creditsLimitStackView;

- (IBAction)moveBack:(id)sender;
- (IBAction)nextStep:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;

@end
