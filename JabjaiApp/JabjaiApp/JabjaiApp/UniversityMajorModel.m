//
//  UniversityMajorModel.m
//  JabjaiApp
//
//  Created by mac on 11/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "UniversityMajorModel.h"

@implementation UniversityMajorModel

-(NSNumber *)getMajorID {
    return self.majorID;
}

-(NSString *)getMajorName {
    return self.majorName;
}

@end
