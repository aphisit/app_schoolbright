//
//  LoginViewController.m
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

@import AirshipKit;

#import "LoginViewController.h"
#import "APIURL.h"
#import "UserData.h"
#import "SchoolModel.h"
#import "SignupDataModel.h"
#import "SignupSelectTypeViewController.h"
#import "VerifyCodeDialog.h"
#import "AlertDialog.h"
#import "MultilineAlertDialog.h"
#import "Utils.h"
#import "Constant.h"
#import "MultipleUserModel.h"
#import "UserInfoDBHelper.h"

@interface LoginViewController () {
    SchoolModel *schoolModel;
    
    SchoolCodeDialogViewController *schoolCodeDialog;
    VerifyCodeDialog *verifyCodeDialog;
    AlertDialog *alertDialog;
    MultilineAlertDialog *multilineAlertDialog;
    
    NSInteger connectCounter;

}

@property (nonatomic, strong) SignupDataModel *signupDataModel;
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    schoolCodeDialog = [[SchoolCodeDialogViewController alloc] init];
    schoolCodeDialog.delegate = self;
    verifyCodeDialog = [[VerifyCodeDialog alloc] init];
    alertDialog = [[AlertDialog alloc] init];
    multilineAlertDialog = [[MultilineAlertDialog alloc] init];
    
    _signupDataModel = [[SignupDataModel alloc] init];
    
    connectCounter = 0;
    
//    [[UAirship push] addTag:@"test"];
//    [[UAirship push] updateRegistration];
//    
//    NSArray *tags = [[UAirship push] tags];
    
    NSString *channelID = [UAirship push].channelID;
    NSLog(@"%@", [NSString stringWithFormat:@"Channel ID : %@", channelID]);
    
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    
    // Check whether should or shouldn't log out if app already updated new version
    NSString *currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *savedVersion = [UserData getAppVersion];
    
    if(savedVersion == nil) {
        savedVersion = @"0";
    }
    
    NSInteger compareVersionResult = [Utils compareVersion:savedVersion version2:currentVersion];
    // Check version as if updated
    if(logoutIfNewVersion && compareVersionResult < 0) {
        
        [[UAirship push] removeTags:[[UAirship push] tags]];
        [[UAirship push] updateRegistration];
        
        [UserData setUserLogin:NO];
        [UserData saveMasterUserID:0];
        [UserData saveUserID:0];
        [UserData setUserType:USERTYPENULL];
        [UserData setAcademyType:ACADEMYTYPENULL];
        [UserData setFirstName:@""];
        [UserData setLastName:@""];
        [UserData setUserImage:@""];
        [UserData setGender:MALE];
        [UserData setAppVersion:currentVersion];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    
    if([UserData isUserLogin]) {
        
        // Confirm the masterid is not null
        if([UserData getMasterUserID] == -1) {
            NSInteger userId = [UserData getUserID];
            [UserData saveMasterUserID:userId];
        }
        
        [UIView setAnimationsEnabled:NO];
        self.view.hidden = YES;
        
        [self performSegueWithIdentifier:@"mainContentSegue" sender:self];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView setAnimationsEnabled:YES];
            self.view.hidden = NO;
        });
        
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [self registerForKeyboardNotification];
    
    // Docorate the views
    self.loginBtn.layer.cornerRadius = self.loginBtn.frame.size.height / 2.0;
    self.loginBtn.layer.masksToBounds = YES;
    self.signupBtn.layer.cornerRadius = self.signupBtn.frame.size.height / 2.0;
    self.signupBtn.layer.masksToBounds = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self unRegisterForKeyboardNotification];
}

-(void)viewDidAppear:(BOOL)animated {

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"signupSegue"]) {
        SignupSelectTypeViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
}

- (IBAction)forgotPasswordAction:(id)sender {
    NSString *alertTitle = @"แจ้งเตือน";
    NSString *alertMessage = @"ขออภัย ฟังก์ชันนี้ยังไม่เปิดใช้งาน";

    [self showAlertDialogWithTitle:alertTitle message:alertMessage];
}

- (IBAction)signupAction:(id)sender {
    [self showSchoolCodeDialog];
}

- (IBAction)loginAction:(id)sender {
    
    NSString *username = self.usernameTextField.text;
    NSString *password = self.passwordTextField.text;
    
    if(username.length == 0) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = @"กรุณาระบุชื่อผู้ใช้";
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else if(password.length == 0) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = @"กรุณาระบุรหัสผ่าน";
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else {
        self.loginBtn.userInteractionEnabled = NO;
        
        NSString *URLString = [APIURL getLoginURL:username password:password];
        NSURL *url = [NSURL URLWithString:URLString];
        
        [self sendLoginToServer:url];
    }
    
}

- (void)sendLoginToServer:(NSURL *)url {
    
    // Show the indicator
    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        // When receive data from server.Then stop indicator
        [self stopIndicator];
        self.loginBtn.userInteractionEnabled = YES;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self sendLoginToServer:url];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            NSLog(@"test1");
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self sendLoginToServer:url];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSDictionary class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self sendLoginToServer:url];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSLog(@" test2 = %@",returnedData);
                NSDictionary *returnedDict = returnedData;
                connectCounter = 0;
                
                long long userID = [[returnedDict objectForKey:@"ID"] intValue];
                int academyType = [[returnedDict objectForKey:@"Value"] intValue];
                int userType = [[returnedDict objectForKey:@"Type"] intValue];
                long long schoolId = [[returnedData objectForKey:@"SchoolId"] longLongValue];
                int gender = [[returnedData objectForKey:@"sex"] intValue];
                
                NSString *firstName, *lastName, *imageUrl;
                
                if(![[returnedData objectForKey:@"image"] isKindOfClass:[NSNull class]]) {
                    imageUrl = [returnedData objectForKey:@"image"];
                }
                else {
                    imageUrl = @"";
                }
                
                if(![[returnedData objectForKey:@"name"] isKindOfClass:[NSNull class]]) {
                    firstName = [returnedData objectForKey:@"name"];
                }
                else {
                    firstName = @"";
                }
                
                if(![[returnedData objectForKey:@"lastname"] isKindOfClass:[NSNull class]]) {
                    lastName = [returnedData objectForKey:@"lastname"];
                }
                else {
                    lastName = @"";
                }

                
                if(userID > 0) {
                    
                    [UserData saveMasterUserID:userID];
                    [UserData saveUserID:userID];
                    [UserData setUserLogin:YES];
                    [UserData saveSchoolId:schoolId];
                    [UserData setFirstName:firstName];
                    [UserData setLastName:lastName];
                    [UserData setUserImage:imageUrl];
                    
                    if(academyType == 1) {
                        [UserData setAcademyType:SCHOOL];
                    }
                    else {
                        [UserData setAcademyType:UNIVERSITY];
                    }
                    
                    if(userType == 0) {
                        [UserData setUserType:STUDENT];
                    }
                    else {
                        [UserData setUserType:TEACHER];
                    }
                    
                    if(gender == 0) {
                        [UserData setGender:MALE];
                    }
                    else {
                        [UserData setGender:FEMALE];
                    }
                    
                    // Update user into db
                    MultipleUserModel *userModel = [[MultipleUserModel alloc] init];
                    userModel.masterId = userID;
                    userModel.slaveId = userID;
                    userModel.schoolId = schoolId;
                    userModel.userType = userType;
                    userModel.academyType = academyType;
                    userModel.firstName = firstName;
                    userModel.lastName = lastName;
                    userModel.imageUrl = imageUrl;
                    userModel.gender = gender;
                    
                    [self.dbHelper insertData:userModel];
                    
                    NSArray<MultipleUserModel *> *userArray = [self.dbHelper getUserDataWithMasterId:[UserData getMasterUserID]];
                    NSMutableArray<NSString *> *tags = [[NSMutableArray alloc] init];
                    
                    for(MultipleUserModel *model in userArray) {
                        NSString *tag = [NSString stringWithFormat:@"%lld", [model getSlaveId]];
                        [tags addObject:tag];
                    }
                    
                    [[UAirship push] addTags:tags];
                    [[UAirship push] updateRegistration];
                    
                    [self performSegueWithIdentifier:@"mainContentSegue" sender:self];
                    //                }
                    
                }
                else if(userID < 0) {
                    
                    NSString *title = @"ท่านยังไม่ได้ยืนยันลายนิ้วมือ";
                    [self showVerifyCodeDialogWithTitle:title verifyCode:[NSString stringWithFormat:@"%lld", (userID * -1)]];
                    
                }
                else {
                    NSString *alertTitle = @"แจ้งเตือน";
                    NSString *message1 = @"Username / Password ไม่ถูกต้อง";
                    NSString *message2 = @"*สำหรับนักเรียนให้ใช้เลขบัตรนักเรียน";
                    NSString *message3 = @"*สำหรับอาจารย์ให้ใช้เบอร์โทรศัพท์";
                    
//                    NSMutableAttributedString *textMessage = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n\n%@\n%@", message1, message2, message3]];
//                    [textMessage addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(message1.length + 1, message2.length)];
//                    [textMessage addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(message1.length+message2.length+2, message3.length)];
                    
                    NSString *textMessage = [NSString stringWithFormat:@"%@\n\n%@\n%@", message1, message2, message3];
                    [self showMultilineAlertDialogWithTitle:alertTitle message:textMessage];
                }
                
                NSLog(@"Login Result : %lld", userID);

            }
        }
    }];
}

- (IBAction)DismissKeyboard:(id)sender {
    [self resignFirstResponder];
}

#pragma SchoolCodeDialogViewController

-(void)onCancelButtonPress {
    
}

-(void)onOkButtonPress {
    NSString *schoolCode = schoolCodeDialog.passwordTextField.text;
    
    if([schoolCode length] == 0) {
        schoolCodeDialog.hintLabel.textColor = [UIColor redColor];
    }
    else {
        [self getSchoolInfo:schoolCode];
    }
    
    NSLog(@"Enter School Code : %@", schoolCode);
}

#pragma mark - GetDataFromAPI

- (void)getSchoolInfo:(NSString *)schoolCode {
    
    // Show the indicator
    [self showIndicator];
    
    NSString *URLString = [APIURL getSchoolDataWithPasswordURL:schoolCode];
    
    NSLog(@"getSchoolInfo URL : %@", URLString);
    
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSchoolInfo:schoolCode];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolInfo:schoolCode];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolInfo:schoolCode];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if([returnedArray count] == 0) {
                    schoolCodeDialog.hintLabel.text = @"รหัสไม่ถูกต้อง";
                    schoolCodeDialog.hintLabel.textColor = [UIColor redColor];
                }
                else {
                    
                    //If success remove the dialog from parent view
                    [schoolCodeDialog dismissDialog];
                    
                    NSDictionary *schoolDict = [returnedArray objectAtIndex:0];
                    NSNumber *academyType = [[NSNumber alloc] initWithInt:[[schoolDict objectForKey:@"Type"] intValue]];
                    NSNumber *schoolID = [[NSNumber alloc] initWithInt:[[schoolDict objectForKey:@"ID"] intValue]];
                    NSMutableString *schoolName = [[NSMutableString alloc] initWithString:[schoolDict objectForKey:@"Value"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) schoolName);
                    
                    _signupDataModel.academyType = academyType;
                    _signupDataModel.schoolID = schoolID;
                    _signupDataModel.school = schoolName;
                    
                    [self performSegueWithIdentifier:@"signupSegue" sender:self];
                }

            }
        }

    }];
    
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showMultilineAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(multilineAlertDialog != nil && [multilineAlertDialog isDialogShowing]) {
        [multilineAlertDialog dismissDialog];
    }
    
    [multilineAlertDialog showDialogInView:self.view title:title message:message];
}

- (void)showVerifyCodeDialogWithTitle:(NSString *)title verifyCode:(NSString *)verifyCode {
    
    if(verifyCodeDialog != nil && [verifyCodeDialog isDialogShowing]) {
        [verifyCodeDialog dismissDialog];
    }
    
    [verifyCodeDialog showDialogInView:self.view title:title verifyCode:verifyCode];
}

- (void)showSchoolCodeDialog {
    
    if(schoolCodeDialog != nil && [schoolCodeDialog isDialogShowing]) {
        [schoolCodeDialog dismissDialog];
    }
    
    [schoolCodeDialog showDialogInView:self.view];
}

#pragma mark - Keyboard Listener

- (void)registerForKeyboardNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)unRegisterForKeyboardNotification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    self.signupBtn.hidden = YES;

    self.imgLogoWidthConstraint.constant = 128;
    self.imgLogoHeightConstraint.constant = 64;
    self.imgLogoTopConstraint.constant = 5;
    self.buttonStackViewTopConstraint.constant = 5;
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    self.signupBtn.hidden = NO;
    
    self.imgLogoWidthConstraint.constant = 256;
    self.imgLogoHeightConstraint.constant = 128;
    self.imgLogoTopConstraint.constant = 30;
    self.buttonStackViewTopConstraint.constant = 30;
}


@end


