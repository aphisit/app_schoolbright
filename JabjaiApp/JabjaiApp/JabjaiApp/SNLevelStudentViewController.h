//
//  SNLevelStudentViewController.h
//  JabjaiApp
//
//  Created by toffee on 8/28/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableListDialog.h"
#import "SchoolLevelModel.h"
#import "CallGetSchoolClassLevelAPI.h"



@interface SNLevelStudentViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassLevelAPIDelegate>


@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *userTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

- (IBAction)openDrawer:(id)sender;
- (IBAction)actionNext:(id)sender;



@end
