//
//  BSReportCalendarViewControllerDelegate.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSReportScoreListViewController.h"

@protocol BSReportCalendarViewControllerDelegate <NSObject>

@optional
- (void)onSelectDate:(NSDate *)date;

@end


