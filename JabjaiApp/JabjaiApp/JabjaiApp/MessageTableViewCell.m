//
//  MessageTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 5/17/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "MessageTableViewCell.h"

@implementation MessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.dateTimeLabel.text = nil;
    self.titleLabel.text = nil;
    self.messageLabel.text = nil;
    self.statusLabel.text = nil;
    self.statusLabel.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    self.dateTimeLabel.text = nil;
    self.titleLabel.text = nil;
    self.messageLabel.text = nil;
    self.statusLabel.text = nil;
    self.statusLabel.backgroundColor = [UIColor clearColor];
}

@end
