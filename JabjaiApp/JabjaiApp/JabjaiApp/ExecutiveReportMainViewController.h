//
//  ExecutiveReportMainViewController.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CallGetEXReportSummaryAttendSchoolAPI.h"
#import "EXReportClassStatusModel.h"


@interface ExecutiveReportMainViewController : UIViewController <CallGetEXReportSummaryAttendSchoolAPIDelegate>

// Global variables
@property (nonatomic) NSDate *reportDate;
@property (strong, nonatomic) NSArray<EXReportClassStatusModel *> *reportClassStatusArray;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dearLabel;
@property (weak, nonatomic) IBOutlet UILabel *subjectMatterLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfStudentOntimeNumber;
@property (weak, nonatomic) IBOutlet UILabel *numberOfStudentLateNumber;
@property (weak, nonatomic) IBOutlet UILabel *numberOfStudentAbsenceNumber;
@property (weak, nonatomic) IBOutlet UILabel *numberOfTeacherOntimeNumber;
@property (weak, nonatomic) IBOutlet UILabel *numberOfTeacherLateNumber;
@property (weak, nonatomic) IBOutlet UILabel *numberOfTeacherAbsenceNumber;
@property (weak, nonatomic) IBOutlet UILabel *numberOfEmployeeOntimeNumber;
@property (weak, nonatomic) IBOutlet UILabel *numberOfEmployeeLateNumber;
@property (weak, nonatomic) IBOutlet UILabel *numberOfEmployeeAbsenceNumber;
@property (weak, nonatomic) IBOutlet UILabel *totalStudentNumber;
@property (weak, nonatomic) IBOutlet UILabel *totalTeacherNumber;
@property (weak, nonatomic) IBOutlet UILabel *totalEmployeeNumber;

- (IBAction)actionViewDetailed:(id)sender;
- (IBAction)openDrawer:(id)sender;

@end
