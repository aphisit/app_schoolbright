//
//  CallGetStudentInClassroomAPI.m
//  JabjaiApp
//
//  Created by mac on 7/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetStudentInClassroomAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetStudentInClassroomAPI () {
    NSInteger connectCounter;
    NSMutableArray<BSSelectedStudent *> *selectedStudentArray;
}

@end

@implementation CallGetStudentInClassroomAPI

- (void)call:(long long)schoolId classroomId:(long long)classroomId {
    connectCounter = 0;
    [self getStudentInClass:schoolId classroomId:classroomId];
}

#pragma mark - Get API Data

- (void)getStudentInClass:(long long)schoolId classroomId:(long long)classroomId {
    
    NSString *URLString = [APIURL getStudentInClassroomWithSchoolId:schoolId classroomId:classroomId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getStudentInClass:schoolId classroomId:classroomId];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentInClass:schoolId classroomId:classroomId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentInClass:schoolId classroomId:classroomId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(selectedStudentArray != nil) {
                    [selectedStudentArray removeAllObjects];
                    selectedStudentArray = nil;
                }
                
                selectedStudentArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long studentId = [[dataDict objectForKey:@"studentid"] longLongValue];
                    NSMutableString *studentName;
                    
                    if(![[dataDict objectForKey:@"studentname"] isKindOfClass:[NSNull class]]) {
                        studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"studentname"]];
                    }
                    else {
                        studentName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentName);
                    
                    BSSelectedStudent *model = [[BSSelectedStudent alloc] init];
                    [model setStudentId:studentId];
                    [model setStudentName:studentName];
                    [model setSelected:NO];
                    
                    [selectedStudentArray addObject:model];
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetStudentInClassrommAPI:data:success:)]) {
                    [self.delegate callGetStudentInClassrommAPI:self data:selectedStudentArray success:YES];
                }
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetStudentInClassrommAPI:data:success:)]) {
                [self.delegate callGetStudentInClassrommAPI:self data:nil success:NO];
            }
        }


    }];
}

@end
