//
//  UniversityFacultyModel.h
//  JabjaiApp
//
//  Created by mac on 11/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UniversityFacultyModel : NSObject

@property (nonatomic, strong) NSNumber *facultyID;
@property (nonatomic, strong) NSString *facultyName;

-(NSNumber *)getFacultyID;
-(NSString *)getFacultyName;

@end
