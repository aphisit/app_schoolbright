//
//  ExecutiveReportMainViewController.m
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "ExecutiveReportMainViewController.h"
#import "UserData.h"
#import "Utils.h"
#import "ExecutiveReportClassLevelViewController.h"

@interface ExecutiveReportMainViewController ()

@property (strong, nonatomic) CallGetEXReportSummaryAttendSchoolAPI *callGetEXReportSummaryAttendSchoolAPI;

@end

@implementation ExecutiveReportMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    if(self.reportDate == nil) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        
        self.reportDate = [dateFormatter dateFromString:@"07/08/2017"];
//        self.reportDate = [NSDate date];
    }
    
    self.dateLabel.text = [Utils getThaiDateFormatWithDate:self.reportDate];
    
    if(self.reportClassStatusArray == nil) {
        [self getReportSummaryAttendSchool];
    }
    else {
        [self updateTableData:self.reportClassStatusArray];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - API Caller

- (void)getReportSummaryAttendSchool {
    
    self.callGetEXReportSummaryAttendSchoolAPI = nil;
    self.callGetEXReportSummaryAttendSchoolAPI = [[CallGetEXReportSummaryAttendSchoolAPI alloc] init];
    self.callGetEXReportSummaryAttendSchoolAPI.delegate = self;
    [self.callGetEXReportSummaryAttendSchoolAPI call:[UserData getSchoolId] date:self.reportDate];
}

#pragma mark - CallGetEXReportSummaryAttendSchoolAPIDelegate

- (void)callGetEXReportSummaryAttendSchoolAPI:(CallGetEXReportSummaryAttendSchoolAPI *)classObj data:(NSArray<EXReportClassStatusModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        self.reportClassStatusArray = data;
        [self updateTableData:data];
    }
}

#pragma mark - Utility

- (void)updateTableData:(NSArray<EXReportClassStatusModel *> *)data {
    
    NSInteger totalStudent = 0;
    NSInteger totalStudentOnTimeNumber = 0;
    NSInteger totalStudentLateNumber = 0;
    NSInteger totalStudentAbsenceNumber = 0;
    
    NSInteger totalTeacher = 0;
    NSInteger totalTeacherOnTimeNumber = 0;
    NSInteger totalTeacherLateNumber = 0;
    NSInteger totalteacherAbsenceNumber = 0;
    
    NSInteger totalEmployee = 0;
    NSInteger totalEmployeeOnTimeNumber = 0;
    NSInteger totalEmployeeLateNumber = 0;
    NSInteger totalEmployeeAbsenceNumber = 0;
    
    if(data != nil) {
        
        for(EXReportClassStatusModel *model in data) {
            
            if([model getIId] == -1) { // Teacher
                totalTeacherOnTimeNumber = [model getNumberOfOnTimeStatus];
                totalTeacherLateNumber = [model getNumberOfLateStatus];
                totalteacherAbsenceNumber = [model getNumberOfAbsenceStatus];
                totalTeacher = totalTeacherOnTimeNumber + totalTeacherLateNumber + totalteacherAbsenceNumber;
            }
            else if([model getIId] == -2) { // Employee
                totalEmployeeOnTimeNumber = [model getNumberOfOnTimeStatus];
                totalEmployeeLateNumber = [model getNumberOfLateStatus];
                totalEmployeeAbsenceNumber = [model getNumberOfAbsenceStatus];
                totalEmployee = totalEmployeeOnTimeNumber + totalEmployeeLateNumber + totalEmployeeAbsenceNumber;
            }
            else {
                totalStudentOnTimeNumber += [model getNumberOfOnTimeStatus];
                totalStudentLateNumber += [model getNumberOfLateStatus];
                totalStudentAbsenceNumber += [model getNumberOfAbsenceStatus];
            }
            
        }
        
        totalStudent = totalStudentOnTimeNumber + totalStudentLateNumber + totalStudentAbsenceNumber;
    }
    
    self.numberOfStudentOntimeNumber.text = [NSString stringWithFormat:@"%ld", totalStudentOnTimeNumber];
    self.numberOfStudentLateNumber.text = [NSString stringWithFormat:@"%ld", totalStudentLateNumber];
    self.numberOfStudentAbsenceNumber.text = [NSString stringWithFormat:@"%ld", totalStudentAbsenceNumber];
    
    self.numberOfTeacherOntimeNumber.text = [NSString stringWithFormat:@"%ld", totalTeacherOnTimeNumber];
    self.numberOfTeacherLateNumber.text = [NSString stringWithFormat:@"%ld", totalTeacherLateNumber];
    self.numberOfTeacherAbsenceNumber.text = [NSString stringWithFormat:@"%ld", totalteacherAbsenceNumber];
    
    self.numberOfEmployeeOntimeNumber.text = [NSString stringWithFormat:@"%ld", totalEmployeeOnTimeNumber];
    self.numberOfEmployeeLateNumber.text = [NSString stringWithFormat:@"%ld", totalEmployeeLateNumber];
    self.numberOfEmployeeAbsenceNumber.text = [NSString stringWithFormat:@"%ld", totalEmployeeAbsenceNumber];
    
    self.totalStudentNumber.text = [NSString stringWithFormat:@"%ld", totalStudent];
    self.totalTeacherNumber.text = [NSString stringWithFormat:@"%ld", totalTeacher];
    self.totalEmployeeNumber.text = [NSString stringWithFormat:@"%ld", totalEmployee];
}

#pragma mark - Action functions

- (IBAction)actionViewDetailed:(id)sender {
    if(self.reportClassStatusArray != nil) {
        ExecutiveReportClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecutiveReportClassLevelStoryboard"];
        viewController.reportDate = _reportDate;
        viewController.reportClassStatusArray = _reportClassStatusArray;
        
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
}

- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}
@end
