//
//  NavigationViewController.m
//  JabjaiApp
//
//  Created by mac on 10/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

@import AirshipKit;

#import "NavigationViewController.h"
#import "NavTableViewCell.h"
#import "NavChildTableViewCell.h"
#import "NavUserTableViewCell.h"
#import "MessageInboxViewController.h"
#import "TASelectClassLevelViewController.h"
#import "BSSelectClassLevelViewController.h"
#import "InboxMessageDBManager.h"
#import "UserData.h"
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserInfoDBHelper.h"
#import "MultipleUserModel.h"
#import "AddAccountLoginViewController.h"
#import <Sheriff/GIBadgeView.h>

@interface NavigationViewController () {
    
    NSMutableArray *cellDescriptors;
    NSMutableArray *visibleRowsPerSection;
    //SectionArray *visibleRowsPerSection;
    
    NSInteger connectCounter;
    
    NSMutableArray<MultipleUserModel *> *userArray;
    
    MultipleUserModel *userModelForFloatingUserIcon1;
    MultipleUserModel *userModelForFloatingUserIcon2;
    
    BOOL modeShowUser;
}

@property (nonatomic, strong) InboxMessageDBManager *inboxMessageDatabaseManager;
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (nonatomic, strong) GIBadgeView *floatingIcon1BadgeView;
@property (nonatomic, strong) GIBadgeView *floatingIcon2BadgeView;

@property (nonatomic, strong) CallGetUnreadMessageCountAPI *callGetUnreadMessageCountAPI;

@end

static NSString *tableCellIdentifier = @"mainNavCell";
static NSString *tableChildCellIdentifier = @"childNavCell";
static NSString *cellIdentifier = @"UserCell";

static NSString *floatingUserIcon1RequestCode = @"icon1RequestCode";
static NSString *floatingUserIcon2RequestCode = @"icon2RequestCode";

@implementation NavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setModeShowUser:NO];
    connectCounter = 0;
    
    visibleRowsPerSection = [[NSMutableArray alloc] init];
    
    self.tblExpandable.dataSource = self;
    self.tblExpandable.delegate = self;
    
    [self configureTableView];
    [self loadCellDescriptor];
    
    //setup inbox database manager
    self.inboxMessageDatabaseManager = [[InboxMessageDBManager alloc] init];
    
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    userArray = [[NSMutableArray alloc] init];
    [self updateUserList];
    
    
    //[self getUserInfoData];
    [self updateNavbarInfo];
    
    self.floatingIcon1BadgeView = [GIBadgeView new];
    [self.viewIcon1 addSubview:self.floatingIcon1BadgeView];
    self.viewIcon1.clipsToBounds = NO;
    self.viewIcon1.backgroundColor = [UIColor clearColor];
    
    self.floatingIcon2BadgeView = [GIBadgeView new];
    [self.viewIcon2 addSubview:self.floatingIcon2BadgeView];
    self.viewIcon2.clipsToBounds = NO;
    self.viewIcon2.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapSelectFloatingUserIcon:)];
    [self.floatingUserIcon1 addGestureRecognizer:tap1];
    [self.floatingUserIcon1 setMultipleTouchEnabled:YES];
    [self.floatingUserIcon1 setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapSelectFloatingUserIcon:)];
    [self.floatingUserIcon2 addGestureRecognizer:tap2];
    [self.floatingUserIcon2 setMultipleTouchEnabled:YES];
    [self.floatingUserIcon2 setUserInteractionEnabled:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureTableView {
    [self.tblExpandable registerNib:[UINib nibWithNibName:NSStringFromClass([NavTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableCellIdentifier];
    [self.tblExpandable registerNib:[UINib nibWithNibName:NSStringFromClass([NavChildTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableChildCellIdentifier];
    [self.tblExpandable registerNib:[UINib nibWithNibName:NSStringFromClass([NavUserTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    [self.tblExpandable setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self.tblExpandable setSeparatorColor:[UIColor clearColor]];
}

- (void)viewWillAppear:(BOOL)animated {
    
    CGFloat max = MAX(self.userImage.frame.size.width, self.userImage.frame.size.height);
    self.userImage.layer.cornerRadius = max / 2.0;
    self.userImage.layer.masksToBounds = YES;
    
    max = MAX(self.floatingUserIcon1.frame.size.width, self.floatingUserIcon1.frame.size.height);
    self.floatingUserIcon1.layer.cornerRadius = max / 2.0;
    self.floatingUserIcon1.layer.masksToBounds = YES;
    
    self.floatingUserIcon2.layer.cornerRadius = max / 2.0;
    self.floatingUserIcon2.layer.masksToBounds = YES;
    
    [self updateUserList];
    [self updateNavbarInfo];
    [self.tblExpandable reloadData];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma TableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(modeShowUser) {
        return 1;
    }
    else {
        
        if(cellDescriptors != nil) {
            return cellDescriptors.count;
        }
        
        return 0;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(modeShowUser) {
        
        if(userArray != nil) {
            return userArray.count + 1;
        }
        else {
            return 1;
        }
        
    }
    else {
        NSLog(@"count = %d",[[visibleRowsPerSection objectAtIndex:section] count]);
        
        return [[visibleRowsPerSection objectAtIndex:section] count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
       if(modeShowUser) {
                NSLog(@"modeShowUser1 = %d" , modeShowUser);
                NavUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(userArray == nil || userArray.count == 0 || indexPath.row > userArray.count - 1) {
            
            cell.imgUser.image = [UIImage imageNamed:@"ic_gray_plus"];
            cell.titleLabel.text = @"เพิ่มผู้ใช้";
        }
        else {
            
            MultipleUserModel *model = [userArray objectAtIndex:indexPath.row];
            
            if([model getImageUrl] == nil || [[model getImageUrl] length] == 0) {
                
                if([model getGender] == 0) { // Male
                    cell.imgUser.image = [UIImage imageNamed:@"ic_user_info"];
                }
                else { // Female
                    cell.imgUser.image = [UIImage imageNamed:@"ic_user_women"];
                }
            }
            else {
                [cell.imgUser loadImageFromURL:[model getImageUrl]];
            }
            
            NSString *name = [[NSString alloc] initWithFormat:@"%@ %@", [model getFirstName], [model getLastName]];
            cell.titleLabel.text = name;

        }
        
        return cell;
    }
    else {
        NSMutableDictionary *currentCellDescriptor = [self getCellDescriptorForIndexPath:indexPath];
        NSString *cellIdentifier = [currentCellDescriptor objectForKey:@"cellIdentifier"];
        NSString *title = [currentCellDescriptor objectForKey:@"title"];
        NSString *storyboardIdentifier = [currentCellDescriptor objectForKey:@"storyboardIdentifier"];
        //NSLog(@"title = %s" , title);
        
        if([cellIdentifier isEqualToString:tableCellIdentifier]) {
            NSLog(@"cellIdentifier = %@" , cellIdentifier);
            NavTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            
            NSString *icon = [currentCellDescriptor objectForKey:@"icon"];
            NSInteger additionalRows = [[currentCellDescriptor objectForKey:@"additionalRows"] integerValue];
            Boolean isExpand = [[currentCellDescriptor objectForKey:@"isExpanded"] boolValue];
            
            //when it is cell of inbox get number of unread message
            if([title isEqualToString:@"กล่องข้อความ"]) {
                int unreadMessage = [self.inboxMessageDatabaseManager getNumberOfUnreadMessageWithUserID:(int)[UserData getUserID]];
                
                if(unreadMessage > 0) {
                    title = [NSString stringWithFormat:@"%@ (%d)", title, unreadMessage];
                }
                
            }
            
            cell.navLabel.text = title;
            cell.navIcon.image = [UIImage imageNamed:icon];
            
            if(additionalRows == 0) {
                [cell.navArrow setHidden:YES];
            }
            else {
                [cell.navArrow setHidden:NO];
            }
            
            if(isExpand) {
                if(!cell.navArrow.isHidden) {
                    cell.navArrow.image = [UIImage imageNamed:@"ic_sort_up"];
                }
            }
            else {
                if(!cell.navArrow.isHidden) {
                    cell.navArrow.image = [UIImage imageNamed:@"ic_sort_down"];
                }
            }
            
            return cell;
        }
        else if([cellIdentifier isEqualToString:tableChildCellIdentifier]) {
            NavChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            
            //check inbox cell
            if([storyboardIdentifier isEqualToString:@"MessageInboxStoryboard"]) {
                int actualRowIndex = [[[visibleRowsPerSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] intValue];
                int unreadMessage = [self.inboxMessageDatabaseManager getNumberOfUnreadMessageWithUserID:(int)[UserData getUserID] typeID:actualRowIndex];
                
                if(unreadMessage > 0) {
                    title = [NSString stringWithFormat:@"%@ (%d)", title, unreadMessage];
                }
                
            }
            
            cell.navLabel.text = title;
            
            return cell;
        }
    }
    
    return [[UITableViewCell alloc] init];
}

#pragma TableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(modeShowUser) {
        
        [self setModeShowUser:NO];
        
        if(userArray == nil || userArray.count == 0 || indexPath.row > userArray.count - 1) { // When select add account
            AddAccountLoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAccountLoginStoryboard"];
            viewController.userArray = userArray;
            [self.revealViewController pushFrontViewController:viewController animated:YES];
        }
        else {
            
            MultipleUserModel *model = [userArray objectAtIndex:indexPath.row];
            
            // Login with selected user
            [self reLogin:model];
            
            UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
            
            [self.revealViewController pushFrontViewController:viewController animated:YES];
            
            //[self.revealViewController revealToggle:self.revealViewController];
        }
    }
    else {
        NSMutableDictionary *currentCellDescriptor = [self getCellDescriptorForIndexPath:indexPath];
        NSInteger indexOfTappedRow = [[[visibleRowsPerSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] integerValue];
        Boolean isExpandable = [[currentCellDescriptor objectForKey:@"isExpandable"] boolValue];
        
        if(isExpandable) {
            Boolean shouldExpandAndShowSubRows = NO;
            Boolean isExpanded = [[currentCellDescriptor objectForKey:@"isExpanded"] boolValue];
            NSInteger additionalRows = [[currentCellDescriptor objectForKey:@"additionalRows"] integerValue];
            
            if(!isExpanded) {
                shouldExpandAndShowSubRows = YES;
            }
            
            
            [[[cellDescriptors objectAtIndex:indexPath.section] objectAtIndex:indexOfTappedRow] setObject:[NSNumber numberWithBool:shouldExpandAndShowSubRows] forKey:@"isExpanded"];
            NSInteger last_index = indexOfTappedRow + additionalRows;
            
            for(NSInteger i=indexOfTappedRow+1; i<=last_index; i++) {
                [[[cellDescriptors objectAtIndex:indexPath.section] objectAtIndex:i] setObject:[NSNumber numberWithBool:shouldExpandAndShowSubRows] forKey:@"isVisible"];
            }
        }
        else {
            //Go to another view
            
            SWRevealViewController *revealViewController = self.revealViewController;
            
            NSString *storyboardIdentifier = [currentCellDescriptor objectForKey:@"storyboardIdentifier"];
            
            if([storyboardIdentifier isEqualToString:@"MessageInboxStoryboard"]) {
                
                MessageInboxViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                viewController.message_id = -1; // -1 mean do not show dialog
                
                if(indexOfTappedRow == 1) {
                    viewController.inboxType = ATTENDANCE;
                }
                else if(indexOfTappedRow == 2) {
                    viewController.inboxType = PURCHASING;
                }
                else if(indexOfTappedRow == 3) {
                    viewController.inboxType = TOPUP;
                }
                else if(indexOfTappedRow == 4) {
                    viewController.inboxType = ABSENCEREQUEST;
                }
                else if(indexOfTappedRow == 5) {
                    viewController.inboxType = NEWS;
                }
                
                [revealViewController pushFrontViewController:viewController animated:YES];
            }
            else if([storyboardIdentifier isEqualToString:@"LoginStoryboard"]) {
                
                // Logout
                
                [[UAirship push] removeTags:[[UAirship push] tags]];
                [[UAirship push] updateRegistration];
                
                [UserData setUserLogin:NO];
                [UserData saveMasterUserID:0];
                [UserData saveUserID:0];
                [UserData setUserType:USERTYPENULL];
                [UserData setAcademyType:ACADEMYTYPENULL];
                [UserData setFirstName:@""];
                [UserData setLastName:@""];
                [UserData setUserImage:@""];
                [UserData setGender:MALE];
                
                [self performSegueWithIdentifier:@"LoginSegue" sender:self];
            }
            else if([storyboardIdentifier isEqualToString:@"TakeClassAttendanceStoryboard"]) {
                
                TASelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                
                if(indexOfTappedRow == 1) {
                    viewController.mode = TA_MODE_CHECK;
                }
                else {
                    viewController.mode = TA_MODE_EDIT;
                }
                
                [revealViewController pushFrontViewController:viewController animated:YES];
            }
            else if([storyboardIdentifier isEqualToString:@"BSSelectClassLevelStoryboard"]) {
                
                BSSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                
                if(indexOfTappedRow == 1) {
                    viewController.mode = BS_MODE_ADD;
                }
                else {
                    viewController.mode = BS_MODE_REDUCE;
                }
                
                [revealViewController pushFrontViewController:viewController animated:YES];
            }
            else {
                UIViewController *nextView = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
                
                [revealViewController pushFrontViewController:nextView animated:YES];
            }
            
            
        }
        
        [self updateIndicesOfVisibleRows];
        [self.tblExpandable reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
    
}

#pragma CellDescriptionManangment
- (void)loadCellDescriptor {
    
    NSString *filePath = nil;
    
    if([UserData getAcademyType] == SCHOOL && [UserData getUserType] == STUDENT) {
        filePath = [[NSBundle mainBundle] pathForResource:@"NavItemDescriptor" ofType:@"plist"];
    }
    else if([UserData getAcademyType] == SCHOOL && [UserData getUserType] == TEACHER) {
        filePath = [[NSBundle mainBundle] pathForResource:@"SchoolTeacherNavItemDescriptor" ofType:@"plist"];
    }
    else if([UserData getAcademyType] == UNIVERSITY && [UserData getUserType] == STUDENT) {
        filePath = [[NSBundle mainBundle] pathForResource:@"UniversityStudentNavItemDescriptor" ofType:@"plist"];
    }
    else if([UserData getAcademyType] == UNIVERSITY && [UserData getUserType] == TEACHER) {
        filePath = [[NSBundle mainBundle] pathForResource:@"UniversityTeacherNavItemDescriptor" ofType:@"plist"];
    }
    
    cellDescriptors = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    
    [self updateIndicesOfVisibleRows];
    
    [self.tblExpandable reloadData];
}

- (void)updateIndicesOfVisibleRows {
    [visibleRowsPerSection removeAllObjects];
    
    for(int section=0; section<cellDescriptors.count; section++) {
 
        NSMutableArray *visibleRow = [[NSMutableArray alloc] init];
        NSMutableArray *currentSectionCells = [cellDescriptors objectAtIndex:section];
        
        for(int row=0; row<currentSectionCells.count; row++) {
            NSMutableDictionary *dict = [currentSectionCells objectAtIndex:row];
            
            NSString *title = [dict objectForKey:@"title"];
            Boolean isVisible = [[dict objectForKey:@"isVisible"] boolValue];
            
            if(isVisible) {
                [visibleRow addObject:[NSNumber numberWithInt:row]];
                
                //NSLog([NSString stringWithFormat:@"section = %i, row = %i, title = %@ isVisible = YES", section, row, title]);

            }
//            else {
//                NSLog([NSString stringWithFormat:@"section = %i, row = %i, title = %@ isVisible = NO", section, row, title]);
//            }
        }
        
        [visibleRowsPerSection addObject:visibleRow];
    }
    
    NSLog(@"Finish Update");
}

- (NSMutableDictionary*) getCellDescriptorForIndexPath:(NSIndexPath*)indexPath {
    
    //Get actual index of celldescriptor from index of visual row
    NSMutableArray *visibleRow =[visibleRowsPerSection objectAtIndex:indexPath.section];
    NSInteger indexOfVisibleRow = [[visibleRow objectAtIndex:indexPath.row] integerValue];
    NSMutableDictionary *descriptor = [[cellDescriptors objectAtIndex:indexPath.section] objectAtIndex:indexOfVisibleRow];
    
    return descriptor;
}
- (IBAction)viewProfile:(id)sender {
    
    UIViewController *nextView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileStoryboard"];
    [self.revealViewController pushFrontViewController:nextView animated:YES];
    
}

- (IBAction)actionShowUser:(id)sender {
    
    [self setModeShowUser:!modeShowUser];
    
    if(modeShowUser) {
        [self updateUserList];
    }
    
    [self.tblExpandable reloadData];
}

#pragma mark - GetAPIData

- (void)getUserInfoData {
    NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUserInfoData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(returnedArray.count != 0) {
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    NSMutableString *firstName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sName"]];
                    NSMutableString *lastName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sLastname"]];
                    NSInteger sex = [[dataDict objectForKey:@"cSex"] integerValue];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) firstName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lastName);
                    
                    self.userNameLabel.text = [[NSString alloc] initWithFormat:@"%@ %@", firstName, lastName];
                    
                    if(sex == 0) { // Male
                        self.userImage.image = [UIImage imageNamed:@"ic_man"];
                    }
                    else {
                        // Female
                        self.userImage.image = [UIImage imageNamed:@"ic_woman"];
                    }
                }
            }

        }
        
    }];

}

- (void)getUnreadMessageCountWithUserId:(long long)userId requestCode:(NSString *)requestCode {
    
    self.callGetUnreadMessageCountAPI = nil;
    self.callGetUnreadMessageCountAPI = [[CallGetUnreadMessageCountAPI alloc] init];
    self.callGetUnreadMessageCountAPI.delegate = self;
    [self.callGetUnreadMessageCountAPI call:userId requestCode:requestCode];
}

#pragma mark - CallGetUnreadMessageCountAPIDelegate
- (void)callGetUnreadMessageCountAPI:(CallGetUnreadMessageCountAPI *)classObj unreadCount:(NSInteger)unreadCount requestCode:(NSString *)requestCode success:(BOOL)success {
    
    if(success) {
        
        if([requestCode isEqualToString:floatingUserIcon1RequestCode]) {
            [self.floatingIcon1BadgeView setBadgeValue:unreadCount];
        }
        else if([requestCode isEqualToString:floatingUserIcon2RequestCode]) {
            [self.floatingIcon2BadgeView setBadgeValue:unreadCount];
        }
    }
    else {
        if([requestCode isEqualToString:floatingUserIcon1RequestCode]) {
            [self.floatingIcon1BadgeView setBadgeValue:0];
        }
        else if([requestCode isEqualToString:floatingUserIcon2RequestCode]) {
            [self.floatingIcon2BadgeView setBadgeValue:0];
        }
    }
}

#pragma mark - Utility

- (void)reLogin:(MultipleUserModel *)model {
    
    [UserData saveUserID:[model getSlaveId]];
    [UserData setUserLogin:YES];
    [UserData saveSchoolId:[model getSchoolId]];
    [UserData setFirstName:[model getFirstName]];
    [UserData setLastName:[model getLastName]];
    [UserData setUserImage:[model getImageUrl]];
    
    if([model getAcademyType] == 1) {
        [UserData setAcademyType:SCHOOL];
    }
    else {
        [UserData setAcademyType:UNIVERSITY];
    }
    
    if([model getUserType] == 0) {
        [UserData setUserType:STUDENT];
    }
    else {
        [UserData setUserType:TEACHER];
    }
    
    if([model getGender] == 0) {
        [UserData setGender:MALE];
    }
    else {
        [UserData setGender:FEMALE];
    }

    // Clear expandable navbar item
    
    if(cellDescriptors != nil) {
        [cellDescriptors removeAllObjects];
        cellDescriptors = nil;
    }
    cellDescriptors = [[NSMutableArray alloc] init];
    
    if(visibleRowsPerSection != nil) {
        [visibleRowsPerSection removeAllObjects];
        visibleRowsPerSection = nil;
    }
    
    visibleRowsPerSection = [[NSMutableArray alloc] init];
    
    [self updateUserList];
    [self updateNavbarInfo];
    [self loadCellDescriptor];
    [self.tblExpandable reloadData];
}

- (void)updateUserList {
    
    NSArray *users = [self.dbHelper getUserDataWithMasterId:[UserData getMasterUserID]];
    
    if(userArray != nil) {
        [userArray removeAllObjects];
        userArray = nil;
    }
    userArray = [[NSMutableArray alloc] init];
    
    if(users != nil) {
        for(MultipleUserModel *model in users) {
            if([model getSlaveId] != [UserData getUserID]) {
                [userArray addObject:model];
            }
        }
    }
}

- (void)updateNavbarInfo {
    
    NSString *name = [[NSString alloc] initWithFormat:@"%@ %@", [UserData getFirstName], [UserData getLastName]];
    NSString *imageUrl = [UserData getUserImage];
    
    self.userNameLabel.text = name;
    
    if(imageUrl == nil || imageUrl.length == 0) {
        
        if([UserData getGender] == MALE) {
            self.userImage.image = [UIImage imageNamed:@"ic_user_info"];
        }
        else {
            self.userImage.image = [UIImage imageNamed:@"ic_user_women"];
        }

    }
    else {
        [self.userImage loadImageFromURL:imageUrl];
    }
    
    [self updateFloatingIcon];
}

- (void)updateFloatingIcon {
    
    if(userArray == nil || userArray.count == 0) {
        self.floatingUserIcon1.hidden = YES;
        self.floatingUserIcon2.hidden = YES;
    }
    else if(userArray.count == 1) {
        self.floatingUserIcon1.hidden = YES;
        self.floatingUserIcon2.hidden = NO;
        
        userModelForFloatingUserIcon2 = [userArray objectAtIndex:0];
        
        NSString *imageUrl = [userModelForFloatingUserIcon2 getImageUrl];
        
        if(imageUrl == nil || imageUrl.length == 0) {
            if([userModelForFloatingUserIcon2 getGender] == 0) { // male
                self.floatingUserIcon2.image = [UIImage imageNamed:@"ic_user_info"];
            }
            else {
                self.floatingUserIcon2.image = [UIImage imageNamed:@"ic_user_women"];
            }
        }
        else {
            [self.floatingUserIcon2 loadImageFromURL:imageUrl];
        }
        
        [self getUnreadMessageCountWithUserId:[userModelForFloatingUserIcon2 getSlaveId] requestCode:floatingUserIcon2RequestCode];
    }
    else {
        self.floatingUserIcon1.hidden = NO;
        self.floatingUserIcon2.hidden = NO;
        
        userModelForFloatingUserIcon1 = [userArray objectAtIndex:0];
        userModelForFloatingUserIcon2 = [userArray objectAtIndex:1];
        
        NSString *imageUrl1 = [userModelForFloatingUserIcon1 getImageUrl];
        NSString *imageUrl2 = [userModelForFloatingUserIcon2 getImageUrl];
        
        if(imageUrl1 == nil || imageUrl1.length == 0) {
            if([userModelForFloatingUserIcon1 getGender] == 0) { // male
                self.floatingUserIcon1.image = [UIImage imageNamed:@"ic_user_info"];
            }
            else {
                self.floatingUserIcon1.image = [UIImage imageNamed:@"ic_user_women"];
            }
        }
        else {
            [self.floatingUserIcon1 loadImageFromURL:imageUrl1];
        }

        
        if(imageUrl2 == nil || imageUrl2.length == 0) {
            if([userModelForFloatingUserIcon2 getGender] == 0) { // male
                self.floatingUserIcon2.image = [UIImage imageNamed:@"ic_user_info"];
            }
            else {
                self.floatingUserIcon2.image = [UIImage imageNamed:@"ic_user_women"];
            }
        }
        else {
            [self.floatingUserIcon2 loadImageFromURL:imageUrl2];
        }
        
        [self getUnreadMessageCountWithUserId:[userModelForFloatingUserIcon1 getSlaveId] requestCode:floatingUserIcon1RequestCode];
        [self getUnreadMessageCountWithUserId:[userModelForFloatingUserIcon2 getSlaveId] requestCode:floatingUserIcon2RequestCode];

    }
}

- (void)setModeShowUser:(BOOL)show {
    
    modeShowUser = show;
    
    if(modeShowUser) {
        [self.btnDropdown setImage:[UIImage imageNamed:@"ic_dropdown_up_white"] forState:UIControlStateNormal];
    }
    else {
        [self.btnDropdown setImage:[UIImage imageNamed:@"ic_dropdown_white"] forState:UIControlStateNormal];
    }
}

#pragma mark - TapGesture
- (void)onTapSelectFloatingUserIcon:(UITapGestureRecognizer *)gesture {
    
    if(gesture.view.tag == 1) { // select floating user icon 1
        
        [self setModeShowUser:NO];
        
        // Login with selected user
        [self reLogin:userModelForFloatingUserIcon1];
        
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
        
        [self.revealViewController pushFrontViewController:viewController animated:YES];

    }
    else if(gesture.view.tag == 2) { // select floating user icon 2
        
        [self setModeShowUser:NO];
        
        // Login with selected user
        [self reLogin:userModelForFloatingUserIcon2];
        
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
        
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
}

@end
