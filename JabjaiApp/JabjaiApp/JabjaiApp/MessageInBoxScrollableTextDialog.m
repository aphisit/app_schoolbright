//
//  MessageBoxScrollableTextDialog.m
//  JabjaiApp
//
//  Created by mac on 11/24/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "MessageInBoxScrollableTextDialog.h"
#import "Utils.h"

@interface MessageInBoxScrollableTextDialog () {
    
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, assign) BOOL isShowing;

@end

@implementation MessageInBoxScrollableTextDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Decorate view
    self.dialogView.layer.cornerRadius = 10;
    self.dialogView.layer.masksToBounds = YES;
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10,10)].CGPath;
    self.headerView.layer.mask = maskLayer;
    self.headerView.layer.masksToBounds = YES;
    
    self.closeBtn.layer.cornerRadius = 20;
    self.closeBtn.layer.masksToBounds = YES;
    
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm 'น.'"];
    
    [self.messageTextView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:NULL];
    [self.messageTextView setEditable:NO];
    
    self.isShowing = NO;

}

- (void)dealloc {
    [self.messageTextView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    //Align textview content center vertically
    UITextView *tv = object;
    CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])/2.0;
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
    [tv setContentInset:UIEdgeInsetsMake(topCorrect,0,0,0)];
}

- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

-(void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message messageDate:(NSDate *)messageDate {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    [self.titleLabel setText:title];
    [self.messageTextView setText:message];
    [self.dateLabel setText:[self.dateFormatter stringFromDate:messageDate]];
    
    self.isShowing = YES;

    
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
    
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(messageInBoxScrollableTextClose)]) {
        [self.delegate messageInBoxScrollableTextClose];
    }
    
}

-(BOOL)isDialogShowing {
    return self.isShowing;
}

@end
