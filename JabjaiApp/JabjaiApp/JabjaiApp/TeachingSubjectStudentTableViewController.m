//
//  TeachingSubjectStudentTableViewController.m
//  JabjaiApp
//
//  Created by mac on 12/30/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "TeachingSubjectStudentTableViewController.h"
#import "AgendaHeaderTableViewCell.h"
#import "AgendaTableViewCell.h"
#import "APIURL.h"
#import "StudentInSectionModel.h"
#import "Constant.h"
#import "Utils.h"

@interface TeachingSubjectStudentTableViewController () {
    NSArray *titleArr;
    
    NSMutableArray<StudentInSectionModel *> *studentInSectionArray;
    
    NSInteger connectCounter;
}

@end

static NSString *tableHeaderCellIdentifier = @"HeaderCell";
static NSString *tableCellIdentifier = @"Cell";

@implementation TeachingSubjectStudentTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AgendaHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableHeaderCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AgendaTableViewCell class]) bundle:nil] forCellReuseIdentifier:tableCellIdentifier];
    
    titleArr = @[@"รหัส", @"ชื่อ"];
    
    connectCounter = 0;
    
    [self getStudentInSectionData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(section == 0) {
        return 1;
    }
    else {
        
        if(studentInSectionArray == nil) {
            return 0;
        }
        else {
            return studentInSectionArray.count;
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        AgendaHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableHeaderCellIdentifier forIndexPath:indexPath];
        
        cell.headerLeft.text = titleArr[0];
        cell.headerRight.text = titleArr[1];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        
        return cell;
    }
    else {
        
        AgendaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableCellIdentifier forIndexPath:indexPath];
        
        StudentInSectionModel *model = [studentInSectionArray objectAtIndex:indexPath.row];
        cell.labelLeft.text = model.studentID;
        cell.labelRight.text = model.studentName;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Get API Data

- (void) getStudentInSectionData {
    NSString *URLString = [APIURL getStudentsInSectionUrl:self.sectionID];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getStudentInSectionData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentInSectionData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentInSectionData];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                studentInSectionArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int sectionID = [[dataDict objectForKey:@"SectionId"] intValue];
                    NSMutableString *studentID = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"StudentId"]];
                    NSMutableString *studentName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Name"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentID);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentName);
                    
                    StudentInSectionModel *model = [[StudentInSectionModel alloc] init];
                    model.sectionID = sectionID;
                    model.studentID = studentID;
                    model.studentName = studentName;
                    
                    [studentInSectionArray addObject:model];
                }
                
                [self.tableView reloadData];
            }

        }

    }];
    
}

@end
