//
//  CallGetStudentInClassAPI.h
//  JabjaiApp
//
//  Created by mac on 7/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAStudentStatusModel.h"

@class CallGetStudentInClassAPI;

@protocol CallGetStudentInClassAPIDelegate <NSObject>

@optional
- (void)callGetStudentInClassAPI:(CallGetStudentInClassAPI *)classObj data:(NSArray<TAStudentStatusModel *> *)data success:(BOOL)success;

@end

@interface CallGetStudentInClassAPI : NSObject

@property (nonatomic, weak) id<CallGetStudentInClassAPIDelegate> delegate;

- (void)call:(long long)schoolId classroomId:(long long)classroomId subjectId:(long long)subjectId teacherId:(long long)teacherId;

@end
