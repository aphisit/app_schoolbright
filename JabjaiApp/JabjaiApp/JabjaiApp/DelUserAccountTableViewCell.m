//
//  DelUserAccountTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "DelUserAccountTableViewCell.h"

@implementation DelUserAccountTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat max = MAX(self.imgUser.frame.size.width, self.imgUser.frame.size.height);
    self.imgUser.layer.cornerRadius = max / 2.0;
    self.imgUser.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionDeleteUser:(id)sender {
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onDeleteUser:atIndex:)]) {
        [self.delegate onDeleteUser:self atIndex:_index];
    }
}
@end
