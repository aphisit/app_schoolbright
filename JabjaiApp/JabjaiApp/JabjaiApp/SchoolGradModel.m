//
//  SchoolGradModel.m
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SchoolGradModel.h"

@implementation SchoolGradModel

-(NSNumber *)getGradID {
    return self.gradID;
}

-(NSString *)getGradName {
    return self.gradName;
}

@end
