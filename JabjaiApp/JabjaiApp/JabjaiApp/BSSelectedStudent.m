//
//  BSSelectedStudent.m
//  JabjaiApp
//
//  Created by mac on 7/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSSelectedStudent.h"

@implementation BSSelectedStudent

@synthesize studentId = _studentId;
@synthesize studentName = _studentName;
@synthesize selected = _selected;

- (void)setStudentId:(long long)studentId {
    _studentId = studentId;
}

- (void)setStudentName:(NSString *)studentName {
    _studentName = studentName;
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
}

- (long long)getStudentId {
    return _studentId;
}

- (NSString *)getStudentName {
    return _studentName;
}

- (BOOL)isSelected {
    return _selected;
}

@end
