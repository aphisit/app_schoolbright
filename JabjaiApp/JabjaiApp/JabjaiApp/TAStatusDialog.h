//
//  TAStatusDialog.h
//  JabjaiApp
//
//  Created by mac on 7/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@class TAStatusDialog;

@protocol TAStatusDialogDelegate <NSObject>

- (void)taStatusDialog:(TAStatusDialog *)dialog onSelectStatus:(TA_SCAN_STATUS)scanStatus;

@end

@interface TAStatusDialog : UIViewController

@property (nonatomic, retain) id<TAStatusDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIStackView *dialogStackView;
@property (weak, nonatomic) IBOutlet UIButton *statusOnTimeButton;
@property (weak, nonatomic) IBOutlet UIButton *statusLateButton;
@property (weak, nonatomic) IBOutlet UIButton *statusAbsenceButton;
@property (weak, nonatomic) IBOutlet UIButton *statusEventButton;
@property (weak, nonatomic) IBOutlet UIButton *statusPersonalLeaveButton;
@property (weak, nonatomic) IBOutlet UIButton *statusSickLeaveButton;

- (IBAction)selectStatus:(id)sender;

- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
