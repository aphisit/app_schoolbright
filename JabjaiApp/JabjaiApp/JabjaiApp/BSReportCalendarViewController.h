//
//  BSReportCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import ARSlidingPanel;

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CFSCalendar.h"
#import "BSReportScoreListViewController.h"
#import "BSReportScoreListViewControllerDelegate.h"
#import "BSReportCalendarViewControllerDelegate.h"

@interface BSReportCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, ARSPDragDelegate, ARSPVisibilityStateDelegate, BSReportScoreListViewControllerDelegate>

@property (weak, nonatomic) id<BSReportCalendarViewControllerDelegate> delegate;

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)openDrawer:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
