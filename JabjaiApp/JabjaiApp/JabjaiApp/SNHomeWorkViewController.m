//
//  SNHomeWorkViewController.m
//  JabjaiApp
//
//  Created by toffee on 9/1/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNHomeWorkViewController.h"
#import <QBImagePickerController/QBImagePickerController.h>

@interface SNHomeWorkViewController ()

@end

@implementation SNHomeWorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.detailHomeWork.layer.borderWidth = 1;
    self.detailHomeWork.layer.borderColor = [UIColor grayColor].CGColor;
    self.viewFile.layer.borderWidth = 1;
    self.viewFile.layer.borderColor = [UIColor grayColor].CGColor;
    self.imageViewFile.layer.borderWidth = 1;
    self.imageViewFile.layer.borderColor = [UIColor grayColor].CGColor;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
    for (PHAsset *asset in assets) {
        // Do something with the asset
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)selectFile:(id)sender {
}

- (IBAction)selectImage:(id)sender {
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.maximumNumberOfSelection = 6;
    imagePickerController.showsNumberOfSelectedAssets = YES;
    
    [self presentViewController:imagePickerController animated:YES completion:NULL];}
@end
