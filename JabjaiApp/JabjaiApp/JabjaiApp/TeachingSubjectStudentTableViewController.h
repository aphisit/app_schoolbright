//
//  TeachingSubjectStudentTableViewController.h
//  JabjaiApp
//
//  Created by mac on 12/30/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeachingSubjectStudentTableViewController : UITableViewController

@property (nonatomic) int sectionID;

@end
