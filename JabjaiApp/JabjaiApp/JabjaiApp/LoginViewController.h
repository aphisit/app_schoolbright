//
//  LoginViewController.h
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SchoolCodeDialogViewController.h"

@interface LoginViewController : UIViewController <SchoolCodeDialogViewControllerDelegate>

// Layout Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgLogoWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgLogoHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgLogoTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonStackViewTopConstraint;

// UI View

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn;
@property (weak, nonatomic) IBOutlet UIButton *signupBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIView *inputContainerView;

- (IBAction)forgotPasswordAction:(id)sender;
- (IBAction)signupAction:(id)sender;
- (IBAction)loginAction:(id)sender;
- (IBAction)DismissKeyboard:(id)sender;

@end
