//
//  TAStudentListViewController.h
//  JabjaiApp
//
//  Created by mac on 7/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "TAStudentStatusTableViewCell.h"
#import "CallGetStudentInClassAPI.h"
#import "CallGetPreviousAttendClassStatusAPI.h"
#import "TAStatusDialog.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "SSubjectModel.h"

@interface TAStudentListViewController : UIViewController <TAStudentStatusTableViewCellDelegate, UITableViewDataSource, UITableViewDelegate, CallGetStudentInClassAPIDelegate, CallGetPreviousAttendClassStatusAPIDelegate, TAStatusDialogDelegate, UISearchBarDelegate>

// Global variables
// TASelectClassLevelViewController
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SSubjectModel *> *subjectArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
@property (nonatomic) NSInteger mode;

// TAStudentListViewController
@property (strong, nonatomic) NSArray<TAStudentStatusModel *> *studentStatusArray;
@property (assign, nonatomic) long long classroomId;
@property (assign, nonatomic) long long subjectId;
@property (assign, nonatomic) NSString *subjectName;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *optCopyButton;

- (IBAction)moveBack:(id)sender;
- (IBAction)actionSelectAll:(id)sender;
- (IBAction)actionCopyHistoryStatus:(id)sender;
- (IBAction)actionNext:(id)sender;

@end
