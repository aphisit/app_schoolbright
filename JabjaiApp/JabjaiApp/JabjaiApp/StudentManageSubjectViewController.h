//
//  StudentManageSubjectViewController.h
//  JabjaiApp
//
//  Created by mac on 12/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CAPSPageMenu.h"

@interface StudentManageSubjectViewController : UIViewController <CAPSPageMenuDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UIView *contentView;

- (IBAction)openDrawer:(id)sender;

- (void)notifyUpdateData;
@end
