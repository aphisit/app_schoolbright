//
//  DeleteSubjectTableViewController.h
//  JabjaiApp
//
//  Created by mac on 12/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StudentManageSubjectViewController.h"

@interface DeleteSubjectTableViewController : UITableViewController

@property (strong, nonatomic) StudentManageSubjectViewController *studentManageSubjectViewController;

- (void)notifyUpdateData;
- (void)filterWithSearchText:(NSString *)searchText;
- (void)setSearchActive:(BOOL)active;

@end
