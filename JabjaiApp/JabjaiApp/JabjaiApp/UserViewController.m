//
//  UserViewController.m
//  JabjaiApp
//
//  Created by mac on 5/17/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "UserViewController.h"
#import "UserInfoModel.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"
#import "UserData.h"
#import "UserInfoDBHelper.h"
#import "MultipleUserModel.h"

@interface UserViewController () {
    NSInteger connectCounter;
}

@property (strong, nonatomic) UserInfoModel *userInfoModel;
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;

@end

@implementation UserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.firstNameLabel.text = nil;
    self.lastNameLabel.text = nil;
    self.classLabel.text = nil;
    self.studentIDLabel.text = nil;
    self.scoreLabel.text = @"n/a";
    self.studentBalanceLabel.text = @"0 บ.";
    self.teacherBalanceLabel.text = @"0 บ.";
    
    if([UserData getUserType] != STUDENT) {
        [self.studentFooterView setAlpha:0.0f];
        [self.teacherFooterView setAlpha:1.0f];
        
        [self.classStackView removeFromSuperview];
        [self.studentIDStackView removeFromSuperview];
    }
    else {
        [self.studentFooterView setAlpha:1.0f];
        [self.teacherFooterView setAlpha:0.0f];
    }
    
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    
    connectCounter = 0;
    [self getUserInfoData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - GetAPIData

- (void)getUserInfoData {
    NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUserInfoData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(returnedArray.count != 0) {
                    
                    self.userInfoModel = [[UserInfoModel alloc] init];
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    NSMutableString *firstName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sName"]];
                    NSMutableString *lastName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sLastname"]];
                    NSInteger sex = [[dataDict objectForKey:@"cSex"] integerValue];
                    NSMutableString *birthdayStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dBirth"]];
                    NSMutableString *phoneNumber = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sPhone"]];
                    NSMutableString *email = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sEmail"]];
                    float money = [[dataDict objectForKey:@"nMoney"] floatValue];
                    float creditLimits = [[dataDict objectForKey:@"nMax"] floatValue];
                    NSInteger score = [[dataDict objectForKey:@"Score"] integerValue];
                    
                    NSString *imageUrl;
                    
                    if(![[dataDict objectForKey:@"image"] isKindOfClass:[NSNull class]]) {
                        imageUrl = [dataDict objectForKey:@"image"];
                    }
                    else {
                        imageUrl = @"";
                    }
                    
                    NSMutableString *studentCode, *studentClass, *schoolName;
                    
                    if(![[dataDict objectForKey:@"StudentId"] isKindOfClass:[NSNull class]]) {
                        studentCode = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"StudentId"]];
                    }
                    else {
                        studentCode = [[NSMutableString alloc] init];
                    }
                    
                    if(![[dataDict objectForKey:@"StudentClass"] isKindOfClass:[NSNull class]]) {
                        studentClass = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"StudentClass"]];
                    }
                    else {
                        studentClass = [[NSMutableString alloc] init];
                    }
                    
                    if(![[dataDict objectForKey:@"SchoolName"] isKindOfClass:[NSNull class]]) {
                        schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolName"]];
                    }
                    else {
                        schoolName = [[NSMutableString alloc] init];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) firstName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lastName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) birthdayStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) phoneNumber);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) email);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentCode);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentClass);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) schoolName);
                    
                    NSDate *birtdayDate = [Utils parseServerDateStringToDate:birthdayStr];
                    
                    self.userInfoModel.firstName = firstName;
                    self.userInfoModel.lastName = lastName;
                    self.userInfoModel.sex = sex;
                    self.userInfoModel.birthday = birtdayDate;
                    self.userInfoModel.studentCode = studentCode;
                    self.userInfoModel.studentClass = studentClass;
                    self.userInfoModel.schoolName = schoolName;
                    self.userInfoModel.email = email;
                    self.userInfoModel.phoneNumber = phoneNumber;
                    self.userInfoModel.money = money;
                    self.userInfoModel.creditLimits = creditLimits;
                    self.userInfoModel.score = score;
                    self.userInfoModel.imageUrl = imageUrl;
                    
                    [self updateUserData];
                    
                    if(imageUrl != nil && imageUrl.length > 0) {
                        BOOL success = [self.dbHelper updateImageUrlWithSlaveId:[UserData getUserID] imageUrl:imageUrl];
                        NSLog(@"%@", [NSString stringWithFormat:@"update user image success : %d", success]);
                    }
                }
                
            }
            
        }
        
    }];
    
}

- (void)updateUserData {
    
    if(self.userInfoModel != nil) {
        
        self.firstNameLabel.text = self.userInfoModel.firstName;
        self.lastNameLabel.text = self.userInfoModel.lastName;
        
        if(self.userInfoModel.imageUrl == nil || self.userInfoModel.imageUrl.length == 0) {
            
            if(self.userInfoModel.sex == 0) { // M
                self.userImageView.image = [UIImage imageNamed:@"boy"];
            }
            else { // F
                self.userImageView.image = [UIImage imageNamed:@"girl"];
            }
            
            self.userImageView.backgroundColor = [UIColor whiteColor];
        }
        else {
            self.userImageView.backgroundColor = [UIColor clearColor];
            [self.userImageView loadImageFromURL:self.userInfoModel.imageUrl];
        }
        
        if([UserData getUserType] != STUDENT) {
            
            if([Utils isIntegerWithDouble:self.userInfoModel.money]) {
                self.teacherBalanceLabel.text = [[NSString alloc] initWithFormat:@"%ld บ.", (long int)self.userInfoModel.money];
            }
            else {
                self.teacherBalanceLabel.text = [[NSString alloc] initWithFormat:@"%.2f บ.", self.userInfoModel.money];
            }
            
        }
        else {
            
            self.studentIDLabel.text = self.userInfoModel.studentCode;
            self.classLabel.text = self.userInfoModel.studentClass;
            self.scoreLabel.text = [[NSString alloc] initWithFormat:@"%ld", self.userInfoModel.score];
            
            if([Utils isIntegerWithDouble:self.userInfoModel.money]) {
                self.studentBalanceLabel.text = [[NSString alloc] initWithFormat:@"%ld บ.", (long int)self.userInfoModel.money];
            }
            else {
                self.studentBalanceLabel.text = [[NSString alloc] initWithFormat:@"%.2f บ.", self.userInfoModel.money];
            }
        }
    }
    
}

@end
