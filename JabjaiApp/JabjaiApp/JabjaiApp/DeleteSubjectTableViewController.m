//
//  DeleteSubjectTableViewController.m
//  JabjaiApp
//
//  Created by mac on 12/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "DeleteSubjectTableViewController.h"
#import "StudentManageSubjectHeaderViewCell.h"
#import "StudentManageSubjectChildViewCell.h"
#import "UTeachingYearModel.h"
#import "APIURL.h"
#import "Utils.h"
#import "DateUtility.h"
#import "Constant.h"

@interface DeleteSubjectTableViewController () {
    
    BOOL searchActive;
    
    NSMutableArray *sectionKeys;
    NSMutableArray *expandedSections;
    NSMutableDictionary *subjectInTableSection; // key (section) -> NSArray []
    
    NSMutableArray *searchSectionKeys;
    NSMutableArray *searchExpandedSections;
    NSMutableDictionary *searchSubjectInTableSection;
    
    NSMutableArray *teachingYearArray;
    
    NSInteger connectCounter;
}

@end

@implementation DeleteSubjectTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([StudentManageSubjectHeaderViewCell class]) bundle:nil] forCellReuseIdentifier:@"HeaderCell"];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([StudentManageSubjectChildViewCell class]) bundle:nil] forCellReuseIdentifier:@"ChildCell"];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    // Initial table expandable variables
    sectionKeys = [[NSMutableArray alloc] init];
    expandedSections = [[NSMutableArray alloc] init];
    subjectInTableSection = [[NSMutableDictionary alloc] init];
    
    searchSectionKeys = [[NSMutableArray alloc] init];
    searchExpandedSections = [[NSMutableArray alloc] init];
    searchSubjectInTableSection = [[NSMutableDictionary alloc] init];
    
    searchActive = NO;
    connectCounter = 0;
    
    [self getStudentSubjectAddedData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    if(searchActive) {
        return searchSubjectInTableSection.count;
    }
    else {
        return subjectInTableSection.count;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if(searchActive) {
        
        if([searchExpandedSections containsObject:@(section)]) {
            
            NSString *key = [searchSectionKeys objectAtIndex:section];
            USubjectModel *subjectModel = [searchSubjectInTableSection objectForKey:key];
            
            return subjectModel.sections.count + 1; // Plus 1 add header of each section
        }
        else {
            return 1;
        }
        
    }
    else {
        
        if([expandedSections containsObject:@(section)]) {
            
            NSString *key = [sectionKeys objectAtIndex:section];
            USubjectModel *subjectModel = [subjectInTableSection objectForKey:key];
            
            return subjectModel.sections.count + 1; // Plus 1 add header of each section
        }
        else {
            return 1; // If section is not expaned show only header
        }
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(searchActive) {
        
        NSString *key = [searchSectionKeys objectAtIndex:indexPath.section];
        
        if(indexPath.row == 0) { // For the 1st row of each section show header
            
            StudentManageSubjectHeaderViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell" forIndexPath:indexPath];
            
            BOOL isExpanded = [searchExpandedSections containsObject:@(indexPath.section)];
            
            if(isExpanded) {
                cell.arrowImage.image = [UIImage imageNamed:@"ic_sort_up"];
            }
            else {
                cell.arrowImage.image = [UIImage imageNamed:@"ic_sort_down"];
            }
            
            USubjectModel *subjectModel = [searchSubjectInTableSection objectForKey:key];
            
            cell.titleLabel.text = subjectModel.subjectName;
            
            return cell;
        }
        else {
            
            StudentManageSubjectChildViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChildCell" forIndexPath:indexPath];
            
            USubjectModel *subjectModel = [searchSubjectInTableSection objectForKey:key];
            NSArray *sections = subjectModel.sections;
            USectionModel *sectionModel = [sections objectAtIndex:indexPath.row - 1];
            
            cell.titleLabel.text = sectionModel.sectionName;
            cell.actionImage.tag = sectionModel.sectionID; //set section id as tag of actionImage;
            cell.actionImage.image = [UIImage imageNamed:@"close"];
            
            UITapGestureRecognizer *singerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteSubject:)];
            singerTap.numberOfTapsRequired = 1;
            
            [cell.actionImage setUserInteractionEnabled:YES];
            [cell.actionImage addGestureRecognizer:singerTap];
            
            return cell;
        }

    }
    else {
        
        NSString *key = [sectionKeys objectAtIndex:indexPath.section];
        
        if(indexPath.row == 0) { // For the 1st row of each section show header
            
            StudentManageSubjectHeaderViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell" forIndexPath:indexPath];
            
            BOOL isExpanded = [expandedSections containsObject:@(indexPath.section)];
            
            if(isExpanded) {
                cell.arrowImage.image = [UIImage imageNamed:@"ic_sort_up"];
            }
            else {
                cell.arrowImage.image = [UIImage imageNamed:@"ic_sort_down"];
            }
            
            USubjectModel *subjectModel = [subjectInTableSection objectForKey:key];
            
            cell.titleLabel.text = subjectModel.subjectName;
            
            return cell;
        }
        else {
            
            StudentManageSubjectChildViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChildCell" forIndexPath:indexPath];
            
            USubjectModel *subjectModel = [subjectInTableSection objectForKey:key];
            NSArray *sections = subjectModel.sections;
            USectionModel *sectionModel = [sections objectAtIndex:indexPath.row - 1];
            
            cell.titleLabel.text = sectionModel.sectionName;
            cell.actionImage.tag = sectionModel.sectionID; //set section id as tag of actionImage;
            cell.actionImage.image = [UIImage imageNamed:@"close"];
            
            UITapGestureRecognizer *singerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteSubject:)];
            singerTap.numberOfTapsRequired = 1;
            
            [cell.actionImage setUserInteractionEnabled:YES];
            [cell.actionImage addGestureRecognizer:singerTap];
            
            return cell;
        }
    }
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(searchActive) {
        
        if([searchExpandedSections containsObject:@(indexPath.section)]) {
            
            if(indexPath.row == 0) { // when select header of each section, then toggle up and down
                [searchExpandedSections removeObject:@(indexPath.section)];
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
            }
            else {
                [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }
        else {
            [searchExpandedSections addObject:@(indexPath.section)];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
        
    }
    else {
        
        if([expandedSections containsObject:@(indexPath.section)]) {
            
            if(indexPath.row == 0) { // when select header of each section, then toggle up and down
                [expandedSections removeObject:@(indexPath.section)];
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
            }
            else {
                [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }
        else {
            [expandedSections addObject:@(indexPath.section)];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
        
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0) {
        return 60;
    }
    else {
        return 44;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - GetAPIData

- (void)getStudentSubjectAddedData {
    NSString *URLString = [APIURL getSubjectAddedUrl];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getStudentSubjectAddedData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentSubjectAddedData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getStudentSubjectAddedData];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;

                teachingYearArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    
                    NSDictionary *yearDataDict = [returnedArray objectAtIndex:i];
                    
                    int yearID = [[yearDataDict objectForKey:@"YearId"] intValue];
                    NSMutableString *yearName = [[NSMutableString alloc] initWithFormat:@"%@", [yearDataDict objectForKey:@"Year"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) yearName);
                    
                    NSArray *termArray = [yearDataDict objectForKey:@"lTerm"];
                    
                    NSMutableArray<USemesterModel *> *semesters = [[NSMutableArray alloc] init];
                    
                    for(int j=0; j<termArray.count; j++) {
                        
                        NSDictionary *termDataDict = [termArray objectAtIndex:j];
                        
                        NSMutableString *semesterID = [[NSMutableString alloc] initWithFormat:@"%@", [termDataDict objectForKey:@"TermId"]];
                        NSMutableString *semesterName = [[NSMutableString alloc] initWithFormat:@"%@", [termDataDict objectForKey:@"Term"]];
                        
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) semesterID);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) semesterName);
                        
                        NSArray *subjectArray = [termDataDict objectForKey:@"lPlane"];
                        
                        NSMutableArray<USubjectModel *> *subjects = [[NSMutableArray alloc] init];
                        
                        for(int k=0; k<subjectArray.count; k++) {
                            
                            NSDictionary *subjectDataDict = [subjectArray objectAtIndex:k];
                            
                            NSMutableString *subjectID = [[NSMutableString alloc] initWithFormat:@"%@", [subjectDataDict objectForKey:@"PlaneId"]];
                            NSMutableString *subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [subjectDataDict objectForKey:@"Plane"]];
                            
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectID);
                            CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                            
                            NSArray *sectionArray = [subjectDataDict objectForKey:@"lSection"];
                            
                            NSMutableArray<USectionModel *> *sections = [[NSMutableArray alloc] init];
                            
                            for(int l=0; l<sectionArray.count; l++) {
                                NSDictionary *sectionDataDict = [sectionArray objectAtIndex:l];
                                
                                int sectionID = [[sectionDataDict objectForKey:@"SectionId"] intValue];
                                NSMutableString *sectionName = [[NSMutableString alloc] initWithFormat:@"%@", [sectionDataDict objectForKey:@"Section"]];
                                
                                CFStringTrimWhitespace((__bridge CFMutableStringRef) sectionName);
                                
                                USectionModel *sectionModel = [[USectionModel alloc] init];
                                sectionModel.sectionID = sectionID;
                                sectionModel.sectionName = sectionName;
                                
                                [sections addObject:sectionModel];
                            }
                            
                            USubjectModel *subjectModel = [[USubjectModel alloc] init];
                            subjectModel.subjectID = subjectID;
                            subjectModel.subjectName = subjectName;
                            subjectModel.sections = sections;
                            
                            [subjects addObject:subjectModel];
                            
                        }
                        
                        USemesterModel *semesterModel = [[USemesterModel alloc] init];
                        semesterModel.semesterID = semesterID;
                        semesterModel.semesterName = semesterName;
                        semesterModel.subjects = subjects;
                        
                        [semesters addObject:semesterModel];
                        
                    }
                    
                    UTeachingYearModel *teachingYearModel = [[UTeachingYearModel alloc] init];
                    teachingYearModel.yearID = yearID;
                    teachingYearModel.yearName = yearName;
                    teachingYearModel.semesters = semesters;
                    
                    [teachingYearArray addObject:teachingYearModel];
                    
                }
                
                //
                [self performTeachingYearData];
                
            }
        }

    }];
    
}

- (void)deleteSection:(NSInteger)sectionID {
    NSString *URLString = [APIURL getDeleteSectionUrl:sectionID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self deleteSection:sectionID];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            connectCounter = 0;
            
            NSString *resultStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSLog(@"%@", [NSString stringWithFormat:@"Delete Section Result : %@", resultStr]);
            
            // Fetch new data
            //[self getStudentSubjectAddedData];
            
            // Notify update data
            if(self.studentManageSubjectViewController != nil) {
                [self.studentManageSubjectViewController notifyUpdateData];
            }

        }

    }];
    
}

#pragma mark - Manage Data

- (void) performTeachingYearData {
    
    if(teachingYearArray == nil) {
        return;
    }
    
    if(sectionKeys != nil) {
        [sectionKeys removeAllObjects];
    }
    
    if(expandedSections != nil) {
        [expandedSections removeAllObjects];
    }
    
    if(subjectInTableSection != nil) {
        [subjectInTableSection removeAllObjects];
    }
    
    sectionKeys = [[NSMutableArray alloc] init];
    expandedSections = [[NSMutableArray alloc] init];
    subjectInTableSection = [[NSMutableDictionary alloc] init];
    
    for(UTeachingYearModel *teachingYearModel in teachingYearArray) {
        NSArray *semesters = teachingYearModel.semesters;
        
        for(USemesterModel *semesterModel in semesters) {
            NSArray *subjects = semesterModel.subjects;
            
            for(USubjectModel *subjectModel in subjects) {
                [subjectInTableSection setObject:subjectModel forKey:subjectModel.subjectID];
                [sectionKeys addObject:subjectModel.subjectID];
            }
            
        }
    }
    
    [self.tableView reloadData];
}

- (void)deleteSubject:(UITapGestureRecognizer *)recognizer {
    
    UIImageView *imageView = (UIImageView *)recognizer.view;
    NSInteger sectionID = imageView.tag;
    
    [self deleteSection:sectionID];
    
    NSLog(@"%@", [NSString stringWithFormat:@"Delete section : %d", (int)sectionID]);
}

#pragma mark - Class functions

- (void)notifyUpdateData {
    [self getStudentSubjectAddedData];
}

- (void)filterWithSearchText:(NSString *)searchText {
    
    if(teachingYearArray == nil) {
        return;
    }
    
    if(searchSectionKeys != nil) {
        [searchSectionKeys removeAllObjects];
    }
    
    if(searchExpandedSections != nil) {
        [searchExpandedSections removeAllObjects];
    }
    
    if(searchSubjectInTableSection != nil) {
        [searchSubjectInTableSection removeAllObjects];
    }
    
    searchSectionKeys = [[NSMutableArray alloc] init];
    searchExpandedSections = [[NSMutableArray alloc] init];
    searchSubjectInTableSection = [[NSMutableDictionary alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subjectID contains[c] %@ or subjectName contains[c] %@", searchText, searchText];
    
    NSMutableArray *dataBeforeFilterArray = [[NSMutableArray alloc] init];
    
    for(UTeachingYearModel *teachingYearModel in teachingYearArray) {
        NSArray *semesters = teachingYearModel.semesters;
        
        for(USemesterModel *semesterModel in semesters) {
            NSArray *subjects = semesterModel.subjects;
            
            for(USubjectModel *subjectModel in subjects) {
                [dataBeforeFilterArray addObject:subjectModel];
            }
            
        }
    }
    
    NSArray *dataAfterFilterArray = [dataBeforeFilterArray filteredArrayUsingPredicate:predicate];
    
    for(USubjectModel *subjectModel in dataAfterFilterArray) {
        [searchSubjectInTableSection setObject:subjectModel forKey:subjectModel.subjectID];
        [searchSectionKeys addObject:subjectModel.subjectID];
    }
    
    searchActive = YES;
    
    [self.tableView reloadData];
}

- (void)setSearchActive:(BOOL)active {
    
    searchActive = active;
    
    if(!active) {
        [self performTeachingYearData];
    }
    
}

@end
