//
//  AttendClassReportViewController.h
//  JabjaiApp
//
//  Created by mac on 10/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CAPSPageMenu.h"
#import "CalendarDialog.h"
#import "Attend2SchoolReportFilterDialog.h"

@interface AttendSchoolReportViewController : UIViewController <CAPSPageMenuDelegate, Attend2SchoolReportFilterDialogDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;

- (IBAction)openDrawer:(id)sender;
- (IBAction)showFilterDialog:(id)sender;

@end
