//
//  AttendClassCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 10/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CFSCalendar.h"
#import "Attend2SchoolFilter.h"

@interface AttendSchoolCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, CFSCalendarDelegateAppearance>

@property (weak, nonatomic) IBOutlet CFSCalendar *calendar;

-(void)applyFilter:(Attend2SchoolFilter *)filter;

@end
