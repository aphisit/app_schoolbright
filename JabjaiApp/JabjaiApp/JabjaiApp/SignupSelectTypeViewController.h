//
//  SignupSelectTypeViewController.h
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupDataModel.h"
#import "SchoolCodeDialogViewController.h"

@interface SignupSelectTypeViewController : UIViewController <SchoolCodeDialogViewControllerDelegate>

@property (nonatomic, strong) SignupDataModel *signupDataModel;

@property (weak, nonatomic) IBOutlet UIView *studentView;
@property (weak, nonatomic) IBOutlet UIView *teacherView;

- (IBAction)moveBack:(id)sender;

@end
