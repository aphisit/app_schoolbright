//
//  TASummaryViewController.m
//  JabjaiApp
//
//  Created by mac on 7/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TASummaryViewController.h"
#import "TASummaryHeaderTableViewCell.h"
#import "TASummaryChildTableViewCell.h"
#import "TAStudentListViewController.h"
#import "TASelectClassLevelViewController.h"
#import "Utils.h"
#import "UserData.h"

@interface TASummaryViewController () {
    
    NSArray *sectionKeys;
    NSMutableArray *expandedSections;
    NSMutableDictionary<NSString *, NSMutableArray<TAStudentStatusModel *> *> *studentInTableSection;
    
    NSInteger totalStudents;
    
    BOOL updateStatusSuccess;
    
    CallUpdateStudentStatusPOSTAPI *callUpdateStudentStatusPOSTAPI;
    
    // The dialog
    AlertDialog *alertDialog;
}

@end

static NSString *headerCellIdentifier = @"HeaderCell";
static NSString *childCellIdentifier = @"ChildCell";

static NSString *SECTION_ONTIME = @"SECTION_ONTIME";
static NSString *SECTION_LATE = @"SECTION_LATE";
static NSString *SECTION_ABSENCE = @"SECTION_ABSENCE";
static NSString *SECTION_PERSONAL_LEAVE = @"SECTION_PERSONAL_LEAVE";
static NSString *SECTION_SICK_LEAVE = @"SECTION_SICK_LEAVE";
static NSString *SECTION_EVENT_LEAVE = @"SECTION_EVENT_LEAVE";
static NSString *SECTION_UNDEFINED = @"SECTION_UNDEFINED";

@implementation TASummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // register table nib
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TASummaryHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:headerCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TASummaryChildTableViewCell class]) bundle:nil] forCellReuseIdentifier:childCellIdentifier];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    // Initial table expandable variables
    sectionKeys = @[SECTION_ONTIME, SECTION_LATE, SECTION_ABSENCE, SECTION_PERSONAL_LEAVE, SECTION_SICK_LEAVE, SECTION_EVENT_LEAVE, SECTION_UNDEFINED];
    expandedSections = [[NSMutableArray alloc] init];
    studentInTableSection = [[NSMutableDictionary alloc] init];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_ONTIME];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_LATE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_ABSENCE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_PERSONAL_LEAVE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_SICK_LEAVE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_EVENT_LEAVE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_UNDEFINED];
    
    if(self.studentStatusArray != nil) {
        totalStudents = self.studentStatusArray.count;
    }
    else {
        totalStudents = 0;
    }
    
    updateStatusSuccess = NO;
    
    if(_mode == TA_MODE_CHECK) {
        self.headerTitleLabel.text = @"เช็คชื่อรายวิชา";
    }
    else {
        self.headerTitleLabel.text = @"แก้ไขเช็คชื่อรายวิชา";
    }
    
    [self performStudentStatusData];
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)performStudentStatusData {
    
    if(self.studentStatusArray != nil) {
        
        for(NSString *key in studentInTableSection.allKeys) {
            [[studentInTableSection objectForKey:key] removeAllObjects];
        }
        
        for(TAStudentStatusModel *model in self.studentStatusArray) {
            
            switch ([model getScanStatus]) {
                case 0:
                    [[studentInTableSection objectForKey:SECTION_ONTIME] addObject:model];
                    break;
                case 1:
                    [[studentInTableSection objectForKey:SECTION_LATE] addObject:model];
                    break;
                case 3:
                    [[studentInTableSection objectForKey:SECTION_ABSENCE] addObject:model];
                    break;
                case 4:
                    [[studentInTableSection objectForKey:SECTION_PERSONAL_LEAVE] addObject:model];
                    break;
                case 5:
                    [[studentInTableSection objectForKey:SECTION_SICK_LEAVE] addObject:model];
                    break;
                case 6:
                    [[studentInTableSection objectForKey:SECTION_EVENT_LEAVE] addObject:model];
                    break;
                default:
                    [[studentInTableSection objectForKey:SECTION_UNDEFINED] addObject:model];
                    break;
            }
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(sectionKeys != nil) {
        return sectionKeys.count;
    }
    else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([expandedSections containsObject:@(section)]) {
        NSString *key = [sectionKeys objectAtIndex:section];
        
        if([studentInTableSection objectForKey:key] != nil) {
            NSArray *students = [studentInTableSection objectForKey:key];
            return students.count + 1;
        }
        else {
            return 1;
        }
    }
    else {
        return 1; // show table header cell
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *key = [sectionKeys objectAtIndex:indexPath.section];
    
    if(indexPath.row == 0) { // For the first row of each section we'll show header
        TASummaryHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier forIndexPath:indexPath];
        
        BOOL isExpanded = [expandedSections containsObject:@(indexPath.section)];
        
        if(isExpanded) {
            cell.arrowImageView.image = [UIImage imageNamed:@"ic_sort_up"];
        }
        else {
            cell.arrowImageView.image = [UIImage imageNamed:@"ic_sort_down"];
        }
        
        NSArray *students = [studentInTableSection objectForKey:key];
        
        NSInteger numberOfStudents = 0;
        
        if(students != nil && students.count > 0) {
            [cell.arrowImageView setAlpha:1.0f];
            numberOfStudents = students.count;
        }
        else {
            [cell.arrowImageView setAlpha:0.0f];
        }
        
        NSString *titleStr;
        
        if([key isEqualToString:SECTION_ONTIME]) {
            titleStr = @"เข้าเรียน";
        }
        else if([key isEqualToString:SECTION_LATE]) {
            titleStr = @"สาย";
        }
        else if([key isEqualToString:SECTION_ABSENCE]) {
            titleStr = @"ขาด";
        }
        else if([key isEqualToString:SECTION_PERSONAL_LEAVE]) {
            titleStr = @"ลากิจ";
        }
        else if([key isEqualToString:SECTION_SICK_LEAVE]) {
            titleStr = @"ลาป่วย";
        }
        else if([key isEqualToString:SECTION_EVENT_LEAVE]) {
            titleStr = @"กิจกรรม";
        }
        else {
            titleStr = @"ไม่ระบุสถานะ";
        }
        
        cell.titleLabel.text = titleStr;
        cell.numberLabel.text = [NSString stringWithFormat:@"%li/%li", (long)numberOfStudents, (long)totalStudents];
        
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.userInteractionEnabled = YES;
        
        return cell;
        
    }
    else {
        
        TASummaryChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:childCellIdentifier forIndexPath:indexPath];
        
        TAStudentStatusModel *model = [[studentInTableSection objectForKey:key] objectAtIndex:indexPath.row - 1];
        cell.titleLabel.text = [NSString stringWithFormat:@"%li. %@", (long)indexPath.row, [model getStudentName]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0) {
        return 60;
    }
    else {
        return 44;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([expandedSections containsObject:@(indexPath.section)]) {
        
        if(indexPath.row == 0) { // If select header of each section, then toggle up and down
            [expandedSections removeObject:@(indexPath.section)];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    else {
        [expandedSections addObject:@(indexPath.section)];
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - API Caller

- (void)updateStudentStatusWithJSON:(NSString *)jsonString {
    
    if(callUpdateStudentStatusPOSTAPI != nil) {
        callUpdateStudentStatusPOSTAPI = nil;
    }
    
    [self showIndicator];
    
    callUpdateStudentStatusPOSTAPI = [[CallUpdateStudentStatusPOSTAPI alloc] init];
    callUpdateStudentStatusPOSTAPI.delegate = self;
    [callUpdateStudentStatusPOSTAPI call:[UserData getSchoolId] subjectId:self.subjectId teacherId:[UserData getUserID] jsonString:jsonString];
    
}

- (void)callUpdateStudentStatusPOSTAPI:(CallUpdateStudentStatusPOSTAPI *)classObj response:(NSString *)response success:(BOOL)success {
    
    [self stopIndicator];
    updateStatusSuccess = success;
    
    if(success) {
        NSString *alertMessage = @"ทำรายการสำเร็จ";
        [self showAlertDialogWithMessage:alertMessage];
    }
    else {
        NSString *alertMessage = @"ทำรายการไม่สำเร็จ\nโปรดลองใหม่";
        [self showAlertDialogWithMessage:alertMessage];
    }
}

#pragma mark - Dialog

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)onAlertDialogClose {
    
    if(updateStatusSuccess) {
        TASelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TakeClassAttendanceStoryboard"];
        viewController.mode = _mode;
        
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}


#pragma mark - Action functions
- (IBAction)moveBack:(id)sender {
    
    TAStudentListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TAStudentListStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.subjectArray = _subjectArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.selectedSubjectIndex = _selectedSubjectIndex;
    viewController.mode = _mode;
    
    viewController.studentStatusArray = _studentStatusArray;
    viewController.classroomId = _classroomId;
    viewController.subjectId = _subjectId;
    viewController.subjectName = _subjectName;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (IBAction)actionSubmit:(id)sender {
    
    NSString *jsonString = [Utils getStudentStatusJSON:_studentStatusArray];
    NSLog(@"%@", jsonString);
    [self updateStudentStatusWithJSON:jsonString];
    
}
@end
