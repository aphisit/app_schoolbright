//
//  AddAccountLoginViewController.h
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MultipleUserModel.h"
#import "CallGetLoginAPI.h"

@interface AddAccountLoginViewController : UIViewController <CallGetLoginAPIDelegate>

// Global variable
@property (strong, nonatomic) NSArray<MultipleUserModel *> *userArray;

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)actionLogin:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
