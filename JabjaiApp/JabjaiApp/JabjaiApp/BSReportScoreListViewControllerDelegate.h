//
//  BSReportScoreListViewControllerDelegate.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSHistoryModel.h"

@protocol BSReportScoreListViewControllerDelegate <NSObject>

@optional
- (void)onSelectItem:(BSHistoryModel *)data date:(NSDate *)date;

@end
