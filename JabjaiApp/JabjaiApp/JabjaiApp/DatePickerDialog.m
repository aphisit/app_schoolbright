//
//  DatePickerDialog.m
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "DatePickerDialog.h"
#import "Utils.h"

@interface DatePickerDialog () {
}

@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic, strong) NSDate *showDate;

@end

@implementation DatePickerDialog

- (instancetype)initWithDate:(NSDate *)date {
    self = [super init];
    
    if(self) {
        self.showDate = date;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSCalendar *calendar = [Utils getGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    _datePicker.calendar = calendar;
    _datePicker.maximumDate = [calendar dateFromComponents:components];
    _datePicker.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierBuddhist];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"th"];
    _datePicker.locale = locale;
    
    if(self.showDate != nil) {
        [self.datePicker setDate:self.showDate];
    }
    
    // Decorate view
    self.dialogView.layer.cornerRadius = 10;
    self.dialogView.layer.masksToBounds = YES;

    self.cancelBtn.layer.cornerRadius = 20;
    self.cancelBtn.layer.masksToBounds = YES;
    self.okBtn.layer.cornerRadius = 20;
    self.okBtn.layer.masksToBounds = YES;
    
    // Initialize variables
    self.isShowing = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelAction:(id)sender {
    [self dismissDialog];
}

- (IBAction)okAction:(id)sender {
    [self dismissDialog];
    [self.delegate onSelectDate: self.datePicker.date];
}

#pragma mark - Dialog Functions

- (void)showDialogInView:(UIView *)targetView {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    self.isShowing = YES;
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

@end
