//
//  UniversityDegreeModel.h
//  JabjaiApp
//
//  Created by mac on 11/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UniversityDegreeModel : NSObject

@property (nonatomic, strong) NSNumber *degreeID;
@property (nonatomic, strong) NSString *degreeName;

-(NSNumber *)getDegreeID;
-(NSString *)getDegreeName;

@end
