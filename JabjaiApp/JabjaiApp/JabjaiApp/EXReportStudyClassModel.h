//
//  EXReportStudyClassModel.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXReportStatusDetailModel.h"

@interface EXReportStudyClassModel : NSObject

@property (nonatomic) long long iid;
@property (nonatomic) NSString *title;
@property (nonatomic) NSMutableArray<EXReportStatusDetailModel *> *reportStatusDetailArray;

- (void)setIId:(long long)iid;
- (void)setTitle:(NSString *)title;
- (void)setReportStatusDetailArray:(NSMutableArray<EXReportStatusDetailModel *> *)reportStatusDetailArray;

- (long long)getIId;
- (NSString *)getTitle;
- (NSMutableArray<EXReportStatusDetailModel *> *)getReportStatusDetailArray;

@end
