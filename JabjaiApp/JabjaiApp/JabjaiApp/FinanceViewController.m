//
//  FinanceViewController.m
//  JabjaiApp
//
//  Created by mac on 10/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "FinanceViewController.h"
#import "APIURL.h"
#import "UserData.h"
#import "Utils.h"
#import "Constant.h"

@interface FinanceViewController () {
    NSInteger connectCounter;
}

@end

@implementation FinanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    connectCounter = 0;
    
    if([UserData getAcademyType] == SCHOOL) {
        [self getCreditBureauSummaryData];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - GetAPIData

-(void)getCreditBureauSummaryData {
    NSString *URLString = [APIURL getCreditBureauSummaryURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getCreditBureauSummaryData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getCreditBureauSummaryData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getCreditBureauSummaryData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(returnedArray.count > 0) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    NSInteger creditLimits = [[dataDict objectForKey:@"nMax"] integerValue];
                    NSInteger money = [[dataDict objectForKey:@"nMoney"] integerValue];
                    
                    self.remainingLabel.text = [[NSString alloc] initWithFormat:@"ยอดคงเหลือ : %i", (int)money];
                    
                    if(creditLimits == 0) {
                        self.creditLimitLabel.text = @"วงเงินจำกัด : ไม่จำกัด";
                    }
                    else {
                        self.creditLimitLabel.text = [[NSString alloc] initWithFormat:@"วงเงินจำกัด : %i", (int)creditLimits];
                    }
                    
                }
            }

        }

    }];
    
}


@end
