//
//  SchoolDegreeModel.m
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SchoolDegreeModel.h"

@implementation SchoolDegreeModel

-(NSNumber *)getDegreeID {
    return self.degreeID;
}
-(NSString *)getDegreeName {
    return self.degreeName;
}

@end
