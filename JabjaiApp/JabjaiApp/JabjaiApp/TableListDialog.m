//
//  TableListDialog.m
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "TableListDialog.h"

@interface TableListDialog () {
}

@property (nonatomic, strong) NSString *requestCode;
@property (nonatomic) BOOL isShowing;

@end

static NSString *cellIdentifier = @"Cell";
static CGFloat cellHeight = 50;

@implementation TableListDialog

-(id)initWithRequestCode:(NSString *)requestCode {
    
    self = [[TableListDialog alloc] init];
    
    if(self) {
        self.requestCode = requestCode;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([TableListViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    self.tblView.delegate = self;
    self.tblView.dataSource = self;
    
    self.tblView.layer.cornerRadius = 10.0;
    self.tblView.layer.masksToBounds = YES;
    self.tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.isShowing = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark - <UIGestureRecognizerDelegate>
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//    
//    if([touch.view isKindOfClass:[UIButton class]] || [touch.view.superview isKindOfClass:[TableListViewCell class]]) {
//        return NO;
//    }
//    
//    return YES;
//}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.tblView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(_dataArr == nil) {
        return 0;
    }
    else {
        return _dataArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.label.text = [_dataArr objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate onItemSelect:self.requestCode atIndex:indexPath.row];
    [self dismissDialog];
}

#pragma mark - Dialog Functions

-(void)showDialogInView:(UIView *)targetView dataArr:(NSArray *)dataArr {

    _dataArr = dataArr;
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.tblView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.tblView setAlpha:1.0];
    [UIView commitAnimations];
    
    
    // Adjust table view frame size;
    
    int cellNumber = 5;
    if(_dataArr.count < 6) {
        cellNumber = (int)_dataArr.count;
    }
    
    // Calcalate position of new table view dialog
    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat viewHeight = self.view.frame.size.height;
    
    CGFloat tableViewWidth = self.view.frame.size.width - 32; //self.tblView.frame.size.width;
    CGFloat tableViewHeight = cellHeight * cellNumber;
    
    CGFloat posX = (viewWidth - tableViewWidth)/2.0;
    CGFloat posY = (viewHeight - tableViewHeight)/2.0;
    
    self.tblView.frame = CGRectMake(posX, posY, tableViewWidth, tableViewHeight);
    
    [self.tblView reloadData];
    
    self.isShowing = YES;;
    
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.tblView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

-(BOOL)isDialogShowing {
    return self.isShowing;
}

@end
