//
//  UniversitySignupPage1ViewController.m
//  JabjaiApp
//
//  Created by mac on 11/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "UniversitySignupPage1ViewController.h"
#import "SignupSelectTypeViewController.h"
#import "SignupPage2ViewController.h"
#import "APIURL.h"
#import "AlertDialog.h"
#import "UniversityFacultyModel.h"
#import "UniversityDegreeModel.h"
#import "UniversityMajorModel.h"
#import "Constant.h"
#import "Utils.h"

@interface UniversitySignupPage1ViewController () {
    
    NSMutableArray *universityFacultyArr;
    NSMutableArray *universityDegreeArr;
    NSMutableArray *universityMajorArr;
    
    NSInteger facultyID;
    NSInteger degreeID;
    NSInteger majorID;
    
    // The dialog
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    
    NSInteger connectCounter;
}

@end

static NSString *facultyDialogRequestCode = @"facultyDialogRequestCode";
static NSString *degreeDialogRequestCode = @"degreeDialogRequestCode";
static NSString *majorDialogRequestCode = @"majorDialogRequestCode";

@implementation UniversitySignupPage1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.universityTextField.delegate = self;
    self.facultyTextField.delegate = self;
    self.degreeTextField.delegate = self;
    self.majorTextField.delegate = self;
    self.studentIDTextField.delegate = self;
    
    self.universityTextField.text = _signupDataModel.school;
    
    alertDialog = [[AlertDialog alloc] init];
    
    // Restore data in text field when move back from page2
    if(_signupDataModel.faculty != nil) {
        self.facultyTextField.text = _signupDataModel.faculty;
        facultyID = [_signupDataModel.facultyID integerValue];
    }
    
    if(_signupDataModel.universityDegree != nil) {
        self.degreeTextField.text = _signupDataModel.universityDegree;
        degreeID = [_signupDataModel.universityDegreeID integerValue];
    }
    
    if(_signupDataModel.major != nil) {
        self.majorTextField.text = _signupDataModel.major;
        majorID = [_signupDataModel.majorID integerValue];
    }
    
    if(_signupDataModel.studentID != nil) {
        self.studentIDTextField.text = _signupDataModel.studentID;
    }
    
    // Docorate views
    self.nextBtn.layer.cornerRadius = 20;
    self.nextBtn.layer.masksToBounds = YES;
    
    connectCounter = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"selectTypeSegue"]) {
        SignupSelectTypeViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
    else if([segue.identifier isEqualToString:@"signupPage2Segue"]) {
        SignupPage2ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
}


- (IBAction)moveBack:(id)sender {
    [self performSegueWithIdentifier:@"selectTypeSegue" sender:self];
}

- (IBAction)dismissKeyboard:(id)sender {
    [self resignFirstResponder];
}

- (IBAction)nextStep:(id)sender {
    
    BOOL isDataValidate = [self validateData];
    
    if(isDataValidate) {
        NSString *faculty = self.facultyTextField.text;
        NSString *degree = self.degreeTextField.text;
        NSString *major = self.majorTextField.text;
        
        NSMutableString *studentID = [[NSMutableString alloc] initWithString:self.studentIDTextField.text];
        CFStringTrimWhitespace((__bridge CFMutableStringRef) studentID);
        
        _signupDataModel.faculty = faculty;
        _signupDataModel.facultyID = [[NSNumber alloc] initWithInteger:facultyID];
        _signupDataModel.universityDegree = degree;
        _signupDataModel.universityDegreeID = [[NSNumber alloc] initWithInteger:degreeID];
        _signupDataModel.major = major;
        _signupDataModel.majorID = [[NSNumber alloc] initWithInteger:majorID];
        _signupDataModel.studentID = studentID;
        
        [self performSegueWithIdentifier:@"signupPage2Segue" sender:self];
        
    }

}

#pragma mark - Validation Functions
-(BOOL)validateData {
    
    NSString *dialogTitle = @"แจ้งเตือน";
    
    if([self.universityTextField.text length] == 0 || [self.facultyTextField.text length] == 0 || [self.degreeTextField.text length] == 0 || [self.majorTextField.text length] == 0 || [self.studentIDTextField.text length] == 0) {
        
        NSString *alertMessage = @"กรุณากรอกข้อมูลให้ครบถ้วน";
        [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    NSString *dialogTitle = @"แจ้งเตือน";
    
    if(textField.tag == 1) { // University
        return NO;
    }
    else if(textField.tag == 2) { // Faculty
        
        if(tableListDialog == nil || (tableListDialog != nil && ![tableListDialog isDialogShowing])) {
            [self getUniversityFacultyData];
        }
        
        return NO;
    }
    else if(textField.tag == 3) { // Degree
        
        if([self.facultyTextField.text length] == 0) {
            
            NSLog(@"%@", @"Please Select Faculty");
            
            NSString *alertMessage = @"กรุณาเลือกคณะ";
            [alertDialog showDialogInView:self.view title:dialogTitle message:alertMessage];
            
        }
        else {
            
            if(tableListDialog == nil || (tableListDialog != nil && ![tableListDialog isDialogShowing])) {
                [self getUniversityDegreeData];
            }
        }
        
        return NO;
        
    }
    else if(textField.tag == 4) { // Major
        
        if([self.facultyTextField.text length] == 0) {
            
            NSLog(@"%@", @"Please Select Faculty");
            
            NSString *alertMessage = @"กรุณาเลือกคณะ";
            [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
            
        }
        else if([self.degreeTextField.text length] == 0) {
            NSLog(@"%@", @"Please Select Degree");
            
            NSString *alertMessage = @"กรุณาเลิือกระดับปริญญา";
            [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
            
        }
        else {
            
            if(tableListDialog == nil || (tableListDialog != nil && ![tableListDialog isDialogShowing])) {
                [self getUniversityMajorData];
            }
            
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - GetAPIData
-(void)getUniversityFacultyData {
    
    NSString *URLString = [APIURL getUniversityFacultyURL:[self.signupDataModel.schoolID integerValue]];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUniversityFacultyData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUniversityFacultyData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUniversityFacultyData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                universityFacultyArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    
                    NSNumber *lFacultyID = [[NSNumber alloc] initWithInteger:[[dataDict objectForKey:@"nTLevel"] integerValue]];
                    NSMutableString *lFacultyName = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"LevelName"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lFacultyName);
                    
                    UniversityFacultyModel *model = [[UniversityFacultyModel alloc] init];
                    model.facultyID = lFacultyID;
                    model.facultyName = lFacultyName;
                    
                    [universityFacultyArr addObject:model];
                    [dataArr addObject:lFacultyName];
                }
                
                // Create NSArray data to show in dialog
                [self showTableListDialogWithRequestCode:facultyDialogRequestCode data:dataArr];
                
            }
        }

    }];
    
}

-(void)getUniversityDegreeData {
    
    NSString *URLString = [APIURL getUniversityDegreeURL:[self.signupDataModel.schoolID integerValue] facultyID:facultyID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUniversityDegreeData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUniversityDegreeData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUniversityDegreeData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                universityDegreeArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    
                    NSNumber *lDegreeID = [[NSNumber alloc] initWithInteger:[[dataDict objectForKey:@"ID"] integerValue]];
                    NSMutableString *lDegreeName = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"Value"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lDegreeName);
                    
                    UniversityDegreeModel *model = [[UniversityDegreeModel alloc] init];
                    model.degreeID = lDegreeID;
                    model.degreeName = lDegreeName;
                    
                    [universityDegreeArr addObject:model];
                    [dataArr addObject:lDegreeName];
                }
                
                // Create NSArray data to show in dialog
                [self showTableListDialogWithRequestCode:degreeDialogRequestCode data:dataArr];
                
            }
        }
        
    }];
    
}

-(void)getUniversityMajorData {
    
    NSString *URLString = [APIURL getUniversityMajorURL:[self.signupDataModel.schoolID integerValue] degreeID:degreeID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUniversityMajorData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUniversityMajorData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUniversityMajorData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                universityMajorArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    
                    NSNumber *lMajorID = [[NSNumber alloc] initWithInteger:[[dataDict objectForKey:@"ID"] integerValue]];
                    NSMutableString *lMajorName = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"Value"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lMajorName);
                    
                    UniversityMajorModel *model = [[UniversityMajorModel alloc] init];
                    model.majorID = lMajorID;
                    model.majorName = lMajorName;
                    
                    [universityMajorArr addObject:model];
                    [dataArr addObject:lMajorName];
                }
                
                // Create NSArray data to show in dialog
                [self showTableListDialogWithRequestCode:majorDialogRequestCode data:dataArr];
                
            }
            
        }
        
    }];
    
}

#pragma mark - TableListDialogDelegate
-(void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:facultyDialogRequestCode]) {
        self.facultyTextField.text = [[universityFacultyArr objectAtIndex:index] getFacultyName];
        facultyID = [[[universityFacultyArr objectAtIndex:index] getFacultyID] integerValue];
        
        // Clear dependency text field values
        self.degreeTextField.text = @"";
        self.majorTextField.text = @"";
        
    }
    else if([requestCode isEqualToString:degreeDialogRequestCode]) {
        self.degreeTextField.text = [[universityDegreeArr objectAtIndex:index] getDegreeName];
        degreeID = [[[universityDegreeArr objectAtIndex:index] getDegreeID] integerValue];
        
        // Clear dependency text field values
        self.majorTextField.text = @"";
    }
    else if([requestCode isEqualToString:majorDialogRequestCode]) {
        self.majorTextField.text = [[universityMajorArr objectAtIndex:index] getMajorName];
        majorID = [[[universityMajorArr objectAtIndex:index] getMajorID] integerValue];
    }
    
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}


@end
