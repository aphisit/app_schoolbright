//
//  VerifyCodeDialog.h
//  JabjaiApp
//
//  Created by mac on 10/29/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VerifyCodeDialogDelegate <NSObject>

@optional
- (void)onVerifyCodeDialogClose;

@end

@interface VerifyCodeDialog : UIViewController

@property (retain, nonatomic) id<VerifyCodeDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hintLabel;
@property (weak, nonatomic) IBOutlet UILabel *verifyCodeLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;


- (IBAction)closeDialog:(id)sender;

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title verifyCode:(NSString *)verifyCode;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
