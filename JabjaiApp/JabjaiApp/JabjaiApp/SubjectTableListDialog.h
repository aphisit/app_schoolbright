//
//  SubjectTableListDialog.h
//  JabjaiApp
//
//  Created by mac on 7/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableSubjectListViewCell.h"
#import "SSubjectModel.h"

@class SubjectTableListDialog;

@protocol SubjectTableListDialogDelegate <NSObject>

- (void)subjectTableListDialog:(SubjectTableListDialog *)dialog onItemSelectWithRequestCode:(NSString *)requestCode atIndex:(NSInteger)index;

@end

@interface SubjectTableListDialog : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) id<SubjectTableListDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (instancetype)initWithRequestCode:(NSString *)requestCode;
- (void)showDialogInView:(UIView *)targetView subjectArray:(NSArray<SSubjectModel *> *)subjectArray;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
