//
//  CFSCalendarDynamicHeader.h
//  Pods
//
//  Created by DingWenchao on 6/29/15.
//
//  动感头文件，仅供框架内部使用。
//  Private header, don't use it.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "CFSCalendar.h"
#import "CFSCalendarCell.h"
#import "CFSCalendarHeader.h"
#import "CFSCalendarStickyHeader.h"
#import "CFSCalendarCollectionView.h"
#import "CFSCalendarFlowLayout.h"
#import "CFSCalendarScopeHandle.h"
#import "CFSCalendarAnimator.h"

@interface CFSCalendar (Dynamic)

@property (readonly, nonatomic) NSExtensionContext *extensionContext;
@property (readonly, nonatomic) CFSCalendarHeader *header;
@property (readonly, nonatomic) CFSCalendarCollectionView *collectionView;
@property (readonly, nonatomic) CFSCalendarScopeHandle *scopeHandle;
@property (readonly, nonatomic) CFSCalendarFlowLayout *collectionViewLayout;
@property (readonly, nonatomic) CFSCalendarAnimator *animator;
@property (readonly, nonatomic) NSArray *weekdays;
@property (readonly, nonatomic) BOOL floatingMode;
@property (readonly, nonatomic) NSArray *visibleStickyHeaders;
@property (readonly, nonatomic) CGFloat preferredHeaderHeight;
@property (readonly, nonatomic) CGFloat preferredWeekdayHeight;
@property (readonly, nonatomic) CGFloat preferredRowHeight;
@property (readonly, nonatomic) CGFloat preferredPadding;
@property (readonly, nonatomic) UIView *bottomBorder;

@property (readonly, nonatomic) NSCalendar *gregorian;
@property (readonly, nonatomic) NSDateComponents *components;
@property (readonly, nonatomic) NSDateFormatter *formatter;

@property (readonly, nonatomic) UIView *contentView;
@property (readonly, nonatomic) UIView *daysContainer;

@property (assign, nonatomic) BOOL needsAdjustingMonthPosition;
@property (assign, nonatomic) BOOL needsAdjustingViewFrame;

- (void)invalidateWeekdayFont;
- (void)invalidateWeekdayTextColor;

- (void)invalidateHeaders;
- (void)invalidateWeekdaySymbols;
- (void)invalidateAppearanceForCell:(CFSCalendarCell *)cell;

- (BOOL)isPageInRange:(NSDate *)page;
- (BOOL)isDateInRange:(NSDate *)date;
- (NSDate *)dateForIndexPath:(NSIndexPath *)indexPath;
- (NSDate *)dateForIndexPath:(NSIndexPath *)indexPath scope:(CFSCalendarScope)scope;
- (NSIndexPath *)indexPathForDate:(NSDate *)date;
- (NSIndexPath *)indexPathForDate:(NSDate *)date scope:(CFSCalendarScope)scope;

- (NSInteger)numberOfHeadPlaceholdersForMonth:(NSDate *)month;
- (NSInteger)numberOfRowsInMonth:(NSDate *)month;

- (NSDate *)beginingOfMonth:(NSDate *)month;
- (NSDate *)endOfMonth:(NSDate *)month;
- (NSDate *)beginingOfWeek:(NSDate *)week;
- (NSDate *)endOfWeek:(NSDate *)week;
- (NSDate *)middleOfWeek:(NSDate *)week;
- (NSInteger)numberOfDatesInMonth:(NSDate *)month;

- (CGSize)sizeThatFits:(CGSize)size scope:(CFSCalendarScope)scope;

@end

@interface CFSCalendarAppearance (Dynamic)

@property (readwrite, nonatomic) CFSCalendar *calendar;

@property (readonly, nonatomic) NSDictionary *backgroundColors;
@property (readonly, nonatomic) NSDictionary *titleColors;
@property (readonly, nonatomic) NSDictionary *subtitleColors;
@property (readonly, nonatomic) NSDictionary *borderColors;

@property (readonly, nonatomic) UIFont *preferredTitleFont;
@property (readonly, nonatomic) UIFont *preferredSubtitleFont;
@property (readonly, nonatomic) UIFont *preferredWeekdayFont;
@property (readonly, nonatomic) UIFont *preferredHeaderTitleFont;

- (void)adjustTitleIfNecessary;
- (void)invalidateFonts;

@end
