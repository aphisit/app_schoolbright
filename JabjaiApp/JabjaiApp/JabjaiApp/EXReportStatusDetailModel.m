//
//  EXReportStatusDetailModel.m
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "EXReportStatusDetailModel.h"

@implementation EXReportStatusDetailModel

@synthesize iid = _iid;
@synthesize title = _title;
@synthesize amount = _amount;
@synthesize percent = _percent;
@synthesize isButton = _isButton;

- (void)setIId:(long long)iid {
    _iid = iid;
}

- (void)setTitle:(NSString *)title {
    _title = title;
}

- (void)setAmount:(NSInteger)amount {
    _amount = amount;
}

- (void)setPercent:(double)percent {
    _percent = percent;
}

- (void)setIsButton:(BOOL)isButton {
    _isButton = isButton;
}

- (long long)getIId {
    return _iid;
}

- (NSString *)getTitle {
    return _title;
}

- (NSInteger)getAmount {
    return _amount;
}

- (double)getPercent {
    return _percent;
}

- (BOOL)getIsButton {
    return _isButton;
}

@end
