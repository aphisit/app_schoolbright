//
//  BSSelectStudentsViewController.m
//  JabjaiApp
//
//  Created by mac on 7/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSSelectStudentsViewController.h"
#import "BSSelectClassLevelViewController.h"
#import "BSSelectBehaviorScoreViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "Constant.h"

@interface BSSelectStudentsViewController () {
   
    UIColor *greenBGColor;
    UIColor *redBGColor;
    
    BOOL optionStatusSelectAll;
    
    // The dialog
    AlertDialog *alertDialog;
}

@property (strong, nonatomic) CallGetStudentInClassroomAPI *callGetStudentInClassroomAPI;

@end

static NSString *cellIdentifier = @"Cell";

@implementation BSSelectStudentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSStudentNameRadioTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    greenBGColor = [UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0];
    redBGColor = [UIColor colorWithRed:220/255.0 green:43/255.0 blue:69/255.0 alpha:1.0];
    
    self.titleLabel.text = [[self.classroomArray objectAtIndex:self.selectedClassroomIndex] getClassroomName];
    
    if(_mode == BS_MODE_ADD) {
        self.headerTitleLabel.text = @"เพิ่มคะแนนความประพฤติ";
        [self.nextButton setBackgroundColor:greenBGColor];
    }
    else {
        self.headerTitleLabel.text = @"ตัดคะแนนความประพฤติ";
        [self.nextButton setBackgroundColor:redBGColor];
    }
    
    if(self.selectedStudentArray == nil) {
        [self getStudentInClassroom];
    }
    
    [self checkOptionButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.selectedStudentArray != nil) {
        return self.selectedStudentArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BSStudentNameRadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    BSSelectedStudent *model = [self.selectedStudentArray objectAtIndex:indexPath.row];
    
    cell.delegate = self;
    cell.index = indexPath.row;
    cell.titleLabel.text = [NSString stringWithFormat:@"%li. %@", (indexPath.row + 1), [model getStudentName]];
    
    if([model isSelected]) {
        
        if(self.mode == BS_MODE_ADD) {
            NSLog(@"Mode = %d",self.mode);
            [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
        }
        else {    
            [cell setRadioButtonSelectedWithType:RADIO_TYPE_RED];
        }
    }
    else {
        [cell setRadioButtonClearSelected];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}



#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}


#pragma mark - BSRadionTableViewCellDelegate

- (void)onPressRadioButton:(BSStudentNameRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index{
    
    
    BOOL isSelected = [[self.selectedStudentArray objectAtIndex:index] isSelected];
    [[self.selectedStudentArray objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - API Caller

- (void)getStudentInClassroom {
    
    if(self.callGetStudentInClassroomAPI != nil) {
        self.callGetStudentInClassroomAPI = nil;
    }
    
    self.callGetStudentInClassroomAPI = [[CallGetStudentInClassroomAPI alloc] init];
    self.callGetStudentInClassroomAPI.delegate = self;
    [self.callGetStudentInClassroomAPI call:[UserData getSchoolId] classroomId:_classroomId];
}

#pragma mark - API Delegate

- (void)callGetStudentInClassrommAPI:(CallGetStudentInClassroomAPI *)classObj data:(NSArray<BSSelectedStudent *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _selectedStudentArray = data;
        
        [self.tableView reloadData];
        [self checkOptionButton];
    }
}

#pragma mark - Dialog

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil) {
        if([alertDialog isDialogShowing]) {
            [alertDialog dismissDialog];
        }
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Utility

- (BOOL)validateData {
    
    if(_selectedStudentArray != nil) {
        for(BSSelectedStudent *model in _selectedStudentArray) {
            
            if([model isSelected]) {
                return YES;
            }
        }
    }
    
    return NO;
}

- (void)gotoBSSelectBehaviorScoreViewController {
    
    BSSelectBehaviorScoreViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectBehaviorScoreStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    
    viewController.selectedStudentArray = _selectedStudentArray;
        
    viewController.behaviorScoreArray = _behaviorScoreArray;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (BOOL)isStudentSelectAll {
    
    BOOL selectAll = NO;
    
    if(_selectedStudentArray != nil) {
        for(BSSelectedStudent *model in _selectedStudentArray) {
            
            if(![model isSelected]) {
                return NO;
            }
            else {
                selectAll = YES;
            }
        }
    }
    
    return selectAll;
}

- (void)checkOptionButton {
    
    if(![self isStudentSelectAll]) {
        optionStatusSelectAll = YES;
        [self.optSelectButton setImage:[UIImage imageNamed:@"ic_select_all"] forState:UIControlStateNormal];
    }
    else {
        optionStatusSelectAll = NO;
        [self.optSelectButton setImage:[UIImage imageNamed:@"ic_deselect_all"] forState:UIControlStateNormal];
    }
}

- (void)selectAll {
    
    if(_selectedStudentArray != nil) {
        for(BSSelectedStudent *model in _selectedStudentArray) {
            [model setSelected:YES];
        }
    }
    
    [self.tableView reloadData];
}

- (void)deSelectAll {
    if(_selectedStudentArray != nil) {
        for(BSSelectedStudent *model in _selectedStudentArray) {
            [model setSelected:NO];
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - Action functions
- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        [self gotoBSSelectBehaviorScoreViewController];
    }
    else {
        NSString *alertMessage = @"กรุณาเลือกนักเรียนอย่างน้อย 1 คน";
        [self showAlertDialogWithMessage:alertMessage];
    }
}

- (IBAction)moveBack:(id)sender {
    
    BSSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectClassLevelStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    
    viewController.selectedStudentArray = _selectedStudentArray;
    
    viewController.behaviorScoreArray = _behaviorScoreArray;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}

- (IBAction)actionSelect:(id)sender {
    
    if(optionStatusSelectAll) {
        [self selectAll];
    }
    else {
        [self deSelectAll];
    }
    
    [self checkOptionButton];
}
@end
