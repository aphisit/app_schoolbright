//
//  ProgressTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
