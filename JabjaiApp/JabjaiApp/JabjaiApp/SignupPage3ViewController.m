//
//  SignupPage3ViewController.m
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SignupPage3ViewController.h"
#import "SignupPage2ViewController.h"
#import "SignupPage4ViewController.h"
#import "StaffSignupPage1ViewController.h"
#import "MultilineAlertDialog.h"
#import "AlertDialog.h"
#import "Utils.h"

@interface SignupPage3ViewController () {
    Boolean isShowCreditLimits;
    
    AlertDialog *alertDialog;
    DatePickerDialog *datePickerDialog;
    MultilineAlertDialog *multilineAlertDialog;
    
    NSDate *birthdayDate;
}

@end

@implementation SignupPage3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.telephoneNumberTextField.delegate = self;
    self.birthdayTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.creditLimitsTextField.delegate = self;
    
    isShowCreditLimits = YES;
    
    alertDialog = [[AlertDialog alloc] init];
    multilineAlertDialog = [[MultilineAlertDialog alloc] init];
    
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSDate *date = [gregorian dateByAddingUnit:NSCalendarUnitYear value:-10 toDate:[NSDate date] options:0];
    
    datePickerDialog = [[DatePickerDialog alloc] initWithDate:date];
    datePickerDialog.delegate = self;
    
    // Add gesture recognizer for hide keyboard when tap outside phone pad keyboard
    UITapGestureRecognizer *phonePadTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(phonePadHideKeyboard)];
    [self.view addGestureRecognizer:phonePadTap];
    
    // Restore the data
    if(_signupDataModel.birthDay != nil) {
        NSDateFormatter *formatter = [Utils getDateFormatter];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        NSString *strDate = [formatter stringFromDate:_signupDataModel.birthDay];
        
        self.birthdayTextField.text = strDate;
        birthdayDate = _signupDataModel.birthDay;
    }
    
    if(_signupDataModel.phoneNumber != nil) {
        self.telephoneNumberTextField.text = _signupDataModel.phoneNumber;
    }
    
    if(_signupDataModel.email != nil) {
        self.emailTextField.text = _signupDataModel.email;
    }
    
    if(_signupDataModel.creditLimit != nil && [_signupDataModel.creditLimit length] != 0) {
        
        //NSLog(@"Credit Limit Length : %i", (int)[_signupDataModel.creditLimit length]);
        
        self.creditLimitsTextField.text = [NSString stringWithFormat:@"%i", [_signupDataModel.creditLimit intValue]];
    }
    
    // Docorate the views
    self.nextBtn.layer.cornerRadius = 20;
    self.nextBtn.layer.masksToBounds = YES;
    
    if([_signupDataModel.academyType intValue] == 2) {
        //If user is university's user hide creditslimite fields
        
        [self.creditLimitsTextField setEnabled:NO];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.creditsLimitStackView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0]];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
}

#pragma mark - Keyboard

- (void)phonePadHideKeyboard {
    [self.telephoneNumberTextField resignFirstResponder];
}

- (void)emailHideKeyboard {
    [self.emailTextField resignFirstResponder];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"signupPage2Segue"]) {
        SignupPage2ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
    else if([segue.identifier isEqualToString:@"staffSignupPage1Segue"]) {
        StaffSignupPage1ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
    else if([segue.identifier isEqualToString:@"signupPage4Segue"]) {
        SignupPage4ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
        controller.previousSegue = _previousSegue;
    }
}

- (IBAction)moveBack:(id)sender {
    [self performSegueWithIdentifier:self.previousSegue sender:self];
}

- (IBAction)nextStep:(id)sender {
    
    BOOL isValidData = [self validateData];
    
    if(isValidData) {
        
        NSMutableString *phoneNumber = [[NSMutableString alloc] initWithString:self.telephoneNumberTextField.text];
        CFStringTrimWhitespace((__bridge CFMutableStringRef) phoneNumber);

        NSMutableString *email = [[NSMutableString alloc] initWithString:self.emailTextField.text];
        CFStringTrimWhitespace((__bridge CFMutableStringRef) email);
        
//      _signupDataModel.personalID = phoneNumber;
        _signupDataModel.phoneNumber = phoneNumber;
        _signupDataModel.email = email;
        _signupDataModel.birthDay = birthdayDate;
        
        
        NSNumber *creditLimits = [[NSNumber alloc] initWithInteger:0];
        
        NSMutableString *creditText = [[NSMutableString alloc] initWithString:self.creditLimitsTextField.text];
        CFStringTrimWhitespace((__bridge CFMutableStringRef) creditText);
        
        if([creditText length] != 0) {
            creditLimits = [[NSNumber alloc] initWithInteger:[creditText integerValue]];
        }
        
        
        if([creditLimits intValue] <= 0) {
            _signupDataModel.creditLimit = @"";
        }
        else {
            _signupDataModel.creditLimit = [creditLimits stringValue];
        }
        
        [self performSegueWithIdentifier:@"signupPage4Segue" sender:self];
    }
}

- (IBAction)dismissKeyboard:(id)sender {
    [self resignFirstResponder];
}

#pragma mark - Individual Functions
-(BOOL)validateData {

    NSString *dialogTitle = @"แจ้งเตือน";
    
    if([self.telephoneNumberTextField.text length] == 0 || [self.birthdayTextField.text length] == 0 || [self.emailTextField.text length] == 0) {
        NSString *alertMessage = @"กรุณากรอกข้อมูลให้ครบถ้วน";
        
        [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        
        return NO;
    }
    else if([self.telephoneNumberTextField.text length] > 20) {
        NSString *alertMessage = @"ขออภัยระบบไม่รองรับหมายเลขโทรศัพท์ความยาวมากกว่า 20 ตัวอักษร";
        
        [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        
        return NO;
    }
    else if(![Utils isValidEmail:self.emailTextField.text]) {
        NSString *alertMessage = @"กรุณาระบุอีเมลให้ถูกต้อง";
        
        [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        
        return NO;
    }
    else if([self.creditLimitsTextField.text length] != 0 && ![Utils isInteger:self.creditLimitsTextField.text]) {
        NSString *alertMessage = @"กรุณาระบุวงเงินจำกัดเป็นตัวเลขจำนวนเต็ม";
        
        [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 2) { //tag 2 : birthdayTextField
        
        [self showDatePickerDialog];
        
        return NO;
        
    }
    else if(textField.tag == 4 && isShowCreditLimits) {
        NSString *title = @"ข้อควรรู้";
        NSString *hint = @"ใส่จำนวนเงินสูงสุดในการใช้ต่อวัน หากไม่ต้องการจำกัดวงเงิน โปรดใส่ 0 หรือเว้นว่างเอาไว้";
        
        [self showMultilineAlertDialogWithTitle:title message:hint];
            
        isShowCreditLimits = NO;
        
        return NO;
    }
    
    return YES;
}

#pragma mark - DatePickerDialogDelegate
-(void)onSelectDate:(NSDate *)date {
    
    birthdayDate = date;
    
    NSDateFormatter *dateFormatter = [Utils getDateFormatter];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:birthdayDate];
    
    self.birthdayTextField.text = strDate;
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showMultilineAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(multilineAlertDialog != nil && [multilineAlertDialog isDialogShowing]) {
        [multilineAlertDialog dismissDialog];
    }
    
    [multilineAlertDialog showDialogInView:self.view title:title message:message];
}

- (void)showDatePickerDialog {
    
    if(datePickerDialog != nil && [datePickerDialog isDialogShowing]) {
        [datePickerDialog dismissDialog];
    }
    
    [datePickerDialog showDialogInView:self.view];
}

@end
