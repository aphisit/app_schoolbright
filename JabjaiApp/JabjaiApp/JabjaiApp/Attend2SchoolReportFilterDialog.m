//
//  Attend2SchoolReportFilterDialog.m
//  JabjaiApp
//
//  Created by mac on 12/16/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "Attend2SchoolReportFilterDialog.h"
#import "Utils.h"
#import "DateUtility.h"
#import "YearTermModel.h"

@interface Attend2SchoolReportFilterDialog () {
    BOOL selectStartDateTextField;
    
    NSMutableArray<NSNumber *> *yearList;
    NSMutableArray *semesterList;
    NSMutableArray *statusList;
    
    NSDictionary *yearTermDict; //Key is yearNumber and data are array of YearTermModel
    
    NSNumber *selected_year;
    NSString *selected_semester;
    NSString *selected_status;
}

@property (nonatomic, assign) BOOL isShowing;

@property (strong, nonatomic) CalendarDialog *calendarDialog;
@property (strong, nonatomic) NSDate *minDate;
@property (strong, nonatomic) NSDate *maxDate;
@property (strong, nonatomic) NSCalendar *gregorian;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (strong, nonatomic) UIColor *orangeColor;

@end

@implementation Attend2SchoolReportFilterDialog

- (id)initWithYearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict {
    
    self = [super init];
    
    if(self) {
        yearTermDict = dataDict;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.orangeColor = UIColorWithHexString(@"#F56B20");
    
    // Decorate view
    self.dialogView.layer.cornerRadius = 10;
    self.dialogView.layer.masksToBounds = YES;
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10,10)].CGPath;
    self.headerView.layer.mask = maskLayer;
    self.headerView.layer.masksToBounds = YES;
    
    self.cancelButton.layer.cornerRadius = 20;
    self.cancelButton.layer.masksToBounds = YES;
    self.okButton.layer.cornerRadius = 20;
    self.okButton.layer.masksToBounds = YES;
    
    // Set up dropdown
    self.schoolYearDropdown.dataSource = self;
    self.schoolYearDropdown.delegate = self;
    self.schoolYearDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.schoolYearDropdown.backgroundDimmingOpacity = 0;
    self.schoolYearDropdown.dropdownShowsTopRowSeparator = YES;
    self.schoolYearDropdown.dropdownCornerRadius = 5.0;
    self.schoolYearDropdown.tintColor = [UIColor blackColor];
    
    self.semesterDropdown.dataSource = self;
    self.semesterDropdown.delegate = self;
    self.semesterDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.semesterDropdown.backgroundDimmingOpacity = 0;
    self.semesterDropdown.dropdownShowsTopRowSeparator = YES;
    self.semesterDropdown.dropdownCornerRadius = 5.0;
    self.semesterDropdown.tintColor = [UIColor blackColor];
    
    self.statusDropdown.dataSource = self;
    self.statusDropdown.delegate = self;
    self.statusDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.statusDropdown.backgroundDimmingOpacity = 0;
    self.statusDropdown.dropdownShowsTopRowSeparator = YES;
    self.statusDropdown.dropdownCornerRadius = 5.0;
    self.statusDropdown.tintColor = [UIColor blackColor];
    
    self.schoolYearDropdown.layer.cornerRadius = 5;
    self.schoolYearDropdown.layer.masksToBounds = YES;
    self.semesterDropdown.layer.cornerRadius = 5;
    self.semesterDropdown.layer.masksToBounds = YES;
    self.statusDropdown.layer.cornerRadius = 5;
    self.statusDropdown.layer.masksToBounds = YES;
    self.startDateTextField.layer.cornerRadius = 5;
    self.startDateTextField.layer.masksToBounds = YES;
    self.endDateTextField.layer.cornerRadius = 5;
    self.endDateTextField.layer.masksToBounds = YES;
    
    // Set textfield delegate
    self.startDateTextField.delegate = self;
    self.endDateTextField.delegate = self;
    
    // Initialize variables
    self.isShowing = NO;
    
    self.gregorian = [Utils getGregorianCalendar];
    self.maxDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:+1 toDate:[NSDate date] options:0];//[NSDate date];
    self.minDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:[NSDate date] options:0];
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.calendarDialog = [[CalendarDialog alloc] initWithTitle:@"เลือกวันที่" minimumDate:self.minDate maximumDate:self.maxDate];
    self.calendarDialog.delegate = self;
    
    yearList = [[NSMutableArray alloc] init];
    semesterList = [[NSMutableArray alloc] init];
    statusList = [[NSMutableArray alloc] init];
    
    [self initializeYearData];
    [self initializeSemesterData];
    
    // Initialize Status
    [statusList addObject:@"ทั้งหมด"];
    [statusList addObject:@"ตรงเวลา"];
    [statusList addObject:@"สาย"];
    [statusList addObject:@"ขาด"];
    [statusList addObject:@"ลา"];
    [statusList addObject:@"วันหยุด"];
    
    // Make default
    selected_status = [statusList objectAtIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onPressCancel:(id)sender {
    [self dismissDialog];
}

- (IBAction)onPressOK:(id)sender {

    NSDate *startDate = nil;
    NSDate *endDate = nil;
    
    if(self.startDateTextField.text.length > 0) {
        startDate = [self.dateFormatter dateFromString:self.startDateTextField.text];
    }
    else {
        startDate = self.minDate;
    }
    
    if(self.endDateTextField.text.length > 0) {
        endDate = [self.dateFormatter dateFromString:self.endDateTextField.text];
    }
    else {
        endDate = self.maxDate;
    }
    
    [self dismissDialog];
    
    [self.delegate applyFilter:selected_year semester:selected_semester status:selected_status startDate:startDate endDate:endDate];
    
    
}

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    self.titleLabel.text = title;
    
    self.isShowing = YES;
    
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
    
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

- (void)YearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict {
    yearTermDict = dataDict;
    
    [self initializeYearData];
    [self initializeSemesterData];
}

#pragma mark - MKDropdownMenuDataSource

-(NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu {
    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component {
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        return yearList.count;
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        return semesterList.count;
    }
    else if(dropdownMenu.tag == 2) { // 2 : Status
        return statusList.count;
    }
    
    return 0;
}

#pragma mark - MKDropdownMenuDelegate

- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
    return 35;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {
    
    NSString *textString;
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        textString = [[NSString alloc] initWithFormat:@"%d", [selected_year intValue]];
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        textString = selected_semester;
    }
    else if(dropdownMenu.tag == 2) { // 2 : Status
        textString = selected_status;
    }
    else {
        textString = @"";
    }

    NSAttributedString *attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"ThaiSansNeue-Regular" size:24], NSForegroundColorAttributeName: self.orangeColor }];
    
    return attributes;
}

-(NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *textString;
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        textString = [[NSString alloc] initWithFormat:@"%d", [[yearList objectAtIndex:row] intValue]];
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        textString = [semesterList objectAtIndex:row];
    }
    else if(dropdownMenu.tag == 2) { // 2 : Status
        textString = [statusList objectAtIndex:row];
    }
    else {
        textString = @"";
    }
    
    NSAttributedString *attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"ThaiSansNeue-Regular" size:24], NSForegroundColorAttributeName: self.orangeColor }];
    
    return attributes;
}

- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
    return NO;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        NSNumber *year = [yearList objectAtIndex:row];
        
        if([year intValue] != [selected_year intValue]) {
            selected_year = year;
            [self initializeSemesterData];
            
            [dropdownMenu reloadAllComponents];
            [self.semesterDropdown reloadAllComponents];
            self.startDateTextField.text = @"";
            self.endDateTextField.text = @"";
        }
        
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        
        NSString *semester = [semesterList objectAtIndex:row];
        
        if(![semester isEqualToString:selected_semester]) {
            selected_semester = semester;
            [self adjustCalendarDateRangeBySemester];
            
            [dropdownMenu reloadAllComponents];
            self.startDateTextField.text = @"";
            self.endDateTextField.text = @"";
        }
        
    }
    else if(dropdownMenu.tag == 2) { // 2 : Status
        
        NSString *status = [statusList objectAtIndex:row];
        
        if(![status isEqualToString:selected_status]) {
            selected_status = status;
            [dropdownMenu reloadAllComponents];
        }
    }
    
    delay(0.15, ^{
        [dropdownMenu closeAllComponentsAnimated:YES];
    });
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 0) { // tag 0 : startDateTextField
        
        if([self.calendarDialog isDialogShowing]) {
            [self.calendarDialog dismissDialog];
        }
        
        [self.calendarDialog showDialogInView:self.view];
        
        selectStartDateTextField = YES;
        
        return NO;
    }
    else if(textField.tag == 1) { // tag 1 : endDateTextField
        
        if([self.calendarDialog isDialogShowing]) {
            [self.calendarDialog dismissDialog];
        }
        
        [self.calendarDialog showDialogInView:self.view];
        
        selectStartDateTextField = NO;
        
        return NO;
    }
    
    return YES;
}

#pragma mark - CalendarDialogDelegate
-(void)calendarDialogPressOK:(NSArray<NSDate *> *)selectedDates {
    
    if(selectedDates.count >= 2) {
        NSDate *minDate = [DateUtility minDate:selectedDates];
        NSDate *maxDate = [DateUtility maxDate:selectedDates];
        
        self.startDateTextField.text = [self.dateFormatter stringFromDate:minDate];
        self.endDateTextField.text = [self.dateFormatter stringFromDate:maxDate];
    }
    else if(selectedDates.count > 0) {
        
        NSDate *selectDate = [selectedDates objectAtIndex:0];
        
        if(selectStartDateTextField) {
            self.startDateTextField.text = [self.dateFormatter stringFromDate:selectDate];
            self.endDateTextField.text = @"";
        }
        else {
            self.endDateTextField.text = [self.dateFormatter stringFromDate:selectDate];
            self.startDateTextField.text = @"";
        }
        
    }
    else {
        self.startDateTextField.text = @"";
        self.endDateTextField.text = @"";
    }
}

-(void)calendarDialogPressCancel {
    NSLog(@"%@", @"Cancel calendar dialog");
}

#pragma mark - Manage dialog data

// extract year from yeartermdict
- (void)initializeYearData {
    
    if(yearTermDict == nil) {
        return;
    }
    
    if(yearList != nil && yearList.count != 0) {
        [yearList removeAllObjects];
    }
    
    yearList = [[NSMutableArray alloc] initWithArray:[yearTermDict allKeys]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
    [yearList sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    // Make first year in array as default
    selected_year = [yearList objectAtIndex:0];
    
}

- (void)initializeSemesterData {
    
    if(yearTermDict == nil || selected_year == nil) {
        return;
    }
    
    if(semesterList != nil && semesterList.count != 0) {
        [semesterList removeAllObjects];
    }
    
    NSArray *yearSemesters = [yearTermDict objectForKey:selected_year];
    NSMutableArray *semesters = [[NSMutableArray alloc] init];
    
    NSMutableArray *allDatesInYear = [[NSMutableArray alloc] init];
    
    for(int i=0; i<yearSemesters.count; i++) {
        YearTermModel *model = [yearSemesters objectAtIndex:i];
//        NSNumber *semesterNO = [[NSNumber alloc] initWithInt:[model.termName intValue]];
//        [semesters addObject:semesterNO];
        
        [semesters addObject:model.termName];
        
        [allDatesInYear addObject:model.termBegin];
        [allDatesInYear addObject:model.termEnd];
    }
    
//    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
//    [semesters sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    semesterList = [[NSMutableArray alloc] initWithCapacity:semesters.count + 1];
    [semesterList addObject:@"ทั้งหมด"];
    
    for(int i=0; i < semesters.count; i++) {;
        NSString *semesterStr = [[NSString alloc] initWithFormat:@"%@", [semesters objectAtIndex:i]];
        [semesterList addObject:semesterStr];
    }
    
    // Make the first order in semesterList as default
    selected_semester = [semesterList objectAtIndex:0];
    
    self.minDate = [DateUtility minDate:allDatesInYear];
    self.maxDate = [DateUtility maxDate:allDatesInYear];
    
    [self.calendarDialog setDateWithMinimumDate:self.minDate maximumDate:self.maxDate];
}

- (void)adjustCalendarDateRangeBySemester {
    
    if(yearTermDict == nil || selected_year == nil) {
        return;
    }
    
    NSArray *yearSemesters = [yearTermDict objectForKey:selected_year];
    NSMutableArray *allDatesInTerm = [[NSMutableArray alloc] init];
    
    for(int i=0; i<yearSemesters.count; i++) {
        YearTermModel *model = [yearSemesters objectAtIndex:i];
        
        if([selected_semester isEqualToString:@"ทั้งหมด"]) {
            [allDatesInTerm addObject:model.termBegin];
            [allDatesInTerm addObject:model.termEnd];
        }
        else if([selected_semester isEqualToString:model.termName]) {
            [allDatesInTerm addObject:model.termBegin];
            [allDatesInTerm addObject:model.termEnd];
            
            break;
        }
    }
    
    self.minDate = [DateUtility minDate:allDatesInTerm];
    self.maxDate = [DateUtility maxDate:allDatesInTerm];
    
    [self.calendarDialog setDateWithMinimumDate:self.minDate maximumDate:self.maxDate];
    
}

@end
