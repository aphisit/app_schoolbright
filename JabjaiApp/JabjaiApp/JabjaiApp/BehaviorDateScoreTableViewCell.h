//
//  BehaviorDateScoreTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 7/27/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BehaviorDateScoreTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *behaviorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@end
