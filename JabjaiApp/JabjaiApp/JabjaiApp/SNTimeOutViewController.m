//
//  SNTimeOutViewController.m
//  JabjaiApp
//
//  Created by toffee on 9/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNTimeOutViewController.h"
#import <QBImagePickerController/QBImagePickerController.h>
#import "SNLevelStudentViewController.h"

@interface SNTimeOutViewController ()

@end

@implementation SNTimeOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //SWRevealViewController *revealViewController = self.revealViewController;
    self.detail.layer.borderWidth = 1;
    self.detail.layer.borderColor = [UIColor grayColor].CGColor;
    self.sendFileView.layer.borderWidth = 1;
    self.sendFileView.layer.borderColor = [UIColor grayColor].CGColor;
    self.sendImageView.layer.borderWidth = 1;
    self.sendImageView.layer.borderColor = [UIColor grayColor].CGColor;
    chackRadioSendNow = YES;
    chackRadioSetTime = NO;
    
    [self.btnSendNow setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
    
    [self doInit];
   
   }

- (void)doInit{
    
        // creat PicKerTime
        self.timeTextFiled.delegate = self;
        datePicker = [[UIDatePicker alloc]init];
        [datePicker setDate:[NSDate date]];
        [datePicker setDatePickerMode:UIDatePickerModeTime]; //setdatatime
        [datePicker addTarget:self action:@selector(doTimeTextField:) forControlEvents:UIControlEventValueChanged];
        [self.timeTextFiled setInputView:datePicker];
        
        // creat PicKerDate
        self.dateTextFiled.delegate = self;
        datePicker = [[UIDatePicker alloc]init];
        [datePicker setDate:[NSDate date]];
        [datePicker setDatePickerMode:UIDatePickerModeDate]; //setdatatime
        [datePicker addTarget:self action:@selector(doDateTextField:) forControlEvents:UIControlEventValueChanged];
        [self.dateTextFiled setInputView:datePicker];
        
        
        // creat button Done
        UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setTintColor:[UIColor blackColor]];
        UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(switchDatePickerHidden)];
        UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
        [self.dateTextFiled setInputAccessoryView:toolBar];
        [self.timeTextFiled setInputAccessoryView:toolBar];

    
    

}

- (void)switchDatePickerHidden
{
    if(chackDateTime == 0){
         [_dateTextFiled resignFirstResponder];
    }
    else{
         [_timeTextFiled resignFirstResponder];
    }
}

-(void)doTimeTextField:(id)sender {
    if(chackRadioSetTime == YES){
        chackDateTime = 1;
        UIDatePicker *picker = (UIDatePicker*)self.timeTextFiled.inputView;
        self.timeTextFiled.text = [self formatDate:picker.date];
    }
   
}

-(void)doDateTextField:(id)sender {
    if(chackRadioSetTime == YES){
        chackDateTime = 0;
        UIDatePicker *picker = (UIDatePicker*)self.dateTextFiled.inputView;
        self.dateTextFiled.text = [self formatDate:picker.date];
    }
}

- (NSString *)formatDate:(NSDate *)date {
    
    if(chackDateTime == 0){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
        NSString *formattedDate = [dateFormatter stringFromDate:date];
        return formattedDate;
    }
    else{
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        [dateFormatter setDateFormat:@"HH:mm"];
        NSString *formattedDate = [dateFormatter stringFromDate:date];
        return formattedDate;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)sendNowRadio:(id)sender {
    
    if(chackRadioSetTime == YES){
            [self.btnSettime setImage:[UIImage imageNamed:@"ic_radio_clear"] forState:UIControlStateNormal];
            [self.btnSendNow setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
        self.dateTextFiled.enabled = YES;
        self.timeTextFiled.userInteractionEnabled = YES;
            chackRadioSetTime = NO;
            chackRadioSendNow = YES;
    
    }else{
        if(chackRadioSendNow == NO){
            [self.btnSendNow setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
            chackRadioSendNow = YES;
        }else{
            [self.btnSendNow setImage:[UIImage imageNamed:@"ic_radio_clear"] forState:UIControlStateNormal];
            chackRadioSendNow = NO;
        }
    
    }
    
   }

- (IBAction)setTimeRadio:(id)sender {
    if(chackRadioSendNow == YES){
        [self.btnSettime setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
        [self.btnSendNow setImage:[UIImage imageNamed:@"ic_radio_clear"] forState:UIControlStateNormal];
        chackRadioSetTime = YES;
        chackRadioSendNow = NO;
    }else{
        if(chackRadioSetTime == NO){
            [self.btnSettime setImage:[UIImage imageNamed:@"ic_radio_green"] forState:UIControlStateNormal];
            chackRadioSetTime = YES;
        
        }else{
            [self.btnSettime setImage:[UIImage imageNamed:@"ic_radio_clear"] forState:UIControlStateNormal];
            chackRadioSetTime = NO;
        }
    
    }
}
- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
    for (PHAsset *asset in assets) {
        // Do something with the asset
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)selectFile:(id)sender {
}


- (IBAction)selectImage:(id)sender {
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.maximumNumberOfSelection = 6;
    imagePickerController.showsNumberOfSelectedAssets = YES;
    
    [self presentViewController:imagePickerController animated:YES completion:NULL];
}
- (IBAction)backToMenu:(id)sender {
    SNLevelStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SendNewsStoryboard"];
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
@end
