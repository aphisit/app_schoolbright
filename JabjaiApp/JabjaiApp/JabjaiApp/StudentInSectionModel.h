//
//  StudentInSectionModel.h
//  JabjaiApp
//
//  Created by mac on 12/30/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StudentInSectionModel : NSObject

@property (nonatomic) int sectionID;
@property (strong, nonatomic) NSString *studentID;
@property (strong, nonatomic) NSString *studentName;

@end
