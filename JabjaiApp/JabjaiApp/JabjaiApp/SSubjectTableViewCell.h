//
//  SSubjectTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 5/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSubjectTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *redView;
@property (weak, nonatomic) IBOutlet UILabel *labelSubject;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeStart;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeEnd;

@end
