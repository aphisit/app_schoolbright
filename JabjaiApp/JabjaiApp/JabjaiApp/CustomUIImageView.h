//
//  CustomUIImageView.h
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUIImageView : UIImageView

@property (nonatomic, copy) NSString *imageURL;

- (void)loadImageFromURL:(NSString *)url;

@end
