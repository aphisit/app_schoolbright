//
//  TASelectClassLevelViewController.m
//  JabjaiApp
//
//  Created by mac on 7/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TASelectClassLevelViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "TAStudentListViewController.h"
#import "TAStudentStatusModel.h"

@interface TASelectClassLevelViewController () {
    
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    
    BOOL notArrangeTimeTable;
    
    // The dialog
    TableListDialog *tableListDialog;
    SubjectTableListDialog *subjectTableListDialog;
    AlertDialog *alertDialog;
}

@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;
@property (strong, nonatomic) CallGetSubjectAPI *callGetSubjectAPI;
@property (strong, nonatomic) CallGetStudentInClassAPI *callGetStudentInClassAPI;

@end

static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";
static NSString *subjectRequestCode = @"subjectRequestCode";

@implementation TASelectClassLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.classLevelTextField.delegate = self;
    self.classroomTextField.delegate = self;
    self.subjectTextField.delegate = self;
    
    if(_classLevelArray != nil && _classroomArray != nil && _subjectArray != nil) {
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classLevelArray.count; i++) {
            SchoolLevelModel *model = [_classLevelArray objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classroomArray.count; i++) {
            SchoolClassroomModel *model = [_classroomArray objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:_selectedClassLevelIndex] getClassLevelName];
        self.classroomTextField.text = [[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomName];
        self.subjectTextField.text = [_subjectArray objectAtIndex:_selectedSubjectIndex].subjectName;
        
        notArrangeTimeTable = NO;
        
    }
    else {
        _selectedClassLevelIndex = -1;
        _selectedClassroomIndex = -1;
        _selectedSubjectIndex = -1;
        
        notArrangeTimeTable = YES;
        
        [self getSchoolClassLevel];
    }
   
    if(_mode == TA_MODE_CHECK) {
        self.headerTitleLabel.text = @"เช็คชื่อรายวิชา";
    }
    else {
        self.headerTitleLabel.text = @"แก้ไขเช็คชื่อรายวิชา";
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // when user select class level textfield
        
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        
        return NO;
    }
    else if(textField.tag == 2) { // select classroom textfield
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }
        
        return NO;
    }
    else if(textField.tag == 3) {
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(self.classroomTextField.text.length == 0) {
            NSString *alertMessage = @"กรุณาเลือกชั้นเรียน";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(notArrangeTimeTable) {
            NSString *alertMessage = @"ขออภัย ขณะนี้ยังไม่ได้จัดตารางสอนในระบบ";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(_subjectArray != nil && _subjectArray.count > 0) {
            
            // show dialog
            [self showSubjectTableListDialogWithRequestCode:subjectRequestCode data:_subjectArray];
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - API Caller

- (void)getSchoolClassLevel {
    
    if(self.callGetSchoolLevelAPI != nil) {
        self.callGetSchoolLevelAPI = nil;
    }
    
    self.callGetSchoolLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolLevelAPI.delegate = self;
    [self.callGetSchoolLevelAPI call:[UserData getSchoolId]];
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}

- (void)getSubjectDataWithClassroomId:(long long)classroomId {
    
    if(self.callGetSubjectAPI != nil) {
        self.callGetSubjectAPI = nil;
    }
    
    self.callGetSubjectAPI = [[CallGetSubjectAPI alloc] init];
    self.callGetSubjectAPI.delegate = self;
    [self.callGetSubjectAPI call:[UserData getSchoolId] classroomId:classroomId];
}

- (void)getStudentInClassData {
    
    if(self.callGetStudentInClassAPI != nil) {
        self.callGetStudentInClassAPI = nil;
    }
    
    self.callGetStudentInClassAPI = [[CallGetStudentInClassAPI alloc] init];
    self.callGetStudentInClassAPI.delegate = self;
    [self.callGetStudentInClassAPI call:[UserData getSchoolId] classroomId:[[[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomID] longLongValue] subjectId:[_subjectArray objectAtIndex:_selectedSubjectIndex].subjectID teacherId:[UserData getUserID]];
}

#pragma mark - CallGetSchoolClassLevelAPIDelegate

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        
        _classLevelArray = data;
        
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
    }
}

#pragma mark - CallGetSchoolClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _classroomArray = data;
        
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}

#pragma mark - CallGetSubjectAPIDelegate

- (void)callGetSubjectAPI:(CallGetSubjectAPI *)classObj data:(NSArray<SSubjectModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _subjectArray = data;
        notArrangeTimeTable = NO;
    }
}

- (void)notArrangeTimeTable:(CallGetSubjectAPI *)classObj {
    notArrangeTimeTable = YES;
}

#pragma mark - CallGetStudentInClassAPIDelegate

- (void)callGetStudentInClassAPI:(CallGetStudentInClassAPI *)classObj data:(NSArray<TAStudentStatusModel *> *)data success:(BOOL)success {
    
    if(success && data != nil && data.count > 0) {
        
        BOOL alreadyCheck = NO;
        
        for(TAStudentStatusModel *model in data) {
            if([model getScanStatus] != -1) {
                alreadyCheck = YES;
                break;
            }
        }
        
        if(_mode == TA_MODE_CHECK && alreadyCheck) {
            NSString *alertMessage = @"วิชานี้ได้มีการเช็คชื่อเรียบร้อยแล้ว";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(_mode == TA_MODE_EDIT && !alreadyCheck) {
            NSString *alertMessage = @"วิชานี้ยังไม่มีการเช็คชื่อ";
            [self showAlertDialogWithMessage:alertMessage];
        }
        else {
            [self gotoTAStudentListViewController];
        }
        
    }
    else {
        [self gotoTAStudentListViewController];
    }
}

#pragma mark - Action functions
- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        [self getStudentInClassData];
    }
}

#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classroomTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกชั้นเรียน";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.subjectTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกวิชา";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}

- (void)gotoTAStudentListViewController {
    TAStudentListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TAStudentListStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.subjectArray = _subjectArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.selectedSubjectIndex = _selectedSubjectIndex;
    viewController.mode = _mode;
    
    viewController.classroomId = [[[_classroomArray objectAtIndex:_selectedClassroomIndex] getClassroomID] longLongValue];
    viewController.subjectId = [_subjectArray objectAtIndex:_selectedSubjectIndex].subjectID;
    viewController.subjectName = [_subjectArray objectAtIndex:_selectedSubjectIndex].subjectName;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
        
        if(index == _selectedClassLevelIndex) {
            return;
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classroomTextField.text = @"";
        self.subjectTextField.text = @"";
        
        _selectedClassroomIndex = -1;
        _selectedSubjectIndex = -1;
        
        // get classroom data with class level
        long long classLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:classLevelId];
        
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        
        if(index == _selectedClassroomIndex) {
            return;
        }
        
        self.classroomTextField.text = [[_classroomArray objectAtIndex:index] getClassroomName];
        _selectedClassroomIndex = index;
        
        // Clear dependency text field values;
        self.subjectTextField.text = @"";
        _selectedSubjectIndex = -1;
        
        // get subject data with classroom id
        long long classroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        [self getSubjectDataWithClassroomId:classroomId];
        
    }
    

}

#pragma mark - SubjectTableListDialogDelegate
- (void)subjectTableListDialog:(SubjectTableListDialog *)dialog onItemSelectWithRequestCode:(NSString *)requestCode atIndex:(NSInteger)index {
 
    if([requestCode isEqualToString:subjectRequestCode]) {
        
        if(index == _selectedSubjectIndex) {
            return;
        }
        
        self.subjectTextField.text = [_subjectArray objectAtIndex:index].subjectName;
        _selectedSubjectIndex = index;
    }

}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showSubjectTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray<SSubjectModel *> *)data {
    
    if(subjectTableListDialog != nil && [subjectTableListDialog isDialogShowing]) {
        [subjectTableListDialog dismissDialog];
        subjectTableListDialog = nil;
    }
    
    subjectTableListDialog = [[SubjectTableListDialog alloc] initWithRequestCode:requestCode];
    subjectTableListDialog.delegate = self;
    [subjectTableListDialog showDialogInView:self.view subjectArray:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

@end
