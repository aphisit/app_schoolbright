//
//  TAStudentListViewController.m
//  JabjaiApp
//
//  Created by mac on 7/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TAStudentListViewController.h"
#import "TASelectClassLevelViewController.h"
#import "TASummaryViewController.h"
#import "TAStudentStatusModel.h"
#import "UserData.h"
#import "Constant.h"
#import "AlertDialog.h"
#import "MultilineAlertDialog.h"

@interface TAStudentListViewController () {
    
    NSArray<TAStudentStatusModel *> *studentStatusHistoryForCopyArray;
    
    NSInteger selectedStudentIndex;
    
    UIColor *unauthorizedBG;
    UIColor *searchBarBG;
    BOOL searchActive;
    NSArray<TAStudentStatusModel *> *searchStudentStatusArray;
    
    UIGestureRecognizer *searchBarCancelGesture;
    
    // The dialog
    TAStatusDialog *taStatusDialog;
    AlertDialog *alertDialog;
    MultilineAlertDialog *multilineAlertDialog;
}

@property (strong, nonatomic) CallGetStudentInClassAPI *callGetStudentInClassAPI;
@property (strong, nonatomic) CallGetPreviousAttendClassStatusAPI *callGetPreviousAttendClassStatusAPI;

@end

static NSString *cellIdentifier = @"Cell";

@implementation TAStudentListViewController

@synthesize studentStatusArray = _studentStatusArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.titleLabel.text = self.subjectName;
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TAStudentStatusTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    
    selectedStudentIndex = -1;
    
    searchActive = NO;
    
    unauthorizedBG = [UIColor colorWithRed:213/255.0 green:213/255.0 blue:213/255.0 alpha:1.0];
    
    searchBarBG = [UIColor colorWithRed:224/255.0 green:93/255.0 blue:37/255.0 alpha:1];
    self.searchBar.delegate = self;
    self.searchBar.translucent = NO;
    self.searchBar.opaque = NO;
    self.searchBar.barTintColor = searchBarBG;
    self.searchBar.backgroundImage = [[UIImage alloc] init];
    
    UIView *view = [self.searchBar.subviews objectAtIndex:0];
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *cancelButton = (UIButton *)subView;
            [cancelButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        }
    }
    
    if(_studentStatusArray == nil) {
        [self getStudentInClassData];
    }
    else if(_mode == TA_MODE_EDIT) {
        if(![self checkHavePermission]) {
            [self showTeacherDonotHavePermissionDialog];
        }
    }
    
    if(_mode != TA_MODE_EDIT) {
        [self getPreviousAttendClassStatus];
    }
    else {
        [self.optCopyButton removeFromSuperview];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(searchActive) {
        if(searchStudentStatusArray != nil) {
            return searchStudentStatusArray.count;
        }
        else {
            return 0;
        }
    }
    else if(_studentStatusArray != nil) {
        return _studentStatusArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TAStudentStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    TAStudentStatusModel *model;
    
    if(searchActive) {
        model = [searchStudentStatusArray objectAtIndex:indexPath.row];
    }
    else {
        model = [_studentStatusArray objectAtIndex:indexPath.row];
    }
    
    cell.index = indexPath.row;
    cell.titleLabel.text = [NSString stringWithFormat:@"%li. %@", (indexPath.row + 1), [model getStudentName]];
    [cell updateStatus:[model scanStatus]];
    cell.delegate = self;
    
    if([model authorized]) {
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.userInteractionEnabled = YES;
    }
    else {
        cell.contentView.backgroundColor = unauthorizedBG;
        cell.userInteractionEnabled = NO;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - TAStudentTableViewCellDelegate

- (void)onSelectStatusButton:(TAStudentStatusTableViewCell *)tableViewCell atIndex:(NSInteger)index {
    selectedStudentIndex = index;
    
    [self showSelectStatusDialog];
}

#pragma mark - API Caller

- (void)getStudentInClassData {
    
    if(self.callGetStudentInClassAPI != nil) {
        self.callGetStudentInClassAPI = nil;
    }
    
    self.callGetStudentInClassAPI = [[CallGetStudentInClassAPI alloc] init];
    self.callGetStudentInClassAPI.delegate = self;
    [self.callGetStudentInClassAPI call:[UserData getSchoolId] classroomId:self.classroomId subjectId:self.subjectId teacherId:[UserData getUserID]];
}

- (void)getPreviousAttendClassStatus {
    
    if(self.callGetPreviousAttendClassStatusAPI != nil) {
        self.callGetPreviousAttendClassStatusAPI = nil;
    }
    
    self.callGetPreviousAttendClassStatusAPI = [[CallGetPreviousAttendClassStatusAPI alloc] init];
    self.callGetPreviousAttendClassStatusAPI.delegate = self;
    [self.callGetPreviousAttendClassStatusAPI call:[UserData getSchoolId] classroomId:self.classroomId subjectId:self.subjectId teacherId:[UserData getUserID]];
}

#pragma mark - API Delegate

- (void)callGetStudentInClassAPI:(CallGetStudentInClassAPI *)classObj data:(NSArray<TAStudentStatusModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _studentStatusArray = data;
        [self.tableView reloadData];
        
        if(_mode == TA_MODE_EDIT) {
            if(![self checkHavePermission]) {
                [self showTeacherDonotHavePermissionDialog];
            }
        }
    }
}

- (void)callGetPreviousAttendClassStatusAPI:(CallGetPreviousAttendClassStatusAPI *)classObj data:(NSArray<TAStudentStatusModel *> *)data success:(BOOL)success {
    
    if(success) {
        studentStatusHistoryForCopyArray = data;
    }
}

- (void)onNoPreviousAttendClassStatus:(CallGetPreviousAttendClassStatusAPI *)classObj {
    [self.optCopyButton removeFromSuperview];
}

#pragma mark - Dialog

- (void)showTeacherDonotHavePermissionDialog {
    NSArray<NSString *> *teachers = [self getTeacherName];
    
    if(teachers != nil && teachers.count > 0) {
        NSMutableString *message = [[NSMutableString alloc] initWithString:@"คุณไม่มีสิทธิ์ในการแก้ไข กรุณาติดต่ออาจารย์"];
        
        for(NSString *name in teachers) {
            [message appendString:[NSString stringWithFormat:@"\n%@", name]];
        }
        
        [self showMultilineAlertDialogWithMessage:message];
    }
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showMultilineAlertDialogWithMessage:(NSString *)message {
    
    if(multilineAlertDialog != nil && [multilineAlertDialog isDialogShowing]) {
        [multilineAlertDialog dismissDialog];
    }
    else {
        multilineAlertDialog = [[MultilineAlertDialog alloc] init];
    }
    
    [multilineAlertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showSelectStatusDialog {
    
    if(taStatusDialog != nil) {
        
        if([taStatusDialog isDialogShowing]) {
            [taStatusDialog dismissDialog];
        }
        
        taStatusDialog = nil;
    }
    
    taStatusDialog = [[TAStatusDialog alloc] init];
    taStatusDialog.delegate = self;
    [taStatusDialog showDialogInView:self.view];
}

- (void)taStatusDialog:(TAStatusDialog *)dialog onSelectStatus:(TA_SCAN_STATUS)scanStatus {
    
    if(selectedStudentIndex >= 0) {
        TAStudentStatusModel *model;
        
        if(searchActive) {
            model = [searchStudentStatusArray objectAtIndex:selectedStudentIndex];
        }
        else {
            model = [_studentStatusArray objectAtIndex:selectedStudentIndex];
        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedStudentIndex inSection:0];
        
        switch (scanStatus) {
            case TA_ONTIME:
                [model setScanStatus:0];
                break;
            case TA_LATE:
                [model setScanStatus:1];
                break;
            case TA_ABSENCE:
                [model setScanStatus:3];
                break;
            case TA_EVENT:
                [model setScanStatus:6];
                break;
            case TA_PERSONAL_LEAVE:
                [model setScanStatus:4];
                break;
            case TA_SICK_LEAVE:
                [model setScanStatus:5];
                break;
            default:
                [model setScanStatus:-1];
                break;
        }
        
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if(_studentStatusArray != nil) { // if select all
        
        BOOL hasChange = NO;
        
        for(int i=0; i<_studentStatusArray.count; i++) {
            TAStudentStatusModel *model = [_studentStatusArray objectAtIndex:i];
            
            if([model getAuthorized]) {
                
                hasChange = YES;
                
                switch (scanStatus) {
                    case TA_ONTIME:
                        [model setScanStatus:0];
                        break;
                    case TA_LATE:
                        [model setScanStatus:1];
                        break;
                    case TA_ABSENCE:
                        [model setScanStatus:3];
                        break;
                    case TA_PERSONAL_LEAVE:
                        [model setScanStatus:4];
                        break;
                    case TA_SICK_LEAVE:
                        [model setScanStatus:5];
                        break;
                    case TA_EVENT:
                        [model setScanStatus:6];
                        break;
                    default:
                        [model setScanStatus:-1];
                        break;
                }

            }
        }
        
        if(hasChange) {
            [self.tableView reloadData];
        }
    }
    
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBarCancelGesture = [UITapGestureRecognizer new];
    [searchBarCancelGesture addTarget:self action:@selector(backgroundTouched:)];
    [self.view addGestureRecognizer:searchBarCancelGesture];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if(searchBarCancelGesture != nil) {
        [self.view removeGestureRecognizer:searchBarCancelGesture];
        searchBarCancelGesture = nil;
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // called when text changes (including clear)
    
    searchActive = YES;
    
    if(searchText.length != 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"studentName contains[c] %@", searchText];
        searchStudentStatusArray = [_studentStatusArray filteredArrayUsingPredicate:predicate];
    }
    else {
        searchStudentStatusArray = _studentStatusArray;
    }
    
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // called when keyboard search button pressed
    
    [self.searchBar resignFirstResponder];
}

#pragma mark - Utils

- (void)backgroundTouched:(id)sender {
    [self.view endEditing:YES];
    [self.searchBar resignFirstResponder];
    
    if(searchActive && self.searchBar.text.length == 0) {
        searchActive = NO;
        [self.tableView reloadData];
    }
}

- (void)copyStatus {
    
    if(_studentStatusArray != nil && studentStatusHistoryForCopyArray != nil) {
        NSMutableDictionary<NSNumber *, NSNumber *> *studentIdStatusDict = [[NSMutableDictionary alloc] init];
        
        for(int i=0; i<studentStatusHistoryForCopyArray.count; i++) {
            TAStudentStatusModel *model = [studentStatusHistoryForCopyArray objectAtIndex:i];
            
            NSNumber *userId = [[NSNumber alloc] initWithLongLong:[model getUserId]];
            NSNumber *scanStatus = [[NSNumber alloc] initWithInt:[model getScanStatus]];
            
            [studentIdStatusDict setObject:scanStatus forKey:userId];
        }
        
        for(int i=0; i<_studentStatusArray.count; i++) {
            TAStudentStatusModel *model = [_studentStatusArray objectAtIndex:i];
            
            NSNumber *userId = [[NSNumber alloc] initWithLongLong:[model getUserId]];
            
            if([studentIdStatusDict objectForKey:userId] != nil) {
                int scanStatus = [[studentIdStatusDict objectForKey:userId] intValue];
                [model setScanStatus:scanStatus];
            }
        }
    }
}

- (BOOL)validateData {
    
    if(_studentStatusArray != nil) {
        
        for(TAStudentStatusModel *model in _studentStatusArray) {
            if([model getScanStatus] != -1) {
                return YES;
            }
        }
    }
    
    return NO;
}

- (NSArray<NSString *> *)getTeacherName {
    
    if(_studentStatusArray == nil) {
        return nil;
    }
    
    NSMutableArray<NSString *> *teachers = [[NSMutableArray alloc] init];
    
    for(TAStudentStatusModel *model in _studentStatusArray) {
        if([model getCheckedTeacherName] != nil && [model getCheckedTeacherName].length > 0) {
            
            if(![teachers containsObject:[model getCheckedTeacherName]]) {
                [teachers addObject:[model getCheckedTeacherName]];
            }
        }
    }
    
    return teachers;
}

- (BOOL)checkHavePermission {
    if(_studentStatusArray == nil) {
        return YES;
    }
    
    for(TAStudentStatusModel *model in _studentStatusArray) {
        if(![model getAuthorized]) {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - Actions

- (IBAction)moveBack:(id)sender {
    
    TASelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TakeClassAttendanceStoryboard"];
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.subjectArray = _subjectArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.selectedSubjectIndex = _selectedSubjectIndex;
    viewController.mode = _mode;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}

- (IBAction)actionSelectAll:(id)sender {
    
    selectedStudentIndex = -1;
    [self showSelectStatusDialog];
}

- (IBAction)actionCopyHistoryStatus:(id)sender {
    
    if(_studentStatusArray != nil && studentStatusHistoryForCopyArray != nil) {
        [self copyStatus];
        [self.tableView reloadData];
    }
}

- (IBAction)actionNext:(id)sender {
    
    if(_mode == TA_MODE_EDIT && ![self checkHavePermission]) {
        [self showTeacherDonotHavePermissionDialog];
    }
    else if([self validateData]) {
        TASummaryViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TASummaryStoryboard"];
        
        viewController.classLevelArray = _classLevelArray;
        viewController.classroomArray = _classroomArray;
        viewController.subjectArray = _subjectArray;
        viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
        viewController.selectedClassroomIndex = _selectedClassroomIndex;
        viewController.selectedSubjectIndex = _selectedSubjectIndex;
        viewController.mode = _mode;
        
        viewController.studentStatusArray = _studentStatusArray;
        viewController.classroomId = _classroomId;
        viewController.subjectId = _subjectId;
        viewController.subjectName = _subjectName;
        
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else {
        NSString *alertMessage = @"กรุณาเลือกสถานะที่ต้องการเช็คอย่างน้อย 1 คน";
        [self showAlertDialogWithMessage:alertMessage];
    }
    
}
@end
