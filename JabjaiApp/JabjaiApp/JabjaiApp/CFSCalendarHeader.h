//
//  CFSCalendarHeader.h
//  Pods
//
//  Created by Wenchao Ding on 29/1/15.
//
//

#import <UIKit/UIKit.h>
#import "CFSCalendarCollectionView.h"

@class CFSCalendar,CFSCalendarAppearance, CFSCalendarHeaderLayout;

@interface CFSCalendarHeader : UIView

@property (weak, nonatomic) CFSCalendarCollectionView *collectionView;
@property (weak, nonatomic) CFSCalendarHeaderLayout *collectionViewLayout;
@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) CFSCalendarAppearance *appearance;

@property (assign, nonatomic) CGFloat scrollOffset;
@property (assign, nonatomic) UICollectionViewScrollDirection scrollDirection;
@property (assign, nonatomic) BOOL scrollEnabled;
@property (assign, nonatomic) BOOL needsAdjustingViewFrame;
@property (assign, nonatomic) BOOL needsAdjustingMonthPosition;

- (void)reloadData;

@end


@interface CFSCalendarHeaderCell : UICollectionViewCell

@property (weak, nonatomic) UILabel *titleLabel;
@property (weak, nonatomic) CFSCalendarHeader *header;

- (void)invalidateHeaderFont;
- (void)invalidateHeaderTextColor;

@end

@interface CFSCalendarHeaderLayout : UICollectionViewFlowLayout

@end

@interface CFSCalendarHeaderTouchDeliver : UIView

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) CFSCalendarHeader *header;

@end
