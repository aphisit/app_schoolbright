//
//  CallUpdateStudentStatusPOSTAPI.h
//  JabjaiApp
//
//  Created by mac on 7/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CallUpdateStudentStatusPOSTAPI;

@protocol CallUpdateStudentStatusPOSTAPIDelegate <NSObject>

@optional
- (void)callUpdateStudentStatusPOSTAPI:(CallUpdateStudentStatusPOSTAPI *)classObj response:(NSString *)response success:(BOOL)success;

@end
@interface CallUpdateStudentStatusPOSTAPI : NSObject

@property (nonatomic, weak) id<CallUpdateStudentStatusPOSTAPIDelegate> delegate;

- (void)call:(long long)schoolId subjectId:(long long)subjectId teacherId:(long long)teacherId jsonString:(NSString *)jsonString;

@end
