//
//  TheacherAllModel.h
//  JabjaiApp
//
//  Created by toffee on 9/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TheacherAllModel : UIViewController

@property (nonatomic) long long theaCherId;
@property (nonatomic) NSString *theaCherName;

- (long long)getTheaCherId;
- (NSString *)getTheaCherName;

@end
