//
//  CalendarDialog.m
//  JabjaiApp
//
//  Created by mac on 12/12/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "CalendarDialog.h"
#import "Utils.h"

@interface CalendarDialog ()

@property (nonatomic, strong) NSString *dialogTitle;
@property (nonatomic, strong) NSDate *minimumDate;
@property (nonatomic, strong) NSDate *maximumDate;
@property (nonatomic) BOOL isShowing;

@property (nonatomic, strong) NSArray<NSDate *> *selectedDates;

@property (strong, nonatomic) UIColor *calendarHeaderColor;
@property (strong, nonatomic) UIColor *calendarWeekdayTitleColor;
@property (strong, nonatomic) UIColor *calendarBackgroundColor;
@property (strong, nonatomic) UIColor *customTextSelectedColor;
@property (strong, nonatomic) UIColor *customTextDeselectedColor;
@property (strong, nonatomic) UIColor *selectionOrangeColor;
@property (strong, nonatomic) UIColor *todayColor;

@end

@implementation CalendarDialog

- (id)init {
    self = [super init];
    
    if(self) {
        NSCalendar *gregorian = [Utils getGregorianCalendar];
        NSDate *minDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
        
        self.dialogTitle = @"Calendar";
        self.minimumDate = minDate;
        self.maximumDate = [NSDate date];
        
    }
    
    return self;
}

- (id)initWithTitle:(NSString *)title minimumDate:(NSDate *)minimumDate maximumDate:(NSDate *)maximumDate {
    self = [super init];
    
    if(self) {
        self.dialogTitle = title;
        self.minimumDate = minimumDate;
        self.maximumDate = maximumDate;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Decorate view
    self.dialogView.layer.cornerRadius = 10;
    self.dialogView.layer.masksToBounds = YES;
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10,10)].CGPath;
    self.headerView.layer.mask = maskLayer;
    self.headerView.layer.masksToBounds = YES;
    
    self.cancelButton.layer.cornerRadius = 20;
    self.cancelButton.layer.masksToBounds = YES;
    self.okButton.layer.cornerRadius = 20;
    self.okButton.layer.masksToBounds = YES;
    
    self.titleLabel.text = self.dialogTitle;
    
    // Set up calendar
    
    // Set up colors
    self.calendarHeaderColor = [UIColor colorWithRed:42/255.0 green:45/255.0 blue:44/255.0 alpha:1.0];
    self.calendarWeekdayTitleColor = [UIColor colorWithRed:99/255.0 green:96/255.0 blue:99/255.0 alpha:1.0];
    self.calendarBackgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
    self.customTextSelectedColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.customTextDeselectedColor = [UIColor colorWithRed:190/255.0 green:194/255.0 blue:197/255.0 alpha:1.0];
    self.selectionOrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1.0];
    self.todayColor = [UIColor colorWithRed:56/255.0 green:230/255.0 blue:195/255.0 alpha:1.0];
    
    self.calendar.backgroundColor = self.calendarBackgroundColor;
    self.calendar.dataSource = self;
    self.calendar.delegate = self;
    self.calendar.reverseCalendar = NO;
    self.calendar.pagingEnabled = NO;
    self.calendar.allowsSelection = YES;
    self.calendar.allowsMultipleSelection = NO;
    self.calendar.allowsRangeSelection = YES;
    self.calendar.firstWeekday = 1; // Give 1 means saturday
    
    // appearance setter
    self.calendar.appearance.weekdayTextColor = self.calendarWeekdayTitleColor;
    self.calendar.appearance.headerTitleColor = self.calendarHeaderColor;
    self.calendar.appearance.titlePlaceholderColor = self.customTextDeselectedColor;
    self.calendar.appearance.selectionColor = self.selectionOrangeColor;
    self.calendar.appearance.todayColor = self.todayColor;
    self.calendar.appearance.titleDefaultColor = self.selectionOrangeColor;
    self.calendar.appearance.adjustsFontSizeToFitContentSize = YES;
    
    // Initialize variables
    self.selectedDates = [[NSArray alloc] init];
    self.isShowing = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dismissDialog {
    [self removeDialogFromView];
}

- (IBAction)onPressCancel:(id)sender {
    [self dismissDialog];
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(calendarDialogPressCancel)]) {
        [self.delegate calendarDialogPressCancel];
    }
}

- (IBAction)onPressOK:(id)sender {
    
    self.selectedDates = self.calendar.selectedDates;
    
    [self removeDialogFromView];
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(calendarDialogPressOK:)]) {
        [self.delegate calendarDialogPressOK:self.selectedDates];
    }
}

#pragma mark - DialogFunctions

- (void)showDialogInView:(UIView *)targetView {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    self.isShowing = YES;

}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

- (void)setDateWithMinimumDate:(NSDate *)minDate maximumDate:(NSDate *)maxDate {
    
    if(minDate != nil) {
        self.minimumDate = minDate;
    }
    
    if(maxDate != nil) {
        self.maximumDate = maxDate;
    }
    
    [self.calendar reloadData];
}

#pragma mark - CFSCalendarDataSource
- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

@end
