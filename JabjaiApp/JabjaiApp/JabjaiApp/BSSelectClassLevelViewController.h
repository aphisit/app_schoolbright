//
//  BSSelectClassLevelViewController.h
//  JabjaiApp
//
//  Created by mac on 7/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "TableListDialog.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "BSSelectedStudent.h"
#import "BehaviorScoreModel.h"

@interface BSSelectClassLevelViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate>

//Global variables
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger mode;

// BSSelectStudentsViewController
@property (strong, nonatomic) NSArray<BSSelectedStudent *> *selectedStudentArray;

// BSSelectBehaviorScoreViewController
@property (strong, nonatomic) NSArray<BehaviorScoreModel *> *behaviorScoreArray;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classroomTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

- (IBAction)openDrawer:(id)sender;
- (IBAction)actionNext:(id)sender;

@end
