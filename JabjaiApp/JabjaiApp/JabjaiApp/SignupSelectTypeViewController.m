//
//  SignupSelectTypeViewController.m
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SignupSelectTypeViewController.h"
#import "StaffSignupPage1ViewController.h"
#import "SignupPage1ViewController.h"
#import "UniversitySignupPage1ViewController.h"

@interface SignupSelectTypeViewController () {
    SchoolCodeDialogViewController *schoolCodeDialog;
}

@end

@implementation SignupSelectTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    schoolCodeDialog = [[SchoolCodeDialogViewController alloc] init];
    schoolCodeDialog.delegate = self;
    
    NSLog(@"ID: %i, School : %@", (int)_signupDataModel.schoolID, _signupDataModel.school);
    
    //Add Gesture for each view
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoSignupPage1)];
    [self.studentView addGestureRecognizer:singleTap1];
    
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoStaffSignupPage1)];
    [self.teacherView addGestureRecognizer:singleTap2];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"signupPage1Segue"]) {
        SignupPage1ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
    else if([segue.identifier isEqualToString:@"universitySignupPage1Segue"]) {
        UniversitySignupPage1ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
    else if([segue.identifier isEqualToString:@"staffSignupPage1Segue"]) {
        StaffSignupPage1ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
}

#pragma CustomFunctions

-(void)gotoSignupPage1 {
    
    _signupDataModel.type = [[NSNumber alloc] initWithInt:0];
    
    if([_signupDataModel.academyType intValue] == 1) {
        
        // School Type
        [self performSegueWithIdentifier:@"signupPage1Segue" sender:self];

    }
    else {
        // University Type
        [self performSegueWithIdentifier:@"universitySignupPage1Segue" sender:self];
    }

}

-(void)gotoStaffSignupPage1 {
    
    NSString *title = @"ยืนยันตัวตนอาจารย์";
    NSString *hint = @"กรุณากรอกรหัสผ่านสำหรับอาจารย์";
    
    [schoolCodeDialog showDialogInView:self.view title:title hint:hint];
    
}

- (IBAction)moveBack:(id)sender {
    [self performSegueWithIdentifier:@"loginSegue" sender:self];
}

#pragma mark - SchoolCodeDialogViewControllerDelegate
-(void)onCancelButtonPress {
    
}

-(void)onOkButtonPress {
    
    NSString *password = schoolCodeDialog.passwordTextField.text;
    NSMutableString *pwd = [[NSMutableString alloc] initWithString:password];
    CFStringTrimWhitespace((__bridge CFMutableStringRef) pwd);

    if([pwd caseInsensitiveCompare:@"teacher"] == NSOrderedSame) {
        _signupDataModel.type = [[NSNumber alloc] initWithInt:1];
        [self performSegueWithIdentifier:@"staffSignupPage1Segue" sender:self];
    }
    else {
        NSString *hint = @"รหัสผ่านไม่ถูกต้อง โปรดลองใหม่";
        schoolCodeDialog.hintLabel.text = hint;
        schoolCodeDialog.hintLabel.textColor = [UIColor redColor];
    }
}
@end
