//
//  CFSCalendarAnimator.h
//  CFSCalendar
//
//  Created by dingwenchao on 3/13/16.
//  Copyright © 2016 wenchaoios. All rights reserved.
//

#import "CFSCalendar.h"
#import "CFSCalendarCollectionView.h"
#import "CFSCalendarFlowLayout.h"
#import "CFSCalendarScopeHandle.h"

typedef NS_ENUM(NSUInteger, CFSCalendarTransition) {
    CFSCalendarTransitionNone,
    CFSCalendarTransitionMonthToWeek,
    CFSCalendarTransitionWeekToMonth
};
typedef NS_ENUM(NSUInteger, CFSCalendarTransitionState) {
    CFSCalendarTransitionStateIdle,
    CFSCalendarTransitionStateInProgress
};

@interface CFSCalendarAnimator : NSObject <UIGestureRecognizerDelegate>

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) CFSCalendarCollectionView *collectionView;
@property (weak, nonatomic) CFSCalendarFlowLayout *collectionViewLayout;

@property (assign, nonatomic) CFSCalendarTransition transition;
@property (assign, nonatomic) CFSCalendarTransitionState state;

@property (assign, nonatomic) CGSize cachedMonthSize;

- (void)performScopeTransitionFromScope:(CFSCalendarScope)fromScope toScope:(CFSCalendarScope)toScope animated:(BOOL)animated;
- (void)performBoundingRectTransitionFromMonth:(NSDate *)fromMonth toMonth:(NSDate *)toMonth duration:(CGFloat)duration;

- (void)handlePan:(id)sender;

@end


@interface CFSCalendarTransitionAttributes : NSObject

@property (assign, nonatomic) CGRect sourceBounds;
@property (assign, nonatomic) CGRect targetBounds;
@property (strong, nonatomic) NSDate *sourcePage;
@property (strong, nonatomic) NSDate *targetPage;
@property (assign, nonatomic) NSInteger focusedRowNumber;
@property (assign, nonatomic) NSDate *focusedDate;
@property (strong, nonatomic) NSDate *firstDayOfMonth;

@end

