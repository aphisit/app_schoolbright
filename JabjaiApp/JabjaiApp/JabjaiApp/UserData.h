//
//  UserData.h
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    STUDENT, TEACHER, USERTYPENULL
} USERTYPE;

typedef enum {
    SCHOOL, UNIVERSITY, ACADEMYTYPENULL
} ACADEMYTYPE;

typedef enum {
    MALE, FEMALE
} GENDERTYPE;

static NSString *MASTER_USERID = @"MASTER_USERID";
static NSString *USERID = @"USERID";
static NSString *USERLOGIN = @"USERLOGIN";
static NSString *USER_TYPE = @"USERTYPE";
static NSString *ACADEMY_TYPE = @"ACADEMYTYPE";
static NSString *SCHOOLID = @"SCHOOLID";
static NSString *USER_IMAGE = @"USER_IMAGE";
static NSString *FIRSTNAME = @"FIRSTNAME";
static NSString *LASTNAME = @"LASTNAME";
static NSString *GENDER = @"GENDER";
static NSString *APPVERSION = @"APPVERSION";

@interface UserData : NSObject

+ (void)setUserLogin:(BOOL)login;
+ (BOOL)isUserLogin;
+ (void)saveMasterUserID:(NSInteger)masterId;
+ (NSInteger)getMasterUserID;
+ (void)saveUserID:(NSInteger)userid;
+ (NSInteger)getUserID;
+ (void)saveSchoolId:(long long)schoolId;
+ (long long)getSchoolId;
+ (void)setUserImage:(NSString *)imageUrl;
+ (NSString *)getUserImage;
+ (void)setFirstName:(NSString *)firstName;
+ (NSString *)getFirstName;
+ (void)setLastName:(NSString *)lastName;
+ (NSString *)getLastName;
+ (void)setAppVersion:(NSString *)appVersion;
+ (NSString *)getAppVersion;

+ (void)setUserType:(USERTYPE)userType;
+ (USERTYPE)getUserType;
+ (void)setAcademyType:(ACADEMYTYPE)academyType;
+ (ACADEMYTYPE)getAcademyType;
+ (void)setGender:(GENDERTYPE)genderType;
+ (GENDERTYPE)getGender;

@end
