//
//  MultipleUserModel.h
//  JabjaiApp
//
//  Created by mac on 8/10/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MultipleUserModel : NSObject

@property (nonatomic) long long masterId;
@property (nonatomic) long long slaveId;
@property (nonatomic) long long schoolId;
@property (nonatomic) NSInteger userType; // 0: student, 1: teacher
@property (nonatomic) NSInteger academyType; // 1: school, 2: university
@property (nonatomic) NSString *imageUrl;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;
@property (nonatomic) NSInteger gender; // 0:male, 1:female

- (void)setMasterId:(long long)masterId;
- (void)setSlaveId:(long long)slaveId;
- (void)setSchoolId:(long long)schoolId;
- (void)setUserType:(NSInteger)userType;
- (void)setAcademyType:(NSInteger)academyType;
- (void)setImageUrl:(NSString *)imageUrl;
- (void)setFirstName:(NSString *)firstName;
- (void)setLastName:(NSString *)lastName;
- (void)setGender:(NSInteger)gender;

- (long long)getMasterId;
- (long long)getSlaveId;
- (long long)getSchoolId;
- (NSInteger)getUserType;
- (NSInteger)getAcademyType;
- (NSString *)getImageUrl;
- (NSString *)getFirstName;
- (NSString *)getLastName;
- (NSInteger)getGender;

@end
