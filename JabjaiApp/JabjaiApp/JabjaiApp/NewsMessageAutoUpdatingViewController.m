//
//  NewsMessageAutoUpdatingViewController.m
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NewsMessageAutoUpdatingViewController.h"
#import "NewsMessageTableViewCell.h"
#import "ProgressTableViewCell.h"
#import "MessageInboxDataModel.h"
#import "SWRevealViewController.h"
#import "MessageInBoxScrollableTextDialog.h"
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"

static NSString *dataCellIdentifier = @"MessageCell";

@interface NewsMessageAutoUpdatingViewController () {
    
    NSMutableDictionary<NSNumber *, NSArray<MessageInboxDataModel *> *> *messageSections;
    NSMutableArray<MessageInboxDataModel *> *messages;
    
    UIColor *unreadBGColor;
    UIColor *readBGColor;
    NSInteger connectCounter;
}

@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property (nonatomic, strong) MessageInBoxScrollableTextDialog *messageInboxScrollableTextDialog;

@end

@implementation NewsMessageAutoUpdatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    messageSections = [[NSMutableDictionary alloc] init];
    messages = [[NSMutableArray alloc] init];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NewsMessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:dataCellIdentifier];
    
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    unreadBGColor = [UIColor whiteColor];
    readBGColor = [UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1.0];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    [self getNewsMessageDataWithPage:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return messageSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger page = section + 1;
    return [[messageSections objectForKey:@(page)] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
    
    NSInteger page = indexPath.section + 1;
    MessageInboxDataModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
    cell.titleLabel.text = model.title;
    cell.messageLabel.text = model.message;
    
    if(model.status == 0) {
        // Unread
        cell.backgroundColor = unreadBGColor;
    }
    else {
        cell.backgroundColor = readBGColor;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger page = indexPath.section + 1;
    MessageInboxDataModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    [self showMessage:model];
    
    if(model.status == 0) { // message still unread
        [self updateReadStatus:model];
        model.status = 1; // set message already read
    }

    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

#pragma mark - Get API Data
- (void)getNewsMessageDataWithPage:(NSUInteger)page {
    
    [self showIndicator];
    
    long long userID = [UserData getUserID];
    NSString *URLString = [APIURL getNewsMessageWithPage:page userID:userID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getNewsMessageDataWithPage:page];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getNewsMessageDataWithPage:page];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getNewsMessageDataWithPage:page];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    int status = [[dataDict objectForKey:@"nStatus"] intValue];
                    int type = [[dataDict objectForKey:@"nType"] intValue];
                    NSMutableString *messageDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dSend"]];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sTitle"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    NSRange dotRange = [messageDateStr rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:messageDateStr];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:messageDateStr];
                    }
                    
                    MessageInboxDataModel *model = [[MessageInboxDataModel alloc] init];
                    model.messageID = messageID;
                    model.userID = (int)[UserData getUserID];
                    model.messageType = type;
                    model.status =status;
                    model.title = title;
                    model.message = message;
                    model.date = messageDate;
                    
                    [newDataArr addObject:model];
                    
                }
                
                [messages removeAllObjects];
                messages = [[NSMutableArray alloc] initWithArray:newDataArr];
                [messageSections setObject:newDataArr forKey:@(page)];
            
                [self.tableView reloadData];
            }
            
        }
    }];
}

-(void)updateReadStatus:(MessageInboxDataModel *)model {
    
        long long userID = [UserData getUserID];
        NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:model.messageID userID:userID];
        NSURL *URL = [NSURL URLWithString:URLString];
        
        [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
                
                if(error != nil) {
                    NSLog(@"An error occured : %@" , [error localizedDescription]);
                }
                else if(data == nil) {
                    NSLog(@"Data is null");
                }
                else {
                    NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                    NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
                }
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateReadStatus:model];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if(error != nil) {
                    NSLog(@"An error occured : %@", [error localizedDescription]);
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self updateReadStatus:model];
                    }
                    else {
                        connectCounter = 0;
                    }
                    
                }
                else if(![returnedData isKindOfClass:[NSArray class]]) {
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self updateReadStatus:model];
                    }
                    else {
                        connectCounter = 0;
                    }
                    
                }
                else {
                    
                    NSArray *returnedArray = returnedData;
                    connectCounter = 0;
                    
                    if(returnedArray.count == 0) {
                        NSLog(@"%s", "Update read message status failed (return array size 0)");
                    }
                }
                
            }
            
        }];
    
}

- (void)fetchMoreData {
    
    if(messages.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *lastRowIndexPath = [visiblePaths lastObject];
        
        // Check whether or not the very last row is visible.
        NSInteger numberOfSections = [self.tableView numberOfSections];
        NSInteger lastRowSection = [lastRowIndexPath section];
        NSInteger lastRowRow = [lastRowIndexPath row];
        NSInteger numberOfRowsInSection = [self.tableView numberOfRowsInSection:lastRowSection];
        
        if(lastRowSection == numberOfSections -1 && lastRowRow == numberOfRowsInSection - 1) {
            
            if(messages.count % 20 == 0) {
                NSInteger nextPage = lastRowSection + 2;
                [self getNewsMessageDataWithPage:nextPage];
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self fetchMoreData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self fetchMoreData];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Dialog
- (void)showMessage:(MessageInboxDataModel *)model {
    
    if(self.messageInboxScrollableTextDialog == nil) {
        self.messageInboxScrollableTextDialog = [[MessageInBoxScrollableTextDialog alloc] init];
    }
    
    if(self.messageInboxScrollableTextDialog != nil && [self.messageInboxScrollableTextDialog isDialogShowing]) {
        [self.messageInboxScrollableTextDialog dismissDialog];
    }
    
    [self.messageInboxScrollableTextDialog showDialogInView:self.view title:model.title message:model.message messageDate:model.date];
}

- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

@end
