//
//  ExecutiveReportTeacherListViewController.m
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "ExecutiveReportTeacherListViewController.h"
#import "ExecutiveReportClassLevelViewController.h"
#import "EXReportNameStatusTableViewCell.h"
#import "UserData.h"
#import "Utils.h"

@interface ExecutiveReportTeacherListViewController () {
    NSArray<EXReportNameStatusModel *> *nameStatusModelArray;
}

@property (strong, nonatomic) CallGetEXReportAttendSchoolTeacherListAPI *callGetEXReportAttendSchoolTeacherListAPI;
@property (strong, nonatomic) CallGetEXReportAttendSchoolEmployeeListAPI *callGetEXReportAttendSchoolEmployeeListAPI;

@end

static NSString *cellIdentifier = @"Cell";

@implementation ExecutiveReportTeacherListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // register table nib
     [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EXReportNameStatusTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    if(self.mode == 0) {
        self.mode = EXREPORT_TEACHER_LIST;
    }
    
    if(self.mode == EXREPORT_TEACHER_LIST) {
        self.typeLabel.text = @"อาจารย์";
        [self getTeacherList];
    }
    else {
        self.typeLabel.text = @"พนักงาน";
        [self getEmployeeList];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(nameStatusModelArray != nil) {
        return nameStatusModelArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EXReportNameStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    EXReportNameStatusModel *model = [nameStatusModelArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = [NSString stringWithFormat:@"%li. %@", (indexPath.row + 1), [model getName]];
    [cell updateStatus:[model getStatus]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

#pragma mark - API Caller

- (void)getTeacherList {
    
    self.callGetEXReportAttendSchoolTeacherListAPI = nil;
    self.callGetEXReportAttendSchoolTeacherListAPI = [[CallGetEXReportAttendSchoolTeacherListAPI alloc] init];
    self.callGetEXReportAttendSchoolTeacherListAPI.delegate = self;
    [self.callGetEXReportAttendSchoolTeacherListAPI call:[UserData getSchoolId] date:self.reportDate];
}

- (void)getEmployeeList {
    
    self.callGetEXReportAttendSchoolEmployeeListAPI = nil;
    self.callGetEXReportAttendSchoolEmployeeListAPI = [[CallGetEXReportAttendSchoolEmployeeListAPI alloc] init];
    self.callGetEXReportAttendSchoolEmployeeListAPI.delegate = self;
    [self.callGetEXReportAttendSchoolEmployeeListAPI call:[UserData getSchoolId] date:self.reportDate];
}

#pragma mark - CallGetEXReportAttendSchoolTeacherListAPIDelegate
- (void)callGetEXReportAttendSchoolTeacherListAPI:(CallGetEXReportAttendSchoolTeacherListAPI *)classObj data:(NSArray<EXReportNameStatusModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        nameStatusModelArray = data;
        [self.tableView reloadData];
    }
}

#pragma mark - CallGetEXReportAttendSchoolEmployeeListAPIDelegate
- (void)callGetEXReportAttendSchoolEmployeeListAPI:(CallGetEXReportAttendSchoolEmployeeListAPI *)classObj data:(NSArray<EXReportNameStatusModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        nameStatusModelArray = data;
        [self.tableView reloadData];
    }
}

#pragma mark - Action functions

- (IBAction)moveBack:(id)sender {
    
    ExecutiveReportClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecutiveReportClassLevelStoryboard"];
    viewController.reportDate = _reportDate;
    viewController.reportClassStatusArray = _reportClassStatusArray;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}
@end
