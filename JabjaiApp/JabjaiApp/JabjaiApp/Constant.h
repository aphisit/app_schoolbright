//
//  Constant.h
//  JabjaiApp
//
//  Created by mac on 3/24/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    JPEG, PNG
} MIME_TYPE;

typedef enum {
    TA_ONTIME, TA_LATE, TA_ABSENCE, TA_EVENT, TA_PERSONAL_LEAVE, TA_SICK_LEAVE, TA_NOTSCAN
} TA_SCAN_STATUS;

static const NSInteger TRY_CONNECT_MAX = 5;

static const NSInteger BS_MODE_ADD = 1;
static const NSInteger BS_MODE_REDUCE = 2;

static const int RADIO_TYPE_GREEN = 100;
static const int RADIO_TYPE_RED = 101;

static const BOOL logoutIfNewVersion = YES;

@interface Constant : NSObject

@end
