//
//  BSStatusModel.m
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSStatusModel.h"

@implementation BSStatusModel

@synthesize statusId = _statusId;
@synthesize statusName = _statusName;

- (void)setStatusId:(NSInteger)statusId {
    _statusId = statusId;
}

- (void)setStatusName:(NSString *)statusName {
    _statusName = statusName;
}

- (NSInteger)getStatusId {
    return _statusId;
}

- (NSString *)getStatusName {
    return _statusName;
}

@end
