//
//  SignupPage1ViewController.m
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SignupPage1ViewController.h"
#import "SignupSelectTypeViewController.h"
#import "SignupPage2ViewController.h"
#import "APIURL.h"
#import "SchoolDegreeModel.h"
#import "SchoolGradModel.h"
#import "SchoolClassroomModel.h"
#import "AlertDialog.h"
#import "Constant.h"
#import "Utils.h"

@interface SignupPage1ViewController () {
    NSMutableArray *schoolDegreeArr;
    NSMutableArray *schoolGradArr;
    NSMutableArray *schoolClassroomArr;
    
    NSInteger degreeID;
    NSInteger gradID;
    NSInteger classroomID;
    
    // The dialog
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    
    NSInteger connectCounter;
}

@end

static NSString *degreeDialogRequestCode = @"degreeDialogRequestCode";
static NSString *gradDialogRequestCode = @"gradDialogRequestCode";
static NSString *classroomDialogRequestCode = @"classroomDialogRequestCode";

@implementation SignupPage1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.schoolTextField.delegate = self;
    self.degreeTextField.delegate = self;
    self.gradTextField.delegate = self;
    self.classroomTextField.delegate = self;
    self.studentIDTextField.delegate = self;
    
    self.schoolTextField.text = _signupDataModel.school;
    
    alertDialog = [[AlertDialog alloc] init];
    
    connectCounter = 0;
    
    // Restore data in text field when move back from page2
    if(_signupDataModel.degree != nil) {
        self.degreeTextField.text = _signupDataModel.degree;
        degreeID = [_signupDataModel.degreeID integerValue];
    }
    
    if(_signupDataModel.grade != nil) {
        self.gradTextField.text = _signupDataModel.grade;
        gradID = [_signupDataModel.gradID integerValue];
    }
    
    if(_signupDataModel.classroom != nil) {
        self.classroomTextField.text = _signupDataModel.classroom;
        classroomID = [_signupDataModel.classroomID integerValue];
    }
    
    if(_signupDataModel.studentID != nil) {
        self.studentIDTextField.text = _signupDataModel.studentID;
    }
    
    // Docorate the views
    self.nextBtn.layer.cornerRadius = 20;
    self.nextBtn.layer.masksToBounds = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if([segue.identifier isEqualToString:@"selectTypeSegue"]) {
        SignupSelectTypeViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
    else if([segue.identifier isEqualToString:@"signupPage2Segue"]) {
        SignupPage2ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
}

- (IBAction)moveBack:(id)sender {
    [self performSegueWithIdentifier:@"selectTypeSegue" sender:self];
}

- (IBAction)dismissKeyboard:(id)sender {
    [self resignFirstResponder];
}

- (IBAction)nextStep:(id)sender {
    
    BOOL isDataValidate = [self validateData];
    
    if(isDataValidate) {
        NSString *degree = self.degreeTextField.text;
        NSString *grad = self.gradTextField.text;
        NSString *classroom = self.classroomTextField.text;
        
        NSMutableString *studentID = [[NSMutableString alloc] initWithString:self.studentIDTextField.text];
        CFStringTrimWhitespace((__bridge CFMutableStringRef) studentID);
        
        _signupDataModel.degree = degree;
        _signupDataModel.degreeID = [[NSNumber alloc] initWithInteger:degreeID];
        _signupDataModel.grade = grad;
        _signupDataModel.gradID = [[NSNumber alloc] initWithInteger:gradID];
        _signupDataModel.classroom = classroom;
        _signupDataModel.classroomID = [[NSNumber alloc] initWithInteger:classroomID];
        _signupDataModel.studentID = studentID;
        
        [self performSegueWithIdentifier:@"signupPage2Segue" sender:self];
        
    }
}

#pragma mark - Validation Functions
-(BOOL)validateData {
    
    NSString *dialogTitle = @"แจ้งเตือน";
    
    if([self.schoolTextField.text length] == 0 || [self.degreeTextField.text length] == 0 || [self.gradTextField.text length] == 0 || [self.classroomTextField.text length] == 0 || [self.studentIDTextField.text length] == 0) {
        
        NSString *alertMessage = @"กรุณากรอกข้อมูลให้ครบถ้วน";
        [alertDialog showDialogInView:self.view title:dialogTitle message:alertMessage];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    NSString *dialogTitle = @"แจ้งเตือน";
    
    if(textField.tag == 1) {
        return NO;
    }
    else if(textField.tag == 2) {
        
        if(tableListDialog == nil) {
            [self getSchoolDegree];
        }
        else if(tableListDialog != nil && ![tableListDialog isDialogShowing]) {
            [self getSchoolDegree];
        }
        
        return NO;
    }
    else if(textField.tag == 3) {
        
        if([self.degreeTextField.text length] == 0) {
            NSLog(@"%@", @"Please Select Degree");
            
            NSString *alertMessage = @"กรุณาเลือกช่วงชั้น";
            [alertDialog showDialogInView:self.view title:dialogTitle message:alertMessage];
            
        }
        else {
            
            
            if(tableListDialog == nil) {
                [self getSchoolGrad];
            }
            else if(tableListDialog != nil && ![tableListDialog isDialogShowing]) {
                [self getSchoolGrad];
            }
            
        }
        
        return NO;
    }
    else if(textField.tag == 4) {
        
        if([self.degreeTextField.text length] == 0) {
            NSLog(@"%@", @"Please Select Degree");
            
            NSString *alertMessage = @"กรุณาเลือกช่วงชั้น";
            [alertDialog showDialogInView:self.view title:dialogTitle message:alertMessage];
        }
        else if([self.gradTextField.text length] == 0) {
            NSLog(@"%@", @"Please Select Grad");
            
            NSString *alertMessage = @"กรุณาเลือกระดับชั้น";
            [alertDialog showDialogInView:self.view title:dialogTitle message:alertMessage];
        }
        else {
            
            if(tableListDialog == nil) {
                [self getSchoolClassroom];
            }
            else if(tableListDialog != nil && ![tableListDialog isDialogShowing]) {
                [self getSchoolClassroom];
            }
            
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - GetAPIData

- (void)getSchoolDegree {
    NSString *URLString = [APIURL getSchoolDegree:[_signupDataModel.schoolID integerValue]];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSchoolDegree];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolDegree];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolDegree];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                schoolDegreeArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *degreeDict = [returnedArray objectAtIndex:i];
                    
                    NSNumber *degID = [[NSNumber alloc] initWithInt:(int)[[degreeDict objectForKey:@"nTLevel"] integerValue]];
                    NSMutableString *degreeName = [[NSMutableString alloc] initWithString:[degreeDict objectForKey:@"LevelName"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) degreeName);
                    
                    SchoolDegreeModel *model = [[SchoolDegreeModel alloc] init];
                    model.degreeID = degID;
                    model.degreeName = degreeName;
                    
                    [schoolDegreeArr addObject:model];
                    [dataArr addObject:degreeName];
                }
                
                // Create NSArray data to show in dialog
                if(dataArr.count > 0) {
                    [self showTableListDialogWithRequestCode:degreeDialogRequestCode data:dataArr];
                }
                
            }

        }

    }];

}

- (void)getSchoolGrad {
    NSString *URLString = [APIURL getSchoolGrad:[_signupDataModel.schoolID integerValue] degreeID:degreeID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSchoolGrad];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolGrad];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolGrad];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                schoolGradArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSNumber *nGradID = [[NSNumber alloc] initWithInt:(int)[[dataDict objectForKey:@"ID"] integerValue]];
                    NSMutableString *gradName = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"Value"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) gradName);
                    
                    SchoolGradModel *model = [[SchoolGradModel alloc] init];
                    model.gradID = nGradID;
                    model.gradName = gradName;
                    
                    [schoolGradArr addObject:model];
                    [dataArr addObject:gradName];
                }
                
                // Create NSArray data to show in dialog
                if(dataArr.count > 0) {
                    [self showTableListDialogWithRequestCode:gradDialogRequestCode data:dataArr];
                }

            }
            
        }
        
    }];

}

- (void)getSchoolClassroom {
    NSString *URLString = [APIURL getSchoolClassroom:[_signupDataModel.schoolID integerValue] gradID:gradID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSchoolClassroom];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolClassroom];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolClassroom];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                schoolClassroomArr = [[NSMutableArray alloc] init];
                
                // Create NSArray data to show in dialog
                NSMutableArray *dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSNumber *nClassroomID = [[NSNumber alloc] initWithInt:(int)[[dataDict objectForKey:@"ID"] integerValue]];
                    NSMutableString *classroomName = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"Value"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) classroomName);
                    
                    SchoolClassroomModel *model = [[SchoolClassroomModel alloc] init];
                    model.classroomID = nClassroomID;
                    model.classroomName = classroomName;
                    
                    [schoolClassroomArr addObject:model];
                    [dataArr addObject:classroomName];
                }
                
                // Create NSArray data to show in dialog
                if(dataArr.count > 0) {
                    [self showTableListDialogWithRequestCode:classroomDialogRequestCode data:dataArr];
                }
                
            }
            
        }
        
    }];
}

#pragma mark - TableListDialogDelegate
-(void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:degreeDialogRequestCode]) {
        
        self.degreeTextField.text = [[schoolDegreeArr objectAtIndex:index] getDegreeName];
        degreeID = [[[schoolDegreeArr objectAtIndex:index] getDegreeID] integerValue];
        
        // Clear dependency text field values
        self.gradTextField.text = @"";
        self.classroomTextField.text = @"";
        
    }
    else if([requestCode isEqualToString:gradDialogRequestCode]) {
        
        self.gradTextField.text = [[schoolGradArr objectAtIndex:index] getGradName];
        gradID = [[[schoolGradArr objectAtIndex:index] getGradID] integerValue];
        
        //NSLog(@"Grad : %@", [[schoolGradArr objectAtIndex:index] getGradName]);
        // Clear dependency text field values
        self.classroomTextField.text = @"";
        
    }
    else if([requestCode isEqualToString:classroomDialogRequestCode]) {
        self.classroomTextField.text = [[schoolClassroomArr objectAtIndex:index] getClassroomName];
        classroomID = [[[schoolClassroomArr objectAtIndex:index] getClassroomID] integerValue];
    }
}

#pragma mark - Show dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

@end
