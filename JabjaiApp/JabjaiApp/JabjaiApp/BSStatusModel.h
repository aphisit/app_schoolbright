//
//  BSStatusModel.h
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSStatusModel : NSObject

@property (nonatomic) NSInteger statusId; // 0:add, 1:reduce, 2:all
@property (nonatomic) NSString *statusName;

- (void)setStatusId:(NSInteger)statusId;
- (void)setStatusName:(NSString *)statusName;

- (NSInteger)getStatusId;
- (NSString *)getStatusName;
@end
