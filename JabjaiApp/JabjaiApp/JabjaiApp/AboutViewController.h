//
//  AboutViewController.h
//  JabjaiApp
//
//  Created by mac on 12/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *borderView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

- (IBAction)openDrawer:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
