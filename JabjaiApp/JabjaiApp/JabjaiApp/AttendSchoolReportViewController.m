//
//  AttendClassReportViewController.m
//  JabjaiApp
//
//  Created by mac on 10/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "AttendSchoolReportViewController.h"
#import "AttendSchoolCalendarViewController.h"
#import "AttendSchoolAgendaTableViewController.h"
#import "APIURL.h"
#import "Utils.h"
#import "DateUtility.h"
#import "YearTermModel.h"
#import "Attend2SchoolFilter.h"
#import "Constant.h"

@interface AttendSchoolReportViewController () {
    UIColor *pagerOrangeColor;
    UIColor *pagerGreenColor;
    UIColor *indicatorOrangeColor;
    UIColor *indicatorGreenColor;
    
    // Data Containers
    NSMutableDictionary *yearTermDict; //Key is yearNumber and data are array of YearTermModel
    
    NSInteger connectCounter;
}

@property (nonatomic) CAPSPageMenu *pageMenu;

//
@property (strong, nonatomic) NSCalendar *gregorian;

@property (nonatomic, strong) CalendarDialog *calendarDialog;
@property (nonatomic, strong) Attend2SchoolReportFilterDialog *filterDialog;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@property (nonatomic, strong) AttendSchoolCalendarViewController *calendarController;
@property (nonatomic, strong) AttendSchoolAgendaTableViewController *agendaController;

@end

@implementation AttendSchoolReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    pagerOrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1];
    pagerGreenColor = [UIColor colorWithRed:73/255.0 green:160/255.0 blue:152/255.0 alpha:1];
    indicatorOrangeColor = [UIColor colorWithRed:254/255.0 green:180/255.0 blue:57/255.0 alpha:1];
    indicatorGreenColor = [UIColor colorWithRed:31/255.0 green:124/255.0 blue:114/255.0 alpha:1];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.gregorian = [Utils getGregorianCalendar];
    
    NSDate *minDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:[NSDate date] options:0];
    NSDate *maxDate = [NSDate date];
    self.calendarDialog = [[CalendarDialog alloc] initWithTitle:@"ปฏิทิน" minimumDate:minDate maximumDate:maxDate];
    
    connectCounter = 0;
    
    [self callAPIGetYearAndTerm];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [self setupPageMenu];
}

- (void)setupPageMenu {
    self.calendarController = [[AttendSchoolCalendarViewController alloc] init];
    self.calendarController.title = @"ปฏิทิน";
    
    self.agendaController = [[AttendSchoolAgendaTableViewController alloc] init];
    self.agendaController.title = @"ประวัติ";

    
    NSArray *controllerArray = @[self.calendarController, self.agendaController];
    
    // Color background for each controller tab
    NSArray *colorBackground = @[pagerOrangeColor, pagerGreenColor];
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionMenuItemSeparatorWidth: @(4.3),
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                                 CAPSPageMenuOptionSelectionIndicatorColor: indicatorOrangeColor,
                                 CAPSPageMenuOptionMenuMargin: @(20),
                                 CAPSPageMenuOptionMenuHeight: @(40),
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor: [UIColor whiteColor],                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"ThaiSansNeue-SemiBold" size:22.0],
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                                 CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                                 CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                 CAPSPageMenuOptionColorBackgroundArray: colorBackground
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    //Optional delegate
    _pageMenu.delegate = self;
    
    [self.contentView addSubview:_pageMenu.view];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)showFilterDialog:(id)sender {
    
    //[self.calendarDialog showDialogInView:self.view];
    if(self.filterDialog != nil) {
        [self.filterDialog showDialogInView:self.view title:@"Filter"];
    }
    
}

#pragma CAPSPageMenuDelegate
- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    NSLog(@"WillMoveToPage");
    
    if(index == 0) {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorOrangeColor;
    }
    else {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorGreenColor;
        
    }
    
    
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
    NSLog(@"DidMoveToPage");
    
    if(index == 0) {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorOrangeColor;
    }
    else {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorGreenColor;
        
    }
    
}

#pragma mark - GetAPIData

-(void)callAPIGetYearAndTerm {
    [self getYearAndTermData];
}

-(void)getYearAndTermData {
    NSString *URLString = [APIURL getYearAndTermURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self callAPIGetYearAndTerm];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetYearAndTerm];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetYearAndTerm];
                }
                else {
                    connectCounter = 0;
                }
            
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                yearTermDict = [[NSMutableDictionary alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSInteger yearID = [[dataDict objectForKey:@"Yearid"] integerValue];
                    NSInteger yearNumber = [[dataDict objectForKey:@"YearNumber"] integerValue];
                    NSMutableString *termID = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Termid"]];
                    NSMutableString *termName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TermName"]];
                    NSMutableString *startDateStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dStart"]];
                    NSMutableString *endDateStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dEnd"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) termID);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) termName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endDateStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *lStartDate = [formatter dateFromString:startDateStr];
                    NSDate *lEndDate = [formatter dateFromString:endDateStr];
                    
                    YearTermModel *model = [[YearTermModel alloc] init];
                    [model setYearID:[[NSNumber alloc] initWithInteger:yearID]];
                    [model setYearNumber:[[NSNumber alloc] initWithInteger:yearNumber]];
                    [model setTermID:termID];
                    [model setTermName:termName];
                    [model setTermBegin:lStartDate];
                    [model setTermEnd:lEndDate];
                    
                    //                    NSLog(@"%@", [NSString stringWithFormat:@"Term Start : %@", [Utils dateInStringFormat:lStartDate format:@"yyyy-MM-dd"]]);
                    //                    NSLog(@"%@", [NSString stringWithFormat:@"Term End : %@", [Utils dateInStringFormat:lEndDate format:@"yyyy-MM-dd"]]);
                    
                    if([yearTermDict objectForKey:model.yearNumber] != nil) {
                        NSMutableArray *termArr = [yearTermDict objectForKey:model.yearNumber];
                        [termArr addObject:model];
                        [yearTermDict setObject:termArr forKey:model.yearNumber];
                    }
                    else {
                        NSMutableArray *termArr = [[NSMutableArray alloc] init];
                        [termArr addObject:model];
                        [yearTermDict setObject:termArr forKey:model.yearNumber];
                    }
                }
                
                // Initialize filter dialog
                if(self.filterDialog != nil && [self.filterDialog isDialogShowing]) {
                    [self.filterDialog dismissDialog];
                    self.filterDialog = nil;
                }
                
                self.filterDialog = [[Attend2SchoolReportFilterDialog alloc] initWithYearTermDictionary:yearTermDict];
                self.filterDialog.delegate = self;
    
            }
        }
        
    }];
    
}

#pragma mark - Attend2SchoolReportFilterDialogDelegate

-(void)applyFilter:(NSNumber *)schoolYear semester:(NSString *)semester status:(NSString *)status startDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    
    Attend2SchoolFilter *filter = [[Attend2SchoolFilter alloc] init];
    filter.schoolYear = schoolYear;
    filter.semester = semester;
    
    if([status isEqualToString:@"ตรงเวลา"]) {
        filter.status = INTIME;
    }
    else if([status isEqualToString:@"สาย"]) {
        filter.status = LATE;
    }
    else if([status isEqualToString:@"ขาด"]) {
        filter.status = ABSENCE;
    }
    else if([status isEqualToString:@"ลา"]) {
        filter.status = ONLEAVE;
    }
    else if([status isEqualToString:@"วันหยุด"]) {
        filter.status = HOLIDAY;
    }
    else {
        filter.status = ALL;
    }
    
    filter.startDate = startDate;
    filter.endDate = endDate;
    
    if(self.calendarController != nil) {
        [self.calendarController applyFilter:filter];
    }
    
    if(self.agendaController != nil) {
        [self.agendaController applyFilter:filter];
    }
    
    NSLog(@"%@", @"Apply Filter");
}


@end
