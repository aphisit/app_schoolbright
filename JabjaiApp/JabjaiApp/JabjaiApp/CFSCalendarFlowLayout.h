//
//  CFSCalendarAnimationLayout.h
//  CFSCalendar
//
//  Created by dingwenchao on 1/3/16.
//  Copyright © 2016 wenchaoios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CFSCalendar;

typedef NS_ENUM(NSUInteger, CFSCalendarScope);

@interface CFSCalendarFlowLayout : UICollectionViewFlowLayout <UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) CFSCalendar *calendar;


@end
