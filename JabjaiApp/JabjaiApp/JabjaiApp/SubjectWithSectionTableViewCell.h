//
//  SubjectWithSectionTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 1/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubjectWithSectionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *subjectNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sectionLabel;

@end
