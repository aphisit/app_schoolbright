//
//  AttendClassCalendarViewController.m
//  JabjaiApp
//
//  Created by mac on 10/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "AttendSchoolCalendarViewController.h"
#import "APIURL.h"
#import "AttendToSchoolModel.h"
#import "Utils.h"
#import "DateUtility.h"
#import "Constant.h"

@interface AttendSchoolCalendarViewController () {
    NSMutableDictionary *fillDefaultColors;
    
    NSMutableArray *attendToSchoolArray;
    NSMutableArray *weekendDays;
    
    NSInteger connectCounter;
}

@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) NSDate *minimumDate;
@property (strong, nonatomic) NSDate *maximumDate;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (strong, nonatomic) UIColor *calendarHeaderColor;
@property (strong, nonatomic) UIColor *calendarWeekdayTitleColor;
@property (strong, nonatomic) UIColor *calendarBackgroundColor;
@property (strong, nonatomic) UIColor *customTextSelectedColor;
@property (strong, nonatomic) UIColor *customTextDeselectedColor;

@property (strong, nonatomic) UIColor *eventRedColor;
@property (strong, nonatomic) UIColor *eventGreenColor;
@property (strong, nonatomic) UIColor *eventYellowColor;
@property (strong, nonatomic) UIColor *eventPinkColor;
@property (strong, nonatomic) UIColor *eventGrayColor;

@property (strong, nonatomic) UIColor *selectionOrangeColor;

@end

@implementation AttendSchoolCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Set up colors
    self.calendarHeaderColor = [UIColor colorWithRed:42/255.0 green:45/255.0 blue:44/255.0 alpha:1.0];
    self.calendarWeekdayTitleColor = [UIColor colorWithRed:99/255.0 green:96/255.0 blue:99/255.0 alpha:1.0];
    self.calendarBackgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
    self.customTextSelectedColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    self.customTextDeselectedColor = [UIColor colorWithRed:190/255.0 green:194/255.0 blue:197/255.0 alpha:1.0];
    self.eventRedColor = [UIColor colorWithRed:224/255.0 green:39/255.0 blue:33/255.0 alpha:1.0];
    self.eventGreenColor = [UIColor colorWithRed:62/255.0 green:194/255.0 blue:45/255.0 alpha:1.0];
    self.eventYellowColor = [UIColor colorWithRed:247/255.0 green:206/255.0 blue:41/255.0 alpha:1.0];
    self.eventPinkColor = [UIColor colorWithRed:247/255.0 green:152/255.0 blue:158/255.0 alpha:1.0];
    self.eventGrayColor = [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1.0];
    self.selectionOrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1.0];
    
    // Set up calendar
    _calendar.backgroundColor = self.calendarBackgroundColor;
    _calendar.dataSource = self;
    _calendar.delegate = self;
    _calendar.reverseCalendar = YES;
    _calendar.pagingEnabled = NO;
    _calendar.allowsSelection = NO;
    _calendar.allowsMultipleSelection = NO;
    _calendar.allowsRangeSelection = NO;
    _calendar.firstWeekday = 1; // Give 1 means saturday
    //_calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
    
    // appearance setter
    _calendar.appearance.weekdayTextColor = self.calendarWeekdayTitleColor;
    _calendar.appearance.headerTitleColor = self.calendarHeaderColor;
    _calendar.appearance.titlePlaceholderColor = self.customTextDeselectedColor;
    _calendar.appearance.selectionColor = self.selectionOrangeColor;
    _calendar.appearance.todayColor = [UIColor clearColor];
    _calendar.appearance.titleTodayColor = self.calendar.appearance.titleDefaultColor;
    _calendar.appearance.adjustsFontSizeToFitContentSize = YES;
    
    self.gregorian = [Utils getGregorianCalendar];
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.minimumDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:[NSDate date] options:0];
    self.maximumDate = [NSDate date];
    
    connectCounter = 0;
    
    //    fillDefaultColors = [NSMutableDictionary dictionaryWithDictionary:@{@"2016/08/10" : redColor, @"2016/08/11" : redColor, @"2016/08/12" : redColor, @"2016/08/13" : redColor, @"2016/08/14" : redColor, @"2016/08/20" : greenColor, @"2016/08/21" : greenColor, @"2016/08/22" : greenColor, @"2016/08/23" : greenColor, @"2016/08/24" : greenColor, @"2016/09/10" : yellowColor, @"2016/09/11" : yellowColor, @"2016/09/12" : yellowColor, @"2016/09/13" : yellowColor, @"2016/09/14" : yellowColor}];
    
    [self callAPIGetAttendSchoolData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma CFSCalendarDataSource
- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

#pragma CFSCalendarDelegate
- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
    
    NSString *dateAsString = [self.dateFormatter stringFromDate:date];
    NSLog(@"%@", [NSString stringWithFormat:@"Did select %@", dateAsString]);
    
}

- (void)calendarCurrentPageDidChange:(CFSCalendar *)calendar {
    
    NSString *dateAsString = [self.dateFormatter stringFromDate:calendar.currentPage];
    NSLog(@"%@", [NSString stringWithFormat:@"Did change page %@", dateAsString]);
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date {
    
    if(fillDefaultColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if([fillDefaultColors objectForKey:key] != nil) {
            return [fillDefaultColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date {
    
    if(fillDefaultColors != nil) {
        NSString *key = [self.dateFormatter stringFromDate:date];
        
        if([fillDefaultColors objectForKey:key] != nil) {
            
            if([weekendDays containsObject:key]) {
                return self.customTextDeselectedColor;
            }
            else {
                return self.customTextSelectedColor;
            }
            
        }
    }
    
    return nil;
}

#pragma mark - GetAPIData

-(void)callAPIGetAttendSchoolData {
    [self getAttendSchoolData:self.minimumDate endDate:self.maximumDate status:-1];
}

-(void)getAttendSchoolData:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status {
    
    NSString *URLString = [APIURL getAttendSchoolDataURLWithStartDate:startDate endDate:endDate status:status];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self callAPIGetAttendSchoolData];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetAttendSchoolData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetAttendSchoolData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                attendToSchoolArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSString *statusStr = [dataDict objectForKey:@"StatusIN"];
                    NSMutableString *scanDateTimeStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dScan"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) scanDateTimeStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *scanDate = [formatter dateFromString:scanDateTimeStr];
                    
                    NSInteger status;
                    
                    if([statusStr isKindOfClass:[NSNull class]] || [statusStr length] == 0) {
                        status = 3; //Absence
                    }
                    else {
                        status = [statusStr integerValue];
                    }
                    
                    AttendToSchoolModel *model = [[AttendToSchoolModel alloc] init];
                    [model setStatus:[[NSNumber alloc] initWithInteger:status]];
                    [model setScanDate:scanDate];
                    
                    [attendToSchoolArray addObject:model];
                }
                
                [self updateDateEvents];
                
            }
            
        }
    }];
    
}

#pragma mark - Manager date event

- (void)updateDateEvents {
    
    if(attendToSchoolArray == nil) {
        return;
    }
    
    if(fillDefaultColors != nil) {
        [fillDefaultColors removeAllObjects];
    }
    
    if(weekendDays != nil) {
        [weekendDays removeAllObjects];
    }
    
    fillDefaultColors = [[NSMutableDictionary alloc] init];
    weekendDays = [[NSMutableArray alloc] init];
    
    for(AttendToSchoolModel *model in attendToSchoolArray) {
        
        NSString *dateKey = [self.dateFormatter stringFromDate:model.scanDate];
        
        if([model.status intValue] == 0) { // ON TIME
            [fillDefaultColors setObject:self.eventGreenColor forKey:dateKey];
        }
        else if([model.status intValue] == 1) { // LATE
            [fillDefaultColors setObject:self.eventYellowColor forKey:dateKey];
        }
        else if([model.status intValue] == 3) { // ABSENCE
            [fillDefaultColors setObject:self.eventRedColor forKey:dateKey];
        }
        else if([model.status intValue] == 7) { // ON LEAVE
            [fillDefaultColors setObject:self.eventPinkColor forKey:dateKey];
        }
        else if([model.status intValue] == 8) { // HOLIDAY
            [fillDefaultColors setObject:self.eventGrayColor forKey:dateKey];
        }
        else if([model.status intValue] == 9) { // WEEKEND
            [fillDefaultColors setObject:[UIColor clearColor] forKey:dateKey];
            [weekendDays addObject:dateKey];
        }
    }
    
    [self.calendar reloadData];
}

#pragma mark - Filter

-(void)applyFilter:(Attend2SchoolFilter *)filter {
    
    NSInteger status = -1;
    
    if(filter.status == INTIME) {
        status = 0;
    }
    else if(filter.status == LATE) {
        status = 1;
    }
    else if(filter.status == ABSENCE) {
        status = 3;
    }
    else if(filter.status == ONLEAVE) {
        status = 7;
    }
    else if(filter.status == HOLIDAY) {
        status = 8;
    }
    
    self.minimumDate = filter.startDate;
    self.maximumDate = filter.endDate;
    [self getAttendSchoolData:filter.startDate endDate:filter.endDate status:status];
    
    NSLog(@"%@", @"Attend To School Filter Applied");
}

@end
