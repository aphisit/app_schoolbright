//
//  AttendToSectionHistoryModel.h
//  JabjaiApp
//
//  Created by mac on 1/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AttendToSectionHistoryModel : NSObject

@property (nonatomic, strong) NSNumber *status; // 0 : INTIME, 1 : LATE, 3 : ABSENCE
@property (nonatomic, strong) NSDate *scanDate;

@end
