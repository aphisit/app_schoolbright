//
//  StudyViewController.m
//  JabjaiApp
//
//  Created by mac on 10/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "StudyViewController.h"
#import "APIURL.h"
#import "Utils.h"
#import "YearTermModel.h"
#import "DateUtility.h"
#import "AttendToSchoolModel.h"
#import "CreditBureauGraphDialog.h"
#import "UserData.h"
#import "Constant.h"

@interface StudyViewController () {
    UIColor *yellowColor;
    UIColor *greenColor;
    UIColor *onTimeColor;
    UIColor *lateColor;
    UIColor *absenceColor;
    
    NSMutableArray *attendToSchoolArray;
    NSMutableArray *yData;
    NSMutableArray *xData;
    
    NSInteger totalStatus;
    NSInteger statusOnTime;
    NSInteger statusLate;
    NSInteger statusAbsence;

    // The dialog
    TableListWithHeaderDialog *dialogSelectMonth;
    TableListDialog *dialogSelectYear;
    CreditBureauGraphDialog *creditBureauGraphDialog;
    
    // Data Containers
    NSMutableDictionary *yearTermDict; //Key is yearNumber and data are array of YearTermModel
    
    NSString *weekly_text;
    NSString *monthly_text;
    NSString *yearly_text;
    
    NSString *buttonFontName;
    CGFloat buttonFontSize;
    UIFont *buttonUIFont;
    
    // Calendar Filter Date
    //NSDate *startDate;
    //NSDate *endDate;
    
    // List of data show in listview
    NSMutableArray *years;
    
    NSInteger selectedYearIndex;
    
    NSInteger connectCounter;
}

@end

static NSString *selectMonthRequestCode = @"SELECT_MONTH";
static NSString *selectYearRequestCode = @"SELECT_YEAR";

@implementation StudyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    yellowColor = [UIColor colorWithRed:254/255.0 green:180/255.0 blue:59/255.0 alpha:1.0];
    greenColor = [UIColor colorWithRed:34/255.0 green:125/255.0 blue:117/255.0 alpha:1.0];
    onTimeColor = [UIColor colorWithRed:34/255.0 green:125/255.0 blue:117/255.0 alpha:1.0];
    lateColor = [UIColor colorWithRed:72/255.0 green:160/255.0 blue:152/255.0 alpha:1.0];
    absenceColor = [UIColor colorWithRed:230/255.0 green:107/255.0 blue:77/255.0 alpha:1.0];
    
    weekly_text = @"สัปดาห์";
    monthly_text = @"เดือน";
    yearly_text = @"ปีการศึกษา";
    
    buttonFontName = self.monthlyBtn.titleLabel.font.fontName;
    buttonFontSize = self.monthlyBtn.titleLabel.font.pointSize;
    buttonUIFont = [UIFont fontWithName:buttonFontName size:buttonFontSize];
    
    // setup list view data variables
    years = [[NSMutableArray alloc] init];
    
    selectedYearIndex = 0;
    
    connectCounter = 0;
    
    // setup the dialog
    dialogSelectMonth = [[TableListWithHeaderDialog alloc] initWithRequestCode:selectMonthRequestCode];
    dialogSelectMonth.delegate = self;
    dialogSelectYear = [[TableListDialog alloc] initWithRequestCode:selectYearRequestCode];
    dialogSelectYear.delegate = self;
    creditBureauGraphDialog = [[CreditBureauGraphDialog alloc] init];
    
    [self.pieChart setBackgroundColor:[UIColor clearColor]];
    
    //yData = @[@10, @30, @60];
    //xData = @[@"On time", @"Late", @"Absence"];
    xData = [[NSMutableArray alloc] init];
    yData = [[NSMutableArray alloc] init];
    
    [self decorateButton];
    [self setupPieChartView];
    [self setupGraphData];
    //[self setupGraphLegend];
    
    //set up graphLegend
    [self.viewOnTime setBackgroundColor:onTimeColor];
    [self.viewLate setBackgroundColor:lateColor];
    [self.viewAbsence setBackgroundColor:absenceColor];
    
    // Attach tap event to each legend view
    UITapGestureRecognizer *onTimeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleOnTimeStackViewTap:)];
    [self.onTimeStackView addGestureRecognizer:onTimeTap];
    
    UITapGestureRecognizer *lateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLateStackViewTap:)];
    [self.lateStackView addGestureRecognizer:lateTap];
    
    UITapGestureRecognizer *absenceTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleAbsenceStackViewTap:)];
    [self.absenceStackView addGestureRecognizer:absenceTap];
    
    if([UserData getAcademyType] == SCHOOL) {
        [self getYearAndTermData];
        [self performSelectWeeklyButton];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    
    if(self.pieChart != nil) {
        [self.pieChart animateWithXAxisDuration:2.0 easingOption:ChartEasingOptionEaseOutBack];
    }
    

}

- (void)decorateButton {
    self.yearlyBtn.layer.cornerRadius = 5.0;
    self.yearlyBtn.layer.masksToBounds = YES;
    self.yearlyBtn.backgroundColor = greenColor;
    [self.yearlyBtn setTitle:yearly_text forState:UIControlStateNormal];
    [self.yearlyBtn.titleLabel setFont:buttonUIFont];
    
    
    self.monthlyBtn.layer.cornerRadius = 5.0;
    self.monthlyBtn.layer.masksToBounds = YES;
    self.monthlyBtn.backgroundColor = greenColor;
    [self.monthlyBtn setTitle:monthly_text forState:UIControlStateNormal];
    [self.monthlyBtn.titleLabel setFont:buttonUIFont];
    
    self.weeklyBtn.layer.cornerRadius = 5.0;
    self.weeklyBtn.layer.masksToBounds = YES;
    self.weeklyBtn.backgroundColor = yellowColor;
    [self.weeklyBtn setTitle:weekly_text forState:UIControlStateNormal];
    [self.weeklyBtn.titleLabel setFont:buttonUIFont];
}

#pragma mark - Pie Chart

- (void)setupPieChartView {
    
    self.pieChart.delegate = self;
    self.pieChart.usePercentValuesEnabled = YES;
    self.pieChart.drawSlicesUnderHoleEnabled = NO;
    //self.pieChart.drawSliceTextEnabled = NO;
    self.pieChart.holeRadiusPercent = 0.45;
    self.pieChart.transparentCircleRadiusPercent = 0.48;
    self.pieChart.chartDescription.enabled = NO;
    [self.pieChart setExtraOffsetsWithLeft:5.f top:10.f right:5.f bottom:5.f];
    
    self.pieChart.drawCenterTextEnabled = NO;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    self.pieChart.drawHoleEnabled = YES;
    self.pieChart.rotationAngle = 0.0;
    self.pieChart.rotationEnabled = YES;
    self.pieChart.highlightPerTapEnabled = YES;
    
    self.pieChart.entryLabelColor = [UIColor whiteColor];
    self.pieChart.entryLabelFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:18.f];
    
    ChartLegend *l = self.pieChart.legend;
    [l setEnabled:NO];
    
}

//- (void)setupGraphLegend {
//    ChartLegend *l = self.pieChart.legend;
//    l.position = ChartLegendPositionBelowChartCenter;
//    l.orientation = ChartLegendOrientationHorizontal;
//    l.drawInside = NO;
//    l.xEntrySpace = 7.0;
//    l.yEntrySpace = 0.0;
//    l.yOffset = 20.0;
//    
//    [l setFont:[UIFont fontWithName:@"ThaiSansNeue-Regular" size:22.0]];
//    l.formSize =  25.0f;
//    
//    NSMutableArray *legendColors = [[NSMutableArray alloc] initWithObjects:onTimeColor, lateColor, absenceColor, nil];
//    
//    NSString *onTimeLegend = [NSString stringWithFormat:@"%@ (%i%%)", [xData objectAtIndex:0], (int)[[yData objectAtIndex:0] integerValue]];
//    NSString *lateLegend = [NSString stringWithFormat:@"%@ (%i%%)", [xData objectAtIndex:1], (int)[[yData objectAtIndex:1] integerValue]];
//    NSString *absenceLegend = [NSString stringWithFormat:@"%@ (%i%%)", [xData objectAtIndex:2], (int)[[yData objectAtIndex:2] integerValue]];
//    
//    NSMutableArray *legendLabel = [[NSMutableArray alloc] initWithObjects:onTimeLegend, lateLegend, absenceLegend, nil];
//    [l setCustomWithColors:legendColors labels:legendLabel];
//}

- (void)setupGraphData {
    
    if(yData.count == 0) {
        [self.pieChart setAlpha:0.0];
        [self.graphLegendView setAlpha:0.0];
        [self.noDataLabel setAlpha:1.0];
        
        return;
    }
    else {
        [self.pieChart setAlpha:1.0];
        [self.graphLegendView setAlpha:1.0];
        [self.noDataLabel setAlpha:0.0];
    }
    
    NSMutableArray *values = [[NSMutableArray alloc] init];
    
    // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
    for (int i = 0; i < yData.count; i++)
    {
        [values addObject:[[PieChartDataEntry alloc] initWithValue:([[yData objectAtIndex:i] integerValue]) label:[xData objectAtIndex:i]]];
    }
    
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:values label:@""];
    dataSet.sliceSpace = 2.0;
    
    // add a lot of colors
    
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    [colors addObject:onTimeColor];
    [colors addObject:lateColor];
    [colors addObject:absenceColor];
    [colors addObjectsFromArray:ChartColorTemplates.vordiplom];
    [colors addObjectsFromArray:ChartColorTemplates.joyful];
    [colors addObjectsFromArray:ChartColorTemplates.colorful];
    [colors addObjectsFromArray:ChartColorTemplates.liberty];
    [colors addObjectsFromArray:ChartColorTemplates.pastel];
    [colors addObject:[UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f]];
    
    dataSet.colors = colors;
    
    PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
    
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
    [data setValueFont:[UIFont fontWithName:@"ThaiSansNeue-Regular" size:18.0f]];
    [data setValueTextColor:UIColor.whiteColor];
    
    self.pieChart.data = data;
    [self.pieChart highlightValue:nil];
    
    [self.pieChart animateWithXAxisDuration:2.0 easingOption:ChartEasingOptionEaseOutBack];
    
}

#pragma ChartDelegate

-(void)chartValueSelected:(ChartViewBase *)chartView entry:(ChartDataEntry *)entry highlight:(ChartHighlight *)highlight {
    
    NSInteger selectedIndex = highlight.x
    ;
    
    NSString *dialogTitle = @"สถิติการมาเรียน";
    
    if(selectedIndex == 0) { // Select on time
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:ONTIME times:statusOnTime totalTimes:totalStatus];
    }
    else if(selectedIndex == 1) {
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:LATE times:statusLate totalTimes:totalStatus];
    }
    else {
        [self showCreditBureauGraphDialogWithTitle:dialogTitle status:ABSENCE times:statusAbsence totalTimes:totalStatus];
    }
    
    NSString *logStr = [NSString stringWithFormat:@"chartValueSelected at index %i", (int)selectedIndex];
    NSLog(@"%@", logStr);
}

#pragma mark - Update chart data
- (void)updatePieChartDataWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    [self getAttendSchoolData:startDate endDate:endDate status:-1];
}

#pragma mark - Graph Legend Tap
-(void)handleOnTimeStackViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:ONTIME times:statusOnTime totalTimes:totalStatus];
}

-(void)handleLateStackViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:LATE times:statusLate totalTimes:totalStatus];
}

-(void)handleAbsenceStackViewTap:(UITapGestureRecognizer *)recognizer {
    [self showCreditBureauGraphDialogWithTitle:@"สถิติการมาเรียน" status:ABSENCE times:statusAbsence totalTimes:totalStatus];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark View Controller Functions
- (IBAction)yearlyButtonTap:(id)sender {
    
    if(years == nil) {
        return;
    }
    
    NSMutableArray *yearStrArr = [[NSMutableArray alloc] init];
    
    for(NSNumber *year in years) {
        [yearStrArr addObject:[NSString stringWithFormat:@"%i", [year intValue]]];
    }
    
    [self showDialogSelectYearWithYearArray:yearStrArr];
}

- (IBAction)monthlyButtonTap:(id)sender {
    
//    NSLog(@"%@", @"Tap Monthly Button");
//    
//    NSArray *months1 = @[@"January", @"February", @"March"];
//    NSArray *months2 = @[@"May", @"June", @"July"];
//    NSArray *months3 = @[@"September", @"October", @"November"];
//    
//    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
//    [dataDict setObject:months1 forKey:@"Term 1"];
//    [dataDict setObject:months2 forKey:@"Term 2"];
//    [dataDict setObject:months3 forKey:@"Term 3"];
//    
//    NSArray *dataKeys = @[@"Term 1", @"Term 2", @"Term 3"];
//    
//    [dialogSelectMonth showDialogInView:self.parentViewController.view stringDataDict:dataDict dataDictKeys:dataKeys];
    
    NSMutableArray *monthsTermDataKeys = [[NSMutableArray alloc] init];
    NSMutableDictionary *monthsDict = [[NSMutableDictionary alloc] init];
    
    if(yearTermDict == nil) {
        return;
    }
    
    // Start
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSNumber *selectedYear = [years objectAtIndex:selectedYearIndex];
    NSMutableArray *termArr = [yearTermDict objectForKey:selectedYear];
    
    [monthsTermDataKeys addObject:@"ทั้งหมด"];
    [monthsDict setObject:[[NSArray alloc] initWithObjects:@"ทั้งหมด", nil] forKey:@"ทั้งหมด"];
    
    for(YearTermModel *model in termArr) {
        
        NSInteger startMonth = [gregorian component:NSCalendarUnitMonth fromDate:model.termBegin];
        NSInteger endMonth = [gregorian component:NSCalendarUnitMonth fromDate:model.termEnd];
        
        NSArray *monthsInTerm = [Utils getThaiMonthRangeWithStartMonth:startMonth endMonth:endMonth];
        
        if(monthsInTerm != nil) {
            
            [monthsTermDataKeys addObject:model.termName];
            [monthsDict setObject:monthsInTerm forKey:model.termName];
            
        }
    }
    
    [self showDialogSelectMonthWithMonthDict:monthsDict monthsTermDataKeys:monthsTermDataKeys];
    
}

- (IBAction)weeklyButtonTap:(id)sender {
    
    NSLog(@"%@", @"Tap Weekly Button");
    
    [self performSelectWeeklyButton];
}


#pragma mark TableListWithHeaderDialogDelegate

- (void)onItemSelectWithRequestCode:(NSString *)requestCode headerSectionString:(NSString *)headerSectionString selectedString:(NSString *)selectedString {
    
    NSLog(@"%@, %@", headerSectionString, selectedString);
    
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSInteger gregorianYear = [gregorian component:NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSNumber *selectedYear = [years objectAtIndex:selectedYearIndex];
    NSInteger yearAD = [selectedYear integerValue];
    
    if(yearAD > gregorianYear) {
        yearAD -= 543; //Convert from BE to AD
    }
    
    NSMutableArray *termArr = [yearTermDict objectForKey:selectedYear];
    
    if([headerSectionString isEqualToString:selectedString]) { // If user select all months
        
        [self performSelectYearlyButton];
        
    }
    else {
        
        NSInteger selectedMonthNumber = [Utils getThaiMonthNumberWithName:selectedString];
        
        for(YearTermModel *model in termArr) {
            if([model.termName isEqualToString:headerSectionString]) {
                NSInteger startMonth = [gregorian component:NSCalendarUnitMonth fromDate:model.termBegin];
                NSInteger endMonth = [gregorian component:NSCalendarUnitMonth fromDate:model.termEnd];
                
                if(selectedMonthNumber >= startMonth && selectedMonthNumber <= endMonth) {
                    NSDate *startMonth = [DateUtility startDateOfMonthAndYearInRangeWithMonth:selectedMonthNumber year:yearAD minDate:model.termBegin maxDate:model.termEnd];
                    NSDate *endMonth = [DateUtility endDateOfMonthAndYearInRangeWithMonth:selectedMonthNumber year:yearAD minDate:model.termBegin maxDate:model.termEnd];
                    
                    [self updatePieChartDataWithStartDate:startMonth endDate:endMonth];
                    
                    break;
                }
                
            }
        }
        
        // Change color and text of buttons
        self.weeklyBtn.backgroundColor = greenColor;
        [self.weeklyBtn setTitle:weekly_text forState:UIControlStateNormal];
        [self.weeklyBtn.titleLabel setFont:buttonUIFont];
        self.monthlyBtn.backgroundColor = yellowColor;
        [self.monthlyBtn setTitle:selectedString forState:UIControlStateNormal];
        [self.monthlyBtn.titleLabel setFont:buttonUIFont];
        self.yearlyBtn.backgroundColor = yellowColor;
        [self.yearlyBtn setTitle:[NSString stringWithFormat:@"%i", [selectedYear intValue]] forState:UIControlStateNormal];
        [self.yearlyBtn.titleLabel setFont:buttonUIFont];
        
    }

}


#pragma mark TableListDialogDelegate
-(void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:selectYearRequestCode]) {
        selectedYearIndex = index;
        [self performSelectYearlyButton];
    }
}

-(void)performSelectWeeklyButton {
    
    NSDate *endDate = [NSDate date];
    NSDate *sixDaysAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-6 toDate:endDate options:0];
    
    [self updatePieChartDataWithStartDate:sixDaysAgo endDate:endDate];
    
    // Change color and text of buttons
    self.weeklyBtn.backgroundColor = yellowColor;
    [self.weeklyBtn setTitle:weekly_text forState:UIControlStateNormal];
    [self.weeklyBtn.titleLabel setFont:buttonUIFont];
    self.monthlyBtn.backgroundColor = greenColor;
    [self.monthlyBtn setTitle:monthly_text forState:UIControlStateNormal];
    [self.monthlyBtn.titleLabel setFont:buttonUIFont];
    self.yearlyBtn.backgroundColor = greenColor;
    [self.yearlyBtn setTitle:yearly_text forState:UIControlStateNormal];
    [self.yearlyBtn.titleLabel setFont:buttonUIFont];
    
    // Set current study year at 0
    selectedYearIndex = 0;

}

-(void)performSelectYearlyButton {
    
    NSNumber *selectedYear = [years objectAtIndex:selectedYearIndex];
    NSMutableArray *termArr = [yearTermDict objectForKey:selectedYear];
    NSMutableArray *allDatesInYear = [[NSMutableArray alloc] init];
    
    for(YearTermModel *model in termArr) {
        [allDatesInYear addObject:model.termBegin];
        [allDatesInYear addObject:model.termEnd];
    }
    
    NSDate *startDate = [DateUtility minDate:allDatesInYear];
    NSDate *endDate = [DateUtility maxDate:allDatesInYear];
    
    [self updatePieChartDataWithStartDate:startDate endDate:endDate];
    
    //Clear other button to defaults
    self.weeklyBtn.backgroundColor = greenColor;
    [self.weeklyBtn setTitle:weekly_text forState:UIControlStateNormal];
    [self.weeklyBtn.titleLabel setFont:buttonUIFont];
    self.monthlyBtn.backgroundColor = greenColor;
    [self.monthlyBtn setTitle:monthly_text forState:UIControlStateNormal];
    [self.monthlyBtn.titleLabel setFont:buttonUIFont];
    self.yearlyBtn.backgroundColor = yellowColor;
    
    NSNumber *yearNumber = [years objectAtIndex:selectedYearIndex];
    [self.yearlyBtn setTitle:[NSString stringWithFormat:@"%i", [yearNumber intValue]] forState:UIControlStateNormal];
    [self.yearlyBtn.titleLabel setFont:buttonUIFont];
}

#pragma mark - GetAPIData

-(void)getYearAndTermData {
    NSString *URLString = [APIURL getYearAndTermURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getYearAndTermData];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getYearAndTermData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getYearAndTermData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                yearTermDict = [[NSMutableDictionary alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSInteger yearID = [[dataDict objectForKey:@"Yearid"] integerValue];
                    NSInteger yearNumber = [[dataDict objectForKey:@"YearNumber"] integerValue];
                    NSMutableString *termID = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Termid"]];
                    NSMutableString *termName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TermName"]];
                    NSMutableString *startDateStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dStart"]];
                    NSMutableString *endDateStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dEnd"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) termID);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) termName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endDateStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *lStartDate = [formatter dateFromString:startDateStr];
                    NSDate *lEndDate = [formatter dateFromString:endDateStr];
                    
                    YearTermModel *model = [[YearTermModel alloc] init];
                    [model setYearID:[[NSNumber alloc] initWithInteger:yearID]];
                    [model setYearNumber:[[NSNumber alloc] initWithInteger:yearNumber]];
                    [model setTermID:termID];
                    [model setTermName:termName];
                    [model setTermBegin:lStartDate];
                    [model setTermEnd:lEndDate];
                    
                    if([yearTermDict objectForKey:model.yearNumber] != nil) {
                        NSMutableArray *termArr = [yearTermDict objectForKey:model.yearNumber];
                        [termArr addObject:model];
                        [yearTermDict setObject:termArr forKey:model.yearNumber];
                    }
                    else {
                        NSMutableArray *termArr = [[NSMutableArray alloc] init];
                        [termArr addObject:model];
                        [yearTermDict setObject:termArr forKey:model.yearNumber];
                    }
                }
                
                // Update yearlist data
                years = [[NSMutableArray alloc] initWithArray:yearTermDict.allKeys];
                NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
                [years sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            }

        }

    }];
    
}

-(void)getAttendSchoolData:(NSDate *)startDate endDate:(NSDate *)endDate status:(NSInteger)status {
    NSString *URLString = [APIURL getAttendSchoolDataURLWithStartDate:startDate endDate:endDate status:status];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getAttendSchoolData:startDate endDate:endDate status:status];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getAttendSchoolData:startDate endDate:endDate status:status];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getAttendSchoolData:startDate endDate:endDate status:status];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                attendToSchoolArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSString *statusStr = [dataDict objectForKey:@"StatusIN"];
                    NSMutableString *scanDateTimeStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dScan"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) scanDateTimeStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *scanDate = [formatter dateFromString:scanDateTimeStr];
                    
                    NSInteger status;
                    
                    if([statusStr isKindOfClass:[NSNull class]] || [statusStr length] == 0) {
                        status = 3; //Absence
                    }
                    else {
                        status = [statusStr integerValue];
                    }
                    
                    AttendToSchoolModel *model = [[AttendToSchoolModel alloc] init];
                    [model setStatus:[[NSNumber alloc] initWithInteger:status]];
                    [model setScanDate:scanDate];
                    
                    [attendToSchoolArray addObject:model];
                }
                
                // prepare data for pie chart
                [xData removeAllObjects];
                [yData removeAllObjects];
                
                statusOnTime = 0;
                statusLate = 0;
                statusAbsence = 0;
                
                for(AttendToSchoolModel *model in attendToSchoolArray) {
                    
                    if([model.status integerValue] == 0) {
                        statusOnTime++;
                    }
                    else if([model.status integerValue] == 1) {
                        statusLate++;
                    }
                    else if([model.status integerValue] == 3) {
                        statusAbsence++;
                    }
                }
                
                totalStatus = statusOnTime + statusLate + statusAbsence;
                
                [xData addObject:@"ตรงเวลา"];
                [xData addObject:@"สาย"];
                [xData addObject:@"ขาด"];
                
                [yData addObject:[[NSNumber alloc] initWithInteger:statusOnTime]];
                [yData addObject:[[NSNumber alloc] initWithInteger:statusLate]];
                [yData addObject:[[NSNumber alloc] initWithInteger:statusAbsence]];
                
                float onTimePercent = (float)statusOnTime/totalStatus * 100;
                float latePercent = (float)statusLate/totalStatus * 100;
                float absencePercent = (float)statusAbsence/totalStatus * 100;
                
                NSString *onTimeLegendStr;
                NSString *lateLegendStr;
                NSString *absenceLegendStr;
                
                if(onTimePercent == 0 || onTimePercent == 100) {
                    onTimeLegendStr = [NSString stringWithFormat:@"ตรงเวลา\n(%.0f%%)", onTimePercent];
                }
                else {
                    onTimeLegendStr = [NSString stringWithFormat:@"ตรงเวลา\n(%.1f%%)", onTimePercent];
                }
                
                if(latePercent == 0 || latePercent == 100) {
                    lateLegendStr = [NSString stringWithFormat:@"สาย\n(%.0f%%)", latePercent];
                }
                else {
                    lateLegendStr = [NSString stringWithFormat:@"สาย\n(%.1f%%)", latePercent];
                }
                
                if(absencePercent == 0 || absencePercent == 100) {
                    absenceLegendStr = [NSString stringWithFormat:@"ขาด\n(%.0f%%)", absencePercent];
                }
                else {
                    absenceLegendStr = [NSString stringWithFormat:@"ขาด\n(%.1f%%)", absencePercent];
                    
                }
                
                [self.onTimeLabel setText:onTimeLegendStr];
                [self.lateLabel setText:lateLegendStr];
                [self.absenceLabel setText:absenceLegendStr];
                
                [self setupGraphData];

            }
        }

    }];
}

#pragma mark - Dialog
- (void)showDialogSelectMonthWithMonthDict:(NSDictionary *)monthsDict  monthsTermDataKeys:(NSArray *)monthsTermDataKeys {
    
    if(dialogSelectMonth != nil && [dialogSelectMonth isDialogShowing]) {
        [dialogSelectMonth dismissDialog];
    }
    
    [dialogSelectMonth showDialogInView:self.parentViewController.view stringDataDict:monthsDict dataDictKeys:monthsTermDataKeys];
}

- (void)showDialogSelectYearWithYearArray:(NSArray *)yearArray {
    
    if(dialogSelectYear != nil && [dialogSelectYear isDialogShowing]) {
        [dialogSelectYear dismissDialog];
    }
    
    [dialogSelectYear showDialogInView:self.parentViewController.view dataArr:yearArray];
}

- (void)showCreditBureauGraphDialogWithTitle:(NSString *)title status:(ATTENDSTATUS)status times:(NSInteger)times totalTimes:(NSInteger)totalTimes {
    
    if(creditBureauGraphDialog != nil && [creditBureauGraphDialog isDialogShowing]) {
        [creditBureauGraphDialog dismissDialog];
    }

    [creditBureauGraphDialog showDialogInView:self.parentViewController.view title:title status:status times:times totalTimes:totalTimes];
}

@end
