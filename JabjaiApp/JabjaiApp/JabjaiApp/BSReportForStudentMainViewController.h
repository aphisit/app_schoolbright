//
//  BSReportForStudentMainViewController.h
//  JabjaiApp
//
//  Created by mac on 7/27/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "BSHistoryModel.h"
#import "CallGetBSHistoryForStudentReportAPI.h"

@interface BSReportForStudentMainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CallGetBSHistoryForStudentReportAPIDelegate>

// Global variables
@property (strong, nonatomic) NSArray<BSHistoryModel *> *bsHistoryArray;
@property (nonatomic) NSInteger remainingScore;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *remainingScoreLabel;

- (IBAction)openDrawer:(id)sender;
@end
