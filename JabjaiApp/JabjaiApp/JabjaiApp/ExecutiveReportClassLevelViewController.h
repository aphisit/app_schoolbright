//
//  ExecutiveReportClassLevelViewController.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "EXReportClassStatusModel.h"

@interface ExecutiveReportClassLevelViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

// Global variables
@property (nonatomic) NSDate *reportDate;
@property (strong, nonatomic) NSArray<EXReportClassStatusModel *> *reportClassStatusArray;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)moveBack:(id)sender;

@end
