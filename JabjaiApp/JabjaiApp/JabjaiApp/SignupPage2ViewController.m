//
//  SignupPage2ViewController.m
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SignupPage2ViewController.h"
#import "SignupPage1ViewController.h"
#import "SignupPage3ViewController.h"
#import "UniversitySignupPage1ViewController.h"
#import "AlertDialog.h"

@interface SignupPage2ViewController () {
    AlertDialog *alertDialog;
}

@end

@implementation SignupPage2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.firstNameTextField.delegate = self;
    self.lastNameTextField.delegate = self;
    
    alertDialog = [[AlertDialog alloc] init];

    // Restore data in all text fields
    
    if(_signupDataModel.gender == nil || [_signupDataModel.gender integerValue] == 0) {
        self.genderRadioButton.selected = YES;
    }
    else if([_signupDataModel.gender integerValue] == 1) {
        NSArray *otherButtons = self.genderRadioButton.otherButtons;
        
        for(DLRadioButton *radioBtn in otherButtons) {
            if(radioBtn.tag == 1) {
                radioBtn.selected = YES;
            }
        }
    }
    
    if(_signupDataModel.firstName != nil) {
        self.firstNameTextField.text = _signupDataModel.firstName;
    }
    
    if(_signupDataModel.lastName != nil) {
        self.lastNameTextField.text = _signupDataModel.lastName;
    }
    
    // Docorate the views
    self.nextBtn.layer.cornerRadius = 20;
    self.nextBtn.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"signupPage1Segue"]) {
        SignupPage1ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
    else if([segue.identifier isEqualToString:@"universitySignupPage1Segue"]) {
        UniversitySignupPage1ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
    }
    else if([segue.identifier isEqualToString:@"signupPage3Segue"]) {
        SignupPage3ViewController *controller = [segue destinationViewController];
        controller.signupDataModel = _signupDataModel;
        controller.previousSegue = @"signupPage2Segue";
    }

}

- (IBAction)nextStep:(id)sender {
    
    BOOL isDataValidate = [self validateData];
    
    if(isDataValidate) {
        NSInteger gender = 0;
        if(_genderRadioButton.selectedButton.tag == 1) {
            gender = 1;
        }
        
        NSMutableString *firstName = [[NSMutableString alloc] initWithString:self.firstNameTextField.text];
        CFStringTrimWhitespace((__bridge CFMutableStringRef) firstName);
        
        NSMutableString *lastName = [[NSMutableString alloc] initWithString:self.lastNameTextField.text];
        CFStringTrimWhitespace((__bridge CFMutableStringRef) lastName);
        
        _signupDataModel.gender = [[NSNumber alloc] initWithInteger:gender];
        _signupDataModel.firstName = firstName;
        _signupDataModel.lastName = lastName;
        
        [self performSegueWithIdentifier:@"signupPage3Segue" sender:self];
    }
    
}

- (IBAction)moveBack:(id)sender {
    
    if([_signupDataModel.academyType intValue] == 1) { // school user
        [self performSegueWithIdentifier:@"signupPage1Segue" sender:self];
    }
    else {
        // university user
        [self performSegueWithIdentifier:@"universitySignupPage1Segue" sender:self];
    }
    
}

- (IBAction)dismissKeyboard:(id)sender {
    [self resignFirstResponder];
}

#pragma mark - Individual Functions
-(BOOL)validateData {
    
    NSString *dialogTitle = @"แจ้งเตือน";
    
    if([self.firstNameTextField.text length] == 0 || [self.lastNameTextField.text length] == 0) {
        
        NSString *alertMessage = @"กรุณากรอกข้อมูลให้ครบถ้วน";
        [self showAlertDialogWithTitle:dialogTitle message:alertMessage];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

@end
