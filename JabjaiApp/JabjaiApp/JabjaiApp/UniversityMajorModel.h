//
//  UniversityMajorModel.h
//  JabjaiApp
//
//  Created by mac on 11/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UniversityMajorModel : NSObject

@property (nonatomic, strong) NSNumber *majorID;
@property (nonatomic, strong) NSString *majorName;

-(NSNumber *)getMajorID;
-(NSString *)getMajorName;

@end
