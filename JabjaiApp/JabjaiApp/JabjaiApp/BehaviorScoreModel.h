//
//  BehaviorScoreModel.h
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BehaviorScoreModel : NSObject

@property (nonatomic) NSInteger behaviorScoreType; // 0: type add, 1: type reduce
@property (nonatomic) long long behaviorScoreId;
@property (nonatomic) NSString *behaviorScoreName;
@property (nonatomic) NSInteger score;
@property (nonatomic) BOOL selected;

- (void)setBehaviorScoreType:(NSInteger)behaviorScoreType;
- (void)setBehaviorScoreId:(long long)behaviorScoreId;
- (void)setBehaviorScoreName:(NSString *)behaviorScoreName;
- (void)setScore:(NSInteger)score;
- (void)setSelected:(BOOL)selected;

- (NSInteger)getBehaviorScoreType;
- (long long)getBehaviorScoreId;
- (NSString *)getBehaviorScoreName;
- (NSInteger)getScore;
- (BOOL)isSelected;

@end
