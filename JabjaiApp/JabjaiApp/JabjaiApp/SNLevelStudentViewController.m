//
//  SNLevelStudentViewController.m
//  JabjaiApp
//
//  Created by toffee on 8/28/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNLevelStudentViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import "SNClassLevelViewController.h"
#import "SNSelectTheaCherViewController.h"



@interface SNLevelStudentViewController (){

    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    
    NSArray *category,*recipient;
    

  
}
@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;

@end
static NSString *classLevelRequestCode = @"group";
static NSString *classroomRequestCode = @"oneUser";


@implementation SNLevelStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }

    
    // Do any additional setup after loading the view.
    self.classLevelTextField.delegate = self;
    self.userTextField.delegate = self;
    
    if(_classLevelArray != nil) {
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<_classLevelArray.count; i++) {
            SchoolLevelModel *model = [_classLevelArray objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:_selectedClassLevelIndex] getClassLevelName];

    }
    else {
        _selectedClassLevelIndex = -1;
        _selectedClassLevelIndex = -1;
        
        // Call api
       // [self getSchoolClassLevel];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)actionNext:(id)sender {
    if([self validateData]) {
        [self gotoSelectStudentsPage];
    }

    
}
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"xxxxxxxxxxx");
   category = [NSArray arrayWithObjects:@"แบบกลุ่ม",@"รายบุคคล",nil];
  
    
    if(textField.tag == 1) { // User press class level textfield
        NSLog(@"Level = %@", category);
        if(category != nil && category.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:category];
        }
        return NO;
    }
    else if(textField.tag == 2) { // User press classroom textfield
        
        if ([self.classLevelTextField.text  isEqual: @"แบบกลุ่ม"]) {
            recipient =[NSArray arrayWithObjects:@"ทั้งหมด",@"พนักงาน/อาจารย์ ทั้งหมด",@"อาจารย์ทั้งหมด",@"พนักงานทั้งหมด",@"นักเรียนทั้งหมด",@"เลือกนักเรียนตามลำดับชั้น",nil];
            if(self.classLevelTextField.text.length == 0) {
                NSString *alertMessage = @"กรุณาเลือกรูปแบบการส่ง";
                [self showAlertDialogWithMessage:alertMessage];
            }
            else if(recipient != nil && recipient.count > 0) {
                [self showTableListDialogWithRequestCode:classroomRequestCode data:recipient];
            }

            
        }else{
            recipient =[NSArray arrayWithObjects:@"อาจารย์",@"พนักงาน",@"นักเรียน",nil];
            if(self.classLevelTextField.text.length == 0) {
                NSString *alertMessage = @"กรุณาเลือกรูปแบบการส่ง";
                [self showAlertDialogWithMessage:alertMessage];
            }
            else if(recipient != nil && recipient.count > 0) {
                [self showTableListDialogWithRequestCode:classroomRequestCode data:recipient];
            }

        }
        
        
        
        
        return NO;
    }
    
    return YES;
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}
#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
    
    if([requestCode isEqualToString:classLevelRequestCode]) {
        
       
        
        self.classLevelTextField.text = [category objectAtIndex:index];
        
        
    }
    if([requestCode isEqualToString:classroomRequestCode]) {
        
        NSLog(@"classroomRequestCode = %@" , classroomRequestCode );
        NSLog(@"requestCode = %@" , requestCode );
        
       
        self.userTextField.text = [recipient objectAtIndex:index];
        
        
    }

    
}

#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกรูปแบบการส่ง";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.userTextField.text.length == 0) {
        NSString *alertMessage = @"กรุณาเลือกผู้รับ";
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}


- (void)gotoSelectStudentsPage {
    
    
    if([self.userTextField.text  isEqual: @"ทั้งหมด"]){
        SNClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    }
    else if([self.userTextField.text  isEqual: @"พนักงาน/อาจารย์ ทั้งหมด"]){
        SNClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    }
    else if([self.userTextField.text  isEqual: @"อาจารย์ทั้งหมด"]){
        SNClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    }
    else if([self.userTextField.text  isEqual: @"พนักงานทั้งหมด"]){
        SNClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else if([self.userTextField.text  isEqual: @"นักเรียนทั้งหมด"]){
        SNClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTimeOutViewMainStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else if([self.userTextField.text  isEqual: @"เลือกนักเรียนตามลำดับชั้น"]){
        SNClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNClassLevelGroupStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
//รายบุคคล
    else if([self.userTextField.text  isEqual: @"นักเรียน"]){
        SNClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNClassLevelGroupStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else if([self.userTextField.text  isEqual: @"อาจารย์"]){
        SNSelectTheaCherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNSelectTheacherStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    
   
}






@end
