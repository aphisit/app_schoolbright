//
//  DelUserAccountTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUIImageView.h"

@class DelUserAccountTableViewCell;

@protocol DelUserAccountTableViewCellDelegate <NSObject>

- (void)onDeleteUser:(DelUserAccountTableViewCell *)tableViewCell atIndex:(NSInteger)index;

@end

@interface DelUserAccountTableViewCell : UITableViewCell

@property (nonatomic, weak) id<DelUserAccountTableViewCellDelegate> delegate;

@property (nonatomic) NSInteger index;

@property (weak, nonatomic) IBOutlet CustomUIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnDel;

- (IBAction)actionDeleteUser:(id)sender;

@end
