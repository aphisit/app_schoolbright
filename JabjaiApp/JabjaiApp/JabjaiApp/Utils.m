//
//  Utils.m
//  JabjaiApp
//
//  Created by mac on 10/28/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "Utils.h"
#import <AFNetworking/AFNetworking.h>

@interface Utils () {
}

@end

@implementation Utils

+ (void)downloadDataFromURL:(NSURL *)url withCompletionHandler:(void(^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        //        if(error != nil) {
        //            // If any error occurs then just display its description on the console.
        //            NSLog(@"%@", [error localizedDescription]);
        //        }
        //        else {
        //            // If no error occurs, check the HTTP status code.
        //            NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
        //
        //            // If it's other than 200, then show it on the console.
        //            if(HTTPStatusCode != 200) {
        //                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
        //            }
        //        }
        
        // Call the completion handler with the returned data on the main thread.
        [[NSOperationQueue mainQueue] addOperationWithBlock:^(void) {
            completionHandler(data, response, error);
        }];
        
    }];
    
    [task resume];
    
}

+ (void)uploadImageFromURL:(NSURL *)url data:(NSData *)data imageparameterName:(NSString *)imageParameterName imageFileName:(NSString *)imageFileName mimeType:(MIME_TYPE)mimeType parameters:(NSDictionary<NSString *, NSString *> *)parameters withCompletionHandler:(void(^)(id responseObject, NSError *error))completionHandler {
    
    NSString *mimeTypeStr;
    
    if(mimeType == JPEG) {
        mimeTypeStr = @"image/jpeg";
    }
    else {
        mimeTypeStr = @"image/png";
    }
    
    NSError *error;
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url.absoluteString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if(data != nil) {
            
            [formData appendPartWithFileData:data name:imageParameterName fileName:imageFileName mimeType:mimeTypeStr];
            //[formData appendPartWithFileURL:imageURL name:imageParameterName fileName:[imageURL lastPathComponent]  mimeType:mimeTypeStr error:nil];
        }
    } error:&error];
    
    if(error != nil) {
        completionHandler(nil, error);
        return;
    }

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager uploadTaskWithStreamedRequest:request progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        NSLog(@"Response %@", responseObject);
        completionHandler(responseObject, error);
        
    }];
    
    [uploadTask resume];
}

+(BOOL)isValidEmail:(NSString *)email {
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
}

+(BOOL)stringIsNumeric:(NSString *)str {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *number = [formatter numberFromString:str];
    return !!number; // If the string is not numeric, number will be nil
}

+(BOOL)isInteger:(NSString *)str {
    
    BOOL isNumeric = [Utils stringIsNumeric:str];
    
    if(isNumeric) {
        double number = [str doubleValue];
        double d = 1;
        
        if(modf(number, &d) == 0.0) {
            return YES;
        }
        else {
            return NO;
        }
    }
    else {
        return NO;
    }
    
}

+(BOOL)isIntegerWithDouble:(double)number {
    double d = 1;
    
    if(modf(number, &d) == 0.0) {
        return YES;
    }
    else {
        return NO;
    }
}

+(NSString *)encodeString:(NSString *)str {
    NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
    NSString *strEncoded = [str stringByAddingPercentEncodingWithAllowedCharacters:set];
    return strEncoded;
}

+(NSString *)getSeverDateTimeFormat {
    
    //2016-08-01T00:00:00
    NSString *datetimeformat = @"yyyy-MM-dd'T'HH:mm:ss";
    return datetimeformat;
    
}

+(NSString *)dateToServerDateFormat:(NSDate *)date {
    NSString *serverFormat = @"MM/dd/yyyy";
    NSString *serverDateStr = [self dateInStringFormat:date format:serverFormat];
    
    return serverDateStr;
}

+(NSString *)getXMLDateFormat {
    NSString *xmlDateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    return xmlDateFormat;
}

+(NSArray *)getThaiMonthRangeWithStartMonth:(NSInteger)startMonth endMonth:(NSInteger)endMonth {
    
    if(startMonth < 1 || endMonth > 12) {
        return nil;
    }
    
    NSArray *thaiMonths = @[@"มกราคม", @"กุมภาพันธ์", @"มีนาคม", @"เมษายน", @"พฤษภาคม", @"มิถุนายน", @"กรกฎาคม", @"สิงหาคม", @"กันยายน", @"ตุลาคม", @"พฤศจิกายน", @"ธันวาคม"];
    
    NSMutableArray *monthsRange = [[NSMutableArray alloc] init];
    
    NSInteger counter = startMonth - 1;
    
    do {
        [monthsRange addObject:[thaiMonths objectAtIndex:counter]];
        
        counter = (counter + 1) % 12;
    } while (counter != endMonth);
    
    return monthsRange;
}

+(NSInteger)getThaiMonthNumberWithName:(NSString *)month {
    NSArray *thaiMonths = @[@"มกราคม", @"กุมภาพันธ์", @"มีนาคม", @"เมษายน", @"พฤษภาคม", @"มิถุนายน", @"กรกฎาคม", @"สิงหาคม", @"กันยายน", @"ตุลาคม", @"พฤศจิกายน", @"ธันวาคม"];
    
    for(int i=0; i<thaiMonths.count; i++) {
        if([[thaiMonths objectAtIndex:i] isEqualToString:month]) {
            return i+1;
        }
    }
    
    return -1;
}

+(NSString *)dateInStringFormat:(NSDate *)date {
    
    NSDateFormatter *dateFormatter = [self getDateFormatter];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    
    return strDate;
}

+(NSString *)dateInStringFormat:(NSDate *)date format:(NSString *)format {
    
    NSDateFormatter *dateFormatter = [self getDateFormatter];
    [dateFormatter setDateFormat:format];
    NSString *strDate = [dateFormatter stringFromDate:date];
    
    return strDate;
}

+(NSDate *)parseServerDateStringToDate:(NSString *)dateStr {
    
    NSDateFormatter *formatter = [self getDateFormatter];
    [formatter setDateFormat:[Utils getXMLDateFormat]];
    
    NSDateFormatter *formatter2 = [self getDateFormatter];
    [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
    
    NSDate *parseDate;
    
    NSRange dotRange = [dateStr rangeOfString:@"."];
    
    if(dotRange.length != 0) {
        parseDate = [formatter dateFromString:dateStr];
    }
    else {
        parseDate = [formatter2 dateFromString:dateStr];
    }
    
    return parseDate;
}

+ (NSDateFormatter *)getDateFormatter {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    formatter.locale = locale;
    return formatter;
}

+ (NSCalendar *)getGregorianCalendar {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    return calendar;
}

+ (NSString *)getThaiDateFormatWithDate:(NSDate *)date {
    
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSArray *thaiMonths = @[@"มกราคม", @"กุมภาพันธ์", @"มีนาคม", @"เมษายน", @"พฤษภาคม", @"มิถุนายน", @"กรกฏาคม", @"สิงหาคม", @"กันยายน", @"ตุลาคม", @"พฤศจิกายน", @"ธันวาคม"];
    NSString *dateStr = [[NSString alloc] initWithFormat:@"%02li %@ %li", day, thaiMonths[month - 1], year + 543];
    
    return dateStr;
}

+ (NSString *)getBuddhistEraFormatWithDate:(NSDate *)date {
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSString *dateStr = [[NSString alloc] initWithFormat:@"%02li/%02li/%li", day, month, year + 543];

    return dateStr;
}

+ (unsigned)parseIntFromData:(NSData *)data {
    NSString *dataDescription = [data description];
    NSString *dataAsString = [dataDescription substringWithRange:NSMakeRange(1, [dataDescription length] - 2)];
    
    unsigned intData = 0;
    NSScanner *scanner = [NSScanner scannerWithString:dataAsString];
    [scanner scanHexInt:&intData];
    
    return intData;
}

+ (NSString *)getStudentStatusJSON:(NSArray<TAStudentStatusModel *> *)studentStatusArray {
    
    if(studentStatusArray == nil || studentStatusArray.count == 0) {
        return @"{\"rootobject\":[]}";
    }
    
    NSMutableString *jsonString = [[NSMutableString alloc] initWithString:@"{\"rootobject\":["];
    
    BOOL hasData = NO;
    
    for(TAStudentStatusModel *model in studentStatusArray) {
        
        if([model getScanStatus] != -1) {
            long long userId = [model getUserId];
            int scanStatus = [model getScanStatus];
            
            NSString *jsonObjStr = [NSString stringWithFormat:@"{\"UserId\":%lld,\"scanstatus\":%i},", userId, scanStatus];
            
            [jsonString appendString:jsonObjStr];
            hasData = YES;
        }
        
    }
    
    if(hasData) {
        [jsonString deleteCharactersInRange:[jsonString rangeOfString:@"," options:NSBackwardsSearch]];
    }
    
    [jsonString appendString:@"]}"];
    
    return jsonString;
}

+ (NSString *)getStudentsBehaviorsScoreJSONWithSelectedStudentArray:(NSArray<BSSelectedStudent *> *)selectedStudentsArray selectedBehaviorsScoreArray:(NSArray<BehaviorScoreModel *> *)selectedBehaviorsScoreArray {
    
    NSMutableString *behaviorIds = [[NSMutableString alloc] initWithString:@"["];
    
    if(selectedBehaviorsScoreArray != nil) {
        
        for(int i=0; i<selectedBehaviorsScoreArray.count; i++) {
            
            BehaviorScoreModel *model = [selectedBehaviorsScoreArray objectAtIndex:i];
            
            [behaviorIds appendFormat:@"%lld", [model getBehaviorScoreId]];
            
            if(i+1 != selectedBehaviorsScoreArray.count) {
                [behaviorIds appendString:@","];
            }
        }
    }
    
    [behaviorIds appendString:@"]"];
    
    NSMutableString *studentIds = [[NSMutableString alloc] initWithString:@"["];
    
    if(selectedStudentsArray != nil) {
        
        for(int i=0; i<selectedStudentsArray.count; i++) {
            
            BSSelectedStudent *model = [selectedStudentsArray objectAtIndex:i];
            
            [studentIds appendFormat:@"%lld", [model getStudentId]];
            
            if(i+1 != selectedStudentsArray.count) {
                [studentIds appendString:@","];
            }
        }
    }
    
    [studentIds appendString:@"]"];
    
    NSString *jsonString = [[NSString alloc] initWithFormat:@"{\"studentId\" : %@, \"BehaviorsId\" : %@}", studentIds, behaviorIds];
    
    return jsonString;
}

+ (NSInteger)compareVersion:(NSString *)version1 version2:(NSString *)version2 {
    NSInteger compareResult = 0;
    BOOL isEqual = YES;
    
    NSArray *v1 = [version1 componentsSeparatedByString:@"."];
    NSArray *v2 = [version2 componentsSeparatedByString:@"."];
    
    NSInteger min = MIN(v1.count, v2.count);
    
    for(int i=0; i<min; i++) {
        NSInteger n1 = [[v1 objectAtIndex:i] integerValue];
        NSInteger n2 = [[v2 objectAtIndex:i] integerValue];
        
        if(n1 > n2) {
            compareResult = 1;
            isEqual = NO;
            break;
        }
        else if(n1 < n2) {
            compareResult = -1;
            isEqual = NO;
            break;
        }
    }
    
    if(isEqual) {
        if(v1.count > v2.count) {
            compareResult = 1;
        }
        else if(v1.count < v2.count) {
            compareResult = -1;
        }
        else {
            compareResult = 0;
        }
    }
    
    return compareResult;
}


@end
