//
//  UniversityFacultyModel.m
//  JabjaiApp
//
//  Created by mac on 11/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "UniversityFacultyModel.h"

@implementation UniversityFacultyModel

-(NSNumber *)getFacultyID {
    return self.facultyID;
}

-(NSString *)getFacultyName {
    return self.facultyName;
}

@end
