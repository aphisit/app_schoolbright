//
//  TAReportStudentStatusTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TAReportStudentStatusTableViewCell.h"

@interface TAReportStudentStatusTableViewCell () {
    UIColor *greenColor;
    UIColor *yellowColor;
    UIColor *redColor;
    UIColor *pinkColor;
    UIColor *lightBluecolor;
    UIColor *blueColor;
    UIColor *grayColor;
}

@end
@implementation TAReportStudentStatusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    greenColor = [UIColor colorWithRed:0/255.0 green:201/255.0 blue:56/255.0 alpha:1.0];
    yellowColor = [UIColor colorWithRed:230/255.0 green:147/255.0 blue:29/255.0 alpha:1.0];
    redColor = [UIColor colorWithRed:252/255.0 green:53/255.0 blue:46/255.0 alpha:1.0];
    blueColor = [UIColor colorWithRed:0/255.0 green:141/255.0 blue:197/255.0 alpha:1.0];
    pinkColor = [UIColor colorWithRed:233/255.0 green:203/255.0 blue:237/255.0 alpha:1.0];
    lightBluecolor = [UIColor colorWithRed:152/255.0 green:189/255.0 blue:229/255.0 alpha:1.0];
    grayColor = [UIColor colorWithRed:223/255.0 green:233/255.0 blue:235/255.0 alpha:1.0];
    
    CGFloat max = MAX(self.statusButton.frame.size.width, self.statusButton.frame.size.height);
    self.statusButton.layer.cornerRadius = max/2.0;
    self.statusButton.layer.masksToBounds = YES;
    self.statusButton.titleLabel.numberOfLines = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateStatus:(NSInteger)status {
    switch (status) {
        case 0:
            [self.statusButton setTitle:@"เข้าเรียน" forState:UIControlStateNormal];
            [self.statusButton setBackgroundColor:greenColor];
            break;
        case 1:
            [self.statusButton setTitle:@"สาย" forState:UIControlStateNormal];
            [self.statusButton setBackgroundColor:yellowColor];
            break;
        case 3:
            [self.statusButton setTitle:@"ขาด" forState:UIControlStateNormal];
            [self.statusButton setBackgroundColor:redColor];
            break;
        case 6:
            [self.statusButton setTitle:@"กิจกรรม" forState:UIControlStateNormal];
            [self.statusButton setBackgroundColor:blueColor];
            break;
        case 4:
            [self.statusButton setTitle:@"ลากิจ" forState:UIControlStateNormal];
            [self.statusButton setBackgroundColor:pinkColor];
            break;
        case 5:
            [self.statusButton setTitle:@"ลาป่วย" forState:UIControlStateNormal];
            [self.statusButton setBackgroundColor:lightBluecolor];
            break;
        default:
            [self.statusButton setTitle:@"ไม่ระบุ\nสถานะ" forState:UIControlStateNormal];
            [self.statusButton setBackgroundColor:grayColor];
            break;
    }

}
@end
