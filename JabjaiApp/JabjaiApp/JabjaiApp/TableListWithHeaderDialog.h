//
//  TableListWithHeaderDialog.h
//  JabjaiApp
//
//  Created by mac on 11/5/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableListViewCell.h"
#import "TableListHeaderViewCell.h"

@protocol TableListWithHeaderDialogDelegate

-(void)onItemSelectWithRequestCode:(NSString *)requestCode headerSectionString:(NSString *)headerSectionString selectedString:(NSString *)selectedString;

@end

@interface TableListWithHeaderDialog : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id<TableListWithHeaderDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tblView;

- (id)initWithRequestCode:(NSString *)requestCode;
- (void)showDialogInView:(UIView *)targetView stringDataDict:(NSDictionary *)dataDict dataDictKeys:(NSArray *)dataKeys;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end
