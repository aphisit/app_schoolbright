//
//  SNClassLevelViewController.m
//  JabjaiApp
//
//  Created by toffee on 8/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SNClassLevelViewController.h"
#import "EXReportClassHeaderTableViewCell.h"
#import "EXReportClassChildTableViewCell.h"
#import "EXReportButtonTableViewCell.h"
#import "SNLevelStudentViewController.h"
#import "UserData.h"
#import "Utils.h"
#import "EXReportStudyClassModel.h"
#import "ExecutiveReportClassLevelViewController.h"
#import "ExecutiveReportTeacherListViewController.h"
#import "ExecutiveReportClassroomViewController.h"

@interface SNClassLevelViewController (){
    NSArray *key;
    NSDictionary *d;
    NSInteger *num;
    NSMutableArray *classLevelStringArray;
    NSMutableArray *expandedSections;
    NSMutableDictionary<NSNumber *, EXReportStudyClassModel *> *studyClassDict;
    NSMutableArray *sectionKeys;
    NSMutableArray *classroomStringArrray;
    
}

@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolClassLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;

@end
static NSString *headerCellIdentifier = @"HeaderCell";
static NSString *childCellIdentifier = @"ChildCell";
static NSString *buttonCellIdentifier = @"ButtonCell";
static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";

@implementation SNClassLevelViewController
@synthesize classLevelId = _classLevelId;
@synthesize classroomId = _classroomId;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getSchoolClassLevel];
    
    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([EXReportClassHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:headerCellIdentifier];
    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([EXReportClassChildTableViewCell class]) bundle:nil] forCellReuseIdentifier:childCellIdentifier];
    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([EXReportButtonTableViewCell class]) bundle:nil] forCellReuseIdentifier:buttonCellIdentifier];
    
    

    
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    self.tableview.tableFooterView = [[UIView alloc] init];
    self.tableview.separatorInset = UIEdgeInsetsZero;
    
    classroomStringArrray = [[NSMutableArray alloc] init];
    
//    for(int i=0; i<_classroomArray.count; i++) {
//        SchoolClassroomModel *model = [_classroomArray objectAtIndex:i];
//        [classroomStringArrray addObject:[model getClassroomName]];
//    }


     //[self prepareData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - API Caller

- (void)getSchoolClassLevel {
    
    sectionKeys = [[NSMutableArray alloc] init];
    expandedSections = [[NSMutableArray alloc] init];
    studyClassDict = [[NSMutableDictionary alloc] init];

    //[sectionKeys addObject:@("1")];
    if(self.callGetSchoolClassLevelAPI != nil) {
        self.callGetSchoolClassLevelAPI = nil;
    }
    self.callGetSchoolClassLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolClassLevelAPI.delegate = self;
    [self.callGetSchoolClassLevelAPI call:[UserData getSchoolId]];
    
    
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    num = classLevelId;
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
    
    
   
    
    
}

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    
    if(success && data != nil) {
        _classLevelArray = data;
        NSLog(@"classroomStringArrray = %@",classroomStringArrray);
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
        [self.tableview reloadData];
        NSLog(@"Count = %d", classroomStringArrray.count);
        NSLog(@"num = %d", num);
        [self.tableview reloadSections:[NSIndexSet indexSetWithIndex:num] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark - CallGetSchoolClassLevelAPIDelegate

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    //NSLog(@"numberOfRowsInSection returning");
    
    if(success && data != nil) {
        
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        classLevelStringArray = [[NSMutableArray alloc] init];
        EXReportStudyClassModel *studyClassModel = [[EXReportStudyClassModel alloc] init];
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
            [sectionKeys addObject:@([model getClassLevelId])];
            [studyClassDict setObject:studyClassModel forKey:@([model getClassLevelId])];
            
        }
        
        [self.tableview reloadData];
        
    }
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(classLevelStringArray == nil) {
        return 0;
    }
    else {
       
        return classLevelStringArray.count;
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([expandedSections containsObject:@(section)]) {
        NSNumber *key = [classLevelStringArray objectAtIndex:section];
        EXReportStudyClassModel *studyClassModel = [studyClassDict objectForKey:key];
        NSLog(@"studenclassmodel = %@",[studyClassModel getReportStatusDetailArray]);
        if([studyClassModel getReportStatusDetailArray] != nil) {
            return [[studyClassModel getReportStatusDetailArray] count] + 1  ; // +1 header for each section
        }
        else {
            
            //return 4 ;
            return classroomStringArrray.count+1;
        }
    }
    else {
        return 1; // show only header
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *key = [sectionKeys objectAtIndex:indexPath.section];
    EXReportStudyClassModel *studyClassModel = [studyClassDict objectForKey:key]; 
   NSLog(@"indexpathrow1 = %d",indexPath.row);
    if(indexPath.row == 0) { // For the first row of each section we'll show header
        
        EXReportClassHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier forIndexPath:indexPath];
        BOOL isExpanded = [expandedSections containsObject:@(indexPath.section)];
        if(isExpanded) {
            cell.imgArrow.image = [UIImage imageNamed:@"ic_arrow_down"];
        }
        else {
            cell.imgArrow.image = [UIImage imageNamed:@"ic_arrow_left"];
            //num = 0;
        }
        
        cell.titleLabel.text = [classLevelStringArray objectAtIndex:indexPath.section];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.userInteractionEnabled = YES;
        return cell;
    
    }
    else {
        NSLog(@"indexpathrow2 = %d",indexPath.row);
        EXReportStatusDetailModel *statusDetailModel = [[studyClassModel getReportStatusDetailArray] objectAtIndex:indexPath.row - 1];// -1
        if([statusDetailModel getIsButton]) {
            EXReportButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:buttonCellIdentifier forIndexPath:indexPath];
            cell.titleLabel.text = [statusDetailModel getTitle];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userInteractionEnabled = YES;
            
            return cell;
        }
        else {
            EXReportClassChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:childCellIdentifier forIndexPath:indexPath];
            //cell.titleLabel.text = @"xxxx";
            cell.titleLabel.text = [classroomStringArrray objectAtIndex:indexPath.row-1];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userInteractionEnabled = NO;
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   // int section = indexPath.section;
   
    if([expandedSections containsObject:@(indexPath.section)]) {
        //NSLog(@"index1 = %d",indexPath.row);
        if(indexPath.row == 0) { // If select header of each section, then toggle up and down
            [expandedSections removeObject:@(indexPath.section)];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
            ;
        }
        else {
            NSNumber *key = [sectionKeys objectAtIndex:indexPath.section];
            EXReportStudyClassModel *studyClassModel = [studyClassDict objectForKey:key];
            EXReportStatusDetailModel *statusDetailModel = [[studyClassModel getReportStatusDetailArray] objectAtIndex:indexPath.row - 1];
            
            if([statusDetailModel getIsButton]) {
                if ([statusDetailModel getIId] == -1) { // Teacher type
                    
                    ExecutiveReportTeacherListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecutiveReportTeacherListStoryboard"];
                    viewController.reportDate = _reportDate;
                    viewController.reportClassStatusArray = _reportClassStatusArray;
                    viewController.mode = EXREPORT_TEACHER_LIST;
                    
                    [self.revealViewController pushFrontViewController:viewController animated:YES];
                }
                else if ([statusDetailModel getIId] == -2) { // Employee type
                    
                    ExecutiveReportTeacherListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecutiveReportTeacherListStoryboard"];
                    viewController.reportDate = _reportDate;
                    viewController.reportClassStatusArray = _classLevelArray;
                    viewController.mode = EXREPORT_EMPLOYEE_LIST;
                    
                    [self.revealViewController pushFrontViewController:viewController animated:YES];
                }
                else { // Student class level
                    long long classLevelId = [statusDetailModel getIId];
                    NSString *classLevelName = [studyClassModel getTitle];
                    
                    ExecutiveReportClassroomViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecutiveReportClassroomStoryboard"];
                    viewController.reportDate = _reportDate;
                    viewController.classLevelName = _classLevelArray;
                    viewController.selectedClassLevelId = classLevelId;
                    viewController.classLevelName = classLevelName;
                    
                    [self.revealViewController pushFrontViewController:viewController animated:YES];
                }
            }
        }
    }
    else {
       
        [expandedSections addObject:@(indexPath.section)];
        _classLevelId = 0;
        NSLog(@"index = %d",indexPath.section);
        classroomStringArrray = nil;
        [self getSchoolClassroomWithClassLevelId:indexPath.section+1];
        
        
       
    }
}
#pragma mark - Utility

- (IBAction)moveBack:(id)sender {
    
    SNLevelStudentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SendNewsStoryboard"];
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
@end
