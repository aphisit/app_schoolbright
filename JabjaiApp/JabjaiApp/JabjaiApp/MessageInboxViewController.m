//
//  MessageInboxViewController.m
//  JabjaiApp
//
//  Created by mac on 11/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "MessageInboxViewController.h"
#import "SWRevealViewController.h"
#import "MessageHeaderTableViewCell.h"
#import "MessageChildTableViewCell.h"
#import "MessageInboxDataModel.h"
#import "APIURL.h"
#import "DateUtility.h"
#import "Utils.h"
#import "InboxMessageDBManager.h"
#import "NavigationViewController.h"
#import "Constant.h"

static NSString *KEY_TODAY = @"TODAY";
static NSString *KEY_YESTERDAY = @"YESTERDAY";
static NSString *KEY_LAST7DAYS = @"LAST7DAYS";
static NSString *KEY_LAST30DAYS = @"LAST30DAYS";
static NSString *KEY_OLDER = @"OLDER";

@interface MessageInboxViewController () {
    
    NSMutableArray *sectionKeys;
    NSMutableArray *expandedSections;
    NSMutableDictionary *messageInboxPerDays; // key (sectionkeys) -> NSArray [MessageInboxDataModel...];
    
    int todayTotalMessage;
    int yesterdayTotalMessage;
    int last7daysTotalMessage;
    int last30daysTotalMessage;
    int olderTotalMessage;
    
    UIColor *unreadBGColor;
    UIColor *readBGColor;
    
    NSInteger connectCounter;
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property (nonatomic, strong) MessageInboxDialog *messageInboxDialog;
@property (nonatomic, strong) MessageInboxLongTextDialog *messageInboxLongTextDialog;
@property (nonatomic, strong) MessageInBoxScrollableTextDialog *messageInboxScrollableTextDialog;

@property (nonatomic, strong) InboxMessageDBManager *inboxMessageDatabaseManager;

@end

@implementation MessageInboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    unreadBGColor = [UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1.0];
    readBGColor = [UIColor whiteColor];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    if(self.inboxType == ATTENDANCE) {
        [self.titleBarLabel setText:@"แจ้งเตือนเวลาเรียน"];
    }
    else if(self.inboxType == PURCHASING) {
        [self.titleBarLabel setText:@"แจ้งเตือนการซื้อขาย"];
    }
    else if(self.inboxType == TOPUP) {
        [self.titleBarLabel setText:@"แจ้งเตือนการเติมเงิน"];
    }
    else if(self.inboxType == ABSENCEREQUEST) {
        [self.titleBarLabel setText:@"แจ้งลา"];
    }
    else if(self.inboxType == NEWS) {
        [self.titleBarLabel setText:@"ข้อมูลข่าวสาร"];
    }
    
    // configure table view
    self.tblExpandable.delegate = self;
    self.tblExpandable.dataSource = self;
    
    [self.tblExpandable registerNib:[UINib nibWithNibName:NSStringFromClass([MessageHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"HeaderCell"];
    [self.tblExpandable registerNib:[UINib nibWithNibName:NSStringFromClass([MessageChildTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.tblExpandable setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self.tblExpandable setSeparatorColor:[UIColor clearColor]];
    
    // set up formatter
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    // initial table expandable variables
    sectionKeys = [[NSMutableArray alloc] init];
    expandedSections = [[NSMutableArray alloc] init];
    messageInboxPerDays = [[NSMutableDictionary alloc] init];
    
    // setup the dialog
    self.messageInboxDialog = [[MessageInboxDialog alloc] init];
    self.messageInboxDialog.delegate = self;
    self.messageInboxLongTextDialog = [[MessageInboxLongTextDialog alloc] init];
    self.messageInboxLongTextDialog.delegate = self;
    self.messageInboxScrollableTextDialog = [[MessageInBoxScrollableTextDialog alloc] init];
    self.messageInboxScrollableTextDialog.delegate = self;
    
    connectCounter = 0;
    
    self.inboxMessageDatabaseManager = [[InboxMessageDBManager alloc] init];
    
    //[self.inboxMessageDatabaseManager deleteAllDataInTable];
    [self groupMessage];
    [self getMessageData];
    [self showMessageWhenInit];
    
    NSLog(@"%@", @"End View Did Load");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

#pragma mark - TableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(messageInboxPerDays != nil) {
        return messageInboxPerDays.count;
    }
    
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([expandedSections containsObject:@(section)]) {
        NSString *key = [sectionKeys objectAtIndex:section];
        return [[messageInboxPerDays objectForKey:key] count] + 1; // plus 1 add header of each section
    }
    else {
        return 1; // If section is not expanded show only header
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *key = [sectionKeys objectAtIndex:indexPath.section];
    
    if(indexPath.row == 0) { // For the 1st row of each section show header
        
        MessageHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell" forIndexPath:indexPath];
        
        BOOL isExpaned = [expandedSections containsObject:@(indexPath.section)];
        
        if(isExpaned) {
            cell.arrowImage.image = [UIImage imageNamed:@"ic_sort_up"];
        }
        else {
            cell.arrowImage.image = [UIImage imageNamed:@"ic_sort_down"];
        }
        
        
        if([key isEqualToString:KEY_TODAY]) {
            
            NSString *todayDateStr = [self.dateFormatter stringFromDate:[NSDate date]];
            
            cell.titleLabel.text = [NSString stringWithFormat:@"Today (%d)", todayTotalMessage];
            cell.dateLabel.text = todayDateStr;
            
        }
        else if([key isEqualToString:KEY_YESTERDAY]) {
            
            NSDate *yesterday = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-1 toDate:[NSDate date] options:0];
            
            NSString *yesterdayStr = [self.dateFormatter stringFromDate:yesterday];
            cell.titleLabel.text = [NSString stringWithFormat:@"Yesterday (%d)", yesterdayTotalMessage];
            cell.dateLabel.text = yesterdayStr;
            
        }
        else if([key isEqualToString:KEY_LAST7DAYS]) {
            
            NSDate *last7days = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-6 toDate:[NSDate date] options:0];
            
            NSString *last7daysStr = [self.dateFormatter stringFromDate:last7days];
            cell.titleLabel.text = [NSString stringWithFormat:@"Last 7 days (%d)", last7daysTotalMessage];
            cell.dateLabel.text = last7daysStr;
            
        }
        else if([key isEqualToString:KEY_LAST30DAYS]) {
            
            NSDate *last30days = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-29 toDate:[NSDate date] options:0];
            
            NSString *last30daysStr = [self.dateFormatter stringFromDate:last30days];
            cell.titleLabel.text = [NSString stringWithFormat:@"Last 30 days (%d)", last30daysTotalMessage];
            cell.dateLabel.text = last30daysStr;
            
        }
        else if([key isEqualToString:KEY_OLDER]) {
            
            NSDate *olderdays = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-30 toDate:[NSDate date] options:0];
            
            NSString *olderdaysStr = [self.dateFormatter stringFromDate:olderdays];
            cell.titleLabel.text = [NSString stringWithFormat:@"Older (%d)", olderTotalMessage];
            cell.dateLabel.text = olderdaysStr;
        }
        
        return cell;
    }
    else {
        
        MessageChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
        NSArray *allMessageInboxModels = [messageInboxPerDays objectForKey:key];
        MessageInboxDataModel *model = [allMessageInboxModels objectAtIndex:indexPath.row-1];
        
        NSString *timeStr = [self.timeFormatter stringFromDate:model.date];
        cell.titleLabel.text = model.title;
        cell.messageLabel.text = model.message;
        cell.timeLabel.text = timeStr;
        
        if(model.status == 0) { // Message unread
            cell.backgroundColor = unreadBGColor;
        }
        else {
            cell.backgroundColor = readBGColor;
        }
        
        return cell;
    }
}

#pragma mark - TableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if(indexPath.row == 0) {
        return 44.0;
    }
    else {
        return 60;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([expandedSections containsObject:@(indexPath.section)]) {
        NSLog(@"Section %d expanded", (int)indexPath.section);
        
        if(indexPath.row == 0) { // when tap header of each section, then toggle up or down
            [expandedSections removeObject:@(indexPath.section)];
            [self.tblExpandable reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
        else {
            
            NSString *key = [sectionKeys objectAtIndex:indexPath.section];
            NSArray *allMessageInboxModels = [messageInboxPerDays objectForKey:key];
            MessageInboxDataModel *model = [allMessageInboxModels objectAtIndex:indexPath.row-1];
            
            [self showMessage:model];
            
            [self.tblExpandable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            NSLog(@"Select %@ : %@", model.title, model.message);
        }
    }
    else {
        [expandedSections addObject:@(indexPath.section)];
        [self.tblExpandable reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - DialogDelegate
-(void)messageInboxDialogClose {
    NSLog(@"MessageInboxDialog Close");
}

-(void)messageInboxLongTextDialogClose {
    NSLog(@"MessageInboxLongTextDialog Close");
}

-(void)messageInBoxScrollableTextClose {
    NSLog(@"MessageInboxScrollableTextClose");
}

#pragma mark - Get API Data
- (void)getMessageData {
    
    [self showIndicator];
    
    int lastestMessageID = [self.inboxMessageDatabaseManager getLatestMessageIDWithUserID:(int)[UserData getUserID]];
    NSInteger messageType = [self getMessageType:self.inboxType];
    
    NSString *URLString = [APIURL getMessageInboxURLWithLatestID:lastestMessageID messageType:messageType];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getMessageData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageData];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                int countMessage = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    int status = [[dataDict objectForKey:@"nStatus"] intValue];
                    int type = [[dataDict objectForKey:@"nType"] intValue];
                    NSMutableString *messageDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dSend"]];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sTitle"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    NSRange dotRange = [messageDateStr rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:messageDateStr];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:messageDateStr];
                    }
                    
                    MessageInboxDataModel *model = [[MessageInboxDataModel alloc] init];
                    model.messageID = messageID;
                    model.userID = (int)[UserData getUserID];
                    model.messageType = type;
                    model.status =status;
                    model.title = title;
                    model.message = message;
                    model.date = messageDate;
                    
                    [newDataArr addObject:model];
                    
                    //NSLog(@"%@", [NSString stringWithFormat:@"message id : %i", model.messageID]);
                    countMessage++;
                }
                
                if(newDataArr.count > 0) {
                    // update message into database
                    BOOL success = [self.inboxMessageDatabaseManager insertMultipleMessage:newDataArr];
                    
                    // If there is a problem then clean table and insert again
                    if(!success) {
                        [self.inboxMessageDatabaseManager deleteAllDataInTable];
                        [self.inboxMessageDatabaseManager insertMultipleMessage:newDataArr];
                    }
                    
                }
                
                NSLog(@"%@", [NSString stringWithFormat:@"Total Message Get From Server : %i", countMessage]);
                
                [self groupMessage];
            }

        }
        
        [self stopIndicator];
        
    }];
}

-(void)updateReadStatus:(MessageInboxDataModel *)model {
    
    if(model.status == 0) { //This message still unread
        
        model.status = 1; // set message read
        [self.inboxMessageDatabaseManager updateMessageStatusWithID:model.messageID status:1 userID:model.userID];
        
        long long userID = [UserData getUserID];
        NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:model.messageID userID:userID];
        NSURL *URL = [NSURL URLWithString:URLString];
        
        [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
                
                if(error != nil) {
                    NSLog(@"An error occured : %@" , [error localizedDescription]);
                }
                else if(data == nil) {
                    NSLog(@"Data is null");
                }
                else {
                    NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                    NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
                }
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateReadStatus:model];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if(error != nil) {
                    NSLog(@"An error occured : %@", [error localizedDescription]);
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self updateReadStatus:model];
                    }
                    else {
                        connectCounter = 0;
                    }
                    
                }
                else if(![returnedData isKindOfClass:[NSArray class]]) {
                    
                    if(connectCounter < TRY_CONNECT_MAX) {
                        connectCounter++;
                        
                        NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                        [self updateReadStatus:model];
                    }
                    else {
                        connectCounter = 0;
                    }
                    
                }
                else {
                    
                    NSArray *returnedArray = returnedData;
                    connectCounter = 0;
                    
                    if(returnedArray.count == 0) {
                        NSLog(@"%s", "Update read message status failed (return array size 0)");
                    }
                }

            }
            
        }];
        
    }
    
}

#pragma mark - Manage Message Inbox 
-(void)groupMessage {
    
    // Clear old data
    
    if(sectionKeys != nil) {
        [sectionKeys removeAllObjects];
    }
    
    if(expandedSections != nil) {
         [expandedSections removeAllObjects];
    }
    
    if(messageInboxPerDays != nil) {
        [messageInboxPerDays removeAllObjects];
    }
   
    sectionKeys = [[NSMutableArray alloc] init];
    expandedSections = [[NSMutableArray alloc] init];
    messageInboxPerDays = [[NSMutableDictionary alloc] init];
    
    NSArray *dataMessageArr;
    
    if(self.inboxType == ATTENDANCE) {
        dataMessageArr = [self.inboxMessageDatabaseManager getMessageWithType:1 userID:(int)[UserData getUserID]];
    }
    else if(self.inboxType == PURCHASING) {
        dataMessageArr = [self.inboxMessageDatabaseManager getMessageWithType:2 userID:(int)[UserData getUserID]];
    }
    else if(self.inboxType == TOPUP) {
        dataMessageArr = [self.inboxMessageDatabaseManager getMessageWithType:3 userID:(int)[UserData getUserID]];
    }
    else if(self.inboxType == ABSENCEREQUEST) {
        dataMessageArr = [self.inboxMessageDatabaseManager getMessageWithType:4 userID:(int)[UserData getUserID]];
    }
    else if(self.inboxType == NEWS) {
        dataMessageArr = [self.inboxMessageDatabaseManager getMessageWithType:5 userID:(int)[UserData getUserID]];
    }
    
    if(dataMessageArr == nil || dataMessageArr.count == 0) {
        return;
    }
    
    NSMutableArray *messageToday = [[NSMutableArray alloc] init];
    NSMutableArray *messageYesterday = [[NSMutableArray alloc] init];
    NSMutableArray *messageLast7days = [[NSMutableArray alloc] init];
    NSMutableArray *messageLast30days = [[NSMutableArray alloc] init];
    NSMutableArray *messageOlder = [[NSMutableArray alloc] init];
    
    NSDate *today = [NSDate date];
    NSDate *yesterday = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-1 toDate:[NSDate date] options:0];
    NSDate *dayBeforeYesterday = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-2 toDate:[NSDate date] options:0];
    NSDate *last7days = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-6 toDate:[NSDate date] options:0];
    NSDate *last8days = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-7 toDate:[NSDate date] options:0];
    NSDate *last30days = [[Utils getGregorianCalendar]dateByAddingUnit:NSCalendarUnitDay value:-29 toDate:[NSDate date] options:0];
    //NSDate *olderdays = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-30 toDate:[NSDate date] options:0];
    
    for(int i=0; i<dataMessageArr.count; i++) {
        MessageInboxDataModel *model = [dataMessageArr objectAtIndex:i];
        
        if([DateUtility sameDate:model.date date2:today]) {
            [messageToday addObject:model];
        }
        else if([DateUtility sameDate:model.date date2:yesterday]) {
            [messageYesterday addObject:model];
        }
        else if([DateUtility betweenDate:model.date minDate:last7days maxDate:dayBeforeYesterday]) {
            [messageLast7days addObject:model];
            
        }
        else if([DateUtility betweenDate:model.date minDate:last30days maxDate:last8days]) {
            [messageLast30days addObject:model];
        }
        else {
            [messageOlder addObject:model];
        }
    }
    
    if(messageToday.count > 0) {
        [messageInboxPerDays setObject:messageToday forKey:KEY_TODAY];
        [sectionKeys addObject:KEY_TODAY];
        
        todayTotalMessage = (int)messageToday.count;
    }
    
    if(messageYesterday.count > 0) {
        [messageInboxPerDays setObject:messageYesterday forKey:KEY_YESTERDAY];
        [sectionKeys addObject:KEY_YESTERDAY];
        
        yesterdayTotalMessage = (int)messageYesterday.count;
    }
    
    if(messageLast7days.count > 0) {
        [messageInboxPerDays setObject:messageLast7days forKey:KEY_LAST7DAYS];
        [sectionKeys addObject:KEY_LAST7DAYS];
        
        last7daysTotalMessage = (int)messageLast7days.count;
    }
    
    if(messageLast30days.count > 0) {
        [messageInboxPerDays setObject:messageLast30days forKey:KEY_LAST30DAYS];
        [sectionKeys addObject:KEY_LAST30DAYS];
        
        last30daysTotalMessage = (int)messageLast30days.count;
    }
    
    if(messageOlder.count > 0) {
        [messageInboxPerDays setObject:messageOlder forKey:KEY_OLDER];
        [sectionKeys addObject:KEY_OLDER];
        
        olderTotalMessage = (int)messageOlder.count;
    }
    
    [self.tblExpandable reloadData];
    
}

-(NSString *)adjustPurchasingAndTopupText:(NSString *)str {
    //NSString *str = @"มีรายการใช้ระบบจับจ่าย จำนวน 8.00 บาท วันที่ 05/09/2016 ยอดคงเหลือ 2246.00 บาท หากไม่ถูกต้องติดต่อหมาย เลข 091-8233139";
    
    NSMutableString *resultStr = [[NSMutableString alloc] init];
    
    NSRange range1 = [str rangeOfString:@"จำนวน"];
    NSRange range2 = [str rangeOfString:@"วันที่"];
    NSRange range3 = [str rangeOfString:@"ยอดคงเหลือ"];
    NSRange range4 = [str rangeOfString:@"หาก"];
    
    if(range1.length != 0 && range2.length != 0 && range3.length != 0 && range4.length != 0) {
        
        NSMutableString *str1 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(0, range1.location)]];
        NSMutableString *str2 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(range1.location, range2.location - str1.length)]];
        NSMutableString *str3 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(range2.location, range3.location - str1.length - str2.length)]];
        NSMutableString *str4 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(range3.location, range4.location - str1.length - str2.length - str3.length)]];
        NSMutableString *str5 = [[NSMutableString alloc] initWithString:[str substringFromIndex:range4.location]];
        
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str1);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str2);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str3);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str4);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str5);
        
        [resultStr appendString:str1];
        
        // adjust text "จำนวน"
        NSRange str2Range = [str2 rangeOfString:@" "];
        if(str2Range.length != 0) {
            NSMutableString *str2FirstSectionStr = [[NSMutableString alloc] initWithString:[str2 substringWithRange:NSMakeRange(0, str2Range.location)]];
            NSMutableString *str2SecondSectionStr = [[NSMutableString alloc] initWithString:[str2 substringFromIndex:str2Range.location + str2Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str2FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str2SecondSectionStr);
            
            [resultStr appendFormat:@"\n%@ : %@", str2FirstSectionStr, str2SecondSectionStr];
        }
        
        // adjust text "วันที่"
        NSRange str3Range = [str3 rangeOfString:@" "];
        if(str3Range.length != 0) {
            NSMutableString *str3FirstSectionStr = [[NSMutableString alloc] initWithString:[str3 substringWithRange:NSMakeRange(0, str3Range.location)]];
            NSMutableString *str3SecondSectionStr = [[NSMutableString alloc] initWithString:[str3 substringFromIndex:str3Range.location + str3Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str3FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str3SecondSectionStr);
            
            [resultStr appendFormat:@"\n%@ : %@", str3FirstSectionStr, str3SecondSectionStr];
        }
        
        // adjust text "ยอดคงเหลือ"
        NSRange str4Range = [str4 rangeOfString:@" "];
        if(str4Range.length != 0) {
            NSMutableString *str4FirstSectionStr = [[NSMutableString alloc] initWithString:[str4 substringWithRange:NSMakeRange(0, str4Range.location)]];
            NSMutableString *str4SecondSectionStr = [[NSMutableString alloc] initWithString:[str4 substringFromIndex:str4Range.location + str4Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str4FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str4SecondSectionStr);
            
            [resultStr appendFormat:@"\n%@ : %@", str4FirstSectionStr, str4SecondSectionStr];
        }
        
        [resultStr appendFormat:@"\n%@", str5];
        
    }
    else {
        return str;
    }
    
    return resultStr;
    
}

-(NSString *)adjustAttendaceText:(NSString *)str {
    
    //NSString *str = @"ถึงโรงเรียนเวลา 00:04 น. สถานะ เข้าตรงเวลา";
    NSRange range1 = [str rangeOfString:@"สถานะ"];
    
    if(range1.length != 0) {
        
        // Substring first section
        NSString *firstSectionStr = [str substringWithRange:NSMakeRange(0, range1.location)];
        NSRange sec1Range = [firstSectionStr rangeOfString:@" "];
        
        if(sec1Range.length != 0) {
            
            NSMutableString *sec1FirstSectionStr = [[NSMutableString alloc] initWithString:[firstSectionStr substringWithRange:NSMakeRange(0, sec1Range.location)]];
            NSMutableString *sec1SecondSectionStr = [[NSMutableString alloc] initWithString:[firstSectionStr substringFromIndex:sec1Range.location + sec1Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec1FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec1SecondSectionStr);
            
            firstSectionStr = [NSString stringWithFormat:@"%@ : %@", sec1FirstSectionStr, sec1SecondSectionStr];
        }
        
        // Substring second section
        NSString *secondSectionStr = [str substringFromIndex:range1.location];
        NSRange sec2Range = [secondSectionStr rangeOfString:@" "];
        
        if(sec2Range.length != 0) {
            
            NSMutableString *sec2FirstSectionStr = [[NSMutableString alloc] initWithString:[secondSectionStr substringWithRange:NSMakeRange(0, sec2Range.location)]];
            NSMutableString *sec2SecondSectionStr = [[NSMutableString alloc] initWithString:[secondSectionStr substringFromIndex:sec2Range.location + sec2Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec2FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec2SecondSectionStr);
            
            secondSectionStr = [NSString stringWithFormat:@"%@ : %@", sec2FirstSectionStr, sec2SecondSectionStr];
        }
        
        NSString *resultString;
        
        if(firstSectionStr != nil && firstSectionStr.length > 0 && secondSectionStr != nil && secondSectionStr.length > 0) {
            resultString = [NSString stringWithFormat:@"%@\n%@", firstSectionStr, secondSectionStr];
        }
        else if(firstSectionStr != nil && firstSectionStr.length > 0) {
            resultString = [NSString stringWithFormat:@"%@", firstSectionStr];
        }
        else if(secondSectionStr != nil && secondSectionStr.length > 0) {
            resultString = [NSString stringWithFormat:@"%@", secondSectionStr];
        }
        
        return resultString;
        
    }
    else {
        return str;
    }
    
}

-(void)showMessageWhenInit {
    
    if(self.message_id != -1) {
        MessageInboxDataModel *model = [self.inboxMessageDatabaseManager getMessageWithID:self.message_id userID:(int)[UserData getUserID]];
        
        if(model == nil) {
            return ;
        }
        
        [self showMessage:model];
        
        [self.tblExpandable reloadData];
        self.message_id = -1;
    }
}

-(void)showMessage:(MessageInboxDataModel *)model {
    
    if(self.inboxType == PURCHASING || self.inboxType == TOPUP) {
        
        NSString *adjustMessage = [self adjustPurchasingAndTopupText:model.message];
        
        NSString *messageStr;
        NSString *noteStr;
        NSRange noteRange = [adjustMessage rangeOfString:@"หาก"];
        
        if(noteRange.length != 0) {
            messageStr = [adjustMessage substringToIndex:noteRange.location-1]; //-1 minus new line position
            noteStr = [NSString stringWithFormat:@"*%@", [adjustMessage substringFromIndex:noteRange.location]];
        }
        else {
            messageStr = adjustMessage;
            noteStr = @"";
        }
        
        if(self.messageInboxLongTextDialog != nil && [self.messageInboxLongTextDialog isDialogShowing]) {
            [self.messageInboxLongTextDialog dismissDialog];
        }
        
        [self.messageInboxLongTextDialog showDialogInView:self.view title:model.title message:messageStr noteMessage:noteStr messageDate:model.date];
        
    }
    else if(self.inboxType == ABSENCEREQUEST || self.inboxType == NEWS) {
        
        if(self.messageInboxScrollableTextDialog != nil && [self.messageInboxScrollableTextDialog isDialogShowing]) {
            [self.messageInboxScrollableTextDialog dismissDialog];
        }
        
        [self.messageInboxScrollableTextDialog showDialogInView:self.view title:model.title message:model.message messageDate:model.date];
        
    }
    else {
        
        NSString *adjustMessage = [self adjustAttendaceText:model.message];
        
        if(self.messageInboxDialog != nil && [self.messageInboxDialog isDialogShowing]) {
            [self.messageInboxDialog dismissDialog];
        }
        
        [self.messageInboxDialog showDialogInView:self.view title:model.title message:adjustMessage messageDate:model.date];
    }
    
     [self updateReadStatus:model];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Utility
- (NSInteger)getMessageType:(INBOXTYPE)type {
    if(type == ATTENDANCE) {
        return 1;
    }
    else if(type == PURCHASING) {
        return 2;
    }
    else if(type == TOPUP) {
        return 3;
    }
    else if(type == ABSENCEREQUEST) {
        return 4;
    }
    else if(type == NEWS) {
        return 5;
    }
    else if(type == HOMEWORK) {
        return 6;
    }
    
    return 0;
}


@end
