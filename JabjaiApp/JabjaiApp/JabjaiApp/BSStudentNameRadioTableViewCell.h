//
//  BSStudentNameRadioTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@class BSStudentNameRadioTableViewCell;

@protocol BSStudentNameRadioTableViewCellDelegate <NSObject>

- (void)onPressRadioButton:(BSStudentNameRadioTableViewCell *)tableViewCell atIndex:(NSInteger)index;

@end
@interface BSStudentNameRadioTableViewCell : UITableViewCell

@property (weak, nonatomic) id<BSStudentNameRadioTableViewCellDelegate> delegate;

@property (nonatomic) NSInteger index;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *radioButton;

- (void)setRadioButtonSelected;
- (void)setRadioButtonSelectedWithType:(int) type;
- (void)setRadioButtonClearSelected;

- (IBAction)actionPressRadioButton:(id)sender;

@end
