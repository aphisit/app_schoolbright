//
//  TASelectClassLevelViewController.h
//  JabjaiApp
//
//  Created by mac on 7/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "TableListDialog.h"
#import "SubjectTableListDialog.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "CallGetSubjectAPI.h"
#import "CallGetStudentInClassAPI.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "SSubjectModel.h"

static NSInteger TA_MODE_CHECK = 1;
static NSInteger TA_MODE_EDIT = 2;

@interface TASelectClassLevelViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, SubjectTableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate, CallGetSubjectAPIDelegate, CallGetStudentInClassAPIDelegate>

//Global variables
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SSubjectModel *> *subjectArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
@property (nonatomic) NSInteger mode;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classroomTextField;
@property (weak, nonatomic) IBOutlet UITextField *subjectTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

- (IBAction)openDrawer:(id)sender;
- (IBAction)actionNext:(id)sender;

@end
