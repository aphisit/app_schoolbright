//
//  ExecutiveReportTeacherListViewController.h
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CallGetEXReportAttendSchoolTeacherListAPI.h"
#import "CallGetEXReportAttendSchoolEmployeeListAPI.h"
#import "EXReportClassStatusModel.h"

static const NSInteger EXREPORT_TEACHER_LIST = 1;
static const NSInteger EXREPORT_EMPLOYEE_LIST = 2;

@interface ExecutiveReportTeacherListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CallGetEXReportAttendSchoolTeacherListAPIDelegate, CallGetEXReportAttendSchoolEmployeeListAPIDelegate>

// Global variables
@property (nonatomic) NSDate *reportDate;
@property (strong, nonatomic) NSArray<EXReportClassStatusModel *> *reportClassStatusArray;
@property (nonatomic) NSInteger mode;

@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)moveBack:(id)sender;
@end
