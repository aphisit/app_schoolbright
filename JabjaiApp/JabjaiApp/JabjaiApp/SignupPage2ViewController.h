//
//  SignupPage2ViewController.h
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupDataModel.h"
#import "DLRadioButton.h"

@interface SignupPage2ViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) SignupDataModel *signupDataModel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet DLRadioButton *genderRadioButton;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

- (IBAction)nextStep:(id)sender;
- (IBAction)moveBack:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;

@end
