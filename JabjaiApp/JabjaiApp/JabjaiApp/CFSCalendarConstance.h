//
//  CFSCalendarConstane.h
//  CFSCalendar
//
//  Created by dingwenchao on 8/28/15.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//
//  https://github.com/WenchaoD
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#pragma mark - Constance

UIKIT_EXTERN CGFloat const CFSCalendarStandardHeaderHeight;
UIKIT_EXTERN CGFloat const CFSCalendarStandardWeekdayHeight;
UIKIT_EXTERN CGFloat const CFSCalendarStandardMonthlyPageHeight;
UIKIT_EXTERN CGFloat const CFSCalendarStandardWeeklyPageHeight;
UIKIT_EXTERN CGFloat const CFSCalendarStandardCellDiameter;
UIKIT_EXTERN CGFloat const CFSCalendarAutomaticDimension;
UIKIT_EXTERN CGFloat const CFSCalendarDefaultBounceAnimationDuration;
UIKIT_EXTERN CGFloat const CFSCalendarStandardRowHeight;
UIKIT_EXTERN CGFloat const CFSCalendarStandardTitleTextSize;
UIKIT_EXTERN CGFloat const CFSCalendarStandardSubtitleTextSize;
UIKIT_EXTERN CGFloat const CFSCalendarStandardWeekdayTextSize;
UIKIT_EXTERN CGFloat const CFSCalendarStandardHeaderTextSize;
UIKIT_EXTERN CGFloat const CFSCalendarMaximumEventDotDiameter;
UIKIT_EXTERN CGFloat const CFSCalendarStandardScopeHandleHeight;

UIKIT_EXTERN NSInteger const CFSCalendarDefaultHourComponent;

#if TARGET_INTERFACE_BUILDER
#define CFSCalendarDeviceIsIPad NO
#else
#define CFSCalendarDeviceIsIPad [[UIDevice currentDevice].model hasPrefix:@"iPad"]
#endif

#define CFSCalendarStandardSelectionColor   FSColorRGBA(31,119,219,1.0)
#define CFSCalendarStandardTodayColor       FSColorRGBA(198,51,42 ,1.0)
#define CFSCalendarStandardTitleTextColor   FSColorRGBA(14,69,221 ,1.0)
#define CFSCalendarStandardEventDotColor    FSColorRGBA(31,119,219,0.75)

#define CFSCalendarStandardSeparatorColor   [[UIColor lightGrayColor] colorWithAlphaComponent:0.25]
#define CFSCalendarStandardScopeHandleColor [[UIColor lightGrayColor] colorWithAlphaComponent:0.50]

#define FSColorRGBA(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define CFSCalendarInAppExtension [[[NSBundle mainBundle] bundlePath] hasSuffix:@".appex"]

#if CGFLOAT_IS_DOUBLE
#define CFSCalendarFloor(c) floor(c)
#else
#define CFSCalendarFloor(c) floorf(c)
#endif

#define CFSCalendarUseWeakSelf __weak __typeof__(self) CFSCalendarWeakSelf = self;
#define CFSCalendarUseStrongSelf __typeof__(self) self = CFSCalendarWeakSelf;


#pragma mark - Deprecated

#define CFSCalendarDeprecated(instead) DEPRECATED_MSG_ATTRIBUTE(" Use " # instead " instead")

CFSCalendarDeprecated('CFSCalendarCellShape')
typedef NS_ENUM(NSInteger, CFSCalendarCellStyle) {
    CFSCalendarCellStyleCircle      = 0,
    CFSCalendarCellStyleRectangle   = 1
};

CFSCalendarDeprecated('CFSCalendarScrollDirection')
typedef NS_ENUM(NSInteger, CFSCalendarFlow) {
    CFSCalendarFlowVertical,
    CFSCalendarFlowHorizontal
};

CFSCalendarDeprecated('borderRadius')
typedef NS_ENUM(NSUInteger, CFSCalendarCellShape) {
    CFSCalendarCellShapeCircle    = 0,
    CFSCalendarCellShapeRectangle = 1
};

typedef NS_ENUM(NSUInteger, CFSCalendarUnit) {
    CFSCalendarUnitMonth = NSCalendarUnitMonth,
    CFSCalendarUnitWeekOfYear = NSCalendarUnitWeekOfYear,
    CFSCalendarUnitDay = NSCalendarUnitDay
};
