//
//  ExecutiveReportClassroomViewController.h
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "EXReportClassStatusModel.h"
#import "CallGetEXReportClassroomAttendSchoolAPI.h"

@interface ExecutiveReportClassroomViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CallGetEXReportClassroomAttendSchoolAPIDelegate>

// Global variables
@property (nonatomic) NSDate *reportDate;
@property (strong, nonatomic) NSArray<EXReportClassStatusModel *> *reportClassStatusArray;
@property (nonatomic) long long selectedClassLevelId;
@property (nonatomic) NSString *classLevelName;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

- (IBAction)moveBack:(id)sender;

@end
