//
//  TeachingSubjectDialog.h
//  JabjaiApp
//
//  Created by mac on 12/29/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MKDropdownMenu/MKDropdownMenu.h>
#import "UTeachingYearModel.h"

@protocol TeachingSubjectDialogDelegate <NSObject>

- (void)applyFilter:(NSNumber *)schoolYear semester:(NSString *)semester;

@optional
- (void)onTeachingSubjectDialogClose;

@end
@interface TeachingSubjectDialog : UIViewController <MKDropdownMenuDelegate, MKDropdownMenuDataSource>

@property (retain, nonatomic) id<TeachingSubjectDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet MKDropdownMenu *schoolYearDropdown;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *semesterDropdown;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

- (IBAction)onPressCancel:(id)sender;
- (IBAction)onPressOK:(id)sender;

- (id)initWithUTeachingYearModelArray:(NSArray<UTeachingYearModel *> *) dataArray;
- (void)setUTeachingYearModelArray:(NSArray<UTeachingYearModel *> *) dataArray;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
