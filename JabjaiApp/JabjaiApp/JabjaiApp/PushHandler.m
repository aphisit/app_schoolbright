//
//  PushHandler.m
//  JabjaiApp
//
//  Created by mac on 12/6/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "PushHandler.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "MessageInboxViewController.h"
#import "UserData.h"
#import "SWRevealViewController.h"
#import "ExecutiveReportMainViewController.h"

@implementation PushHandler

/**
 * Called when a notification is received in the foreground.
 *
 * @param notificationContent UANotificationContent object representing the notification info.
 *
 * @param completionHandler the completion handler to execute when notification processing is complete.
 */
-(void)receivedForegroundNotification:(UANotificationContent *)notificationContent completionHandler:(void (^)())completionHandler {
    
    NSLog(@"%@", @"received foreground notification");
    completionHandler();
    
}

/**
 * Called when a notification is received in the background.
 *
 * @param notificationContent UANotificationContent object representing the notification info.
 *
 * @param completionHandler the completion handler to execute when notification processing is complete.
 */
-(void)receivedBackgroundNotification:(UANotificationContent *)notificationContent completionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Application received a background notification
    UA_LDEBUG(@"The application received a background notification");
    
    // Call the completion handler
    completionHandler(UIBackgroundFetchResultNoData);
}

/**
 * Called when a notification is received in the background or foreground and results in a user interaction.
 * User interactions can include launching the application from the push, or using an interactive control on the notification interface
 * such as a button or text field.
 *
 * @param notificationResponse UANotificationResponse object representing the user's response
 * to the notification and the associated notification contents.
 *
 * @param completionHandler the completion handler to execute when processing the user's response has completed.
 */
-(void)receivedNotificationResponse:(UANotificationResponse *)notificationResponse completionHandler:(void (^)())completionHandler {
    
    UA_LDEBUG(@"The user selected the following action identifier:%@", notificationResponse.actionIdentifier);
    
    NSDictionary *notificationInfo = [[notificationResponse notificationContent] notificationInfo];
    
    if(notificationInfo != nil && [notificationInfo objectForKey:@"^d"] != nil && [UserData getUserID] != -1) {
        
        /* 
         * Deep link to come_to_school
         * vnd.jabjai.jabjaiapp://deeplink/come_to_school?message_id=1234
         *
         * Deep link for executive report
         * vnd.jabjai.jabjaiapp://deeplink/executive?message_id=1234&date=ddMMyyyy
        */
        
        NSString *urlString = [notificationInfo valueForKey:@"^d"];
        NSArray *queryArray = [urlString componentsSeparatedByString:@"/"];
        
        if(queryArray.count == 4) {
            
            NSString *query = [queryArray objectAtIndex:3];
    
            // Get main storyboard instance
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            MessageInboxViewController *viewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"MessageInboxStoryboard"];
//            viewController.message_id = [parameter intValue];
//            
//            if([deeplink isEqualToString:@"come_to_school"]) {
//                viewController.inboxType = ATTENDANCE;
//            }
//            else if([deeplink isEqualToString:@"buy_product"]) {
//                viewController.inboxType = PURCHASING;
//            }
//            else if([deeplink isEqualToString:@"topup"]) {
//                viewController.inboxType = TOPUP;
//            }
//            else if([deeplink isEqualToString:@"absence_request"]) {
//                viewController.inboxType = ABSENCEREQUEST;
//            }
//            else if([deeplink isEqualToString:@"news"]) {
//                viewController.inboxType = NEWS;
//            }
            
            NSRange questionmarkRange = [query rangeOfString:@"?"];
            NSString *deeplink = [query substringToIndex:questionmarkRange.location];
            NSRange messageParameterRange = [query rangeOfString:@"message_id="];
            
            if([deeplink isEqualToString:@"executive"]) {
                
                NSRange andRange = [query rangeOfString:@"&"];
                NSString *messageParameter = [query substringWithRange:NSMakeRange(messageParameterRange.location + messageParameterRange.length, andRange.location - (messageParameterRange.location + messageParameterRange.length))];
                NSRange dateParameterRange = [query rangeOfString:@"date="];
                NSString *dateStr = [query substringFromIndex:dateParameterRange.location + dateParameterRange.length];
                
                NSLog(@"%@", [NSString stringWithFormat:@"Get Deep Link : [%@], Message ID = [%@], Date = [%@]", deeplink, messageParameter, dateStr]);
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"ddMMyyyy"];

                NSDate *reportDate = [dateFormatter dateFromString:dateStr];
                
                ExecutiveReportMainViewController *viewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"ExecReportMainStoryboard"];
                viewController.reportDate = reportDate;
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.deeplink = deeplink;
                appDelegate.deeplink_message_id = [messageParameter integerValue];
                
                [[appDelegate window] setRootViewController:viewController];
            }
            else {
                NSString *messageParameter = [query substringFromIndex:messageParameterRange.location + messageParameterRange.length];
                
                NSLog(@"%@", [NSString stringWithFormat:@"Get Deep Link : [%@], Message ID = [%@]", deeplink, messageParameter]);
                
                SWRevealViewController *viewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"SWRevealViewControllerStoryboard"];
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.deeplink = deeplink;
                appDelegate.deeplink_message_id = [messageParameter integerValue];
                
                [[appDelegate window] setRootViewController:viewController];
            }
            
        }
        
    }
    
    // reset badge
    [[UAirship push] resetBadge];
    
    completionHandler();
}

/**
 * Called when a notification has arrived in the foreground and is available for display.
 *
 * Note: this method is relevant only for iOS 10 and above.
 *
 * @param notification The notification.
 * @return a UNNotificationPresentationOptions enum value indicating the presentation options for the notification.
 */
- (UNNotificationPresentationOptions)presentationOptionsForNotification:(UNNotification *)notification {
    
    UA_LDEBUG(@"presentationOptionsForNotification");
    
    return UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound;
}

@end
