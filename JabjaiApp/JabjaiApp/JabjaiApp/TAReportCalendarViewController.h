//
//  TAReportCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import ARSlidingPanel;

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CFSCalendar.h"

@class TAReportCalendarViewController;

@protocol TAReportCalendarViewControllerDelegate <NSObject>

@optional
- (void)onSelectDate:(TAReportCalendarViewController *)classObj date:(NSDate *)date;

@end

@interface TAReportCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, ARSPDragDelegate, ARSPVisibilityStateDelegate>

@property (weak, nonatomic) id<TAReportCalendarViewControllerDelegate> delegate;

@property (weak ,nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)moveBack:(id)sender;

@end
