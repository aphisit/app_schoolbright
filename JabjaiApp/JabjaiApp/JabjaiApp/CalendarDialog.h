//
//  CalendarDialog.h
//  JabjaiApp
//
//  Created by mac on 12/12/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CFSCalendar.h"

@protocol CalendarDialogDelegate <NSObject>

@optional
-(void)calendarDialogPressOK:(NSArray<NSDate *> *)selectedDates;
-(void)calendarDialogPressCancel;

@end

@interface CalendarDialog : UIViewController <CFSCalendarDelegate, CFSCalendarDataSource>

@property (nonatomic, retain) id<CalendarDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet CFSCalendar *calendar;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

- (IBAction)onPressCancel:(id)sender;
- (IBAction)onPressOK:(id)sender;

- (id)initWithTitle:(NSString *)title minimumDate:(NSDate *)minimumDate maximumDate:(NSDate *)maximumDate;
- (void)setDateWithMinimumDate:(NSDate *)minDate maximumDate:(NSDate *)maxDate;
- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;


@end
