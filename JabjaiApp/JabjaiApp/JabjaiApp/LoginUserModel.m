//
//  LoginUserModel.m
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "LoginUserModel.h"

@implementation LoginUserModel

@synthesize userId = _userId;
@synthesize schoolId = _schoolId;
@synthesize userType = _userType;
@synthesize academyType = _academyType;
@synthesize imageUrl = _imageUrl;
@synthesize firstName = _firstName;
@synthesize lastName = _lastName;
@synthesize gender = _gender;

- (void)setUserId:(long long)userId {
    _userId = userId;
}

- (void)setSchoolId:(long long)schoolId {
    _schoolId = schoolId;
}

- (void)setUserType:(NSInteger)userType {
    _userType = userType;
}

- (void)setAcademyType:(NSInteger)academyType {
    _academyType = academyType;
}

- (void)setImageUrl:(NSString *)imageUrl {
    _imageUrl = imageUrl;
}

- (void)setFirstName:(NSString *)firstName {
    _firstName = firstName;
}

- (void)setLastName:(NSString *)lastName {
    _lastName = lastName;
}

- (void)setGender:(NSInteger)gender {
    _gender = gender;
}

- (long long)getUserId {
    return _userId;
}

- (long long)getSchoolId {
    return _schoolId;
}

- (NSInteger)getUserType {
    return _userType;
}

- (NSInteger)getAcademyType {
    return _academyType;
}

- (NSString *)getImageUrl {
    return _imageUrl;
}

- (NSString *)getFirstName {
    return _firstName;
}

- (NSString *)getLastName {
    return _lastName;
}

- (NSInteger)getGender {
    return _gender;
}

@end
