//
//  BSSummaryViewController.m
//  JabjaiApp
//
//  Created by mac on 7/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSSummaryViewController.h"
#import "BSSelectClassLevelViewController.h"
#import "BSSelectBehaviorScoreViewController.h"
#import "BSLeftRightLabelTableViewCell.h"
#import "UserData.h"
#import "Constant.h"
#import "Utils.h"

@interface BSSummaryViewController () {
    
    NSMutableArray<BSSelectedStudent *> *allSelectedStudentsArray;
    NSMutableArray<BehaviorScoreModel *> *allSelectedBehaviorScoreArray;
    
    UIColor *greenBGColor;
    UIColor *redBGColor;
    
    BOOL updateSuccess;
    
    // The dialog
    AlertDialog *alertDialog;
}

@property (strong, nonatomic) CallUpdateBehaviorScorePOSTAPI *callUpdateBehaviorScorePOSTAPI;

@end

static NSString *cellIdentifier = @"LeftRightCell";

@implementation BSSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.tableViewListOfNames.dataSource = self;
    self.tableViewListOfNames.delegate = self;
    self.tableViewListOfNames.tableFooterView = [[UIView alloc] init];
    self.tableViewListOfNames.separatorInset = UIEdgeInsetsZero;
    
    self.tableViewBehaviorScore.dataSource = self;
    self.tableViewBehaviorScore.delegate = self;
    self.tableViewBehaviorScore.tableFooterView = [[UIView alloc] init];
    self.tableViewBehaviorScore.separatorInset = UIEdgeInsetsZero;
    
    [self.tableViewListOfNames registerNib:[UINib nibWithNibName:NSStringFromClass([BSLeftRightLabelTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self.tableViewBehaviorScore registerNib:[UINib nibWithNibName:NSStringFromClass([BSLeftRightLabelTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    greenBGColor = [UIColor colorWithRed:13/255.0 green:199/255.0 blue:66/255.0 alpha:1.0];
    redBGColor = [UIColor colorWithRed:220/255.0 green:43/255.0 blue:69/255.0 alpha:1.0];
    
    updateSuccess = NO;
    
    if(_mode == BS_MODE_ADD) {
        self.headerTitleLabel.text = @"เพิ่มคะแนนความประพฤติ";
        [self.submitButton setBackgroundColor:greenBGColor];
    }
    else {
        self.headerTitleLabel.text = @"ตัดคะแนนความประพฤติ";
        [self.submitButton setBackgroundColor:redBGColor];
    }
    
    [self createSelectedData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView.tag == 1) { // tableViewListOfNames
        
        if(allSelectedStudentsArray != nil) {
            return allSelectedStudentsArray.count;
        }
    }
    else if(tableView.tag == 2) { // tableViewBehaviorScore
        
        if(allSelectedBehaviorScoreArray != nil) {
            return allSelectedBehaviorScoreArray.count;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BSLeftRightLabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if(tableView.tag == 1) {
        
        BSSelectedStudent *model = [allSelectedStudentsArray objectAtIndex:indexPath.row];
        
        cell.LeftLabel.text = [[NSString alloc] initWithFormat:@"%ld. %@", (indexPath.row + 1), [model getStudentName]];
        cell.RightLabel.text = @"";
        [cell.RightLabel setAlpha:0.0f];
        
        return cell;
    }
    else if(tableView.tag == 2) {
        
        BehaviorScoreModel *model = [allSelectedBehaviorScoreArray objectAtIndex:indexPath.row];
        
        cell.LeftLabel.text = [[NSString alloc] initWithFormat:@"%ld. %@", (indexPath.row + 1), [model getBehaviorScoreName]];
        [cell.RightLabel setAlpha:1.0f];
        
        if([model getBehaviorScoreType] == 0) {
            cell.RightLabel.text = [[NSString alloc] initWithFormat:@"+%i", (int)[model getScore]];
        }
        else {
            cell.RightLabel.text = [[NSString alloc] initWithFormat:@"-%i", (int)[model getScore]];
        }
        
        return cell;
    }
    
    return [UITableViewCell new];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

#pragma mark - API Caller

- (void)updateStudentBehaviorScoreWithJSONString:(NSString *)jsonString {
    
    if(self.callUpdateBehaviorScorePOSTAPI != nil) {
        self.callUpdateBehaviorScorePOSTAPI = nil;
    }
    
    self.callUpdateBehaviorScorePOSTAPI = [[CallUpdateBehaviorScorePOSTAPI alloc] init];
    self.callUpdateBehaviorScorePOSTAPI.delegate = self;
    [self.callUpdateBehaviorScorePOSTAPI call:[UserData getSchoolId] teacherId:[UserData getUserID] jsonString:jsonString];
    
    [self showIndicator];
}

#pragma mark - API Delegate

- (void)callUpdateBehaviorScorePOSTAPI:(CallUpdateBehaviorScorePOSTAPI *)classObj response:(NSString *)response success:(BOOL)success {
    
    [self stopIndicator];
    updateSuccess = success;
    
    if(success) {
        NSString *alertMessage = @"ทำรายการสำเร็จ";
        [self showAlertDialogWithMessage:alertMessage];
    }
    else {
        NSString *alertMessage = @"ทำรายการไม่สำเร็จ\nโปรดลองใหม่";
        [self showAlertDialogWithMessage:alertMessage];
    }

}

#pragma mark - Dialog

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil) {
        if([alertDialog isDialogShowing]) {
            [alertDialog dismissDialog];
        }
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)onAlertDialogClose {
    
    if(updateSuccess) {
        [self gotoBSSelectClassLevelViewController];
    }
}

#pragma mark - Utility

- (void)createSelectedData {
    
    allSelectedStudentsArray = [[NSMutableArray alloc] init];
    allSelectedBehaviorScoreArray = [[NSMutableArray alloc] init];
    
    if(self.selectedStudentArray != nil) {
        for(BSSelectedStudent *model in self.selectedStudentArray) {
            
            if([model isSelected]) {
                [allSelectedStudentsArray addObject:model];
            }
        }
    }
    
    NSInteger totalBehaviorScore = 0;
    
    if(self.behaviorScoreArray != nil) {
        for(BehaviorScoreModel *model in self.behaviorScoreArray) {
            
            if([model isSelected]) {
                [allSelectedBehaviorScoreArray addObject:model];
                
                if([model getBehaviorScoreType] == 0) { // type add
                    totalBehaviorScore += [model getScore];
                }
                else {
                    totalBehaviorScore -= [model getScore];
                }
            }
        }
    }
    
    if(totalBehaviorScore > 0) {
        self.totalScoreLabel.text = [[NSString alloc] initWithFormat:@"+%ld", totalBehaviorScore];
    }
    else {
        self.totalScoreLabel.text = [[NSString alloc] initWithFormat:@"%ld", totalBehaviorScore];
    }
    
    [self.tableViewListOfNames reloadData];
    [self.tableViewBehaviorScore reloadData];
}

- (void)gotoBSSelectClassLevelViewController {
    BSSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectClassLevelStoryboard"];
    
    viewController.mode = _mode;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Action functions
- (IBAction)actionSubmit:(id)sender {
    
    if(allSelectedStudentsArray != nil && allSelectedBehaviorScoreArray != nil) {
        NSString *jsonString = [Utils getStudentsBehaviorsScoreJSONWithSelectedStudentArray:allSelectedStudentsArray selectedBehaviorsScoreArray:allSelectedBehaviorScoreArray];
        
        //NSLog(@"%@", jsonString);
        [self updateStudentBehaviorScoreWithJSONString:jsonString];
    }
}

- (IBAction)moveBack:(id)sender {
    
    BSSelectBehaviorScoreViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSSelectBehaviorScoreStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.mode = _mode;
    
    viewController.selectedStudentArray = _selectedStudentArray;
    
    viewController.behaviorScoreArray = _behaviorScoreArray;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
@end
