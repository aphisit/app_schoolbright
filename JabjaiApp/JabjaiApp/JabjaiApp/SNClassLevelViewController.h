//
//  SNClassLevelViewController.h
//  JabjaiApp
//
//  Created by toffee on 8/29/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "EXReportClassStatusModel.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"

@interface SNClassLevelViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{

}
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSDate *reportDate;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (strong, nonatomic) NSArray<EXReportClassStatusModel *> *reportClassStatusArray;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

- (IBAction)moveBack:(id)sender;

@end
