//
//  BSReportScoreListViewController.m
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSReportScoreListViewController.h"
#import "BSNameBehaviorScoreTableViewCell.h"
#import "BSReportARSPContainerController.h"
#import "Utils.h"
#import "DateUtility.h"
#import "UserData.h"

@interface BSReportScoreListViewController () {
    
    NSMutableDictionary<NSString *, NSMutableArray<BSHistoryModel *> *> *bsHistoryModelDict;
    NSMutableArray<BSHistoryModel *> *bsHistoryArray;
    
    UIColor *grayColor;
    UIColor *greenColor;
    UIColor *redColor;
    
    NSDate *consideredDate;
    NSDateFormatter *dateFormatter;
}

@property (strong, nonatomic) BSReportCalendarViewController *bsReportCalendarViewController;
@property (strong, nonatomic) BSReportARSPContainerController *panelControllerContainer;

@property (strong, nonatomic) CallGetBSHistoryInDateRangeAPI *callGetBSHistoryInDateRangeAPI;

@end

static NSString *cellIdentifier = @"Cell";

@implementation BSReportScoreListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    grayColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
    greenColor = [UIColor colorWithRed:12/255.0 green:190/255.0 blue:64/255.0 alpha:1.0];
    redColor = [UIColor colorWithRed:255/255.0 green:57/255.0 blue:56/255.0 alpha:1.0];
    
    self.panelControllerContainer = (BSReportARSPContainerController *)self.parentViewController;
    self.bsReportCalendarViewController = (BSReportCalendarViewController *)self.panelControllerContainer.mainViewController;
    self.bsReportCalendarViewController.delegate = self;
    
    self.statusType = self.panelControllerContainer.statusId;
    self.classroomId = self.panelControllerContainer.classroomId;
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.backgroundColor = grayColor;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BSNameBehaviorScoreTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    dateFormatter = [Utils getDateFormatter];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    consideredDate = [NSDate date];
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:consideredDate];
    
    NSDate *startDate = [DateUtility firstDayOfMonth:consideredDate];
    NSDate *endDate = [DateUtility lastDayOfMonth:consideredDate];
    
    [self getBSHistoryInDateRangeWithStartDate:startDate endDate:endDate];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(bsHistoryArray == nil) {
        return 0;
    }
    else {
        return bsHistoryArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BSNameBehaviorScoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    BSHistoryModel *model = [bsHistoryArray objectAtIndex:indexPath.row];
    
    if([model getBehaviorType] == 0) { // type add
        cell.scoreLabel.backgroundColor = greenColor;
        cell.scoreLabel.text = [NSString stringWithFormat:@"+%ld", [model getBehaviorScore]];
    } else {
        cell.scoreLabel.backgroundColor = redColor;
        cell.scoreLabel.text = [NSString stringWithFormat:@"-%ld", [model getBehaviorScore]];
    }
    
    cell.nameLabel.text = [model getStudentName];
    cell.behaviorNameLabel.text = [model getBehaviorName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onSelectItem:date:)]) {
        
        BSHistoryModel *model = [bsHistoryArray objectAtIndex:indexPath.row];
        
        [self.delegate onSelectItem:model date:consideredDate];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

#pragma mark - API Caller

- (void)getBSHistoryInDateRangeWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    
    if(self.callGetBSHistoryInDateRangeAPI != nil) {
        self.callGetBSHistoryInDateRangeAPI = nil;
    }
    
    self.callGetBSHistoryInDateRangeAPI = [[CallGetBSHistoryInDateRangeAPI alloc] init];
    self.callGetBSHistoryInDateRangeAPI.delegate = self;
    [self.callGetBSHistoryInDateRangeAPI call:[UserData getSchoolId] classroomId:self.classroomId startDate:startDate endDate:endDate type:self.statusType];
    
    [self showIndicator];
}

#pragma mark - API Delegate

- (void)callGetBSHistoryInDateRangeAPI:(CallGetBSHistoryInDateRangeAPI *)classObj data:(NSArray<BSHistoryModel *> *)data success:(BOOL)success {
    
    [self stopIndicator];
    
    if(success && data != nil) {
        
        if(bsHistoryModelDict != nil) {
            [bsHistoryModelDict removeAllObjects];
            bsHistoryModelDict = nil;
        }
        
        bsHistoryModelDict = [[NSMutableDictionary alloc] init];
        
        for(BSHistoryModel *model in data) {
            
            NSDate *date = [model getDateTime];
            NSString *dateStr = [dateFormatter stringFromDate:date];
            
            if([bsHistoryModelDict objectForKey:dateStr] == nil) {
                NSMutableArray<BSHistoryModel *> *dataArray = [[NSMutableArray alloc] init];
                [dataArray addObject:model];
                
                [bsHistoryModelDict setObject:dataArray forKey:dateStr];
            }
            else {
                NSMutableArray<BSHistoryModel *> *dataArray = [bsHistoryModelDict objectForKey:dateStr];
                
                [dataArray addObject:model];
            }
        }
        
        [self updateBSHistoryArray];
    }
}

#pragma mark - BSReportCalendarViewControllerDelegate

- (void)onSelectDate:(NSDate *)date {
    
    self.dateTitle.text = [Utils getThaiDateFormatWithDate: date];
    
    if([DateUtility sameDate:date date2:consideredDate]) {
        consideredDate = date;
        
        [self updateBSHistoryArray];
    }
    else {
        consideredDate = date;
        
        NSDate *startDate = [DateUtility firstDayOfMonth:consideredDate];
        NSDate *endDate = [DateUtility lastDayOfMonth:consideredDate];
        
        [self getBSHistoryInDateRangeWithStartDate:startDate endDate:endDate];
    }
}

#pragma mark - Utility

- (void)updateBSHistoryArray {
    
    if(consideredDate != nil && bsHistoryModelDict != nil) {
        NSString *dateStr = [dateFormatter stringFromDate:consideredDate];
        
        if([bsHistoryModelDict objectForKey:dateStr] != nil) {
            bsHistoryArray = [bsHistoryModelDict objectForKey:dateStr];
        }
        else {
            bsHistoryArray = nil;
        }
        
        [self.tableView reloadData];
    }
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

@end
