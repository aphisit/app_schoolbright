//
//  CallGetLoginAPI.h
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginUserModel.h"

@class CallGetLoginAPI;

@protocol CallGetLoginAPIDelegate <NSObject>

@optional
- (void)callGetLoginAPI:(CallGetLoginAPI *)classObj data:(LoginUserModel *)data success:(BOOL)success;
- (void)onUnRegisterFinger:(CallGetLoginAPI *)classObj confirmCode:(NSString *)confirmCode;
- (void)incorrectUserNameOrPassword:(CallGetLoginAPI *)classObj;

@end
@interface CallGetLoginAPI : NSObject

@property (weak, nonatomic) id<CallGetLoginAPIDelegate> delegate;

- (void)call:(NSString *)userName password:(NSString *)password;

@end
