//
//  NavigationViewController.h
//  JabjaiApp
//
//  Created by mac on 10/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CustomUIImageView.h"
#import "CallGetUnreadMessageCountAPI.h"

@interface NavigationViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CallGetUnreadMessageCountAPIDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblExpandable;
@property (weak, nonatomic) IBOutlet CustomUIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *viewProfileLabel;
@property (weak, nonatomic) IBOutlet CustomUIImageView *floatingUserIcon1;
@property (weak, nonatomic) IBOutlet CustomUIImageView *floatingUserIcon2;
@property (weak, nonatomic) IBOutlet UIView *viewIcon1;
@property (weak, nonatomic) IBOutlet UIView *viewIcon2;
@property (weak, nonatomic) IBOutlet UIButton *btnDropdown;


- (IBAction)viewProfile:(id)sender;
- (IBAction)actionShowUser:(id)sender;


@end
