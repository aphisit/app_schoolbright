//
//  MultipleUserModel.m
//  JabjaiApp
//
//  Created by mac on 8/10/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "MultipleUserModel.h"

@implementation MultipleUserModel

@synthesize masterId = _masterId;
@synthesize slaveId = _slaveId;
@synthesize schoolId = _schoolId;
@synthesize userType = _userType;
@synthesize academyType = _academyType;
@synthesize imageUrl = _imageUrl;
@synthesize firstName = _firstName;
@synthesize lastName = _lastName;
@synthesize gender = _gender;

- (void)setMasterId:(long long)masterId {
    _masterId = masterId;
}

- (void)setSlaveId:(long long)slaveId {
    _slaveId = slaveId;
}

- (void)setSchoolId:(long long)schoolId {
    _schoolId = schoolId;
}

- (void)setUserType:(NSInteger)userType {
    _userType = userType;
}

- (void)setAcademyType:(NSInteger)academyType {
    _academyType = academyType;
}

- (void)setImageUrl:(NSString *)imageUrl {
    _imageUrl = imageUrl;
}

- (void)setFirstName:(NSString *)firstName {
    _firstName = firstName;
}

- (void)setLastName:(NSString *)lastName {
    _lastName = lastName;
}

- (void)setGender:(NSInteger)gender {
    _gender = gender;
}

- (long long)getMasterId {
    return _masterId;
}

- (long long)getSlaveId {
    return _slaveId;
}

- (long long)getSchoolId {
    return _schoolId;
}

- (NSInteger)getUserType {
    return _userType;
}

- (NSInteger)getAcademyType {
    return _academyType;
}

- (NSString *)getImageUrl {
    return _imageUrl;
}

- (NSString *)getFirstName {
    return _firstName;
}

- (NSString *)getLastName {
    return _lastName;
}

- (NSInteger)getGender {
    return _gender;
}

@end
