//
//  ExecutiveReportClassLevelViewController.m
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "ExecutiveReportClassLevelViewController.h"
#import "ExecutiveReportMainViewController.h"
#import "ExecutiveReportClassroomViewController.h"
#import "ExecutiveReportTeacherListViewController.h"
#import "UserData.h"
#import "Utils.h"
#import "EXReportStudyClassModel.h"
#import "EXReportStatusDetailModel.h"
#import "EXReportClassHeaderTableViewCell.h"
#import "EXReportClassChildTableViewCell.h"
#import "EXReportButtonTableViewCell.h"

@interface ExecutiveReportClassLevelViewController () {
    NSMutableArray *sectionKeys;
    NSMutableArray *expandedSections;
    NSMutableDictionary<NSNumber *, EXReportStudyClassModel *> *studyClassDict;
}

@end

static NSString *headerCellIdentifier = @"HeaderCell";
static NSString *childCellIdentifier = @"ChildCell";
static NSString *buttonCellIdentifier = @"ButtonCell";

@implementation ExecutiveReportClassLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   //  register table nib
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EXReportClassHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:headerCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EXReportClassChildTableViewCell class]) bundle:nil] forCellReuseIdentifier:childCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EXReportButtonTableViewCell class]) bundle:nil] forCellReuseIdentifier:buttonCellIdentifier];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self prepareData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(sectionKeys != nil) {
        return sectionKeys.count ;
    }
    else {
        return 0;
    }
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"numbersection = %d",section) ;
    if([expandedSections containsObject:@(section)]) {
        NSNumber *key = [sectionKeys objectAtIndex:section];
        EXReportStudyClassModel *studyClassModel = [studyClassDict objectForKey:key];
       NSLog(@"studenclassmodel = %@",[studyClassModel getReportStatusDetailArray]);
        if([studyClassModel getReportStatusDetailArray] != nil) {
            return [[studyClassModel getReportStatusDetailArray] count] + 1; // +1 header for each section
        }
        else {
            return 1;
        }
    }
    else {
        return 1; // show only header
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *key = [sectionKeys objectAtIndex:indexPath.section];
    EXReportStudyClassModel *studyClassModel = [studyClassDict objectForKey:key];
  
    NSLog(@"showsection = %d", indexPath.row);
       if(indexPath.row == 0) { // For the first row of each section we'll show header
           
        EXReportClassHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier forIndexPath:indexPath];
        BOOL isExpanded = [expandedSections containsObject:@(indexPath.section)];
        if(isExpanded) {
            cell.imgArrow.image = [UIImage imageNamed:@"ic_arrow_down"];
        }
        else {
            cell.imgArrow.image = [UIImage imageNamed:@"ic_arrow_left"];
        }
        
        cell.titleLabel.text = [studyClassModel getTitle];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.userInteractionEnabled = YES;
        return cell;
        
    }
    else {
        NSLog(@"indexpathrow2 = %d",indexPath.row);
        EXReportStatusDetailModel *statusDetailModel = [[studyClassModel getReportStatusDetailArray] objectAtIndex:indexPath.row - 1];
        
        if([statusDetailModel getIsButton]) {
            EXReportButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:buttonCellIdentifier forIndexPath:indexPath];
             cell.titleLabel.text = [statusDetailModel getTitle];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userInteractionEnabled = YES;
            return cell;
        }
        else {
            EXReportClassChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:childCellIdentifier forIndexPath:indexPath];
            cell.titleLabel.text = [statusDetailModel getTitle];
            if([Utils isIntegerWithDouble:[statusDetailModel getPercent]]) {
                cell.percentLabel.text = [NSString stringWithFormat:@"%ld (%.0f%%)", [statusDetailModel getAmount], [statusDetailModel getPercent]];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.userInteractionEnabled = YES;
            }
            else {
                cell.percentLabel.text = [NSString stringWithFormat:@"%ld (%.2f%%)", [statusDetailModel getAmount], [statusDetailModel getPercent]];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userInteractionEnabled = NO;
            
            return cell;
        }
    }
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //NSLog(@"index0 = %d",indexPath.section);
    int section = indexPath.section;
    if([expandedSections containsObject:@(indexPath.section)]) {
        //NSLog(@"index1 = %d",indexPath.section);
        if(indexPath.row == 0) { // If select header of each section, then toggle up and down
            [expandedSections removeObject:@(indexPath.section)];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
        else {
            NSNumber *key = [sectionKeys objectAtIndex:indexPath.section];
            EXReportStudyClassModel *studyClassModel = [studyClassDict objectForKey:key];
            EXReportStatusDetailModel *statusDetailModel = [[studyClassModel getReportStatusDetailArray] objectAtIndex:indexPath.row - 1];
            
            if([statusDetailModel getIsButton]) {
                if ([statusDetailModel getIId] == -1) { // Teacher type
                    
                    ExecutiveReportTeacherListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecutiveReportTeacherListStoryboard"];
                    viewController.reportDate = _reportDate;
                    viewController.reportClassStatusArray = _reportClassStatusArray;
                    viewController.mode = EXREPORT_TEACHER_LIST;
                    
                    [self.revealViewController pushFrontViewController:viewController animated:YES];
                }
                else if ([statusDetailModel getIId] == -2) { // Employee type
                    
                    ExecutiveReportTeacherListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecutiveReportTeacherListStoryboard"];
                    viewController.reportDate = _reportDate;
                    viewController.reportClassStatusArray = _reportClassStatusArray;
                    viewController.mode = EXREPORT_EMPLOYEE_LIST;
                    
                    [self.revealViewController pushFrontViewController:viewController animated:YES];
                }
                else { // Student class level
                    long long classLevelId = [statusDetailModel getIId];
                    NSString *classLevelName = [studyClassModel getTitle];
                    
                    ExecutiveReportClassroomViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecutiveReportClassroomStoryboard"];
                    viewController.reportDate = _reportDate;
                    viewController.reportClassStatusArray = _reportClassStatusArray;
                    viewController.selectedClassLevelId = classLevelId;
                    viewController.classLevelName = classLevelName;
                    
                    [self.revealViewController pushFrontViewController:viewController animated:YES];
                }
            }
        }
    }
    else {
         NSLog(@"section = %d",indexPath.section);
        [expandedSections addObject:@(indexPath.section)];
         NSLog(@"expandedSections = %@",expandedSections);
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Utility

- (void)prepareData {
    sectionKeys = [[NSMutableArray alloc] init];
    expandedSections = [[NSMutableArray alloc] init];
    studyClassDict = [[NSMutableDictionary alloc] init];
    
    if(self.reportClassStatusArray != nil) {
        for(EXReportClassStatusModel *model in self.reportClassStatusArray) {
            NSInteger total = [model getNumberOfOnTimeStatus] + [model getNumberOfLateStatus] + [model getNumberOfAbsenceStatus];
            double factor = 100.0 / total;
            
            EXReportStatusDetailModel *statusDetailModel1 = [[EXReportStatusDetailModel alloc] init];
            [statusDetailModel1 setIId:[model getIId]];
            [statusDetailModel1 setTitle:@"ตรงเวลา"];
            [statusDetailModel1 setAmount:[model getNumberOfOnTimeStatus]];
            [statusDetailModel1 setPercent:[model getNumberOfOnTimeStatus] * factor];
            [statusDetailModel1 setIsButton:NO];
            
            EXReportStatusDetailModel *statusDetailModel2 = [[EXReportStatusDetailModel alloc] init];
            [statusDetailModel2 setIId:[model getIId]];
            [statusDetailModel2 setTitle:@"สาย"];
            [statusDetailModel2 setAmount:[model getNumberOfLateStatus]];
            [statusDetailModel2 setPercent:[model getNumberOfLateStatus] * factor];
            [statusDetailModel2 setIsButton:NO];
            
            EXReportStatusDetailModel *statusDetailModel3 = [[EXReportStatusDetailModel alloc] init];
            [statusDetailModel3 setIId:[model getIId]];
            [statusDetailModel3 setTitle:@"ขาด"];
            [statusDetailModel3 setAmount:[model getNumberOfAbsenceStatus]];
            [statusDetailModel3 setPercent:[model getNumberOfAbsenceStatus] * factor];
            [statusDetailModel3 setIsButton:NO];
            
            EXReportStatusDetailModel *statusDetailModel4 = [[EXReportStatusDetailModel alloc] init];
            [statusDetailModel4 setIId:[model getIId]];
            [statusDetailModel4 setAmount:0];
            [statusDetailModel4 setPercent:0];
            [statusDetailModel4 setIsButton:YES];
            
            if([model getIId] == -1 || [model getIId] == -2) {
                [statusDetailModel4 setTitle:@"ดูสถิติรายบุคคล"];
            }
            else {
                [statusDetailModel4 setTitle:@"ดูสถิติรายห้อง"];
            }
            
            NSMutableArray *statusDetailArray = [[NSMutableArray alloc] init];
            [statusDetailArray addObject:statusDetailModel1];
            [statusDetailArray addObject:statusDetailModel2];
            [statusDetailArray addObject:statusDetailModel3];
            [statusDetailArray addObject:statusDetailModel4];
            
            EXReportStudyClassModel *studyClassModel = [[EXReportStudyClassModel alloc] init];
            [studyClassModel setIId:[model getIId]];
            [studyClassModel setTitle:[model getTitle]];
            [studyClassModel setReportStatusDetailArray:statusDetailArray];
            
            [sectionKeys addObject:@([model getIId])];
            NSLog(@"studyClassModel = %@", studyClassModel) ;
            [studyClassDict setObject:studyClassModel forKey:@([model getIId])];
        }
    }
}

#pragma mark - Action functions
- (IBAction)moveBack:(id)sender {
    ExecutiveReportMainViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecReportMainStoryboard"];
    
    viewController.reportDate = _reportDate;
    viewController.reportClassStatusArray = _reportClassStatusArray;
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
@end
