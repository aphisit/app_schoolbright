//
//  BSReportCalendarViewController.m
//  JabjaiApp
//
//  Created by mac on 7/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSReportCalendarViewController.h"
#import "Utils.h"
#import "DateUtility.h"
#import "BSReportDetailViewController.h"
#import "BSReportARSPContainerController.h"
#import "BSReportSelectClassLevelViewController.h"

@interface BSReportCalendarViewController () {
    UIColor *grayColor;
    UIColor *headerTitleColor;
    UIColor *todayColor;
    UIColor *selectionColor;
    
    NSCalendar *gregorian;
}

@property (strong, nonatomic) BSReportARSPContainerController *panelControllerContainer;
@property (strong, nonatomic) BSReportScoreListViewController *bsReportScoreListViewController;

@property (nonatomic) CGFloat shadowRadius;
@property (nonatomic) CGFloat shadowOpacity;
@property (nonatomic) CGFloat animationDuration;

@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;

@end

@implementation BSReportCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    selectionColor = [UIColor colorWithRed:212/255.0 green:44/255.0 blue:31/255.0 alpha:1.0];
    
    gregorian = [Utils getGregorianCalendar];
    
    self.minimumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
    self.maximumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:1 toDate:[NSDate date] options:0];
    
    // Sliding up panel
    self.panelControllerContainer = (BSReportARSPContainerController *)self.parentViewController;
    self.panelControllerContainer.dragDelegate = self;
    self.panelControllerContainer.visibilityStateDelegate = self;
    
    self.shadowRadius = 5.0f;
    self.shadowOpacity = 0.5f;
    self.animationDuration = 0.55f;
    
    self.panelControllerContainer.dropShadow = YES;
    self.panelControllerContainer.shadowRadius = self.shadowRadius;
    self.panelControllerContainer.shadowOpacity = self.shadowOpacity;
    self.panelControllerContainer.animationDuration = self.animationDuration;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createCalendar {
    
    // Remove all subviews from container
    NSArray *viewsToRemove = self.containerView.subviews;
    for(UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    self.containerView.backgroundColor = grayColor;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    CGFloat width = CGRectGetWidth(self.containerView.bounds);
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    
    calendar.dataSource = self;
    calendar.delegate = self;
    //calendar.swipeToChooseGesture.enabled = YES;
    calendar.backgroundColor = grayColor;//[UIColor whiteColor];
    //    calendar.appearance.titleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.subtitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.weekdayFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.headerTitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:32.0f];
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
    calendar.appearance.selectionColor = selectionColor;
    calendar.appearance.todayColor = todayColor;
    [self.containerView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 95, 34);
    previousButton.backgroundColor = grayColor;//[UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(CGRectGetWidth(self.containerView.frame) - 95, 5, 95, 34);
    nextButton.backgroundColor = grayColor; //[UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:nextButton];
    self.nextButton = nextButton;
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    self.panelControllerContainer.visibleZoneHeight = screenHeight - (self.calendar.frame.size.height + self.calendar.frame.origin.y) - (self.headerView.frame.origin.y + self.headerView.frame.size.height);
    self.panelControllerContainer.swipableZoneHeight = 0;
    self.panelControllerContainer.draggingEnabled = YES;
    self.panelControllerContainer.shouldOverlapMainViewController = YES;
    self.panelControllerContainer.shouldShiftMainViewController = NO;
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.bsReportScoreListViewController = (BSReportScoreListViewController *)self.panelControllerContainer.panelViewController;
    self.bsReportScoreListViewController.delegate = self;
    
    [self createCalendar];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Drag delegate

- (void)panelControllerWasDragged:(CGFloat)panelControllerVisibility {
    
}

- (void)panelControllerChangedVisibilityState:(ARSPVisibilityState)state {
    
}

#pragma mark - CFSCalendarDataSource

- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

#pragma mark - CFSCalendarDelegate

- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
    if([DateUtility sameMonthOfYear:date date2:calendar.currentPage]) {
        return YES;
    }
    
    return NO;
}

- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
    if(self.delegate && [self.delegate respondsToSelector:@selector(onSelectDate:)]) {
        [self.delegate onSelectDate:date];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd/MM/yyyy";
    NSLog(@"%@", [formatter stringFromDate:date]);
}

#pragma mark - Class buttons event

- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
}

- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
}

#pragma mark - Drawer
- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)moveBack:(id)sender {
    
    BSReportSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSReportSelectClassLevelStoryboard"];
    
    viewController.classLevelArray = self.panelControllerContainer.classLevelArray;
    viewController.classroomArray = self.panelControllerContainer.classroomArray;
    viewController.statusArray = self.panelControllerContainer.statusArray;
    viewController.selectedClassLevelIndex = self.panelControllerContainer.selectedClassLevelIndex;
    viewController.selectedClassroomIndex = self.panelControllerContainer.selectedClassroomIndex;
    viewController.selectedStatusIndex = self.panelControllerContainer.selectedStatusIndex;
    viewController.classLevelId = self.panelControllerContainer.classLevelId;
    viewController.classroomId = self.panelControllerContainer.classroomId;
    viewController.statusId = self.panelControllerContainer.statusId;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

#pragma mark - BSReportScoreListViewControllerDelegate
- (void)onSelectItem:(BSHistoryModel *)data date:(NSDate *)date {
    
    BSReportDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSReportDetailStoryboard"];
    
    viewController.classLevelArray = self.panelControllerContainer.classLevelArray;
    viewController.classroomArray = self.panelControllerContainer.classroomArray;
    viewController.statusArray = self.panelControllerContainer.statusArray;
    viewController.selectedClassLevelIndex = self.panelControllerContainer.selectedClassLevelIndex;
    viewController.selectedClassroomIndex = self.panelControllerContainer.selectedClassroomIndex;
    viewController.selectedStatusIndex = self.panelControllerContainer.selectedStatusIndex;
    viewController.classLevelId = self.panelControllerContainer.classLevelId;
    viewController.classroomId = self.panelControllerContainer.classroomId;
    viewController.statusId = self.panelControllerContainer.statusId;
    
    viewController.selectedHistoryModel = data;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}
@end
