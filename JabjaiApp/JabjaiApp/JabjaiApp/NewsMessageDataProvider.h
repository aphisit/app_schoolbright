//
//  NewsMessageDataProvider.h
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NewsMessageDataProvider;
@protocol NewsMessageDataProviderDelegate <NSObject>

@optional
- (void)dataProvider:(NewsMessageDataProvider *)dataProvider willLoadDataAtPage:(NSUInteger)page indexes:(NSIndexSet *)indexes;
- (void)dataProvider:(NewsMessageDataProvider *)dataProvider didLoadDataAtPage:(NSUInteger)page indexes:(NSIndexSet *)indexes;

@end

@interface NewsMessageDataProvider : NSObject

- (instancetype)initWithPageSize:(NSUInteger)pageSize;

@property (nonatomic, weak) id<NewsMessageDataProviderDelegate> delegate;

/**
 * The array returned contains NSNull values for data
 * objects not yet loaded. As data loads, the array updates
 * automatically to include the newly loaded objects.
 *
 * @see shouldLoadAutomatically
 */
@property (nonatomic, readonly) NSArray *dataObjects;

@property (nonatomic, readonly) NSUInteger pageSize;
@property (nonatomic, readonly) NSUInteger loadedCount;

/**
 * When this property is set, new data is automatically loaded when
 * the dataObjects array returns an NSNull reference.
 *
 * @see dataObjects
 */
@property (nonatomic) BOOL shouldLoadAutomatically;
@property (nonatomic) NSUInteger automaticPreloadMargin;

- (BOOL)isLoadingDataAtIndex:(NSUInteger)index;
- (void)loadDataForIndex:(NSUInteger)index;

@end
