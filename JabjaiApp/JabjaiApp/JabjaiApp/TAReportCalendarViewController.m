//
//  TAReportCalendarViewController.m
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TAReportCalendarViewController.h"
#import "Utils.h"
#import "DateUtility.h"
#import "TAReportARSPContainerController.h"
#import "TAReportSelectClassLevelViewController.h"

@interface TAReportCalendarViewController () {
    UIColor *grayColor;
    UIColor *headerTitleColor;
    UIColor *todayColor;
    UIColor *selectionColor;
    
    NSCalendar *gregorian;
}

@property (strong, nonatomic) TAReportARSPContainerController *arspContainerController;

@property (nonatomic) CGFloat shadowRadius;
@property (nonatomic) CGFloat shadowOpacity;
@property (nonatomic) CGFloat animationDuration;

@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;

@end

@implementation TAReportCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    selectionColor = [UIColor colorWithRed:212/255.0 green:44/255.0 blue:31/255.0 alpha:1.0];
    
    gregorian = [Utils getGregorianCalendar];
    
    self.minimumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
    self.maximumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:1 toDate:[NSDate date] options:0];
    
    // Sliding up panel
    self.arspContainerController = (TAReportARSPContainerController *)self.parentViewController;
    self.arspContainerController.dragDelegate = self;
    self.arspContainerController.visibilityStateDelegate = self;
    
    NSString *subjectName = [self.arspContainerController.subjectArray objectAtIndex:self.arspContainerController.selectedSubjectIndex].subjectName;
    self.headerTitleLabel.text = subjectName;
    
    self.shadowRadius = 5.0f;
    self.shadowOpacity = 0.5f;
    self.animationDuration = 0.55f;
    
    self.arspContainerController.dropShadow = YES;
    self.arspContainerController.shadowRadius = self.shadowRadius;
    self.arspContainerController.shadowOpacity = self.shadowOpacity;
    self.arspContainerController.animationDuration = self.animationDuration;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createCalendar {
    
    // Remove all subviews from container
    NSArray *viewsToRemove = self.containerView.subviews;
    for(UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    self.containerView.backgroundColor = grayColor;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    CGFloat width = CGRectGetWidth(self.containerView.bounds);
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    
    calendar.dataSource = self;
    calendar.delegate = self;
    //calendar.swipeToChooseGesture.enabled = YES;
    calendar.backgroundColor = grayColor;//[UIColor whiteColor];
    //    calendar.appearance.titleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.subtitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.weekdayFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:25.0f];
    //    calendar.appearance.headerTitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:32.0f];
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
    calendar.appearance.selectionColor = selectionColor;
    calendar.appearance.todayColor = todayColor;
    [self.containerView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 95, 34);
    previousButton.backgroundColor = grayColor;//[UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(CGRectGetWidth(self.containerView.frame) - 95, 5, 95, 34);
    nextButton.backgroundColor = grayColor; //[UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:nextButton];
    self.nextButton = nextButton;
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    self.arspContainerController.visibleZoneHeight = screenHeight - (self.calendar.frame.size.height + self.calendar.frame.origin.y) - (self.headerView.frame.origin.y + self.headerView.frame.size.height);
    self.arspContainerController.swipableZoneHeight = 0;
    self.arspContainerController.draggingEnabled = YES;
    self.arspContainerController.shouldOverlapMainViewController = YES;
    self.arspContainerController.shouldShiftMainViewController = NO;
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self createCalendar];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)panelControllerWasDragged:(CGFloat)panelControllerVisibility {
    
}

- (void)panelControllerChangedVisibilityState:(ARSPVisibilityState)state {
    
}

#pragma mark - CFSCalendarDataSource

- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

#pragma mark - CFSCalendarDelegate

- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
    if([DateUtility sameMonthOfYear:date date2:calendar.currentPage]) {
        return YES;
    }
    
    return NO;
}

- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
    if(self.delegate && [self.delegate respondsToSelector:@selector(onSelectDate:date:)]) {
        [self.delegate onSelectDate:self date:date];
    }
    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    formatter.dateFormat = @"dd/MM/yyyy";
//    NSLog(@"%@", [formatter stringFromDate:date]);
}

#pragma mark - Class buttons event

- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
}

- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
}

#pragma mark - Action functions
- (IBAction)moveBack:(id)sender {
    
    TAReportSelectClassLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TAReportSelectClassLevelStoryboard"];
    viewController.classLevelArray = self.arspContainerController.classLevelArray;
    viewController.classroomArray = self.arspContainerController.classroomArray;
    viewController.subjectArray = self.arspContainerController.subjectArray;
    viewController.selectedClassLevelIndex = self.arspContainerController.selectedClassLevelIndex;
    viewController.selectedClassroomIndex = self.arspContainerController.selectedClassroomIndex;
    viewController.selectedSubjectIndex = self.arspContainerController.selectedSubjectIndex;
    viewController.selectedClassLevelId = self.arspContainerController.selectedClassLevelId;
    viewController.selectedClassroomId = self.arspContainerController.selectedClassroomId;
    viewController.selectedSubjectId = self.arspContainerController.selectedSubjectId;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
@end
