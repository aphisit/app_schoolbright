//
//  AgendaHeaderTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 10/24/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerLeft;
@property (weak, nonatomic) IBOutlet UILabel *headerRight;

@end
