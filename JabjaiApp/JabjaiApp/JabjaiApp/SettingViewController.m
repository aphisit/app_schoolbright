//
//  SettingViewController.m
//  JabjaiApp
//
//  Created by mac on 12/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "SettingViewController.h"
#import "SWRevealViewController.h"
#import "ProfileViewController.h"
#import "FeedbackViewController.h"
#import "SettingTableViewCell.h"
#import "APIURL.h"
#import "Utils.h"
#import "Constant.h"
#import "AddAccountMainViewController.h"

@interface SettingViewController () {
    NSInteger connectCounter;
}

@property (strong, nonatomic) ChangeFingerprintAlertDialog *changeFingerprintAlertDialog;
@property (strong, nonatomic) ChangeFingerprintConfirmDialog *changeFingerprintConfirmDialog;

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.tblView.dataSource = self;
    self.tblView.delegate = self;
    
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([SettingTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.tblView.layoutMargins = UIEdgeInsetsZero;
    self.tblView.separatorInset = UIEdgeInsetsZero;
    self.tblView.preservesSuperviewLayoutMargins = NO;
    self.tblView.tableFooterView = [[UIView alloc] init];
    
    self.changeFingerprintAlertDialog = [[ChangeFingerprintAlertDialog alloc] init];
    self.changeFingerprintConfirmDialog = [[ChangeFingerprintConfirmDialog alloc] init];
    self.changeFingerprintConfirmDialog.delegate = self;
    
    connectCounter = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    //cell.label.text = @"แก้ไขลายนิ้วมือ";

    if(indexPath.row == 0) {
        cell.label.text = @"จัดการผู้ใช้";
    }
    else if(indexPath.row == 1) {
        cell.label.text = @"เกี่ยวกับเรา";
    }
    else if(indexPath.row == 2) {
        cell.label.text = @"ข้อเสนอแนะ";
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if(indexPath.row == 0) {
//        [self showChangeFingerprintConfirmDialogWithTitle:@"แจ้งเตือน" message:@"ยืนยันการขอรับรหัส\nเพื่อแก้ไขลายนิ้วมือ"];
//    }
    
    if( (indexPath.row == 0)) {
        AddAccountMainViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAccountMainStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else if (indexPath.row == 1) {
        
        // Go to profile view controller
        ProfileViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else if (indexPath.row == 2) {
        
        FeedbackViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedbackStoryboard"];
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - ChangeFingerprintConfirmDialogDelegate

- (void)changeFingerpringDialogConfirmOK {
    [self sendChangeFingerprintReqeust];
}

#pragma mark - GetAPIData

- (void) sendChangeFingerprintReqeust {
    NSString *URLString = [APIURL getChangeFingerprintRequestURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self sendChangeFingerprintReqeust];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            connectCounter = 0;
            
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"%@", [NSString stringWithFormat:@"Change Fingerprint Request Response : %@", result]);
            
            result = [result stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            
            if([Utils stringIsNumeric:result]) {
                
                int resultCode = [result intValue];
                
                if(resultCode > 0) {
                    [self showChangeFingerprintAlertDialogWithTitle:@"แจ้งเตือน" message:@"ระบบได้ทำการส่งรหัสยืนยัน\nให้ท่านทางอีเมลแล้ว\nกรุณาตรวจสอบอีเมลของท่าน"];
                }
                else if(resultCode == 0) {
                    [self showChangeFingerprintAlertDialogWithTitle:@"แจ้งเตือน" message:@"ท่านทำการขอรหัสยืนยันเรียบร้อยแล้ว กรุณาตรวจสอบอีเมล"];
                }
                else {
                    NSLog(@"%@", [NSString stringWithFormat:@"Change finger print request error : %@", result]);
                    [self showChangeFingerprintAlertDialogWithTitle:@"แจ้งเตือน" message:@"ขอภัยเนื่องจาก\nเกิดข้อผิดพลาดในการทำรายการ"];
                }
            }
            else {
                NSLog(@"%@", [NSString stringWithFormat:@"Change finger print request error : %@", result]);
                [self showChangeFingerprintAlertDialogWithTitle:@"แจ้งเตือน" message:@"ขอภัยเนื่องจาก\nเกิดข้อผิดพลาดในการทำรายการ"];
            }
        }

    }];
    
}

#pragma mark - Dialog

- (void)showChangeFingerprintConfirmDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(self.changeFingerprintConfirmDialog != nil && [self.changeFingerprintConfirmDialog isDialogShowing]) {
        [self.changeFingerprintConfirmDialog dismissDialog];
    }
    
    [self.changeFingerprintConfirmDialog showDialogInView:self.view title:title message:message];
}

- (void)showChangeFingerprintAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(self.changeFingerprintAlertDialog != nil && [self.changeFingerprintAlertDialog isDialogShowing]) {
        [self.changeFingerprintAlertDialog dismissDialog];
    }
    
    [self.changeFingerprintAlertDialog showDialogInView:self.view title:title message:message];
}

@end
