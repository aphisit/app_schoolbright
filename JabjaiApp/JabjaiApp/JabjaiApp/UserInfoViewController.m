//
//  UserInfoViewController.m
//  JabjaiApp
//
//  Created by mac on 5/17/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "UserInfoViewController.h"
#import "NewsMessageTableViewCell.h"
#import "MessageTableViewCell.h"
#import "MessageInboxDataModel.h"
#import "StudyViewController.h"
#import "UserViewController.h"
#import <Sheriff/GIBadgeView.h>
#import "CallGetUnreadMessageCountAPI.h"
#import "AppDelegate.h"
#import "ExecutiveReportMainViewController.h"

#import "MessageInboxDialog.h"
#import "MessageInboxLongTextDialog.h"
#import "MessageInBoxScrollableTextDialog.h"

#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"

static NSString *dataCellIdentifier = @"MessageCell";
static NSString *statusCellIdentifier = @"Cell";
static NSInteger itemPerPage = 20;

@interface UserInfoViewController () <CallGetUnreadMessageCountAPIDelegate> {
    NSMutableDictionary<NSNumber *, NSArray<MessageInboxDataModel *> *> *messageSections;
    NSMutableArray<MessageInboxDataModel *> *messages;
    
    UIColor *unreadBGColor;
    UIColor *readBGColor;
    UIColor *absenceStatusColor;
    UIColor *lateStatusColor;
    UIColor *onTimeStatusColor;
    UIColor *outAheadOfTimeColor;
    UIColor *outOfTimeColor;
    
    UIColor *OrangeColor;
    UIColor *GrayColor;
    
    NSInteger connectCounter;
    BOOL isTableViewShowing;
}

@property (nonatomic) CAPSPageMenu *pageMenu;

@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property (nonatomic, strong) GIBadgeView *badgeView;
@property (nonatomic, strong) MessageInboxDialog *messageInboxDialog;
@property (nonatomic, strong) MessageInboxLongTextDialog *messageInboxLongTextDialog;
@property (nonatomic, strong) MessageInBoxScrollableTextDialog *messageInboxScrollableTextDialog;
@property (nonatomic, strong) CallGetUnreadMessageCountAPI *callGetUnreadMessageCountAPI;

@end



@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    messageSections = [[NSMutableDictionary alloc] init];
    messages = [[NSMutableArray alloc] init];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NewsMessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:dataCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:statusCellIdentifier];
    
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    unreadBGColor = [UIColor whiteColor];
    readBGColor = [UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1.0];
    absenceStatusColor = [UIColor colorWithRed:255/255.0 green:57/255.0 blue:56/255.0 alpha:1.0];
    lateStatusColor = [UIColor colorWithRed:228/255.0 green:145/255.0 blue:50/255.0 alpha:1.0];
    onTimeStatusColor = [UIColor colorWithRed:12/255.0 green:190/255.0 blue:64/255.0 alpha:1.0];
    outAheadOfTimeColor = [UIColor colorWithRed:153/255.0 green:73/255.0 blue:227/255.0 alpha:1.0];
    outOfTimeColor = [UIColor colorWithRed:64/255.0 green:114/255.0 blue:177/255.0 alpha:1.0];
    OrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1.0];
    GrayColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.badgeView = [GIBadgeView new];
    [self.notificationButton.imageView addSubview:self.badgeView];
    self.notificationButton.imageView.clipsToBounds = NO;
    
    [self hideTableView];
    
    [self getMessageDataWithPage:1];
    [self getUnreadMessageCount];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    [self setupPageMenu];
}

- (void)viewWillAppear:(BOOL)animated {
    [self checkDeepLinkMessage];
}

- (void)checkDeepLinkMessage {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.deeplink != nil) {
        
        self.messageID = appDelegate.deeplink_message_id;
        self.showMessageDialogWhenOpen = YES;
        
        [self showTableView];
        [self getMessageWithID:self.messageID];
        
        // Clear deep link
        appDelegate.deeplink = nil;
        appDelegate.deeplink_message_id = -1;
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return messageSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger page = section + 1;
    return [[messageSections objectForKey:@(page)] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *mCell;
    
    NSInteger page = indexPath.section + 1;
    MessageInboxDataModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
    if(model.messageType == 1 && ![model.message containsString:@"คะแนนพฤติกรรม"]) { // Attendance message
        
        MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:statusCellIdentifier forIndexPath:indexPath];
        
        cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
        cell.titleLabel.text = model.title;
        cell.messageLabel.text = model.message;
        
        if([model.message rangeOfString:@"เข้าตรงเวลา" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            cell.statusLabel.text = @"ตรงเวลา";
            cell.statusLabel.backgroundColor = onTimeStatusColor;
        }
        else if([model.message rangeOfString:@"เข้าเรียน" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            cell.statusLabel.text = @"เข้าเรียน";
            cell.statusLabel.backgroundColor = onTimeStatusColor;
        }
        else if([model.message rangeOfString:@"สาย" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            cell.statusLabel.text = @"สาย";
            cell.statusLabel.backgroundColor = lateStatusColor;
        }
        else if([model.message rangeOfString:@"ขาด" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            cell.statusLabel.text = @"ขาด";
            cell.statusLabel.backgroundColor = absenceStatusColor;
        }
        else if([model.message rangeOfString:@"ออกตรงเวลา" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            cell.statusLabel.text = @"ออกตรงเวลา";
            cell.statusLabel.backgroundColor = onTimeStatusColor;
        }
        else if([model.message rangeOfString:@"ออกก่อนเวลา" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            cell.statusLabel.text = @"ออกก่อนเวลา";
            cell.statusLabel.backgroundColor = outAheadOfTimeColor;
        }
        else if([model.message rangeOfString:@"ออกเกินเวลา" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            cell.statusLabel.text = @"ออกเกินเวลา";
            cell.statusLabel.backgroundColor = outOfTimeColor;
        }
        else {
            cell.statusLabel.text = @"n/a";
            cell.statusLabel.backgroundColor = [UIColor blackColor];
        }
        
        cell.statusLabel.layer.cornerRadius = cell.statusLabel.frame.size.width / 2;
        cell.statusLabel.clipsToBounds = YES;
        cell.statusLabel.layer.shouldRasterize = YES;
        
        mCell = cell;
    }
    else {
        NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
        
        cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
        cell.titleLabel.text = model.title;
        cell.messageLabel.text = model.message;
        
        mCell = cell;
    }
    
    if(model.status == 0) {
        // Unread
        mCell.backgroundColor = unreadBGColor;
    }
    else {
        mCell.backgroundColor = readBGColor;
    }
    
    return mCell;
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger page = indexPath.section + 1;
    MessageInboxDataModel *model = [[messageSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
    if(model.status == 0) { // message still unread
        [self updateReadStatus:model];
        model.status = 1; // set message already read
    }
    
    if(model.messageType == 7) { // message type executive we do not show dialog but we go to executive report
        ExecutiveReportMainViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExecReportMainStoryboard"];
        viewController.reportDate = model.date;
        
        [self.revealViewController pushFrontViewController:viewController animated:YES];
    }
    else {
        [self showMessage:model];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

#pragma mark - Get API Data
- (void)getMessageDataWithPage:(NSUInteger)page {
    [self showIndicator];
    
    long long userID = [UserData getUserID];
    NSString *URLString = [APIURL getAllMessageWithPage:page userID:userID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getMessageDataWithPage:page];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageDataWithPage:page];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageDataWithPage:page];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    int status = [[dataDict objectForKey:@"nStatus"] intValue];
                    int type = [[dataDict objectForKey:@"nType"] intValue];
                    NSMutableString *messageDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dSend"]];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sTitle"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    NSRange dotRange = [messageDateStr rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:messageDateStr];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:messageDateStr];
                    }
                    
                    MessageInboxDataModel *model = [[MessageInboxDataModel alloc] init];
                    model.messageID = messageID;
                    model.userID = (int)[UserData getUserID];
                    model.messageType = type;
                    model.status =status;
                    model.title = title;
                    model.message = message;
                    model.date = messageDate;
                    
                    [newDataArr addObject:model];
                    
                }
                
                [messages removeAllObjects];
                messages = [[NSMutableArray alloc] initWithArray:newDataArr];
                [messageSections setObject:newDataArr forKey:@(page)];
                
                [self.tableView reloadData];
            }
            
        }
    }];
    
}

-(void)updateReadStatus:(MessageInboxDataModel *)model {
    
    long long userID = [UserData getUserID];
    NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:model.messageID userID:userID];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self updateReadStatus:model];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateReadStatus:model];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self updateReadStatus:model];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(returnedArray.count == 0) {
                    NSLog(@"%s", "Update read message status failed (return array size 0)");
                }
                
                [self getUnreadMessageCount];
            }
            
        }
        
    }];
}

- (void)getMessageWithID:(long long)messageID {
    
    long long userID = [UserData getUserID];
    NSString *URLString = [APIURL getUpdateReadMessageURLWithMessageID:messageID userID:userID];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getMessageWithID:messageID];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageWithID:messageID];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getMessageWithID:messageID];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                MessageInboxDataModel *messageInboxDataModel = nil;
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int messageID = [[dataDict objectForKey:@"nMessageID"] intValue];
                    int status = [[dataDict objectForKey:@"nStatus"] intValue];
                    int type = [[dataDict objectForKey:@"nType"] intValue];
                    NSMutableString *messageDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dSend"]];
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sMessage"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sTitle"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) messageDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    NSRange dotRange = [messageDateStr rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:messageDateStr];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:messageDateStr];
                    }
                    
                    messageInboxDataModel = [[MessageInboxDataModel alloc] init];
                    messageInboxDataModel.messageID = messageID;
                    messageInboxDataModel.userID = (int)[UserData getUserID];
                    messageInboxDataModel.messageType = type;
                    messageInboxDataModel.status =status;
                    messageInboxDataModel.title = title;
                    messageInboxDataModel.message = message;
                    messageInboxDataModel.date = messageDate;
                    
                    break;
                }
                
                if(messageInboxDataModel != nil) {
                    [self showMessage:messageInboxDataModel];
                    
                    // check to update table view whether current message has read
                    [messageSections enumerateKeysAndObjectsUsingBlock:^(NSNumber * _Nonnull key, NSArray<MessageInboxDataModel *> * _Nonnull obj, BOOL * _Nonnull stop) {
                        
                        BOOL shouldStop = NO;
                        
                        for(MessageInboxDataModel *model in obj) {
                            
                            if(model.messageID == messageInboxDataModel.messageID) {
                                
                                [self updateReadStatus:model];
                                
                                model.status = 1; //update read status
                                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:[key integerValue] - 1] withRowAnimation:UITableViewRowAnimationFade];
                                
                                shouldStop = YES;
                                break;
                            }
                        }
                        
                        if(shouldStop) {
                            
                            *stop = YES; // stop enumerate
                        }
                        
                    }];
                }
            }
            
        }
        
    }];
}

- (void)getUnreadMessageCount {
    
    if(self.callGetUnreadMessageCountAPI == nil) {
        self.callGetUnreadMessageCountAPI = [[CallGetUnreadMessageCountAPI alloc] init];
        self.callGetUnreadMessageCountAPI.delegate = self;
    }
    
    [self.callGetUnreadMessageCountAPI call:[UserData getUserID]];
}

- (void)fetchMoreData {
    
    if(messages.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *lastRowIndexPath = [visiblePaths lastObject];
        
        // Check whether or not the very last row is visible.
        NSInteger numberOfSections = [self.tableView numberOfSections];
        NSInteger lastRowSection = [lastRowIndexPath section];
        NSInteger lastRowRow = [lastRowIndexPath row];
        NSInteger numberOfRowsInSection = [self.tableView numberOfRowsInSection:lastRowSection];
        
        if(lastRowSection == numberOfSections -1 && lastRowRow == numberOfRowsInSection - 1) {
            
            if(messages.count % itemPerPage == 0) {
                NSInteger nextPage = lastRowSection + 2;
                [self getMessageDataWithPage:nextPage];
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self fetchMoreData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self fetchMoreData];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Dialog
- (void)showMessage:(MessageInboxDataModel *)model {
    
    if(model.messageType == 2 || model.messageType == 3) { //2:purchasing, 3:topup
        
        NSString *adjustMessage = [self adjustPurchasingAndTopupText:model.message];
        
        NSString *messageStr;
        NSString *noteStr;
        NSRange noteRange = [adjustMessage rangeOfString:@"หาก"];
        
        if(noteRange.length != 0) {
            messageStr = [adjustMessage substringToIndex:noteRange.location-1]; //-1 minus new line position
            noteStr = [NSString stringWithFormat:@"*%@", [adjustMessage substringFromIndex:noteRange.location]];
        }
        else {
            messageStr = adjustMessage;
            noteStr = @"";
        }
        
        if(self.messageInboxLongTextDialog == nil) {
            self.messageInboxLongTextDialog = [[MessageInboxLongTextDialog alloc] init];
        }
        
        if(self.messageInboxLongTextDialog != nil && [self.messageInboxLongTextDialog isDialogShowing]) {
            [self.messageInboxLongTextDialog dismissDialog];
        }
        
        [self.messageInboxLongTextDialog showDialogInView:self.view title:model.title message:messageStr noteMessage:noteStr messageDate:model.date];
        
    }
    else if(model.messageType == 1 && ![model.message containsString:@"คะแนนพฤติกรรม"]) { // 1:attendance
        
        NSString *adjustMessage = [self adjustAttendaceText:model.message];
        
        
        if(self.messageInboxDialog == nil) {
            self.messageInboxDialog = [[MessageInboxDialog alloc] init];
        }
        
        if(self.messageInboxDialog != nil && [self.messageInboxDialog isDialogShowing]) {
            [self.messageInboxDialog dismissDialog];
        }
        
        [self.messageInboxDialog showDialogInView:self.view title:model.title message:adjustMessage messageDate:model.date];
        
        
    }
    else { // other type 4:absencerequest 5:news 6:homework
        
        if(self.messageInboxScrollableTextDialog == nil) {
            self.messageInboxScrollableTextDialog = [[MessageInBoxScrollableTextDialog alloc] init];
        }
        
        if(self.messageInboxScrollableTextDialog != nil && [self.messageInboxScrollableTextDialog isDialogShowing]) {
            [self.messageInboxScrollableTextDialog dismissDialog];
        }
        
        [self.messageInboxScrollableTextDialog showDialogInView:self.view title:model.title message:model.message messageDate:model.date];
        
    }
    
}

#pragma mark - Actions
- (IBAction)openDrawer:(id)sender {
    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)showNotification:(id)sender {
    
    if(isTableViewShowing) {
        [self hideTableView];
    }
    else {
        [self showTableView];
    }
    
}

#pragma mark - Utility
-(NSString *)adjustPurchasingAndTopupText:(NSString *)str {
    //NSString *str = @"มีรายการใช้ระบบจับจ่าย จำนวน 8.00 บาท วันที่ 05/09/2016 ยอดคงเหลือ 2246.00 บาท หากไม่ถูกต้องติดต่อหมาย เลข 091-8233139";
    
    NSMutableString *resultStr = [[NSMutableString alloc] init];
    
    NSRange range1 = [str rangeOfString:@"จำนวน"];
    NSRange range2 = [str rangeOfString:@"วันที่"];
    NSRange range3 = [str rangeOfString:@"ยอดคงเหลือ"];
    NSRange range4 = [str rangeOfString:@"หาก"];
    
    if(range1.length != 0 && range2.length != 0 && range3.length != 0 && range4.length != 0) {
        
        NSMutableString *str1 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(0, range1.location)]];
        NSMutableString *str2 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(range1.location, range2.location - str1.length)]];
        NSMutableString *str3 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(range2.location, range3.location - str1.length - str2.length)]];
        NSMutableString *str4 = [[NSMutableString alloc] initWithString:[str substringWithRange:NSMakeRange(range3.location, range4.location - str1.length - str2.length - str3.length)]];
        NSMutableString *str5 = [[NSMutableString alloc] initWithString:[str substringFromIndex:range4.location]];
        
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str1);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str2);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str3);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str4);
        CFStringTrimWhitespace((__bridge  CFMutableStringRef) str5);
        
        [resultStr appendString:str1];
        
        // adjust text "จำนวน"
        NSRange str2Range = [str2 rangeOfString:@" "];
        if(str2Range.length != 0) {
            NSMutableString *str2FirstSectionStr = [[NSMutableString alloc] initWithString:[str2 substringWithRange:NSMakeRange(0, str2Range.location)]];
            NSMutableString *str2SecondSectionStr = [[NSMutableString alloc] initWithString:[str2 substringFromIndex:str2Range.location + str2Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str2FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str2SecondSectionStr);
            
            [resultStr appendFormat:@"\n%@ : %@", str2FirstSectionStr, str2SecondSectionStr];
        }
        
        // adjust text "วันที่"
        NSRange str3Range = [str3 rangeOfString:@" "];
        if(str3Range.length != 0) {
            NSMutableString *str3FirstSectionStr = [[NSMutableString alloc] initWithString:[str3 substringWithRange:NSMakeRange(0, str3Range.location)]];
            NSMutableString *str3SecondSectionStr = [[NSMutableString alloc] initWithString:[str3 substringFromIndex:str3Range.location + str3Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str3FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str3SecondSectionStr);
            
            [resultStr appendFormat:@"\n%@ : %@", str3FirstSectionStr, str3SecondSectionStr];
        }
        
        // adjust text "ยอดคงเหลือ"
        NSRange str4Range = [str4 rangeOfString:@" "];
        if(str4Range.length != 0) {
            NSMutableString *str4FirstSectionStr = [[NSMutableString alloc] initWithString:[str4 substringWithRange:NSMakeRange(0, str4Range.location)]];
            NSMutableString *str4SecondSectionStr = [[NSMutableString alloc] initWithString:[str4 substringFromIndex:str4Range.location + str4Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str4FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) str4SecondSectionStr);
            
            [resultStr appendFormat:@"\n%@ : %@", str4FirstSectionStr, str4SecondSectionStr];
        }
        
        [resultStr appendFormat:@"\n%@", str5];
        
    }
    else {
        return str;
    }
    
    return resultStr;
    
}

-(NSString *)adjustAttendaceText:(NSString *)str {
    
    //NSString *str = @"ถึงโรงเรียนเวลา 00:04 น. สถานะ เข้าตรงเวลา";
    NSRange range1 = [str rangeOfString:@"สถานะ"];
    
    if(range1.length != 0) {
        
        // Substring first section
        NSString *firstSectionStr = [str substringWithRange:NSMakeRange(0, range1.location)];
        NSRange sec1Range = [firstSectionStr rangeOfString:@" "];
        
        if(sec1Range.length != 0) {
            
            NSMutableString *sec1FirstSectionStr = [[NSMutableString alloc] initWithString:[firstSectionStr substringWithRange:NSMakeRange(0, sec1Range.location)]];
            NSMutableString *sec1SecondSectionStr = [[NSMutableString alloc] initWithString:[firstSectionStr substringFromIndex:sec1Range.location + sec1Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec1FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec1SecondSectionStr);
            
            firstSectionStr = [NSString stringWithFormat:@"%@ : %@", sec1FirstSectionStr, sec1SecondSectionStr];
        }
        
        // Substring second section
        NSString *secondSectionStr = [str substringFromIndex:range1.location];
        NSRange sec2Range = [secondSectionStr rangeOfString:@" "];
        
        if(sec2Range.length != 0) {
            
            NSMutableString *sec2FirstSectionStr = [[NSMutableString alloc] initWithString:[secondSectionStr substringWithRange:NSMakeRange(0, sec2Range.location)]];
            NSMutableString *sec2SecondSectionStr = [[NSMutableString alloc] initWithString:[secondSectionStr substringFromIndex:sec2Range.location + sec2Range.length]];
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec2FirstSectionStr);
            CFStringTrimWhitespace((__bridge CFMutableStringRef) sec2SecondSectionStr);
            
            secondSectionStr = [NSString stringWithFormat:@"%@ : %@", sec2FirstSectionStr, sec2SecondSectionStr];
        }
        
        NSString *resultString;
        
        if(firstSectionStr != nil && firstSectionStr.length > 0 && secondSectionStr != nil && secondSectionStr.length > 0) {
            resultString = [NSString stringWithFormat:@"%@\n%@", firstSectionStr, secondSectionStr];
        }
        else if(firstSectionStr != nil && firstSectionStr.length > 0) {
            resultString = [NSString stringWithFormat:@"%@", firstSectionStr];
        }
        else if(secondSectionStr != nil && secondSectionStr.length > 0) {
            resultString = [NSString stringWithFormat:@"%@", secondSectionStr];
        }
        
        return resultString;
        
    }
    else {
        return str;
    }
    
}

- (void)hideTableView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.tableView setAlpha:0.0];
    [self.contentView setAlpha:1.0];
    [UIView commitAnimations];
    
    isTableViewShowing = NO;
    
}

- (void)showTableView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.tableView setAlpha:1.0];
    [self.contentView setAlpha:0.0];
    [UIView commitAnimations];
    
    isTableViewShowing = YES;
}

#pragma mark - Pager

- (void)setupPageMenu {
    UserViewController *controller1 = [[UserViewController alloc] init];
    controller1.title = @"ข้อมูลส่วนตัว";
    
    StudyViewController *controller2 = [[StudyViewController alloc] init];
    controller2.title = @"ด้านการเรียน";
    
    NSArray *controllerArray = @[controller1, controller2];
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionMenuItemSeparatorWidth: @(4.3),
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                                 CAPSPageMenuOptionMenuMargin: @(20),
                                 CAPSPageMenuOptionMenuHeight: @(40),
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"ThaiSansNeue-SemiBold" size:22.0],
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                                 CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                                 CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    // For the first time set first tab background to orange and label color to white
    NSArray *menuItems = _pageMenu.menuItems;
    MenuItemView *menuItemView = [menuItems objectAtIndex:_pageMenu.currentPageIndex];
    menuItemView.backgroundColor = OrangeColor;
    menuItemView.titleLabel.textColor = [UIColor whiteColor];
    
    //Optional delegate
    _pageMenu.delegate = self;
    
    [self.contentView addSubview:_pageMenu.view];
    
}

#pragma CAPSPageMenuDelegate

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
    NSArray *menuItems = _pageMenu.menuItems;
    
    int count = 0;
    
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = OrangeColor;
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        
        count++;
    }
    
}

#pragma mark - CallGetUnreadMessageCountAPIDelegate

- (void)callGetUnreadMessageCountAPI:(CallGetUnreadMessageCountAPI *)classObj unreadCount:(NSInteger)unreadCount success:(BOOL)success {
    
    if(success) {
        [self.badgeView setBadgeValue:unreadCount];
    }
    else {
        [self.badgeView setBadgeValue:0];
    }
}

@end
