//
//  NavChildTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 10/21/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavChildTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *navLabel;

@end
