//
//  AddAccountMainViewController.m
//  JabjaiApp
//
//  Created by mac on 8/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

@import AirshipKit;

#import "AddAccountMainViewController.h"
#import "SWRevealViewController.h"
#import "UserInfoDBHelper.h"
#import "MultipleUserModel.h"
#import "SettingViewController.h"
#import "AddAccountLoginViewController.h"
#import "UserData.h"

@interface AddAccountMainViewController () {
    NSMutableArray<MultipleUserModel *> *userArray;
    
    NSInteger selectedIndex;
    
    //The dialog
    ConfirmDialog *confirmDialog;
}

@property (nonatomic, strong) UserInfoDBHelper *dbHelper;

@end

static NSString *cellIdentifier = @"Cell";

@implementation AddAccountMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DelUserAccountTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    selectedIndex = -1;
    
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    [self updateUserList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(userArray != nil) {
        return userArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DelUserAccountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    MultipleUserModel *model = [userArray objectAtIndex:indexPath.row];
    
    if([model getImageUrl] == nil || [[model getImageUrl] length] == 0) {
        
        if([model getGender] == 0) { // Male
            cell.imgUser.image = [UIImage imageNamed:@"ic_user_info"];
        }
        else { // Female
            cell.imgUser.image = [UIImage imageNamed:@"ic_user_women"];
        }
    }
    else {
        [cell.imgUser loadImageFromURL:[model getImageUrl]];
    }
    
    if([model getSlaveId] == [UserData getMasterUserID]) {
        cell.btnDel.hidden = YES;
    }
    else {
        cell.btnDel.hidden = NO;
    }
    
    NSString *name = [[NSString alloc] initWithFormat:@"%@ %@", [model getFirstName], [model getLastName]];
    cell.titleLabel.text = name;
    cell.index = indexPath.row;
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

#pragma mark - DelUserAccountTableViewCellDelegate

- (void)onDeleteUser:(DelUserAccountTableViewCell *)tableViewCell atIndex:(NSInteger)index {
    selectedIndex = index;
    [self showConfirmDialog];
}

#pragma mark - ConfirmDialogDelegate

- (void)onConfirmDialog:(ConfirmDialog *)confirmDialog confirm:(BOOL)confirm {
    if(confirm && selectedIndex >= 0) {
        MultipleUserModel *model = [userArray objectAtIndex:selectedIndex];
        
        [self.dbHelper deleteUserWithMasterId:[UserData getMasterUserID] slaveId:[model getSlaveId]];
        
        NSString *tag = [NSString stringWithFormat:@"%lld", [model getSlaveId]];
        [[UAirship push] removeTag:tag];
        [[UAirship push] updateRegistration];
        
        [self updateUserList];
        [self.tableView reloadData];
    }
}

#pragma mark - Dialog

- (void)showConfirmDialog {
    
    if(confirmDialog == nil) {
        confirmDialog = [[ConfirmDialog alloc] init];
        confirmDialog.delegate = self;
    }
    else if(confirmDialog != nil && [confirmDialog isDialogShowing]) {
        [confirmDialog dismissDialog];
    }
    
    [confirmDialog showDialogInView:self.view title:@"แจ้งเตือน" message:@"ยืนยันการลบบัญชีผุ้ใช้ ?"];
}

#pragma mark - Utility

- (void)updateUserList {
    NSArray *users = [self.dbHelper getUserDataWithMasterId:[UserData getMasterUserID]];
    
    if(userArray != nil) {
        [userArray removeAllObjects];
        userArray = nil;
    }
    userArray = [[NSMutableArray alloc] init];
    
    if(users != nil) {
        for(MultipleUserModel *model in users) {
            if([model getSlaveId] != [UserData getUserID]) {
                [userArray addObject:model];
            }
        }
    }

}

#pragma mark - Action functions

- (IBAction)moveBack:(id)sender {
    SettingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingStoryboard"];
    [self.revealViewController pushFrontViewController:viewController animated:YES];

}

- (IBAction)actionAddAccount:(id)sender {
    
    AddAccountLoginViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAccountLoginStoryboard"];
    viewController.userArray = userArray;
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
@end
