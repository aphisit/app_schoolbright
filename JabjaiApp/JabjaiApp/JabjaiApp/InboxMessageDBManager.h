//
//  InboxMessageDBManager.h
//  JabjaiInboxMessageDBApp
//
//  Created by mac on 11/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageInboxDataModel.h"

@interface InboxMessageDBManager : NSObject

-(instancetype)init;

-(BOOL)insertMessage:(MessageInboxDataModel *)dataModel;
-(BOOL)insertMultipleMessage:(NSArray *)dataArr;
-(void)deleteMessageWithID:(int)messageID userID:(int)userID;
-(void)updateMessageStatusWithID:(int)messageID status:(int)status userID:(int)userID;
-(BOOL)isMessageIDExistsInDatabase:(int)messageID;
-(NSArray *)getMessageWithType:(int)type userID:(int)userID;
-(NSArray *)getAllMessageWithUserID:(int)userID;
-(MessageInboxDataModel *)getMessageWithID:(int)messageID userID:(int)userID;
-(int)getLatestMessageIDWithUserID:(int)userID;
-(void)deleteAllDataInTable;
-(int)getNumberOfUnreadMessageWithUserID:(int)userID;
-(int)getNumberOfUnreadMessageWithUserID:(int)userID typeID:(int)typeID;

@end
