//
//  UserInfoViewController.h
//  JabjaiApp
//
//  Created by mac on 5/17/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "JabjaiApp-Swift.h"
#import "CAPSPageMenu.h"

@interface UserInfoViewController : UIViewController <CAPSPageMenuDelegate, UITableViewDataSource, UITableViewDelegate>

// Deep link to open notification message
@property (nonatomic) long long messageID;
@property (nonatomic) BOOL showMessageDialogWhenOpen;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *notificationButton;

- (IBAction)openDrawer:(id)sender;
- (IBAction)showNotification:(id)sender;
@end
