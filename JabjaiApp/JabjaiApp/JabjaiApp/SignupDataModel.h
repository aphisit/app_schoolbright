//
//  SignupDataModel.h
//  JabjaiApp
//
//  Created by mac on 10/26/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignupDataModel : NSObject

@property (nonatomic, strong) NSNumber *type; //Type 0 -> Student, Type 1 -> Teacher, Type 2 -> Staff
@property (nonatomic, strong) NSNumber *academyType; //Type 1 -> School, Type 2 -> University
@property (nonatomic, strong) NSNumber *gender; //Gender 0 -> Male, Gender 1 -> Female
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSDate *birthDay;
@property (nonatomic, strong) NSString *personalID;
@property (nonatomic, strong) NSString *studentID;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *creditLimit;
@property (nonatomic, strong) NSNumber *schoolID;
@property (nonatomic, strong) NSString *school;
@property (nonatomic, strong) NSNumber *degreeID;
@property (nonatomic, strong) NSString *degree;
@property (nonatomic, strong) NSNumber *gradID;
@property (nonatomic, strong) NSString *grade;
@property (nonatomic, strong) NSNumber *classroomID;
@property (nonatomic, strong) NSString *classroom;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSNumber *provinceID;
@property (nonatomic, strong) NSString *district;
@property (nonatomic, strong) NSNumber *districtID;
@property (nonatomic, strong) NSString *subDistrict;
@property (nonatomic, strong) NSNumber *subDistrictID;
@property (nonatomic, strong) NSString *postalCode;

// These properties use for university registration
@property (nonatomic, strong) NSString *faculty;
@property (nonatomic, strong) NSNumber *facultyID;
@property (nonatomic, strong) NSString *universityDegree;
@property (nonatomic, strong) NSNumber *universityDegreeID;
@property (nonatomic, strong) NSString *major;
@property (nonatomic, strong) NSNumber *majorID;

@end
