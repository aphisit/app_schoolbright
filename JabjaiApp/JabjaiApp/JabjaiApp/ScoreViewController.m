//
//  ScoreViewController.m
//  JabjaiApp
//
//  Created by mac on 10/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "ScoreViewController.h"
#import "APIURL.h"
#import "CreditBureauSummaryModel.h"
#import "UserData.h"
#import "Utils.h"
#import "Constant.h"

@interface ScoreViewController () {
    NSMutableArray *dataArr;
    
    UIColor *gradAColor;
    UIColor *gradBColor;
    UIColor *gradCColor;
    UIColor *gradDColor;
    UIColor *gradFColor;
    
    NSInteger connectCounter;
}

@end

@implementation ScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    gradAColor = [[UIColor alloc] initWithRed:243/255.0 green:152/255.0 blue:23/255.0 alpha:1.0];
    gradBColor = [[UIColor alloc] initWithRed:30/255.0 green:150/255.0 blue:78/255.0 alpha:1.0];
    gradCColor = [[UIColor alloc] initWithRed:58/255.0 green:112/255.0 blue:245/255.0 alpha:1.0];
    gradDColor = [[UIColor alloc] initWithRed:242/255.0 green:131/255.0 blue:140/255.0 alpha:1.0];
    gradFColor = [[UIColor alloc] initWithRed:209/255.0 green:50/255.0 blue:47/255.0 alpha:1.0];
    
    connectCounter = 0;
    
    if([UserData getAcademyType] == SCHOOL) {
        [self getCreditBureauSummaryData];
    }
    
    [self getUserInfoData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    
    CGFloat width = self.gradLabel.frame.size.width;
    CGFloat heigh = self.gradLabel.frame.size.height;
    float max = fmaxf((float)width, (float)heigh);
    self.gradLabel.layer.cornerRadius = max/2;
    self.gradLabel.layer.masksToBounds = YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma CustomFunctions

- (void)updateGradLabel:(NSString *)grad {
    self.gradLabel.text = grad;
    
    if([grad isEqualToString:@"A"] || [grad isEqualToString:@"A+"]) {
        self.gradLabel.backgroundColor = gradAColor;
    }
    else if([grad isEqualToString:@"B"] || [grad isEqualToString:@"B+"]) {
        self.gradLabel.backgroundColor = gradBColor;
    }
    else if([grad isEqualToString:@"C"] || [grad isEqualToString:@"C+"]) {
        self.gradLabel.backgroundColor = gradCColor;
    }
    else if([grad isEqualToString:@"D"] || [grad isEqualToString:@"D+"]) {
        self.gradLabel.backgroundColor = gradDColor;
    }
    else {
        self.gradLabel.backgroundColor = gradFColor;
    }
    
    //NSLog([NSString stringWithFormat:@"Max : %.2f", max]);
}

#pragma mark - GetAPIData

-(void)getCreditBureauSummaryData {
    NSString *URLString = [APIURL getCreditBureauSummaryURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    NSLog(@"%@", URLString);
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getCreditBureauSummaryData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getCreditBureauSummaryData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getCreditBureauSummaryData];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                dataArr = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *firstName = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sName"]];
                    NSMutableString *lastName = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"sLastname"]];
                    NSMutableString *statusStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"StatusIN"]];
                    NSInteger times = [[dataDict objectForKey:@"COUNTSTATUS"] integerValue];
                    NSInteger creditLimits = [[dataDict objectForKey:@"nMax"] integerValue];
                    NSInteger money = [[dataDict objectForKey:@"nMoney"] integerValue];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)firstName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)lastName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)statusStr);
                    
                    NSInteger status = 0;
                    
                    if([statusStr isKindOfClass:[NSNull class]] || [statusStr length] == 0) {
                        status = 3; //Absence
                    }
                    else {
                        status = [statusStr integerValue];
                    }
                    
                    CreditBureauSummaryModel *model = [[CreditBureauSummaryModel alloc] init];
                    model.status = [[NSNumber alloc] initWithInteger:status];
                    model.times = [[NSNumber alloc] initWithInteger:times];
                    model.creditLimits = [[NSNumber alloc] initWithInteger:creditLimits];
                    model.money = [[NSNumber alloc] initWithInteger:money];
                    model.firstName = firstName;
                    model.lastName = lastName;
                    
                    [dataArr addObject:model];
                }
                
                [self performData];
                
            }
        }
    }];
    
}

-(void)performData {
    NSInteger total = 0;
    NSInteger score = 0;
    double percent = 0;
    
    NSInteger statusOntime = 0;
    NSInteger statusLate = 0;
    NSInteger statusAbsence = 0;
    
    NSString *firstName;
    NSString *lastName;
    
    for(int i=0; i<dataArr.count; i++) {
        CreditBureauSummaryModel *model = [dataArr objectAtIndex:i];
        
        if([model.status integerValue] == 0) {
            statusOntime = [model.times integerValue];
        }
        else if([model.status integerValue] == 1) {
            statusLate = [model.times integerValue];
        }
        else if([model.status integerValue] == 3) {
            statusAbsence = [model.times integerValue];
        }
        
        if(firstName == nil) {
            firstName = model.firstName;
        }
        
        if(lastName == nil) {
            lastName = model.lastName;
        }
    }
    
    total = statusOntime + statusLate + statusAbsence;
    score = (statusOntime * 3) + statusLate;
    percent = (score/(double)(total * 3)) * 100;
    
    NSLog(@"Total: %i, Score: %i, Percentage: %g", total, score, percent);
    
    NSString *grad;
    
    if(percent < 50) {
        grad = @"F";
    }
    else if(percent < 60) {
        grad = @"D";
    }
    else if(percent < 70) {
        grad = @"D+";
    }
    else if(percent <= 75) {
        grad = @"C";
    }
    else if(percent <= 80) {
        grad = @"C+";
    }
    else if(percent <= 85) {
        grad = @"B";
    }
    else if(percent <= 90) {
        grad = @"B+";
    }
    else if(percent <= 95) {
        grad = @"A";
    }
    else {
        grad = @"A+";
    }
    
    [self updateGradLabel:grad];
    
    self.nameLabel.text = [[NSString alloc] initWithFormat:@"ชื่อ : %@ %@", firstName, lastName];
}

- (void)getUserInfoData {
    NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUserInfoData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(returnedArray.count != 0) {
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    NSMutableString *firstName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sName"]];
                    NSMutableString *lastName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sLastname"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) firstName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lastName);
                    
                    self.nameLabel.text = [[NSString alloc] initWithFormat:@"ชื่อ : %@ %@", firstName, lastName];
                }
            }
            
        }
        
    }];
    
}


@end
