//
//  CallGetTAHistoryStudentStatusListAPI.m
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetTAHistoryStudentStatusListAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallGetTAHistoryStudentStatusListAPI () {
    NSInteger connectCounter;
    NSMutableArray<TAReportStudentStatusModel *> *studentStatusModelArray;
}

@end
@implementation CallGetTAHistoryStudentStatusListAPI

- (instancetype)init {
    self = [super init];
    
    if(self) {
        connectCounter = 0;
    }
    
    return self;
}

- (void)call:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId date:(NSDate *)date {
    [self getDataFromServer:schoolId classroomId:classroomId subjectId:subjectId date:date];
}

- (void)getDataFromServer:(long long)schoolId classroomId:(long long)classroomId subjectId:(NSString *)subjectId date:(NSDate *)date {
    
    NSString *urlString = [APIURL getTAHistoryStudentStatusListWithSchoolId:schoolId classroomId:classroomId subjectId:subjectId date:date];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataFromServer:schoolId classroomId:classroomId subjectId:subjectId date:date];
                
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId classroomId:classroomId subjectId:subjectId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataFromServer:schoolId classroomId:classroomId subjectId:subjectId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else {

                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(studentStatusModelArray != nil) {
                    [studentStatusModelArray removeAllObjects];
                    studentStatusModelArray = nil;
                }
                
                studentStatusModelArray = [[NSMutableArray alloc] init];
                
                if(returnedArray.count > 0) {
                    NSDictionary *dict = [returnedArray objectAtIndex:0];
                    NSArray *dataArray = [dict objectForKey:@"ScheduleScan4Level2"];
                    
                    if(dataArray != nil) {
                        for(int i=0; i<dataArray.count; i++) {
                            NSDictionary *dataDict = [dataArray objectAtIndex:i];
                            
                            long long studentId = [[dataDict objectForKey:@"studentid"] longLongValue];
                            NSString *studentName = [dataDict objectForKey:@"studentname"];
                            NSInteger status = [[dataDict objectForKey:@"status"] integerValue];
                            
                            TAReportStudentStatusModel *model = [[TAReportStudentStatusModel alloc] init];
                            [model setStudentId:studentId];
                            [model setStudentName:studentName];
                            [model setStatus:status];
                            
                            [studentStatusModelArray addObject:model];
                        }
                    }
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTAHistoryStudentStatusListAPI:data:success:)]) {
                    [self.delegate callGetTAHistoryStudentStatusListAPI:self data:studentStatusModelArray success:YES];
                }
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callGetTAHistoryStudentStatusListAPI:data:success:)]) {
                [self.delegate callGetTAHistoryStudentStatusListAPI:self data:nil success:NO];
            }
        }
    }];
}
@end
