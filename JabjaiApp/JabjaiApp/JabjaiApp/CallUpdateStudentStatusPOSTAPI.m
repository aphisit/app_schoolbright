//
//  CallUpdateStudentStatusPOSTAPI.m
//  JabjaiApp
//
//  Created by mac on 7/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallUpdateStudentStatusPOSTAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"
#import <AFNetworking/AFNetworking.h>

@implementation CallUpdateStudentStatusPOSTAPI

- (void)call:(long long)schoolId subjectId:(long long)subjectId teacherId:(long long)teacherId jsonString:(NSString *)jsonString {
    
    [self updateStudentStatus:schoolId subjectId:subjectId teacherId:teacherId jsonString:jsonString];
}

- (void)updateStudentStatus:(long long)schoolId subjectId:(long long)subjectId teacherId:(long long)teacherId jsonString:(NSString *)jsonString {
    
    NSString *URLString = [APIURL getUpdateStudentStatusPOSTWithSchoolId:schoolId subjectId:subjectId teacherId:teacherId];
    
    NSError *error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&error];
    
    if(error != nil) {
        NSLog(@"%@", @"Parse JSON data failed");
    }
    else {
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        
        [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"%@", responseObject);
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callUpdateStudentStatusPOSTAPI:response:success:)]) {
                NSString *response = responseObject;
                [self.delegate callUpdateStudentStatusPOSTAPI:self response:response success:YES];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(callUpdateStudentStatusPOSTAPI:response:success:)]) {
                [self.delegate callUpdateStudentStatusPOSTAPI:self response:nil success:NO];
            }
            
        }];
        
    }
}

@end
