//
//  BSReportForStudentDetailViewController.m
//  JabjaiApp
//
//  Created by mac on 7/27/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSReportForStudentDetailViewController.h"
#import "BSReportForStudentMainViewController.h"
#import "Utils.h"

@interface BSReportForStudentDetailViewController () {
    UIColor *greenColor;
    UIColor *redColor;
    
    NSDateFormatter *timeFormatter;
}

@end

@implementation BSReportForStudentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    greenColor = [UIColor colorWithRed:12/255.0 green:190/255.0 blue:64/255.0 alpha:1.0];
    redColor = [UIColor colorWithRed:255/255.0 green:57/255.0 blue:56/255.0 alpha:1.0];
    
    timeFormatter = [Utils getDateFormatter];
    [timeFormatter setDateFormat:@"HH:mm 'น.'"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewDidLayoutSubviews {
    
    CGFloat max = MAX(self.scoreLabel.frame.size.width, self.scoreLabel.frame.size.height);
    self.scoreLabel.layer.cornerRadius = max / 2.0;
    self.scoreLabel.layer.masksToBounds = YES;
    
    [self updateUI];
}

- (void)updateUI {
    
    if(self.selectedHistoryModel != nil) {
        self.behaviorNameLabel.text = [self.selectedHistoryModel getBehaviorName];
        self.datetimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:[self.selectedHistoryModel getDateTime]], [timeFormatter stringFromDate:[self.selectedHistoryModel getDateTime]]];
        
        if([self.selectedHistoryModel getBehaviorType] == 0) { // type add
            self.scoreLabel.text = [NSString stringWithFormat:@"+%ld", [self.selectedHistoryModel getBehaviorScore]];
            self.scoreLabel.backgroundColor = greenColor;
        }
        else { // type reduce
            self.scoreLabel.text = [NSString stringWithFormat:@"-%ld", [self.selectedHistoryModel getBehaviorScore]];
            self.scoreLabel.backgroundColor = redColor;
        }
        
        if([self.selectedHistoryModel getRecorder] != nil && [[self.selectedHistoryModel getRecorder] length] > 0) {
            self.recorderLabel.text = [self.selectedHistoryModel getRecorder];
        }
        else {
            self.recorderLabel.text = @"-";
        }
        
        if([self.selectedHistoryModel getRemark] != nil && [[self.selectedHistoryModel getRemark] length] > 0) {
            self.remarkLabel.text = [self.selectedHistoryModel getRemark];
        }
        else {
            self.remarkLabel.text = @"-";
        }
        
        self.remainingScoreLabel.text = [NSString stringWithFormat:@"%ld", _remainingScore];
    }
    else {
        self.behaviorNameLabel.text = nil;
        self.datetimeLabel.text = nil;
        self.scoreLabel.text = @"+0";
        self.scoreLabel.backgroundColor = greenColor;
        self.recorderLabel.text = @"-";
        self.remarkLabel.text = @"-";
        self.remainingScoreLabel.text = @"0";
    }
}

#pragma mark - Action functions

- (IBAction)moveBack:(id)sender {
    
    BSReportForStudentMainViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSReportForStudentMainStoryboard"];
    
    viewController.bsHistoryArray = _bsHistoryArray;
    viewController.remainingScore = _remainingScore;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
}
@end
