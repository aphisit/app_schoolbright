//
//  SNHomeWorkViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/1/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNHomeWorkViewController : UIViewController{}

@property (weak, nonatomic) IBOutlet UITextView *detailHomeWork;
@property (weak, nonatomic) IBOutlet UIView *viewFile;
@property (weak, nonatomic) IBOutlet UIView *imageViewFile;
@property (weak, nonatomic) IBOutlet UILabel *nameFile;
- (IBAction)selectFile:(id)sender;
- (IBAction)selectImage:(id)sender;



@end
