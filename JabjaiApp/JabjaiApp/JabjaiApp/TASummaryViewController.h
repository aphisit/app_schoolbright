//
//  TASummaryViewController.h
//  JabjaiApp
//
//  Created by mac on 7/13/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "TAStudentStatusModel.h"
#import "CallUpdateStudentStatusPOSTAPI.h"
#import "AlertDialog.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "SSubjectModel.h"

@interface TASummaryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CallUpdateStudentStatusPOSTAPIDelegate, AlertDialogDelegate>

// Global variables
// TASelectClassLevelViewController
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SSubjectModel *> *subjectArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
@property (nonatomic) NSInteger mode;

// TAStudentListViewController
@property (strong, nonatomic) NSArray<TAStudentStatusModel *> *studentStatusArray;
@property (assign, nonatomic) long long classroomId;
@property (assign, nonatomic) long long subjectId;
@property (assign, nonatomic) NSString *subjectName;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)moveBack:(id)sender;
- (IBAction)actionSubmit:(id)sender;

@end
