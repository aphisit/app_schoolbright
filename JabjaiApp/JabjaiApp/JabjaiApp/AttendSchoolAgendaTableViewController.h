//
//  AttendSchoolAgendaTableViewController.h
//  JabjaiApp
//
//  Created by mac on 10/24/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Attend2SchoolFilter.h"

@interface AttendSchoolAgendaTableViewController : UITableViewController

-(void)applyFilter:(Attend2SchoolFilter *)filter;

@end
