//
//  SSubjectTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 5/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "SSubjectTableViewCell.h"

@implementation SSubjectTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat max = MAX(self.redView.frame.size.width, self.redView.frame.size.height);
    self.redView.layer.cornerRadius = max/2.0;
    self.redView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
