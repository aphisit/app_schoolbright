//
//  TeachingSubjectViewController.h
//  JabjaiApp
//
//  Created by mac on 12/29/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeachingSubjectDialog.h"

@interface TeachingSubjectViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, TeachingSubjectDialogDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblView;

- (IBAction)openDrawer:(id)sender;
- (IBAction)showFilterDialog:(id)sender;

@end
