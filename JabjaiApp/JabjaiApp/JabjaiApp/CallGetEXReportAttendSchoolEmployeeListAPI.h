//
//  CallGetEXReportAttendSchoolEmployeeListAPI.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXReportNameStatusModel.h"

@class CallGetEXReportAttendSchoolEmployeeListAPI;

@protocol CallGetEXReportAttendSchoolEmployeeListAPIDelegate <NSObject>

@optional
- (void)callGetEXReportAttendSchoolEmployeeListAPI:(CallGetEXReportAttendSchoolEmployeeListAPI *)classObj data:(NSArray<EXReportNameStatusModel *> *)data success:(BOOL)success;

@end

@interface CallGetEXReportAttendSchoolEmployeeListAPI : NSObject

@property (nonatomic, weak) id<CallGetEXReportAttendSchoolEmployeeListAPIDelegate> delegate;

- (void)call:(long long)schoolId date:(NSDate *)date;

@end
