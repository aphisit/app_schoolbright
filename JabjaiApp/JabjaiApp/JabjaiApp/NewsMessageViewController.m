//
//  NewsMessageViewController.m
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NewsMessageViewController.h"
#import "NewsMessageDataProvider.h"
#import "NewsMessageTableViewCell.h"
#import "ProgressTableViewCell.h"
#import "MessageInboxDataModel.h"
#import "Utils.h"

const NSUInteger NewsMessageVCPreloadMargin = 5;

static NSString *dataCellIdentifier = @"MessageCell";
static NSString *loaderCellIdentifier = @"ProgressCell";

@interface NewsMessageViewController () <NewsMessageDataProviderDelegate>

@property NewsMessageDataProvider *_dataProvider;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;

@end

@implementation NewsMessageViewController {
    NSIndexPath *_loaderCellIndexPath;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NewsMessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:dataCellIdentifier];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ProgressTableViewCell class]) bundle:nil] forCellReuseIdentifier:loaderCellIdentifier];
    
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    [self setDataProvider:[NewsMessageDataProvider new]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Accessors/Users/mac/Desktop/New iOS Project/JabJaiAppProject/JabjaiApp/JabjaiApp
- (void)setDataProvider:(NewsMessageDataProvider *)dataProvider {
    if(dataProvider != self._dataProvider) {
        self._dataProvider = dataProvider;
        self._dataProvider.delegate = self;
        self._dataProvider.shouldLoadAutomatically = YES;
        self._dataProvider.automaticPreloadMargin = NewsMessageVCPreloadMargin;
        
        if([self isViewLoaded]) {
            [self.tableView reloadData];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self._dataProvider.dataObjects != nil) {
        return self._dataProvider.dataObjects.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier;
    NSUInteger index = indexPath.row;
    
    if([self._dataProvider isLoadingDataAtIndex:index]) {
        cellIdentifier = loaderCellIdentifier;
        _loaderCellIndexPath = indexPath;
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        return cell;
    }
    else {
        cellIdentifier = dataCellIdentifier;
        
        NewsMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        id dataObject = self._dataProvider.dataObjects[indexPath.row];
        
        if(![dataObject isKindOfClass:[NSNull class]]) {
            
            MessageInboxDataModel *model = dataObject;
            
            cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:model.date], [self.timeFormatter stringFromDate:model.date]];
            cell.titleLabel.text = model.title;
            cell.messageLabel.text = model.message;

        }
        
        return cell;
        
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == self._dataProvider.loadedCount) {
        [self._dataProvider loadDataForIndex:indexPath.row];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

#pragma mark - Data controller delegate

- (void)dataProvider:(NewsMessageDataProvider *)dataProvider didLoadDataAtPage:(NSUInteger)page indexes:(NSIndexSet *)indexes {
    NSMutableArray *indexPathsToReload = [NSMutableArray array];
    
    [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
        
        if([self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
            [indexPathsToReload addObject:indexPath];
        }
    }];
    
    if(indexPathsToReload.count > 0) {
        [self.tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationFade];
    }

}

@end
