//
//  TAReportStudentStatusModel.m
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TAReportStudentStatusModel.h"

@implementation TAReportStudentStatusModel

@synthesize studentId = _studentId;
@synthesize studentName = _studentName;
@synthesize status = _status;

- (void)setStudentId:(long long)studentId {
    _studentId = studentId;
}

- (void)setStudentName:(NSString *)studentName {
    _studentName = studentName;
}

- (void)setStatus:(NSInteger)status {
    _status = status;
}

- (long long)getStudentId {
    return _studentId;
}

- (NSString *)getStudentName {
    return _studentName;
}

- (NSInteger)getStatus {
    return _status;
}

@end
