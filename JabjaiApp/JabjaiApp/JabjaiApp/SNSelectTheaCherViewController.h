//
//  SNSelectTheaCherViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TheacherAllModel.h"
#import "BSStudentNameRadioTableViewCell.h"
#import "BSSelectedStudent.h"
#import "CallGetSNTheacherAll.h"
@interface SNSelectTheaCherViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
