 //
//  CallGetSNTheacherAll.m
//  JabjaiApp
//
//  Created by toffee on 9/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallGetSNTheacherAll.h"
#import "APIURL.h"
#import "UserData.h"
#import "Utils.h"
@interface CallGetSNTheacherAll ()

@end
NSMutableArray<TheacherAllModel *> *addTheacherArrray;
@implementation CallGetSNTheacherAll

- (void)call:(long long)schoolId {
   // connectCounter = 0;
    [self getStudentClassLevelWithSchoolId:schoolId];
}

- (void)getStudentClassLevelWithSchoolId:(long long)schoolId {
    NSInteger schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getTheacherAllUrl:schoolID] ;
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        NSArray *returnedArray = returnedData;
        addTheacherArrray = [[NSMutableArray alloc] init];
        for(int i=0; i<returnedArray.count; i++) {
            NSDictionary *dataDict = [returnedArray objectAtIndex:i];
            
            long long theacherId = [[dataDict objectForKey:@"emplyoeesid"] longLongValue];
            
            NSMutableString *theacherName;
            
            if(![[dataDict objectForKey:@"emplyoeesname"] isKindOfClass:[NSNull class]]) {
                theacherName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"emplyoeesname"]];
            }
            else {
                theacherName = [[NSMutableString alloc] initWithString:@""];
            }
            
            CFStringTrimWhitespace((__bridge CFMutableStringRef) theacherName);
            
            TheacherAllModel *model = [[TheacherAllModel alloc] init];
            model.theaCherId = theacherId;
            model.theaCherName = theacherName;
            NSLog(@"NAME = %@",theacherName);
            //TheacherAllModel *model1 = [returnedArray objectAtIndex:i];
            [addTheacherArrray addObject:theacherName ];
            
        }
        
       
    }];
    

}


@end
