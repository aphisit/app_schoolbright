//
//  ReportLeaveAllDataSelectClassViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CFSCalendar.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "SlideMenuController.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "TableListDialog.h"

@interface ReportLeaveAllDataSelectClassViewController : UIViewController <UITextFieldDelegate,SlideMenuControllerDelegate,CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate, TableListDialogDelegate, CFSCalendarDelegate, CFSCalendarDelegateAppearance, CFSCalendarDataSource >

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *calendarContentView;

- (IBAction)moveBack:(id)sender;

@end
