//
//  ReportHomeWorkListViewController.h
//  JabjaiApp
//
//  Created by mac on 1/4/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

@import ARSlidingPanel;

#import <UIKit/UIKit.h>
#import "ReportHomeWorkCalendarViewController.h"
#import "ReportHomeWorkViewControllerDelegate.h"


@interface ReportHomeWorkListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, ReportHomeworkControllerDelegate>

@property (retain, nonatomic) id<ReportHomeWorkViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
