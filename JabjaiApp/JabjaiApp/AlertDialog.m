//
//  AlertDialog.m
//  JabjaiApp
//
//  Created by mac on 10/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "AlertDialog.h"
#import "UserData.h"
#import "Utils.h"

@interface AlertDialog (){
    NSBundle *myLangBundle;
}

@property (nonatomic, assign) BOOL isShowing;

@end

@implementation AlertDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Decorate view
   
    self.dialogView.layer.masksToBounds = YES;
    self.cancelBtn.layer.masksToBounds = YES;
    
    // Initialize variables
    self.isShowing = NO;
    self.messageLabel.lineBreakMode  = NSLineBreakByWordWrapping;
    self.messageLabel.numberOfLines = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradient;
    //set color header
    [self.cancelBtn layoutIfNeeded];
    gradient = [Utils getGradientColorStatus:@"red"];
    gradient.frame = self.cancelBtn.bounds;
    [self.cancelBtn.layer insertSublayer:gradient atIndex:0];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}



- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

#pragma mark - Dialog Functions
-(void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message {
    
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
        [targetView addSubview:self.view];
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height-20)];
        [targetView addSubview:self.view];
    }
    
//    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    
//    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    [self.messageLabel setAlpha:0.0];
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [self.messageLabel setAlpha:1.0];
    [UIView commitAnimations];
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    [_cancelBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_BTN_FLAG_CLOSE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    //[self.titleLabel setText:title];
    [self.messageLabel setText:message];
    
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [self.messageLabel setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogClose)]) {
        [self.delegate onAlertDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

@end
