//
//  CallGetDetailOfAcademicResultsAPI.h
//  JabjaiApp
//
//  Created by toffee on 19/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARDetailModel.h"
#import "ARSubjectScoreModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallGetDetailOfAcademicResultsAPI;
@protocol CallGetDetailOfAcademicResultsAPIDelegate <NSObject>
@optional
- (void)CallGetDetailOfAcademicResultsAPI:(CallGetDetailOfAcademicResultsAPI *)classObj data:(ARDetailModel *)data success:(BOOL)success;
@end

@interface CallGetDetailOfAcademicResultsAPI : NSObject
    @property (nonatomic, weak) id<CallGetDetailOfAcademicResultsAPIDelegate> delegate;
    - (void)call:(long long)schoolId studentId:(long long)studentId year:(NSString*)year term:(NSString*)term;
@end

NS_ASSUME_NONNULL_END
