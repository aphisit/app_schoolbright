//
//  CallGetEXReportClassroomAttendSchoolAPI.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXReportClassStatusModel.h"

@class CallGetEXReportClassroomAttendSchoolAPI;

@protocol CallGetEXReportClassroomAttendSchoolAPIDelegate <NSObject>

@optional
- (void)callGetEXReportClassroomAttendSchoolAPI:(CallGetEXReportClassroomAttendSchoolAPI *)classObj data:(NSArray<EXReportClassStatusModel *> *)data success:(BOOL)success;

@end

@interface CallGetEXReportClassroomAttendSchoolAPI : NSObject

@property (nonatomic, weak) id<CallGetEXReportClassroomAttendSchoolAPIDelegate> delegate;

- (void)call:(long long)schoolId classLevelId:(long long)classLevelId date:(NSDate *)date;

@end
