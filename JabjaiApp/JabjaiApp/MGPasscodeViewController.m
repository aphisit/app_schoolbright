//
//  MGPasscodeViewController.m
//  JabjaiApp
//
//  Created by toffee on 6/7/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "MGPasscodeViewController.h"
#import "MGListMargetViewController.h"
#import "UserData.h"
#import "UIView+Shake.h"
#import "Utils.h"
@interface MGPasscodeViewController (){
    
    NSString *mode;
    NSString *numberAddPin;
    NSString *numberConfirmPin;
    NSMutableArray  *numberArray;
    NSMutableString *numberCode;
}
@property (strong, nonatomic) CallAddPinAPI *callAddPinAPI;
@end

@implementation MGPasscodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setLangate];
    
    mode = ADD_PINCODE;
    numberArray = [[NSMutableArray alloc] init];
    numberCode = [[NSMutableString alloc] init];
    _confirmPinUnfair.hidden = YES;
    
    _passView1.layer.cornerRadius = _passView1.bounds.size.width/2;
    _passView1.layer.masksToBounds = YES;
    _passView1.layer.borderWidth = 1;
    
    _passView2.layer.cornerRadius = _passView2.bounds.size.width/2;
    _passView2.layer.masksToBounds = YES;
    _passView2.layer.borderWidth = 1;
    
    _passView3.layer.cornerRadius = _passView3.bounds.size.width/2;
    _passView3.layer.masksToBounds = YES;
    _passView3.layer.borderWidth = 1;
    
    _passView4.layer.cornerRadius = _passView4.bounds.size.width/2;
    _passView4.layer.masksToBounds = YES;
    _passView4.layer.borderWidth = 1;
    
    _passView5.layer.cornerRadius = _passView5.bounds.size.width/2;
    _passView5.layer.masksToBounds = YES;
    _passView5.layer.borderWidth = 1;
    
    _passView6.layer.cornerRadius = _passView6.bounds.size.width/2;
    _passView6.layer.masksToBounds = YES;
    _passView6.layer.borderWidth = 1;
   
}

- (void)viewDidLayoutSubviews{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

- (void)setLangate{
    self.headerPinLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_ADD_NEWPIN",nil,[Utils getLanguage],nil);
    [self.confirmPinUnfair setText:NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_PIN_INCORRECT",nil,[Utils getLanguage],nil)];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_SHOPPING",nil,[Utils getLanguage],nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)clearPin{
    mode = CONFIRM_PINCODE;
    [_passView1 setBackgroundColor:[UIColor clearColor]];
    [_passView2 setBackgroundColor:[UIColor clearColor]];
    [_passView3 setBackgroundColor:[UIColor clearColor]];
    [_passView4 setBackgroundColor:[UIColor clearColor]];
    [_passView5 setBackgroundColor:[UIColor clearColor]];
    [_passView6 setBackgroundColor:[UIColor clearColor]];
    numberAddPin = numberCode;
    numberArray = [[NSMutableArray alloc] init];
    numberCode = [[NSMutableString alloc] init];
    
    [_headerPinLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_MARKET_NEWPIN_AGAIN",nil,[Utils getLanguage],nil)];
    
    
}

- (void)doAddPinCode {
   
    if(self.callAddPinAPI != nil) {
        self.callAddPinAPI = nil;
    }
    self.callAddPinAPI = [[CallAddPinAPI alloc] init];
    self.callAddPinAPI.delegate = self;
    [self.callAddPinAPI call:[UserData getUserID] pinCode:numberConfirmPin schoolid:[UserData getSchoolId]] ;
}
-(void)callAddPinAPI:(CallAddPinAPI *)classObj status:(NSString *)status success:(BOOL)success{
    if ([status isEqualToString:@"Success"]) {
       
        MGListMargetViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MGListMargetViewStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
        
    }
}

//-(NSString *)createJsonString:(NSString*)pinCode{
//    NSString  *jsonString = [[NSString alloc] initWithFormat:@"{ \"Pin\" : \"%@\", \"User_Id\" : %d }", pinCode, [UserData getUserID] ];
//    return jsonString;
//}

-(void)checkAddAndConfirm{
    numberConfirmPin = numberCode;
    if ([numberAddPin isEqualToString:numberConfirmPin]) {
        [self doAddPinCode];
    }else{
        _confirmPinUnfair.hidden = NO;
        [self.passView1 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView2 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView3 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView4 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView5 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
        [self.passView6 shake:5   // 10 times
                    withDelta:5    // 5 points wide
         ];
    }
}

- (IBAction)actionButtonNumber:(UIControl*)sender {
    
    if (sender.tag != 10) {
        if (numberArray.count < 6) {
            [numberCode appendString:[NSString stringWithFormat:@"%d",sender.tag]];
            [numberArray addObject:[NSString stringWithFormat:@"%d",sender.tag]];
            
            if (numberArray.count == 1) {
                
                [_passView1 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 2) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 3) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 4) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 5) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 6) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
                [_passView6 setBackgroundColor:[UIColor grayColor]];
                
                if (mode == ADD_PINCODE) {
                    [self clearPin];
                }else{
                    [self checkAddAndConfirm];
                }
                
                
            }
            
        }
    }else{
        _confirmPinUnfair.hidden = YES;
        if (numberArray.count > 0) {
            [numberCode deleteCharactersInRange:NSMakeRange(numberArray.count-1, 1)];
            [numberArray removeObjectAtIndex:numberArray.count-1];
            [_passView1 setBackgroundColor:[UIColor clearColor]];
            [_passView2 setBackgroundColor:[UIColor clearColor]];
            [_passView3 setBackgroundColor:[UIColor clearColor]];
            [_passView4 setBackgroundColor:[UIColor clearColor]];
            [_passView5 setBackgroundColor:[UIColor clearColor]];
            [_passView6 setBackgroundColor:[UIColor clearColor]];
            if (numberArray.count == 1) {
                
                [_passView1 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 2) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 3) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 4) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 5) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
            }
            else if (numberArray.count == 6) {
                [_passView1 setBackgroundColor:[UIColor grayColor]];
                [_passView2 setBackgroundColor:[UIColor grayColor]];
                [_passView3 setBackgroundColor:[UIColor grayColor]];
                [_passView4 setBackgroundColor:[UIColor grayColor]];
                [_passView5 setBackgroundColor:[UIColor grayColor]];
                [_passView6 setBackgroundColor:[UIColor grayColor]];
            }
        }
    }
}

- (IBAction)openDarwer:(id)sender {
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
}
@end
