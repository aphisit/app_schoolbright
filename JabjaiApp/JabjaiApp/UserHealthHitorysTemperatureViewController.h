//
//  UserHealthHitorysTemperatureViewController.h
//  JabjaiApp
//
//  Created by Mac on 23/6/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserHealthHistoryModel.h"
#import "UserHealthTableViewCell.h"

#import "SelectOneDayCalendarViewController.h"

#import "CallUHGetHistoryHealthAPI.h"
NS_ASSUME_NONNULL_BEGIN
@protocol UserHealthHitorysTemperatureDelegate <NSObject>

-(void)userHealthHitorysTemperatureViewController;

@end

@interface UserHealthHitorysTemperatureViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,CallUHGetHistoryHealthAPIDelegate,SelectOneDayCalendarDelegate>
@property (weak, nonatomic) id<UserHealthHitorysTemperatureDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

-(IBAction)moveBackAction:(id)sender;
-(void)showPageHitorysTemperatuer:(UIView *)targetView;
- (IBAction)chooseDateAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
