//
//  ReportUnCheckListDataModel.h
//  JabjaiApp
//
//  Created by toffee on 21/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportUnCheckListDataModel : NSObject
@property (nonatomic) NSString* level;
@property (nonatomic) NSString *teacherName;

- (void)setLevel:(NSString * _Nonnull)level;
- (void)setTeacherName:(NSString * _Nonnull)teacherName;

- (NSString*)getLevel;
- (NSString*)getTeacher;
@end

NS_ASSUME_NONNULL_END
