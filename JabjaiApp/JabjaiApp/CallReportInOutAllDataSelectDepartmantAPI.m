//
//  CallReportInOutAllDataSelectDepartmantAPI.m
//  JabjaiApp
//
//  Created by toffee on 15/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallReportInOutAllDataSelectDepartmantAPI.h"
#import "APIURL.h"
#import "Utils.h"

@interface CallReportInOutAllDataSelectDepartmantAPI () {
    NSInteger connectCounter;
    NSMutableArray <ReportInOutAllDataSelectDepartmantModel *> *dataDepartmantArray;
    
}
@end
@implementation CallReportInOutAllDataSelectDepartmantAPI
- (void) call:(long long)schoolId date:(NSString *)date{
    [self getDataDepartmant:schoolId date:date];
}

- (void) getDataDepartmant:(long long)schoolId date:(NSString *)date{
    NSString *URLString = [APIURL getReportDataDepartmant:schoolId date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getDataDepartmant:schoolId date:date];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataDepartmant:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getDataDepartmant:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                NSString *departmantName;

                if(dataDepartmantArray != nil) {
                    [dataDepartmantArray removeAllObjects];
                    dataDepartmantArray = nil;
                }
                
                dataDepartmantArray = [[NSMutableArray alloc] init];
                for (int i = 0; i < returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];

                    long long departmantId = [[dataDict objectForKey:@"department_Id"] longLongValue];
                    NSInteger amountDepartmant = [[dataDict objectForKey:@"department_Number"] integerValue];
                    NSDictionary *statusArray = [dataDict objectForKey:@"statuses"];

                    if(![[dataDict objectForKey:@"department_Name"] isKindOfClass:[NSNull class]]) {

                        departmantName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"department_Name"]];
                    }
                    else {
                        departmantName = [[NSMutableString alloc] initWithString:@""];
                    }

                    CFStringTrimWhitespace((__bridge CFMutableStringRef) departmantName);

                    ReportInOutAllDataSelectDepartmantModel *model = [[ReportInOutAllDataSelectDepartmantModel alloc]init];
                    [model setDepartmantName:departmantName];
                    [model setDepartmantId:departmantId];
                    [model setAmountDepartmant:amountDepartmant];
                    [model setStatus:statusArray];

                    [dataDepartmantArray addObject:model];
                }
                
                
                
                connectCounter = 0;
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportInOutAllDataSelectDepartmantAPI:dataDepaermantArray:success:)]) {
                    [self.delegate callReportInOutAllDataSelectDepartmantAPI:self dataDepaermantArray:dataDepartmantArray success:YES];
                }
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportInOutAllDataSelectDepartmantAPI:dataDepaermantArray:success:)]) {
                    [self.delegate callReportInOutAllDataSelectDepartmantAPI:self dataDepaermantArray:dataDepartmantArray success:NO];
                }
            }
            
        }
        
    }];
}
@end
