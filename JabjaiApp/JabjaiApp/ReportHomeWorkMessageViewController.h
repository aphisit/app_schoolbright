//
//  ReportHomeWorkMessageViewController.h
//  JabjaiApp
//
//  Created by mac on 1/4/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "JHDetailHomeWorkViewController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface ReportHomeWorkMessageViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

@property (nonatomic) long long messageID;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *notificationButton;

- (IBAction)openDrawer:(id)sender;
- (IBAction)hideNotifications:(id)sender;

@end
