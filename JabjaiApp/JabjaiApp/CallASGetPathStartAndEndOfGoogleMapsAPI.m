//
//  CallASGetPathStartAndEndOfGoogleMapsAPI.m
//  JabjaiApp
//
//  Created by Mac on 12/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "CallASGetPathStartAndEndOfGoogleMapsAPI.h"
#import "APIURL.h"
#import "Utils.h"
@implementation CallASGetPathStartAndEndOfGoogleMapsAPI{
    NSInteger connectCounter;
    ASDistanceModel *model;
}
-(void)call:(NSString *)sourceLocation destinationLocation:(NSString *)destinationLocation{
    connectCounter = 0;
    [self getDistance:sourceLocation destinationLocation:destinationLocation];
}
- (void)getDistance:(NSString *)sourceLocation destinationLocation:(NSString *)destinationLocation{
    NSString *URLString = [APIURL getDistanceSatrtPointAndEndPoint:sourceLocation destinationLocation:destinationLocation];
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {

            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }

            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;

                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getDistance:sourceLocation destinationLocation:destinationLocation];

            }
            else {
                isFail = YES;
                self->connectCounter = 0;
            }

        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                    [self getDistance:sourceLocation destinationLocation:destinationLocation];
                }
                else {
                    isFail = YES;
                    self->connectCounter = 0;
                }
            }
            else {
                NSDictionary *dic = returnedData;
                NSArray  *arrLocation;
                NSString *kilometer,*time,*sourceLocationName,*destinationLocationName;
                NSArray *routes;
                
                
                routes = [dic objectForKey:@"routes"];
                if (routes.count > 0) {
                        arrLocation = dic[@"routes"][0][@"legs"][0][@"steps"];
                        kilometer = dic[@"routes"][0][@"legs"][0][@"distance"][@"text"];
                        time = dic[@"routes"][0][@"legs"][0][@"duration"][@"text"];
                        sourceLocationName = dic[@"routes"][0][@"legs"][0][@"start_address"];
                        destinationLocationName = dic[@"routes"][0][@"legs"][0][@"end_address"];
                        self->model = [[ASDistanceModel alloc] init];
                        self->model.distance = arrLocation;
                        self->model.kilomater = kilometer;
                        self->model.timeLeft = time;
                        self->model.nameLocationStart = sourceLocationName;
                        self->model.nameLocationEnd = destinationLocationName;
                        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(CallASGetPathStartAndEndOfGoogleMapsAPI:data:success:)]) {
                            [self.delegate CallASGetPathStartAndEndOfGoogleMapsAPI:self data:self->model success:YES];
                        }
                    
                }else{
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(CallASGetPathStartAndEndOfGoogleMapsAPI:data:success:)]) {
                        [self.delegate CallASGetPathStartAndEndOfGoogleMapsAPI:self data:self->model success:NO];
                    }
                }
                
               
                
              
                
               
            }
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(CallASGetPathStartAndEndOfGoogleMapsAPI:data:success:)]) {
                [self.delegate CallASGetPathStartAndEndOfGoogleMapsAPI:self data:self->model success:NO];
            }
        }
     
        
    }
     ];
}

@end
