//
//  TECheckLateScanerQrCodeViewController.h
//  JabjaiApp
//
//  Created by toffee on 4/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "CallTEGetDataStudentScanBarcodeAPI.h"
#import "ConfirmScanerLateViewController.h"
#import "AlertDialogConfirm.h"
#import "AlertDialog.h"
#import "CallTEConfirmScanerBarcode.h"
#import "TELevelClassViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface TECheckLateScanerQrCodeViewController : UIViewController<AVCaptureMetadataOutputObjectsDelegate,CallTEGetDataStudentScanBarcodeAPIDelegate, AlertDialogConfirmDelegate,AlertDialogDelegate,ConfirmScanerLateViewControllerDelegate,CallTEConfirmScanerBarcodeDelegate>
@property (nonatomic,assign) NSInteger mode;
@property (strong, nonatomic) IBOutlet UIView *viewforCamera;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

-(BOOL)startReading;
-(void)stopReading;
@property (strong, nonatomic) IBOutlet UITextView *textView;
-(void)loadBeepSound;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
- (IBAction)moveBackAction:(id)sender;


@end

NS_ASSUME_NONNULL_END
