//
//  CardProfile.h
//  JabjaiApp
//
//  Created by toffee on 5/29/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallGMGetUserInfoDataAPI.h"
#import "CustomUIImageView.h"
#import "CallImageBarCodeAPI.h"



//@class CardProfile;
//@protocol CardProfileDelegate <NSObject, CallGMGetUserInfoDataAPIDelegate, CallImageBarCodeAPIDelegate>
//
//@end
@interface CardProfile : UIView <NSObject, CallGMGetUserInfoDataAPIDelegate, CallImageBarCodeAPIDelegate>


//@property (nonatomic, weak) id<CardProfileDelegate> delegate;



@property (strong, nonatomic) IBOutlet UIView *View;
@property (strong, nonatomic) IBOutlet UIImageView *imageUser;
@property (strong, nonatomic) IBOutlet UIImageView *imageSchool;
@property (strong, nonatomic) IBOutlet UIImageView *imageBarcode;

@property (strong, nonatomic) IBOutlet UILabel *nameHeadLabel;
@property (strong, nonatomic) IBOutlet UILabel *idHeadLabel;
@property (strong, nonatomic) IBOutlet UILabel *departmentHeadLabel;
@property (strong, nonatomic) IBOutlet UILabel *positionHeadLabel;

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *departmentLabel;
@property (strong, nonatomic) IBOutlet UILabel *idCardLabel;
@property (strong, nonatomic) IBOutlet UILabel *positionLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameSchoolLable;



@end
