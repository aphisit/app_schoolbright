//
//  SettingShowIDCardSomeTableViewCell.m
//  JabjaiApp
//
//  Created by toffee on 11/2/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "SettingShowIDCardSomeTableViewCell.h"
#import "UserData.h"
@implementation SettingShowIDCardSomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)switchAction:(id)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
   
   
    if ([mySwitch isOn]) {
        [UserData setShowSomeId:@"on"];
        NSLog(@"its on!");
    } else {
        [UserData setShowSomeId:@"off"];
        NSLog(@"its off!");
    }
}
@end
