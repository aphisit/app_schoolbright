//
//  NavigationViewController.h
//  JabjaiApp
//
//  Created by mac on 10/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CustomUIImageView.h"
#import "CallGetUnreadMessageCountAPI.h"
#import "CallGetTakeEventToChackAPI.h"
#import "UserInfoViewController.h"
#import "SlideMenuController.h"
#import "CallChackSignUpPin.h"
#import "CallGetMenuListAPI.h"
#import "ChangPasswordDialog.h"
#import "CallGetAmountUnreadLeaveAPI.h"
#import "CallChackSignUpPin.h"
#import "CallClearDataSystemNotificationPOSTAPI.h"
extern NSString *tokenStr;
@interface NavigationViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CallGetUnreadMessageCountAPIDelegate, CallGetTakeEventToChackAPIDelegate, SlideMenuControllerDelegate,CallChackSignUpPinDelegate, CallGetMenuListAPIDelegate,ChangPasswordDialogDelegate,CallGetAmountUnreadLeaveAPIDelegate, CallClearDataSystemNotificationPOSTAPIDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblExpandable;
@property (weak, nonatomic) IBOutlet CustomUIImageView *userImage;

@property (weak, nonatomic) IBOutlet UIImageView *schoolImage;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UIButton *viewProfileButton;

@property (weak, nonatomic) IBOutlet CustomUIImageView *floatingUserIcon1;
@property (weak, nonatomic) IBOutlet CustomUIImageView *floatingUserIcon2;
@property (weak, nonatomic) IBOutlet UIView *viewIcon1;
@property (weak, nonatomic) IBOutlet UIView *viewIcon2;
@property (weak, nonatomic) IBOutlet UIImageView *dropdownImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *backgroundIndicator;
@property (retain, nonatomic) UIViewController *mainViewControler;
@property (retain, nonatomic) UIViewController *rootViewControler;

- (IBAction)viewProfile:(id)sender;
- (IBAction)actionShowUser:(id)sender;


@end
