//
//  ReportOrderHomeworkDetailPersonViewController.h
//  JabjaiApp
//
//  Created by mac on 5/2/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageInboxScrollableTextDialogCollectionViewCell.h"
#import "saveImageXIBViewController.h"
#import "ReportOrderHomeWorkReadFileViewController.h"

@interface ReportOrderHomeworkDetailPersonViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource,saveImageXIBViewControllerDelegate>

@property (assign, nonatomic) long long homeworkID;



@property (weak, nonatomic) IBOutlet UILabel *headerSubjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRecipientLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerReadLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerUnReadLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDateSendLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDateAlertLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachFileLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *sendTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet UILabel *recieverLabel;
@property (weak, nonatomic) IBOutlet UILabel *allCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *unreadCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateNotiLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

//@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *fileCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highImaCollectionConstraint;

- (void)getReportOrderHomeworkDetailWithHomeworkID:(long long)homeworkId;

@end
