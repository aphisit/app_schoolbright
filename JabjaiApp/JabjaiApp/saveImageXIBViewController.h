//
//  saveImageXIBViewController.h
//  JabjaiApp
//
//  Created by toffee on 23/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class saveImageXIBViewController;

@protocol saveImageXIBViewControllerDelegate <NSObject>

//- (void)subjectTableListDialog:(JHSubjectTableListDialog *)dialog onItemSelectWithRequestCode:(NSString *)requestCode atIndex:(NSInteger)index;

@end
@interface saveImageXIBViewController : UIViewController

@property (nonatomic, retain) id<saveImageXIBViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;


- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;


- (void)showDialogInView:(UIView *)targetView urlImageArray:(NSArray*)urlImageArray;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end

NS_ASSUME_NONNULL_END
