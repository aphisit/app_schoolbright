//
//  LoginChangPasswordViewController.h
//  JabjaiApp
//
//  Created by toffee on 8/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallChangPasswordLoginAPI.h"
#import "LoginViewController.h"
#import "SlideMenuController.h"
#import "AlertDialogConfirm.h"
#import "UserInfoViewController.h"

@interface LoginChangPasswordViewController : UIViewController <UITextFieldDelegate,CallChangPasswordLoginAPIDelegate,SlideMenuControllerDelegate,AlertDialogConfirmDelegate>


@property (weak, nonatomic) IBOutlet UILabel *passwordNewLabel;
@property (weak, nonatomic) IBOutlet UILabel *confirmPasswordLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordNewTextfield;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextfield;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;


- (IBAction)confirmAction:(id)sender;
- (IBAction)cancelAction:(id)sender;

//TextField
@end
