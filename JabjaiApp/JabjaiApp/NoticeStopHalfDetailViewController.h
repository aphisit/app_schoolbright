//
//  NoticeStopHalfDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 3/7/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "NoticeStopHalfConfimTeacherViewController.h"
#import "NoticeStopHalfConfirmStudentViewController.h"
#import "NoticeStopHalfViewController.h"
#import "SWRevealViewController.h"
#import "NoticeStopCollectionViewCell.h"
#import "SNTimeOutImage.h"
#import "TableListDialog.h"
#import "PhotoAlbumLibary.h"

@interface NoticeStopHalfDetailViewController : UIViewController <SlideMenuControllerDelegate , TableListDialogDelegate, UITextFieldDelegate,UITextViewDelegate,PhotoAlbumLibaryDelegate>

@property (nonatomic) NSString* leaveCause;
@property (nonatomic) NSString* noticeDetail;
@property (nonatomic) NSString* leaveFirstDate;
@property (nonatomic) NSString* leavePeriod;


@property (nonatomic) NSString* address;
@property (nonatomic) NSString* province;
@property (nonatomic) NSString* district;
@property (nonatomic) NSString* subDistrict;
@property (nonatomic) NSString* phoneNumber;
@property (nonatomic) NSDate *leaveStartdate;
@property (nonatomic) NSDate *leaveEndDate;
@property (nonatomic, assign) int fillDetailTag;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerHouseNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerProvinceLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAmphurLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDistrictLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPhoneNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;

@property (weak, nonatomic) IBOutlet UIView *shadowDetailView;
@property (weak, nonatomic) IBOutlet UIView *shadowImageView;
@property (weak, nonatomic) IBOutlet UIView *shadowAddress;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UITextView *fillDetail;
@property (weak, nonatomic) IBOutlet UITextField *fillAddress;
@property (weak, nonatomic) IBOutlet UITextField *fillProvince;
@property (weak, nonatomic) IBOutlet UITextField *fillDistrict;
@property (weak, nonatomic) IBOutlet UITextField *fillSubDistrict;
@property (weak, nonatomic) IBOutlet UITextField *fillPhone;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonBottomConstraint;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

- (IBAction)selectImage:(id)sender;

- (IBAction)moveBack:(id)sender;
- (IBAction)nextPage:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;


@end
