//
//  UnAuthorizeMenuDialog.m
//  JabjaiApp
//
//  Created by toffee on 22/5/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "UnAuthorizeMenuDialog.h"
#import "UserData.h"
#import "Utils.h"
@interface UnAuthorizeMenuDialog (){
    NSBundle *myLangBundle;
}
@property (nonatomic, assign) BOOL isShowing;
@end

@implementation UnAuthorizeMenuDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Decorate view
    
    self.dialogView.layer.masksToBounds = YES;
    self.cancelBtn.layer.masksToBounds = YES;
    
    // Initialize variables
    self.isShowing = NO;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (screenSize.height == 812.0f) {
            self.dialogTopConstraint.constant = 278.5;
            self.dialogBottomConstraint.constant = 278.5;
        }
        
        else if (screenSize.height == 736.0f){
            self.dialogTopConstraint.constant = 240.5;
            self.dialogBottomConstraint.constant = 240.5;
        }
        
        else if (screenSize.height == 667.0f){
            self.dialogTopConstraint.constant = 206.0;
            self.dialogBottomConstraint.constant = 206.0;
        }
        else if (screenSize.height == 568.0f){
            self.dialogTopConstraint.constant = 156.5;
            self.dialogBottomConstraint.constant = 156.5;
        }
        
    }
    
    self.messageLabel.lineBreakMode  = NSLineBreakByWordWrapping;
    self.messageLabel.numberOfLines = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradient;
    //set color header
    [self.cancelBtn layoutIfNeeded];
    gradient = [Utils getGradientColorStatus:@"red"];
    gradient.frame = self.cancelBtn.bounds;
    [self.cancelBtn.layer insertSublayer:gradient atIndex:0];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}



- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

#pragma mark - Dialog Functions

-(void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    [self.messageLabel setAlpha:0.0];
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [self.messageLabel setAlpha:1.0];
    [UIView commitAnimations];
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    [_cancelBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_BTN_FLAG_CLOSE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    [self.titleLabel setText:title];
    [self.messageLabel setText:message];
    
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [self.messageLabel setAlpha:0.0];
    
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogClose)]) {
        [self.delegate onAlertDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

@end
