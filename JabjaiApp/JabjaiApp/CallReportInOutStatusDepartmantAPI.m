//
//  CallReportInOutStatusDepartmantAPI.m
//  JabjaiApp
//
//  Created by toffee on 6/3/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallReportInOutStatusDepartmantAPI.h"
#import "Utils.h"
#import "APIURL.h"
@interface CallReportInOutStatusDepartmantAPI () {
    NSInteger connectCounter;
    NSMutableArray<ReportInOutStatusDepartmentModel *> *dataDepartmentArray;
//    NSMutableArray<ReportInOutStatusClassroomModel*> *dataClassroomArray;
}
@end
@implementation CallReportInOutStatusDepartmantAPI

- (void) call:(long long)schoolId dayReports:(NSString *)dayReports departmentId:(long long)departmentId{
    connectCounter = 0;
    [self getReportInOutStatus:schoolId dayReports:dayReports departmentId:departmentId];
}

- (void) getReportInOutStatus:(long long)schoolId dayReports:(NSString *)dayReports departmentId:(long long)departmentId{
    NSString *URLString = [APIURL getReportInOutStatusDepaetmant:schoolId dayReports:dayReports depaetmanId:departmentId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
               [self getReportInOutStatus:schoolId dayReports:dayReports departmentId:departmentId];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportInOutStatus:schoolId dayReports:dayReports departmentId:departmentId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                   [self getReportInOutStatus:schoolId dayReports:dayReports departmentId:departmentId];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                
                NSArray *returnedArray = returnedData;
                NSMutableString *nameDepartment, *status, *picture ;
                long long departmentId;
                
               
                if(dataDepartmentArray != nil) {
                    [dataDepartmentArray removeAllObjects];
                    dataDepartmentArray = nil;
                }
                dataDepartmentArray = [[NSMutableArray alloc] init];
                
                
               
                    for (int i = 0; i<returnedArray.count; i++) {
                        NSDictionary *departmentDict = [returnedArray objectAtIndex:i];
                        departmentId = [[departmentDict objectForKey:@"id"] longLongValue];
                        if(![[departmentDict objectForKey:@"name"] isKindOfClass:[NSNull class]]) {
                            nameDepartment = [[NSMutableString alloc]
                                           initWithFormat:@"%@", [departmentDict objectForKey:@"name"]];
                        }
                        else {
                            nameDepartment = [[NSMutableString alloc] initWithString:@""];
                        }
                        if(![[departmentDict objectForKey:@"status"] isKindOfClass:[NSNull class]]) {
                            status = [[NSMutableString alloc]
                                      initWithFormat:@"%@", [departmentDict objectForKey:@"status"]];
                        }
                        else {
                            status = [[NSMutableString alloc] initWithString:@""];
                        }

                        if(![[departmentDict objectForKey:@"pictuer"] isKindOfClass:[NSNull class]]) {
                            picture = [[NSMutableString alloc]
                                       initWithFormat:@"%@", [departmentDict objectForKey:@"pictuer"]];
                        }
                        else {
                            picture = [[NSMutableString alloc] initWithString:@""];
                        }
                        
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) nameDepartment);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) status);
                        CFStringTrimWhitespace((__bridge CFMutableStringRef) picture);
                        
                        ReportInOutStatusDepartmentModel *model = [[ReportInOutStatusDepartmentModel alloc] init];
                        [model setNameDepartment:nameDepartment];
                        [model setDepartmentId:departmentId];
                        [model setStatus:status];
                        [model setPictuer:picture];

                        [dataDepartmentArray addObject:model];
                    }
             
                connectCounter = 0;
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportInOutStatusDepartmantAPI:dataDepartment:success:)]) {
                    [self.delegate callReportInOutStatusDepartmantAPI:self dataDepartment:dataDepartmentArray success:YES];
                }
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportInOutStatusDepartmantAPI:dataDepartment:success:)]) {
                    [self.delegate callReportInOutStatusDepartmantAPI:self dataDepartment:dataDepartmentArray success:NO];
                }
            }
            
        }
        
    }];
}

@end
