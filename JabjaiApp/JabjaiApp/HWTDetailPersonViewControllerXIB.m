//
//  HWTDetailPersonViewControllerXIB.m
//  JabjaiApp
//
//  Created by Mac on 10/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "HWTDetailPersonViewControllerXIB.h"
#import <QBImagePickerController/QBImagePickerController.h>
#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"
#import "AppDelegate.h"
#import "ZMImageSliderViewController.h"
#import "AppDelegate.h"
#import "ReportOrderHomeworkDetailModel.h"
@interface HWTDetailPersonViewControllerXIB (){
    ReportOrderHomeworkDetailModel *orderHomeworkDetailModel;
    NSMutableArray<ReportOrderHomeworkDetailModel *> *orderArray;
    NSInteger connectCounter;
    NSMutableArray *nameImageArray,*fileArray,*typeFileArray;
}
@property (nonatomic, strong) ReportOrderHomeWorkReadFileViewController *openFileView;
@property (nonatomic, strong) saveImageXIBViewController *saveImageXIB;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@property UIViewController *myController;
@end

@implementation HWTDetailPersonViewControllerXIB

- (void)viewDidLoad {
    [super viewDidLoad];
    //set collectionView
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.fileCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.fileCollectionView.delegate = self;
    self.fileCollectionView.dataSource = self;

    // set up formatter
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.timeFormatter = [Utils getDateFormatter];
    //[self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];;
    }else{
        [self.timeFormatter setDateFormat:@"HH:mm"];
    }
    
    [self getReportOrderHomeworkDetailWithHomeworkID:self.homeworkID];    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidLayoutSubviews{
//    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
     [self doDesignLayout];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    self.headerSubjectLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_SUBJECT",nil,[Utils getLanguage],nil);
    self.headerSenderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_SENDER",nil,[Utils getLanguage],nil);
    self.headerRecipientLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_AMOUNT_RECIPIENT",nil,[Utils getLanguage],nil);
    self.headerReadLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_READ",nil,[Utils getLanguage],nil);
    self.headerUnReadLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_UNREND",nil,[Utils getLanguage],nil);
    self.headerDateSendLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_DATE_SEND",nil,[Utils getLanguage],nil);
    self.headerDateAlertLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_DATE_ALATE",nil,[Utils getLanguage],nil);
    self.headerDetailLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_LIST",nil,[Utils getLanguage],nil);
    self.headerAttachImageLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_ATTACH_IMAGE",nil,[Utils getLanguage],nil);
    self.headerAttachFileLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_ATTACH_FILE",nil,[Utils getLanguage],nil);
}



- (void)getReportOrderHomeworkDetailWithHomeworkID:(long long)homeworkId{
    [self showIndicator];
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getReportOrderHomeworkDetailWithSchoolID:schoolID homeworkID:homeworkId];
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            if(self->connectCounter < TRY_CONNECT_MAX) {
                self->connectCounter++;
                NSLog(@"Error try to connect #%@", [@(self->connectCounter) stringValue]);
                [self getReportOrderHomeworkDetailWithHomeworkID:homeworkId];
            }
            else {
                self->connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportOrderHomeworkDetailWithHomeworkID:homeworkId];
                }
                else {
                    self->connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(self->connectCounter < TRY_CONNECT_MAX) {
                    self->connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportOrderHomeworkDetailWithHomeworkID:homeworkId];
                }
                else {
                    self->connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                NSArray *imageArray;
               self->nameImageArray = [[NSMutableArray alloc] init];
                self->fileArray = [[NSMutableArray alloc] init];
                self->typeFileArray = [[NSMutableArray alloc] init];
                self->connectCounter = 0;
                
                if (self->orderArray != nil) {
                    [self->orderArray removeAllObjects];
                    self->orderArray = nil;
                }
                
                self->orderArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *subject;
                    
                    if(![[dataDict objectForKey:@"plane_name"] isKindOfClass:[NSNull class]]) {
                        subject = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"plane_name"]];
                    }
                    else {
                        subject = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *sender;
                    
                    if(![[dataDict objectForKey:@"theacher_name"] isKindOfClass:[NSNull class]]) {
                        sender = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"theacher_name"]];
                    }
                    else {
                        sender = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    
                    NSMutableString *message;
                    
                    if(![[dataDict objectForKey:@"message"] isKindOfClass:[NSNull class]]) {
                        message = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"message"]];
                    }
                    else {
                        message = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                   
                    imageArray = [dataDict objectForKey:@"homework_files"];
                    for (int i = 0; i < imageArray.count; i++) {
                        NSString *nameImage = [[imageArray objectAtIndex:i] objectForKey:@"file_type"];
                        
                        if ([nameImage isEqualToString:@"image/jpeg"]) {
                            [self->nameImageArray addObject:[[imageArray objectAtIndex:i] objectForKey:@"file_name"]];
                        }
                        else{
                            NSString *fileType = [[imageArray objectAtIndex:i]objectForKey:@"file_type"];
                            NSString *fileName = [[imageArray objectAtIndex:i]objectForKey:@"file_name"];
                            if (fileType != nil || fileName != nil) {
                                [self->typeFileArray addObject:fileType];
                                [self->fileArray addObject:fileName];
                            }
                            
                        }
                    }
                    
                    NSMutableString *startDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"daystart"]];
                    
                    NSMutableString *endDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"dayend"]];
                    
                    NSMutableString *notiDateStr = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"daynoti"]];
                    
                    int all = [[dataDict objectForKey:@"user_all"] intValue];
                    int read = [[dataDict objectForKey:@"user_read"] intValue];
                    int unread = [[dataDict objectForKey:@"user_unread"] intValue];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subject);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) sender);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) message);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) notiDateStr);
                    
                    
                    NSRange dotRange = [startDateStr rangeOfString:@"."];
                    NSDate *startDate;
                    
                    if(dotRange.length != 0) {
                        startDate = [formatter dateFromString:startDateStr];
                    }
                    else {
                        startDate = [formatter2 dateFromString:startDateStr];
                    }
                    
                    NSRange dotRange2 = [endDateStr rangeOfString:@"."];
                    NSDate *endDate;
                    
                    if(dotRange2.length != 0) {
                        endDate = [formatter dateFromString:endDateStr];
                    }
                    else {
                        endDate = [formatter2 dateFromString:endDateStr];
                    }
                    
                    NSRange dotRange3 = [notiDateStr rangeOfString:@"."];
                    NSDate *notiDate;
                    
                    if(dotRange3.length != 0) {
                        notiDate = [formatter dateFromString:notiDateStr];
                    }
                    else {
                        notiDate = [formatter2 dateFromString:notiDateStr];
                    }
                    
                    ReportOrderHomeworkDetailModel *model = [[ReportOrderHomeworkDetailModel alloc] init];
                    model.subjectTitle = subject;
                    model.sender = sender;
                    model.message = message;
                    model.all = all;
                    model.read = read;
                    model.unread = unread;
                    model.dateStart = startDate;
                    model.dateEnd = endDate;
                    model.dateNoti = notiDate;
                    self->orderHomeworkDetailModel = model;
                    [self performData];
                    [self->orderArray addObject:model];
                    
                }
                
            }
        }
        
    }];
    
}

- (void)performData {
    
    if (orderHomeworkDetailModel != nil) {
        
        self.subjectLabel.text = [[NSString alloc] initWithFormat:@"%@", orderHomeworkDetailModel.subjectTitle];
        self.senderLabel.text = [[NSString alloc] initWithFormat:@"%@", orderHomeworkDetailModel.sender];
//        self.sendTypeLabel.text = @"";
//        self.recieverLabel.text = @"";
        self.allCountLabel.text = [[NSString alloc] initWithFormat:@"%i %@", orderHomeworkDetailModel.all,NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.readCountLabel.text = [[NSString alloc] initWithFormat:@"%i %@", orderHomeworkDetailModel.read,NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.unreadCountLabel.text = [[NSString alloc] initWithFormat:@"%i %@", orderHomeworkDetailModel.unread,NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.dateTimeLabel.text = [[NSString alloc] initWithFormat:@"%@ %@ %@",[self.dateFormatter stringFromDate:orderHomeworkDetailModel.dateStart], NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_TO",nil,[Utils getLanguage],nil),[self.dateFormatter stringFromDate:orderHomeworkDetailModel.dateEnd]];
        NSLog(@"Date = %@",[self.dateFormatter stringFromDate:orderHomeworkDetailModel.dateNoti]);
        NSLog(@"Time = %@",[self.timeFormatter stringFromDate:orderHomeworkDetailModel.dateNoti]);
        
        self.dateNotiLabel.text = [[NSString alloc] initWithFormat:@"%@ %@ %@",[self.dateFormatter stringFromDate:orderHomeworkDetailModel.dateNoti],NSLocalizedStringFromTableInBundle(@"LABEL_HOMEWORK_REP_TIME",nil,[Utils getLanguage],nil) ,[self.timeFormatter stringFromDate:orderHomeworkDetailModel.dateNoti]];
        self.detailTextView.text = [[NSString alloc] initWithFormat:@"%@", orderHomeworkDetailModel.message];
    }
    else{
        [self clearDisplay];
    }
    
    float wCollection = self.collectionView.frame.size.width / ((self.collectionView.frame.size.width/4)-10);
    float amountRow = nameImageArray.count / (int)wCollection;
    NSLog(@"num = %d",(int)wCollection);
    NSInteger modImg = nameImageArray.count % (int)wCollection ;
    if (modImg > 0) {
        amountRow ++;
    }
    self.highImaCollectionConstraint.constant = self.highImaCollectionConstraint.constant * amountRow+((amountRow*5)*2);//set hight image collectionview
    
    float wFileCollection = self.fileCollectionView.frame.size.width / ((self.fileCollectionView.frame.size.width/4)-10);
    float amountFileRow = self->fileArray.count / (int)wFileCollection;
    NSInteger modFile = self->fileArray.count % (int)wFileCollection ;
    if (modFile > 0) {
        amountFileRow ++;
    }
    self.highFileCollectionConstraint.constant = self.highFileCollectionConstraint.constant * amountFileRow + ((amountFileRow*5)*2);//set hight file colectionview
    
    if (nameImageArray.count < 1) {
        self.collectionView.hidden = YES;
    }else{
        self.collectionView.hidden = NO;
       [self.collectionView reloadData];
    }
    
    if (fileArray.count < 1) {
        self.fileCollectionView.hidden = YES;
    }else{
        self.fileCollectionView.hidden = NO;
        [self.fileCollectionView reloadData];
    }
    
    
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (collectionView == self.collectionView) {
        
        return nameImageArray.count;
    }else{
        return fileArray.count;
    }
    
    //return nameImageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MessageInboxScrollableTextDialogCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (collectionView == self.collectionView) {
        NSData *imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [nameImageArray objectAtIndex:indexPath.row]]];
        cell.imageNews.tag = indexPath.row;
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.image = [UIImage imageWithData:imageData];
        [cell.images setImage:imageView.image];
        cell.images.contentMode = UIViewContentModeScaleAspectFill;
        [cell.imageNews addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        cell.imageNews.tag = indexPath.row;
        if ([[typeFileArray objectAtIndex:indexPath.row] isEqualToString:@"PDF"]) {
            [cell.images setImage:[UIImage imageNamed:@"ic_pdf"]];
        }else if ([[typeFileArray objectAtIndex:indexPath.row] isEqualToString:@"MS Word"]){
            [cell.images setImage:[UIImage imageNamed:@"ic_word"]];
        }else{
            [cell.images setImage:[UIImage imageNamed:@"ic_excel"]];
        }
        cell.images.contentMode = UIViewContentModeScaleAspectFit;
        [cell.imageNews addTarget:self action:@selector(openFileTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:   (UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGSize defaultSize = CGSizeMake((self.collectionView.frame.size.width/4)-10, (self.collectionView.frame.size.height));
   
    return defaultSize;
}

- (void)okButtonTapped:(UIButton *)sender {
    if(self.saveImageXIB != nil && [self.saveImageXIB isDialogShowing]) {
        [self.saveImageXIB dismissDialog];
        self.saveImageXIB = nil;
    }

    self.saveImageXIB = [[saveImageXIBViewController  alloc] init];
    self.saveImageXIB.delegate = self;
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;
    [self.saveImageXIB showDialogInView:topView urlImageArray:nameImageArray];
}

- (void)clearDisplay {
    self.subjectLabel.text = @"";
    self.senderLabel.text = @"";
//    self.sendTypeLabel.text = @"";
//    self.recieverLabel.text = @"";
    self.allCountLabel.text = @"";
    self.readCountLabel.text = @"";
    self.unreadCountLabel.text = @"";
    self.dateTimeLabel.text = @"";
    self.dateNotiLabel.text = @"";
    self.detailTextView.text = @"";
}

- (void)openFileTapped:(UIButton *)sender{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;
    self.openFileView = [[ReportOrderHomeWorkReadFileViewController alloc] init];
    [self.openFileView showPagesReadFileInView:topView partFile:[fileArray objectAtIndex:sender.tag]];
}

- (void)showIndicator{
    if (![self.indicator isAnimating]) {
        self.loadBackgroundView.hidden = NO;
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator{
    [self.indicator stopAnimating];
    self.loadBackgroundView.hidden = YES;
    
}




@end
