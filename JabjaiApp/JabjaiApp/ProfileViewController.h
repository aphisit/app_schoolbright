 //
//  ProfileViewController.h
//  JabjaiApp
//
//  Created by mac on 12/21/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUIImageView.h"
#import "SlideMenuController.h"
#import "CardPfileXIBViewController.h"

@protocol ProfileViewControllerDelegate <NSObject>
@optional
@end
@interface ProfileViewController : UIViewController <SlideMenuControllerDelegate, CardPfileXIBViewControllerDelegate>

@property (nonatomic, retain) id<ProfileViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSchoolLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassroomLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerStudentIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSexLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerBirthdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPhoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEmailLabel;


- (IBAction)openDrawer:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (strong, nonatomic) IBOutlet UIView *backgroundCardprofile;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightBackgroundCardConstraint;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet CustomUIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *schoolLabel;
@property (weak, nonatomic) IBOutlet UILabel *schoolValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *classLabel;
@property (weak, nonatomic) IBOutlet UILabel *classValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentIDValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthdayValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditLimitsLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditLimitsValueLabel;

@property (weak, nonatomic) IBOutlet UIStackView *classStackView;
@property (weak, nonatomic) IBOutlet UIStackView *studentIDStackView;

-(void)showDialogCard;

@end
