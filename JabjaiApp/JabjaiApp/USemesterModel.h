//
//  USemesterModel.h
//  JabjaiApp
//
//  Created by mac on 12/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "USubjectModel.h"

@interface USemesterModel : NSObject

@property (strong, nonatomic) NSString *semesterID;
@property (strong, nonatomic) NSString *semesterName;
@property (strong, nonatomic) NSArray<USubjectModel *> *subjects;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;

@end
