//
//  EXReportClassStatusModel.h
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EXReportClassStatusModel : NSObject

@property (nonatomic) long long iid;
@property (nonatomic) NSString *title;
@property (nonatomic) NSInteger numberOfOnTimeStatus;
@property (nonatomic) NSInteger numberOfLateStatus;
@property (nonatomic) NSInteger numberOfAbsenceStatus;
@property (nonatomic) NSInteger numberOfEventStatus;
@property (nonatomic) NSInteger numberOfSickLeaveStatus;
@property (nonatomic) NSInteger numberOfPersonalStatus;
@property (nonatomic) NSInteger numberOfUndefinedStatus;

- (void)setIId:(long long)iid;
- (void)setTitle:(NSString *)title;
- (void)setNumberOfOnTimeStatus:(NSInteger)numberOfOnTimeStatus;
- (void)setNumberOfLateStatus:(NSInteger)numberOfLateStatus;
- (void)setNumberOfAbsenceStatus:(NSInteger)numberOfAbsenceStatus;
- (void)setNumberOfEventStatus:(NSInteger)numberOfEventStatus;
- (void)setNumberOfSickLeaveStatus:(NSInteger)numberOfSickLeaveStatus;
- (void)setNumberOfPersonalStatus:(NSInteger)numberOfPersonalStatus;
- (void)setNumberOfUndefinedStatus:(NSInteger)numberOfUndefinedStatus;

- (long long)getIId;
- (NSString *)getTitle;
- (NSInteger)getNumberOfOnTimeStatus;
- (NSInteger)getNumberOfLateStatus;
- (NSInteger)getNumberOfAbsenceStatus;
- (NSInteger)getNumberOfEventStatus;
- (NSInteger)getNumberOfSickLeaveStatus;
- (NSInteger)getNumberOfPersonalStatus;
- (NSInteger)getNumberOfUndefinedStatus;

@end
