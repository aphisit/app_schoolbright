//
//  ReportStudentViewController.m
//  JabjaiApp
//
//  Created by mac on 4/3/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportStudentViewController.h"
#import "MenuReportTableViewCell.h"
#import "ReportCheckNameViewController.h"
#import "ReportCheckFlagPoleViewController.h"
#import "ReportCheckSubjectViewController.h"
#import "ReportLeaveAllDataSelectClassViewController.h"
#import "ReportBehaviorAllDataSelectViewController.h"
#import "ReportOrderHomeWorkViewController.h"
#import "ReportSendNewsListViewController.h"
#import "NavigationViewController.h"
#import "AlertDialog.h"
#import "UserData.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Utils.h"

@interface ReportStudentViewController (){
    
    UIColor *grayColor;
    AlertDialog *alertDialog;
    
    NSMutableArray *authorizeMenuArray; // authorize Menu user
    NSMutableArray *authorizeSubMenuArray;
    
}
@property (nonatomic, strong) CallGetMenuListAPI *callGetMenuListAPI;
@end

@implementation ReportStudentViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    authorizeMenuArray = [[NSMutableArray alloc] init];
    authorizeSubMenuArray = [[NSMutableArray alloc] init];
   
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MenuReportTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    [self callAuthorizeMenuForUser:[UserData getUserID] clientToken:[UserData getClientToken]];//list authorize menu
}

- (BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) callAuthorizeMenuForUser:(long long)userId clientToken:(NSString *)clientToken{ //Authorize Menu For User
    //[self showIndicator];
    self.callGetMenuListAPI = nil;
    self.callGetMenuListAPI = [[ CallGetMenuListAPI alloc] init];
    self.callGetMenuListAPI.delegate = self;
    [self.callGetMenuListAPI call:userId clientToken:clientToken schoolid:[UserData getSchoolId]];
}
- (void)callGetMenuListAPI:(CallGetMenuListAPI *)classObj data:(NSMutableArray *)data resCode:(NSInteger)resCode success:(BOOL)success{
   // [self stopIndicator];
 
        NSLog(@"xxxxx");
        if (data.count > 0 && success == YES ) {
            for (int i = 0;i < data.count; i++) {
                NSMutableArray *subMenuArray = [[data objectAtIndex:i] objectForKey:@"subMenus"];
                
                NSInteger menuId = [[[data objectAtIndex:i] objectForKey:@"menuGroup_Id"] integerValue];
                [authorizeMenuArray addObject: [NSString stringWithFormat:@"%d",menuId]];
                for (int j = 0; j < subMenuArray.count; j++) {
                    NSInteger subMenuId = [[[subMenuArray objectAtIndex:j] objectForKey:@"menu_Id"] integerValue];
                    [authorizeSubMenuArray addObject: [NSString stringWithFormat:@"%d",subMenuId]];
                }
            }
            [self.tableView reloadData];
        }
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
     if(indexPath.row == 0) {
         if (![authorizeSubMenuArray containsObject: @"18"]) {
             cell.userInteractionEnabled = false;
             //cell.iconReport.image = [UIImage imageNamed:@"ic_pencil"];
             UIImage *myImage =[UIImage imageNamed:@"ic_pencil"];
             cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
             cell.BackgroundView.backgroundColor = [UIColor colorWithRed:0.74 green:0.74 blue:0.74 alpha:1.0];
             cell.nameReport.textColor = [UIColor whiteColor];
         }else{
             
             cell.userInteractionEnabled = true;
             //cell.iconReport.image = [UIImage imageNamed:@"edit_report"];
             UIImage *myImage =[UIImage imageNamed:@"edit_report"];
             cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
             cell.BackgroundView.backgroundColor = [UIColor whiteColor];
             cell.nameReport.textColor = [UIColor blackColor];
             
             //UIImage *newImage =[UIImage imageWithImage:myImage scaledToSize:CGSizeMake(92,91)]
         }
         
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_REPORT_CHECKNAME",nil,[Utils getLanguage],nil);
         
    }
    else if(indexPath.row == 1) {
        if (![authorizeSubMenuArray containsObject: @"19"]) {
            cell.userInteractionEnabled = false;
            //cell.iconReport.image = [UIImage imageNamed:@"ic_leave"];
            UIImage *myImage =[UIImage imageNamed:@"ic_leave"];
            cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
            cell.BackgroundView.backgroundColor = [UIColor colorWithRed:0.74 green:0.74 blue:0.74 alpha:1.0];
            cell.nameReport.textColor = [UIColor whiteColor];
        }else{
            cell.userInteractionEnabled = true;
            //cell.iconReport.image = [UIImage imageNamed:@"leave_report"];
            UIImage *myImage =[UIImage imageNamed:@"leave_report"];
            cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
            cell.BackgroundView.backgroundColor = [UIColor whiteColor];
            cell.nameReport.textColor = [UIColor blackColor];
        }
        
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_HOLIDAY_REPORT",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 2) {
        if (![authorizeSubMenuArray containsObject: @"20"]) {
            cell.userInteractionEnabled = false;
            //cell.iconReport.image = [UIImage imageNamed:@"ic_behaviorscore"];
            UIImage *myImage =[UIImage imageNamed:@"ic_behaviorscore"];
            cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
            cell.BackgroundView.backgroundColor = [UIColor colorWithRed:0.74 green:0.74 blue:0.74 alpha:1.0];
            cell.nameReport.textColor = [UIColor whiteColor];
        }else{
            cell.userInteractionEnabled = true;
           // cell.iconReport.image = [UIImage imageNamed:@"people"];
            UIImage *myImage =[UIImage imageNamed:@"people"];
            cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
            cell.BackgroundView.backgroundColor = [UIColor whiteColor];
            cell.nameReport.textColor = [UIColor blackColor];
        }
        
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_BEHAVIOR_SCORE_REPORT",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 3) {
        if (![authorizeSubMenuArray containsObject: @"21"]) {
            cell.userInteractionEnabled = false;
            //cell.iconReport.image = [UIImage imageNamed:@"ic_homework"];
            UIImage *myImage =[UIImage imageNamed:@"ic_homework"];
            cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
            cell.BackgroundView.backgroundColor = [UIColor colorWithRed:0.74 green:0.74 blue:0.74 alpha:1.0];
            cell.nameReport.textColor = [UIColor whiteColor];
        }else{
            cell.userInteractionEnabled = true;
            //cell.iconReport.image = [UIImage imageNamed:@"report_homework"];
            UIImage *myImage =[UIImage imageNamed:@"report_homework"];
            cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
            cell.BackgroundView.backgroundColor = [UIColor whiteColor];
            cell.nameReport.textColor = [UIColor blackColor];
        }
       
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_HOMEWORK_REPORT",nil,[Utils getLanguage],nil);
    }
    else if(indexPath.row == 4) {
        if (![authorizeSubMenuArray containsObject: @"22"]) {
            cell.userInteractionEnabled = false;
           // cell.iconReport.image = [UIImage imageNamed:@"ic_send_message"];
            UIImage *myImage =[UIImage imageNamed:@"ic_send_message"];
            cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
            cell.BackgroundView.backgroundColor = [UIColor colorWithRed:0.74 green:0.74 blue:0.74 alpha:1.0];
            cell.nameReport.textColor = [UIColor whiteColor];
        }else{
            cell.userInteractionEnabled = true;
            //cell.iconReport.image = [UIImage imageNamed:@"send_news"];
            UIImage *myImage =[UIImage imageNamed:@"send_news"];
            cell.iconReport.image =[self imageWithImage:myImage scaledToSize:CGSizeMake(91,91)];
            cell.BackgroundView.backgroundColor = [UIColor whiteColor];
            cell.nameReport.textColor = [UIColor blackColor];
        }
       
        cell.nameReport.text = NSLocalizedStringFromTableInBundle(@"LABEL_TEACHER_SENDNEWS_REPORT",nil,[Utils getLanguage],nil);;
    }
    [cell.BackgroundView layoutIfNeeded];
    [cell.BackgroundView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.BackgroundView.layer setShadowOpacity:0.3];
    [cell.BackgroundView.layer setShadowRadius:3.0];
    [cell.BackgroundView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

   if( (indexPath.row == 0)) {
       if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportStudentViewController:numberPage:)]) {
           [self.delegate changePageReportStudentViewController:self numberPage:0];
       }
    }
    else if (indexPath.row == 1) {
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportStudentViewController:numberPage:)]) {
            [self.delegate changePageReportStudentViewController:self numberPage:1];
        }
    }
    else if (indexPath.row == 2) {
        
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportStudentViewController:numberPage:)]) {
            [self.delegate changePageReportStudentViewController:self numberPage:2];
        }
        
    }
    else if (indexPath.row == 3) {
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportStudentViewController:numberPage:)]) {
            [self.delegate changePageReportStudentViewController:self numberPage:3];
        }
    }
    else if (indexPath.row == 4) {
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(changePageReportStudentViewController:numberPage:)]) {
            [self.delegate changePageReportStudentViewController:self numberPage:4];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialog alloc] init];
    }
    else if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    // Here pass new size you need
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
