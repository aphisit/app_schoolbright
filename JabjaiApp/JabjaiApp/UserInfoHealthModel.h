//
//  UserInfoHealthModel.h
//  JabjaiApp
//
//  Created by mac on 1/19/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfoHealthModel : NSObject

@property (strong, nonatomic) NSString *sex;

@property (strong, nonatomic) NSString *ageText;
@property (strong, nonatomic) NSString *weightText;
@property (strong, nonatomic) NSString *heightText;

@property (nonatomic) int age;
@property (nonatomic) int weight;
@property (nonatomic) int height;
@property (strong, nonatomic) NSString *bloddType;
@property (strong, nonatomic) NSString *disease;

@end
