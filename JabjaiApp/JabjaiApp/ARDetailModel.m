//
//  ARDetailModel.m
//  JabjaiApp
//
//  Created by toffee on 19/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "ARDetailModel.h"

@implementation ARDetailModel

@synthesize term = _term;
@synthesize gradeAverage = _gradeAverage;
@synthesize year = _year;
@synthesize name = _name;
@synthesize studentId = _studentId;
@synthesize branch = _branch;
@synthesize levelName = _levelName;
@synthesize subLevelName = _subLevelName;
@synthesize schoolNameTH = _schoolNameTH;
@synthesize schoolNameEN = _schoolNameEN;
@synthesize schoolNameImg = _schoolNameImg;
@synthesize basicCoursesArray = _basicCoursesArray;
@synthesize additionalCoursesArray = _additionalCoursesArray;
@synthesize eventCoursesArray = _eventCoursesArray;
@synthesize noCountCreditArray = _noCountCreditArray;

-(void)setTerm:(NSString*)term{
    _term = term;
}
-(void)setGradeAverage:(NSString*)gradeAverage{
    _gradeAverage = gradeAverage;
}
-(void)setYear:(NSInteger)year{
    _year = year;
}
-(void)setName:(NSString *)name{
    _name = name;
}
-(void)setStudentId:(NSString *)studentId{
    _studentId = studentId;
}
-(void)setBranch:(NSString *)branch{
    _branch = branch;
}
-(void)setLevelName:(NSString *)levelName{
    _levelName = levelName;
}
-(void)setSubLevelName:(NSString *)subLevelName{
    _subLevelName = subLevelName;
}
-(void)setSchoolNameTH:(NSString *)schoolNameTH{
    _schoolNameTH = schoolNameTH;
}
-(void)setSchoolNameEN:(NSString *)schoolNameEN{
    _schoolNameEN = schoolNameEN;
}
-(void)setSchoolNameImg:(NSString *)schoolNameImg{
    _schoolNameImg = schoolNameImg;
}
-(void)setBasicCoursesArray:(NSArray *)basicCoursesArray{
    _basicCoursesArray = basicCoursesArray;
}
-(void)setAdditionalCoursesArray:(NSArray *)additionalCoursesArray{
    _additionalCoursesArray = additionalCoursesArray;
}
-(void)setEventCoursesArray:(NSArray *)eventCoursesArray{
    _eventCoursesArray = eventCoursesArray;
}
-(void)setNoCountCreditArray:(NSArray *)noCountCreditArray{
    _noCountCreditArray = noCountCreditArray;
}


-(NSString*)getTerm{
    return _term;
}
-(NSString*)getGradeAverage{
    return _gradeAverage;
}
-(NSInteger)getYear{
    return _year;
}
-(NSString*)getName{
    return _name;
}
-(NSString*)getStudentId{
    return _studentId;
}
-(NSString*)getBranch{
    return _branch;
}
-(NSString*)getLevelName{
    return _levelName;
}
-(NSString*)getSubLevelName{
    return _subLevelName;
}
-(NSString*)getSchoolNameTH{
    return _schoolNameTH;
}
-(NSString*)getSchoolNameEN{
    return _schoolNameEN;
}
-(NSString*)getSchoolImg{
    return _schoolNameImg;
}
-(NSArray*)getBasicCoursesArray{
    return _basicCoursesArray;
}
-(NSArray*)getAdditionalCoursesArray{
    return _additionalCoursesArray;
}
-(NSArray*)getEventCoursesArray{
    return _eventCoursesArray;
}
-(NSArray*)getNoCountCreditArray{
    return _noCountCreditArray;
}

@end
