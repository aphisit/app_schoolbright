//
//  CardProfile.m
//  JabjaiApp
//
//  Created by toffee on 5/29/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "CardProfile.h"
#import "UserData.h"
#import "Utils.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "APIURL.h"

@interface CardProfile (){
    NSInteger connectCounter;
}
@property (strong, nonatomic) CallGMGetUserInfoDataAPI *callGMGetUserInfoDataAPI;
@property (strong, nonatomic) UserInfoModel *userInfoModel;
@property (strong, nonatomic) CallImageBarCodeAPI *callImageBarCodeAPI;
@end
@implementation CardProfile

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        //Ini
        [self customInit];
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [NSTimer scheduledTimerWithTimeInterval:600.0f
        target:self selector:@selector(generateBarcode:) userInfo:nil repeats:YES];
        [self customInit];
        [self getUserInfoData];
        //[self getImageBarcode];
    }
    return self;
}
-(void)customInit{
    
    [[NSBundle mainBundle] loadNibNamed:@"CardProfile" owner:self options:nil];
    NSString *formatBarcodeStr = [NSString stringWithFormat:@"%@@%ld@%@",[UserData getUsername],(long)[UserData getUserID],[Utils dateInStringFormat:[NSDate date] format:@"dd-MM-yyyy:HH:mm:ss"]];
    CIImage *barImage = [self createBarcodeForString:formatBarcodeStr];
    float scaleX = self.imageBarcode.frame.size.width / barImage.extent.size.width;
    float scaleY = self.imageBarcode.frame.size.height / barImage.extent.size.height;
    barImage = [barImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    self.imageBarcode.image = [UIImage imageWithCIImage:barImage
                                                 scale:[UIScreen mainScreen].scale
                                           orientation:UIImageOrientationUp];
    [self addSubview:self.View];
    self.View.frame = self.bounds;
}
//Call To API Info Card
- (void)getUserInfoData {
    if(self.callGMGetUserInfoDataAPI != nil) {
        self.callGMGetUserInfoDataAPI = nil;
    }
    self.callGMGetUserInfoDataAPI = [[CallGMGetUserInfoDataAPI alloc] init];
    self.callGMGetUserInfoDataAPI.delegate = self;
    [self.callGMGetUserInfoDataAPI getUserInfoData] ;
}

-(void)callGMGetUserInfoDataAPI:(CallGMGetUserInfoDataAPI *)classObj data:(UserInfoModel *)data success:(BOOL)success{
    self.userInfoModel = [[UserInfoModel alloc] init];
    self.userInfoModel = data;
    NSString *newIdCard;
    if (data != nil) {
        if([UserData getUserType] == STUDENT) {
            [self.nameHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_NAME",nil,[Utils getLanguage],nil)];
            [self.idHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_STUDENTID",nil,[Utils getLanguage],nil)];
            [self.departmentHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_CLASSROOM",nil,[Utils getLanguage],nil)];
            [self.positionHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_CARD_ID_CARD",nil,[Utils getLanguage],nil)];
            
            if ((NSInteger)self.userInfoModel.sIdentification.length>3) {
                if ([[UserData getShowSomeId] isEqualToString:@"on"]){
                newIdCard = self.userInfoModel.sIdentification;
                    for (int i = 0; i <  (NSInteger)self.userInfoModel.sIdentification.length - 3;i++) {
                        newIdCard = [newIdCard stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@"X"];
                    }
                }else{
                    newIdCard = self.userInfoModel.sIdentification;
                }
            }else{
                newIdCard = self.userInfoModel.sIdentification;
            }
            
            [self.nameSchoolLable setText:self.userInfoModel.schoolName];
            [self.nameLabel setText:[NSString stringWithFormat:@"%@ %@",self.userInfoModel.firstName,self.userInfoModel.lastName]];
            [self.idCardLabel setText:self.userInfoModel.studentId];
            [self.departmentLabel setText:self.userInfoModel.studentClass];
            [self.positionLabel setText:newIdCard];
            if ([self.userInfoModel.imageUrl isEqualToString:@""]){
                self.imageUser.image = [UIImage imageNamed:@"ic_user_default"];
            }else{
                 [self.imageUser sd_setImageWithURL:self.userInfoModel.imageUrl];
            }
            self.imageUser.contentMode = UIViewContentModeScaleAspectFill;
            self.imageUser.clipsToBounds = YES;
            
            if ([[UserData getSchoolPic] isEqualToString:@""]) {
                self.imageSchool.image = [UIImage imageNamed:@"school"];
            }else{
               [self.imageSchool sd_setImageWithURL:[UserData getSchoolPic]];
            }
            
            //[self getImageBarcode];
            
           
            
          
        }
        else {
            
            
            [self.nameHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_NAME",nil,[Utils getLanguage],nil)];
            [self.idHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_CARD_ID_CARD",nil,[Utils getLanguage],nil)];
            [self.departmentHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_DEPARTMENT",nil,[Utils getLanguage],nil)];
            [self.positionHeadLabel setText:NSLocalizedStringFromTableInBundle(@"LABE_CARD_POSITION",nil,[Utils getLanguage],nil)];
            
            if ((NSInteger)self.userInfoModel.sIdentification.length>3) {
                if ([[UserData getShowSomeId] isEqualToString:@"on"]){
                    newIdCard = self.userInfoModel.sIdentification;
                    for (int i = 0; i <  (NSInteger)self.userInfoModel.sIdentification.length - 3;i++) {
                        newIdCard = [newIdCard stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@"X"];
                    }
                }else{
                    newIdCard = self.userInfoModel.sIdentification;
                }
            }else{
                newIdCard = self.userInfoModel.sIdentification;
            }
                
            [self.nameSchoolLable setText:self.userInfoModel.schoolName];
            [self.nameLabel setText:[NSString stringWithFormat:@"%@ %@",self.userInfoModel.firstName,self.userInfoModel.lastName]];
            [self.idCardLabel setText:newIdCard];
            [self.departmentLabel setText:self.userInfoModel.department];
            [self.positionLabel setText:self.userInfoModel.position];
             
            if ([self.userInfoModel.imageUrl isEqualToString:@""]) {
                self.imageUser.image = [UIImage imageNamed:@"ic_user_default"];
            }else{
                [self.imageUser sd_setImageWithURL:self.userInfoModel.imageUrl];
            }
            self.imageUser.contentMode = UIViewContentModeScaleAspectFill;
            self.imageUser.clipsToBounds = YES;
            
            if ([[UserData getSchoolPic] isEqualToString:@""]) {
                self.imageSchool.image = [UIImage imageNamed:@"school"];
            }else{
                [self.imageSchool sd_setImageWithURL:[UserData getSchoolPic]];
            }
            //[self getImageBarcode];
        }
    }
}
////Call To API GetImageBarcode
//- (void)getImageBarcode {
//    if(self.callImageBarCodeAPI != nil) {
//        self.callImageBarCodeAPI = nil;
//    }
//    self.callImageBarCodeAPI = [[CallImageBarCodeAPI alloc] init];
//    self.callImageBarCodeAPI.delegate = self;
//    [self.callImageBarCodeAPI call:460 width:120 userId:[UserData getUserID] generateCode:@"barcode" schoolid:[UserData getSchoolId]];
//}
//
//- (void)callImageBarCodeAPI:(CallImageBarCodeAPI *)classObj data:(NSString *)data generateCode:(NSString *)generateCode success:(BOOL)success{
//    NSLog(@"%@",data);
//    self.imageBarcode.image = [self decodeBase64ToImage:data];
//
//}
//
//- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
//    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
//    return [UIImage imageWithData:data];
//}

//Generate Barcode
- (void)generateBarcode:(NSTimer *)timer
{
    NSString *formatBarcodeStr = [NSString stringWithFormat:@"%@@%ld@%@",[UserData getUsername],(long)[UserData getUserID],[Utils dateInStringFormat:timer.fireDate format:@"dd-MM-yyyy:HH:mm:ss"]];
    CIImage *barImage = [self createBarcodeForString:formatBarcodeStr];
    float scaleX = self.imageBarcode.frame.size.width / barImage.extent.size.width;
    float scaleY = self.imageBarcode.frame.size.height / barImage.extent.size.height;
    barImage = [barImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    self.imageBarcode.image = [UIImage imageWithCIImage:barImage
                                                 scale:[UIScreen mainScreen].scale
                                           orientation:UIImageOrientationUp];
}

- (CIImage *)createBarcodeForString:(NSString *)qrString {
    NSData *stringData = [qrString dataUsingEncoding: NSISOLatin1StringEncoding];
    CIFilter *qrFilter = [CIFilter filterWithName:@"CICode128BarcodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];

    return qrFilter.outputImage;
}
@end
