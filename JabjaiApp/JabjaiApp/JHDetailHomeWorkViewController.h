//
//  JHDetailHomeWorkViewController.h
//  JabjaiApp
//
//  Created by Mac on 4/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageInboxScrollableTextDialogCollectionViewCell.h"
#import "ReportHomeWorkDetailModel.h"
#import "JHReadImageViewController.h"
#import "JHReadFileViewController.h"
NS_ASSUME_NONNULL_BEGIN
@class JHDetailHomeWorkViewController;
@protocol JHDetailHomeWorkViewControllerDelegate <NSObject>

- (void) closePageHomeWork;
- (void) openImageOfHomeWork:(JHDetailHomeWorkViewController*)objClass imageArray:(NSArray*)imageArray;
- (void) openFileOfHomeWork :(JHDetailHomeWorkViewController*)objClass pathFile:(NSString*)pathFile;

@end
@interface JHDetailHomeWorkViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, retain) id<JHDetailHomeWorkViewControllerDelegate> delegate;
// Global variables
//@property (strong, nonatomic) NSString *subjectName;
//@property (strong, nonatomic) NSString *teacherName;
//@property (strong, nonatomic) NSString *detail;
//@property (strong, nonatomic) NSDate *orderhomeDate;
//@property (strong, nonatomic) NSDate *sendhomeDate;
@property (assign, nonatomic) long long homeworkID;
//@property (assign, nonatomic) NSArray *homework;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSubjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerStartDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEndDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttachFileLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *startEndDateLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailHomework;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorFile;

@property (weak, nonatomic) IBOutlet UICollectionView *collecttionView;
@property (weak, nonatomic) IBOutlet UICollectionView *fileCollecttionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highImageConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highFileConstraint;

- (IBAction)moveBack:(id)sender;

- (void)dismissDetailHomeWorkViewController;
- (BOOL)isDialogShowing;
- (void)showDetailHomeWorkViewController:(UIView*)targetView homeworkID:(long long)homeworkID;
@end

NS_ASSUME_NONNULL_END
