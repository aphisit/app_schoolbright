//
//  BoughtProductModel.h
//  JabjaiApp
//
//  Created by mac on 5/12/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BoughtProductModel : NSObject

@property (assign, nonatomic) long long productID;
@property (strong, nonatomic) NSString *productName;
@property (assign, nonatomic) NSInteger amount;
@property (assign, nonatomic) double price;

@end
