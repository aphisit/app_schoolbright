//
//  ReportHomeWorkTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 1/4/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportHomeWorkTableViewCell.h"

@implementation ReportHomeWorkTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat max = MAX(self.redView.frame.size.width, self.redView.frame.size.height);
    self.redView.layer.cornerRadius = max/2.0;
    self.redView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
