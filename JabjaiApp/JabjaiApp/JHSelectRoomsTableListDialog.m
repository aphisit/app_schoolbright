//
//  JHSelectRoomsTableListDialog.m
//  JabjaiApp
//
//  Created by toffee on 1/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "JHSelectRoomsTableListDialog.h"
#import "UserData.h"
#import "Utils.h"

@interface JHSelectRoomsTableListDialog (){
    NSMutableArray *radio;
    NSMutableArray *nameRoom;
}
@property (nonatomic, strong) NSString *requestCode;
@property (nonatomic) BOOL isShowing;


@end

static NSString *cellIdentifier = @"Cell";
static CGFloat cellHeight = 50;

@implementation JHSelectRoomsTableListDialog

-(id)initWithRequestCode:(NSString *)requestCode {
    
    self = [[JHSelectRoomsTableListDialog alloc] init];
    
    if(self) {
        self.requestCode = requestCode;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tblView registerNib:[UINib nibWithNibName:NSStringFromClass([JHSelectRoomTableListViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    self.tblView.delegate = self;
    self.tblView.dataSource = self;
    self.tblView.layer.masksToBounds = YES;
    radio = [[NSMutableArray alloc] init];
    nameRoom = [[NSMutableArray alloc] init];
    self.isShowing = NO;
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color button next
      [self.btnNext layoutIfNeeded];
      CAGradientLayer *gradientNext = [Utils getGradientColorNextAtion];
      gradientNext.frame = self.btnNext.bounds;
      [self.btnNext.layer insertSublayer:gradientNext atIndex:0];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.tblView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(_dataArr == nil) {
        return 0;
    }
    else {
        return _dataArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JHSelectRoomTableListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    NSLog(@"index = %d",indexPath.row);
    JHSelectRoomRadioModel *model;
    model = [_dataArr objectAtIndex:indexPath.row];
    
    cell.label.text = [model classRoomName];
    if([model isSelected]) {
        if (![radio containsObject:[model classRoomID]]) {
            [radio addObject: [model classRoomID]];
            [nameRoom addObject:[model classRoomName]];
        }
       
        [cell setRadioButtonSelectedWithType:RADIO_TYPE_GREEN];
    }
    else {
        [radio removeObject: [model classRoomID]];
        [nameRoom removeObject: [model classRoomName]];
        [cell setRadioButtonClearSelected];
    }
    return cell;
}

#pragma UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isSelect = [[_dataArr objectAtIndex:indexPath.row] isSelected];
    [[_dataArr objectAtIndex:indexPath.row] setSelected:!isSelect];
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    NSLog(@"indexpath = %@",indexPath);
    [self.tblView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
    //[self.delegate onItemSelect:self.requestCode atIndex:indexPath.row];
   // [self dismissDialog];
}

//- (void)onPressRadioButton:(JHSelectRoomTableListViewCell *)tableViewCell atIndex:(NSInteger)index{
////
//    BOOL isSelected = [[_dataArr objectAtIndex:index] isSelected];
//    [[_dataArr objectAtIndex:index] setSelected:!isSelected]; // toggle selected status
////
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
//    NSLog(@"indexpath = %@",indexPath);
//    [self.tblView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//}

#pragma mark - Dialog Functions

-(void)showDialogInView:(UIView *)targetView dataArr:(JHSelectRoomRadioModel *)dataArr {
    
    _dataArr = dataArr;
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.tblView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.tblView setAlpha:1.0];
    [UIView commitAnimations];
    
    // Adjust table view frame size;
    
    int cellNumber = 6;
    if(_dataArr.count < 6) {
        cellNumber = (int)_dataArr.count;
    }
    // Calcalate position of new table view dialog
    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat viewHeight = self.view.frame.size.height;
    
    CGFloat tableViewWidth = self.view.frame.size.width - 32; //self.tblView.frame.size.width;
    CGFloat tableViewHeight = cellHeight * cellNumber;
    
    CGFloat posX = (viewWidth - tableViewWidth)/2.0;
    CGFloat posY = (viewHeight - tableViewHeight)/2.0;
    
    
    
    self.tblView.frame = CGRectMake(posX, posY, tableViewWidth, tableViewHeight);
    self.content.frame = CGRectMake(posX, posY, tableViewWidth, tableViewHeight + 50);
    self.btnNext.frame = CGRectMake(posX-16, tableViewHeight, tableViewWidth, 50);
    
     NSBundle *myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
      [_btnNext setTitle:NSLocalizedStringFromTableInBundle(@"BTN_JOBHOME_NEXT",nil,myLangBundle,nil) forState:UIControlStateNormal];

    [self.tblView reloadData];
    
    self.isShowing = YES;;
    
}

-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.tblView setAlpha:0.0];
    [self.btnNext setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
}

-(BOOL)isDialogShowing {
    return self.isShowing;
}

- (IBAction)actionNext:(id)sender {
    [self.delegate onItemSelectClassrooms:self.requestCode dataClassroomName:nameRoom dataClassroomID:radio];
     [self dismissDialog];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
