//
//  JHSelectRoomRadioModel.h
//  JabjaiApp
//
//  Created by toffee on 1/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JHSelectRoomRadioModel : NSObject

@property (nonatomic) BOOL selected;
@property (nonatomic) NSString *classRoomName;
@property (nonatomic, strong) NSNumber *classRoomID;

- (void)setSelected:(BOOL)selected;
-(void)setClassRoomName:(NSString *)classRoomName;
-(void)setClassRoomID:(NSNumber *)classRoomID;

- (BOOL)isSelected;
- (NSString *)getClassRoomName;
- (NSNumber *)getClassRoomID;
@end
