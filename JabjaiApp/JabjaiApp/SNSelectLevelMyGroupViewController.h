//
//  SNSelectLevelMyGroupViewController.h
//  JabjaiApp
//
//  Created by toffee on 2/15/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"

#import "SlideMenuController.h"

@interface SNSelectLevelMyGroupViewController : UIViewController<UITextFieldDelegate, SlideMenuControllerDelegate>

@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSMutableArray* level2Id;

@property (weak, nonatomic) IBOutlet UITextField *classLavelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classRoomTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)actionNext:(id)sender;



@end
