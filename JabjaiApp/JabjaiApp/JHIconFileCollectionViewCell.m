//
//  JHIconFileCollectionViewCell.m
//  JabjaiApp
//
//  Created by toffee on 4/7/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "JHIconFileCollectionViewCell.h"

@implementation JHIconFileCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)removeFileAction:(id)sender {
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(removeFile:typeCollectionView:)]) {
        [self.delegate removeFile:self.removeFileBtn.tag typeCollectionView:self.typeCollectionView];
    }
}
@end
