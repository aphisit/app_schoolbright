//
//  StudentManageSubjectViewController.m
//  JabjaiApp
//
//  Created by mac on 12/27/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "StudentManageSubjectViewController.h"
#import "AddSubjectTableViewController.h"
#import "DeleteSubjectTableViewController.h"
#import "Utils.h"

@interface StudentManageSubjectViewController () {
    UIColor *pagerOrangeColor;
    UIColor *pagerGreenColor;
    UIColor *indicatorOrangeColor;
    UIColor *indicatorGreenColor;
    UIColor *searchBarBG;
}

@property (nonatomic) CAPSPageMenu *pageMenu;

@property (strong, nonatomic) AddSubjectTableViewController *addSubjectController;
@property (strong, nonatomic) DeleteSubjectTableViewController *deleteSubjectController;

@end

@implementation StudentManageSubjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    pagerOrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1];
    pagerGreenColor = [UIColor colorWithRed:73/255.0 green:160/255.0 blue:152/255.0 alpha:1];
    indicatorOrangeColor = [UIColor colorWithRed:254/255.0 green:180/255.0 blue:57/255.0 alpha:1];
    indicatorGreenColor = [UIColor colorWithRed:31/255.0 green:124/255.0 blue:114/255.0 alpha:1];
    searchBarBG = [UIColor colorWithRed:224/255.0 green:93/255.0 blue:37/255.0 alpha:1];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    self.searchBar.delegate = self;
    self.searchBar.translucent = NO;
    self.searchBar.opaque = NO;
    self.searchBar.barTintColor = searchBarBG;
    self.searchBar.backgroundImage = [[UIImage alloc] init];
    
    UIView *view = [self.searchBar.subviews objectAtIndex:0];
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *cancelButton = (UIButton *)subView;
            [cancelButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        }
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [self setupPageMenu];
}

- (void)setupPageMenu {
    self.addSubjectController = [[AddSubjectTableViewController alloc] init];
    self.addSubjectController.title = @"วิชา";
    self.addSubjectController.studentManageSubjectViewController = self;
    
    self.deleteSubjectController = [[DeleteSubjectTableViewController alloc] init];
    self.deleteSubjectController.title = @"วิชาที่เพิ่ม";
    self.deleteSubjectController.studentManageSubjectViewController = self;
    
    NSArray *controllerArray = @[self.addSubjectController, self.deleteSubjectController];
    
    // Color background for each controller tab
    NSArray *colorBackground = @[pagerOrangeColor, pagerGreenColor];
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionMenuItemSeparatorWidth: @(4.3),
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                                 CAPSPageMenuOptionSelectionIndicatorColor: indicatorOrangeColor,
                                 CAPSPageMenuOptionMenuMargin: @(20),
                                 CAPSPageMenuOptionMenuHeight: @(40),
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor: [UIColor whiteColor],                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"ThaiSansNeue-SemiBold" size:22.0],
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                                 CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                                 CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                 CAPSPageMenuOptionColorBackgroundArray: colorBackground
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    
    //Optional delegate
    _pageMenu.delegate = self;
    
    [self.contentView addSubview:_pageMenu.view];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Class functions
- (IBAction)openDrawer:(id)sender {
//    [self.revealViewController revealToggle:self.revealViewController];
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}

- (void)notifyUpdateData {
    [self.addSubjectController notifyUpdateData];
    [self.deleteSubjectController notifyUpdateData];
}

#pragma mark - CAPSPageMenuDelegate

- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    //NSLog(@"WillMoveToPage");
    
    if(index == 0) {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorOrangeColor;
    }
    else {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorGreenColor;
        
    }
    
    
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
    //NSLog(@"DidMoveToPage");
    
    if(index == 0) {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorOrangeColor;
    }
    else {
        _pageMenu.selectionIndicatorView.backgroundColor = indicatorGreenColor;
        
    }
    
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    NSLog(@"%@", @"searchBarTextDidBeginEditing");
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    // return NO to not resign first responder
    NSLog(@"%@", @"searchBarShouldEndEditing");
    return YES;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    // called when text ends editing
    NSLog(@"%@", @"searchBarTextDidEndEditing");
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // called when text changes (including clear)
    NSLog(@"%@", @"searchBar textDidChange");
    
    [self.addSubjectController filterWithSearchText:searchText];
    [self.deleteSubjectController filterWithSearchText:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // called when keyboard search button pressed
    
    [self.searchBar resignFirstResponder];
    NSLog(@"%@", @"searchBarSearchButtonClicked");
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    // called when cancel button pressed
    
    [self.searchBar resignFirstResponder];
    [self.searchBar setText:@""];
    
    [self.addSubjectController setSearchActive:NO];
    [self.deleteSubjectController setSearchActive:NO];
    
    NSLog(@"%@", @"searchBarCancelButtonClicked");
}

@end
