//
//  CallASSubmitRequestGetStudentAPI.h
//  JabjaiApp
//
//  Created by Mac on 13/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CallASSubmitRequestGetStudentAPI;
@protocol CallASSubmitRequestGetStudentAPIDelegate <NSObject>

- (void) callASSubmitRequestGetStudentAPI:(CallASSubmitRequestGetStudentAPI*)classObj status:(NSString*)status success:(BOOL)success;

@end

@interface CallASSubmitRequestGetStudentAPI : NSObject
@property (nonatomic,weak) id<CallASSubmitRequestGetStudentAPIDelegate> delegate;
- (void)call:(long long)schoolID studentID:(long long)studentID;

@end

NS_ASSUME_NONNULL_END
