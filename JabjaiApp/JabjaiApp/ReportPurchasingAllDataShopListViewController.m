//
//  ReportPurchasingAllDataShopListViewController.m
//  JabjaiApp
//
//  Created by mac on 4/24/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportPurchasingAllDataShopListViewController.h"
#import "ReportPurchasingAllDataDetailViewController.h"
#import "ReportPurchasingAllDataViewController.h"


@interface ReportPurchasingAllDataShopListViewController ()

@end

@implementation ReportPurchasingAllDataShopListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)moveBack:(id)sender {
    
    ReportPurchasingAllDataViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportPurchasingAllDataStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
