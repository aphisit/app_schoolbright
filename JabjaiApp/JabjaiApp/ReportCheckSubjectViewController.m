//
//  ReportCheckSubjectViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportCheckSubjectViewController.h"
#import "ReportStudentViewController.h"
#import "UserData.h"

#import "ReportCheckSubjectCalendarViewController.h"
#import "ReportListTeacherViewController.h"
#import "AppDelegate.h"
#import "NavigationViewController.h"
#import "Utils.h"
@interface ReportCheckSubjectViewController () {
    
    NSMutableArray *classLevelStringArray;
    NSMutableArray *classroomStringArrray;
    NSMutableArray *subjectStringArray;
    
    BOOL notArrangeTimeTable;
    
    // The dialog
    TableListDialog *tableListDialog;
    AlertDialog *alertDialog;
    NSString *nameClassRoom, *nameSubject;
}

@property (strong, nonatomic) CallGetSchoolClassLevelAPI *callGetSchoolLevelAPI;
@property (strong, nonatomic) CallGetSchoolClassroomAPI *callGetSchoolClassroomAPI;
@property (strong, nonatomic) CallGetTASubjectForReportAPI *callGetTASubjectForReportAPI;

@end

static NSString *classLevelRequestCode = @"classLevelRequestCode";
static NSString *classroomRequestCode = @"classroomRequestCode";
static NSString *subjectRequestCode = @"subjectRequestCode";

@implementation ReportCheckSubjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.classLevelTextField.delegate = self;
    self.classroomTextField.delegate = self;
    self.subjectTextField.delegate = self;
    _selectedClassLevelIndex = -1;
    _selectedClassroomIndex = -1;
    _selectedSubjectIndex = -1;
    notArrangeTimeTable = YES;
    [self getSchoolClassLevel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientNext;
    
    //set color button next
    [self.nextButton layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.nextButton.bounds;
    [self.nextButton.layer insertSublayer:gradientNext atIndex:0];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section2.layer setShadowOpacity:0.3];
    [self.section2.layer setShadowRadius:3.0];
    [self.section2.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.section3.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section3.layer setShadowOpacity:0.3];
    [self.section3.layer setShadowRadius:3.0];
    [self.section3.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.nextButton.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.nextButton.layer setShadowOpacity:0.3];
    [self.nextButton.layer setShadowRadius:3.0];
    [self.nextButton.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    self.headerClassLevelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_CLASSLEVEL",nil,[Utils getLanguage],nil);
    self.headerClassLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_CLASS",nil,[Utils getLanguage],nil);
    self.headerSubjectLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_SUBJECT",nil,[Utils getLanguage],nil);
    
    self.classLevelTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_CHECKNAME_REP_CLASS",nil,[Utils getLanguage],nil);
    self.classroomTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_CHECKNAME_REP_CLASSROOM",nil,[Utils getLanguage],nil);
    self.subjectTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_CHECKNAME_REP_SUBJECT",nil,[Utils getLanguage],nil);
    
    [self.nextButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_CHECKNAME_REP_NEXT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 1) { // when user select class level textfield
        
        if(classLevelStringArray != nil && classLevelStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:classLevelRequestCode data:classLevelStringArray];
        }
        
        return NO;
    }
    else if(textField.tag == 2) { // select classroom textfield
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CHECKNAME_REP_SELECT_CLASSLEVEL",nil,[Utils getLanguage],nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(classroomStringArrray != nil && classroomStringArrray.count > 0) {
            [self showTableListDialogWithRequestCode:classroomRequestCode data:classroomStringArrray];
        }
        
        return NO;
    }
    else if(textField.tag == 3) {
        
        if(self.classLevelTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CHECKNAME_REP_SELECT_CLASSLEVEL",nil,[Utils getLanguage],nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(self.classroomTextField.text.length == 0) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CHECKNAME_REP_SELECT_CLASS",nil,[Utils getLanguage],nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(notArrangeTimeTable) {
            NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CHECKNAME_REP_NOTEBLE_TEACHER",nil,[Utils getLanguage],nil);
            [self showAlertDialogWithMessage:alertMessage];
        }
        else if(subjectStringArray != nil && subjectStringArray.count > 0) {
            [self showTableListDialogWithRequestCode:subjectRequestCode data:subjectStringArray];
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - API Caller

- (void)getSchoolClassLevel {
    [self showIndicator];
    if(self.callGetSchoolLevelAPI != nil) {
        self.callGetSchoolLevelAPI = nil;
    }
    
    self.callGetSchoolLevelAPI = [[CallGetSchoolClassLevelAPI alloc] init];
    self.callGetSchoolLevelAPI.delegate = self;
    [self.callGetSchoolLevelAPI call:[UserData getSchoolId]];
}

- (void)getSchoolClassroomWithClassLevelId:(long long)classLevelId {
    [self showIndicator];
    if(self.callGetSchoolClassroomAPI != nil) {
        self.callGetSchoolClassroomAPI = nil;
    }
    
    self.callGetSchoolClassroomAPI = [[CallGetSchoolClassroomAPI alloc] init];
    self.callGetSchoolClassroomAPI.delegate = self;
    [self.callGetSchoolClassroomAPI call:[UserData getSchoolId] classLevelId:classLevelId];
}

- (void)getSubjectDataWithClassroomId:(long long)classroomId {
    [self showIndicator];
    if(self.callGetTASubjectForReportAPI != nil) {
        self.callGetTASubjectForReportAPI = nil;
    }
    
    self.callGetTASubjectForReportAPI = [[CallGetTASubjectForReportAPI alloc] init];
    self.callGetTASubjectForReportAPI.delegate = self;
    [self.callGetTASubjectForReportAPI call:[UserData getSchoolId] classroomId:classroomId];
}

#pragma mark - CallGetSchoolClassLevelAPIDelegate

- (void)callGetSchoolClassLevelAPI:(CallGetSchoolClassLevelAPI *)classObj data:(NSArray<SchoolLevelModel *> *)data success:(BOOL)success {
    [self stopIndicator];
    if(success && data != nil) {
        _classLevelArray = data;
        // clear array
        if(classLevelStringArray != nil) {
            [classLevelStringArray removeAllObjects];
            classLevelStringArray = nil;
        }
        
        classLevelStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolLevelModel *model = [data objectAtIndex:i];
            [classLevelStringArray addObject:[model getClassLevelName]];
        }
    }
}

#pragma mark - CallGetSchoolClassroomAPIDelegate

- (void)callGetSchoolClassroomAPI:(CallGetSchoolClassroomAPI *)classObj data:(NSArray<SchoolClassroomModel *> *)data success:(BOOL)success {
    [self stopIndicator];
    if(success && data != nil) {
        _classroomArray = data;
        // clear array
        if(classroomStringArrray != nil) {
            [classroomStringArrray removeAllObjects];
            classroomStringArrray = nil;
        }
        
        classroomStringArrray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            SchoolClassroomModel *model = [data objectAtIndex:i];
            [classroomStringArrray addObject:[model getClassroomName]];
        }
    }
}

#pragma mark - CallGetTASubjectForReportAPIDelegate

- (void)callGetTASubjectForReportAPI:(CallGetTASubjectForReportAPI *)classObj data:(NSArray<TASubjectModel *> *)data success:(BOOL)success {
    [self stopIndicator];
    if(success && data != nil) {
        
        _subjectArray = data;
        notArrangeTimeTable = NO;
        
        // clear array
        if(subjectStringArray != nil) {
            [subjectStringArray removeAllObjects];
            subjectStringArray = nil;
        }
        
        subjectStringArray = [[NSMutableArray alloc] init];
        
        for(int i=0; i<data.count; i++) {
            TASubjectModel *model = [data objectAtIndex:i];
            [subjectStringArray addObject:model.subjectName];
        }
    }
}

- (void)notArrangeTimeTable:(CallGetTASubjectForReportAPI *)classObj {
    [self stopIndicator];
    notArrangeTimeTable = YES;
}

#pragma mark - Utility
- (BOOL)validateData {
    
    if(self.classLevelTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CHECKNAME_REP_SELECT_CLASSLEVEL",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.classroomTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CHECKNAME_REP_SELECT_CLASS",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    else if(self.subjectTextField.text.length == 0) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_CHECKNAME_REP_SELECT_SUBJECT",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {
   // [self stopIndicator];
    if([requestCode isEqualToString:classLevelRequestCode]) {
        
        if(index == _selectedClassLevelIndex) {
            return;
        }
        
        self.classLevelTextField.text = [[_classLevelArray objectAtIndex:index] getClassLevelName];
        _selectedClassLevelIndex = index;
        
        // Clear dependency text field values
        self.classroomTextField.text = @"";
        self.subjectTextField.text = @"";
        
        _selectedClassroomIndex = -1;
        _selectedSubjectIndex = -1;
        
        // get classroom data with class level
        self.selectedClassLevelId = [[_classLevelArray objectAtIndex:index] getClassLevelId];
        [self getSchoolClassroomWithClassLevelId:self.selectedClassLevelId];
        
    }
    else if([requestCode isEqualToString:classroomRequestCode]) {
        
        if(index == _selectedClassroomIndex) {
            return;
        }
        nameClassRoom = [[_classroomArray objectAtIndex:index] getClassroomName];
        self.classroomTextField.text = nameClassRoom;
        _selectedClassroomIndex = index;
        
        // Clear dependency text field values;
        self.subjectTextField.text = @"";
        _selectedSubjectIndex = -1;
        
        // get subject data with classroom id
        self.selectedClassroomId = [[[_classroomArray objectAtIndex:index] getClassroomID] longLongValue];
        [self getSubjectDataWithClassroomId:self.selectedClassroomId];
        
    }
    else if([requestCode isEqualToString:subjectRequestCode]) {
        
        if(index == _selectedSubjectIndex) {
            return;
        }
        
        nameSubject = [_subjectArray objectAtIndex:index].subjectName;
        self.subjectTextField.text = nameSubject;
        _selectedSubjectIndex = index;
        
        self.selectedSubjectId = [_subjectArray objectAtIndex:index].subjectId;
    }
    
}

#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;
    [tableListDialog showDialogInView:topView dataArr:data];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;
    [alertDialog showDialogInView:topView title:@"แจ้งเตือน" message:message];
}
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)actionNext:(id)sender {
    
    if([self validateData]) {
        
        
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ReportCheckSubjectCalendarViewController *viewController = (ReportCheckSubjectCalendarViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ReportCheckSubjectCalendarStoryboard"];
                viewController.classLevelArray = _classLevelArray;
                viewController.classroomArray = _classroomArray;
                viewController.subjectArray = _subjectArray;
                viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
                viewController.selectedClassroomIndex = _selectedClassroomIndex;
                viewController.selectedSubjectIndex = _selectedSubjectIndex;
                viewController.selectedClassLevelId = _selectedClassLevelId;
                viewController.selectedClassroomId = _selectedClassroomId;
                viewController.selectedSubjectId = _selectedSubjectId;
                viewController.nameClassRoom = nameClassRoom;
                viewController.nameSubject = nameSubject;
        NavigationViewController *leftViewController = (NavigationViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"NavigationStoryboard"];
        
        SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:viewController leftMenuViewController:leftViewController];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [[appDelegate window] setRootViewController:slideMenuController];

        
        [self.slideMenuController changeMainViewController:viewController close:YES];
    
    }
    
    
}

- (IBAction)moveBack:(id)sender {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    
}
@end
