//
//  CallRMRefillMonyAPI.h
//  JabjaiApp
//
//  Created by Mac on 20/5/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CallRMRefillMonyAPI;
@protocol CallRMRefillMonyAPIDelegate <NSObject>
- (void) callRMRefillMonyAPI:(CallRMRefillMonyAPI *)classObj success:(BOOL)success;
@end

@interface CallRMRefillMonyAPI : NSObject
@property (nonatomic, weak) id<CallRMRefillMonyAPIDelegate> delegate;
- (void)call:(NSString*)jsonString;
@end

NS_ASSUME_NONNULL_END
