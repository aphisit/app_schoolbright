//
//  ReportLeaveDetailViewControllerXIB.h
//  JabjaiApp
//
//  Created by Mac on 24/12/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "CancelLeaveDialog.h"
NS_ASSUME_NONNULL_BEGIN

@interface ReportLeaveDetailViewControllerXIB : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource,SlideMenuControllerDelegate, CancelLeaveDialogDelegate>

@property (assign, nonatomic) long long messageID;
@property (assign, nonatomic) long long letterID;
@property (nonatomic) NSInteger page;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPrositionLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerMeanLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerStartDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEndDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerApproveLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAddressLabel;





@property (weak, nonatomic) IBOutlet UIView *headMenuBar;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UILabel *leaveNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *causeLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *approveLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
//@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (weak, nonatomic) IBOutlet UIView *statusBackGround;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
//@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCollectionViewConstraint;


//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContentConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMenuConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightStatusConstraint;
//@property (weak, nonatomic) IBOutlet UILabel *headAddressLabel;.
//@property (weak, nonatomic) IBOutlet UILabel *headChangeLabel;


@property (weak, nonatomic) IBOutlet UIButton *moreButton;



- (IBAction)moveBack:(id)sender;
- (void)dismissDetailLeaveViewController;
- (BOOL)isDialogShowing;
- (void)showDetailLeaveViewController:(UIView*)targetView letterID:(long long)letterID;
@end

NS_ASSUME_NONNULL_END
