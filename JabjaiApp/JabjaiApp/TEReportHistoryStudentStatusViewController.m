//
//  TEReportHistoryStudentStatusViewController.m
//  JabjaiApp
//
//  Created by toffee on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TEReportHistoryStudentStatusViewController.h"
#import "TEReportARSPContainerController.h"
#import "TAReportStudentStatusTableViewCell.h"
#import "Utils.h"
#import "DateUtility.h"
#import "UserData.h"
#import "TAReportStudentStatusModel.h"

@interface TEReportHistoryStudentStatusViewController (){
    NSArray<TAReportStudentStatusModel *> *studentStatusArray;
    
    NSDate *consideredDate;
    NSDateFormatter *dateFormatter;
    
    UIColor *grayColor;
    
    long long classroomId;
    NSString *subjectId;
}
@property (strong, nonatomic) TEReportARSPContainerController *arspContainerController;
@property (strong, nonatomic) TEReportCalendarViewController *taReportCalendarViewController;

@property (strong, nonatomic) CallGetTEHistoryStudentStatusList *callGetTEHistoryStudentStatusListAPI;
@end

@implementation TEReportHistoryStudentStatusViewController
static NSString *cellIdentifier = @"Cell";
- (void)viewDidLoad {
    [super viewDidLoad];
    
    grayColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.backgroundColor = grayColor;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TAReportStudentStatusTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
//    self.arspContainerController = (TEReportARSPContainerController *)self.parentViewController;
//    self.taReportCalendarViewController = (TEReportCalendarViewController *)self.arspContainerController.mainViewController;
//    self.taReportCalendarViewController.delegate = self;
    
    classroomId = self.arspContainerController.selectedClassroomId;
    
    
    dateFormatter = [Utils getDateFormatter];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    if(consideredDate == nil) {
        consideredDate = [NSDate date];
    }
    
    self.dateTitleLabel.text = [Utils getThaiDateFormatWithDate:consideredDate];
    
    [self getHistoryStudentStatus];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - API Caller

- (void)getHistoryStudentStatus {
    
    self.callGetTEHistoryStudentStatusListAPI = nil;
    self.callGetTEHistoryStudentStatusListAPI = [[CallGetTEHistoryStudentStatusList alloc] init];
    self.callGetTEHistoryStudentStatusListAPI.delegate = self;
    [self.callGetTEHistoryStudentStatusListAPI call:[UserData getSchoolId] classroomId:classroomId date:consideredDate];
    
    [self showIndicator];
}

#pragma mark - CallGetTAHistoryStudentStatusListAPIDelegate
- (void)callGetTEHistoryStudentStatusListAPI:(CallGetTEHistoryStudentStatusList *)classObj data:(NSArray<TAReportStudentStatusModel *> *)data success:(BOOL)success {
    
    [self stopIndicator];
    
    if(success && data != nil) {
        studentStatusArray = data;
        
        [self.tableView reloadData];
    }
}
#pragma mark - TAReportCalendarViewControllerDelegate
- (void)onSelectDate:(TEReportCalendarViewController *)classObj date:(NSDate *)date {
    
    self.dateTitleLabel.text = [Utils getThaiDateFormatWithDate: date];
    
    if(![DateUtility sameDate:consideredDate date2:date] ) {
        consideredDate = date;
        [self getHistoryStudentStatus];
    }
}
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(studentStatusArray != nil) {
        return studentStatusArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TAReportStudentStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    TAReportStudentStatusModel *model = [studentStatusArray objectAtIndex:indexPath.row];
    NSArray *array = [[NSString stringWithFormat:@"%@", [model getStudentName]] componentsSeparatedByString:@" "];
    
    cell.titleLabel.text = array[0];
    cell.lastName.text = array[1];
    cell.runNumber.text = [NSString stringWithFormat:@"%ld.",indexPath.row+1];
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [model getStudentPic]]];
    if(imageData != nil){
        cell.imageUser.image = [UIImage imageWithData: imageData];
    }else{
        cell.imageUser.image = [UIImage imageNamed:@"ic_user_info"];
    }
    
    cell.imageUser.layer.cornerRadius = cell.imageUser.frame.size.height /2;
    cell.imageUser.layer.masksToBounds = YES;
    cell.imageUser.layer.borderWidth = 0;
    
    
    [cell updateStatus:[model getStatus]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = [UIColor whiteColor];
    //cell.userInteractionEnabled = YES;
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}


@end
