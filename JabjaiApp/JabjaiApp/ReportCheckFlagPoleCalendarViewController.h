//
//  ReportCheckFlagPoleCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CFSCalendar.h"

#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"

#import "CallGetTEHistoryStudentStatusList.h"
#import "ReportCheckNameViewController.h"
#import "SlideMenuController.h"
#import "RPCheckNameFlagAttendanceModel.h"

@interface ReportCheckFlagPoleCalendarViewController : UIViewController <CFSCalendarDataSource, CFSCalendarDelegate, UITableViewDataSource, UITableViewDelegate, CallGetTEHistoryStudentStatusListAPIDelegate, SlideMenuControllerDelegate>

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long selectedClassLevelId;
@property (nonatomic) long long selectedClassroomId;
@property (nonatomic) NSString *nameClassRoom;

@property (weak ,nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerOntimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLastLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAbsenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSickLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPersonalLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEventLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerUndefinedLabel;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UILabel *dateTitle;

@property (weak, nonatomic) IBOutlet UIView *viewNextAction;

//sum status
@property (weak, nonatomic) IBOutlet UILabel *onTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *lateLabel;
@property (weak, nonatomic) IBOutlet UILabel *absenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *sickLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *undefinedLabel;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailContraint;

@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)openDetail:(id)sender;

- (IBAction)moveBack:(id)sender;

@end
