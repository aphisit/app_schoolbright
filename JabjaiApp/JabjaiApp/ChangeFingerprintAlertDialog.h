//
//  ChangeFingerprintAlertDialog.h
//  JabjaiApp
//
//  Created by mac on 12/21/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangeFingerprintAlertDialogDelegate <NSObject>

@optional
- (void)onChangeFingerprintAlertDialogClose;

@end

@interface ChangeFingerprintAlertDialog : UIViewController

@property (retain, nonatomic) id<ChangeFingerprintAlertDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

- (IBAction)closeDialog:(id)sender;

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
