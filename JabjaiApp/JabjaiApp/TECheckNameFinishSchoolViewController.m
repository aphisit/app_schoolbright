//
//  TECheckNameFinishSchoolViewController.m
//  JabjaiApp
//
//  Created by toffee on 30/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "TECheckNameFinishSchoolViewController.h"
#import "Utils.h"
#import "UserData.h"
@interface TECheckNameFinishSchoolViewController (){
    UIColor *OrangeColor, *GrayColor;
    CAPSPageMenu *pageMenu;
    ConfirmScanerLateViewController *dialogConfirmScaner;
    AlertDialogConfirm *dialogSuccess;
    AlertDialog *dialogUnSuccess;
    NSString *studentCodeStr;
    int numberMode;
}
@property (strong, nonatomic) CallGetTEFinishschoolAPI *callGetTEFinishschoolAPI;
//@property (nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation TECheckNameFinishSchoolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    OrangeColor = [UIColor colorWithRed:245/255.0 green:107/255.0 blue:32/255.0 alpha:1.0];
    GrayColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
    [self setupPagemenu];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_FINISH_SCHOOL",nil,[Utils getLanguage],nil);
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}

- (void)setupPagemenu{
    TEScanCheckNameFinishSchoolViewController *scannerViewController = [[TEScanCheckNameFinishSchoolViewController alloc] init];
    scannerViewController.title = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_QRCODE",nil,[Utils getLanguage],nil);
    scannerViewController.delegate = self;
    scannerViewController.mode = 0;
    TEPadNumberCheckNameFinishSchoolViewController *padNumberViewController = [[TEPadNumberCheckNameFinishSchoolViewController alloc] init];
     padNumberViewController.title = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_STUDENTID",nil,[Utils getLanguage],nil);
    padNumberViewController.delegate = self;
    padNumberViewController.mode = 1;
    NSArray *controllerArray = @[scannerViewController, padNumberViewController];
    NSDictionary *parameters;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(40),
                       CAPSPageMenuOptionMenuHeight: @(50),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:32.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    else{
        parameters = @{
                       CAPSPageMenuOptionMenuItemSeparatorWidth:@(4.3),
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:0.1],
                       CAPSPageMenuOptionSelectionIndicatorColor: [UIColor clearColor],
                       CAPSPageMenuOptionMenuMargin: @(20),
                       CAPSPageMenuOptionMenuHeight: @(40),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: GrayColor,
                       CAPSPageMenuOptionUnselectedMenuItemLabelColor: GrayColor,CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"THSarabunNew-Bold" size:18.0],
                       CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                       CAPSPageMenuOptionMenuItemSeparatorRoundEdges: @(YES),
                       CAPSPageMenuOptionSelectionIndicatorHeight: @(2.0),
                       CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                       };
    }
    [self.contentView layoutIfNeeded];
    pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
    // For the first time set first tab background to orange and label color to white
    NSArray *menuItems = pageMenu.menuItems;
    MenuItemView *menuItemView = [menuItems objectAtIndex:pageMenu.currentPageIndex];
    menuItemView.backgroundColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.40 alpha:1.0];
    menuItemView.titleLabel.textColor = [UIColor whiteColor];

    //Optional delegate
    pageMenu.delegate = self;
    [self.contentView addSubview:pageMenu.view];
}

#pragma CAPSPageMenuDelegate
- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    NSArray *menuItems = pageMenu.menuItems;
    int count = 0;
    for(MenuItemView *menuItemView in menuItems) {
        if(count == index) {
            menuItemView.backgroundColor = [UIColor colorWithRed:1.00 green:0.49 blue:0.40 alpha:1.0];
            menuItemView.titleLabel.textColor = [UIColor whiteColor];
        }
        else {
            menuItemView.backgroundColor = [UIColor whiteColor];
            menuItemView.titleLabel.textColor = GrayColor;
        }
        count++;
    }
}

#pragma responds TEPadNumberCheckNameFinishSchoolViewControllerXIB/TEScanerCheckNameFinishSchoolViewControllerXIB

- (void)replyIdStudent:(TEDataStudentScanerModel *)data studentCode:(NSString *)studentCode mode:(int)mode success:(BOOL)success{
    studentCodeStr = studentCode;
    numberMode = mode;
    [self showAlertDialogConfirmScaner:data];
}
- (void)responseAleatMessage:(NSString *)message mode:(int)mode{
    numberMode = mode;
    [self showAlertDialogUnSuccessWithMessage:message];
}

#pragma  ConfirmScanerLateViewControllerXIB
//show dialog confirmScaner
- (void)showAlertDialogConfirmScaner:(TEDataStudentScanerModel *)data {
    if(dialogConfirmScaner != nil && [dialogConfirmScaner isDialogShowing]) {
        [dialogConfirmScaner dismissDialog];
    }
    dialogConfirmScaner = [[ConfirmScanerLateViewController alloc] init];
    dialogConfirmScaner.delegate = self;
    [dialogConfirmScaner showDialogInView:self.view data:data statusScan:0]; // status 0 is Ontime
}

- (void)doCallConfirmScaner{
    [self doCallCheckOutShcoolAPI:[UserData getSchoolId] userID:[UserData getUserID] studentCode:studentCodeStr status:@"0"];
}

- (void)onAlertDialogScanerClose{
    if (numberMode == 0) {
        [self setupPagemenu];
    }
}

#pragma mark - CallGetTEFinishschoolAPI
-(void) doCallCheckOutShcoolAPI:(long long)shcoolID userID:(long long)userID studentCode:(NSString*)studentCode status:(NSString*)status{
    if (self.callGetTEFinishschoolAPI != nil) {
        self.callGetTEFinishschoolAPI = nil;
    }
    self.callGetTEFinishschoolAPI = [[CallGetTEFinishschoolAPI alloc]init];
    self.callGetTEFinishschoolAPI.delegate = self;
    [self.callGetTEFinishschoolAPI call:shcoolID userId:userID barcode:studentCode status:status];
}
- (void) callGetTEFinishschoolAPI:(CallGetTEFinishschoolAPI *)classObj resCode:(NSInteger)resCode success:(BOOL)success{
    if (resCode == 200 && success) {
        [self showAlertDialogWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_CHECK_OUT",nil,[Utils getLanguage],nil)];
    }else{
        [self showAlertDialogUnSuccessWithMessage:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_TRY_AGAIN",nil,[Utils getLanguage],nil)];
    }
}

#pragma mark - AlertDialogConfirm/AlertDialog
- (void)showAlertDialogWithMessage:(NSString *)message {
    if(dialogSuccess != nil) {
        if([dialogSuccess isDialogShowing]) {
            [dialogSuccess dismissDialog];
        }
    }
    else {
        dialogSuccess = [[AlertDialogConfirm alloc] init];
        dialogSuccess.delegate = self;
    }
    [dialogSuccess showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}
- (void)showAlertDialogUnSuccessWithMessage:(NSString *)message {
    if(dialogUnSuccess != nil && [dialogUnSuccess isDialogShowing]) {
        [dialogUnSuccess dismissDialog];
    }
    else {
        dialogUnSuccess = [[AlertDialog alloc] init];
        dialogUnSuccess.delegate = self;
    }
    [dialogUnSuccess showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}
- (void)onAlertDialogClose{
    if (numberMode == 0) {
        [self setupPagemenu];
    }
}

- (IBAction)openDrawer:(id)sender {
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
}
@end
