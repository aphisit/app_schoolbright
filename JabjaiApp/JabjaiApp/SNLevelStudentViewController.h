//
//  SNLevelStudentViewController.h
//  JabjaiApp
//
//  Created by toffee on 8/28/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfoDBHelper.h"
#import "TableListDialog.h"
#import "SchoolLevelModel.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "SlideMenuController.h"
#import "UnAuthorizeMenuDialog.h"
#import "CallGetMenuListAPI.h"
#import "LoginViewController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface SNLevelStudentViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassLevelAPIDelegate, SlideMenuControllerDelegate,UnAuthorizeMenuDialogDelegate,CallGetMenuListAPIDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>

@property (nonatomic) NSInteger sendType;
@property (nonatomic) NSInteger newsType;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRecipientLabel;
@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *userTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIView *section2;

- (IBAction)openDrawer:(id)sender;
- (IBAction)actionNext:(id)sender;
- (IBAction)selectMenu:(id)sender;



@end
