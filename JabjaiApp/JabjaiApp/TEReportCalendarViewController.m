//
//  TEReportCalendarViewController.m
//  JabjaiApp
//
//  Created by toffee on 11/15/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TEReportCalendarViewController.h"
#import "TEReportARSPContainerController.h"
#import "TEReportSelectClassLevelViewController.h"
#import "TAReportStudentStatusTableViewCell.h"
#import "Utils.h"
#import "DateUtility.h"
#import "UserData.h"
#import "TAReportStudentStatusModel.h"


@interface TEReportCalendarViewController (){
    
    NSArray<TAReportStudentStatusModel *> *studentStatusArray;
    
    NSDate *consideredDate;
    NSDateFormatter *dateFormatter;
    
    UIColor *grayColor;
    
    long long classroomId;
    NSString *subjectId;
    
    UIColor *headerTitleColor;
    UIColor *todayColor;
    UIColor *selectionColor;
    CAGradientLayer *gradient;
    
    NSCalendar *gregorian;
}
//@property (strong, nonatomic) TEReportARSPContainerController *arspContainerController;

@property (nonatomic) CGFloat shadowRadius;
@property (nonatomic) CGFloat shadowOpacity;
@property (nonatomic) CGFloat animationDuration;

@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;

@property (strong, nonatomic) CallGetTEHistoryStudentStatusList *callGetTEHistoryStudentStatusListAPI;

@end

@implementation TEReportCalendarViewController

static NSString *cellIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    selectionColor = [UIColor colorWithRed:212/255.0 green:44/255.0 blue:31/255.0 alpha:1.0];
    
//    gradient = [Utils getGradientColorHeader]; //set color headerView
//    gradient.frame = self.headerView.bounds;
//    [self.headerView.layer insertSublayer:gradient atIndex:0];
//    
//    gregorian = [Utils getGregorianCalendar];
    
    self.minimumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
    self.maximumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:1 toDate:[NSDate date] options:0];
    
    // Sliding up panel
//    self.arspContainerController = (TEReportARSPContainerController *)self.parentViewController;
//    self.arspContainerController.dragDelegate = self;
//    self.arspContainerController.visibilityStateDelegate = self;
    
//    NSString *subjectName = [self.arspContainerController.subjectArray objectAtIndex:self.arspContainerController.selectedSubjectIndex].subjectName;
//    self.headerTitleLabel.text = subjectName;
    
    self.shadowRadius = 5.0f;
    self.shadowOpacity = 0.5f;
    self.animationDuration = 0.55f;
    
//    self.arspContainerController.dropShadow = YES;
//    self.arspContainerController.shadowRadius = self.shadowRadius;
//    self.arspContainerController.shadowOpacity = self.shadowOpacity;
//    self.arspContainerController.animationDuration = self.animationDuration;
    // Do any additional setup after loading the view.
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height >= 1024.0f) {
            self.heightCalendarConstraint.constant = 450.0f;
        }
    }
    else{
        self.heightCalendarConstraint.constant = 300.0f;
    }
    
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_up"] forState:UIControlStateNormal];
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_down"] forState:UIControlStateSelected];
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = grayColor;
    
    classroomId = self.selectedClassroomId;
    
    
    dateFormatter = [Utils getDateFormatter];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    if(consideredDate == nil) {
        consideredDate = [NSDate date];
    }
    
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:consideredDate];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TAReportStudentStatusTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self getHistoryStudentStatus];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createCalendar {
    
    // Remove all subviews from container
    // Remove all subviews from container
    //    NSArray *viewsToRemove = self.containerView.subviews;
    //    for(UIView *v in viewsToRemove) {
    //        [v removeFromSuperview];
    //    }
    
    self.containerView.backgroundColor = grayColor;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, height)];
    
    calendar.dataSource = self;
    calendar.delegate = self;
    //calendar.swipeToChooseGesture.enabled = YES;
    calendar.backgroundColor = grayColor;//[UIColor whiteColor];
    //    calendar.appearance.titleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:22.0f];
    //    calendar.appearance.subtitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:22.0f];
    //    calendar.appearance.weekdayFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:22.0f];
    //    calendar.appearance.headerTitleFont = [UIFont fontWithName:@"ThaiSansNeue-Regular" size:26.0f];
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
    calendar.appearance.selectionColor = selectionColor;
    calendar.appearance.todayColor = todayColor;
    [self.containerView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 95, 34);
    previousButton.backgroundColor = grayColor;//[UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    nextButton.frame = CGRectMake(CGRectGetWidth(self.containerView.frame) - 95, 5, 95, 34);
    nextButton.frame = CGRectMake(self.view.frame.size.width - nextButton.frame.size.width - 95, 5, 95, 34);
    nextButton.backgroundColor = grayColor; //[UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:nextButton];
    self.nextButton = nextButton;
    
    //    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    //
    //    self.panelControllerContainer.visibleZoneHeight = screenHeight - (self.calendar.frame.size.height + self.calendar.frame.origin.y) - (self.headerView.frame.origin.y + self.headerView.frame.size.height);
    //    self.panelControllerContainer.swipableZoneHeight = 0;
    //    self.panelControllerContainer.draggingEnabled = YES;
    //    self.panelControllerContainer.shouldOverlapMainViewController = YES;
    //    self.panelControllerContainer.shouldShiftMainViewController = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self createCalendar];
}

- (void)panelControllerWasDragged:(CGFloat)panelControllerVisibility {
    
}

- (void)panelControllerChangedVisibilityState:(ARSPVisibilityState)state {
    
}

#pragma mark - CFSCalendarDataSource

- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

#pragma mark - CFSCalendarDelegate

- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
    if([DateUtility sameMonthOfYear:date date2:calendar.currentPage]) {
        return YES;
    }
    
    return NO;
}

- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
//    if(self.delegate && [self.delegate respondsToSelector:@selector(onSelectDate:date:)]) {
//        [self.delegate onSelectDate:self date:date];
//    }
    
        self.dateTitle.text = [Utils getThaiDateFormatWithDate: date];
    
        if(![DateUtility sameDate:consideredDate date2:date] ) {
            consideredDate = date;
            [self getHistoryStudentStatus];
        }
    
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //    formatter.dateFormat = @"dd/MM/yyyy";
    //    NSLog(@"%@", [formatter stringFromDate:date]);
}

#pragma mark - Class buttons event

- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
}

- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
}

#pragma mark - Action functions
- (IBAction)moveBack:(id)sender {
    TEReportSelectClassLevelViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TEReportSelectClassLevelStoryboard"];
    
    viewController.classLevelArray = self.classLevelArray;
    viewController.classroomArray = self.classroomArray;

    viewController.selectedClassLevelIndex = self.selectedClassLevelIndex;
    viewController.selectedClassroomIndex = self.selectedClassroomIndex;
    viewController.classLevelId = self.selectedClassLevelId;
    viewController.classroomId = self.selectedClassroomId;

    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openDetail:(id)sender {
    
    self.arrowStatement.selected = !self.arrowStatement.selected;
    
    if (self.arrowStatement.isSelected == YES) {
        //        self.topDetailConstraint.constant = -300.0f;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailContraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailContraint.constant = -450.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailContraint.constant = 0.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailContraint.constant = -300.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
        
        
        
    }else{
        //        self.topDetailConstraint.constant = 0.0f;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailContraint.constant = -450.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailContraint.constant = 0.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailContraint.constant = -300.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailContraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
    }
    
}

- (void)getHistoryStudentStatus {
    
    self.callGetTEHistoryStudentStatusListAPI = nil;
    self.callGetTEHistoryStudentStatusListAPI = [[CallGetTEHistoryStudentStatusList alloc] init];
    self.callGetTEHistoryStudentStatusListAPI.delegate = self;
    [self.callGetTEHistoryStudentStatusListAPI call:[UserData getSchoolId] classroomId:classroomId date:consideredDate];
    
    [self showIndicator];
}

#pragma mark - CallGetTAHistoryStudentStatusListAPIDelegate
- (void)callGetTEHistoryStudentStatusListAPI:(CallGetTEHistoryStudentStatusList *)classObj data:(NSArray<TAReportStudentStatusModel *> *)data success:(BOOL)success {
    
    [self stopIndicator];
    
    if(success && data != nil) {
        studentStatusArray = data;
        
        [self.tableView reloadData];
    }
}
//#pragma mark - TAReportCalendarViewControllerDelegate
//- (void)onSelectDate:(TEReportCalendarViewController *)classObj date:(NSDate *)date {
//
//    self.dateTitleLabel.text = [Utils getThaiDateFormatWithDate: date];
//
//    if(![DateUtility sameDate:consideredDate date2:date] ) {
//        consideredDate = date;
//        [self getHistoryStudentStatus];
//    }
//}
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(studentStatusArray != nil) {
        return studentStatusArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TAReportStudentStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    TAReportStudentStatusModel *model = [studentStatusArray objectAtIndex:indexPath.row];
    NSArray *array = [[NSString stringWithFormat:@"%@", [model getStudentName]] componentsSeparatedByString:@" "];
    
    cell.titleLabel.text = array[0];
    cell.lastName.text = array[1];
    cell.runNumber.text = [NSString stringWithFormat:@"%ld.",indexPath.row+1];
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [model getStudentPic]]];
    if(imageData != nil){
        cell.imageUser.image = [UIImage imageWithData: imageData];
    }else{
        cell.imageUser.image = [UIImage imageNamed:@"ic_user_info"];
    }
    
    cell.imageUser.layer.cornerRadius = cell.imageUser.frame.size.height /2;
    cell.imageUser.layer.masksToBounds = YES;
    cell.imageUser.layer.borderWidth = 0;
    
    
    [cell updateStatus:[model getStatus]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.contentView.backgroundColor = [UIColor whiteColor];
    //cell.userInteractionEnabled = YES;
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

@end
