//
//  TeachersTimeTableViewController.m
//  JabjaiApp
//
//  Created by toffee on 19/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "TeachersTimeTableViewController.h"
#import "Utils.h"
#import "UserData.h"
@interface TeachersTimeTableViewController (){
    int backgroundInterger;
    long indexTableInterger;
    NSArray *dayArray;
    NSMutableArray *subjectArray, *img;
    NSMutableDictionary *keyIndexDict;
    NSArray * weekdays;
}
@property (strong, nonatomic)CallTeacherTimeDayAPI *callTeacherTimeDayAPI;
@property (strong, nonatomic)CallTeacherTimeTableDetailAPI *callTeacherTimeTableDetailAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end
static NSString *cellIdentifier = @"Cell";
@implementation TeachersTimeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    self.showHaveNotSubject.hidden = NO;
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([TeacherTimeTableSelectDayCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TeacherTimeTableListSubjectTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TeacherTimeTableListSubjectDetailTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"CellDetail"];
    
    //set month
    NSString *daysStr = [Utils getThaiDateFormatWithDate:[NSDate date]];
    NSArray *array = [daysStr componentsSeparatedByString:@" "];
    self.monthLable.text = array[1];
    subjectArray = [[NSMutableArray alloc] init];
    keyIndexDict = [[NSMutableDictionary alloc] init];
    dayArray = [self doDayOfWeekArray];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEEE";
    NSDate *date = [NSDate date];
    NSString *dayString = [dateFormatter stringFromDate:date];
    NSInteger weekdayNum = [[dateFormatter weekdaySymbols] indexOfObject:dayString];
    backgroundInterger = weekdayNum;
    indexTableInterger = 0;
    
    //get weekday array
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:[[UserData getChangLanguage]lowercaseString]]];
    weekdays = [df shortWeekdaySymbols];
    NSLog(@"%@", weekdays);
    
    [self doCallTeacherDayAPI:[UserData getUserID] date:[dayArray objectAtIndex:weekdayNum] schoolid:[UserData getSchoolId]];
    self.headerTimeTableLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE",nil,[Utils getLanguage],nil);
    self.showHaveNotSubject.text = NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_NO_SUBJECT",nil,[Utils getLanguage],nil);
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}
//call API
- (void)doCallTeacherDayAPI:(long long)userId date:(NSDate*)date schoolid:(long long)schoolid{
    [self showIndicator];
    if(self.callTeacherTimeDayAPI != nil) {
        self.callTeacherTimeDayAPI = nil;
    }
    self.callTeacherTimeDayAPI = [[CallTeacherTimeDayAPI alloc] init];
    self.callTeacherTimeDayAPI.delegate = self;
    [self.callTeacherTimeDayAPI call:userId date:date schoolid:schoolid] ;
}

- (void)callTeacherTimeDayAPI:(CallTeacherTimeDayAPI *)classObj data:(NSMutableArray<TeacherTimeTableModel *> *)data success:(BOOL)success{
    [self stopIndicator];
    if (success) {
        subjectArray = data;
    }
    [self.tableView reloadData];
}

- (void)doCallTeacherTimeTableDetailAPI:(long long)userId subjectId:(long long)subjectId date:(NSDate*)date schoolid:(long long)schoolid{
    [self showIndicator];
    if(self.callTeacherTimeTableDetailAPI != nil) {
        self.callTeacherTimeTableDetailAPI = nil;
    }
    self.callTeacherTimeTableDetailAPI = [[CallTeacherTimeTableDetailAPI alloc] init];
    self.callTeacherTimeTableDetailAPI.delegate = self;
    [self.callTeacherTimeTableDetailAPI call:subjectId userId:userId date:date schoolid:schoolid] ;
}

- (void)callTeacherTimeTableDetailAPI:(CallTeacherTimeTableDetailAPI *)classObj data:(SScheduleDetailModel *)data success:(BOOL)success{
    if (success) {
        [self stopIndicator];
        [keyIndexDict setValue:data forKey:[NSString stringWithFormat:@"%ld",indexTableInterger]];
        [self.tableView reloadData];
    }
}

-(NSArray*)daysInWeek:(int)weekOffset fromDate:(NSDate*)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    //ask for current week
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    comps=[calendar components:NSWeekCalendarUnit|NSYearCalendarUnit fromDate:date];
    //create date on week start
    NSDate* weekstart=[calendar dateFromComponents:comps];
    NSDateComponents* moveWeeks=[[NSDateComponents alloc] init];
    moveWeeks.weekOfYear=weekOffset;
    weekstart=[calendar dateByAddingComponents:moveWeeks toDate:date options:0];
    //add 7 days
    NSMutableArray* week=[NSMutableArray arrayWithCapacity:7];
    for (int i=1; i<=7; i++) {
        NSDateComponents *compsToAdd = [[NSDateComponents alloc] init];
        compsToAdd.day=i;
        NSDate *nextDate = [calendar dateByAddingComponents:compsToAdd toDate:weekstart options:0];
        [week addObject:nextDate];
    }
    return [NSArray arrayWithArray:week];
}

- (NSArray*) doDayOfWeekArray{
    NSDate *dateNow = [NSDate date];
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    [gregorian setFirstWeekday:1];
    NSUInteger adjustedWeekdayOrdinal = [gregorian ordinalityOfUnit:NSWeekdayCalendarUnit inUnit:NSWeekCalendarUnit forDate:dateNow];
    //first day of week
    NSDate *daysAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-(adjustedWeekdayOrdinal) toDate:[NSDate date] options:0];
    //NSString *daysStr = [Utils getThaiDateFormatWithDate:daysAgo];
    NSArray * daysArray =  [self daysInWeek:0 fromDate:daysAgo];
    return daysArray;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 7;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    TeacherTimeTableSelectDayCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.dayLable.text = [weekdays objectAtIndex:indexPath.row];
        if (backgroundInterger == indexPath.row) {
            cell.backgroundColor.backgroundColor = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0];
        }else{
            cell.backgroundColor.backgroundColor = [UIColor clearColor];
        }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    backgroundInterger = indexPath.row;
    subjectArray = [[NSMutableArray alloc] init];
    keyIndexDict = [[NSMutableDictionary alloc] init];
    [self doCallTeacherDayAPI:[UserData getUserID] date:[dayArray objectAtIndex:backgroundInterger] schoolid:[UserData getSchoolId]];
    [self.collectionView reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize defaultSize = CGSizeMake((self.collectionView.frame.size.width/7)-10, (self.collectionView.frame.size.height));
    return defaultSize;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(subjectArray.count == 0) {
        self.showHaveNotSubject.hidden = NO;
        return 0;
    }
    else {
        self.showHaveNotSubject.hidden = YES;
        return subjectArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

   
    if ([keyIndexDict objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]] != nil) {
        TeacherTimeTableListSubjectDetailTableViewCell *cellDetail = [tableView dequeueReusableCellWithIdentifier:@"CellDetail"];
        SScheduleDetailModel *detailModel = [keyIndexDict valueForKey:[NSString stringWithFormat:@"%ld",indexPath.row]];
        
        cellDetail.subjectHeaderLable.text = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_SUBJECT",nil,[Utils getLanguage],nil)];
        cellDetail.codeHeaderLable.text = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_CODE",nil,[Utils getLanguage],nil)];
        cellDetail.classroomHeaderLable.text = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_ROOM",nil,[Utils getLanguage],nil)];
        cellDetail.instructorHeaderLable.text = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_TEACHERNAME",nil,[Utils getLanguage],nil)];
        cellDetail.timeInOutHeaderLable.text = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_TIMEINOUT",nil,[Utils getLanguage],nil)];
        cellDetail.saveTimeHeaderLable.text = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_SAVETIME",nil,[Utils getLanguage],nil)];
        cellDetail.classRoomHeaderLable.text = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_CLASSROOM",nil,[Utils getLanguage],nil)];
        
        
        
        if (![detailModel.subjectName isEqualToString:@""]) {
            cellDetail.subjectNameLable.text = detailModel.subjectName;
        }else{
            cellDetail.subjectNameLable.text = @"-";
        }
        if (![detailModel.subjectCode isEqualToString:@""]) {
            cellDetail.codeSubjectLable.text = detailModel.subjectCode;
        }else{
            cellDetail.codeSubjectLable.text = @"-";
        }
        if (![detailModel.roomNumber isEqualToString:@""]) {
            cellDetail.classRoomLable.text = detailModel.roomNumber;
        }else{
            cellDetail.classRoomLable.text = @"-";
        }
        if (![detailModel.classRoom isEqualToString:@""]) {
            cellDetail.classLable.text = detailModel.classRoom;
        }else{
            cellDetail.classLable.text = @"-";
        }
        if (![detailModel.lecturer isEqualToString:@""]) {
            cellDetail.nameTeacherLable.text = detailModel.lecturer;
        }else{
            cellDetail.nameTeacherLable.text = @"-";
        }
        if ([detailModel.courseStartTime isEqualToString:@""]&&[detailModel.courseEndTime isEqualToString:@""]) {
            cellDetail.timeInOutLable.text = @"-";
            
        }else{
            cellDetail.timeInOutLable.text = [NSString stringWithFormat:@"%@ %@ %@",detailModel.courseStartTime,NSLocalizedStringFromTableInBundle(@"LABEL_TIMETABLE_TO",nil,[Utils getLanguage],nil),detailModel.courseEndTime];
        }
        if (![detailModel.clockInTime isEqualToString:@""]) {
            cellDetail.saveTimeLable.text = [NSString stringWithFormat:@"%@ / %@",detailModel.clockInTime,[Utils getStatus:detailModel.clockInStatus]];
        }else{
            cellDetail.saveTimeLable.text = [NSString stringWithFormat:@" - / %@",[Utils getStatus:detailModel.clockInStatus]];
        }
        [cellDetail.section1 layoutIfNeeded];
        [cellDetail.section1.layer setShadowColor:[UIColor blackColor].CGColor];
        [cellDetail.section1.layer setShadowOpacity:0.3];
        [cellDetail.section1.layer setShadowRadius:3.0];
        [cellDetail.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        cellDetail.selectionStyle = UITableViewCellSelectionStyleNone;
        return cellDetail;
    }else{
        TeacherTimeTableListSubjectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
                
        TeacherTimeTableModel *model = [subjectArray objectAtIndex:indexPath.row];
        if (![model.subjectName isEqualToString:@""]) {
            cell.subjectLable.text = model.subjectName;
        }else{
            cell.subjectLable.text = @"-";
        }
        if (![model.startTime isEqualToString:@""]) {
            cell.startTimeLable.text = model.startTime;
        }else{
            cell.startTimeLable.text = @"-";
        }
        if (![model.endTime isEqualToString:@""]) {
            cell.endTimeLable.text = model.endTime;
        }else{
            cell.endTimeLable.text = @"-";
        }
        if (![model.codeSubject isEqualToString:@""]) {
            cell.codeSubjectLable.text = model.codeSubject;
        }else{
            cell.codeSubjectLable.text = @"-";
        }
        [cell.section1 layoutIfNeeded];
        [cell.section1.layer setShadowColor:[UIColor blackColor].CGColor];
        [cell.section1.layer setShadowOpacity:0.3];
        [cell.section1.layer setShadowRadius:3.0];
        [cell.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
 
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    indexTableInterger = indexPath.row;
    if ([keyIndexDict objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]] != nil) {
        [keyIndexDict removeObjectForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
        [self.tableView reloadData];
    }
    else {
        if (subjectArray.count > 0) {
            TeacherTimeTableModel *model = [subjectArray objectAtIndex:indexPath.row];
            [self doCallTeacherTimeTableDetailAPI:[UserData getUserID] subjectId:model.subjectID date:[dayArray objectAtIndex:backgroundInterger] schoolid:[UserData getSchoolId]];
        }
      
    }
    
}

#pragma mark - Private functions
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        _tableView.userInteractionEnabled = NO;
        _collectionView.userInteractionEnabled = NO;
        [self.indicator startAnimating];
    }
    
}
- (void)stopIndicator {
    _tableView.userInteractionEnabled = YES;
    _collectionView.userInteractionEnabled = YES;
    [self.indicator stopAnimating];
}

- (IBAction)openDrawer:(id)sender {
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
}
@end
