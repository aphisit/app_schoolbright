//
//  PurchasingListMenuModel.h
//  JabjaiApp
//
//  Created by toffee on 6/6/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PurchasingListMenuModel : NSObject
@property (strong ,nonatomic) NSString* productName;
@property (assign ,nonatomic) long long productId;
@property (assign ,nonatomic) NSInteger productPrice;
@property (assign ,nonatomic) NSInteger productAmount;
@end

NS_ASSUME_NONNULL_END
