//
//  ReportHomeWorkListViewController.m
//  JabjaiApp
//
//  Created by mac on 1/4/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportHomeWorkListViewController.h"
#import "ReportHomeWorkTableViewCell.h"
#import "ReportHomeWorkListModel.h"
#import "Utils.h"
#import "APIURL.h"
#import "UserData.h"
#import "Constant.h"


@interface ReportHomeWorkListViewController (){
    
    NSMutableArray<ReportHomeWorkListModel *> *homeworkArray;
    NSInteger connectCounter;
    
    UIColor *grayColor;
    
}

@property (strong, nonatomic) ReportHomeWorkCalendarViewController *reportHomeWorkCalendarViewController;
@property (strong, nonatomic) ARSPContainerController *panelControllerContainer;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;


@property (nonatomic) NSDate *considerDate;

@end

@implementation ReportHomeWorkListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.panelControllerContainer = (ARSPContainerController *)self.parentViewController;
    self.reportHomeWorkCalendarViewController = (ReportHomeWorkCalendarViewController *)self.panelControllerContainer.mainViewController;
    self.reportHomeWorkCalendarViewController.delegate = self;
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = grayColor;
    
    connectCounter = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ReportHomeWorkTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    self.considerDate = [NSDate date];
    self.dateTitle.text = [Utils getThaiDateFormatWithDate: self.considerDate];
    
    //getAPI ------
    [self getReportHomeWorkListDataWithDate: self.considerDate];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(homeworkArray == nil) {
        return 0;
    }
    else {
        return homeworkArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ReportHomeWorkListModel *model = [homeworkArray objectAtIndex:indexPath.row];
    
    ReportHomeWorkTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.labelSubjectName.text = model.subjectName;
    cell.labelDetail.text = model.detail;
    cell.labelTimeSend.text = [self.timeFormatter stringFromDate:model.sendTime];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onSelectMessage:)]) {
        
        ReportHomeWorkListModel *model = [homeworkArray objectAtIndex:indexPath.row];
        [self.delegate onSelectMessage:model.messageID];

    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

#pragma mark - TimeTableViewControllerDelegate

- (void)onSelectDate:(NSDate *)date {
    
    self.considerDate = date;
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:date];
    [self getReportHomeWorkListDataWithDate:date];
    
}

#pragma mark - Get API Data
- (void)getReportHomeWorkListDataWithDate:(NSDate *)date{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getHomeworkCalendarListWithUserID:userID date:date schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getReportHomeWorkListDataWithDate:date];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportHomeWorkListDataWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReportHomeWorkListDataWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if (homeworkArray != nil) {
                    [homeworkArray removeAllObjects];
                    homeworkArray = nil;
                }
                
                homeworkArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long messageID = [[dataDict objectForKey:@"nMessageID"]longLongValue];
                    long long homeworkID = [[dataDict objectForKey:@"homework_id"]longLongValue];
                    
                    NSMutableString *subjectName, *detailHomework;
                    
                    if (![[dataDict objectForKey:@"sTitle"] isKindOfClass:[NSNull class]]) {
                        subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sTitle"]];
                    }
                    else{
                        subjectName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"sMessage"] isKindOfClass:[NSNull class]]) {
                        detailHomework = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sMessage"]];
                    }
                    
                    else{
                        detailHomework = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    
                    NSMutableString *sendHomework = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dSend"]];
                    
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) detailHomework);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) sendHomework);
                    
                    NSRange dotRange = [sendHomework rangeOfString:@"."];
                    NSDate *messageDate;
                    
                    if (dotRange.length != 0) {
                        messageDate = [formatter dateFromString:sendHomework];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:sendHomework];
                    }
                    
                    
                    ReportHomeWorkListModel *model = [[ReportHomeWorkListModel alloc] init];
                    
                    model.messageID = messageID;
                    model.homeworkID = homeworkID;
                    model.subjectName = subjectName;
                    model.detail = detailHomework;
                    model.sendTime = messageDate;
                    
                    [homeworkArray addObject:model];
                    
                }
                
                [self.tableView reloadData];
            }
        }
        
    }];
    
    
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}


@end
