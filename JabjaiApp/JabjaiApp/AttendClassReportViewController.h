//
//  AttendClassReportViewController.h
//  JabjaiApp
//
//  Created by mac on 1/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CAPSPageMenu.h"
#import "CalendarDialog.h"
#import "Attend2SchoolReportFilterDialog.h"
#import "UTeachingYearModel.h"

@interface AttendClassReportViewController : UIViewController <CAPSPageMenuDelegate, Attend2SchoolReportFilterDialogDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (nonatomic) int sectionID;
@property (strong, nonatomic) NSString *subjectName;
@property (strong, nonatomic) NSMutableArray<UTeachingYearModel *> *teachingYearArray;

- (IBAction)moveBack:(id)sender;
- (IBAction)showFilterDialog:(id)sender;

@end
