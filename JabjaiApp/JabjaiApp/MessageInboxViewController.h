//
//  MessageInboxViewController.h
//  JabjaiApp
//
//  Created by mac on 11/23/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageInboxDialog.h"
#import "MessageInboxLongTextDialog.h"
#import "MessageInBoxScrollableTextDialog.h"
#import <CoreData/CoreData.h>

typedef enum {
    ATTENDANCE, PURCHASING, TOPUP, ABSENCEREQUEST, NEWS, HOMEWORK
} INBOXTYPE;

@interface MessageInboxViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MessageInboxDialogDelegate, MessageInboxLongTextDialogDelegate, MessageInBoxScrollableTextDialogDelegate>

@property (nonatomic) INBOXTYPE inboxType;
@property (nonatomic) int message_id;

@property (weak, nonatomic) IBOutlet UILabel *titleBarLabel;
@property (weak, nonatomic) IBOutlet UITableView *tblExpandable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)openDrawer:(id)sender;

@end
