//
//  ARShowPermissionDialog.h
//  JabjaiApp
//
//  Created by toffee on 25/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ARShowPermissionDialog : UIViewController

@property (weak, nonatomic) IBOutlet UIView *dialogView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message;

@end

NS_ASSUME_NONNULL_END
