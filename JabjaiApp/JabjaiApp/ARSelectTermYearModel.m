//
//  ARSelectTermYearModel.m
//  JabjaiApp
//
//  Created by toffee on 21/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "ARSelectTermYearModel.h"

@implementation ARSelectTermYearModel

@synthesize yearNumber = _yearNumber;
@synthesize yearId = _yearId;
-(void)setYearNumber:(NSInteger)yearNumber{
    _yearNumber = yearNumber;
}
-(void)setYearId:(NSInteger)yearId{
    _yearId = yearId;
}

-(NSInteger)getYearNumber{
    return _yearNumber;
}
-(NSInteger)getYearId{
    return _yearId;
}

@end
