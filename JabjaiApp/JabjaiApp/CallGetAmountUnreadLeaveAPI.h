//
//  CallGetAmountUnreadLeaveAPI.h
//  JabjaiApp
//
//  Created by Mac on 25/6/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CallGetAmountUnreadLeaveAPIDelegate <NSObject>

-(void)callGetAmountUnreadLeaveAPI:(NSString *)amount;

@end
@interface CallGetAmountUnreadLeaveAPI : NSObject
@property (nonatomic, weak) id<CallGetAmountUnreadLeaveAPIDelegate>delegate;

-(void)call:(long long)userID schoolID:(long long)schoolID;

@end

NS_ASSUME_NONNULL_END
