//
//  CallRMRefillMonyAPI.m
//  JabjaiApp
//
//  Created by Mac on 20/5/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import "CallRMRefillMonyAPI.h"
#import "Utils.h"
#import "APIURL.h"
#import <AFNetworking/AFNetworking.h>

@implementation CallRMRefillMonyAPI

- (void)call:(NSString *)jsonString{
    [self getQRCodePayment:jsonString];
}

- (void)getQRCodePayment:(NSString *)jsonString{
    
    NSString *urlString = [APIURL getQRCodePaymentOfKbank];
    NSError* error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableLeaves error:&error];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
    [manager POST:urlString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
//        NSLog(@"responseObject = %@", responseObject);
//        NSString *statusMonny = [responseObject objectForKey:@"status"];
//        NSDictionary *dic = [responseObject objectForKey:@"data"];
        
//        if(self.delegate && [self.delegate respondsToSelector:@selector(callUpdateMargetPOSTAPI:response:chackMonny:success:)]) {
//            NSString *response = responseObject;
//            [self.delegate callUpdateMargetPOSTAPI:self response:dic chackMonny:statusMonny success:YES];
//        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(callUpdateMargetPOSTAPI:response:chackMonny:success:)]) {
             //[self.delegate callUpdateMargetPOSTAPI:self response:nil chackMonny:nil success:NO];
        }
        
    }];

    
}

@end
