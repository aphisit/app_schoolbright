//
//  EXReportClassStatusModel.m
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "EXReportClassStatusModel.h"

@implementation EXReportClassStatusModel

@synthesize iid = _iid;
@synthesize title = _title;
@synthesize numberOfOnTimeStatus = _numberOfOnTimeStatus;
@synthesize numberOfLateStatus = _numberOfLateStatus;
@synthesize numberOfAbsenceStatus = _numberOfAbsenceStatus;
@synthesize numberOfSickLeaveStatus = _numberOfSickLeaveStatus;
@synthesize numberOfPersonalStatus = _numberOfPersonalStatus;
@synthesize numberOfEventStatus = _numberOfEventStatus;
@synthesize numberOfUndefinedStatus = _numberOfUndefinedStatus;

- (void)setIId:(long long)iid {
    _iid = iid;
}

- (void)setTitle:(NSString *)title {
    _title = title;
}

- (void)setNumberOfOnTimeStatus:(NSInteger)numberOfOnTimeStatus {
    _numberOfOnTimeStatus = numberOfOnTimeStatus;
}

- (void)setNumberOfLateStatus:(NSInteger)numberOfLateStatus {
    _numberOfLateStatus = numberOfLateStatus;
}

- (void)setNumberOfAbsenceStatus:(NSInteger)numberOfAbsenceStatus {
    _numberOfAbsenceStatus = numberOfAbsenceStatus;
}

- (void) setNumberOfEventStatus:(NSInteger)numberOfEventStatus{
    _numberOfEventStatus = numberOfEventStatus;
}

- (void) setNumberOfSickLeaveStatus:(NSInteger)numberOfSickLeaveStatus{
    _numberOfSickLeaveStatus = numberOfSickLeaveStatus;
}

- (void) setNumberOfPersonalStatus:(NSInteger)numberOfPersonalStatus{
    _numberOfPersonalStatus = numberOfPersonalStatus;
}

- (void) setNumberOfUndefinedStatus:(NSInteger)numberOfUndefinedStatus{
    _numberOfUndefinedStatus = numberOfUndefinedStatus;
}

- (long long)getIId {
    return _iid;
}

- (NSString *)getTitle {
    return _title;
}

- (NSInteger)getNumberOfOnTimeStatus {
    return _numberOfOnTimeStatus;
}

- (NSInteger)getNumberOfLateStatus {
    return _numberOfLateStatus;
}

- (NSInteger)getNumberOfAbsenceStatus {
    return _numberOfAbsenceStatus;
}

- (NSInteger)getNumberOfEventStatus{
    return _numberOfEventStatus;
}

- (NSInteger)getNumberOfPersonalStatus{
    return _numberOfPersonalStatus;
}

- (NSInteger)getNumberOfSickLeaveStatus{
    return _numberOfSickLeaveStatus;
}

- (NSInteger)getNumberOfUndefinedStatus{
    return _numberOfUndefinedStatus;
}

@end
