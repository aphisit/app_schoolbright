//
//  RPCheckNameFlagAttendanceModel.m
//  JabjaiApp
//
//  Created by toffee on 15/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "RPCheckNameFlagAttendanceModel.h"

@implementation RPCheckNameFlagAttendanceModel
@synthesize studentId = _studentId;
@synthesize studentName = _studentName;
@synthesize status = _status;
@synthesize studentPic = _studentPic;
@synthesize studentState = _studentState;

- (void)setStudentId:(NSString *)studentId{
    _studentId = studentId;
}

- (void)setStudentName:(NSString *)studentName{
    _studentName = studentName;
}

- (void)setStudentPic:(NSString *)studentPic{
    _studentPic = studentPic;
}

- (void)setStatus:(NSInteger)status{
    _status = status;
}

- (void)setStudentState:(NSInteger)studentState{
    _studentState = studentState;
}

- (NSString *)getStudentId{
    return _studentId;
}

- (NSString *)getStudentName{
    return _studentName;
}

- (NSString *)getStudentPic{
    return _studentPic;
}

- (NSInteger)getStatus{
    return _status;
}

-(NSInteger)getStudentState{
    return _studentState;
}
    

    


@end
