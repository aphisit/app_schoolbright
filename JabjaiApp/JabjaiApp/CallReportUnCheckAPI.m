//
//  CallReportUnCheckAPI.m
//  JabjaiApp
//
//  Created by toffee on 21/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "CallReportUnCheckAPI.h"
#import "APIURL.h"
#import "Utils.h"
@interface CallReportUnCheckAPI () {
    NSInteger connectCounter;
    NSMutableArray *holidayArray;
    //NSMutableArray<RPCheckNameSubjectDateTimeModel *> *dateTimeArray;
}
@end
@implementation CallReportUnCheckAPI

-(void)call:(long long)schoolId date:(NSString *)date{
    connectCounter = 0;
    [self doGetDataUnCheck:schoolId date:date];
}

-(void)doGetDataUnCheck:(long long)schoolId date:(NSString *)date {
    
    NSString *URLString = [APIURL getReportUnCheck:schoolId date:date];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL isFail = NO;
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self doGetDataUnCheck:schoolId date:date];
            }
            else {
                isFail = YES;
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self doGetDataUnCheck:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self doGetDataUnCheck:schoolId date:date];
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
        
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                holidayArray = [[NSMutableArray alloc] init];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *datadict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *holidayStr = [[NSMutableString alloc] initWithFormat:@"%@", [datadict objectForKey:@"DateReport"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)holidayStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *holiday = [formatter dateFromString:holidayStr];
                    
                    ReportUnCheckModel *model = [[ReportUnCheckModel alloc] init];
                    
                    [model setScheduleList:holiday];
                    
                    [holidayArray addObject:model];
                }
                
                
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportUnCheckAPI:dateTimeArray:success:)]) {
                    [self.delegate callReportUnCheckAPI:self dateTimeArray:holidayArray success:YES];
                }
                
            }
            if(isFail) {
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callReportUnCheckAPI:dateTimeArray:success:)]) {
                    [self.delegate callReportUnCheckAPI:self dateTimeArray:holidayArray success:NO];
                }
            }
            
        }
        
    }];
    
    
}


@end
