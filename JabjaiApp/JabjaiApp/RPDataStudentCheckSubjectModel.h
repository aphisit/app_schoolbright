//
//  RPDataStudentCheckSubjectModel.h
//  JabjaiApp
//
//  Created by toffee on 28/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RPDataStudentCheckSubjectModel : NSObject
@property (nonatomic) NSString* studentId;
@property (nonatomic) NSString *studentName;
@property (nonatomic) NSString *studentPic;
@property (nonatomic) NSInteger status;
@property (nonatomic) NSInteger studentState;

- (void)setStudentId:(NSString*)studentId;
- (void)setStudentName:(NSString *)studentName;
- (void)setStudentPic:(NSString *)studentPic;
- (void)setStatus:(NSInteger)status;
- (void)setStudentState:(NSInteger)studentState;

- (NSString *)getStudentId;
- (NSString *)getStudentName;
- (NSString *)getStudentPic;
- (NSInteger)getStatus;
- (NSInteger) getStudentState;
@end

NS_ASSUME_NONNULL_END
