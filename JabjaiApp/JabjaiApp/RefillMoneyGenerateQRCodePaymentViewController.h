//
//  RefillMoneyGenerateQRCodePaymentViewController.h
//  JabjaiApp
//
//  Created by Mac on 20/5/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallRMRefillMonyAPI.h"
NS_ASSUME_NONNULL_BEGIN

@interface RefillMoneyGenerateQRCodePaymentViewController : UIViewController <CallRMRefillMonyAPIDelegate>

@end

NS_ASSUME_NONNULL_END
