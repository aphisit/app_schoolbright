//
//  LoginViewController.m
//  JabjaiApp
//
//  Created by mac on 10/25/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

@import AirshipKit;
#import "LoginViewController.h"
#import "APIURL.h"
#import "UserData.h"
#import "SchoolModel.h"
//#import "SignupDataModel.h"
#import "VerifyCodeDialog.h"
#import "AlertDialog.h"
#import "MultilineAlertDialog.h"
#import "Utils.h"
#import "Constant.h"
#import "MultipleUserModel.h"
#import "UserInfoDBHelper.h"
#import "NavigationViewController.h"
#import "UserInfoViewController.h"

@interface LoginViewController () {
    //SchoolModel *schoolModel;
    SchoolCodeDialogViewController *schoolCodeDialog;
    VerifyCodeDialog *verifyCodeDialog;
    AlertDialog *alertDialog;
    MultilineAlertDialog *multilineAlertDialog;
    NSInteger connectCounter;
    NSString *language;
    LoginListAllSchoolNameDialog  *loginListAllSchoolNameDialog;
    long long schoolID;
    BOOL statusShowKeyboard;
}

@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (nonatomic, strong) CallSTSendTokenAndLanguageAPI *callSTSendTokenAndLanguageAPI;

@end
@implementation LoginViewController
- (void)viewDidLoad {
    [super viewDidLoad];
  
    language = @"th";
    [UserData setChangLanguage:language];
    schoolCodeDialog = [[SchoolCodeDialogViewController alloc] init];
    schoolCodeDialog.delegate = self;
    verifyCodeDialog = [[VerifyCodeDialog alloc] init];
    alertDialog = [[AlertDialog alloc] init];
    multilineAlertDialog = [[MultilineAlertDialog alloc] init];
    self.schoolNameTextField.delegate = self;
    connectCounter = 0;
    statusShowKeyboard = NO;
//    NSString *channelID = [UAirship push].channelID;
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    // Check whether should or shouldn't log out if app already updated new version
    NSString *currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *savedVersion = [UserData getAppVersion];
    if(savedVersion == nil) {
        savedVersion = @"0";
    }
    NSInteger compareVersionResult = [Utils compareVersion:savedVersion version2:currentVersion];
    // Check version as if updated
    if(logoutIfNewVersion && compareVersionResult < 0) {
        [[UAirship push] removeTags:[[UAirship push] tags]];
        [[UAirship push] updateRegistration];
        [UserData setUserLogin:NO];
        [UserData saveMasterUserID:0];
        [UserData saveUserID:0];
        [UserData setUserType:USERTYPENULL];
        [UserData setAcademyType:ACADEMYTYPENULL];
        [UserData setFirstName:@""];
        [UserData setLastName:@""];
        [UserData setUserImage:@""];
        [UserData setGender:MALE];
        [UserData setAppVersion:currentVersion];
    }
    [self setLanguage];
}

    
-(void)setLanguage{
    NSBundle *myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    _usernameTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LOGIN_ID_STUDEN",nil,myLangBundle,nil);
    _passwordTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LOGIN_BIRTHDAY",nil,myLangBundle,nil);
    _schoolNameTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LOGIN_SELECT_SCHOOL",nil,myLangBundle,nil);
    [_loginBtn setTitle:NSLocalizedStringFromTableInBundle(@"BUTTON_LOGIN",nil,myLangBundle,nil) forState:UIControlStateNormal];
    self.headerSchoolNameLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LOGIN_SCHOOL",nil,myLangBundle,nil);
    self.headerUsernameLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LOGIN_USERNAME",nil,myLangBundle,nil);
    self.headerPasswordLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LOGIN_PASSWORD",nil,myLangBundle,nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    //[self registerForKeyboardNotifications];
    if([UserData isUserLogin]) {
        // Confirm the masterid is not null
        if([UserData getMasterUserID] == -1) {
            NSInteger userId = [UserData getUserID];
            [UserData saveMasterUserID:userId];
        }
        [UIView setAnimationsEnabled:NO];
        self.view.hidden = YES;
        CATransition *transition = [[CATransition alloc] init];
        transition.duration = 0.275;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
        self.rootViewControler = [[UINavigationController alloc] initWithRootViewController:viewController];
        [self.slideMenuController changeMainViewController:viewController close:YES];
        [self.slideMenuController addLeftGestures];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView setAnimationsEnabled:YES];
            self.view.hidden = NO;
        });
    }
    CAGradientLayer *gradientLogin;
    [self.loginBtn layoutIfNeeded];
    gradientLogin = [Utils getGradientColorHeader];
    gradientLogin.frame = self.loginBtn.bounds;
    [self.loginBtn.layer insertSublayer:gradientLogin atIndex:0];
    [self.loginBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.loginBtn.layer setShadowOpacity:0.3];
    [self.loginBtn.layer setShadowRadius:3.0];
    [self.loginBtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    [self.slideMenuController removeLeftGestures];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}
- (void)viewWillDisappear:(BOOL)animated {
    [self deregisterFromKeyboardNotifications];
    [super viewWillDisappear:animated];
}

#pragma mark - CallSTSendTokenAndLanguageAPI
- (void) doSendTokenAndLanguage:(NSString*)token language:(NSString*)language{
    if ( self.callSTSendTokenAndLanguageAPI != nil) {
        self.callSTSendTokenAndLanguageAPI = nil;
    }
    self.callSTSendTokenAndLanguageAPI = [[CallSTSendTokenAndLanguageAPI alloc] init];
    self.callSTSendTokenAndLanguageAPI.delegate = self;
    [self.callSTSendTokenAndLanguageAPI call:token language:language];
}
- (void)callSTSendTokenAndLanguageAPI:(CallSTSendTokenAndLanguageAPI *)classObj statusCode:(NSInteger)statusCode success:(BOOL)success{
    NSLog(@"%d",statusCode);
}

#pragma KeyboardDelegate
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
            selector:@selector(keyboardWasShown:)
            name:UIKeyboardDidShowNotification object:nil];
   [[NSNotificationCenter defaultCenter] addObserver:self
             selector:@selector(keyboardWillBeHidden:)
             name:UIKeyboardWillHideNotification object:nil];
}

- (void)deregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    if (statusShowKeyboard == NO) {
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        CGPoint buttonOrigin = self.loginBtn.frame.origin;
        CGFloat buttonHeight = self.loginBtn.frame.size.height;
        CGRect visibleRect = self.view.frame;
        visibleRect.size.height -= keyboardSize.height;
        
        if (buttonOrigin.y < visibleRect.size.height) {// fix size iphone screen some version
            //visibleRect.size.height -= visibleRect.size.height - buttonOrigin.y;
            buttonOrigin.y += visibleRect.size.height - buttonOrigin.y;
        }
        
        NSLog(@"%d ",CGRectContainsPoint(visibleRect, buttonOrigin));
        if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
             CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y+50 - visibleRect.size.height + buttonHeight);
            [self.scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
    
    
}
 
- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - Navigation

- (IBAction)forgotPasswordAction:(id)sender {
    NSString *alertTitle = @"แจ้งเตือน";
    NSString *alertMessage = @"ขออภัย ฟังก์ชันนี้ยังไม่เปิดใช้งาน";
    [self showAlertDialogWithTitle:alertTitle message:alertMessage];
}

- (IBAction)signupAction:(id)sender {
    [self showSchoolCodeDialog];
}

- (IBAction)loginAction:(id)sender {
    
    NSString *username = self.usernameTextField.text;
    NSString *password = self.passwordTextField.text;
    NSString *schoolName = self.schoolNameTextField.text;
    
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    
    if(username.length == 0) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = @"กรุณาระบุชื่อผู้ใช้";
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else if(password.length == 0) {
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = @"กรุณาระบุรหัสผ่าน";
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else if (schoolName.length == 0){
        NSString *alertTitle = @"แจ้งเตือน";
        NSString *alertMessage = @"กรุณาเลือกโรงเรียน";
        [self showAlertDialogWithTitle:alertTitle message:alertMessage];
    }
    else {
        self.loginBtn.userInteractionEnabled = NO;
        NSString *URLString = [APIURL getLoginURL:username password:password schoolId:schoolID];
        NSURL *url = [NSURL URLWithString:URLString];
        [self sendLoginToServer:url username:username password:password];
    }
}

- (void)sendLoginToServer:(NSURL *)url username:(NSString*)username password:(NSString*)password {
    
    // Show the indicator
    [self showIndicator];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        // When receive data from server.Then stop indicator
        [self stopIndicator];
        self.loginBtn.userInteractionEnabled = YES;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self sendLoginToServer:url username:username password:password];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self sendLoginToServer:url username:username password:password];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSDictionary class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self sendLoginToServer:url username:username password:password];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSDictionary *returnedDict = returnedData;
                int academyType,userType,gender;
                long long userID, schoolId;
                connectCounter = 0;
                
                 
                
                if(![[returnedData objectForKey:@"ID"] isKindOfClass:[NSNull class]]) {
                    userID = [[returnedDict objectForKey:@"ID"] longLongValue];
                }else{
                    userID = 0;
                }
                
                if(![[returnedData objectForKey:@"Value"] isKindOfClass:[NSNull class]]) {
                    academyType = [[returnedDict objectForKey:@"Value"] intValue];
                }else{
                    academyType = 0;
                }
                
                if(![[returnedData objectForKey:@"Type"] isKindOfClass:[NSNull class]]) {
                    userType = [[returnedDict objectForKey:@"Type"] intValue];
                }else{
                    userType = 0;
                }
                
                if(![[returnedData objectForKey:@"sex"] isKindOfClass:[NSNull class]]) {
                    gender = [[returnedDict objectForKey:@"sex"] intValue];
                }else{
                    gender = 0;
                }
               
                if(![[returnedData objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
                    schoolId = [[returnedDict objectForKey:@"SchoolId"] longLongValue];
                }else{
                    schoolId = 0;
                }
                
                //long long schoolId = [[returnedData objectForKey:@"SchoolId"] longLongValue];
            
                
                NSString *firstName, *lastName, *imageUrl,*token;
                
                if(![[returnedData objectForKey:@"image"] isKindOfClass:[NSNull class]]) {
                    imageUrl = [returnedData objectForKey:@"image"];
                }
                else {
                    imageUrl = @"";
                }
                
                if(![[returnedData objectForKey:@"name"] isKindOfClass:[NSNull class]]) {
                    firstName = [returnedData objectForKey:@"name"];
                }
                else {
                    firstName = @"";
                }
                
                if(![[returnedData objectForKey:@"lastname"] isKindOfClass:[NSNull class]]) {
                    lastName = [returnedData objectForKey:@"lastname"];
                }
                else {
                    lastName = @"";
                }
                if(![[returnedData objectForKey:@"token"] isKindOfClass:[NSNull class]]) {
                    token = [returnedData objectForKey:@"token"];
                }
                else {
                    token = @"";
                }

                if(userID > 0) {
                    
                    [UserData saveMasterUserID:userID];
                    [UserData saveUserID:userID];
                    [UserData setUserLogin:YES];
                    [UserData saveSchoolId:schoolId];
                    [UserData setFirstName:firstName];
                    [UserData setLastName:lastName];
                    [UserData setUserImage:imageUrl];
                    [UserData setClientToken:token];
                    [UserData setUsername:username];
                    [UserData setPassword:password];
                    
                   
                    if(academyType == 1) {
                        [UserData setAcademyType:SCHOOL];
                    }
                    else {
                        [UserData setAcademyType:UNIVERSITY];
                    }
                    
                    if(userType == 0) {
                        [UserData setUserType:STUDENT];
                    }
                    else {
                        [UserData setUserType:TEACHER];
                    }
                    
                    if(gender == 0) {
                        [UserData setGender:MALE];
                    }
                    else {
                        [UserData setGender:FEMALE];
                    }
                    
                    // Update user into db
                    MultipleUserModel *userModel = [[MultipleUserModel alloc] init];
                    userModel.masterId = userID;
                    userModel.slaveId = userID;
                    userModel.schoolId = schoolId;
                    userModel.userType = userType;
                    userModel.academyType = academyType;
                    userModel.firstName = firstName;
                    userModel.lastName = lastName;
                    userModel.imageUrl = imageUrl;
                    userModel.gender = gender;
                    userModel.clientToken = token;
                    userModel.username = username;
                    userModel.password = password;
                    
                   // [self.dbHelper inserColumn];
                    [self.dbHelper  insertData:userModel];
                    
                    NSArray<MultipleUserModel *> *userArray = [self.dbHelper getUserDataWithMasterId:[UserData getMasterUserID]];
                    NSMutableArray<NSString *> *tags = [[NSMutableArray alloc] init];
                    for(MultipleUserModel *model in userArray) {
                        NSString *tag = [NSString stringWithFormat:@"%lld", [model getSlaveId]];
                        [tags addObject:tag];
                    }
                    [[UAirship push] addTags:tags];
                    [[UAirship push] updateRegistration];
                    CATransition *transition = [[CATransition alloc] init];
                    transition.duration = 0.275;
                    transition.type = kCATransitionPush;
                    transition.subtype = kCATransitionFromRight;
                    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                    [self.view.window.layer addAnimation:transition forKey:kCATransition];
                    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
                    self.rootViewControler = [[UINavigationController alloc] initWithRootViewController:viewController];
                    [self.slideMenuController changeMainViewController:viewController close:YES];
                    [self.slideMenuController addLeftGestures];
                }
                else if(userID < 0) {
                    NSString *title = @"ท่านยังไม่ได้ยืนยันลายนิ้วมือ";
                    [self showVerifyCodeDialogWithTitle:title verifyCode:[NSString stringWithFormat:@"%lld", (userID * -1)]];
                }
                else {
                    NSString *alertTitle = @"แจ้งเตือน";
                    NSString *message1 = @"Username / Password ไม่ถูกต้อง";
                    NSString *message2 = @"*สำหรับนักเรียนให้ใช้เลขบัตรนักเรียน";
                    NSString *message3 = @"*สำหรับอาจารย์ให้ใช้เบอร์โทรศัพท์";
                    NSString *textMessage = [NSString stringWithFormat:@"%@\n\n%@\n%@", message1, message2, message3];
                    [self showMultilineAlertDialogWithTitle:alertTitle message:textMessage];
                }
                NSLog(@"Login Result : %lld", userID);
            }
        }
    }];
}

- (IBAction)DismissKeyboard:(id)sender {
    [self resignFirstResponder];
}
    
- (IBAction)changLanguage:(id)sender {
    if([language isEqualToString:@"th"]){
        language = @"en";
        [UserData setChangLanguage:language];
        [_languageBtn setTitle:@"EN" forState:UIControlStateNormal];
       
    }else{
        language = @"th";
        [UserData setChangLanguage:language];
        [_languageBtn setTitle:@"TH" forState:UIControlStateNormal];
    }
    [self doSendTokenAndLanguage:tokenStr language:language];
     [ self setLanguage ];
}
    
#pragma SchoolCodeDialogViewController

-(void)onCancelButtonPress {
    
}

-(void)onOkButtonPress {
    NSString *schoolCode = schoolCodeDialog.passwordTextField.text;
    
    if([schoolCode length] == 0) {
        schoolCodeDialog.hintLabel.textColor = [UIColor redColor];
    }
    else {
        [self getSchoolInfo:schoolCode];
    }
    
    NSLog(@"Enter School Code : %@", schoolCode);
}

#pragma mark - GetDataFromAPI

- (void)getSchoolInfo:(NSString *)schoolCode {
    
    // Show the indicator
    [self showIndicator];
    
    NSString *URLString = [APIURL getSchoolDataWithPasswordURL:schoolCode];
    
    NSLog(@"getSchoolInfo URL : %@", URLString);
    
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSchoolInfo:schoolCode];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolInfo:schoolCode];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchoolInfo:schoolCode];
                }
                else {
                    connectCounter = 0;
                }
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                if([returnedArray count] == 0) {
                    schoolCodeDialog.hintLabel.text = @"รหัสไม่ถูกต้อง";
                    schoolCodeDialog.hintLabel.textColor = [UIColor redColor];
                }
            }
        }

    }];
    
}

- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showMultilineAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(multilineAlertDialog != nil && [multilineAlertDialog isDialogShowing]) {
        [multilineAlertDialog dismissDialog];
    }
    [multilineAlertDialog showDialogInView:self.view title:title message:message];
}

#pragma mark - Login
-(void)showlistAllSchoolNameDialog{
    if(loginListAllSchoolNameDialog != nil && [loginListAllSchoolNameDialog isDialogShowing]) {
        [loginListAllSchoolNameDialog dismissDialog];
    }
    loginListAllSchoolNameDialog = [[LoginListAllSchoolNameDialog alloc] init];
    loginListAllSchoolNameDialog.delegate = self;
    [loginListAllSchoolNameDialog showDialogInView:self.view];
}
-(void)onAlertDialogClose:(LoginListAllSchoolNameModel *)listSchoolNameModel{
    statusShowKeyboard = NO;
    self.schoolNameTextField.text = [listSchoolNameModel getSchoolName];
    schoolID = [listSchoolNameModel getSchoolID];
}
- (void)touchesBeganClose{
    statusShowKeyboard = NO;
}

- (void)showVerifyCodeDialogWithTitle:(NSString *)title verifyCode:(NSString *)verifyCode {
    if(verifyCodeDialog != nil && [verifyCodeDialog isDialogShowing]) {
        [verifyCodeDialog dismissDialog];
    }
    [verifyCodeDialog showDialogInView:self.view title:title verifyCode:verifyCode];
}

- (void)showSchoolCodeDialog {
    if(schoolCodeDialog != nil && [schoolCodeDialog isDialogShowing]) {
        [schoolCodeDialog dismissDialog];
    }
    [schoolCodeDialog showDialogInView:self.view];
}

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    statusShowKeyboard = YES;
    if(textField.tag == 2) {
        [self showlistAllSchoolNameDialog];
    }
    return NO;
}


@end


