//
//  TESummaryViewController.m
//  JabjaiApp
//
//  Created by toffee on 11/14/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "TESummaryViewController.h"
#import "TASummaryHeaderTableViewCell.h"
#import "TASummaryChildTableViewCell.h"
#import "TAStudentStatusModel.h"
#import "TEClassRoomIndividualViewController.h"
#import "TELevelClassViewController.h"
#import "Utils.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "AlertDialogConfirm.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TESummaryViewController (){
     NSArray *sectionKeys;
     NSMutableArray *expandedSections;
     NSInteger totalStudents;
     BOOL updateStatusSuccess;
    CallTEUpdateStudentStatusPOSTAPI *callTEUpdateStudentStatusPOSTAPI;
    NSMutableDictionary<NSString *, NSMutableArray<TAStudentStatusModel *> *> *studentInTableSection;
    // The dialog
    AlertDialogConfirm *alertDialog;
    AlertDialog *alertDialogNoSecess;
    CAGradientLayer *gradient;
    NSBundle  *myLangBundle;
}

@end
static NSString *headerCellIdentifier = @"HeaderCell";
static NSString *childCellIdentifier = @"ChildCell";

static NSString *SECTION_ONTIME = @"SECTION_ONTIME";
static NSString *SECTION_LATE = @"SECTION_LATE";
static NSString *SECTION_ABSENCE = @"SECTION_ABSENCE";
static NSString *SECTION_PERSONAL_LEAVE = @"SECTION_PERSONAL_LEAVE";
static NSString *SECTION_SICK_LEAVE = @"SECTION_SICK_LEAVE";
static NSString *SECTION_EVENT_LEAVE = @"SECTION_EVENT_LEAVE";
static NSString *SECTION_UNDEFINED = @"SECTION_UNDEFINED";
@implementation TESummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TASummaryHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:headerCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TASummaryChildTableViewCell class]) bundle:nil] forCellReuseIdentifier:childCellIdentifier];
    
    // Initial table expandable variables
    sectionKeys = @[SECTION_ONTIME, SECTION_LATE, SECTION_ABSENCE, SECTION_PERSONAL_LEAVE,SECTION_SICK_LEAVE,SECTION_EVENT_LEAVE,SECTION_UNDEFINED];
    expandedSections = [[NSMutableArray alloc] init];
    studentInTableSection = [[NSMutableDictionary alloc] init];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_ONTIME];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_LATE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_ABSENCE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_PERSONAL_LEAVE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_SICK_LEAVE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_EVENT_LEAVE];
    [studentInTableSection setObject:[NSMutableArray new] forKey:SECTION_UNDEFINED];
    // Do any additional setup after loading the view.
    updateStatusSuccess = NO;
    if(self.studentStatusArray != nil) {
        totalStudents = self.studentStatusArray.count;
    }
    else {
        totalStudents = 0;
    }
    [self performStudentStatusData];
    [self setLanguage];
    [self.tableView reloadData];
}
    
- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    //set color button next
    [self.nextBtn layoutIfNeeded];
    gradient = [Utils getGradientColorNextAtion];
    gradient.frame = self.nextBtn.bounds;
    [self.nextBtn.layer insertSublayer:gradient atIndex:0];
    
    [self.nextBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.nextBtn.layer setShadowOpacity:0.3];
    [self.nextBtn.layer setShadowRadius:10.0];
    [self.nextBtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLanguage{
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource: [UserData getChangLanguage] ofType:@"lproj"]];
    self.SummaryDetailBTN.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SUMMARY",nil,myLangBundle,nil);
    [_submitBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_FLAG_SUBMIT",nil,myLangBundle,nil) forState:UIControlStateNormal];
    if(self.mode == 1){
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ATTENDANCE",nil,myLangBundle,nil);
    }else if(self.mode == 2){
        self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_EDIT_ATTENDANCE",nil,myLangBundle,nil);
    }
    
}


- (void)performStudentStatusData {
    
    if(self.studentStatusArray != nil) {
        
        for(NSString *key in studentInTableSection.allKeys) {
            [[studentInTableSection objectForKey:key] removeAllObjects];
        }
        
        for(TAStudentStatusModel *model in self.studentStatusArray) {
            
            switch ([model getScanStatus]) {
                case 0:
                    [[studentInTableSection objectForKey:SECTION_ONTIME] addObject:model];
                    break;
                case 1:
                    [[studentInTableSection objectForKey:SECTION_LATE] addObject:model];
                    break;
                case 3:
                    [[studentInTableSection objectForKey:SECTION_ABSENCE] addObject:model];
                    break;
                case 10:
                    [[studentInTableSection objectForKey:SECTION_PERSONAL_LEAVE] addObject:model];
                    break;
                case 11:
                    [[studentInTableSection objectForKey:SECTION_SICK_LEAVE] addObject:model];
                    break;
                case 12:
                    [[studentInTableSection objectForKey:SECTION_EVENT_LEAVE] addObject:model];
                    break;
                default:
                    [[studentInTableSection objectForKey:SECTION_UNDEFINED] addObject:model];
                    break;
            }
        }
    }
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(sectionKeys != nil) {
        return sectionKeys.count;
    }
    else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([expandedSections containsObject:@(section)]) {
        NSString *key = [sectionKeys objectAtIndex:section];
        
        if([studentInTableSection objectForKey:key] != nil) {
            NSArray *students = [studentInTableSection objectForKey:key];
            return students.count + 1;
        }
        else {
            return 1;
        }
    }
    else {
        return 1; // show table header cell
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *key = [sectionKeys objectAtIndex:indexPath.section];
    
    if(indexPath.row == 0) { // For the first row of each section we'll show header
        TASummaryHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier forIndexPath:indexPath];
        
        BOOL isExpanded = [expandedSections containsObject:@(indexPath.section)];
        
        if(isExpanded) {
            cell.arrowImageView.image = [UIImage imageNamed:@"ic_sort_up"];
        }
        else {
            cell.arrowImageView.image = [UIImage imageNamed:@"ic_sort_down"];
        }
        
        NSArray *students = [studentInTableSection objectForKey:key];
        
        NSInteger numberOfStudents = 0;
        
        if(students != nil && students.count > 0) {
            [cell.arrowImageView setAlpha:1.0f];
            numberOfStudents = students.count;
        }
        else {
            [cell.arrowImageView setAlpha:0.0f];
        }
        
        NSString *titleStr;
        
        if([key isEqualToString:SECTION_ONTIME]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ONTIME",nil,myLangBundle,nil);
        }
        else if([key isEqualToString:SECTION_LATE]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_LATE",nil,myLangBundle,nil);
        }
        else if([key isEqualToString:SECTION_ABSENCE]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ABSENCE",nil,myLangBundle,nil);
        }
        else if([key isEqualToString:SECTION_PERSONAL_LEAVE]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_PERSONAL_LEAVE",nil,myLangBundle,nil);
        }
        else if([key isEqualToString:SECTION_SICK_LEAVE]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SICK_LEAVE",nil,myLangBundle,nil);
        }
        else if([key isEqualToString:SECTION_EVENT_LEAVE]) {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_EVENT_LEAVE",nil,myLangBundle,nil);
        }
        else {
            titleStr = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_UNDEFINED",nil,myLangBundle,nil);
        }
        
        cell.titleLabel.text = titleStr;
        cell.numberLabel.text = [NSString stringWithFormat:@"%li/%li", (long)numberOfStudents, (long)totalStudents];
        
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.userInteractionEnabled = YES;
        
        return cell;
        
    }
    else {
        
        TASummaryChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:childCellIdentifier forIndexPath:indexPath];
        
        TAStudentStatusModel *model = [[studentInTableSection objectForKey:key] objectAtIndex:indexPath.row - 1];
        cell.titleLabel.text = [model getStudentName];
        cell.runNumber.text = [NSString stringWithFormat:@"%ld.",indexPath.row];
        
   
        if ([model getStudentPic] == NULL || [[model getStudentPic] isEqual:@""]) {
            cell.userImageView.image = [UIImage imageNamed:@"ic_user_info"];
        }
        else{
            
            [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:[model getStudentPic]]];
        }
        
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height /2;
        cell.userImageView.layer.masksToBounds = YES;
        cell.userImageView.layer.borderWidth = 0;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0) {
        return 60;
    }
    else {
        return 44;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([expandedSections containsObject:@(indexPath.section)]) {
        
        if(indexPath.row == 0) { // If select header of each section, then toggle up and down
            [expandedSections removeObject:@(indexPath.section)];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    else {
        [expandedSections addObject:@(indexPath.section)];
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - API Caller

- (void)updateStudentStatusWithJSON:(NSString *)jsonString {
    
    if(callTEUpdateStudentStatusPOSTAPI != nil) {
        callTEUpdateStudentStatusPOSTAPI = nil;
    }
    
    [self showIndicator];
    
    callTEUpdateStudentStatusPOSTAPI = [[CallTEUpdateStudentStatusPOSTAPI alloc] init];
    callTEUpdateStudentStatusPOSTAPI.delegate = self;
    [callTEUpdateStudentStatusPOSTAPI call:[UserData getSchoolId]  teacherId:[UserData getUserID] jsonString:jsonString];
    
}

- (void)callTEUpdateStudentStatusPOSTAPI:(CallTEUpdateStudentStatusPOSTAPI *)classObj response:(NSString *)response success:(BOOL)success {
    
    [self stopIndicator];
    updateStatusSuccess = success;
    
    if(success) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_COMPLETED",nil,myLangBundle,nil);
        [self showAlertDialogWithMessage:alertMessage];
    }
    else {
        NSString *alertMessage = @"ทำรายการไม่สำเร็จ\nโปรดลองใหม่";
        [self showAlertDialogNoSecess:alertMessage];
    }
}
#pragma mark - Dialog

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

- (void)showAlertDialogNoSecess:(NSString *)message {
    
    if(alertDialogNoSecess != nil && [alertDialogNoSecess isDialogShowing]) {
        [alertDialogNoSecess dismissDialog];
    }
    else {
        alertDialogNoSecess = [[AlertDialog alloc] init];
        alertDialogNoSecess.delegate = self;
    }
    
    [alertDialogNoSecess showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

#pragma mark - AlertDialogDelegate
- (void)onAlertDialogClose {
    
   
    
    TELevelClassViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"takeeventAttendanceStoryboard"];
    viewController.mode = _mode;
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
  
    
}

- (IBAction)moveBack:(id)sender {
    TEClassRoomIndividualViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TEClassRoomIndividualStoryboard"];
    viewController.studentStatusArray = _studentStatusArray;
    viewController.mode = _mode;
     [self.slideMenuController changeMainViewController:viewController close:YES];
    
//     [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (IBAction)actionSubmit:(id)sender {
    _submitBtn.enabled = NO;
    NSString *jsonString = [Utils getStudentStatusJSON:_studentStatusArray];
    NSLog(@"%@", jsonString);
    [self updateStudentStatusWithJSON:jsonString];
}
@end
