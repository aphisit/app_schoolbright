//
//  ReportInOutAllDataSelectLevelModel.h
//  JabjaiApp
//
//  Created by toffee on 14/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutAllDataSelectLevelModel : NSObject
@property (nonatomic) NSString *levelName;
@property (nonatomic) long long levelId;
@property (nonatomic) NSInteger amountStudent;
@property (nonatomic) NSDictionary *status;

- (void)setLevelName:(NSString *)levelName;
- (void)setLevelId:(long long )levelId;
- (void)setAmountStudent:(NSInteger )amountStudent;
- (void)setStatus:(NSDictionary *)status;

-(NSString*)getLevelName;
-(long long)getLevelId;
-(NSInteger)getAmountStudent;
-(NSDictionary*)getStatus;

@end

NS_ASSUME_NONNULL_END
