//
//  CRClosedForRenovationDialog.m
//  JabjaiApp
//
//  Created by Mac on 10/2/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import "CRClosedForRenovationDialog.h"
#import "Utils.h"
@interface CRClosedForRenovationDialog (){
    BOOL isShowing;
}
@end

@implementation CRClosedForRenovationDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    isShowing = NO;
    // Do any additional setup after loading the view from its nib.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradient, *backgroundGradient;
    //set color header
//    [self.closeBtn layoutIfNeeded];
//    gradient = [Utils getGradientColorNextAtion];
//    gradient.frame = self.closeBtn.bounds;
//    [self.closeBtn.layer insertSublayer:gradient atIndex:0];
    
    [self.dialogView layoutIfNeeded];
    backgroundGradient = [Utils getGradientColorHeader];
    backgroundGradient.frame = self.dialogView.bounds;
    [self.dialogView.layer insertSublayer:backgroundGradient atIndex:0];
    
}

#pragma mark - Dialog Functions
-(void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message {
    
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSInteger versionOS = [ver integerValue];
    
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    if (versionOS > 10) {
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
        [targetView addSubview:self.view];
    }else{
        [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y+20, targetView.frame.size.width, targetView.frame.size.height-20)];
        [targetView addSubview:self.view];
    }
        
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    [self.messageLabel setAlpha:0.0];
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [self.messageLabel setAlpha:1.0];
    [UIView commitAnimations];
    
    
    [self.closeBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_BTN_FLAG_CLOSE",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    [self.messageLabel setText:message];
    isShowing = YES;
}

- (IBAction)closeAppAction:(id)sender {
    UIApplication *app = [UIApplication sharedApplication];
    [app performSelector:@selector(suspend)];

    //wait 2 seconds while app is going background
    [NSThread sleepForTimeInterval:2.0];

    //exit app when app is in background
    exit(0);
    //[self dismissDialog];
}


@end
