//
//  RefillMoneyGenerateQRCodePaymentViewController.m
//  JabjaiApp
//
//  Created by Mac on 20/5/2564 BE.
//  Copyright © 2564 jabjai. All rights reserved.
//

#import "RefillMoneyGenerateQRCodePaymentViewController.h"

@interface RefillMoneyGenerateQRCodePaymentViewController ()
@property (nonatomic, strong)CallRMRefillMonyAPI *callRMRefillMonyAPI;
@end

@implementation RefillMoneyGenerateQRCodePaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)doCallGenerateQRCode:(NSString *)json{
    if (self.callRMRefillMonyAPI != nil) {
        self.callRMRefillMonyAPI = nil;
    }
    self.callRMRefillMonyAPI = [[CallRMRefillMonyAPI alloc] init];
    self.callRMRefillMonyAPI.delegate = self;
    [self.callRMRefillMonyAPI call:json];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
