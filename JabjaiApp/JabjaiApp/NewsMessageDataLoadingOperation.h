//
//  NewsMessageDataLoadingOperation.h
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^DownloadCompletion)(NSUInteger page, NSIndexSet *indexes, NSArray *data);

@interface NewsMessageDataLoadingOperation : NSBlockOperation

- (instancetype)initWithPage:(NSUInteger)page indexes:(NSIndexSet *)indexes;
- (void)downloadSuccess:(void (^)(NSUInteger page, NSIndexSet *indexes, NSArray *data))finishBlock;

@property (nonatomic, copy) DownloadCompletion downloadCompletion;
@property (nonatomic, readonly) NSUInteger page;
@property (nonatomic, readonly) NSIndexSet *indexes;
@property (nonatomic, readonly) NSArray *dataPage;

@end
