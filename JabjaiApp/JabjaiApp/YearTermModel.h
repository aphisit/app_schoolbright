//
//  YearTermModel.h
//  MonthTermDialog
//
//  Created by mac on 11/5/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YearTermModel : NSObject

@property (nonatomic, strong) NSNumber *yearID;
@property (nonatomic, strong) NSNumber *yearNumber;
@property (nonatomic, strong) NSString *termID;
@property (nonatomic, strong) NSString *termName;
@property (nonatomic, strong) NSDate *termBegin;
@property (nonatomic, strong) NSDate *termEnd;

@end
