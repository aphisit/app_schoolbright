//
//  CallReportPurchasingWeekAPI.h
//  JabjaiApp
//
//  Created by toffee on 10/18/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CallReportPurchasingWeekAPI;

@protocol CallReportPurchasingWeekAPIDelegate <NSObject>

@optional

- (void)callReportPurchasingWeekAPI:(CallReportPurchasingWeekAPI *)classObj dayArray:(NSArray *)dayArray totalMoneyArray:(NSArray *)totalMoneyArray success:(BOOL)success;
@end
@interface CallReportPurchasingWeekAPI : NSObject
@property (nonatomic, weak) id<CallReportPurchasingWeekAPIDelegate> delegate;

- (void)call:(long long)schoolId dayStart:(NSString*)dateStart dayEnd:(NSString*)dayEnd;

@end
