//
//  ReportInOutAllDataSelectLevelModel.m
//  JabjaiApp
//
//  Created by toffee on 14/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "ReportInOutAllDataSelectLevelModel.h"

@implementation ReportInOutAllDataSelectLevelModel

@synthesize levelName = _levelName;
@synthesize levelId = _levelId;
@synthesize amountStudent = _amountStudent;
@synthesize status = _status;

- (void) setLevelId:(long long)levelId{
    _levelId = levelId;
}

- (void) setLevelName:(NSString *)levelName{
    _levelName = levelName;
}

- (void) setAmountStudent:(NSInteger)amountStudent{
    _amountStudent = amountStudent;
}

- (void) setStatus:(NSDictionary *)status{
    _status = status;
}

- (NSString*)getLevelName{
    return _levelName;
}

- (long long)getLevelId{
    return _levelId;
}

- (NSInteger)getAmountStudent{
    return _amountStudent;
}

- (NSDictionary*)getStatus{
    return _status;
}

@end
