//
//  ASCloseFuturePickupDialog.m
//  JabjaiApp
//
//  Created by Mac on 26/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import "ASCloseFuturePickupDialog.h"
#import "Utils.h"
#import "UserData.h"
@interface ASCloseFuturePickupDialog (){
    NSBundle *myLangBundle;
}
@property (nonatomic, assign) BOOL isShowing;
@end

@implementation ASCloseFuturePickupDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Decorate view
   
    self.dialogView.layer.masksToBounds = YES;
    self.cancelBtn.layer.masksToBounds = YES;
    
    // Initialize variables
    self.isShowing = NO;
    self.messageLabel.lineBreakMode  = NSLineBreakByWordWrapping;
    self.messageLabel.numberOfLines = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradient;
    //set color header
    [self.cancelBtn layoutIfNeeded];
    gradient = [Utils getGradientColorStatus:@"red"];
    gradient.frame = self.cancelBtn.bounds;
    [self.cancelBtn.layer insertSublayer:gradient atIndex:0];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}



- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

#pragma mark - Dialog Functions
-(void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    [self.messageLabel setAlpha:0.0];
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [self.messageLabel setAlpha:1.0];
    [UIView commitAnimations];
    
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    [_cancelBtn setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_BTN_FLAG_CLOSE",nil,myLangBundle,nil) forState:UIControlStateNormal];
    //[self.titleLabel setText:title];
    [self.messageLabel setText:message];
    
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [self.messageLabel setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertPickupDialogClose)]) {
        [self.delegate onAlertPickupDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
