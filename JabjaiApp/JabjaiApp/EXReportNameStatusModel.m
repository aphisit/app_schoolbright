//
//  EXReportNameStatusModel.m
//  JabjaiApp
//
//  Created by mac on 8/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "EXReportNameStatusModel.h"

@implementation EXReportNameStatusModel

@synthesize name = _name;
@synthesize status = _status;

- (void)setName:(NSString *)name {
    _name = name;
}
- (void)setStatus:(NSInteger)status {
    _status = status;
}

- (NSString *)getName {
    return _name;
}

- (NSInteger)getStatus {
    return _status;
}

@end
