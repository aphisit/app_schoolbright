//
//  StudyViewController.h
//  JabjaiApp
//
//  Created by mac on 10/22/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JabjaiApp-Swift.h>
#import <Charts/Charts.h>
#import "TableListWithHeaderDialog.h"
#import "TableListDialog.h"
#import <MKDropdownMenu/MKDropdownMenu.h>
#import "specifyDateDialog.h"
#import "StudyTableViewCell.h"
@class StudyViewController;
@protocol StudyViewControllerDelegate
-(void) removeMKDropdownMenu:(MKDropdownMenu*)mKDropdownMenu;
@end

//static MKDropdownMenu *mKDropdownMenu ;
@interface StudyViewController : UIViewController <TableListWithHeaderDialogDelegate, TableListDialogDelegate, ChartViewDelegate,MKDropdownMenuDelegate,MKDropdownMenuDataSource, UITextFieldDelegate,specifyDateDialogDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) id<StudyViewControllerDelegate> delegate;
//@property (weak, nonatomic) IBOutlet UIButton *yearlyBtn;
//@property (weak, nonatomic) IBOutlet UIButton *monthlyBtn;
//@property (weak, nonatomic) IBOutlet UIButton *weeklyBtn;
//@property (weak, nonatomic) IBOutlet UILabel *reportAttendanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

@property (weak, nonatomic) IBOutlet PieChartView *pieChart;

@property (weak, nonatomic) IBOutlet MKDropdownMenu *dateTimeDropdown;
//@property (weak, nonatomic) IBOutlet MKDropdownMenu *dropdown;

//@property (weak, nonatomic) IBOutlet UILabel *onTimeLabel;
//@property (weak, nonatomic) IBOutlet UILabel *lateLabel;
//@property (weak, nonatomic) IBOutlet UILabel *absenceLabel;
//@property (weak, nonatomic) IBOutlet UILabel *sickLabel;
//@property (weak, nonatomic) IBOutlet UILabel *personalLabel;
//@property (weak, nonatomic) IBOutlet UILabel *eventLabel;


@property (weak, nonatomic) IBOutlet UITableView *tableView;



//@property (weak, nonatomic) IBOutlet UIStackView *graphLegendView;
//@property (weak, nonatomic) IBOutlet UIView *viewOnTime;
//@property (weak, nonatomic) IBOutlet UIView *viewLate;
//@property (weak, nonatomic) IBOutlet UIView *viewAbsence;
//@property (weak, nonatomic) IBOutlet UIView *viewSick;
//@property (weak, nonatomic) IBOutlet UIView *viewPersonal;
//@property (weak, nonatomic) IBOutlet UIView *viewEvent;


//@property (weak, nonatomic) IBOutlet UIStackView *onTimeStackView;
//@property (weak, nonatomic) IBOutlet UIStackView *lateStackView;
//@property (weak, nonatomic) IBOutlet UIStackView *absenceStackView;
//@property (weak, nonatomic) IBOutlet UIStackView *sickStackView;
//@property (weak, nonatomic) IBOutlet UIStackView *personalStackView;
//@property (weak, nonatomic) IBOutlet UIStackView *eventStackView;

-(MKDropdownMenu*) getMKDropdownMenu;

//- (IBAction)yearlyButtonTap:(id)sender;
//- (IBAction)monthlyButtonTap:(id)sender;
//- (IBAction)weeklyButtonTap:(id)sender;




@end
