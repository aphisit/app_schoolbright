//
//  TECheckLateWithIdStudentViewController.m
//  JabjaiApp
//
//  Created by toffee on 1/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "TECheckLateWithIdStudentViewController.h"
#import "UserData.h"
#import "Utils.h"

@interface TECheckLateWithIdStudentViewController (){
    NSMutableString *idStudentStr ;
    ConfirmScanerLateViewController *dialogConfirmScaner;
    AlertDialogConfirm *alertDialog;
    AlertDialog *alertDialogNoUser;
    NSBundle *myLangBundle;
}
@property (strong, nonatomic) CallTEGetDataStudentScanBarcodeAPI *callTEGetDataStudentScanBarcodeAPI;
@property (strong, nonatomic) CallTEConfirmScanerBarcode *callTEConfirmScanerBarcode;
@end

@implementation TECheckLateWithIdStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    idStudentStr = [NSMutableString string];
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    // Do any additional setup after loading the view.
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    [self setLanguage];
}

- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    self.idStudentTextField.enabled = NO;
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer *gradientHead = [Utils getGradientColorHeader];
    gradientHead.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHead atIndex:0];
    
    //set color button next
    [self.nextBtn layoutIfNeeded];
    CAGradientLayer *gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.nextBtn.bounds;
    [self.nextBtn.layer insertSublayer:gradientNext atIndex:0];
    
    [self.nextBtn.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.nextBtn.layer setShadowOpacity:0.3];
    [self.nextBtn.layer setShadowRadius:10.0];
    [self.nextBtn.layer setShadowOffset:CGSizeMake(0.0, 2.0)];
    
    [self.section1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.section1.layer setShadowOpacity:0.3];
    [self.section1.layer setShadowRadius:3.0];
    [self.section1.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

}

-(void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_MENU_IDUSER",nil,[Utils getLanguage],nil);
    self.headerCheckLateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_MENU_IDUSER",nil,[Utils getLanguage],nil);
    self.idStudentTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_FLAG_SPECIFY",nil,[Utils getLanguage],nil);
    [self.nextBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_FLAG_NEXT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    [self.headerClearBtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_FLAG_CLEAR",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

//show dialog confirmScaner
- (void)showAlertDialogConfirmScaner:(TEDataStudentScanerModel *)data {
    if(dialogConfirmScaner != nil && [dialogConfirmScaner isDialogShowing]) {
        [dialogConfirmScaner dismissDialog];
    }
    else {
        dialogConfirmScaner = [[ConfirmScanerLateViewController alloc] init];
        dialogConfirmScaner.delegate = self;
    }
    [dialogConfirmScaner showDialogInView:self.view data:data statusScan:4]; //Status 1 is Late
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialogNoUser == nil) {
        alertDialogNoUser = [[AlertDialogConfirm alloc] init];
        alertDialogNoUser.delegate = self;
    }
    if(alertDialogNoUser != nil && [alertDialogNoUser isDialogShowing]) {
        [alertDialogNoUser dismissDialog];
    }
    [alertDialogNoUser showDialogInView:self.view title:title message:message];
}

- (void)showAlertDialogWithMessage:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else {
        alertDialog = [[AlertDialog alloc] init];
    }
    [alertDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
}

//call get data student
- (void)doCallTEGetDataStudentScanBarcodeAPI:(long long)schoolId idStudent:(NSString *)idStudent {
    [self showIndicator];
    if(self.callTEGetDataStudentScanBarcodeAPI != nil) {
        self.callTEGetDataStudentScanBarcodeAPI = nil;
    }
    self.callTEGetDataStudentScanBarcodeAPI = [[CallTEGetDataStudentScanBarcodeAPI alloc] init];
    self.callTEGetDataStudentScanBarcodeAPI.delegate = self;
    [self.callTEGetDataStudentScanBarcodeAPI call:schoolId idStudent:idStudent];
}

- (void)callTEGetDataStudentScanBarcodeAPI:(CallTEGetDataStudentScanBarcodeAPI *)classObj data:(TEDataStudentScanerModel *)data resCode:(NSInteger)resCode success:(BOOL)success{
    [self stopIndicator];
    if (success && resCode == 200) {
         [self showAlertDialogConfirmScaner:data];
    }else{
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_USER_NOTFOUND",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
    }
   
    NSLog(@"xxxxx");
}

//call confirm
- (void)doCallComfrimScanerBarcode:(long long)schoolId userId:(long long)userId idCode:(NSString*)idCode{
    [self showIndicator];
    if(self.callTEConfirmScanerBarcode != nil) {
        self.callTEConfirmScanerBarcode = nil;
    }
    self.callTEConfirmScanerBarcode = [[CallTEConfirmScanerBarcode alloc] init];
    self.callTEConfirmScanerBarcode.delegate = self;
    [self.callTEConfirmScanerBarcode call:schoolId userId:userId idCode:idCode];
}
- (void)callTEConfirmScanerBarcode:(CallTEConfirmScanerBarcode *)classObj resCode:(NSInteger)resCode success:(BOOL)success{
    [self stopIndicator];
    if (resCode == 200) {
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_SENDNEWS_COMPLETED",nil,myLangBundle,nil);
        [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
        NSLog(@"sucess...");
    }
    
}

//retrue from dialog confirm
- (void)onAlertDialogClose{
    idStudentStr = [NSMutableString string];
    self.idStudentTextField.text = idStudentStr;
}
//retrue from dialog show info student
- (void)doCallConfirmScaner{
    [self doCallComfrimScanerBarcode:[UserData getSchoolId] userId:[UserData getUserID] idCode:idStudentStr];
}

- (IBAction)numberButton:(UIButton *)sender {
    
    if (sender.tag < 10) {
        [idStudentStr appendFormat:@"%@",[@(sender.tag) stringValue]];
    }else if (sender.tag>10){
        [self onAlertDialogClose];
    }
    else{
        NSUInteger characterCount = [idStudentStr length];
        if (characterCount > 0) {
              [idStudentStr deleteCharactersInRange:NSMakeRange(characterCount-1, 1)];
        }
      
    }
    self.idStudentTextField.text = idStudentStr;
    NSLog(@"xxxx = %@",idStudentStr);
}

// Show the indicator
- (void)showIndicator {
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }    
}
- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (IBAction)moveBack:(id)sender {
    TELevelClassViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"takeeventAttendanceStoryboard"];
    viewController.mode = self.mode;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}
- (IBAction)nextAction:(id)sender {
    
    if (idStudentStr.length > 0) {
         [self doCallTEGetDataStudentScanBarcodeAPI:[UserData getSchoolId] idStudent:idStudentStr];
    }else{
        NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_FLAG_ENTER_ID",nil,[Utils getLanguage],nil);
        [self showAlertDialogWithMessage:alertMessage];
    }
   
}


@end
