//
//  TeachingSubjectDialog.m
//  JabjaiApp
//
//  Created by mac on 12/29/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "TeachingSubjectDialog.h"
#import "Utils.h"

@interface TeachingSubjectDialog () {
    
    NSArray<UTeachingYearModel *> *teachingYearArray;
    
    NSMutableArray<NSNumber *> *yearList;
    NSMutableArray *semesterList;
    
    NSNumber *selected_year;
    NSString *selected_semester;
    
}

@property (nonatomic, assign) BOOL isShowing;
@property (strong, nonatomic) UIColor *orangeColor;

@end

@implementation TeachingSubjectDialog

- (id)initWithUTeachingYearModelArray:(NSArray<UTeachingYearModel *> *)dataArray {
    
    self = [super init];
    
    if(self) {
        teachingYearArray = dataArray;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Decorate view
    self.dialogView.layer.cornerRadius = 10;
    self.dialogView.layer.masksToBounds = YES;
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10,10)].CGPath;
    self.headerView.layer.mask = maskLayer;
    self.headerView.layer.masksToBounds = YES;
    
    self.cancelButton.layer.cornerRadius = 20;
    self.cancelButton.layer.masksToBounds = YES;
    self.okButton.layer.cornerRadius = 20;
    self.okButton.layer.masksToBounds = YES;
    
    // Set up dropdown
    self.schoolYearDropdown.dataSource = self;
    self.schoolYearDropdown.delegate = self;
    self.schoolYearDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.schoolYearDropdown.backgroundDimmingOpacity = 0;
    self.schoolYearDropdown.dropdownShowsTopRowSeparator = YES;
    self.schoolYearDropdown.dropdownCornerRadius = 5.0;
    self.schoolYearDropdown.tintColor = [UIColor blackColor];
    
    self.semesterDropdown.dataSource = self;
    self.semesterDropdown.delegate = self;
    self.semesterDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.semesterDropdown.backgroundDimmingOpacity = 0;
    self.semesterDropdown.dropdownShowsTopRowSeparator = YES;
    self.semesterDropdown.dropdownCornerRadius = 5.0;
    self.semesterDropdown.tintColor = [UIColor blackColor];
    
    // Initialize variables
    self.isShowing = NO;
    self.orangeColor = UIColorWithHexString(@"#F56B20");
    
    yearList = [[NSMutableArray alloc] init];
    semesterList = [[NSMutableArray alloc] init];
    
    [self initializeYearData];
    [self initializeSemesterData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm,*gradientCancel,*gradientHeader;
    //set color header
    [self.okButton layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.okButton.bounds;
    [self.okButton.layer insertSublayer:gradientConfirm atIndex:0];
    
    //set color button next
    [self.cancelButton layoutIfNeeded];
    gradientCancel = [Utils getGradientColorStatus:@"red"];
    gradientCancel.frame = self.cancelButton.bounds;
    [self.cancelButton.layer insertSublayer:gradientCancel atIndex:0];
    
    //set color button next
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader  atIndex:0];
    
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

#pragma mark - Action functions

- (IBAction)onPressCancel:(id)sender {
    [self dismissDialog];
}

- (IBAction)onPressOK:(id)sender {
    
    [self removeDialogFromView];
    
    [self.delegate applyFilter:selected_year semester:selected_semester];
    
}

#pragma mark - Dialog functions

- (void)setUTeachingYearModelArray:(NSArray<UTeachingYearModel *> *) dataArray {
    teachingYearArray = dataArray;
    
    [self initializeYearData];
    [self initializeSemesterData];
}

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    [self doDesignLayout];
    self.titleLabel.text = title;
    self.isShowing = YES;
    
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
    
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onTeachingSubjectDialogClose)]) {
        [self.delegate onTeachingSubjectDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

#pragma mark - MKDropdownMenuDataSource

-(NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu {
    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component {
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        return yearList.count;
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        return semesterList.count;
    }
    
    return 0;
}

#pragma mark - MKDropdownMenuDelegate

- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
    return 35;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {
    
    NSString *textString;
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        textString = [[NSString alloc] initWithFormat:@"%d", [selected_year intValue]];
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        textString = selected_semester;
    }
    else {
        textString = @"";
    }
    
    NSAttributedString *attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"ThaiSansNeue-Regular" size:24], NSForegroundColorAttributeName: self.orangeColor }];
    
    return attributes;
}

-(NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *textString;
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        textString = [[NSString alloc] initWithFormat:@"%d", [[yearList objectAtIndex:row] intValue]];
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        textString = [semesterList objectAtIndex:row];
    }
    else {
        textString = @"";
    }
    
    NSAttributedString *attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"ThaiSansNeue-Regular" size:24], NSForegroundColorAttributeName: self.orangeColor }];
    
    return attributes;
}

- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
    return NO;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        NSNumber *year = [yearList objectAtIndex:row];
        
        if([year intValue] != [selected_year intValue]) {
            selected_year = year;
            [self initializeSemesterData];
            
            [dropdownMenu reloadAllComponents];
            [self.semesterDropdown reloadAllComponents];
        }
        
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        
        NSString *semester = [semesterList objectAtIndex:row];
        
        if(![semester isEqualToString:selected_semester]) {
            selected_semester = semester;
            [dropdownMenu reloadAllComponents];
            
        }
    }
    
    delay(0.15, ^{
        [dropdownMenu closeAllComponentsAnimated:YES];
    });
}


#pragma mark - Manager dialog data

- (void)initializeYearData {
    
    if(teachingYearArray == nil) {
        return ;
    }
    
    if(yearList != nil && yearList.count != 0) {
        [yearList removeAllObjects];
    }
    
    yearList = [[NSMutableArray alloc] init];
    
    for(UTeachingYearModel *model in teachingYearArray) {
        NSNumber *yearNumber = [[NSNumber alloc] initWithInt:[model.yearName intValue]];
        
        if(![yearList containsObject:yearNumber]) {
            [yearList addObject:yearNumber];
        }
    }
    
    if(yearList.count > 0) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
        [yearList sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        // Make first year in array as default
        selected_year = [yearList objectAtIndex:0];
    }
    
}

- (void)initializeSemesterData {
    
    if(teachingYearArray == nil || selected_year == nil) {
        return ;
    }
    
    if(semesterList != nil && semesterList.count != 0) {
        [semesterList removeAllObjects];
    }
    
    NSMutableArray *semestersArray = [[NSMutableArray alloc] init];
    
    for(UTeachingYearModel *teachingYearModel in teachingYearArray) {
        
        int year = [teachingYearModel.yearName intValue];
        
        if(year == [selected_year intValue]) {
            
            NSArray *semesters = teachingYearModel.semesters;
            
            for(USemesterModel *semesterModel in semesters) {
                
                [semestersArray addObject:semesterModel.semesterName];
            }
            
            break;
        }
    }
    
    if(semestersArray.count > 0) {
        semesterList = [[NSMutableArray alloc] initWithCapacity:semestersArray.count + 1];
        [semesterList addObject:@"ทั้งหมด"];
        
        for(int i=0; i < semestersArray.count; i++) {;
            NSString *semesterStr = [[NSString alloc] initWithFormat:@"%@", [semestersArray objectAtIndex:i]];
            [semesterList addObject:semesterStr];
        }
        
        // Make the first order in semesterList as default
        selected_semester = [semesterList objectAtIndex:0];
    }
}

@end
