//
//  MultilineAlertDialog.h
//  JabjaiApp
//
//  Created by mac on 10/28/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MultilineAlertDialogDelegate <NSObject>

@optional
- (void)onMultilineAlertDialogClose;

@end

@interface MultilineAlertDialog : UIViewController

@property (nonatomic, retain) id<MultilineAlertDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *multilineDialogTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *multilineDialogBottomConstraint;

- (IBAction)closeDialog:(id)sender;

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title attribute:(NSAttributedString *)attribute;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
