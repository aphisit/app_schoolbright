//
//  BSPadNumberBehavioViewController.h
//  JabjaiApp
//
//  Created by toffee on 24/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSSelectBehaviorScoreViewController.h"
#import "CallTEConfirmScanerBarcode.h"
#import "CallBSGetDataStudentScanBehaviorAPI.h"
#import "ConfirmScanerLateViewController.h"
#import "BSScanerStudentDataDialog.h"
#import "BSSelectedStudent.h"
NS_ASSUME_NONNULL_BEGIN

@interface BSPadNumberBehavioViewController : UIViewController <CallTEConfirmScanerBarcodeDelegate,CallBSGetDataStudentScanBehaviorAPIDelegate,ConfirmScanerLateViewControllerDelegate, BSScanerStudentDataDialogDelegate>
@property (nonatomic) int status;
@property (assign,nonatomic) int  mode;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;

// BSSelectStudentsViewController
@property (strong, nonatomic) NSArray<BSSelectedStudent *> *selectedStudentArray;
// BSSelectBehaviorScoreViewController
@property (strong, nonatomic) NSArray<BehaviorScoreModel *> *behaviorScoreArray;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UITextField *idStudentTextField;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTypeBehavioLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerClearBtn;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)moveBack:(id)sender;
- (IBAction)nextAction:(id)sender;
- (IBAction)numberButton:(UIButton *)sender;
@end

NS_ASSUME_NONNULL_END
