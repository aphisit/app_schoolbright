//
//  JHIconFileCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 4/7/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol JHIconFileCollectionViewCellDelegate <NSObject>

- (void)removeFile:(NSInteger)index typeCollectionView:(UICollectionView*)typeCollectionView;



@end

@interface JHIconFileCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) id<JHIconFileCollectionViewCellDelegate> delegate;
@property (nonatomic, weak) UICollectionView* typeCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *fileImage;
@property (weak, nonatomic) IBOutlet UIButton *removeFileBtn;
- (IBAction)removeFileAction:(id)sender;
@end

NS_ASSUME_NONNULL_END
