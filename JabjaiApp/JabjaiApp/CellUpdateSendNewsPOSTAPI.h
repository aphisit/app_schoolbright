//
//  CellUpdateSendNewsPOSTAPI.h
//  JabjaiApp
//
//  Created by toffee on 9/15/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "SNLevelStudentViewController.h"
@class CellUpdateSendNewsPOSTAPI;
@protocol CallUpdateSendNewPOSTAPIDelegate <NSObject>

@optional
- (void)callUpdateBehaviorScorePOSTAPI:(CellUpdateSendNewsPOSTAPI *)classObj response:(NSString *)response success:(BOOL)success;
@end
@interface CellUpdateSendNewsPOSTAPI : NSObject
@property (nonatomic, weak) id<CallUpdateSendNewPOSTAPIDelegate> delegate;

- (void)call:(NSString *)jsonString imageArray:(NSArray *)imageArray;


@end
