//
//  SNIconFileCollectionViewCell.h
//  JabjaiApp
//
//  Created by toffee on 27/6/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SNIconFileCollectionViewCellDelegate <NSObject>

- (void) removeIndexFile:(NSInteger)index collectionView:(UICollectionView*)collectionView;

@end

@interface SNIconFileCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) id<SNIconFileCollectionViewCellDelegate> delegate;
@property (weak, nonatomic) UICollectionView *typeCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *fileimage;
@property (weak, nonatomic) IBOutlet UIButton *fileBtn;
- (IBAction)removeFileAction:(id)sender;


@end

NS_ASSUME_NONNULL_END
