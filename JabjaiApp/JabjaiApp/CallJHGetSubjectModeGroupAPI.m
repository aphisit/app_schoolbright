//
//  CallJHGetSubjectModeGroupAPI.m
//  JabjaiApp
//
//  Created by toffee on 12/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CallJHGetSubjectModeGroupAPI.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"

@interface CallJHGetSubjectModeGroupAPI () {
    NSInteger connectCounter;
    NSMutableArray<JHSubjectModel *> *subjectArray;
}
@end
@implementation CallJHGetSubjectModeGroupAPI

- (void)call:(long long)schoolId classLevelId:(long long)classLevelId {
    connectCounter = 0;
    [self getSubjectWithSchoolId:schoolId classLevelId:classLevelId];
}

#pragma mark - Get API Data
- (void)getSubjectWithSchoolId:(long long)schoolId classLevelId:(long long)classLevelId {
    
    NSString *URLString = [APIURL getJHSubjectModeGroupWithSchoolId:schoolId classLevelId:classLevelId];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        BOOL isFail = NO;
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSubjectWithSchoolId:schoolId classLevelId:classLevelId];
            }
            else {
                
                isFail = YES;
                connectCounter = 0;
            }
            
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                     [self getSubjectWithSchoolId:schoolId classLevelId:classLevelId];
                }
                else {
                    
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if([returnedData isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDict = returnedData;
                
                if([dataDict objectForKey:@"Status"] != nil && [[dataDict objectForKey:@"Status"] integerValue] == -1) {
                    
                    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(notArrangeTimeTable:)]) {
                        [self.delegate notArrangeTimeTable:self];
                    }
                }
                else {
                    isFail = YES;
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSubjectWithSchoolId:schoolId classLevelId:classLevelId];
                }
                else {
                    
                    isFail = YES;
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(subjectArray != nil) {
                    [subjectArray removeAllObjects];
                    subjectArray = nil;
                }
                
                subjectArray = [[NSMutableArray alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSString *subjectID = [dataDict objectForKey:@"planeid"] ;
                    
                    NSMutableString *subjectName;
                    
                    if(![[dataDict objectForKey:@"planename"] isKindOfClass:[NSNull class]]) {
                        subjectName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"planename"]];
                    }
                    else {
                        subjectName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) subjectName);
                    
                    
                    JHSubjectModel *model = [[JHSubjectModel alloc] init];
                    model.subjectID = subjectID;
                    model.subjectName = subjectName;
                    
                    
                    [subjectArray addObject:model];
                    
                }
                
                if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callJHGetSubjectModeGroupAPI:data:success:)]) {
                    [self.delegate callJHGetSubjectModeGroupAPI:self data:subjectArray success:YES];
                }
                
                return ;
            }
            
        }
        
        if(isFail) {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(callJHGetSubjectModeGroupAPI:data:success:)]) {
                [self.delegate callJHGetSubjectModeGroupAPI:self data:nil success:NO];
            }
        }
        
    }];
    
}
@end
