//
//  Attend2SchoolReportFilterDialog.m
//  JabjaiApp
//
//  Created by mac on 12/16/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "Attend2SchoolReportFilterDialog.h"
#import "Utils.h"
#import "DateUtility.h"
#import "YearTermModel.h"

@interface Attend2SchoolReportFilterDialog () {
    BOOL selectStartDateTextField;
    
    NSMutableArray<NSNumber *> *yearList;
    NSMutableArray *semesterList;
    NSMutableArray *statusList;
    
    NSDictionary *yearTermDict; //Key is yearNumber and data are array of YearTermModel
    
    NSNumber *selected_year;
    NSString *selected_semester;
    NSString *selected_status;
}

@property (nonatomic, assign) BOOL isShowing;

@property (strong, nonatomic) CalendarDialog *calendarDialog;
@property (strong, nonatomic) NSDate *minDate;
@property (strong, nonatomic) NSDate *maxDate;
@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) UIColor *greenColor;
@property (strong, nonatomic) MKDropdownMenu *dropdownMenu;
@end

@implementation Attend2SchoolReportFilterDialog

- (id)initWithYearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict {
    
    self = [super init];
    
    if(self) {
        yearTermDict = dataDict;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setLanguage];
    self.greenColor = [UIColor colorWithRed: 0.15 green: 0.66 blue: 0.38 alpha: 1.00];
    self.dialogView.layer.cornerRadius = 10.0;
    self.dialogView.layer.masksToBounds = YES;

    // Set up dropdown
    self.schoolYearDropdown.dataSource = self;
    self.schoolYearDropdown.delegate = self;
    self.schoolYearDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.schoolYearDropdown.backgroundDimmingOpacity = 0;
    self.schoolYearDropdown.dropdownShowsTopRowSeparator = YES;
    self.schoolYearDropdown.dropdownCornerRadius = 5.0;
    self.schoolYearDropdown.tintColor = [UIColor blackColor];
//    self.schoolYearDropdown.layer.cornerRadius = 25;
//    self.schoolYearDropdown.layer.masksToBounds = YES;
    
    self.semesterDropdown.dataSource = self;
    self.semesterDropdown.delegate = self;
    self.semesterDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.semesterDropdown.backgroundDimmingOpacity = 0;
    self.semesterDropdown.dropdownShowsTopRowSeparator = YES;
    self.semesterDropdown.dropdownCornerRadius = 5.0;
    self.semesterDropdown.tintColor = [UIColor blackColor];
//    self.semesterDropdown.layer.cornerRadius = 25;
//    self.semesterDropdown.layer.masksToBounds = YES;
    
    self.statusDropdown.dataSource = self;
    self.statusDropdown.delegate = self;
    self.statusDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.statusDropdown.backgroundDimmingOpacity = 0;
    self.statusDropdown.dropdownShowsTopRowSeparator = YES;
    self.statusDropdown.dropdownCornerRadius = 5.0;
    self.statusDropdown.tintColor = [UIColor blackColor];
//    self.statusDropdown.layer.cornerRadius = 25;
//    self.statusDropdown.layer.masksToBounds = YES;
   
    
    
    
   
    
    // Set textfield delegate
    self.startDateTextField.delegate = self;
    self.endDateTextField.delegate = self;
    
    self.startDateTextField.placeholder = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_SELECT_DATE", nil, [Utils getLanguage], nil);
    self.endDateTextField.placeholder = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_SELECT_DATE", nil, [Utils getLanguage], nil);
    
    
    // Initialize variables
    self.isShowing = NO;
    
    self.gregorian = [Utils getGregorianCalendar];
    self.maxDate = [self.gregorian dateByAddingUnit:NSCalendarUnitYear value:+1 toDate:[NSDate date] options:0];//[NSDate date];
    self.minDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:[NSDate date] options:0];
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    self.calendarDialog = [[CalendarDialog alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_SELECT_DATE", nil, [Utils getLanguage], nil) minimumDate:self.minDate maximumDate:self.maxDate];
    self.calendarDialog.delegate = self;
    
    yearList = [[NSMutableArray alloc] init];
    semesterList = [[NSMutableArray alloc] init];
    statusList = [[NSMutableArray alloc] init];
    
    [self initializeYearData];
    [self initializeSemesterData];
    
    // Initialize Status
    [statusList addObject:NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ALL", nil, [Utils getLanguage], nil)];
    [statusList addObject:NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ONTIME", nil, [Utils getLanguage], nil)];
    [statusList addObject:NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_LATE", nil, [Utils getLanguage], nil)];
    [statusList addObject:NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ABSENCE", nil, [Utils getLanguage], nil)];
    [statusList addObject:NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ONLEAVE", nil, [Utils getLanguage], nil)];
    [statusList addObject:NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_HOLIDAY", nil, [Utils getLanguage], nil)];
    
    // Make default
    selected_status = [statusList objectAtIndex:0];
}

- (void) setLanguage{
     self.headerYearLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_WORKING",nil,[Utils getLanguage],nil);
    self.headerTermLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_SEMESTER",nil,[Utils getLanguage],nil);
    self.headerStartDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_BEGIN_DATE", nil, [Utils getLanguage], nil);
    self.headerEndDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_END_DATE", nil, [Utils getLanguage], nil);
    self.headerStatusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_STATUS", nil, [Utils getLanguage], nil);

    [self.cancelButton setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_WORKING_PRI_REP_CANCEL", nil, [Utils getLanguage], nil) forState:UIControlStateNormal];
    [self.okButton setTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_WORKING_PRI_REP_CONFIRM", nil, [Utils getLanguage], nil) forState:UIControlStateNormal];
}

//- (void)viewDidLayoutSubviews{
//    [self.okButton layoutIfNeeded];
//}
- (void)viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm,*gradientCancel;
    //set color header
    [self.okButton layoutIfNeeded];
    gradientConfirm = [[CAGradientLayer alloc] init];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.okButton.bounds;
    [self.okButton.layer insertSublayer:gradientConfirm atIndex:0];
    self.okButton.layer.cornerRadius = (self.schoolYearDropdown.frame.size.height/2);
    self.okButton .layer.masksToBounds = true;
    
    //set color button next
    [self.cancelButton layoutIfNeeded];
    gradientCancel = [Utils getGradientColorStatus:@"red"];
    gradientCancel.frame = self.cancelButton.bounds;
    [self.cancelButton.layer insertSublayer:gradientCancel atIndex:0];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.schoolYearDropdown.layer.borderWidth = 2.0;
        self.semesterDropdown.layer.borderWidth = 2.0;
        self.statusDropdown.layer.borderWidth = 2.0;
        self.startDateTextField.layer.borderWidth = 2.0;
        self.endDateTextField.layer.borderWidth = 2.0;
    }else{
        self.schoolYearDropdown.layer.borderWidth = 3.0;
        self.semesterDropdown.layer.borderWidth = 3.0;
        self.statusDropdown.layer.borderWidth = 3.0;
        self.startDateTextField.layer.borderWidth = 3.0;
        self.endDateTextField.layer.borderWidth = 3.0;
    }
        [self.schoolYearDropdown layoutIfNeeded];
        self.schoolYearDropdown.layer.cornerRadius = (self.schoolYearDropdown.frame.size.height/2);
        self.schoolYearDropdown.layer.borderColor = [UIColor colorWithRed: 0.15 green: 0.66 blue: 0.38 alpha: 1.00].CGColor;
        self.schoolYearDropdown.layer.masksToBounds = YES;
        
        [self.semesterDropdown layoutIfNeeded];
        self.semesterDropdown.layer.cornerRadius = (self.semesterDropdown.frame.size.height/2);
        self.semesterDropdown.layer.borderColor = [UIColor colorWithRed: 0.15 green: 0.66 blue: 0.38 alpha: 1.00].CGColor;
        self.semesterDropdown.layer.masksToBounds = YES;
        
        [self.statusDropdown layoutIfNeeded];
        self.statusDropdown.layer.cornerRadius =  (self.statusDropdown.frame.size.height/2);
        self.statusDropdown.layer.borderColor = [UIColor colorWithRed: 0.15 green: 0.66 blue: 0.38 alpha: 1.00].CGColor;
        self.statusDropdown.layer.masksToBounds = YES;
        
        [self.startDateTextField layoutIfNeeded];
        self.startDateTextField.layer.cornerRadius = (self.startDateTextField.frame.size.height/2);
        self.startDateTextField.layer.borderColor = [UIColor colorWithRed: 0.15 green: 0.66 blue: 0.38 alpha: 1.00].CGColor;
        self.startDateTextField.layer.masksToBounds = YES;
        
        [self.endDateTextField layoutIfNeeded];
        self.endDateTextField.layer.cornerRadius = (self.endDateTextField.frame.size.height/2);
        self.endDateTextField.layer.borderColor = [UIColor colorWithRed: 0.15 green: 0.66 blue: 0.38 alpha: 1.00].CGColor;
        self.endDateTextField.layer.masksToBounds = YES;
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

- (IBAction)onPressCancel:(id)sender {
    [self dismissDialog];
}

- (IBAction)onPressOK:(id)sender {

    NSDate *startDate = nil;
    NSDate *endDate = nil;
    
    if(self.startDateTextField.text.length > 0) {
        startDate = [self.dateFormatter dateFromString:self.startDateTextField.text];
    }
    else {
        startDate = self.minDate;
    }
    
    if(self.endDateTextField.text.length > 0) {
        endDate = [self.dateFormatter dateFromString:self.endDateTextField.text];
    }
    else {
        endDate = self.maxDate;
    }
    
    [self dismissDialog];
    
    [self.delegate applyFilter:selected_year semester:selected_semester status:selected_status startDate:startDate endDate:endDate];
    
}

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title {
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    self.isShowing = YES;
}

- (void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
    
}

- (void)dismissDialog {
   // delay(0.15, ^{
        [self.dropdownMenu closeAllComponentsAnimated:YES];
    //});
    [self removeDialogFromView];
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

- (void)YearTermDictionary:(NSDictionary<NSNumber *, YearTermModel *> *)dataDict {
    yearTermDict = dataDict;
    
    [self initializeYearData];
    [self initializeSemesterData];
}

#pragma mark - MKDropdownMenuDataSource

-(NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu {
    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component {
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        return yearList.count;
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        return semesterList.count;
    }
    else if(dropdownMenu.tag == 2) { // 2 : Status
        return statusList.count;
    }
    
    return 0;
}

#pragma mark - MKDropdownMenuDelegate

- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return 35;
    }else{
        return 70;
    }
   
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {
    
    NSString *textString;
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        textString = [[NSString alloc] initWithFormat:@"%d", [selected_year intValue]];
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        textString = selected_semester;
    }
    else if(dropdownMenu.tag == 2) { // 2 : Status
        textString = selected_status;
    }
    else {
        textString = @"";
    }

    NSAttributedString *attributes;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:26], NSForegroundColorAttributeName: self.greenColor }];
    }else{
        attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:36], NSForegroundColorAttributeName: self.greenColor }];
    }
    
    return attributes;
}

-(NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *textString;
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        textString = [[NSString alloc] initWithFormat:@"%d", [[yearList objectAtIndex:row] intValue]];
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        textString = [semesterList objectAtIndex:row];
    }
    else if(dropdownMenu.tag == 2) { // 2 : Status
        textString = [statusList objectAtIndex:row];
    }
    else {
        textString = @"";
    }
    NSAttributedString *attributes;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:26], NSForegroundColorAttributeName: self.greenColor }];
    }else{
        attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:36], NSForegroundColorAttributeName: self.greenColor }];
    }
    
   
    
    return attributes;
}

- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
    self.dropdownMenu = dropdownMenu;
    return NO;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        NSNumber *year = [yearList objectAtIndex:row];
        if([year intValue] != [selected_year intValue]) {
            selected_year = year;
            [self initializeSemesterData];
            
            [dropdownMenu reloadAllComponents];
            [self.semesterDropdown reloadAllComponents];
            self.startDateTextField.text = @"";
            self.endDateTextField.text = @"";
        }
    }
    else if(dropdownMenu.tag == 1) { // 1 : Semester
        
        NSString *semester = [semesterList objectAtIndex:row];
        
        if(![semester isEqualToString:selected_semester]) {
            selected_semester = semester;
            [self adjustCalendarDateRangeBySemester];
            
            [dropdownMenu reloadAllComponents];
            self.startDateTextField.text = @"";
            self.endDateTextField.text = @"";
        }
        
    }
    else if(dropdownMenu.tag == 2) { // 2 : Status
        
        NSString *status = [statusList objectAtIndex:row];
        
        if(![status isEqualToString:selected_status]) {
            selected_status = status;
            [dropdownMenu reloadAllComponents];
        }
    }
    
    delay(0.15, ^{
        [dropdownMenu closeAllComponentsAnimated:YES];
    });
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField.tag == 0) { // tag 0 : startDateTextField
        
        if([self.calendarDialog isDialogShowing]) {
            [self.calendarDialog dismissDialog];
        }
        
        [self.calendarDialog showDialogInView:self.view];
        
        selectStartDateTextField = YES;
        
        return NO;
    }
    else if(textField.tag == 1) { // tag 1 : endDateTextField
        
        if([self.calendarDialog isDialogShowing]) {
            [self.calendarDialog dismissDialog];
        }
        
        [self.calendarDialog showDialogInView:self.view];
        
        selectStartDateTextField = NO;
        
        return NO;
    }
    
    return YES;
}

#pragma mark - CalendarDialogDelegate
-(void)calendarDialogPressOK:(NSArray<NSDate *> *)selectedDates {
    
    if(selectedDates.count >= 2) {
        NSDate *minDate = [DateUtility minDate:selectedDates];
        NSDate *maxDate = [DateUtility maxDate:selectedDates];
        
        self.startDateTextField.text = [self.dateFormatter stringFromDate:minDate];
        self.endDateTextField.text = [self.dateFormatter stringFromDate:maxDate];
    }
    else if(selectedDates.count > 0) {
        
        NSDate *selectDate = [selectedDates objectAtIndex:0];
        
        if(selectStartDateTextField) {
            self.startDateTextField.text = [self.dateFormatter stringFromDate:selectDate];
            self.endDateTextField.text = @"";
        }
        else {
            self.endDateTextField.text = [self.dateFormatter stringFromDate:selectDate];
            self.startDateTextField.text = @"";
        }
    }
    else {
        self.startDateTextField.text = @"";
        self.endDateTextField.text = @"";
    }
}

-(void)calendarDialogPressCancel {
    NSLog(@"%@", @"Cancel calendar dialog");
}

#pragma mark - Manage dialog data
// extract year from yeartermdict
- (void)initializeYearData {
    if(yearList != nil && yearList.count != 0) {
           [yearList removeAllObjects];
       }
    if(yearTermDict == nil) {
        return;
    }else{
        yearList = [[NSMutableArray alloc] initWithArray:[yearTermDict allKeys]];
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
        [yearList sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        // Make first year in array as default
        selected_year = [yearList objectAtIndex:0];
    }
}

- (void)initializeSemesterData {
    
    if(yearTermDict == nil || selected_year == nil) {
        return;
    }
    
    if(semesterList != nil && semesterList.count != 0) {
        [semesterList removeAllObjects];
    }
    
    NSArray *yearSemesters = [yearTermDict objectForKey:selected_year];
    NSMutableArray *semesters = [[NSMutableArray alloc] init];
    
    NSMutableArray *allDatesInYear = [[NSMutableArray alloc] init];
    
    for(int i=0; i<yearSemesters.count; i++) {
        YearTermModel *model = [yearSemesters objectAtIndex:i];
        [semesters addObject:model.termName];
        [allDatesInYear addObject:model.termBegin];
        [allDatesInYear addObject:model.termEnd];
    }
    
    semesterList = [[NSMutableArray alloc] initWithCapacity:semesters.count + 1];
    [semesterList addObject:NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ALL", nil, [Utils getLanguage], nil)];
    
    for(int i=0; i < semesters.count; i++) {;
        NSString *semesterStr = [[NSString alloc] initWithFormat:@"%@", [semesters objectAtIndex:i]];
        [semesterList addObject:semesterStr];
    }
    
    // Make the first order in semesterList as default
    selected_semester = [semesterList objectAtIndex:0];
    
    self.minDate = [DateUtility minDate:allDatesInYear];
    self.maxDate = [DateUtility maxDate:allDatesInYear];
    
    [self.calendarDialog setDateWithMinimumDate:self.minDate maximumDate:self.maxDate];
}

- (void)adjustCalendarDateRangeBySemester {
    
    if(yearTermDict == nil || selected_year == nil) {
        return;
    }
    
    NSArray *yearSemesters = [yearTermDict objectForKey:selected_year];
    NSMutableArray *allDatesInTerm = [[NSMutableArray alloc] init];
    
    for(int i=0; i<yearSemesters.count; i++) {
        YearTermModel *model = [yearSemesters objectAtIndex:i];
        if([selected_semester isEqualToString:NSLocalizedStringFromTableInBundle(@"LABEL_WORKING_PRI_REP_ALL", nil, [Utils getLanguage], nil)]) {
            [allDatesInTerm addObject:model.termBegin];
            [allDatesInTerm addObject:model.termEnd];
        }
        else if([selected_semester isEqualToString:model.termName]) {
            [allDatesInTerm addObject:model.termBegin];
            [allDatesInTerm addObject:model.termEnd];
            break;
        }
    }
    
    self.minDate = [DateUtility minDate:allDatesInTerm];
    self.maxDate = [DateUtility maxDate:allDatesInTerm];
    
    [self.calendarDialog setDateWithMinimumDate:self.minDate maximumDate:self.maxDate];
    
}

@end
