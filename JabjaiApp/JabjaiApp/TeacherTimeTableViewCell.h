//
//  TeacherTimeTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 11/21/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeacherTimeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelSubject;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeStart;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeEnd;

@end
