//
//  SettingViewController.h
//  JabjaiApp
//
//  Created by mac on 12/20/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangeFingerprintAlertDialog.h"
#import "ChangeFingerprintConfirmDialog.h"
#import "ChangLanguageDialog.h"
#import "SlideMenuController.h"
#import "ReportUnCheckNameViewController.h"
#import "PhotoAlbumLibary.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
#import "CallSTSendTokenAndLanguageAPI.h"
extern NSString *tokenStr;

@interface SettingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, ChangeFingerprintConfirmDialogDelegate, SlideMenuControllerDelegate, ChangLanguageDialogDelegate, CallCRGetStatusClosedForRenovationAPIDelegate, CallSTSendTokenAndLanguageAPIDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

- (IBAction)openDrawer:(id)sender;

@end
