//
//  BSReportForStudentMainViewController.m
//  JabjaiApp
//
//  Created by mac on 7/27/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "BSReportForStudentMainViewController.h"
#import "BSReportForStudentDetailViewController.h"
#import "BehaviorDateScoreTableViewCell.h"
#import "UserData.h"
#import "Utils.h"

@interface BSReportForStudentMainViewController () {
    UIColor *greenColor;
    UIColor *redColor;
}

@property (strong, nonatomic) CallGetBSHistoryForStudentReportAPI *callGetBSHistoryForStudentReportAPI;

@end

static NSString *cellIdentifier = @"Cell";

@implementation BSReportForStudentMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    greenColor = [UIColor colorWithRed:12/255.0 green:190/255.0 blue:64/255.0 alpha:1.0];
    redColor = [UIColor colorWithRed:255/255.0 green:57/255.0 blue:56/255.0 alpha:1.0];
    
    // configure table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorInset = UIEdgeInsetsZero;

    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BehaviorDateScoreTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    if(self.bsHistoryArray == nil) {
        [self getBSHistoryData];
    }
    else {
        self.remainingScoreLabel.text = [NSString stringWithFormat:@"%ld", _remainingScore];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.bsHistoryArray == nil) {
        return 0;
    }
    else {
        return self.bsHistoryArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BehaviorDateScoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    BSHistoryModel *model = [self.bsHistoryArray objectAtIndex:indexPath.row];
    
    if([model getBehaviorType] == 0) { // type add
        cell.scoreLabel.backgroundColor = greenColor;
        cell.scoreLabel.text = [NSString stringWithFormat:@"+%ld", [model getBehaviorScore]];
    } else {
        cell.scoreLabel.backgroundColor = redColor;
        cell.scoreLabel.text = [NSString stringWithFormat:@"-%ld", [model getBehaviorScore]];
    }
    
    cell.dateLabel.text = [Utils getBuddhistEraFormatWithDate:[model getDateTime]];
    cell.behaviorNameLabel.text = [model getBehaviorName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BSHistoryModel *model = [self.bsHistoryArray objectAtIndex:indexPath.row];
    
    [self gotoBSReportForStudentDetailViewController:model];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

#pragma mark - API Caller

- (void)getBSHistoryData {
    
    if(self.callGetBSHistoryForStudentReportAPI != nil) {
        self.callGetBSHistoryForStudentReportAPI = nil;
    }
    
    self.callGetBSHistoryForStudentReportAPI = [[CallGetBSHistoryForStudentReportAPI alloc] init];
    self.callGetBSHistoryForStudentReportAPI.delegate = self;
    [self.callGetBSHistoryForStudentReportAPI call:[UserData getUserID] schoolid:[UserData getSchoolId]];
}

#pragma mark - API Caller delegate

- (void)callGetBSHistoryForStudentReportAPI:(CallGetBSHistoryForStudentReportAPI *)classObj data:(NSArray<BSHistoryModel *> *)data remainingScore:(NSInteger)remainingScore success:(BOOL)success {
    
    if(success) {
        self.remainingScore = remainingScore;
        self.remainingScoreLabel.text = [NSString stringWithFormat:@"%ld", remainingScore];
        
        self.bsHistoryArray = data;
        [self.tableView reloadData];
    }
}

#pragma mark - Utility

- (void)gotoBSReportForStudentDetailViewController:(BSHistoryModel *)model {
    
    BSReportForStudentDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSReportForStudentDetailStoryboard"];
    
    viewController.selectedHistoryModel = model;
    viewController.bsHistoryArray = _bsHistoryArray;
    viewController.remainingScore = _remainingScore;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

#pragma mark - Action functions

- (IBAction)openDrawer:(id)sender {
//    [self.revealViewController revealToggle:self.revealViewController];
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
}
@end
