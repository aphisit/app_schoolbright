//
//  SelectCalendaDayFormerDialog.h
//  JabjaiApp
//
//  Created by toffee on 18/6/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CFSCalendar.h"
NS_ASSUME_NONNULL_BEGIN
@protocol SelectCalendaDayFormerDialogDelegate <NSObject>
@optional
-(void)calendarDialogPressOK:(NSArray<NSDate *> *)selectedDates;
-(void)calendarDialogPressCancel;

@end
@interface SelectCalendaDayFormerDialog : UIViewController
@property (nonatomic, retain) id<SelectCalendaDayFormerDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *dialogView;

@property (weak, nonatomic) IBOutlet CFSCalendar *calendar;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

- (IBAction)onPressCancel:(id)sender;
- (IBAction)onPressOK:(id)sender;

- (id)initWithTitle:(NSString *)title minimumDate:(NSDate *)minimumDate maximumDate:(NSDate *)maximumDate;
- (void)setDateWithMinimumDate:(NSDate *)minDate maximumDate:(NSDate *)maxDate;
- (void)showDialogInView:(UIView *)targetView;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end

NS_ASSUME_NONNULL_END
