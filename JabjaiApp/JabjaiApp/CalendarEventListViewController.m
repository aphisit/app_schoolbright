//
//  CalendarEventListViewController.m
//  JabjaiApp
//
//  Created by mac on 11/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "CalendarEventListViewController.h"
//#import "TopupTableViewCell.h"
#import "CalendarEventTableViewCell.h"
//#import "MessageInboxDataModel.h"
#import "CalendarEventViewController.h"
#import "CalendarEventModel.h"
#import "Constant.h"
#import "APIURL.h"
#import "Utils.h"

@interface CalendarEventListViewController (){
    NSMutableArray<CalendarEventModel *> *eventArray;
    
    NSInteger connectCounter;
    UIColor *grayColor;
}

@property (strong, nonatomic) ARSPContainerController *panelControllerContainer;
@property (strong, nonatomic) CalendarEventViewController *calendarEventViewController;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;

@end

@implementation CalendarEventListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
//    self.panelControllerContainer = (ARSPContainerController *)self.parentViewController;
//    self.calendarEventViewController = (CalendarEventViewController *)self.panelControllerContainer.mainViewController;
//    self.calendarEventViewController.delegate = self;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = grayColor;
    
    connectCounter = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CalendarEventTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    // set up formatter
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:[NSDate date]];
    [self getCalendarEventListWithDate:[NSDate date]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(eventArray == nil) {
        return 0;
    }
    else {
        return eventArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CalendarEventModel *model = [eventArray objectAtIndex:indexPath.row];
    
    CalendarEventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.eventLabel.text = model.eventName;
//    cell.timeLabel.text = [self.timeFormatter stringFromDate:model.date];
//    cell.messageLabel.text = model.message;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

#pragma mark - TopupReportViewControllerDelegate
- (void)onSelectDate:(NSDate *)date {
    self.dateTitle.text = [Utils getThaiDateFormatWithDate:date];
    [self getCalendarEventListWithDate:date];
}

#pragma mark - Get API Data
- (void)getCalendarEventListWithDate:(NSDate *)date {
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getEventCalendarWithUserID:userID date:date schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    [self showIndicator];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [self stopIndicator];
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getCalendarEventListWithDate:date];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getCalendarEventListWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getCalendarEventListWithDate:date];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(eventArray != nil) {
                    [eventArray removeAllObjects];
                    eventArray = nil;
                }
                
                eventArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    
                    NSMutableString *title;
                    
                    if(![[dataDict objectForKey:@"title"] isKindOfClass:[NSNull class]]) {
                        title = [[NSMutableString alloc] initWithString:[dataDict objectForKey:@"title"]];
                    }
                    else {
                        title = [[NSMutableString alloc] initWithString:@""];
                    }

                    CFStringTrimWhitespace((__bridge CFMutableStringRef) title);
                    
                    CalendarEventModel *model = [[CalendarEventModel alloc] init];
                    model.eventName = title;
                    
                    [eventArray addObject:model];
                    
                }
                
                [self.tableView reloadData];
            }
        }
        
    }];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}


@end
