//
//  BSConfirmBehaviorXIB.m
//  JabjaiApp
//
//  Created by toffee on 24/9/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "BSConfirmBehaviorXIB.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Utils.h"
@interface BSConfirmBehaviorXIB ()
@property (nonatomic, assign) BOOL isShowing;
@end

@implementation BSConfirmBehaviorXIB

- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    self.isShowing = NO;
}

- (void)viewDidLayoutSubviews{
    
    [self doDesignLayout];
}

- (void) doDesignLayout{
    
    //set color header
    [self.cancelBnt layoutIfNeeded];
   CAGradientLayer *gradientCancel = [Utils getGradientColorStatus:@"red"];
    gradientCancel.frame = self.cancelBnt.bounds;
    [self.cancelBnt.layer insertSublayer:gradientCancel atIndex:0];
    
    //set color button next
    [self.confirmBnt layoutIfNeeded];
     CAGradientLayer *gradientConfirm = [Utils getGradientColorNextAtion];
    gradientConfirm.frame = self.confirmBnt.bounds;
    [self.confirmBnt.layer insertSublayer:gradientConfirm atIndex:0];
}

- (void)showDialogInView:(UIView *)targetView data:(BSDataStudentScanerModel *)data statusScan:(int)statusScan{

    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    
//    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
//    [f setNumberStyle:NSNumberFormatterDecimalStyle];
//    NSNumber * myNumber = [f numberFromString:data.getStudentID];
//    NSLog(@"longlong: %llu", [myNumber longLongValue]);
    
//    BSSelectedStudent *model = [[BSSelectedStudent alloc] init];
//    model.studentId = data.getStudentID;
    
    if (statusScan == 0) {
        self.headerStatusLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SCANER_ONTIME",nil,[Utils getLanguage],nil);
    }else {
        self.headerStatusLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SCANER_LATE",nil,[Utils getLanguage],nil);
    }
    self.headerNameLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SCANER_NAME",nil,[Utils getLanguage],nil);
    self.headerLevelLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_SCANER_LEVEL",nil,[Utils getLanguage],nil);
    [self.confirmBnt setTitle:NSLocalizedStringFromTableInBundle(@"BTN_FLAG_CONFIRM",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
     [self.cancelBnt setTitle:NSLocalizedStringFromTableInBundle(@"BTN_FLAG_CANCEL",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    
    
    self.studentNameLabel.text = [NSString stringWithFormat:@"%@ %@",[data getStudentFirstName],[data getStudentLastName]];
    self.classRoomLabel.text = [data getStudentClass];
    
    if ([data getStudentPicture] == NULL || [[data getStudentPicture] isEqual:@""]) {
        self.studentPicture.image = [UIImage imageNamed:@"ic_user_info"];
    }
    else{
        [self.studentPicture sd_setImageWithURL:[NSURL URLWithString:[data getStudentPicture]]];
    }
//    self.studentPicture.layer.cornerRadius = self.studentPicture.frame.size.height /2;
    self.studentPicture.layer.masksToBounds = YES;
    self.studentPicture.layer.borderWidth = 0;
    self.isShowing = YES;
}

-(void)removeDialogFromView {
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogScanerClose)]) {
        [self.delegate onAlertDialogScanerClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}


- (IBAction)confirmAction:(id)sender {
    [self removeDialogFromView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(doCallConfirmScaner)]) {
        [self.delegate doCallConfirmScaner];
    }
}

- (IBAction)cancelAction:(id)sender {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onAlertDialogScanerClose)]) {
        [self.delegate onAlertDialogScanerClose];
    }
}

@end
