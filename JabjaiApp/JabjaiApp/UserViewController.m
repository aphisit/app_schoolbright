//
//  UserViewController.m
//  JabjaiApp
//
//  Created by mac on 5/17/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "UserViewController.h"
#import "UserInfoModel.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"
#import "UserData.h"
#import "UserInfoDBHelper.h"
#import "MultipleUserModel.h"
#import "UserInfoViewController.h"
#import "TeacherTimeTableViewController.h"
#import "NavigationViewController.h"
#import "AppDelegate.h"
#import "MGListMargetViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TELevelClassViewController.h"



@interface UserViewController () {
    NSInteger connectCounter;
    NSBundle *myLangBundle;
    CAGradientLayer *gradient;
    NSArray *category;
    BOOL showMenuTakeEvent;
    TableListDialog *tableListDialog; //Dialog Techer Attendance
}

@property (strong, nonatomic) UserInfoModel *userInfoModel;
@property (nonatomic, strong) UserInfoDBHelper *dbHelper;
@property (strong, nonatomic) CardPfileXIBViewController *cardPfileXIBViewController;
@property (strong, nonatomic) UserInfoViewController *userInfoViewController;
@property (strong, nonatomic) CallImageBarCodeAPI *callImageBarCodeAPI;
@property (strong, nonatomic) CallGetTakeEventToChackAPI *callGetTakeEventToChackAPI;
@property (strong, nonatomic) CallChackSignUpPin  *callChackSignUpPin;

@end
static NSString *takeAttendance = @"takeAttendance";
static NSString *takeEvent = @"takeEvent";
@implementation UserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    showMenuTakeEvent = NO;
    
    //Action calendar view
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(singleTapping:)];
    tapGesture1.numberOfTapsRequired = 1;
    [tapGesture1 setDelegate:self];
    [ self.cardContentView addGestureRecognizer:tapGesture1];
    //Action calendar view
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(actionCalandar:)];
    tapGesture2.numberOfTapsRequired = 1;
    [tapGesture2 setDelegate:self];
    [ self.calandarView addGestureRecognizer:tapGesture2];
    self.calandarView.layer.cornerRadius = 8;
    
    //Action Attendance view
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(actionAttendance:)];
    [ self.attendanceView addGestureRecognizer:tapGesture3];
    self.attendanceView.layer.cornerRadius = 8;
    //Action Shoping view
    UITapGestureRecognizer *tapGesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(actionShoping:)];
    //tapGesture4.numberOfTapsRequired = 4;
    [tapGesture4 setDelegate:self];
    [ self.shopingView addGestureRecognizer:tapGesture4];
    self.shopingView.layer.cornerRadius = 8;
    self.scoreView.layer.cornerRadius = 8;
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    
    self.firstNameLabel.text = nil;
    self.lastNameLabel.text = nil;
    self.departmentLabel.text = nil;
    self.positionLabel.text = nil;
    self.scoreLabel.text = @"0 บ.";
    self.teacherBalanceLabel.text = @"0 บ.";
   // [self getImageBarcode:[Utils dateInStringFormat:[NSDate date] format:@"dd-MM-yyyy:HH:mm:ss"]];
   // [_imageCardimg setImage:[UIImage imageNamed:@"ic_idcard"]];
   
    NSString *formatQRcodeStr = [NSString stringWithFormat:@"%@@%ld@%@",[UserData getUsername],(long)[UserData getUserID],[Utils dateInStringFormat:[NSDate date] format:@"dd-MM-yyyy:HH:mm:ss"]];
    CIImage *qrImage = [self createQRForString:formatQRcodeStr];
    float scaleX = self.imageCardimg.frame.size.width / qrImage.extent.size.width;
    float scaleY = self.imageCardimg.frame.size.height / qrImage.extent.size.height;
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    self.imageCardimg.image = [UIImage imageWithCIImage:qrImage
                                                 scale:[UIScreen mainScreen].scale
                                           orientation:UIImageOrientationUp];
    [NSTimer scheduledTimerWithTimeInterval:600.0f
    target:self selector:@selector(generateQRCode:) userInfo:nil repeats:YES];
   
    if([UserData getUserType] != STUDENT) {
        [self.studentFooterView setAlpha:0.0f];
    }
    else {
        [self.studentFooterView setAlpha:1.0f];
    }
   
    self.dbHelper = [[UserInfoDBHelper alloc] init];
    connectCounter = 0;

    [self setLanguage];
    
    //Call API
    [self getUserInfoData];
    [self chackStatusTakeEvent];
}

- (void)generateQRCode:(NSTimer *)timer
{
    NSString *formatQRcodeStr = [NSString stringWithFormat:@"%@@%ld@%@",[UserData getUsername],(long)[UserData getUserID],[Utils dateInStringFormat:timer.fireDate format:@"dd-MM-yyyy:HH:mm:ss"]];
    CIImage *qrImage = [self createQRForString:formatQRcodeStr];
    float scaleX = self.imageCardimg.frame.size.width / qrImage.extent.size.width;
    float scaleY = self.imageCardimg.frame.size.height / qrImage.extent.size.height;
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    self.imageCardimg.image = [UIImage imageWithCIImage:qrImage
                                                 scale:[UIScreen mainScreen].scale
                                           orientation:UIImageOrientationUp];
    //[self getImageBarcode:[Utils dateInStringFormat:timer.fireDate format:@"dd-MM-yyyy:HH:mm:ss"]];
    //[self getImageBarcode:[NSString stringWithFormat:@"%@@%ld@%@",[UserData getUsername],(long)[UserData getUserID],[Utils dateInStringFormat:timer.fireDate format:@"dd-MM-yyyy:HH:mm:ss"]]];
}

- (CIImage *)createQRForString:(NSString *)qrString {
    NSData *stringData = [qrString dataUsingEncoding: NSISOLatin1StringEncoding];

    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];

    return qrFilter.outputImage;
}

-(void)setLanguage{
   
    _firstLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_FIRSTNAME",nil,myLangBundle,nil);
    _lastLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LASTNAME",nil,myLangBundle,nil);
    _balanceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_BALANCE",nil,myLangBundle,nil);
    _departmentHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_DEPARTMENT",nil,myLangBundle,nil);
    _positionHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_POSITION",nil,myLangBundle,nil);
    _teacherScoreHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SCORE",nil,myLangBundle,nil);
    _teacherBalanceHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_BALANCE",nil,myLangBundle,nil);
    
    if([UserData getUserType] != STUDENT) {
        self.departmentHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_DEPARTMENT",nil,myLangBundle,nil);
        self.positionHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_POSITION",nil,myLangBundle,nil);
        self.calandarHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"NAVIGATION_CLASSROOM_TIME_TABLE",nil,myLangBundle,nil);
        self.attendenceHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_CHECKNAME",nil,myLangBundle,nil);
        self.shopingHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SHOPING",nil,myLangBundle,nil);
        
        self.attendenceHeaderImage.image = [UIImage imageNamed:@"ic_info_pencil"];
    }else{
        self.departmentHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_IDSTUDENT",nil,myLangBundle,nil);
        self.positionHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_LEVEL",nil,myLangBundle,nil);
        self.calandarHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"NAVIGATION_TIMETABLE",nil,myLangBundle,nil);
        self.attendenceHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"NAVIGATION_HOMEWORK",nil,myLangBundle,nil);
        self.shopingHeaderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_INFO_SHOPING",nil,myLangBundle,nil);
        
        self.attendenceHeaderImage.image = [UIImage imageNamed:@"ic_info_book"];
    }
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    
    NSLog(@"xxxx = %@",self.view);
    if(self.cardPfileXIBViewController != nil) {
        if([self.cardPfileXIBViewController isDialogShowing]) {
            [self.cardPfileXIBViewController dismissDialog];
        }
    }
    else {
        self.cardPfileXIBViewController = [[CardPfileXIBViewController alloc] init];
        self.cardPfileXIBViewController.delegate = self;
    }
    
    UserViewController *rootController = (UserViewController*)[[(AppDelegate*)
                                                                [[UIApplication sharedApplication]delegate] window] rootViewController];
    
    [ self.cardPfileXIBViewController showDialogInView: rootController.view];
   
}

- (void)onAlertDialogClose{
    
    //[self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+20, self.view.frame.size.width, self.view.frame.size.height)];
}

#pragma mark - GetAPIData

- (void)getUserInfoData {
    [self showIndicator];
    self.disableBackgroundView.hidden = NO ;
    NSString *URLString = [APIURL getUserCredentialURL];
    NSURL *url = [NSURL URLWithString:URLString];
   
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUserInfoData];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserInfoData];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                [self stopIndicator];
                self.disableBackgroundView.hidden = YES ;
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if(returnedArray.count != 0) {
                    
                    self.userInfoModel = [[UserInfoModel alloc] init];
                    
                    NSDictionary *dataDict = [returnedArray objectAtIndex:0];
                    
                    NSLog(@"job_name");
                    NSMutableString *firstName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sName"]];
                    NSMutableString *lastName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sLastname"]];
                    NSInteger sex = [[dataDict objectForKey:@"cSex"] integerValue];
                    NSMutableString *birthdayStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dBirth"]];
                    NSMutableString *phoneNumber = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sPhone"]];
                    NSMutableString *email = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sEmail"]];
                    NSMutableString *department = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"department"]];
                    NSMutableString *position = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"job_name"]];
                    float money = [[dataDict objectForKey:@"nMoney"] floatValue];
                    float creditLimits = [[dataDict objectForKey:@"nMax"] floatValue];
                   // double score = [[dataDict objectForKey:@"Score"] doubleValue];
                    NSNumber *score = [NSNumber numberWithDouble:[[dataDict objectForKey:@"Score"] doubleValue]];
                    NSString *imageUrl;
                    
                    if(![[dataDict objectForKey:@"image"] isKindOfClass:[NSNull class]]) {
                        imageUrl = [dataDict objectForKey:@"image"];
                    }
                    else {
                        imageUrl = @"";
                    }
                    
                    NSMutableString *studentCode, *studentClass, *schoolName;
                    
                    if(![[dataDict objectForKey:@"StudentId"] isKindOfClass:[NSNull class]]) {
                        studentCode = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"StudentId"]];
                    }
                    else {
                        studentCode = [[NSMutableString alloc] init];
                    }
                    
                    if(![[dataDict objectForKey:@"StudentClass"] isKindOfClass:[NSNull class]]) {
                        studentClass = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"StudentClass"]];
                    }
                    else {
                        studentClass = [[NSMutableString alloc] init];
                    }
                    
                    if(![[dataDict objectForKey:@"SchoolName"] isKindOfClass:[NSNull class]]) {
                        schoolName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"SchoolName"]];
                    }
                    else {
                        schoolName = [[NSMutableString alloc] init];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) firstName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) lastName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) birthdayStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) phoneNumber);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) email);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentCode);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) studentClass);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) schoolName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) department);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) position);
                    
                    NSDate *birtdayDate = [Utils parseServerDateStringToDate:birthdayStr];
                    
                    self.userInfoModel.firstName = firstName;
                    self.userInfoModel.lastName = lastName;
                    self.userInfoModel.sex = sex;
                    self.userInfoModel.birthday = birtdayDate;
                    self.userInfoModel.studentCode = studentCode;
                    self.userInfoModel.studentClass = studentClass;
                    self.userInfoModel.schoolName = schoolName;
                    self.userInfoModel.email = email;
                    self.userInfoModel.phoneNumber = phoneNumber;
                    self.userInfoModel.money = money;
                    self.userInfoModel.creditLimits = creditLimits;
                    self.userInfoModel.score = score;
                    self.userInfoModel.imageUrl = imageUrl;
                    self.userInfoModel.department = department;
                    self.userInfoModel.position = position;
                    
                    [self updateUserData];
                    
                    if(imageUrl != nil && imageUrl.length > 0) {
                        BOOL success = [self.dbHelper updateImageUrlWithSlaveId:[UserData getUserID] imageUrl:imageUrl];
                        NSLog(@"%@", [NSString stringWithFormat:@"update user image success : %d", success]);
                    }
                }
                
            }
            
        }
        
    }];
    
}

- (void)updateUserData {
    
    if ([UserData getSchoolImage] == nil) {
       // self.schoolBackgroundConstentView.backgroundColor = [UIColor clearColor];
        UIImage * backgroundColorImage;
        gradient = [Utils getGradientColorStatus:@"yellow"];
        gradient.frame = self.schoolBackgroundImage.bounds;
        UIGraphicsBeginImageContext(gradient.bounds.size);
        [gradient renderInContext:UIGraphicsGetCurrentContext()];
        backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [self.schoolBackgroundImage setBackgroundColor:[UIColor colorWithPatternImage:backgroundColorImage]];
    }else{
        self.schoolBackgroundImage.image = [UIImage imageWithData: [UserData getSchoolImage]];
    }
    
    if(self.userInfoModel != nil) {
        
        if ([self.userInfoModel.firstName isEqualToString:@""]) {
            self.firstNameLabel.text = @"-";
        }else{
             self.firstNameLabel.text = self.userInfoModel.firstName;
        }
        
        if ([self.userInfoModel.lastName isEqualToString:@""]) {
            self.lastNameLabel.text = @"-";
        }else{
            self.lastNameLabel.text = self.userInfoModel.lastName;
        }
        
        CGFloat max = MAX(self.userImageView.frame.size.width, self.userImageView.frame.size.height);
        self.userImageView.layer.cornerRadius = max / 2.0;
        self.userImageView.layer.masksToBounds = YES;
        if(self.userInfoModel.imageUrl == nil || self.userInfoModel.imageUrl.length == 0) {
            
            if(self.userInfoModel.sex == 0) { // M
                self.userImageView.image = [UIImage imageNamed:@"ic_user_default"];
            }
            else { // F
                self.userImageView.image = [UIImage imageNamed:@"ic_user_default"];
            }
            
            self.userImageView.backgroundColor = [UIColor whiteColor];
        }
        else {
            self.userImageView.backgroundColor = [UIColor clearColor];
            [self.userImageView loadImageFromURL:self.userInfoModel.imageUrl];
            
        }
        
        if([UserData getUserType] != STUDENT) {
            
            if ([self.userInfoModel.department isEqualToString:@"" ]) {
                self.departmentLabel.text = @"-";
            }
            else{
                self.departmentLabel.text = self.userInfoModel.department;
                
            }
            
            if ([self.userInfoModel.position isEqualToString:@""]) {
                self.positionLabel.text = @"-";
            }
            else {
                self.positionLabel.text = self.userInfoModel.position;
            }
            if([Utils isIntegerWithDouble:self.userInfoModel.money]) {
                NSString *display = [NSNumberFormatter localizedStringFromNumber:@(self.userInfoModel.money)
                                                                     numberStyle:NSNumberFormatterDecimalStyle];
                self.teacherBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",display,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_B",nil,myLangBundle,nil)];
            }
            else {
                self.teacherBalanceLabel.text = [[NSString alloc] initWithFormat:@"%.2f %@", self.userInfoModel.money,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_B",nil,myLangBundle,nil)];
            }
            
        }
        else {
            
            if ([self.userInfoModel.studentCode isEqualToString:@"" ]) {
                self.departmentLabel.text = @"-";
            }
            else{
                self.departmentLabel.text  = self.userInfoModel.studentCode;
                
            }
            
            if ([self.userInfoModel.studentClass isEqualToString:@""]) {
                self.positionLabel.text = @"-";
            }
            else {
                self.positionLabel.text  = self.userInfoModel.studentClass;
            }
        
            self.scoreLabel.text = [[NSString alloc] initWithFormat:@"%@", self.userInfoModel.score];
            if([Utils isIntegerWithDouble:self.userInfoModel.money]) {
                NSString *display = [NSNumberFormatter localizedStringFromNumber:@(self.userInfoModel.money)
                                                                     numberStyle:NSNumberFormatterDecimalStyle];
                self.studentBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",display,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_B",nil,myLangBundle,nil)];
            }
            else {
                self.studentBalanceLabel.text = [[NSString alloc] initWithFormat:@"%.2f %@", self.userInfoModel.money,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_B",nil,myLangBundle,nil)];
            }

        }
    }
    
}

- (void) actionCalandar:(UIGestureRecognizer *)recognizer{
     NSLog(@"xxxx1");
    //UIViewController *nextView;
    if([UserData getUserType] != STUDENT) {
        
        [self nextPageMenu:@"TeachersTimeTableViewStoryboard"];

     
    }else{
        
       [self nextPageMenu:@"TimeTableStoryboard"];
        
    }
    
}

- (void) actionAttendance:(UIGestureRecognizer *)recognizer{
     NSLog(@"xxxx2");
    if([UserData getUserType] != STUDENT) {
        
       
        
        if (showMenuTakeEvent) {
             category = [NSArray arrayWithObjects:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ATTENDANCE",nil,myLangBundle,nil),NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN",nil,myLangBundle,nil),nil];
            [self showTableListDialogWithRequestCode:takeEvent data:category];
        }else{
            //[self nextPageMenu:@"TakeClassAttendanceStoryboard"];
            [self nextPageAttendance];
        }
        
        
    }else{
        [self nextPageMenu:@"ReportHomeWorkMessageStoryboard"];
    }
}

#pragma mark - CallChackSignUpPin
- (void) actionShoping:(UIGestureRecognizer *)recognizer{ //check pin market
    self.callChackSignUpPin = nil;
    self.callChackSignUpPin = [[ CallChackSignUpPin alloc] init];
    self.callChackSignUpPin.delegate = self;
    [self.callChackSignUpPin call:[UserData getUserID] schoolid:[UserData getSchoolId]];
     NSLog(@"xxxx3");
    
}

-(void)callCheckSignUpPin:(NSString*)status success:(BOOL)success{
    if(success) {
        if ([status isEqualToString:@"Success"]) {
            [self nextPageMenu:@"MGListMargetViewStoryboard"];
        }else{
            [self nextPageMenu:@"MGPasscodeStoryboard"];
        }
    }
}

- (void) nextPageMenu:(NSString *)identityStoryboard{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = (UIViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:identityStoryboard];
    NavigationViewController *leftViewController = (NavigationViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"NavigationStoryboard"];
    
    SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:viewController leftMenuViewController:leftViewController];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[appDelegate window] setRootViewController:slideMenuController];

}

- (void) nextPageAttendance{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TASelectClassLevelViewController *viewController = (TASelectClassLevelViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"TakeClassAttendanceStoryboard"];
    
    viewController.mode = 1;
    NavigationViewController *leftViewController = (NavigationViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"NavigationStoryboard"];
    
    SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:viewController leftMenuViewController:leftViewController];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[appDelegate window] setRootViewController:slideMenuController];
    
}



#pragma mark - Dialog
- (void)showTableListDialogWithRequestCode:(NSString *)requestCode data:(NSArray *)data {
    
    NSLog(@"xxxx = %@",self.view);
    if(tableListDialog != nil && [tableListDialog isDialogShowing]) {
        [tableListDialog dismissDialog];
        tableListDialog = nil;
    }
    
    tableListDialog = [[TableListDialog alloc] initWithRequestCode:requestCode];
    tableListDialog.delegate = self;
    [tableListDialog showDialogInView:self.view dataArr:data];
    UserViewController *rootController = (UserViewController*)[[(AppDelegate*)
                                                                [[UIApplication sharedApplication]delegate] window] rootViewController];
    [tableListDialog showDialogInView: rootController.view dataArr:data];
}

#pragma mark - TableListDialogDelegate
- (void)onItemSelect:(NSString *)requestCode atIndex:(NSInteger)index {

    if ([category[index] isEqualToString:NSLocalizedStringFromTableInBundle(@"LABEL_FLAG_ATTENDANCE",nil,myLangBundle,nil)]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TELevelClassViewController *viewController = (TELevelClassViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"takeeventAttendanceStoryboard"];
        viewController.mode = 1;
        NavigationViewController *leftViewController = (NavigationViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"NavigationStoryboard"];
        SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:viewController leftMenuViewController:leftViewController];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [[appDelegate window] setRootViewController:slideMenuController];
        
       // [self nextPageMenu:@"takeeventAttendanceStoryboard"];
    }else if([category[index] isEqualToString:NSLocalizedStringFromTableInBundle(@"LABEL_TAKE_ATTEN",nil,myLangBundle,nil)]){
        //[self nextPageMenu:@"TakeClassAttendanceStoryboard"];
        [self nextPageAttendance];
    }
   
}
//
////Call To API GetImageBarcode
//- (void)getImageBarcode:(NSString*)formatTime {
//
//    if(self.callImageBarCodeAPI != nil) {
//        self.callImageBarCodeAPI = nil;
//    }
//    self.callImageBarCodeAPI = [[CallImageBarCodeAPI alloc] init];
//    self.callImageBarCodeAPI.delegate = self;
//    [self.callImageBarCodeAPI call:460 width:120 userId:[UserData getUserID] generateCode:@"qrcode" schoolid:[UserData getSchoolId]];
//    //[self.callImageBarCodeAPI callQRCode:460 width:120 formatTime:formatTime schoolid:[UserData getSchoolId]];
//}
//
//- (void)callImageQRCodeAPI:(CallImageBarCodeAPI *)classObj data:(NSString *)data success:(BOOL)success{
//    if (data != nil && success == YES) {
//        self.imageCardimg.image = [self decodeBase64ToImage:data];
//    }
//}
////- (void)callImageBarCodeAPI:(CallImageBarCodeAPI *)classObj data:(NSString *)data generateCode:(NSString *)generateCode success:(BOOL)success{
////    NSLog(@"%@",data);
////    if (data != nil && success == YES && [generateCode isEqualToString:@"qrcode"]) {
////         self.imageCardimg.image = [self decodeBase64ToImage:data];
////    }
////
////   // self.imageCardimg.image = [self decodeBase64ToImage:data];
////}
//
//- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
//    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
//    return [UIImage imageWithData:data];
//}

- (void)chackStatusTakeEvent{
    self.callGetTakeEventToChackAPI = nil;
    self.callGetTakeEventToChackAPI = [[ CallGetTakeEventToChackAPI alloc] init];
    self.callGetTakeEventToChackAPI.delegate = self;
    [self.callGetTakeEventToChackAPI call:[UserData getSchoolId]];
}

#pragma mark - CallGetTAHistoryStudentStatusListAPIDelegate
- (void)callGetTakeEventToChackAPI:(CallGetTakeEventToChackAPI *)classObj data:(BOOL)data success:(BOOL)success {

    NSLog(@"data = %d",data);
    showMenuTakeEvent = data;
}
#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}


@end
