//
//  CallReportInOutAPI.h
//  JabjaiApp
//
//  Created by toffee on 8/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReportInOutAllDataStudentModel.h"

NS_ASSUME_NONNULL_BEGIN

@class CallReportInOutAPI;

@protocol CallReportInOutAPIDelegate <NSObject>
@optional

- (void)callReportInOutAPI:(CallReportInOutAPI *)classObj statusAllArray:(NSMutableArray<ReportInOutAllDataStudentModel *>*)statusAllArray success:(BOOL)success;
@end
@interface CallReportInOutAPI : NSObject

@property (nonatomic, weak) id<CallReportInOutAPIDelegate> delegate;
- (void)call:(long long)schoolId date:(NSString*)date;
@end

NS_ASSUME_NONNULL_END
