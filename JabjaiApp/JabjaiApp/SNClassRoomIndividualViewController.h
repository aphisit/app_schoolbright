//
//  SNClassRoomIndividualViewController.h
//  JabjaiApp
//
//  Created by toffee on 9/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "BSSelectedStudent.h"
#import "BehaviorScoreModel.h"
#import "BSStudentNameRadioTableViewCell.h"
#import "CallGetStudentInClassroomAPI.h"
#import "SWRevealViewController.h"
#import "SNTimeOutViewController.h"
#import <CoreData/CoreData.h>

#import "SlideMenuController.h"

@interface SNClassRoomIndividualViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, BSStudentNameRadioTableViewCellDelegate, CallGetStudentInClassroomAPIDelegate, SlideMenuControllerDelegate>
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger mode;
@property (nonatomic) NSMutableArray* studentImageArray;

//ข้อมูลที่ใช้ส่ง
@property (nonatomic) NSMutableArray* level2id;
@property (nonatomic) NSInteger sendType;
@property (nonatomic) NSInteger sendGroup;
@property (nonatomic) NSInteger newsType;

// BSSelectStudentsViewController
@property (strong, nonatomic) NSArray<BSSelectedStudent *> *selectedStudentArray;

// BSSelectBehaviorScoreViewController
@property (strong, nonatomic) NSArray<BehaviorScoreModel *> *behaviorScoreArray;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSendNewsLabel;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)actionNext:(id)sender;
- (IBAction)moveBack:(id)sender;

@end
