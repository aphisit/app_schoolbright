//
//  ReportSendNewsListViewController.h
//  JabjaiApp
//
//  Created by mac on 4/4/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"
#import "SNPageMenuReportNewsViewControllerXIB.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface ReportSendNewsListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate, SNPageMenuReportNewsViewControllerXIBDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>


@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;

- (IBAction)moveBack:(id)sender;

@end
