//
//  StudyTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 31/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StudyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentLabel;

- (void) updateStatus:(NSInteger)number;

@end

NS_ASSUME_NONNULL_END
