//
//  ReportLeaveViewController.h
//  JabjaiApp
//
//  Created by mac on 4/11/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "NoticeReportDateMessageTableViewCell.h"
#import "CallReportLeaveMessageListAPI.h"
#import "ReportLeaveDetailViewController.h"
#import "ReportLeaveDetailViewControllerXIB.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface ReportLeaveViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,SlideMenuControllerDelegate,CallReportLeaveMessageListAPIDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

@property (weak, nonatomic) IBOutlet UILabel *headerLeaveReportLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *dayView;

@property (weak, nonatomic) IBOutlet UILabel *headerLeaveTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDayLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *dateCount;

- (IBAction)moveBack:(id)sender;


@end
