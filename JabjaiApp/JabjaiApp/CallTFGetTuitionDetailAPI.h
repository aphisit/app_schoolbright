//
//  CallTFGetTuitionDetailAPI.h
//  JabjaiApp
//
//  Created by Mac on 2/10/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TFProductModel.h"
#import "TFInvoicesModel.h"
#import "TFitionFeesModel.h"
NS_ASSUME_NONNULL_BEGIN
@class CallTFGetTuitionDetailAPI;
@protocol CallTFGetTuitionDetailAPIDelegate <NSObject>

- (void)callTFGetTuitionDetailAPI:(CallTFGetTuitionDetailAPI *)classObj data:(TFitionFeesModel*)data success:(BOOL)success;

@end
@interface CallTFGetTuitionDetailAPI : NSObject
@property (nonatomic, weak) id<CallTFGetTuitionDetailAPIDelegate> delegate;
- (void) call:(long long)userID invoiceID:(long long)invoiceID schoolId:(long long)schoolId;
@end

NS_ASSUME_NONNULL_END
