//
//  ASCloseFuturePickupDialog.h
//  JabjaiApp
//
//  Created by Mac on 26/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol ASCloseFuturePickupDialogDelegate <NSObject>

@optional
- (void)onAlertPickupDialogClose;

@end
@interface ASCloseFuturePickupDialog : UIViewController
@property (retain, nonatomic) id<ASCloseFuturePickupDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)closeDialog:(id)sender;
- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message;
- (void)dismissDialog;
- (BOOL)isDialogShowing;
@end

NS_ASSUME_NONNULL_END
