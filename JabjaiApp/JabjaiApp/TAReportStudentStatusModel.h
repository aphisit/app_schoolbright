//
//  TAReportStudentStatusModel.h
//  JabjaiApp
//
//  Created by mac on 8/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAReportStudentStatusModel : NSObject

@property (nonatomic) long long studentId;
@property (nonatomic) NSString *studentName;
@property (nonatomic) NSString *studentPic;
@property (nonatomic) NSInteger status; // 0: On Time, 1:Late, 3: Absence, 4: Personal Leave, 5: Sick Leave, 6:Event Leave, -1: Not Scan

- (void)setStudentId:(long long)studentId;
- (void)setStudentName:(NSString *)studentName;
- (void)setStudentPic:(NSString *)studentPic;
- (void)setStatus:(NSInteger)status;

- (long long)getStudentId;
- (NSString *)getStudentName;
- (NSString *)getStudentPic;
- (NSInteger)getStatus;

@end
