//
//  ReportInOutStatusStudentModel.h
//  JabjaiApp
//
//  Created by toffee on 28/2/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutStatusStudentModel : NSObject

@property (nonatomic) NSString *nameStudent;
@property (nonatomic) long long studentId;
@property (nonatomic) NSString *status;
@property (nonatomic) NSString *pictuer;
@property (nonatomic) NSInteger studentState;

- (void) setNameStudent:(NSString * _Nonnull)nameStudent;
- (void) setStudentId:(long long)studentId;
- (void) setStatus:(NSString * _Nonnull)status;
- (void) setPictuer:(NSString * _Nonnull)pictuer;
- (void)setStudentState:(NSInteger)studentState;

- (NSString*) getNameStudent;
- (long long) getStudentId;
- (NSString*) getStatus;
- (NSString*) getPictuer;
- (NSInteger) getStudentState;
@end

NS_ASSUME_NONNULL_END
