//
//  ReportCheckSubjectCalendarViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportCheckSubjectCalendarViewController.h"

#import "Utils.h"
#import "DateUtility.h"
#import "TAReportStudentStatusTableViewCell.h"
#import "UserData.h"
#import "ReportCheckNameViewController.h"
#import "ReportCheckSubjectViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RPDataStudentCheckSubjectModel.h"

@interface ReportCheckSubjectCalendarViewController (){
    UIColor *grayColor;
    UIColor *headerTitleColor;
    UIColor *todayColor;
    UIColor *selectionColor;
    
    NSCalendar *gregorian;

    long long classroomId;
    NSString *subjectId;
    
    NSDate *consideredDate;
    NSDateFormatter *dateFormatter;
    
    UIColor *dodgerBlue, *aqua, *monaLisa, *bitterSweet, *ripTide, *mountainMeadow, *perfume, *heliotRope, *alto, *dustyGray, *peachOrange, *texasRose;
    
    NSMutableArray<RPDataStudentCheckSubjectModel *> *studentStatusArray, *studentStatusOnTimeArray, *studentStatusLateArray, *studentStatusAbsenArray, *studentStatusSickArray, *studentStatusPersonalArray, *studentStatusEventArray,*studentStatusUndefinedArray,*studentStatusAllArray;
    
    NSInteger onTimeSum, lateSum, absenceSum, sickSum, personalSum, eventSum, studentSum, undefinedSum;
    
    NSMutableDictionary *fillDefaultColors,*titleDefaultColors,*titleSelectionColors,*borderSelectionColors,*fillSelectionColors;
   // NSDateFormatter *dateFormatter;

}

@property (strong, nonatomic) CallGetTAHistoryStudentStatusListAPI *callGetTAHistoryStudentStatusListAPI;
@property (strong, nonatomic) CallTAGetStatusCalendaAPI *callTAGetStatusCalendaAPI;
@property (nonatomic) CGFloat shadowRadius;
@property (nonatomic) CGFloat shadowOpacity;
@property (nonatomic) CGFloat animationDuration;

@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;

@end

static NSString *cellIdentifier = @"Cell";


@implementation ReportCheckSubjectCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    headerTitleColor = [UIColor colorWithRed:167/255.0 green:87/255.0 blue:90/255.0 alpha:1.0];
    todayColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:1/255.0 alpha:1.0];
    selectionColor = [UIColor colorWithRed:212/255.0 green:44/255.0 blue:31/255.0 alpha:1.0];
    
    //04/05/2017
    
    dodgerBlue = [UIColor colorWithRed:0.21 green:0.73 blue:1.00 alpha:1.0]; // Now Full
    aqua = [UIColor colorWithRed:0.21 green:0.89 blue:1.00 alpha:1.0]; // Now Select
    monaLisa = [UIColor colorWithRed:1.00 green:0.68 blue:0.62 alpha:1.0]; // Color status 0 Before Select
    bitterSweet = [UIColor colorWithRed:1.00 green:0.48 blue:0.38 alpha:1.0]; // Color status 0 After Select
    ripTide = [UIColor colorWithRed:0.47 green:0.93 blue:0.71 alpha:1.0]; // Color status 1 Before Select
    mountainMeadow = [UIColor colorWithRed:0.13 green:0.76 blue:0.46 alpha:1.0]; // Color status 1 After Select
    perfume = [UIColor colorWithRed:0.80 green:0.62 blue:0.98 alpha:1.0]; // Color status 2 Before Select
    heliotRope = [UIColor colorWithRed:0.75 green:0.51 blue:1.00 alpha:1.0]; // Color status 2 After Select
    alto = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1.0]; // Color status 3 Before Select
    dustyGray = [UIColor colorWithRed:0.60 green:0.60 blue:0.60 alpha:1.0]; // Color status 3 After Select
    peachOrange = [UIColor colorWithRed:1.00 green:0.85 blue:0.59 alpha:1.0]; // Color status 4 Before Select
    texasRose = [UIColor colorWithRed:1.00 green:0.75 blue:0.31 alpha:1.0]; // Color status 4 After Select
    
    gregorian = [Utils getGregorianCalendar];
    
    self.minimumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:-1 toDate:[NSDate date] options:0];
    //self.maximumDate = [gregorian dateByAddingUnit:NSCalendarUnitYear value:1 toDate:[NSDate date] options:0];
    self.maximumDate = [NSDate date];

    self.shadowRadius = 5.0f;
    self.shadowOpacity = 0.5f;
    self.animationDuration = 0.55f;
    
  
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height >= 1024.0f) {
            self.heightCalendarConstraint.constant = 450.0f;
        }
    }
    else{
        self.heightCalendarConstraint.constant = 300.0f;
    }
    
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_up"] forState:UIControlStateNormal];
    [self.arrowStatement setImage:[UIImage imageNamed:@"ic_sort_down"] forState:UIControlStateSelected];
    
    classroomId = self.selectedClassroomId;
    subjectId = self.selectedSubjectId;
    
    dateFormatter = [Utils getDateFormatter];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    if(consideredDate == nil) {
        consideredDate = [NSDate date];
    }
    
    self.dateTitleLabel.text = [Utils getThaiDateFormatWithDate:consideredDate];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionNextView:)];
    [self.viewNextAction addGestureRecognizer:singleFingerTap];
    
    //[self getStatusCalendar:285720 day:[NSDate date] subjectID:73217 schoolID:307];
    [self createCalendar];
    [self getHistoryStudentStatus];
    
    
}
- (void)viewWillAppear:(BOOL)animated {
    
    [self createCalendar];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_REPORT_SUBJECT",nil,[Utils getLanguage],nil);
    
    self.headerOnTimeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ONTIME",nil,[Utils getLanguage],nil);
    self.headerLateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_LATE",nil,[Utils getLanguage],nil);
    self.headerAbsenceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ABSENCE",nil,[Utils getLanguage],nil);
    self.headerSickLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_SICK",nil,[Utils getLanguage],nil);
    self.headerPersonalLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PERSONAL",nil,[Utils getLanguage],nil);
    self.headerEventLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_EVENT",nil,[Utils getLanguage],nil);
    self.headerUndefinedLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_UNDEFINED",nil,[Utils getLanguage],nil);
}

- (void)createCalendar {
    
    // Remove all subviews from container
    NSArray *viewsToRemove = self.containerView.subviews;
    for(UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    self.containerView.backgroundColor = grayColor;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    CFSCalendar *calendar = [[CFSCalendar alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, height)];
    
    calendar.dataSource = self;
    calendar.delegate = self;
    calendar.backgroundColor = grayColor;//[UIColor whiteColor];
    calendar.appearance.weekdayTextColor = [UIColor blackColor];
    calendar.appearance.headerTitleColor = headerTitleColor;
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = CFSCalendarCaseOptionsWeekdayUsesUpperCase;
    
    // 04/05/2018
    calendar.appearance.todayColor = dodgerBlue;
    calendar.appearance.titleTodayColor = [UIColor whiteColor];
    calendar.appearance.todaySelectionColor = dodgerBlue;
    
    calendar.appearance.selectionColor = [UIColor clearColor];
    calendar.appearance.titleSelectionColor = [UIColor blackColor];
    calendar.appearance.borderSelectionColor = dodgerBlue;
    
    [self.containerView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(0, 5, 95, 34);
    previousButton.backgroundColor = grayColor;//[UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"ic_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(self.view.frame.size.width - nextButton.frame.size.width - 95, 5, 95, 34);
    nextButton.backgroundColor = grayColor; //[UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:nextButton];
    self.nextButton = nextButton;
    [self getStatusCalendar:[UserData getUserID] day:calendar.currentPage subjectID:[self.selectedSubjectId longLongValue] schoolID:[UserData getSchoolId] classroomID:self.selectedClassroomId];
}



#pragma mark - CFSCalendarDataSource

- (NSDate *)minimumDateForCalendar:(CFSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(CFSCalendar *)calendar {
    return self.maximumDate;
}

#pragma mark - CFSCalendarDelegate

- (BOOL)calendar:(CFSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
    if([DateUtility sameMonthOfYear:date date2:calendar.currentPage]) {
        return YES;
    }
    return NO;
}

- (void)calendar:(CFSCalendar *)calendar didSelectDate:(NSDate *)date {
    self.dateTitleLabel.text = [Utils getThaiDateFormatWithDate: date];
    
    if(![DateUtility sameDate:consideredDate date2:date] ) {
        consideredDate = date;
        [self getHistoryStudentStatus];
    }
}


- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date {
    
    if(fillDefaultColors != nil) {
        NSString *key = [dateFormatter stringFromDate:date];
        
        if([fillDefaultColors objectForKey:key] != nil) {
            return [fillDefaultColors objectForKey:key];
        }
    }
    
    return nil;
    
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date{
    
    if (titleDefaultColors != nil) {
        NSString *key = [dateFormatter stringFromDate:date];
        if ([titleDefaultColors objectForKey:key] != nil) {
            return [titleDefaultColors objectForKey:key];
        }
    }
    return nil;
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance titleSelectionColorForDate:(NSDate *)date{
    
    if (titleSelectionColors != nil) {
        NSString *key = [dateFormatter stringFromDate:date];
        if ([titleSelectionColors objectForKey:key] != nil) {
            return [titleSelectionColors objectForKey:key];
        }
    }
    return nil;
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance borderSelectionColorForDate:(NSDate *)date{
    
    if (borderSelectionColors != nil) {
        NSString *key = [dateFormatter stringFromDate:date];
        if ([borderSelectionColors objectForKey:key] != nil) {
            return [borderSelectionColors objectForKey:key];
        }
    }
    return nil;
}

- (nullable UIColor *)calendar:(CFSCalendar *)calendar appearance:(CFSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date{
    
    if (fillSelectionColors != nil) {
        NSString *key = [dateFormatter stringFromDate:date];
        
        if ([fillSelectionColors objectForKey:key] != nil) {
            return [fillSelectionColors objectForKey:key];
        }
    }
    
    return nil;
    
}

#pragma mark - Class buttons event

- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
    [self getStatusCalendar:[UserData getUserID] day:previousMonth subjectID:[self.selectedSubjectId longLongValue] schoolID:[UserData getSchoolId] classroomID:self.selectedClassroomId];
}

- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
    [self getStatusCalendar:[UserData getUserID] day:nextMonth subjectID:[self.selectedSubjectId longLongValue] schoolID:[UserData getSchoolId] classroomID:self.selectedClassroomId];
}
#pragma mark - CallTAGetStatusCalendaAPI
- (void)getStatusCalendar:(long long)userID day:(NSDate*)day subjectID:(long long)subjectID schoolID:(long long)schoolID classroomID:(long long)classroomID{
    if (self.callTAGetStatusCalendaAPI != nil) {
        self.callTAGetStatusCalendaAPI = nil;
    }
    self.callTAGetStatusCalendaAPI = [[CallTAGetStatusCalendaAPI alloc] init];
    self.callTAGetStatusCalendaAPI.delegate = self;
    [self.callTAGetStatusCalendaAPI call:userID day:day subjectID:subjectID schoolID:schoolID classroomID:classroomID];
}
- (void)callTAGetStatusCalendaAPI:(CallTAGetStatusCalendaAPI *)classObj data:(NSArray<TAStatusCalendaModel *> *)data success:(BOOL)success{
    if (data != nil && success) {
        if (fillDefaultColors != nil) {
            [fillDefaultColors removeAllObjects];
        }
        if (titleDefaultColors != nil) {
            [titleDefaultColors removeAllObjects];
        }
        
        if (titleSelectionColors != nil) {
            [titleSelectionColors removeAllObjects];
        }
        if (borderSelectionColors != nil) {
            [borderSelectionColors removeAllObjects];
        }
        if (fillSelectionColors != nil) {
            [fillSelectionColors removeAllObjects];
        }
        fillDefaultColors = [[NSMutableDictionary alloc] init];
        titleDefaultColors = [[NSMutableDictionary alloc] init];
        titleSelectionColors = [[NSMutableDictionary alloc] init];
        borderSelectionColors = [[NSMutableDictionary alloc] init];
        fillSelectionColors = [[NSMutableDictionary alloc] init];
        for (TAStatusCalendaModel *model in data) {
            NSDateFormatter *formatter = [Utils getDateFormatter];
            [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
            NSDate *checkNameDay = [formatter dateFromString:[model getDay]];
            NSString *dateKey = [dateFormatter stringFromDate:checkNameDay];
            NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:checkNameDay];
            NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
            
            if([today day] == [otherDay day] &&
                [today month] == [otherDay month] &&
                [today year] == [otherDay year] &&
                [today era] == [otherDay era]) {
                [fillDefaultColors setObject:dodgerBlue forKey:dateKey];
                [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
                [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
            }
            else{
                [fillDefaultColors setObject:ripTide forKey:dateKey];
                [fillSelectionColors setObject:mountainMeadow forKey:dateKey];
                [titleDefaultColors setObject:[UIColor whiteColor] forKey:dateKey];
                [titleSelectionColors setObject:[UIColor whiteColor] forKey:dateKey];
                [borderSelectionColors setObject:[UIColor clearColor] forKey:dateKey];
            }
        }
        [self.calendar reloadData];
    }
}

#pragma mark - API Caller
- (void)getHistoryStudentStatus {
    self.callGetTAHistoryStudentStatusListAPI = nil;
    self.callGetTAHistoryStudentStatusListAPI = [[CallGetTAHistoryStudentStatusListAPI alloc] init];
    self.callGetTAHistoryStudentStatusListAPI.delegate = self;
    [self.callGetTAHistoryStudentStatusListAPI call:[UserData getSchoolId] classroomId:classroomId subjectId:subjectId date:consideredDate];
    [self showIndicator];
}

#pragma mark - CallGetTAHistoryStudentStatusListAPIDelegate
- (void)callGetTAHistoryStudentStatusListAPI:(CallGetTAHistoryStudentStatusListAPI *)classObj data:(NSMutableArray<RPDataStudentCheckSubjectModel *> *)data success:(BOOL)success{
    
    [self stopIndicator];
    
    if(success && data != nil) {
        studentStatusArray = data;
        onTimeSum = 0;
        lateSum = 0;
        absenceSum = 0;
        personalSum = 0;
        sickSum = 0;
        eventSum = 0;
        studentSum = 0;
        undefinedSum = 0;
        
        studentStatusOnTimeArray = [[NSMutableArray alloc] init];
        studentStatusLateArray = [[NSMutableArray alloc] init];
        studentStatusAbsenArray = [[NSMutableArray alloc] init];
        studentStatusPersonalArray = [[NSMutableArray alloc] init];
        studentStatusSickArray = [[NSMutableArray alloc] init];
        studentStatusEventArray = [[NSMutableArray alloc] init];
        studentStatusUndefinedArray = [[NSMutableArray alloc] init];
        studentStatusAllArray = [[NSMutableArray alloc] init];
        
        for (int i = 0; i<data.count; i++) {
            
            RPDataStudentCheckSubjectModel *model = [data objectAtIndex:i];
            if ([model getStudentState] == 0) {
                [studentStatusAllArray addObject:model];
                if ([model getStatus] == 0) {
                    [studentStatusOnTimeArray addObject:model];
                    onTimeSum ++;
                }
                else if ([model getStatus] == 1){
                    [studentStatusLateArray addObject:model];
                    lateSum ++;
                }
                else if ([model getStatus] == 3){
                    [studentStatusAbsenArray addObject:model];
                    absenceSum ++;
                }
                else if ([model getStatus] == 4){
                    [studentStatusPersonalArray addObject:model];
                    personalSum ++;
                }
                else if ([model getStatus] == 5){
                    [studentStatusSickArray addObject:model];
                    sickSum ++;
                }
                else if ([model getStatus] == 6){
                    [studentStatusEventArray addObject:model];
                    eventSum ++;
                }else{
                    [studentStatusUndefinedArray addObject:model];
                    undefinedSum ++;
                }
            }
            
            
            
        }
        studentSum = onTimeSum + lateSum + absenceSum + personalSum + sickSum + eventSum + undefinedSum;
        
        self.onTimeLabel.text = [NSString stringWithFormat:@"%d %@",onTimeSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.lateLabel.text = [NSString stringWithFormat:@"%d %@",lateSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.absenceLabel.text = [NSString stringWithFormat:@"%d %@",absenceSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.sickLeaveLabel.text = [NSString stringWithFormat:@"%d %@",sickSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.personalLeaveLabel.text = [NSString stringWithFormat:@"%d %@",personalSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.eventLabel.text = [NSString stringWithFormat:@"%d %@",eventSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.studentTotalLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %d %@",NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_ROOM",nil,[Utils getLanguage],nil), self.nameClassRoom, NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_AMOUNT_STUDENT",nil,[Utils getLanguage],nil), studentSum, NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.undefinedLabel.text = [NSString stringWithFormat:@"%d %@",undefinedSum,NSLocalizedStringFromTableInBundle(@"LABEL_CHECKNAME_REP_PEOPLE",nil,[Utils getLanguage],nil)];
        self.subjectLabel.text = self.nameSubject;
        
    }
}

//#pragma mark - TAReportCalendarViewControllerDelegate
//- (void)onSelectDate:(ReportCheckSubjectCalendarViewController *)classObj date:(NSDate *)date {
//
//    self.dateTitleLabel.text = [Utils getThaiDateFormatWithDate: date];
//
//    if(![DateUtility sameDate:consideredDate date2:date] ) {
//        consideredDate = date;
//        [self getHistoryStudentStatus];
//    }
//}


#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}



- (IBAction)moveBack:(id)sender {
    
    ReportCheckNameViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportCheckNameStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];

    
}

- (IBAction)openDetail:(id)sender {
    
    self.arrowStatement.selected = !self.arrowStatement.selected;
    
    if (self.arrowStatement.isSelected == YES) {
        //        self.topDetailConstraint.constant = -300.0f;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailConstraint.constant = -450.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailConstraint.constant = 0.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = -300.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
        
        
        
    }else{
        //        self.topDetailConstraint.constant = 0.0f;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (screenSize.height >= 1024.0f) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = -450.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        self.topDetailConstraint.constant = 0.0f;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
        else{
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.topDetailConstraint.constant = -300.0f;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.topDetailConstraint.constant = 0.0f;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
    }
    
    
}

- (void)actionNextView:(UITapGestureRecognizer *)recognizer
{
    
    
    ReportCheckSubjectHistoryStudentAllViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportCheckSubjectHistoryStudentAllStoryboard"];
    viewController.studentStatusOnTimeArray = studentStatusOnTimeArray;
    viewController.studentStatusLateArray = studentStatusLateArray;
    viewController.studentStatusPersonalArray = studentStatusPersonalArray;
    viewController.studentStatusSickArray = studentStatusSickArray;
    viewController.studentStatusEventArray = studentStatusEventArray;
    viewController.studentStatusAbsenArray = studentStatusAbsenArray;
    viewController.studentStatusAllArray = studentStatusAllArray;
    viewController.studentAllArray = studentStatusArray;
    viewController.studentStatusUndefinedArray = studentStatusUndefinedArray;
    viewController.nameClassRoom = self.nameClassRoom;
    viewController.nameSubject = self.nameSubject;
    viewController.selectedClassroomId = self.selectedClassroomId;
    viewController.consideredDate = consideredDate;
    viewController.selectedSubjectId = self.selectedSubjectId;
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    //Do stuff here...
}
@end
