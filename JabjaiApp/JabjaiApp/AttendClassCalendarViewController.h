//
//  AttendClassCalendarViewController.h
//  JabjaiApp
//
//  Created by mac on 1/11/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CFSCalendar.h"
#import "Attend2SchoolFilter.h"

@interface AttendClassCalendarViewController : UIViewController <CFSCalendarDelegate, CFSCalendarDataSource, CFSCalendarDelegateAppearance>

@property (weak, nonatomic) IBOutlet CFSCalendar *calendar;

@property (nonatomic) int sectionID;

-(void)applyFilter:(Attend2SchoolFilter *)filter;

@end
