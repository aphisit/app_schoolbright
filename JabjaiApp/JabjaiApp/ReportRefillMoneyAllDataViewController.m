//
//  ReportRefillMoneyAllDataViewController.m
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportRefillMoneyAllDataViewController.h"

#import "ReportListTeacherViewController.h"
#import "Utils.h"
#import "UserData.h"
#import "AlertDialog.h"

//#import "DayAxisValueFormatter.h"

@interface ReportRefillMoneyAllDataViewController (){
    
    AlertDialog *alertDialog;
    NSMutableArray *DateList;
    NSBundle *myLangBundle;
    NSString *selected_calendar;
    NSArray *options;
    NSArray *timeXaxisArray;
    NSString *statusDay;
    MKDropdownMenu *hiedDropdownMenu;
}

@property (strong, nonatomic) CallReportRefillMoneyAPI *callReportRefillMoneyAPI;
@property (strong, nonatomic) CallReportRefillMoneyWeekAPI *callReportRefillMoneyWeekAPI;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation ReportRefillMoneyAllDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    [self setLanguage];
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
   // _dateTimeDropdown.layer.borderWidth = 1.0f;
    [self doSelectDayCalendar:@"day"];
    [self setDropdowe];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) setLanguage{
     self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_REP_MONNEY",nil,[Utils getLanguage],nil);
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}

- (void) setBarChar{
    
    _barCharView.delegate = self;
    _barCharView.drawBarShadowEnabled = NO;
    _barCharView.drawValueAboveBarEnabled = YES;
    _barCharView.maxVisibleCount = 60;
    _barCharView.xAxis.drawGridLinesEnabled = NO;
    _barCharView.leftAxis.drawLabelsEnabled = NO;
    _barCharView.legend.enabled = NO;
    _barCharView.chartDescription.text = @"";
    
    _barCharView.leftAxis.drawAxisLineEnabled = false;
    _barCharView.leftAxis.drawGridLinesEnabled = false;
    _barCharView.leftAxis.gridColor = [UIColor clearColor];
    _barCharView.xAxis.drawGridLinesEnabled = false;
    _barCharView.backgroundColor = [UIColor whiteColor];
    _barCharView.translatesAutoresizingMaskIntoConstraints = false;
    [_barCharView animateWithYAxisDuration:3.0];
    
    ChartXAxis *xAxis = _barCharView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.granularity = 1.0; // only intervals of 1 day
    xAxis.valueFormatter = self;
    xAxis.labelRotationAngle = 270.0f;

  
    NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];

    ChartYAxis *leftAxis = _barCharView.leftAxis;
    leftAxis.labelFont = [UIFont systemFontOfSize:0.f];
    leftAxis.labelCount = 8;
    leftAxis.valueFormatter = [[ChartDefaultAxisValueFormatter alloc] initWithFormatter:leftAxisFormatter];
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.spaceTop = 0.15;
    leftAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
    leftAxis.drawAxisLineEnabled = NO;
    
    ChartYAxis *rightAxis = _barCharView.rightAxis;
    rightAxis.enabled = YES;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.labelFont = [UIFont systemFontOfSize:0.f];
    rightAxis.labelCount = 8;
    rightAxis.valueFormatter = leftAxis.valueFormatter;
    rightAxis.spaceTop = 0.15;
    rightAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
    rightAxis.drawAxisLineEnabled = NO;
    
}

//Call API today
- (void) getReportRefillMoneyAPI:(long long)schoolId date:(NSString*)date{
    
    if(self.callReportRefillMoneyAPI != nil) {
        self.callReportRefillMoneyAPI = nil;
    }
    self.callReportRefillMoneyAPI = [[CallReportRefillMoneyAPI alloc] init];
    self.callReportRefillMoneyAPI.delegate = self;
    [self.callReportRefillMoneyAPI call:schoolId date:date];
}

- (void) callReportRefillMoneyAPI:(CallReportRefillMoneyAPI *)classObj timeArray:(NSArray *)timeArray totalMoneyArray:(NSArray *)totalMoneyArray success:(BOOL)success{
    
    timeXaxisArray = timeArray;
    int totalAmount = 0;
    for (int i = 0; i < totalMoneyArray.count; i++) {
        totalAmount = totalAmount + [[totalMoneyArray objectAtIndex:i] integerValue];
    }
    
    NSString *display = [NSNumberFormatter localizedStringFromNumber:@(totalAmount)
                                                         numberStyle:NSNumberFormatterDecimalStyle];
    [self.amountLabel setText:[NSString stringWithFormat:@"%@ %@",display,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_B",nil,myLangBundle,nil)]];
    [self.detailRefillMoneyLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_REP_TODAY",nil,[Utils getLanguage],nil)];
    [self setGraphBarChart:timeArray values:totalMoneyArray];
}

//Call API Week
- (void) getReportRefillMoneyWeekAPI:(long long)schoolId dayStart:(NSString*)dayStart dayEnd:(NSString*)dayEnd{
    
    if(self.callReportRefillMoneyWeekAPI != nil) {
        self.callReportRefillMoneyWeekAPI = nil;
    }
    self.callReportRefillMoneyWeekAPI = [[CallReportRefillMoneyWeekAPI alloc] init];
    self.callReportRefillMoneyWeekAPI.delegate = self;
    [self.callReportRefillMoneyWeekAPI call:schoolId dayStart:dayStart dayEnd:dayEnd] ;
}
-(void) callReportRefillMoneyWeekAPI:(CallReportRefillMoneyWeekAPI *)classObj dayArray:(NSArray *)dayArray totalMoneyArray:(NSArray *)totalMoneyArray success:(BOOL)success{
    
    if (success) {
        timeXaxisArray = dayArray;
        int totalAmount = 0;
        for (int i = 0; i < totalMoneyArray.count; i++) {
            totalAmount = totalAmount + [[totalMoneyArray objectAtIndex:i] integerValue];
        }
        NSString *display = [NSNumberFormatter localizedStringFromNumber:@(totalAmount)
                                                             numberStyle:NSNumberFormatterDecimalStyle];
        [self.amountLabel setText:[NSString stringWithFormat:@"%@ %@",display,NSLocalizedStringFromTableInBundle(@"LABEL_INFO_B",nil,myLangBundle,nil)]];
        if ([statusDay isEqualToString:@"week"]) {
             [self.detailRefillMoneyLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_REP_MOUNT_WEEK",nil,[Utils getLanguage],nil)];
        }else if ([statusDay isEqualToString:@"month"]){
             [self.detailRefillMoneyLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_REP_MOUNT_MONNEY",nil,[Utils getLanguage],nil)];
        }else if ([statusDay isEqualToString:@"year"]){
            [self.detailRefillMoneyLabel setText:NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_REP_YEAR_MONNEY",nil,[Utils getLanguage],nil)];
        }
        
        [self setGraphBarChart:dayArray values:totalMoneyArray];
    }
}


- (void) setDropdowe{
    // Set up dropdown
    self.dateTimeDropdown.dataSource = self;
    self.dateTimeDropdown.delegate = self;
    self.dateTimeDropdown.rowTextAlignment = NSTextAlignmentCenter;
    self.dateTimeDropdown.backgroundDimmingOpacity = 0;
    self.dateTimeDropdown.dropdownShowsTopRowSeparator = YES;
    self.dateTimeDropdown.dropdownCornerRadius = 5.0;
    self.dateTimeDropdown.tintColor = [UIColor blackColor];
    // self.dateTimeDropdown.layer.cornerRadius = 5;
    self.dateTimeDropdown.layer.masksToBounds = YES;
}

- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {

    if(alertDialog == nil) {
        alertDialog = [[AlertDialog alloc] init];
    }
    else if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }

    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showFinishAlertDialogWithTitle:(NSString *)title message:(NSString *)message {

    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else{

        //        alertDialog = [[AlertDialog alloc] initWithNibName:@"AlertDialog" bundle:nil];

        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }

    [alertDialog showDialogInView:self.view title:title message:message];

}

- (void)onAlertDialogClose {

    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    [self.slideMenuController changeMainViewController:viewController close:YES];

}

-(void)doSelectDayCalendar:(NSString*)selectMode{
    statusDay = selectMode;
    
    NSDate *dateNow = [NSDate date];
    //day
    NSCalendar *gregorian = [Utils getGregorianCalendar];
    [gregorian setFirstWeekday:2]; // Sunday == 1, Saturday == 7
    NSUInteger adjustedWeekdayOrdinal = [gregorian ordinalityOfUnit:NSWeekdayCalendarUnit inUnit:NSWeekCalendarUnit forDate:dateNow];
    NSLog(@"Adjusted weekday ordinal: %d", adjustedWeekdayOrdinal);
    //month
    NSCalendar *gregorian1 = [Utils getGregorianCalendar];
    NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:dateNow];
    [comp setDay:1];
    NSDate *firstDayOfMonthDate = [gregorian dateFromComponents:comp];
    //year
    NSDateComponents *components= [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:dateNow];
    components.month = 1;
    components.day = 1;
    NSDate *firstDayOfYear = [[NSCalendar currentCalendar] dateFromComponents:components];



    NSString *daysNow = [Utils getThaiDateFormatWithDate:dateNow]; // 20 /กันยายน /2561

    //day
    NSDate *daysAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-(adjustedWeekdayOrdinal-1) toDate:[NSDate date] options:0];
    NSString *daysStr = [Utils getThaiDateFormatWithDate:daysAgo];
    //month
    NSDate *monthAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfMonthDate options:0];
    NSString *monthStr = [Utils getThaiDateFormatWithDate:monthAgo];
    //year
    NSDate *yearAgo = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfYear options:0];
    NSString *yearStr = [Utils getThaiDateFormatWithDate:yearAgo];

    DateList = [[NSMutableArray alloc] init];
    
    [DateList addObject:[NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_DAY",nil,myLangBundle,nil), daysNow]];
    [DateList addObject:[NSString stringWithFormat:@"%@ %@ - %@",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_WEEKLY",nil,myLangBundle,nil),daysStr,daysNow]];
    [DateList addObject:[NSString stringWithFormat:@"%@ %@ - %@",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_MONTHLY",nil,myLangBundle,nil),monthStr,daysNow]];
    [DateList addObject:[NSString stringWithFormat:@"%@ %@ - %@",NSLocalizedStringFromTableInBundle(@"LABEL_INFO_YEARLY",nil,myLangBundle,nil),yearStr,daysNow]];
   

    
    if ([selectMode isEqualToString:@"day"]) {
        selected_calendar = [DateList objectAtIndex:0];
        NSString *date = [Utils dateToServerDateFormat:[NSDate date]];
        [self getReportRefillMoneyAPI:[UserData getSchoolId] date:date];
        
    }
    else if ([selectMode isEqualToString:@"week"]) {
        statusDay = selectMode;
        //selected_calendar = [DateList objectAtIndex:1];
        NSDate *days = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:-(adjustedWeekdayOrdinal-1) toDate:[NSDate date] options:0];
        NSString *dayStart = [Utils dateToServerDateFormat:days];
        NSString *dayEnd = [Utils dateToServerDateFormat:[NSDate date]];
        [self getReportRefillMoneyWeekAPI:[UserData getSchoolId] dayStart:dayStart dayEnd:dayEnd];
       
    }else if([selectMode isEqualToString:@"month"]){
        NSDate *month = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfMonthDate options:0];
        NSString *dayStart = [Utils dateToServerDateFormat:month];
        NSString *dayEnd = [Utils dateToServerDateFormat:[NSDate date]];
        [self getReportRefillMoneyWeekAPI:[UserData getSchoolId] dayStart:dayStart dayEnd:dayEnd];
      
    }else if([selectMode isEqualToString:@"year"]){
        NSDate *year = [[Utils getGregorianCalendar] dateByAddingUnit:NSCalendarUnitDay value:0 toDate:firstDayOfYear options:0];
        NSString *dayStart = [Utils dateToServerDateFormat:year];
        NSString *dayEnd = [Utils dateToServerDateFormat:[NSDate date]];
        [self getReportRefillMoneyWeekAPI:[UserData getSchoolId] dayStart:dayStart dayEnd:dayEnd];
       
    }
    [self setBarChar];
    NSLog(@"xxx");
}

#pragma mark - MKDropdownMenuDataSource
-(NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu {

    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component {
    hiedDropdownMenu  = dropdownMenu;
    return DateList.count;
}

#pragma mark - MKDropdownMenuDelegate
- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
    return 35;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {

    NSString *textString;

    textString = selected_calendar;

    NSAttributedString *attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:20], NSForegroundColorAttributeName: [UIColor blackColor] }];

    return attributes;
}

-(NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {

    NSString *textString;
    textString = [DateList objectAtIndex:row];
    NSAttributedString *attributes = [[NSAttributedString alloc] initWithString:textString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"TH Sarabun New" size:20], NSForegroundColorAttributeName: [UIColor blackColor] }];
    return attributes;
}

- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
    return NO;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(dropdownMenu.tag == 0) { // 0 : Study Year
        selected_calendar = [DateList objectAtIndex:row];
        if (row == 0) {
            [self doSelectDayCalendar:@"day"];
            
        }else if (row == 1){
            [self doSelectDayCalendar:@"week"];
            
        }else if(row == 2){
            [self doSelectDayCalendar:@"month"];
            
        }else if (row == 3){
            [self doSelectDayCalendar:@"year"];
        }
        [dropdownMenu reloadAllComponents];
    }

    delay(0.0, ^{
        [dropdownMenu closeAllComponentsAnimated:YES];

    });
}

-(void)setGraphBarChart:(NSArray*)dataPoints values:(NSArray*)values{
    self.barCharView.noDataText = @"No Data Available kingly add some";
    NSMutableArray *dataEntries = [[NSMutableArray alloc] init];

    for (int i = 0; i<dataPoints.count; i++) {
        double x1 = [values[i]doubleValue];
        [dataEntries addObject:[[BarChartDataEntry alloc] initWithX:i y:x1]];
    }
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithEntries:dataEntries];
    BarChartData *chartData = [[BarChartData alloc] initWithDataSet:set1];
    self.barCharView.data = chartData;
    [set1 setColor:[UIColor colorWithRed:0.16 green:0.81 blue:0.65 alpha:1.0]];
   // [self.barCharView animateWithXAxisDuration:2.0 easingOption:ChartEasingOptionEaseInBounce];
   
}
#pragma mark - IAxisValueFormatter
- (NSString * _Nonnull)stringForValue:(double)value axis:(ChartAxisBase * _Nullable)axis
{
    NSString *xAxisStringValue = @"";
    int myInt = (int)value;
    if(timeXaxisArray.count > myInt)
        xAxisStringValue = [timeXaxisArray objectAtIndex:myInt];
    
    return xAxisStringValue;
}


#pragma mark - ChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight
{
    NSLog(@"chartValueSelected");
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
    NSLog(@"chartValueNothingSelected");
}


- (IBAction)moveBack:(id)sender {
    [hiedDropdownMenu closeAllComponentsAnimated:YES];
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
