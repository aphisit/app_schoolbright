//
//  ReportOrderHomeWorkListTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 4/30/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportOrderHomeWorkListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UILabel *headTitle;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIImageView *homeworkLogo;
@property (weak, nonatomic) IBOutlet UIImageView *attachFileLogo;
@property (weak, nonatomic) IBOutlet UIView *sectionView;


@end
