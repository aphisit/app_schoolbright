//
//  NewsMessageTableViewCell.m
//  JabjaiApp
//
//  Created by mac on 5/16/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NewsMessageTableViewCell.h"

@implementation NewsMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.dateTimeLabel.text = nil;
    self.titleLabel.text = nil;
    self.messageLabel.text = nil;
    self.schoolLogo.image =  [UIImage imageNamed:@"school"];
    
    [self.bgView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.bgView.layer setShadowOpacity:0.3];
    [self.bgView.layer setShadowRadius:3.0];
    [self.bgView.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    self.dateTimeLabel.text = nil;
    self.titleLabel.text = nil;
    self.messageLabel.text = nil;
    self.schoolLogo.image =  [UIImage imageNamed:@"school"];
}

@end
