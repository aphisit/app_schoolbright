//
//  ReportOrderHomeWorkDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 4/25/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportOrderHomeWorkDetailViewController : UIViewController

@property (assign, nonatomic) long long homeworkID;

@property (weak, nonatomic) IBOutlet UILabel *headerTypeLabel;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *sendTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *recieverLabel;
@property (weak, nonatomic) IBOutlet UILabel *allCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *unreadCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (void)getReportOrderHomeworkDetailWithHomeworkID:(long long)homeworkId;

@end
