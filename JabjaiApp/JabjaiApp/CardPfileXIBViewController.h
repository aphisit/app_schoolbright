//
//  CardPfileXIBViewController.h
//  JabjaiApp
//
//  Created by toffee on 7/9/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallGMGetUserInfoDataAPI.h"
#import "CallImageBarCodeAPI.h"
@protocol CardPfileXIBViewControllerDelegate <NSObject, CallGMGetUserInfoDataAPIDelegate, CallImageBarCodeAPIDelegate>

@optional

//- (void)successConfirmPin:(NSString*)status;
- (void)onAlertDialogClose;
@end
@interface CardPfileXIBViewController : UIViewController
@property (nonatomic, retain) id<CardPfileXIBViewControllerDelegate> delegate;
@property BOOL isFlib;



@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *contentViewCard;
@property (weak, nonatomic) IBOutlet UIView *contentViewCardBack;
@property (weak, nonatomic) IBOutlet UIButton *flibbutton;
//page barcode
@property (strong, nonatomic) IBOutlet UIImageView *imageUser;
@property (strong, nonatomic) IBOutlet UIImageView *imageSchool;
@property (strong, nonatomic) IBOutlet UIImageView *imageBarcode;
@property (strong, nonatomic) IBOutlet UILabel *nameHeadLabel;
@property (strong, nonatomic) IBOutlet UILabel *idHeadLabel;
@property (strong, nonatomic) IBOutlet UILabel *departmentHeadLabel;
@property (strong, nonatomic) IBOutlet UILabel *positionHeadLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *departmentLabel;
@property (strong, nonatomic) IBOutlet UILabel *idCardLabel;
@property (strong, nonatomic) IBOutlet UILabel *positionLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameSchoolLable;

//page qrcode
@property (weak, nonatomic) IBOutlet UIImageView *qrImageSchool;
@property (weak, nonatomic) IBOutlet UIImageView *qrImageUser;
@property (weak, nonatomic) IBOutlet UIImageView *qrImageQrCode;

@property (weak, nonatomic) IBOutlet UILabel *qrNameHeadLabel;
@property (weak, nonatomic) IBOutlet UILabel *qrClassroomHeadLabel;

@property (weak, nonatomic) IBOutlet UILabel *qrNameSchoolLabel;
@property (weak, nonatomic) IBOutlet UILabel *qrNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *qrClassroomLabel;


- (IBAction)closeAction:(id)sender;
- (IBAction)flibButtonAction:(id)sender;

- (void)dismissDialog;
- (BOOL)isDialogShowing;
- (void)showDialogInView:(UIView *)targetView  ;
@end
