//
//  JHReadFileViewController.h
//  JabjaiApp
//
//  Created by toffee on 15/7/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JHReadFileViewController : UIViewController <UIWebViewDelegate>

-(void)showPagesReadFileInView:(UIView *)targetView partFile:(NSString*)partFile;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *hideLoadView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

- (IBAction)closeAction:(id)sender;
- (IBAction)saveFileAction:(id)sender;
@end

NS_ASSUME_NONNULL_END
