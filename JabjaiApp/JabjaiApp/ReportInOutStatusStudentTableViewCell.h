//
//  ReportInOutStatusStudentTableViewCell.h
//  JabjaiApp
//
//  Created by toffee on 1/3/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportInOutStatusStudentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameStudentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *picStudentImage;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (weak, nonatomic) IBOutlet UILabel *runNumberLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundColorView;

- (void)updateStatus:(int)status;

@end

NS_ASSUME_NONNULL_END
