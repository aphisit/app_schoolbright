//
//  TEStatusModel.h
//  JabjaiApp
//
//  Created by toffee on 1/16/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TEStatusModel : NSObject

@property (nonatomic) long long userId;
@property (nonatomic) int scanStatus; // 0: On Time, 1:Late, 3: Absence, 4: Personal Leave, 5: Sick Leave, 6:Event Leave, -1: Not Scan
@property (nonatomic) NSString *studentId;
@property (nonatomic) NSString *studentName;
@property (nonatomic) long long teacherId;
@property (nonatomic) NSString *checkedTeacherName;
@property (nonatomic) BOOL authorized;
@property (nonatomic) NSString* pic;
@property (nonatomic) int studentState;
@property (nonatomic) BOOL statusCheck;

- (void)setUserId:(long long)userId;
- (void)setScanStatus:(int)scanStatus;
- (void)setStudentId:(NSString *)studentId;
- (void)setStudentName:(NSString *)studentName;
- (void)setTeacherId:(long long)teacherId;
- (void)setCheckedTeacherName:(NSString *)checkedTeacherName;
- (void)setAuthorized:(BOOL)authorized;
- (void)setStudentPic:(NSString*)studentPic;
- (void)setStudentState:(int)studentState;
- (void)setstatusCheck:(BOOL)statusCheck;

- (long long)getUserId;
- (int)getScanStatus;
- (NSString *)getStudentId;
- (NSString *)getStudentName;
- (long long)getTeacherId;
- (NSString *)getCheckedTeacherName;
- (BOOL)getAuthorized;
- (NSString*)getStudentPic;
- (int) getStudentState;
- (BOOL)getstatusCheck;


@end
