//
//  JHSelectStudenClassViewController.h
//  JabjaiApp
//
//  Created by toffee on 11/21/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "JHStudentNameRadioTableViewCell.h"
#import "CallGetPreviousAttendClassStatusAPI.h"
#import "TAStatusDialog.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "JHSubjectModel.h"
#import "SSubjectModel.h"
#import "CallJHGetStudentInClassroomAPI.h"

#import "SlideMenuController.h"

@interface JHSelectStudenClassViewController : UIViewController<JHStudentNameRadioTableViewCellDelegate, UITableViewDataSource, UITableViewDelegate, CallGetPreviousAttendClassStatusAPIDelegate, TAStatusDialogDelegate,CallJHGetStudentInClassroomAPIDelegate, UISearchBarDelegate, SlideMenuControllerDelegate>
// Global variables

// TAStudentListViewController
@property (strong, nonatomic) NSArray<JHSelectedStudentModel *> *studentRadioArray;
 //JHClassLevelViewController
@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classRoomArray;
@property (strong, nonatomic) NSArray<JHSubjectModel *> *sujectNameArrray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedSubjectIndex;
@property (nonatomic) NSInteger mode;

// BSSelectStudentsViewController
@property (strong, nonatomic) NSArray<JHSelectedStudentModel *> *selectedStudentArray;

// JHClassLevelViewController
@property (strong, nonatomic) NSArray<JHSelectedStudentModel *> *studentArray;
@property (assign, nonatomic) NSMutableArray* classroomId;
@property (assign, nonatomic) NSString* subjectId;
@property (assign, nonatomic) NSString *subjectName;
@property (assign, nonatomic) NSInteger *sendType;



@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


- (IBAction)moveBack:(id)sender;


- (IBAction)actionNext:(id)sender;




@end
