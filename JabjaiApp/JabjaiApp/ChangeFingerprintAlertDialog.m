//
//  ChangeFingerprintAlertDialog.m
//  JabjaiApp
//
//  Created by mac on 12/21/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import "ChangeFingerprintAlertDialog.h"

@interface ChangeFingerprintAlertDialog ()

@property (nonatomic, assign) BOOL isShowing;

@end

@implementation ChangeFingerprintAlertDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Decorate view
    self.dialogView.layer.cornerRadius = 10;
    self.dialogView.layer.masksToBounds = YES;
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)].CGPath;
    self.headerView.layer.mask = maskLayer;
    self.headerView.layer.masksToBounds = YES;
    
    self.closeButton.layer.cornerRadius = 20;
    self.closeButton.layer.masksToBounds = YES;
    
    // Initialize variables
    self.isShowing = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.dialogView.frame, touchLocation)) {
        [self dismissDialog];
    }
}

- (IBAction)closeDialog:(id)sender {
    [self dismissDialog];
}

#pragma mark - Dialgo Functions

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message {
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.dialogView setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.dialogView setAlpha:1.0];
    [UIView commitAnimations];
    
    [self.titleLabel setText:title];
    [self.messageLabel setText:message];
    
    self.isShowing = YES;
    
}

- (void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.dialogView setAlpha:0.0];
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    self.isShowing = NO;
}

- (void)dismissDialog {
    [self removeDialogFromView];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(onChangeFingerprintAlertDialogClose)]) {
        [self.delegate onChangeFingerprintAlertDialogClose];
    }
}

- (BOOL)isDialogShowing {
    return self.isShowing;
}

@end
