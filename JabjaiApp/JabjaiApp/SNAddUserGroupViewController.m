//
//  SNAddUserGroupViewController.m
//  JabjaiApp
//
//  Created by toffee on 2/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "SNAddUserGroupViewController.h"
#import "SNAddUserInGroupCell.h"
#import "SNDeleteUserGroupCell.h"
#import "SNSelectLevelMyGroupViewController.h"

@interface SNAddUserGroupViewController (){
   
}

@end
 static NSString *addCellIdentifier = @"addCell";
static NSString *deleteCellIdentifier = @"deleteCell";
@implementation SNAddUserGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SNAddUserInGroupCell class]) bundle:nil] forCellWithReuseIdentifier:addCellIdentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SNDeleteUserGroupCell class]) bundle:nil] forCellWithReuseIdentifier:deleteCellIdentifier];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 5;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
   
    if (indexPath.row == 0) {
        SNAddUserInGroupCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:addCellIdentifier forIndexPath:indexPath];
        
         cell.delegate = self;
        return cell;
    }else{
        SNDeleteUserGroupCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:deleteCellIdentifier forIndexPath:indexPath];
        
        //cell.delegate = self;
        return cell;
    }
  
    //recipeImageView.image = [UIImage imageNamed:[recipeImages objectAtIndex:indexPath.row]];
    

}
- (void)onPressAddButton:(SNAddUserInGroupCell *)collectionViewCell atIndex:(NSInteger)index{
    NSLog(@"xxxxxxxxx");
    SNSelectLevelMyGroupViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNSelectLevelMyGroupStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
