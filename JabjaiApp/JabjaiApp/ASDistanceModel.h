//
//  ASDistanceModel.h
//  JabjaiApp
//
//  Created by Mac on 11/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ASDistanceModel : NSObject
@property (nonatomic,strong) NSString *nameLocationStart;
@property (nonatomic,strong) NSString *nameLocationEnd;
@property (nonatomic,strong) NSString *kilomater;
@property (nonatomic,strong) NSString *timeLeft;
@property (nonatomic,strong) NSArray *distance;
@end

NS_ASSUME_NONNULL_END
