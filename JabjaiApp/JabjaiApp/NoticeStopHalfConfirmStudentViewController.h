//
//  NoticeStopHalfConfirmStudentViewController.h
//  JabjaiApp
//
//  Created by mac on 3/7/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SNTimeOutImage.h"
#import "NoticeStopHalfConfirmStudentCollectionViewCell.h"
#import "SlideMenuController.h"
#import "SWRevealViewController.h"
#import "NoticeImageCollectionViewCell.h"
#import "AlertDialog.h"
@interface NoticeStopHalfConfirmStudentViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,SlideMenuControllerDelegate,AlertDialogDelegate>

@property (nonatomic) NSInteger schoolId;
@property (nonatomic) NSString* noticeDetail;
@property (nonatomic) NSArray* imageNotice;
@property (nonatomic) NSString* leaveCause;
@property (nonatomic) NSString* setleaveCause;
@property (nonatomic) NSString* headTitleAddress;
@property (nonatomic) NSString* headSubDistrict;
@property (nonatomic) NSString* headDistrict;
@property (nonatomic) NSString* headProvince;
@property (nonatomic) NSString* headPhoneNumber;
@property (nonatomic) NSString* address;
@property (nonatomic) NSString* province;
@property (nonatomic) NSString* district;
@property (nonatomic) NSString* subDistrict;
@property (nonatomic) NSString* road;
@property (nonatomic) NSString* phoneNumber;
@property (nonatomic) NSDate *leaveStartdate;
@property (nonatomic) NSDate *leaveEndDate;
@property (nonatomic) NSMutableArray* level2id;
@property (nonatomic) NSMutableArray* studentid;
@property (nonatomic) NSMutableArray* employeesid;
@property (nonatomic) NSString* leavingFirstDate;
@property (nonatomic) NSString* leavingLastDate;
@property (nonatomic) NSString* leaveFirstDate;
@property (nonatomic) NSString* setleaveFirstDate;
@property (nonatomic) NSString* leaveLastDate;
@property (nonatomic) NSString* setleaveLastDate;
@property (nonatomic) NSString* leavePeriod;
@property (nonatomic, assign) int fillDetailTag;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UILabel *headerLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTeacherLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerReasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDatefromLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSeasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerImageUserLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerAddressLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerSubmitbtn;



@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *leaveStudentName;
@property (weak, nonatomic) IBOutlet UILabel *leaveClass;
@property (weak, nonatomic) IBOutlet UILabel *leaveTeacherName;
@property (weak, nonatomic) IBOutlet UILabel *leaveCauseitem;
@property (weak, nonatomic) IBOutlet UILabel *firstLeaveDate;
@property (weak, nonatomic) IBOutlet UILabel *periodLeaveDate;
@property (weak, nonatomic) IBOutlet UILabel *confirmDetail;


@property (weak, nonatomic) IBOutlet UILabel *addressDetail;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCollectionConStraint;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)confirmPage:(id)sender;
- (IBAction)moveback:(id)sender;


@end
