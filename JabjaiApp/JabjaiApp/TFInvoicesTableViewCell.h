//
//  TFInvoicesTableViewCell.h
//  JabjaiApp
//
//  Created by Mac on 7/10/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TFInvoicesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *roundLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end

NS_ASSUME_NONNULL_END
