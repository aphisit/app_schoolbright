//
//  ReportFeeFilter.h
//  JabjaiApp
//
//  Created by mac on 4/17/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportFeeFilter : NSObject

@property (nonatomic, strong) NSNumber *schoolyearID;
@property (nonatomic, strong) NSNumber *schoolyearNumber;
@property (nonatomic, strong) NSString *schooltermID;
@property (nonatomic, strong) NSString *schooltermName;
@property (nonatomic, strong) NSDate *schooltermBegin;
@property (nonatomic, strong) NSDate *schooltermEnd;

@end
