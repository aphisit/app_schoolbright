//
//  SchoolFeeListModel.h
//  JabjaiApp
//
//  Created by mac on 3/13/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SchoolFeeListModel : NSObject

@property (assign, nonatomic) long long invoiceID;

@property (nonatomic) int status;

//@property (strong, nonatomic) NSString *status;

@property (nonatomic) float totalPrice;

@property (strong, nonatomic) NSString *termName;
@property (strong, nonatomic) NSString *yearName;

@property (strong, nonatomic) NSDate *dateIssue;
@property (strong, nonatomic) NSDate *dateDue;


@end
