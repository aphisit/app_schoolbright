//
//  NoticeInboxDetailModel.h
//  JabjaiApp
//
//  Created by mac on 1/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoticeInboxDetailModel : NSObject

@property (assign, nonatomic) long long messageID;
@property (assign, nonatomic) long long letterID;
@property (assign, nonatomic) long long userID;
@property (assign, nonatomic) NSArray *homework;
@property (nonatomic) int status;
@property (nonatomic) int pageStatus;
@property (strong, nonatomic) NSString *season;
@property (strong, nonatomic) NSString *leaveName;
@property (strong, nonatomic) NSString *leavePosition;
@property (strong, nonatomic) NSString *leaveCause;
@property (strong, nonatomic) NSDate *leaveStart;
@property (strong, nonatomic) NSDate *leaveEnd;
@property (strong, nonatomic) NSString *leaveDetail;
@property (strong, nonatomic) NSString *aumphur;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *province;
@property (strong, nonatomic) NSString *tumbon;
@property (strong, nonatomic) NSDate *leaveDateRequest;
@property (strong, nonatomic) NSMutableArray *file;
@property (strong, nonatomic) NSString *classRoom;
@property (strong, nonatomic) NSString *studentCode;

@end
