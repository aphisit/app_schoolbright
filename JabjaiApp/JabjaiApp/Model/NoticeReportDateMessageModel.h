//
//  NoticeReportDateMessageModel.h
//  JabjaiApp
//
//  Created by mac on 1/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoticeReportDateMessageModel : NSObject

@property (nonatomic) long long letterID;

@property (nonatomic) int status;

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *leaveType;
@property (nonatomic, strong) NSString *countLeave;

@end
