//
//  NoticeInboxModel.h
//  JabjaiApp
//
//  Created by mac on 12/8/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoticeInboxModel : NSObject

@property (nonatomic) long long letterID;
@property (nonatomic) long long userID;
@property (nonatomic) int letterType;
@property (nonatomic) int status;
@property (nonatomic, strong) NSString *leaveName;
@property (nonatomic, strong) NSString *leaveType;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *userType;

@end
