//
//  SchoolFeeDetailModel.h
//  JabjaiApp
//
//  Created by mac on 3/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SchoolFeeDetailModel : NSObject

@property (assign, nonatomic) long long invoiceID;

@property (nonatomic) int status;

//@property (strong, nonatomic) NSString *status;

@property (nonatomic) float totalPrice;

@property (strong, nonatomic) NSString *personName;

@property (strong, nonatomic) NSString *termName;
@property (strong, nonatomic) NSString *yearName;

@property (strong, nonatomic) NSString *codeName;
@property (strong, nonatomic) NSString *levelName;
@property (strong, nonatomic) NSString *className;

@property (strong, nonatomic) NSString *productName;
@property (nonatomic) float price;

@property (strong, nonatomic) NSMutableArray *product;

@property (strong, nonatomic) NSDate *dateIssue;
@property (strong, nonatomic) NSDate *dateDue;

@property (strong, nonatomic) NSString *schoolName;
@property (strong, nonatomic) NSString *imageUrl;

@end
