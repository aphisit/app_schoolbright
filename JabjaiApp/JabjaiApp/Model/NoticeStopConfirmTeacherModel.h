//
//  NoticeStopConfirmTeacherModel.h
//  JabjaiApp
//
//  Created by mac on 11/30/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoticeStopConfirmTeacherModel : NSObject

@property (strong , nonatomic) NSString *senderLeaveName;
@property (strong , nonatomic) NSString *senderLeavePosition;
@property (strong , nonatomic) NSString *senderLeaveCause;
@property (strong , nonatomic) NSString *senderLeaveStartDate;
@property (strong , nonatomic) NSString *senderLeaveLastDate;
@property (strong , nonatomic) NSString *senderLeaveDetail;

@end
