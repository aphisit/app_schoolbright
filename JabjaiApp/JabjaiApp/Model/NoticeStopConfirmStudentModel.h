//
//  NoticeStopConfirmStudentModel.h
//  JabjaiApp
//
//  Created by mac on 1/18/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoticeStopConfirmStudentModel : NSObject

@property (strong , nonatomic) NSString *senderLeaveName;
@property (strong , nonatomic) NSString *senderLeaveClass;
@property (strong , nonatomic) NSString *senderLeaveTeacher;
@property (strong , nonatomic) NSString *senderLeaveCause;
@property (strong , nonatomic) NSDate *senderLeaveStartDate;
@property (strong , nonatomic) NSDate *senderLeaveLastDate;
@property (strong , nonatomic) NSString *senderLeaveDetail;

@end
