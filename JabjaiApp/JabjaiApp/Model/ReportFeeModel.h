//
//  ReportFeeModel.h
//  JabjaiApp
//
//  Created by mac on 4/18/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportFeeModel : NSObject

@property (nonatomic) int status;
@property (nonatomic) float countStudent;
@property (nonatomic) float amountPrice;
@property (nonatomic) float amountPercent;
@property (nonatomic) float paymentPrice;
@property (nonatomic) float paymentPercent;

@end
