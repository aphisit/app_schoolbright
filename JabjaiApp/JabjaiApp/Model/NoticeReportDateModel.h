//
//  NoticeReportDateModel.h
//  JabjaiApp
//
//  Created by mac on 12/18/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoticeReportDateModel : NSObject

@property (assign, nonatomic) long long letterID;
@property (strong , nonatomic) NSDate *leaveDate;
@property (strong , nonatomic) NSString *leaveCause;

@property (strong , nonatomic) NSString *status;

@end
