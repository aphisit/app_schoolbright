//
//  LevelFeeModel.h
//  JabjaiApp
//
//  Created by mac on 3/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LevelFeeModel : NSObject

@property (nonatomic) long long levelId;
@property (nonatomic) NSString *levelName;

@end
