//
//  PersonDataModel.h
//  JabjaiApp
//
//  Created by mac on 3/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonDataModel : NSObject

@property (nonatomic) long long personId;

@property (nonatomic) NSInteger gender;

@property (nonatomic) NSString *personName;
@property (nonatomic) NSString *personImage;

@end
