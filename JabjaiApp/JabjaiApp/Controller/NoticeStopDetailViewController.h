//
//  NoticeStopDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 11/23/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "SNTimeOutImage.h"
#import "NoticeStopCollectionViewCell.h"
#import "NoticeStopViewController.h"
#import "NoticeStopConfirmStudentViewController.h"
#import "NoticeStopConfirmTeacherViewController.h"
#import "NoticeStopConfirmViewController.h"
#import "TableListDialog.h"
#import "SlideMenuController.h"
#import "PhotoAlbumLibary.h"

@interface NoticeStopDetailViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate, TableListDialogDelegate, SlideMenuControllerDelegate,PhotoAlbumLibaryDelegate, UICollectionViewDelegate, UICollectionViewDataSource>{
    BOOL checkAddress;
}

@property (nonatomic) NSString* leaveCause;
@property (nonatomic) NSString* noticeDetail;
@property (nonatomic) NSString* leaveFirstDate;
@property (nonatomic) NSString* leaveLastDate;
@property (nonatomic) NSString* address;
@property (nonatomic) NSString* province;
@property (nonatomic) NSString* district;
@property (nonatomic) NSString* subDistrict;
@property (nonatomic) NSString* phoneNumber;
@property (nonatomic) NSDate *leaveStartdate;
@property (nonatomic) NSDate *leaveEndDate;
@property (nonatomic, assign) int fillDetailTag;



@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAddressLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerHouseNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerProvinceLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAmphurLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDistrictLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPhoneNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;



@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextView *fillDetail;
@property (weak, nonatomic) IBOutlet UIView *shadowImageView;
@property (weak, nonatomic) IBOutlet UIView *shadowDetailView;
@property (weak, nonatomic) IBOutlet UIView *shadowAddress;


@property (weak, nonatomic) IBOutlet UITextField *fillAddress;
@property (weak, nonatomic) IBOutlet UITextField *fillProvince;
@property (weak, nonatomic) IBOutlet UITextField *fillDistrict;
@property (weak, nonatomic) IBOutlet UITextField *fillSubDistrict;
@property (weak, nonatomic) IBOutlet UITextField *fillPhone;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonBottomConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContentConstraint;


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

- (IBAction)selectImage:(id)sender;

- (IBAction)moveBack:(id)sender;
- (IBAction)nextPage:(id)sender;
- (IBAction)checkRadio:(id)sender;


- (IBAction)dismissKeyboard:(id)sender;



@end
