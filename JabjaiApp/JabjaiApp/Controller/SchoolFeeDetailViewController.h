//
//  SchoolFeeDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 3/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"

#import "SWRevealViewController.h"

@interface SchoolFeeDetailViewController : UIViewController <SlideMenuControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (assign, nonatomic) long long invoiceID;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMenuConstraint;

@property (weak, nonatomic) IBOutlet UILabel *dateIssueLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueLabel;

@property (weak, nonatomic) IBOutlet UILabel *openBracketLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeBracketLabel;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (weak, nonatomic) IBOutlet UIImageView *schoolLogo;
@property (weak, nonatomic) IBOutlet UILabel *schoolThaiName;
@property (weak, nonatomic) IBOutlet UILabel *schoolEngName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *schoolViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *schoolLogoWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *schoolLogoHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *termLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *classLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *headnameLabel;
@property (weak, nonatomic) IBOutlet UILabel *headcodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headyearLabel;
@property (weak, nonatomic) IBOutlet UILabel *headtermLabel;
@property (weak, nonatomic) IBOutlet UILabel *headlevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *headclassLabel;
@property (weak, nonatomic) IBOutlet UILabel *headpriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *headbahtLabel;

@property (weak, nonatomic) IBOutlet UILabel *headList;

@property (weak, nonatomic) IBOutlet UIButton *buttonStatus;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightButtonConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topButtonConstraint;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;

- (IBAction)moveBack:(id)sender;

@end
