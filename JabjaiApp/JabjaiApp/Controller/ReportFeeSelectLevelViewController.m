//
//  ReportFeeSelectLevelViewController.m
//  JabjaiApp
//
//  Created by mac on 3/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportFeeSelectLevelViewController.h"

#import "ReportFeeViewController.h"
#import "ReportFeeSelectClassViewController.h"

#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"

#import "LevelFeeModel.h"
#import "LevelPersonTableViewCell.h"


@interface ReportFeeSelectLevelViewController (){
    
    NSMutableArray <LevelFeeModel *> *levelArray;
    
    NSInteger connectCounter;
    
    UIColor *grayColor;
}

@end

@implementation ReportFeeSelectLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    if (self.status == 0) {
//        self.headMenu.text = @"ชำระแล้ว";
//    }
//    else if (self.status == 1){
//        self.headMenu.text = @"ชำระบางส่วน";
//    }
//    else if (self.status == 2){
//        self.headMenu.text = @"ค้างชำระ";
//    }
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([LevelPersonTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    
    [self getSchollLevel];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(levelArray == nil) {
        return 0;
    }
    else {
        return levelArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LevelFeeModel *model = [levelArray objectAtIndex:indexPath.row];
    
    LevelPersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.levelLabel.text = model.levelName;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LevelFeeModel *model = [levelArray objectAtIndex:indexPath.row];
    
    ReportFeeSelectClassViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportFeeSelectClassStoryboard"];
    
    viewController.levelID = model.levelId;
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getSchollLevel{
    
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getStudentClassLevelWithSchoolId:schoolID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getSchollLevel];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchollLevel];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getSchollLevel];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                levelArray = [[NSMutableArray alloc] init];
                
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long levelID = [[dataDict objectForKey:@"ID"]longLongValue];
                    
                    NSMutableString *levelName;

                    if (![[dataDict objectForKey:@"Value"] isKindOfClass:[NSNull class]]) {
                        levelName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Value"]];
                    }
                    else{
                        levelName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) levelName);
                    
                    LevelFeeModel *model = [[LevelFeeModel alloc] init];
                    
                    model.levelId = levelID;
                    model.levelName = levelName;
                    
                    [levelArray addObject:model];
                    
                }

                [self.tableView reloadData];
            }
        }
        
    }];
    
}


- (IBAction)moveBack:(id)sender {
    
    ReportFeeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportFeeStoryboard"];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
