//
//  NoticeStopConfirmTeacherViewController.h
//  JabjaiApp
//
//  Created by mac on 11/28/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "SNTimeOutImage.h"
#import "NoticeStopConfirm2CollectionViewCell.h"
#import "SlideMenuController.h"
#import "NoticeImageCollectionViewCell.h"
#import "AlertDialog.h"

@interface NoticeStopConfirmTeacherViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,SlideMenuControllerDelegate,AlertDialogDelegate>

@property (nonatomic) NSString* noticeDetail;
@property (nonatomic) NSArray* imageNotice;
@property (nonatomic) NSString* leaveCause;
@property (nonatomic) NSString* setleaveCause;
@property (nonatomic) NSString* headTitleAddress;
@property (nonatomic) NSString* headSubDistrict;
@property (nonatomic) NSString* headDistrict;
@property (nonatomic) NSString* headProvince;
@property (nonatomic) NSString* headPhoneNumber;
@property (nonatomic) NSString* address;
@property (nonatomic) NSString* province;
@property (nonatomic) NSString* district;
@property (nonatomic) NSString* subDistrict;
@property (nonatomic) NSString* road;
@property (nonatomic) NSString* phoneNumber;
@property (nonatomic) NSDate *leaveStartdate;
@property (nonatomic) NSDate *leaveEndDate;
@property (nonatomic) NSString* leavingFirstDate;
@property (nonatomic) NSString* leavingLastDate;
@property (nonatomic) NSString* leaveFirstDate;
@property (nonatomic) NSString* setleaveFirstDate;
@property (nonatomic) NSString* leaveLastDate;
@property (nonatomic) NSString* setleaveLastDate;
@property(nonatomic, assign) int fillDetailTag;


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLeaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPositionLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerReasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDatefromLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDatetoLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAddressLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;
@property (weak, nonatomic) IBOutlet UILabel *headerImageuserlabel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *leaveTeacherName;
@property (weak, nonatomic) IBOutlet UILabel *position;
@property (weak, nonatomic) IBOutlet UILabel *leaveCauseitem;
@property (weak, nonatomic) IBOutlet UILabel *firstLeaveDate;
@property (weak, nonatomic) IBOutlet UILabel *lastLeaveDate;
@property (weak, nonatomic) IBOutlet UILabel *confirmDetail;
@property (weak, nonatomic) IBOutlet UILabel *headAddrressDetail;
@property (weak, nonatomic) IBOutlet UILabel *addressDetail;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCollectionConStraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)moveback:(id)sender;
- (IBAction)confirmPage:(id)sender;




@end
