//
//  NoticeInboxViewController.m
//  JabjaiApp
//
//  Created by mac on 12/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NoticeInboxViewController.h"
#import "NoticeInboxTableViewCell.h"
#import "NoticeInboxModel.h"
#import "SWRevealViewController.h"
#import "ProgressTableViewCell.h"
#import "NoticeInboxDetailViewController.h"
#import "Utils.h"
#import "Constant.h"
#import "APIURL.h"
#import "UserData.h"
#import "NoticeInboxStatusViewController.h"
#import "KxMenu.h"

static NSString *dataCellIdentifier = @"NoticeInboxCell";

@interface NoticeInboxViewController (){
    
    UIColor *grayColor;
    NSMutableDictionary<NSNumber *, NSArray<NoticeInboxModel *> *> *inboxSections;
    NSMutableArray<NoticeInboxModel *> *inboxMessage;
    NSInteger connectCounter;
    NSString *sortData;
    UIColor *acceptLeave;  // Mountain Meadow
    UIColor *declineLeave; // BitterSweet
    UIColor *waitLeave;    // Mariner
    UIColor *cancelLeave;  // HeliotRope
    
    UIColor *unreadBGColor;
    UIColor *readBGColor;
    
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) LIDetailLeaveViewControllerXIB *lIDetailLeaveViewControllerXIB;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation NoticeInboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_LEAVE_REQUEST",nil,[Utils getLanguage],nil);
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    inboxSections = [[NSMutableDictionary alloc] init];
    inboxMessage = [[NSMutableArray alloc] init];
    self.showNoShopLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_NO_REQUEST",nil,[Utils getLanguage],nil);
    self.showNoShopLabel.hidden = YES;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = grayColor;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NoticeInboxTableViewCell class]) bundle:nil] forCellReuseIdentifier:dataCellIdentifier];
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    unreadBGColor = [UIColor whiteColor];
    readBGColor = [UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1.0];
    acceptLeave = [UIColor colorWithRed:0.13 green:0.76 blue:0.46 alpha:1.0];
    declineLeave = [UIColor colorWithRed:1.00 green:0.48 blue:0.38 alpha:1.0];
    waitLeave = [UIColor colorWithRed:0.13 green:0.49 blue:0.76 alpha:1.0];
    cancelLeave = [UIColor colorWithRed:0.75 green:0.51 blue:1.00 alpha:1.0];
    
    [self.sortDataBtn addTarget:self action:@selector(showMenuSortData:) forControlEvents:UIControlEventTouchUpInside];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.heightMenuConstraint.constant = 120;
    }
    else{
        self.heightMenuConstraint.constant = 70;
    }
    [self getLeaveInboxWithPage:1 status:sortData];
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return inboxSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger page = section + 1;
    return  [[inboxSections objectForKey:@(page)] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NoticeInboxTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dataCellIdentifier forIndexPath:indexPath];
    NSInteger page = indexPath.section + 1;
    NoticeInboxModel *model = [[inboxSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    cell.leaveCause.text = model.leaveType;
    cell.leaveName.text = model.leaveName;
    cell.dateInbox.text = [Utils getThaiDateAcronymFormatWithDate:model.date];
    [cell.noticeStatusColor layoutIfNeeded];
    cell.noticeStatusColor.layer.cornerRadius = cell.noticeStatusColor.frame.size.width / 2;
    cell.noticeStatusColor.clipsToBounds = YES;
    cell.noticeStatusColor.layer.shouldRasterize = YES;
    if (model.status == 1) {
        cell.noticeStatusColor.backgroundColor = waitLeave;
    }
    else if (model.status == 2){
        cell.noticeStatusColor.backgroundColor = acceptLeave;
    }
    else if (model.status == 3){
        cell.noticeStatusColor.backgroundColor = declineLeave;
    }
    else {
        cell.noticeStatusColor.backgroundColor = cancelLeave;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    NSInteger page = indexPath.section + 1;
    NoticeInboxModel *model = [[inboxSections objectForKey:@(page)] objectAtIndex:indexPath.row];
    
    if (self.lIDetailLeaveViewControllerXIB != nil) {
        self.lIDetailLeaveViewControllerXIB = nil;
    }
    self.lIDetailLeaveViewControllerXIB = [[LIDetailLeaveViewControllerXIB alloc] init];
    self.lIDetailLeaveViewControllerXIB.delegate = self;
    [self.lIDetailLeaveViewControllerXIB showDetailLeaveViewController:self.view letterID:model.letterID];
//    NoticeInboxStatusViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeInboxStatusStoryboard"];
//        viewController.letterID = model.letterID;
//        viewController.page = 2;
//        [self.slideMenuController changeMainViewController:viewController close:YES];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath]  withRowAnimation:UITableViewRowAnimationFade];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return 130;
    }
    else{
        return 90;
    }
}

- (void)getLeaveInboxWithPage:(NSInteger)page status:(NSString*)status{
    
    [self showIndicator];
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getLeaveInboxWithPage:page userID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self stopIndicator];
        if (error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            if (connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getLeaveInboxWithPage:page status:status];
            }
            else{
                connectCounter = 0;
            }
        }
        
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if (error != nil) {
                NSLog(@"An error occured : #%@" , [error localizedDescription]);
                if (connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    NSLog(@"Error try to connect #%@" , [@(connectCounter) stringValue]);
                    [self getLeaveInboxWithPage:page status:status];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if (![returnedData isKindOfClass:[NSArray class]]){
                if (connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    NSLog(@"Error try to connect #%@" , [@(connectCounter) stringValue]);
                    [self getLeaveInboxWithPage:page status:status];
                }
                else{
                    connectCounter = 0;
                }
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                NSMutableArray *newDataArr = [[NSMutableArray alloc] init];
                NSString *userType;
                for (int i = 0; i<returnedArray.count; i++) {
                    NSDictionary *datadict = [returnedArray objectAtIndex:i];
                    int letterID = [[datadict objectForKey:@"letterId"] intValue];
                    int status = [[datadict objectForKey:@"status"] intValue];
                    NSMutableString *inboxDate = [[NSMutableString alloc] initWithString:[datadict objectForKey:@"letterSubmitDate"]];
                    NSMutableString *letterType;
                    if (![[datadict objectForKey:@"letterType"] isKindOfClass:[NSNull class]]) {
                        letterType = [[NSMutableString alloc] initWithString:[datadict objectForKey:@"letterType"]];
                    }
                    else {
                        letterType = [[NSMutableString alloc] initWithString:@""];
                    }
                    NSMutableString *letterSenderName;
                    if (![[datadict objectForKey:@"senderName"] isKindOfClass:[NSNull class]]) {
                        letterSenderName = [[NSMutableString alloc] initWithString:[datadict objectForKey:@"senderName"]];
                    }
                    else{
                        letterSenderName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[datadict objectForKey:@"userType"] isKindOfClass:[NSNull class]]) {
                        userType = [datadict objectForKey:@"userType"];
                    }
                    else{
                        userType = @"";
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)letterSenderName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)inboxDate);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef)letterType);
                  
                    NSDate *inboxLeaveDate;
                    NSRange dotRange = [inboxDate rangeOfString:@"."];
                    if (dotRange.length != 0) {
                        inboxLeaveDate = [formatter dateFromString:inboxDate];
                    }
                    else{
                        inboxLeaveDate = [formatter2 dateFromString:inboxDate];
                    }
                    NoticeInboxModel *model = [[NoticeInboxModel alloc] init];
                    model.letterID = letterID;
//                    model.userID = 883;
                    model.leaveName = letterSenderName;
                    model.leaveType = letterType;
                    model.status = status;
                    model.date = inboxLeaveDate;
                    model.userType = userType;
                    [newDataArr addObject:model];
                }
                [inboxMessage removeAllObjects];
                inboxMessage = [[NSMutableArray alloc] initWithArray:newDataArr];
                [inboxSections setObject:newDataArr forKey:@(page)];
                if (returnedArray.count == 0) {
                    self.showNoShopLabel.hidden = NO;
                }else{
                    self.showNoShopLabel.hidden = YES;
                }
                [self.tableView reloadData];
            }
        }
    }];
}

- (void)fetchMoreData{
    
    if (inboxMessage.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *lastRowIndexPath = [visiblePaths lastObject];
        NSInteger numberOfSections = [self.tableView numberOfSections];
        NSInteger lastRowSection = [lastRowIndexPath section];
        NSInteger lastRowRow = [lastRowIndexPath row];
        NSInteger numberOfRowInsection = [self.tableView numberOfRowsInSection:lastRowSection];
        
        if (lastRowSection == numberOfSections -1 && lastRowRow == numberOfRowInsection - 1) {
            if (inboxMessage.count % 20 == 0) {
                NSInteger nextpage = lastRowSection + 2;
                [self getLeaveInboxWithPage:nextpage status:sortData];
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self fetchMoreData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self fetchMoreData];
}


- (void)showIndicator{
    if (![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator{
    [self.indicator stopAnimating];

}

- (void)showMenuSortData:(UIButton *)sender{
    
    NSArray *menuItems =
    @[
      [KxMenuItem menuItem:@"ทั้งหมด"
                     image:nil
                    target:self
                    action:@selector(sortAll:)],
      
      [KxMenuItem menuItem:@"อนุมัติ"
                     image:nil
                    target:self
                    action:@selector(sortConfirm:)],
      
      [KxMenuItem menuItem:@"ปฏิเสธ"
                     image:nil
                    target:self
                    action:@selector(sortReject:)],
      
      [KxMenuItem menuItem:@"รอผล"
                     image:nil
                    target:self
                    action:@selector(sortWaitForResults:)],
      
      [KxMenuItem menuItem:@"ยกเลิก"
                     image:nil
                    target:self
                    action:@selector(sortCancel:)]
      
      ];

    const CGFloat W = self.view.bounds.size.width;
    const CGFloat H = self.view.bounds.size.height;
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(W - 16, 18, 40, 40) menuItems:menuItems];
}

- (void) sortAll:(id)sender{
    sortData = nil; //status read
    double delayInsecond = 1;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        NSLog(@"All");
    });
}
- (void) sortConfirm:(id)sender{
    sortData = @"";
    double delayInsecond = 5;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        NSLog(@"Confirm");
    });
}
- (void) sortReject:(id)sender{
    sortData = @"";
    double delayInsecond = 1;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        NSLog(@"Reject");
    });
}
- (void)sortWaitForResults:(id)sender{
    sortData = @"";
    double delayInsecond = 1;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        NSLog(@"WaitForResults");
    });
}
- (void)sortCancel:(id)sender{
    sortData = @"";
    double delayInsecond = 1;
    dispatch_time_t poptime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInsecond * NSEC_PER_SEC));
    dispatch_after(poptime, dispatch_get_main_queue(), ^{
        
    });
}

- (IBAction)openDrawer:(id)sender {
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
//    [self.revealViewController revealToggle:self.revealViewController];
}
@end
