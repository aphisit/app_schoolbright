//
//  NoticeReportDateMessageDetailViewController.m
//  JabjaiApp
//
//  Created by mac on 1/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "NoticeReportDateMessageDetailViewController.h"
#import "NoticeReportDateMessageDetailModel.h"
#import "UserData.h"
#import "APIURL.h"
#import "Utils.h"

#import "NoticeReportDateViewController.h"
#import "NoticeReportDateMessageViewController.h"
#import "ReportLeaveViewController.h"

#import "MessageInboxScrollableTextDialogCollectionViewCell.h"

#import "AlertDialog.h"

#import <QBImagePickerController/QBImagePickerController.h>
#import "AppDelegate.h"
#import "ZMImageSliderViewController.h"

#import "NoticeReportDateMessageViewController.h"

#import "ERProgressHud.h"
#import "KxMenu.h"

@interface NoticeReportDateMessageDetailViewController (){
    
    NoticeReportDateMessageDetailModel *noticeReportDateMessageDetailModel;
    
    NSMutableArray<NoticeReportDateMessageDetailModel *> *leaveReportArray;
    
    NSInteger connectCounter;
    
    NSArray *returnedArray;
    
    NSMutableArray *imageArray;
    
    NSString *address, *tumbon, *aumphur, *province, *phone;
    NSString *headtumbon, *headaumphur, *headprovince, *headphone;
    
    NSString *urlFile;
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;

@end

@implementation NoticeReportDateMessageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    self.detailLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.detailLabel.numberOfLines = 0;
    
    self.addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.addressLabel.numberOfLines = 0;
    
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.heightContentConstraint.constant = 1000;
        self.heightMenuConstraint.constant = 120;
    }
    else{
        self.heightContentConstraint.constant = 750;
        self.heightMenuConstraint.constant = 70;
    }
    
    imageArray = [[NSMutableArray alloc] init];
    

    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    self.moreButton.hidden = YES;
    self.moreButton.enabled = false;
    [self.moreButton addTarget:self action:@selector(showRightMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [self clearDisplay];
    [self getLetterDetailWithLetterID:self.letterID];

    
}

- (void)viewDidLayoutSubviews{
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getLetterDetailWithLetterID:(long long)letterID{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getLetterLeaveDetailWithLetterID:letterID userID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [[ERProgressHud sharedInstance] showDarkBackgroundViewWithTitle:@"กรุณารอสักครู่...."];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getLetterDetailWithLetterID:letterID];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[ERProgressHud sharedInstance] hide];
            });
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getLetterDetailWithLetterID:letterID];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getLetterDetailWithLetterID:letterID];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if (leaveReportArray != nil) {
                    [leaveReportArray removeAllObjects];
                    leaveReportArray = nil;
                }
                
                leaveReportArray = [[NSMutableArray alloc] init];

                imageArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *leaveName, *leavePosition, *leaveCause, *leaveStart, *leaveEnd, *leaveDetail, *leaveApprove;
                    
                    //leave name
                    if (![[dataDict objectForKey:@"senderName"] isKindOfClass:[NSNull class]]) {
                        leaveName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderName"]];
                    }
                    else{
                        leaveName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave position
                    if (![[dataDict objectForKey:@"senderJob"] isKindOfClass:[NSNull class]]) {
                        leavePosition = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderJob"]];
                    }
                    else{
                        leavePosition = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave cause
                    if (![[dataDict objectForKey:@"letterType"] isKindOfClass:[NSNull class]]) {
                        leaveCause = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"letterType"]];
                    }
                    else{
                        leaveCause = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave start
                    if (![[dataDict objectForKey:@"leaveStart"] isKindOfClass:[NSNull class]]) {
                        leaveStart = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"leaveStart"]];
                    }
                    else{
                        leaveStart = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave end
                    if (![[dataDict objectForKey:@"leaveEnd"] isKindOfClass:[NSNull class]]) {
                        leaveEnd = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"leaveEnd"]];
                    }
                    else{
                        leaveEnd = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave approve
                    if (![[dataDict objectForKey:@"sendApproveDate"] isKindOfClass:[NSNull class]]) {
                        leaveApprove = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sendApproveDate"]];
                    }
                    else{
                        leaveApprove = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave detail
                    if (![[dataDict objectForKey:@"letterDescription"] isKindOfClass:[NSNull class]]) {
                        leaveDetail = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"letterDescription"]];
                    }
                    else{
                        leaveDetail = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    int status = [[dataDict objectForKey:@"status"] intValue];
                    
                    NSMutableString *season;
                    
                    if (![[dataDict objectForKey:@"Season"] isKindOfClass:[NSNull class]]) {
                        season = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Season"]];
                    }
                    else{
                        season = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableString *address, *tumbon, *aumphur, *province, *phone;
                    
                    if (![[dataDict objectForKey:@"Aumpher"] isKindOfClass:[NSNull class]]) {
                        aumphur = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Aumpher"]];
                    }
                    else{
                        aumphur = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Tumbon"] isKindOfClass:[NSNull class]]) {
                        tumbon = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Tumbon"]];
                    }
                    else{
                        tumbon = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"HomeNumber"] isKindOfClass:[NSNull class]]) {
                        address = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"HomeNumber"]];
                    }
                    else{
                        address = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Province"] isKindOfClass:[NSNull class]]) {
                        province = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Province"]];
                    }
                    else{
                        province = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Phone"] isKindOfClass:[NSNull class]]) {
                        phone = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Phone"]];
                    }
                    else{
                        phone = [[NSMutableString alloc] initWithString:@""];
                    }
     
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leavePosition);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveCause);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveStart);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveEnd);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveDetail);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveApprove);
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) address);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) tumbon);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) aumphur);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) province);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) phone);
                
                    NSDate *leaveStartDate;
                    leaveStartDate = [formatter2 dateFromString:leaveStart];
                    
                    NSDate *leaveEndDate;
                    leaveEndDate = [formatter2 dateFromString:leaveEnd];
                    
                    NSRange dotRange = [leaveApprove rangeOfString:@"."];
                    
                    NSDate *leaveApproveDate;
                    
                    if(dotRange.length != 0) {
                        leaveApproveDate = [formatter dateFromString:leaveApprove];
                    }
                    else {
                        leaveApproveDate = [formatter2 dateFromString:leaveApprove];
                    }
                    
                    NoticeReportDateMessageDetailModel *model = [[NoticeReportDateMessageDetailModel alloc] init];
                    
                    model.leaveName = leaveName;
                    model.leavePosition = leavePosition;
                    model.leaveCause = leaveCause;
                    model.leaveStart = leaveStartDate;
                    model.leaveEnd = leaveEndDate;
                    model.leaveDetail = leaveDetail;
                    
//                    model.file = fileImage;
                    
                    model.status = status;
                    
                    model.address = address;
                    model.tumbon = tumbon;
                    model.aumphur = aumphur;
                    model.province = province;
                    model.phone = phone;
                    
                    model.season = season;
                    
                    model.leaveDateApprove = leaveApproveDate;
                    
                    noticeReportDateMessageDetailModel = model;
                    [self performData];
                    
                    [leaveReportArray addObject:model];
                    
                    [self.collectionView reloadData];
                    
                }
               
            }
        }
        
    }];
    
}

- (void)performData{
    
    if (noticeReportDateMessageDetailModel != nil) {
        self.leaveNameLabel.text = noticeReportDateMessageDetailModel.leaveName;
        self.positionLabel.text = noticeReportDateMessageDetailModel.leavePosition;
        self.causeLabel.text = noticeReportDateMessageDetailModel.leaveCause;
//        self.startDateLabel.text = [self.dateFormatter stringFromDate:noticeReportDateMessageDetailModel.leaveStart];
        
        self.startDateLabel.text = [Utils getThaiDateAcronymFormatWithDate:noticeReportDateMessageDetailModel.leaveStart];
        
//        self.endDateLabel.text = [self.dateFormatter stringFromDate:noticeReportDateMessageDetailModel.leaveEnd];
        self.detailLabel.text = noticeReportDateMessageDetailModel.leaveDetail;
        
        if (noticeReportDateMessageDetailModel.status == 1) {
            self.statusLabel.text = @"รออนุญาต";
            self.statusLabel.backgroundColor = [UIColor colorWithRed:0.26 green:0.78 blue:0.96 alpha:1.0];
        }
        else if (noticeReportDateMessageDetailModel.status == 2) {
            self.statusLabel.text = @"อนุญาต";
            self.statusLabel.backgroundColor = [UIColor colorWithRed:0.13 green:0.86 blue:0.55 alpha:1.0];
        }
        
        else if (noticeReportDateMessageDetailModel.status == 3) {
            self.statusLabel.text = @"ไม่อนุญาต";
            self.statusLabel.backgroundColor = [UIColor colorWithRed:1.00 green:0.32 blue:0.32 alpha:1.0];
        } else {
            self.statusLabel.text = @"ไม่ทราบสถานะ";
        }
        
        address = noticeReportDateMessageDetailModel.address;
        tumbon = noticeReportDateMessageDetailModel.tumbon;
        aumphur = noticeReportDateMessageDetailModel.aumphur;
        province = noticeReportDateMessageDetailModel.province;
        phone = noticeReportDateMessageDetailModel.phone;
        
        if ([address isEqual:@""] || [tumbon isEqual:@""] || [aumphur isEqual:@""] || [province isEqual:@""] || [phone isEqual:@""]) {
            
//            self.headAddressLabel.text = @"";
            
            self.headAddressLabel.hidden = YES;
            
            headtumbon = @"";
            headaumphur = @"";
            headprovince = @"";
            headphone = @"";
            
            self.addressLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@" , address, headtumbon, tumbon, headaumphur, aumphur, headprovince, province, headphone, phone];
            
            self.heightContentConstraint.constant = 500;
            
        }
        else{
            
            headtumbon = @"ตำบล";
            headaumphur = @"อำเภอ";
            headprovince = @"จังหวัด";
            headphone = @"เบอร์โทรศัพท์";
            
            self.addressLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@" , address, headtumbon, tumbon, headaumphur, aumphur, headprovince, province, headphone, phone];
            
        }
        
        if ([noticeReportDateMessageDetailModel.season isEqual:@""]) {
            self.headChangeLabel.text = @"ลาถึงวันที่";
            self.endDateLabel.text = [Utils getThaiDateAcronymFormatWithDate:noticeReportDateMessageDetailModel.leaveEnd];
        }
        else{
            if ([noticeReportDateMessageDetailModel.season isEqual:@"0"]) {
                self.headChangeLabel.text = @"ช่วงเวลา";
                self.endDateLabel.text = @"ลาครึ่งวันเช้า";
            }
            else if ([noticeReportDateMessageDetailModel.season isEqual:@"1"]) {
                self.headChangeLabel.text = @"ช่วงเวลา";
                self.endDateLabel.text = @"ลาครึ่งวันบ่าย";
            }
            else if ([noticeReportDateMessageDetailModel.season isEqual:@"-1"]) {
                self.headChangeLabel.text = @"ลาถึงวันที่";
                self.endDateLabel.text = [Utils getThaiDateAcronymFormatWithDate:noticeReportDateMessageDetailModel.leaveEnd];
            }
        }
        
        if (noticeReportDateMessageDetailModel.leaveDateApprove == nil) {
            self.approveLabel.text = @"รออนุมัติ";
        }
        else{
            self.approveLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateAcronymFormatWithDate:noticeReportDateMessageDetailModel.leaveDateApprove] , [self.timeFormatter stringFromDate:noticeReportDateMessageDetailModel.leaveDateApprove]];
        }
        
//        imageArray = noticeReportDateMessageDetailModel.file;
        
    }
    else{
        
        [self clearDisplay];
    }
    
}

- (void)clearDisplay {
    
}

- (IBAction)moveBack:(id)sender {
    
    if (self.page == 1) {
        NoticeReportDateViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeReportStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (self.page == 2){
        NoticeReportDateMessageViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeReportDateMessageStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (self.page == 3){
        ReportLeaveViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportLeaveStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MessageInboxScrollableTextDialogCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSData *imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [imageArray objectAtIndex:indexPath.row]]];
    cell.imageNews.tag = indexPath.row;
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageWithData:imageData];
    [cell.imageNews setBackgroundImage:imageView.image forState:UIControlStateNormal];
    [cell.imageNews addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:   (UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(60, 100);
}

- (void)okButtonTapped:(UIButton *)sender {
    NSLog(@"index = %d",sender.tag);
    ZMImageSliderViewController*controller = [[ZMImageSliderViewController alloc] initWithOptions:sender.tag imageUrls:imageArray];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)showRightMenu:(UIButton *)sender{
    UIImage *unreadImage = [UIImage imageNamed:@"sort_with_unread"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [unreadImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *unreadImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    NSArray *menuItems =
    @[

      [KxMenuItem menuItem:@"ยกเลิกการลา"
                     image:unreadImage2
                    target:self
                    action:@selector(cancelLeave:)],
      ];
    
    const CGFloat W = self.view.bounds.size.width;
    const CGFloat H = self.view.bounds.size.height;
    
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(W - 16, 18, 40, 40) menuItems:menuItems];
    
}

- (void) cancelLeave:(id)sender
{
    
}

@end
