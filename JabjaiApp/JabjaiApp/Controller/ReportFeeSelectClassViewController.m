//
//  ReportFeeSelectClassViewController.m
//  JabjaiApp
//
//  Created by mac on 3/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportFeeSelectClassViewController.h"

#import "ReportFeeSelectLevelViewController.h"

#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"

#import "ClassFeeModel.h"
#import "ClassPersonTableViewCell.h"

@interface ReportFeeSelectClassViewController (){
    NSMutableArray <ClassFeeModel *> *classArray;
    
    NSInteger connectCounter;
    
    UIColor *grayColor;
}

@end

@implementation ReportFeeSelectClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ClassPersonTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    
    [self getClassLevelWith:self.levelID];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(classArray == nil) {
        return 0;
    }
    else {
        return classArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ClassFeeModel *model = [classArray objectAtIndex:indexPath.row];
    
    ClassPersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.classLabel.text = model.classLevelName;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ClassFeeModel *model = [classArray objectAtIndex:indexPath.row];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70;
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)getClassLevelWith:(long long)classID{
    
    long long schoolID = [UserData getSchoolId];
    NSString *URLString = [APIURL getSchoolClassroomWithSchoolId:schoolID classLevelId:classID];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getClassLevelWith:classID];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getClassLevelWith:classID];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getClassLevelWith:classID];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                classArray = [[NSMutableArray alloc] init];
                
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long classID = [[dataDict objectForKey:@"ID"]longLongValue];
                    
                    NSMutableString *className;
                    
                    if (![[dataDict objectForKey:@"Value"] isKindOfClass:[NSNull class]]) {
                        className = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Value"]];
                    }
                    else{
                        className = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) className);
                    
                    ClassFeeModel *model = [[ClassFeeModel alloc] init];
                    
                    model.classLevelId = classID;
                    model.classLevelName = className;
                    
                    [classArray addObject:model];
                    
                }
                
                [self.tableView reloadData];
            }
        }
        
    }];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)moveBack:(id)sender {
    
    ReportFeeSelectLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportFeeSelectLevelStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}
@end
