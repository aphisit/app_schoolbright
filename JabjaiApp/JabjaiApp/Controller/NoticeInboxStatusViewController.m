//
//  NoticeInboxStatusViewController.m
//  JabjaiApp
//
//  Created by mac on 5/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "NoticeInboxStatusViewController.h"

#import "NoticeInboxViewController.h"
#import "NoticeInboxDetailModel.h"
#import "APIURL.h"
#import "Utils.h"
#import "UserData.h"
#import "UserInfoViewController.h"
#import "MessageInboxScrollableTextDialogCollectionViewCell.h"
#import "AlertDialog.h"
#import "AlertDialogConfirm.h"
#import <QBImagePickerController/QBImagePickerController.h>
#import "AppDelegate.h"
#import "ZMImageSliderViewController.h"
#import "ERProgressHud.h"
#import "EZYGradientView.h"
#import "KxMenu.h"

#import "UserInfoViewController.h"

@interface NoticeInboxStatusViewController (){
    
    NoticeInboxDetailModel *noticeInboxDetailModel;
    
    NSMutableArray<NoticeInboxDetailModel *> *noticeLeaveArray;
    NSInteger connectCounter;
    NSInteger approve;
    AlertDialogConfirm *alertDialog;
    NSString *address, *tumbon, *aumphur, *province, *phone;
    NSString *headtumbon, *headaumphur, *headprovince, *headphone;
    NSArray *returnedArray;
    NSMutableArray *imageArray;
    NSString *urlFile;
    CAGradientLayer *greenGradient ,*redGradient ,*blueGradient,*purpleGradient;
    UIColor *jungleGreen, *downy, *salmon, *macaroniAndCheese, *mariner, *malibu, *heliotRope ,*melrose;
    UIColor *bitterSweet;
    CGFloat screenWidth;
    
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@end

@implementation NoticeInboxStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.leaveInboxDetailAddress.lineBreakMode = NSLineBreakByWordWrapping;
    self.leaveInboxDetailAddress.numberOfLines = 0;
    
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    imageArray = [[NSMutableArray alloc] init];
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;

    screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {

        if (screenWidth > 320.0f) {
            self.heightStatusConstraint.constant = 70;
            self.heightButtonMenuConstraint.constant = 70;
        }
        else{
            self.heightStatusConstraint.constant = 60;
            self.heightButtonMenuConstraint.constant = 60;
        }

    }
    else{
        self.heightStatusConstraint.constant = 120;
        self.heightButtonMenuConstraint.constant = 120;
    }

    // Color
    jungleGreen = [UIColor colorWithRed:0.18 green:0.76 blue:0.47 alpha:1.0]; //Accept
    downy = [UIColor colorWithRed:0.41 green:0.83 blue:0.63 alpha:1.0];
    salmon = [UIColor colorWithRed:0.99 green:0.56 blue:0.44 alpha:1.0]; // Decline
    macaroniAndCheese = [UIColor colorWithRed:0.99 green:0.76 blue:0.52 alpha:1.0];
    mariner = [UIColor colorWithRed:0.16 green:0.49 blue:0.75 alpha:1.0];  // Wait
    malibu = [UIColor colorWithRed:0.31 green:0.69 blue:0.98 alpha:1.0];
    heliotRope = [UIColor colorWithRed:0.56 green:0.40 blue:0.98 alpha:1.0]; // Cancel
    melrose = [UIColor colorWithRed:0.69 green:0.57 blue:0.99 alpha:1.0];

    // 09/05/2561
    bitterSweet = [UIColor colorWithRed:0.99 green:0.48 blue:0.40 alpha:1.0];
    
    EZYGradientView *gradientView = [[EZYGradientView alloc] init];
    //[self.headMenuBar layoutIfNeeded];
    gradientView.frame = self.view.frame;
    gradientView.firstColor = bitterSweet;
    gradientView.secondColor = macaroniAndCheese;
    gradientView.angleº = 90;
    gradientView.colorRatio = 0.5;
    gradientView.fadeIntensity = 1;
    gradientView.isBlur = YES;
    gradientView.blurOpacity = 0.5;
    [self.headMenuBar insertSubview:gradientView atIndex:0];
    
   // [self doSetLayout];
    self.moreButton.hidden = YES;
    self.moreButton.enabled = false;
    [self.moreButton addTarget:self action:@selector(showRightMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [self clearDisplay];
    [self getLetterConfirmDetailWithLetterID:self.letterID];
    [self setLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews{
    
    [self doDesignLayout];
}

- (void) doDesignLayout{
    
    CAGradientLayer  *headerGradient,*acceptGradient,*cancelGradient;
    
    
    //set color header
    [self.headerView layoutIfNeeded];
    headerGradient = [Utils getGradientColorHeader];
    headerGradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:headerGradient atIndex:0];
    
    [self.acceptBackGround layoutIfNeeded];
    acceptGradient = [Utils getGradientColorStatus:@"green"];
    acceptGradient.frame = self.acceptBackGround.bounds;
    [self.acceptBackGround.layer insertSublayer:acceptGradient atIndex:0];
    
    [self.declineBackGround layoutIfNeeded];
    cancelGradient = [Utils getGradientColorStatus:@"red"];
    cancelGradient.frame = self.declineBackGround.bounds;
    [self.declineBackGround.layer insertSublayer:cancelGradient atIndex:0];
    
    //set color stutus
    [self.statusBackGround layoutIfNeeded];
    
    greenGradient = [Utils getGradientColorStatus:@"green"];
    greenGradient.frame = self.statusBackGround.bounds;
    
    redGradient = [Utils getGradientColorHeader];
    redGradient.frame = self.statusBackGround.bounds;
    
    blueGradient = [Utils getGradientColorStatus:@"blue"];
    blueGradient.frame = self.statusBackGround.bounds;
    
    purpleGradient = [Utils getGradientColorStatus:@"purple"];
    purpleGradient.frame = self.statusBackGround.bounds;
}

- (void) setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_LEAVE_REQUEST",nil,[Utils getLanguage],nil);
    self.headerSenderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_SENDER",nil,[Utils getLanguage],nil);
    self.headerPositionLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_POSITION",nil,[Utils getLanguage],nil);
    self.headerReasonLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_REASON",nil,[Utils getLanguage],nil);
    self.headerStarDayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_START_DAY",nil,[Utils getLanguage],nil);
    self.headerEndDayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_END_DAY",nil,[Utils getLanguage],nil);
    self.headerSubmitDateLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_SUBMITDATE",nil,[Utils getLanguage],nil);
    self.headerDetailLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_DETAIL",nil,[Utils getLanguage],nil);
    self.headerAddressLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_ADDRESS",nil,[Utils getLanguage],nil);
    self.headerAttactImageLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_ATTACT_IMAGE",nil,[Utils getLanguage],nil);
    [self.acceptButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_LEAVE_REPORT_ALLOW",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    [self.declineButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_LEAVE_REPORT_NOT_ALLOW",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
}

- (void)getLetterConfirmDetailWithLetterID:(long long)letterID{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getLetterLeaveDetailWithLetterID:letterID userID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    NSLog(@"%@", URLString);
    
    [[ERProgressHud sharedInstance] showDarkBackgroundViewWithTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LEAVE_REPORT_PLEASE_WAIT",nil,[Utils getLanguage],nil)];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getLetterConfirmDetailWithLetterID:letterID];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[ERProgressHud sharedInstance] hide];
            });
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getLetterConfirmDetailWithLetterID:letterID];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getLetterConfirmDetailWithLetterID:letterID];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if (noticeLeaveArray != nil) {
                    [noticeLeaveArray removeAllObjects];
                    noticeLeaveArray = nil;
                }
                
                noticeLeaveArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *leaveName, *leavePosition, *leaveCause, *leaveStart, *leaveEnd, *leaveDetail, *leaveRequest,*leaveClassRoom,*leaveStudentCode;
                    
                    //leave name
                    if (![[dataDict objectForKey:@"senderName"] isKindOfClass:[NSNull class]]) {
                        leaveName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderName"]];
                    }
                    else{
                        leaveName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave position
                    if (![[dataDict objectForKey:@"senderJob"] isKindOfClass:[NSNull class]]) {
                        leavePosition = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderJob"]];
                    }
                    else{
                        leavePosition = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave cause
                    if (![[dataDict objectForKey:@"letterType"] isKindOfClass:[NSNull class]]) {
                        leaveCause = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"letterType"]];
                    }
                    else{
                        leaveCause = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave start
                    if (![[dataDict objectForKey:@"leaveStart"] isKindOfClass:[NSNull class]]) {
                        leaveStart = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"leaveStart"]];
                    }
                    else{
                        leaveStart = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave end
                    if (![[dataDict objectForKey:@"leaveEnd"] isKindOfClass:[NSNull class]]) {
                        leaveEnd = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"leaveEnd"]];
                    }
                    else{
                        leaveEnd = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave request
                    if (![[dataDict objectForKey:@"sendRequestDate"] isKindOfClass:[NSNull class]]) {
                        leaveRequest = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sendRequestDate"]];
                    }
                    else{
                        leaveRequest = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave detail
                    if (![[dataDict objectForKey:@"letterDescription"] isKindOfClass:[NSNull class]]) {
                        leaveDetail = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"letterDescription"]];
                    }
                    else{
                        leaveDetail = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    int status = [[dataDict objectForKey:@"status"] intValue];
                    int pageStatus = [[dataDict objectForKey:@"pageStatus"] intValue];
                    
                    NSMutableString *season;
                    
                    if (![[dataDict objectForKey:@"Season"] isKindOfClass:[NSNull class]]) {
                        season = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Season"]];
                    }
                    else{
                        season = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave Classroom
                    if (![[dataDict objectForKey:@"senderClassroom"] isKindOfClass:[NSNull class]]) {
                        leaveClassRoom = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderClassroom"]];
                    }
                    else{
                        leaveClassRoom = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave StudentCode
                    if (![[dataDict objectForKey:@"Code"] isKindOfClass:[NSNull class]]) {
                        leaveStudentCode = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Code"]];
                    }
                    else{
                        leaveStudentCode = [[NSMutableString alloc] initWithString:@""];
                    }
         
                    NSMutableString *address, *tumbon, *aumphur, *province, *phone;
                    
                    if (![[dataDict objectForKey:@"Aumpher"] isKindOfClass:[NSNull class]]) {
                        aumphur = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Aumpher"]];
                    }
                    else{
                        aumphur = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Tumbon"] isKindOfClass:[NSNull class]]) {
                        tumbon = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Tumbon"]];
                    }
                    else{
                        tumbon = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"HomeNumber"] isKindOfClass:[NSNull class]]) {
                        address = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"HomeNumber"]];
                    }
                    else{
                        address = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Province"] isKindOfClass:[NSNull class]]) {
                        province = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Province"]];
                    }
                    else{
                        province = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Phone"] isKindOfClass:[NSNull class]]) {
                        phone = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Phone"]];
                    }
                    else{
                        phone = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSMutableArray *fileImage;
                    fileImage = [dataDict objectForKey:@"file"];
                    NSLog(@"%@", fileImage);
                    
                    if (fileImage.count == 0) {
                        self.heightCollectionViewConstraint.constant = 0;
                        self.heightContentConstraint.constant = 500;
                    }
                    
                    for (i=0; i < fileImage.count; i++) {
                        NSString *picture = fileImage[i];
                    
                        NSLog(@"%@", picture);
                    
                        urlFile = picture;
                    
                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leavePosition);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveCause);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveStart);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveEnd);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveDetail);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveRequest);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveStudentCode);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveClassRoom);
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) address);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) tumbon);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) aumphur);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) province);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) phone);
                    
                    NSDate *leaveStartDate;
                    leaveStartDate = [formatter2 dateFromString:leaveStart];
                    
                    NSDate *leaveEndDate;
                    leaveEndDate = [formatter2 dateFromString:leaveEnd];
                    
                    NSRange dotRange = [leaveRequest rangeOfString:@"."];
                    
                    NSDate *leaveRequestDate;
                    
                    if(dotRange.length != 0) {
                        leaveRequestDate = [formatter dateFromString:leaveRequest];
                    }
                    else {
                        leaveRequestDate = [formatter2 dateFromString:leaveRequest];
                    }
                    
                    NoticeInboxDetailModel *model = [[NoticeInboxDetailModel alloc] init];
                    
                    model.leaveName = leaveName;
                    model.leavePosition = leavePosition;
                    model.leaveCause = leaveCause;
                    model.leaveStart = leaveStartDate;
                    model.leaveEnd = leaveEndDate;
                    model.leaveDetail = leaveDetail;
                    model.leaveDateRequest = leaveRequestDate;
                    model.classRoom = leaveClassRoom;
                    model.studentCode = leaveStudentCode;
                    
                    model.address = address;
                    model.tumbon = tumbon;
                    model.aumphur = aumphur;
                    model.province = province;
                    model.phone = phone;
                    
                    model.file = fileImage;
                    
                    model.status = status;
                    model.season = season;
                    model.pageStatus = pageStatus;
                    
                    noticeInboxDetailModel = model;
                    [self performData];
                    
                    [noticeLeaveArray addObject:model];
                    
                    [self.collectionView reloadData];
                    
                }
                
            }
        }
        
    }];
    
}

- (void)performData{
    
    if (noticeInboxDetailModel != nil) {
        
        if ([noticeInboxDetailModel.classRoom isEqualToString:@""]) {
            self.iconClassroomImg.hidden = YES;
            self.headerClassroomLabel.hidden = YES;
            self.leaveInClassRoomLabel.hidden = YES;
            self.coronClassroomLabel.hidden = YES;
            self.pointClassroomConstraint.constant = -2;
        }
        
        
        self.leaveInboxDetailName.text = [NSString stringWithFormat:@"%@ %@",noticeInboxDetailModel.studentCode,noticeInboxDetailModel.leaveName];
        self.leaveInboxDetailPosiion.text = noticeInboxDetailModel.leavePosition;
        self.leaveInboxDetailCause.text = noticeInboxDetailModel.leaveCause;
        self.leaveInboxDetailFirstDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveStart];
        self.leaveInboxDetailRequestDate.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveDateRequest] , [self.timeFormatter stringFromDate:noticeInboxDetailModel.leaveDateRequest]];
        self.leaveDetailTextView.text = noticeInboxDetailModel.leaveDetail;
        self.leaveInClassRoomLabel.text = noticeInboxDetailModel.classRoom;
        
        address = noticeInboxDetailModel.address;
        tumbon = noticeInboxDetailModel.tumbon;
        aumphur = noticeInboxDetailModel.aumphur;
        province = noticeInboxDetailModel.province;
        phone = noticeInboxDetailModel.phone;
        
        //        if ([self.leaveInboxDetailAddress.text length] != 0) {
        
        if ([address isEqual:@""] || [tumbon isEqual:@""] || [aumphur isEqual:@""] || [province isEqual:@""] || [phone isEqual:@""]) {
            self.leaveInboxDetailAddress.text = @"-";
            self.heightContentConstraint.constant = 525;
        }
        else{
            
            headtumbon = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_SUBDISTRICT",nil,[Utils getLanguage],nil);
            headaumphur = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_DISTRICT",nil,[Utils getLanguage],nil);
            headprovince = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_PROVINCE",nil,[Utils getLanguage],nil);
            headphone = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_CONTACT_NUMBER",nil,[Utils getLanguage],nil);
            
            self.leaveInboxDetailAddress.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@" , address, headtumbon, tumbon, headaumphur, aumphur, headprovince, province, headphone, phone];
            
        }
        
        if ([noticeInboxDetailModel.season isEqual:@""]) {
            self.headerEndDayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_END_DAY",nil,[Utils getLanguage],nil);
            self.leaveInboxDetailLastDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveEnd];
        }
        else{
            if ([noticeInboxDetailModel.season isEqual:@"0"]) {
                self.headerEndDayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_TIME_PERIOD",nil,[Utils getLanguage],nil);
                self.leaveInboxDetailLastDate.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_HALF_MORNING",nil,[Utils getLanguage],nil);
            }
            else if ([noticeInboxDetailModel.season isEqual:@"1"]) {
                self.headerEndDayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_TIME_PERIOD",nil,[Utils getLanguage],nil);
                self.leaveInboxDetailLastDate.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_HALF_AFTERNOON",nil,[Utils getLanguage],nil);
            }
            else if ([noticeInboxDetailModel.season isEqual:@"-1"]) {
                self.headerEndDayLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_END_DAY",nil,[Utils getLanguage],nil);
                self.leaveInboxDetailLastDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveEnd];
            }
            else {
                self.leaveInboxDetailLastDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveEnd];
            }
        }
        if (noticeInboxDetailModel.file.count == 0) {
            self.collectionView.hidden = YES;
        }else{
            self.collectionView.hidden = NO;
        }
        imageArray = noticeInboxDetailModel.file;
        [self.statusBackGround layoutIfNeeded];

        switch (noticeInboxDetailModel.pageStatus) {
            case 0:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVALTREE",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineLeave:) forControlEvents:UIControlEventTouchUpInside];
                //[self.statusBackGround insertSubview:blueGradient atIndex:0];
                [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
            break;
            case 1:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVALTWO",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineLeave:) forControlEvents:UIControlEventTouchUpInside];
               [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
                break;
            case 2:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVALONE",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
                break;
            case 3:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVAL",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
//
                self.statusContent.hidden = YES;
                [self.statusBackGround.layer insertSublayer:greenGradient atIndex:0];
                break;
            case 4:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_REJECRED",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.statusContent.hidden = YES;
                [self.statusBackGround.layer insertSublayer:redGradient atIndex:0];
                break;
            case 5:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_WAIT_APPROVAL",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.statusContent.hidden = YES;
               [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
                break;
            case 6:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_TREE_CANCEL",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.statusBackGround.layer insertSublayer:purpleGradient atIndex:0];
                break;
            case 7:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_TWO_CANCEL",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.statusBackGround.layer insertSublayer:purpleGradient atIndex:0];
                break;
            case 8:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_ONE_CANCEL",nil,[Utils getLanguage],nil);
                [self.acceptButton addTarget:self action:@selector(acceptCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.declineButton addTarget:self action:@selector(declineCancelLeave:) forControlEvents:UIControlEventTouchUpInside];
                [self.statusBackGround.layer insertSublayer:purpleGradient atIndex:0];
                break;
            case 9:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_CANCEL_REQUEST",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.statusContent.hidden = YES;
                [self.statusBackGround.layer insertSublayer:purpleGradient atIndex:0];
                break;
            case 10:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVALTWO",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.statusContent.hidden = YES;
               [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
                break;
            case 11:
                self.statusLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REPORT_APPROVALONE",nil,[Utils getLanguage],nil);
                self.heightButtonMenuConstraint.constant = 0;
                self.statusContent.hidden = YES;
                [self.statusBackGround.layer insertSublayer:blueGradient atIndex:0];
                break;
            default:
                break;
        }
        
        
        
        
    }
    else{
        
        [self clearDisplay];
    }
    
}

- (void)clearDisplay {
    
    self.leaveInboxDetailName.text = @"";
    self.leaveInboxDetailPosiion.text = @"";
    self.leaveInboxDetailCause.text = @"";
    self.leaveInboxDetailFirstDate.text = @"";
    self.leaveInboxDetailLastDate.text = @"";
    self.leaveInboxDetailRequestDate.text = @"";
    self.leaveDetailTextView.text = @"";

}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    
    return imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MessageInboxScrollableTextDialogCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSData *imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [imageArray objectAtIndex:indexPath.row]]];
    cell.imageNews.tag = indexPath.row;
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageWithData:imageData];
    
    [cell.imageNews setBackgroundImage:imageView.image forState:UIControlStateNormal];
    [cell.imageNews addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:   (UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(60, 100);
}

- (void)okButtonTapped:(UIButton *)sender {
    
    NSLog(@"index = %d",sender.tag);
    ZMImageSliderViewController*controller = [[ZMImageSliderViewController alloc] initWithOptions:sender.tag imageUrls:imageArray];
    [self presentViewController:controller animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)acceptLeave:(UIButton *)sender{
    
    approve = 1;
    [self getRequestLetterWithLetterID:self.letterID approve:approve];
    
}

- (void)declineLeave:(UIButton *)sender{
    
    approve = 0;
    [self getRequestLetterWithLetterID:self.letterID approve:approve];
    
}

- (void)acceptCancelLeave:(UIButton *)sender{
    
    approve = 3;
    [self getRequestLetterWithLetterID:self.letterID approve:approve];
    
}

- (void)declineCancelLeave:(UIButton *)sender{
    
    approve = 4;
    [self getRequestLetterWithLetterID:self.letterID approve:approve];
    
}

- (void)getRequestLetterWithLetterID:(long long)letterID approve:(NSInteger)approve{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getRequestLetterWithLetterID:letterID userID:userID approve:approve schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    //    [self showIndicator];
    
    [[ERProgressHud sharedInstance] showDarkBackgroundViewWithTitle:NSLocalizedStringFromTableInBundle(@"DIALOG_LEAVE_REPORT_PLEASE_WAIT",nil,[Utils getLanguage],nil)];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            //            [self stopIndicator];
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getRequestLetterWithLetterID:letterID approve:approve];
            }
            else {
                connectCounter = 0;
            }
            
        }
        
        else{
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[ERProgressHud sharedInstance] hide];
            });
            
            [self validateData];
        }
    }];
    
    
}

- (void)validateData{
    
    NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LEAVE_REPORT_SUCCESS",nil,[Utils getLanguage],nil);
    [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
    
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)onAlertDialogClose{
    
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    //    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (void)showRightMenu:(UIButton *)sender{
    
    UIImage *unreadImage = [UIImage imageNamed:@"sort_with_unread"];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), YES, 5.0);
    [unreadImage drawInRect:CGRectMake(0, 0, 35, 35)];
    UIImage *unreadImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:NSLocalizedStringFromTableInBundle(@"BTN_LEAVE_REPORT_CANCEL_LEAVE_REQUEST",nil,[Utils getLanguage],nil)
                     image:unreadImage2
                    target:self
                    action:@selector(cancelLeave:)],
      ];
    
    const CGFloat W = self.view.bounds.size.width;
    const CGFloat H = self.view.bounds.size.height;
    
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(W - 16, 18, 40, 40) menuItems:menuItems];
    
}

- (void) cancelLeave:(id)sender
{
    
}

- (IBAction)moveback:(id)sender {
    
    if (self.page == 1) {
        UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (self.page == 2){
        NoticeInboxViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeInboxStoryboard"];
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    
    
}
@end
