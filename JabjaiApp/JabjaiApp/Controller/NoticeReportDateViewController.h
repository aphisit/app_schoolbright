//
//  NoticeReportDateViewController.h
//  JabjaiApp
//
//  Created by mac on 12/18/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "NoticeReportDteControllerDelagate.h"

#import "CFSCalendar.h"

#import "SlideMenuController.h"
#import "CAPSPageMenu.h"

@interface NoticeReportDateViewController : UIViewController <CFSCalendarDelegate, CFSCalendarDataSource, CFSCalendarDelegateAppearance, UITableViewDelegate, UITableViewDataSource, SlideMenuControllerDelegate>

@property (weak, nonatomic) CFSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) NSCalendar *gregorian;

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCalendarConstraint;

@property (weak, nonatomic) IBOutlet UIButton *arrowStatement;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDetailConstraint;

@property (weak, nonatomic) IBOutlet UILabel *dateTitle;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (retain, nonatomic) UIViewController *rootViewControler;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;


- (IBAction)openDrawer:(id)sender;
- (IBAction)openDetail:(id)sender;
- (IBAction)openList:(id)sender;

@end
