//
//  SchoolFeeDetailViewController.m
//  JabjaiApp
//
//  Created by mac on 3/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "SchoolFeeDetailViewController.h"

#import "SchoolFeeDetailModel.h"
#import "SchoolFeePriceListTableViewCell.h"

#import "SchoolFeeListViewController.h"

#import "UserInfoImageUrlModel.h"

#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"



@interface SchoolFeeDetailViewController (){
    
    NSMutableArray <SchoolFeeDetailModel *> *feeArray;
    NSInteger connectCounter;
    UIColor *waitColor;
    UIColor *paidColor;
    UIColor *oweColor;
    UIColor *grayColor;
    UIView *view;
    NSDate *now , *due , *issue;
    UserInfoImageUrlModel *userInfoImageModel;
    SchoolFeeDetailModel *schoolFeeDetailModel;
    NSData *imageData;
    CGFloat screenSizewidth;
    NSString *headDue;
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation SchoolFeeDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.heightMenuConstraint.constant = 120;
    }
    else{
        self.heightMenuConstraint.constant = 70;
        screenSizewidth = [UIScreen mainScreen].bounds.size.width;
        NSLog(@"%.2f", screenSizewidth);
        if (screenSizewidth > 320) {
            
            [self.headyearLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.yearLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.headtermLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.termLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.headnameLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.nameLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.headcodeLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.codeLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.headlevelLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.levelLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.headclassLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.classLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:22.0]];
            [self.headList setFont:[UIFont fontWithName:@"THSarabunNew" size:36.0]];
            [self.headpriceLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:26.0]];
            [self.totalPriceLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:26.0]];
            [self.headbahtLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:26.0]];
            [self.openBracketLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:24.0]];
            [self.dueLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:24.0]];
            [self.statusLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:24.0]];
            [self.closeBracketLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:24.0]];
            
            
        }
        else{
            
            
            [self.headyearLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.yearLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.headtermLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.termLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.headnameLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.nameLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.headcodeLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.codeLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.headlevelLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.levelLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.headclassLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.classLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:18.0]];
            [self.headList setFont:[UIFont fontWithName:@"THSarabunNew" size:28.0]];
            [self.headpriceLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:22.0]];
            [self.totalPriceLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:22.0]];
            [self.headbahtLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:22.0]];
            [self.openBracketLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:20.0]];
            [self.dueLabel setFont:[UIFont fontWithName:@"THSarabunNew" size:20.0]];
            [self.statusLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:20.0]];
            [self.closeBracketLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:20.0]];
        }
        
        
        
    }

    headDue = @"กำหนดชำระวันที่";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SchoolFeePriceListTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    waitColor = [UIColor colorWithRed:0.95 green:0.43 blue:0.00 alpha:1.0]; // orange
    paidColor = [UIColor colorWithRed:0.27 green:0.71 blue:0.26 alpha:1.0]; // green
    oweColor = [UIColor colorWithRed:0.93 green:0.23 blue:0.23 alpha:1.0]; // red
    
    //    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    self.tableView.backgroundColor = grayColor;
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    

    
    // 09/04/2561
    self.buttonStatus.enabled = false;
    [self.buttonStatus setTitle:@"" forState:UIControlStateNormal];
    
    self.topButtonConstraint.constant = 0;
    self.heightButtonConstraint.constant = 0;
    
    
    [self getInvoiceDetailWithInvoiceID:self.invoiceID];
    [self setLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

- (void) setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE",nil,[Utils getLanguage],nil);
    self.headyearLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_YEAR",nil,[Utils getLanguage],nil);
    self.headtermLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_SEMESTER",nil,[Utils getLanguage],nil);
    self.headnameLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_NAME",nil,[Utils getLanguage],nil);
    self.headcodeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_ID",nil,[Utils getLanguage],nil);
    self.headlevelLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_CLASS",nil,[Utils getLanguage],nil);
    self.headclassLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_ROOM",nil,[Utils getLanguage],nil);
    self.headList.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_LIST",nil,[Utils getLanguage],nil);
    self.headpriceLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_TOTAL",nil,[Utils getLanguage],nil);
    self.headbahtLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_BATH",nil,[Utils getLanguage],nil);
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(feeArray == nil) {
        return 0;
    }
    else {
        return feeArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SchoolFeeDetailModel *model = [feeArray objectAtIndex:indexPath.row];
    
    SchoolFeePriceListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    NSNumber *num = [NSNumber numberWithFloat:model.price];
    
    NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numFormatter setUsesSignificantDigits:NO];
    [numFormatter setMaximumFractionDigits:2];
    [numFormatter setMinimumFractionDigits:2];
    [numFormatter setUsesGroupingSeparator:YES];
    [numFormatter setGroupingSeparator:@","];
    [numFormatter setDecimalSeparator:@"."];
    [numFormatter setGroupingSize:3];
    
    NSString *stringNum = [numFormatter stringFromNumber:num];
    
    NSLog(@"%@", stringNum);

    cell.priceListLabel.text = model.productName;
//    cell.priceLabel.text = [[NSString alloc] initWithFormat:@"%.02f" , model.price];
    cell.priceLabel.text = [[NSString alloc] initWithFormat:@"%@" , stringNum];
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;

        if (screenSize.width <= 320.0f) {
            return 30;
        }
        else{
            return 40;
        }

//        return 30;
        
    }
    else{
        return 120;
    }
}

- (void)getInvoiceDetailWithInvoiceID:(long long)invoiceID{
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getSchoolFeeDetailWithUserID:userID InvoiceID:invoiceID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %d" , 1);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getInvoiceDetailWithInvoiceID:invoiceID];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            NSArray *returnedArray = returnedData;
            connectCounter = 0;
            feeArray = [[NSMutableArray alloc] init];
            NSMutableArray *dataDict = [[NSMutableArray alloc] init];
            NSArray *invoicesArray;
            double totalPrice = 0;
            
            NSLog(@"returnedArrayCount = %d",returnedArray.count);
            dataDict = [returnedData objectForKey:@"product"];
            float priceSum = 0;
            for (int i=0; i<dataDict.count; i++) {
                     SchoolFeeDetailModel *model = [[SchoolFeeDetailModel alloc] init];
                        NSString *product = [dataDict[i] objectForKey:@"product_name"];
                        float price = [[dataDict[i] objectForKey:@"price"] floatValue];
//                        priceSum += price;
//                        NSLog(@"%.02f",priceSum);
//                        NSNumber *num = [NSNumber numberWithFloat:priceSum];
//                        NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
//                        [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
//                        [numFormatter setUsesSignificantDigits:NO];
//                        [numFormatter setMaximumFractionDigits:2];
//                        [numFormatter setMinimumFractionDigits:2];
//                        [numFormatter setUsesGroupingSeparator:YES];
//                        [numFormatter setGroupingSeparator:@","];
//                        [numFormatter setDecimalSeparator:@"."];
//                        [numFormatter setGroupingSize:3];
//                        NSString *stringNum = [numFormatter stringFromNumber:num];
                       // NSLog(@"%@", stringNum);
                        model.productName = product;
                        model.price = price;
                        //self.totalPriceLabel.text = [[NSString alloc] initWithFormat:@"%@", stringNum];
                        [feeArray addObject:model];

            }
            totalPrice = [[returnedData objectForKey:@"totalprice"] doubleValue];
            invoicesArray = [returnedData objectForKey:@"invoicesPayments"];
            double amount = 0, sum = 0;
            NSDictionary *invoicesDic;
            for (int f = 0; f < invoicesArray.count ; f++) {
                invoicesDic = [invoicesArray objectAtIndex:f];
                amount = [[invoicesDic objectForKey:@"amount"] doubleValue];
                sum = sum + amount;
                
            }
            NSNumber *num = [NSNumber numberWithDouble:totalPrice - sum];
            NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
            [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [numFormatter setUsesSignificantDigits:NO];
            [numFormatter setMaximumFractionDigits:2];
            [numFormatter setMinimumFractionDigits:2];
            [numFormatter setUsesGroupingSeparator:YES];
            [numFormatter setGroupingSeparator:@","];
            [numFormatter setDecimalSeparator:@"."];
            [numFormatter setGroupingSize:3];
            self.totalPriceLabel.text =   [numFormatter stringFromNumber:num];
            
                    int status = [[returnedData objectForKey:@"status"] intValue];
                    NSMutableString *term, *year, *name, *code, *level, *class;
                    if (![[returnedData objectForKey:@"trem"] isKindOfClass:[NSNull class]]) {
                        term = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"trem"]];
                    }
                    else{
                        term = [[NSMutableString alloc] initWithString:@""];
                    }

                    if (![[returnedData objectForKey:@"yesr"] isKindOfClass:[NSNull class]]) {
                        year = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"yesr"]];
                    }
                    else{
                        year = [[NSMutableString alloc] initWithString:@""];
                    }

                    if (![[returnedData objectForKey:@"student_name"] isKindOfClass:[NSNull class]]) {
                        name = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"student_name"]];
                    }
                    else{
                        name = [[NSMutableString alloc] initWithString:@""];
                    }

                    if (![[returnedData objectForKey:@"student_id"] isKindOfClass:[NSNull class]]) {
                        code = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"student_id"]];
                    }
                    else{
                        code = [[NSMutableString alloc] initWithString:@""];
                    }

                    if (![[returnedData objectForKey:@"level_name"] isKindOfClass:[NSNull class]]) {
                        level = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"level_name"]];
                    }
                    else{
                        level = [[NSMutableString alloc] initWithString:@""];
                    }

                    if (![[returnedData objectForKey:@"class_name"] isKindOfClass:[NSNull class]]) {
                        class = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"class_name"]];
                    }
                    else{
                        class = [[NSMutableString alloc] initWithString:@""];
                    }
            
                    NSString *imageSchoolIconUrl, *schoolThaiName, *schoolEngName;
            
                    if(![[returnedData objectForKey:@"school_pic"] isKindOfClass:[NSNull class]]){
                        imageSchoolIconUrl = [returnedData objectForKey:@"school_pic"];
                    }
                    else{
                        imageSchoolIconUrl = @"";
                    }
            
                    if(![[returnedData objectForKey:@"school_nameEN"] isKindOfClass:[NSNull class]]){
                        schoolEngName = [returnedData objectForKey:@"school_nameEN"];
                    }
                    else{
                        schoolEngName = @"";
                    }
            
                    if(![[returnedData objectForKey:@"school_name"] isKindOfClass:[NSNull class]]){
                        schoolThaiName = [returnedData objectForKey:@"school_name"];
                    }
                    else{
                        schoolThaiName = @"";
                    }

                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getXMLDateFormat]];
                    NSDateFormatter *formatter2 = [Utils getDateFormatter];
                    [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                    NSMutableString *dueDate = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"dueDate"]];
                    NSMutableString *issueDate = [[NSMutableString alloc] initWithFormat:@"%@", [returnedData objectForKey:@"issuedDate"]];
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) name);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) term);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) year);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) code);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) level);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) class);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) dueDate);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) issueDate);

                    NSRange dotRange = [dueDate rangeOfString:@"."];
                    NSRange dotRange2 = [issueDate rangeOfString:@"."];
                    NSDate *dueDay, *issueDay;
                    if(dotRange.length != 0) {
                        dueDay = [formatter dateFromString:dueDate];
                    }
                    else {
                        dueDay = [formatter2 dateFromString:dueDate];
                    }
                    if(dotRange2.length != 0) {
                        issueDay = [formatter dateFromString:issueDate];
                    }
                    else {
                        issueDay = [formatter2 dateFromString:issueDate];
                    }
                    self.dateIssueLabel.text = [self.dateFormatter stringFromDate:issueDay];
                    self.nameLabel.text = name;
                    self.yearLabel.text = year;
                    self.termLabel.text = term;
                    self.codeLabel.text = code;
                    self.levelLabel.text = level;
                    self.classLabel.text = class;
                    self.schoolThaiName.text = schoolThaiName;
                    self.schoolEngName.text = schoolEngName;
            
            imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imageSchoolIconUrl]];
            self.schoolLogo.clipsToBounds = YES;
            self.schoolLogo.layer.shouldRasterize = YES;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                CGSize screenSize = [[UIScreen mainScreen] bounds].size;
                
                if (screenSize.width > 320.0f) {
                    self.schoolLogo.layer.cornerRadius = 40;
                    self.schoolLogoWidthConstraint.constant = 80;
                    self.schoolLogoHeightConstraint.constant = 80;
                    self.schoolViewHeightConstraint.constant = 90;
                    UIImage *imageweb = [UIImage imageWithData:imageData];
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), YES, 5);
                    [imageweb drawInRect:CGRectMake(0, 0, 80, 80)];
                    UIImage *imageweb2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    self.schoolLogo.image = imageweb2;
                    self.schoolLogo.contentMode = UIViewContentModeCenter;
                }
                else{
                    self.schoolLogo.layer.cornerRadius = 25;
                    self.schoolLogoWidthConstraint.constant = 50;
                    self.schoolLogoHeightConstraint.constant = 50;
                    self.schoolViewHeightConstraint.constant = 60;
                    UIImage *imageweb = [UIImage imageWithData:imageData];
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), YES, 8);
                    [imageweb drawInRect:CGRectMake(0, 0, 50, 50)];
                    UIImage *imageweb2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    self.schoolLogo.image = imageweb2;
                    self.schoolLogo.contentMode = UIViewContentModeCenter;
                }
            }
                        if (status == 0) {
                            due = dueDay;
                            issue = issueDay;
                            now = [NSDate date];
                            
                            // Compare Date
                            if ([now compare:due] == NSOrderedAscending) {
//                                self.statusLabel.text = [self.dateFormatter stringFromDate:dueDay];
                                self.statusLabel.text = [[NSString alloc] initWithFormat:@"%@ %@", headDue , [self.dateFormatter stringFromDate:dueDay]];
                            }
                            else if ([now compare:due] == NSOrderedDescending){
                                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_OVERDUE",nil,[Utils getLanguage],nil);
                                self.statusLabel.textColor = oweColor;
                                [self.dueLabel removeFromSuperview];
                            }
                            else{
//                                self.statusLabel.text = [self.dateFormatter stringFromDate:dueDay];
                                self.statusLabel.text = [[NSString alloc] initWithFormat:@"%@ %@", headDue , [self.dateFormatter stringFromDate:dueDay]];
                            }
                        }
                        else if (status == 1){
                            due = dueDay;
                            issue = issueDay;
                            now = [NSDate date];
                            
                            self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PAID",nil,[Utils getLanguage],nil);
                            self.statusLabel.textColor = paidColor;
                            [self.dueLabel removeFromSuperview];
                            
                            self.buttonStatus.enabled = false;
                            [self.buttonStatus setTitle:@"" forState:UIControlStateNormal];
                            
                            self.topButtonConstraint.constant = 0;
                            self.heightButtonConstraint.constant = 0;
                            
                        }
                        else {
                            
                        }
            
                    [self.tableView reloadData];
        }
        
    }];
    
}

- (void)performData{
    
    if (schoolFeeDetailModel != nil) {
        
        self.dateIssueLabel.text = [self.dateFormatter stringFromDate:schoolFeeDetailModel.dateIssue];
        
        self.nameLabel.text = schoolFeeDetailModel.personName;
        self.yearLabel.text = schoolFeeDetailModel.yearName;
        self.termLabel.text = schoolFeeDetailModel.termName;
        self.codeLabel.text = schoolFeeDetailModel.codeName;
        self.levelLabel.text = schoolFeeDetailModel.levelName;
        self.classLabel.text = schoolFeeDetailModel.className;
        
        if (schoolFeeDetailModel.status == 0) {
            self.statusLabel.text = [self.dateFormatter stringFromDate:schoolFeeDetailModel.dateDue];
            
        }
        else if (schoolFeeDetailModel.status == 1){
            due = schoolFeeDetailModel.dateDue;
            issue = schoolFeeDetailModel.dateIssue;
            now = [NSDate date];
            
            // Compare Date
            if ([now compare:issue] == NSOrderedAscending) {
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PAID",nil,[Utils getLanguage],nil);
                self.statusLabel.textColor = paidColor;
                [self.dueLabel removeFromSuperview];
                
//                self.buttonStatus.enabled = false;
//                [self.buttonStatus setTitle:@"" forState:UIControlStateNormal];
//
//                self.topButtonConstraint.constant = 0;
//                self.heightButtonConstraint.constant = 0;
//                [self.buttonStatus removeFromSuperview];
            }
            else if ([now compare:issue] == NSOrderedDescending){
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_OVERDUE",nil,[Utils getLanguage],nil);
                self.statusLabel.textColor = oweColor;
                [self.dueLabel removeFromSuperview];
            }
            else{
                self.statusLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PAID",nil,[Utils getLanguage],nil);
                self.statusLabel.textColor = paidColor;
                [self.dueLabel removeFromSuperview];
                
//                self.buttonStatus.enabled = false;
//                [self.buttonStatus setTitle:@"" forState:UIControlStateNormal];
//                
//                self.topButtonConstraint.constant = 0;
//                self.heightButtonConstraint.constant = 0;
                
//                [self.buttonStatus removeFromSuperview];
            }
            
        }
        else {
            
        }
        
    }
    else{
        
    }
    
}



- (IBAction)moveBack:(id)sender {
    
    SchoolFeeListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SchoolFeeStoryboard"];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}
@end
