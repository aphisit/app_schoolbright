//
//  NoticeStopConfirmTeacherViewController.m
//  JabjaiApp
//
//  Created by mac on 11/28/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NoticeStopConfirmTeacherViewController.h"
#import "NoticeStopDetailViewController.h"
#import "NoticeStopViewController.h"
#import "UserInfoViewController.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "AlertDialogConfirm.h"
#import "NoticeStopConfirmTeacherModel.h"
#import "Utils.h"
#import "APIURL.h"
#import <AFNetworking/AFNetworking.h>
#import "ERProgressHud.h"
#import <Photos/Photos.h>
@interface NoticeStopConfirmTeacherViewController (){
    
    NSString *jsonString;
    AlertDialogConfirm *alertDialog;
    PHImageRequestOptions *requestOptions;
    UIImage *ima;
    NSInteger *num,highImage;
    NoticeStopConfirmTeacherModel *noticeStopConfirmTeacherModel;
    NSMutableArray<NoticeStopConfirmTeacherModel *> *leaveArray;
    NSInteger connectCounter;
    NSString *positionLeave , *positionTeacher;
    NSDateFormatter *formatter ,*formatter2;
    NSDate *date, *dateCurrent, *dayStart, *dayEnd;
    NSString *leaveName, *dateNow,  *dayFirst, *dayLast, *dayFirstone, *dayLastone;
    NSString *ampher, *homeNumber, *phone, *province, *road, *tumbon, *reason, *reasonLeave;
    NSString *URLString;
    NSArray *returnedArray;
    NSInteger leaveid;
    NSBundle *myLangBundle;
}
@property (nonatomic, strong) AlertDialog *errorDialog;
@end

static NSString *cellIdentifier = @"Cell";


@implementation NoticeStopConfirmTeacherViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    
   
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([NoticeImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    leaveName = [NSString stringWithFormat:@"%@ %@", [UserData getFirstName] , [UserData getLastName]];
    self.leaveTeacherName.text = leaveName;
    self.leaveCauseitem.text = self.leaveCause;  //Insert from previous page
    self.firstLeaveDate.text = self.leaveFirstDate;  //Insert from previous page
    self.lastLeaveDate.text = self.leaveLastDate;   //Insert from previous page
    self.confirmDetail.text = self.noticeDetail;  //Insert from previous page
    self.confirmDetail.lineBreakMode = NSLineBreakByWordWrapping;
    self.confirmDetail.numberOfLines = 0;
    self.addressDetail.lineBreakMode = NSLineBreakByWordWrapping;
    self.addressDetail.numberOfLines = 0;
    if ([self.headTitleAddress isEqual:@""]) {
        self.headAddrressDetail.text = @"";
    }
    else{
        if ( self.imageNotice == nil) {
            self.heightCollectionConStraint.constant = 0;
        }
    }
    

    if (self.address.length == 0) {
        self.addressDetail.text = @"-";
    }else{
        self.addressDetail.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@", self.address , self.headSubDistrict , self.subDistrict , self.headDistrict , self.district , self.headProvince , self.province , self.headPhoneNumber , self.phoneNumber];
    }
    
    NSLog(@"xxx = %f",self.heightCollectionConStraint.constant);
    highImage = (int)self.heightCollectionConStraint.constant  ;
    float wImageCollection = self.collectionView.frame.size.width / ((self.collectionView.frame.size.width/4)-10);
    float amountFileRow = _imageNotice.count / (int)wImageCollection;
    NSLog(@"num = %d",(int)wImageCollection);
    NSInteger modFile = _imageNotice.count % (int)wImageCollection ;
    
    if (modFile > 0) {
        amountFileRow ++;
    }
    self.heightCollectionConStraint.constant = self.heightCollectionConStraint.constant * amountFileRow+((amountFileRow*5)*2);
    
    
    [self getPositionForTeacher];
    
}

- (void) doDesignLayout{
    
    [self setLanguage];
    CAGradientLayer *gradientHeader,*gradientNext;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    //set color button next
    [self.headerNextbtn layoutIfNeeded];
    gradientNext = [Utils getGradientColorNextAtion];
    gradientNext.frame = self.headerNextbtn.bounds;
    [self.headerNextbtn.layer insertSublayer:gradientNext atIndex:0];
}

-(void)setLanguage{
    
    self.headerLeaveLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE",nil,myLangBundle,nil);
    self.headerSenderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_SENDER",nil,myLangBundle,nil);
    self.headerPositionLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_POSITION",nil,myLangBundle,nil);
    self.headerReasonLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_REASON",nil,myLangBundle,nil);
    self.headerDatefromLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_DATE_FROM",nil,myLangBundle,nil);
    self.headerDatetoLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_DATE_TO",nil,myLangBundle,nil);
    self.headerDescriptionLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_DESCRIPTION",nil,myLangBundle,nil);
    self.headerAddressLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_ADDRESS",nil,myLangBundle,nil);
    self.headerImageuserlabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_ATTIMAGE",nil,myLangBundle,nil);
   // [self.headerDescriptionLabel.text addAttribute: NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSSingleUnderlineStyle] range:range];
    
  
     [self.headerNextbtn setTitle:NSLocalizedStringFromTableInBundle(@"BTN_LEAVE_SUBMIT",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

- (void)viewDidLayoutSubviews{
    
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
    [self doDesignLayout];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_imageNotice count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //    static NSString *cellIdentifier = @"Cell";
    NoticeImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.image.tag = 200;
    cell.image.image = [self.imageNotice objectAtIndex:indexPath.row];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;{
    CGSize  defaultSize = CGSizeMake((self.collectionView.frame.size.width/4)-10, highImage);
    return defaultSize;
}


#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onAlertDialogClose{

    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];

    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];

}

- (void)showIndicator{
    if (![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator{
    [self.indicator stopAnimating];
}

- (void)getPositionForTeacher{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getPositionForTeacherWithUserID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getPositionForTeacher];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {

                connectCounter = 0;

            NSString *position = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            positionLeave = position;
            
                [self performData];
//            }
            
        }
        
    }];
}

- (void)performData{
    
        positionTeacher = positionLeave;

    self.position.text = positionTeacher;
}

- (void)clearDisplay {
    
}

- (IBAction)moveback:(id)sender {
    
    NoticeStopDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeStopDetailStoryboard"];
    
    viewController.leaveCause = self.leaveCause;
    viewController.leaveFirstDate = self.leaveFirstDate;
    viewController.leaveLastDate = self.leaveLastDate;
    viewController.noticeDetail = self.noticeDetail;
    viewController.address = self.address;
    viewController.province = self.province;
    viewController.district = self.district;
    viewController.subDistrict = self.subDistrict;
    viewController.phoneNumber = self.phoneNumber;
    viewController.leaveStartdate = self.leaveStartdate;
    viewController.leaveEndDate = self.leaveEndDate;
    viewController.fillDetailTag = self.fillDetailTag;
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}

- (IBAction)confirmPage:(id)sender {
    [[ERProgressHud sharedInstance] showDarkBackgroundViewWithTitle:@"กรุณารอสักครู่...."];
    [self doSetParameter];
}

- (void)doSetParameter{
    
    long long userID = [UserData getUserID];
    
    date = [NSDate date];
    dayStart = self.leaveStartdate;
    dayEnd = self.leaveEndDate;
    reason = self.leaveCause;
    
    if ([reason isEqual:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_SICK",nil,myLangBundle,nil)]) {
        reasonLeave = @"0";
    }
    else if ([reason isEqual:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_PERSONAL",nil,myLangBundle,nil)]){
        reasonLeave = @"1";
    }
    else if ([reason isEqual:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_MATERNITY",nil,myLangBundle,nil)]){
        reasonLeave = @"2";
    }
    else if ([reason isEqual:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_VACATION",nil,myLangBundle,nil)]){
        reasonLeave = @"3";
    }
    else if ([reason isEqual:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_ORDINATION",nil,myLangBundle,nil)]){
        reasonLeave = @"4";
    }
    else if ([reason isEqual:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_MILITARY",nil,myLangBundle,nil)]){
        reasonLeave = @"5";
    }
    else if ([reason isEqual:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_STUDY",nil,myLangBundle,nil)]){
        reasonLeave = @"6";
    }
    else if ([reason isEqual:NSLocalizedStringFromTableInBundle(@"LABEL_LEAVE_GOVERNMENT",nil,myLangBundle,nil)]){
        reasonLeave = @"7";
    }

    
    dayFirst = [Utils dateToServerDateFormat:dayStart];
    dayLast = [Utils dateToServerDateFormat:dayEnd];
    dateNow = [Utils dateToServerDateFormat:date];
    
    ampher = self.district;
    homeNumber = self.address;
    province = self.province;
    tumbon = self.subDistrict;
    road = self.road;
    phone = self.phoneNumber;
    
    dayFirst = [NSString stringWithFormat: @"\"%@\"", dayFirst];
    dayLast = [NSString stringWithFormat: @"\"%@\"", dayLast];
    dateNow = [NSString stringWithFormat: @"\"%@\"", dateNow];
    reasonLeave = [NSString stringWithFormat: @"\"%@\"", reasonLeave];
    ampher = [NSString stringWithFormat:@"\"%@\"", ampher];
    homeNumber = [NSString stringWithFormat: @"\"%@\"", homeNumber];
    province = [NSString stringWithFormat: @"\"%@\"", province];
    tumbon = [NSString stringWithFormat: @"\"%@\"", tumbon];
    road = [NSString stringWithFormat: @"\"%@\"", road];
    phone = [NSString stringWithFormat: @"\"%@\"", phone];

    
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    jsonString = [[NSString alloc] initWithFormat:@"{\"UserId\" : %lld , \"LeaveType\" : %@ , \"DayStart\" : %@ , \"DayEnd\" : %@,\"HomeNumber\" : %@ , \"Road\" : %@ ,\"Tumbon\" : %@ ,\"Aumpher\" : %@,\"Province\" : %@ ,\"Phone\" : %@, \"SubmitDate\" : %@, \"Season\" : %i}", userID , reasonLeave , dayFirst , dayLast, homeNumber , road , tumbon , ampher , province , phone , dateNow , -1];
    
    NSLog(@"jsonString = %@",jsonString);
    
    [self updateSNSendLeave:jsonString imageArray:(NSArray*)self.imageNotice];
    
}

- (void)updateSNSendLeave:(NSString *)jsonString imageArray:(NSArray *)imageArray{
    
    NSString *URLString = [APIURL getUpdateLeavePOST];
    NSError* error;
    NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *json = [[NSMutableDictionary alloc] init];
    json = [[NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableLeaves error:&error] mutableCopy];
    [json setObject:self.noticeDetail forKey:@"Description" ];
    //NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableLeaves error:&error];
    
    static NSString *KEY_LETTERID = @"leave_id";
    static NSString *KEY_SCHOOLID = @"schoolid";
    
    if(error != nil) {
        NSLog(@"%@", @"Parse JSON data failed");
    }
    else {
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        [manager POST:URLString parameters:json progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"responseObject = %@", responseObject);
            NSString *letterStatus;
            leaveid = [[responseObject objectForKey:@"letterId"] integerValue];
            if (![[responseObject objectForKey:@"letterStatus"] isKindOfClass:[NSNull class]]) {
                letterStatus = [responseObject objectForKey:@"letterStatus"];
            }else{
                letterStatus = @"";
            }
            if(imageArray.count > 0){
                
                //NSData *imageData = nil;
                //NSMutableArray *addImage = [[NSMutableArray alloc] init];
                NSString *urlString = [APIURL getSNLeavePOSTURL];
                NSURL *url = [NSURL URLWithString:urlString];
                NSString *schoolID = [[NSString alloc] initWithFormat:@"%lld", (long long)[UserData getSchoolId]];
                NSString *letterID = [[NSString alloc] initWithFormat:@"%lld", (long long)leaveid];
                NSLog(@"num = %d",  leaveid);
                NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
                [parameters setObject:schoolID forKey:KEY_SCHOOLID];
                [parameters setObject:letterID forKey:KEY_LETTERID];
                for (UIImage *ima in imageArray) {
                    if (ima != nil) {
                        NSData *dataImage = UIImageJPEGRepresentation(ima, 0.3);
                        [Utils uploadImageFromURL:url data:dataImage imageparameterName:@"image" imageFileName:[NSString stringWithFormat:@"%ld",num++] mimeType:JPEG parameters:parameters withCompletionHandler:^(id responseObject, NSError *error) {
                        }];
                    }
                }
            }
            if(error != nil) {
                NSLog(@"error");
            }
            else {
                if ([letterStatus isEqual:@"Leave Letter Limit"]) {
                    [[ERProgressHud sharedInstance] hide];
                    [self showAlertNotPassDialog:NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_LEAVE_LIMIT",nil,[Utils getLanguage],nil)];
                    
                }else if([letterStatus isEqual:@"Success"]){
                    [self validateData];
                }else{
                    [self validateData];
                }
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            
            
        }];
        
    }
}


- (void)applyData{
    
    date = [NSDate date];
    dayStart = self.leaveStartdate;
    dayEnd = self.leaveEndDate;
    reason = self.leaveCause;
    ampher = self.district;
    homeNumber = self.address;
    province = self.province;
    tumbon = self.subDistrict;
    road = self.road;
    phone = self.phoneNumber;
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    URLString = [APIURL getSendLetterDataLeaveWithLeaveType:reason description:self.noticeDetail dayStart:dayStart dayEnd:dayEnd userID:userID ampher:ampher homeNumber:homeNumber phone:phone province:province road:road tumbon:tumbon season:-1 schoolid:schoolid];
    NSURL *url = [[NSURL alloc] initWithString:URLString];
    NSLog(@"%@", URLString);
    [self sendLetterToServerWithURL:url];
}

- (void)sendLetterToServerWithURL:(NSURL *)url{
    
    [self showIndicator];
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self stopIndicator];
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self sendLetterToServerWithURL:url];
            }
            else {
                connectCounter = 0;
            }
        }
        else{
            connectCounter = 0;
            [self validateData];
        }
    }];
}

- (void)validateData {
    //    [self stopIndicator];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [[ERProgressHud sharedInstance] hide];
        
    });
    
    NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_LEAVE_COMPLETED",nil,myLangBundle,nil);
    [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
    
}

#pragma mark - AlertDialog
- (void) showAlertNotPassDialog:(NSString*)message{
    if (self.errorDialog != nil) {
        self.errorDialog = nil;
    }
    self.errorDialog = [[AlertDialog alloc] init];
    self.errorDialog.delegate = self;
    [self.errorDialog showDialogInView:self.view title:@"" message:message];
}

@end
