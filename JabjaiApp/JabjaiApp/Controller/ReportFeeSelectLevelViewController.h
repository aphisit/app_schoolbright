//
//  ReportFeeSelectLevelViewController.h
//  JabjaiApp
//
//  Created by mac on 3/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"

#import "SWRevealViewController.h"

@interface ReportFeeSelectLevelViewController : UIViewController <SlideMenuControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSInteger status;

@property (weak, nonatomic) IBOutlet UILabel *headMenu;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)moveBack:(id)sender;

@end
