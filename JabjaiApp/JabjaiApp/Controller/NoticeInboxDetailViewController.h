//
//  NoticeInboxDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 12/14/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

#import "SlideMenuController.h"

@interface NoticeInboxDetailViewController : UIViewController <SlideMenuControllerDelegate>

@property (assign, nonatomic) long long letterID;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailName;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailPosiion;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailCause;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailFirstDate;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailLastDate;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailRequestDate;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetail;

@property (weak, nonatomic) IBOutlet UILabel *headChangeInboxDetail;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIButton *leaveInboxAccept;
@property (weak, nonatomic) IBOutlet UIButton *leaveInboxDecline;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *acceptButtonConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *declineButtonConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContentConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMenuConstraint;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *headLeaveInboxAddressDetail;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailAddress;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCollectionViewConstraint;


- (IBAction)accept:(id)sender;
- (IBAction)decline:(id)sender;
- (IBAction)moveBack:(id)sender;


@end
