//
//  ReportFeeSelectClassViewController.h
//  JabjaiApp
//
//  Created by mac on 3/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"
#import "SWRevealViewController.h"

@interface ReportFeeSelectClassViewController : UIViewController <SlideMenuControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) long long classroomID;
@property (nonatomic) long long levelID;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)moveBack:(id)sender;

@end
