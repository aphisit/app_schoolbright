//
//  ReportFeeViewController.m
//  JabjaiApp
//
//  Created by mac on 3/20/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportFeeViewController.h"
#import "ReportFeeSelectLevelViewController.h"

#import "ReportListTeacherViewController.h"

#import "ReportFeeFilter.h"
#import "Constant.h"
#import "Utils.h"
#import "APIURL.h"
#import "UserData.h"

#import "ReportFeeModel.h"
#import "AlertDialog.h"

@interface ReportFeeViewController (){
    
    UIColor *yellowColor;
    UIColor *greenColor;
    UIColor *redColor;
    
    int randNum;
    
    CGFloat screenSizeWidth;
    
    NSMutableArray *reportFeeArray;
    NSMutableArray *yData;
    NSMutableArray *xData;
//
//    NSArray *yData;
//    NSArray *xData;
    
    float total, totalHalf, totalPerson;
    
    float statusPaid, statusHalfPaid, statusHalfOwe, statusOwe;
    
    float percentPaid, percentHalfPaid, percentHalfOwe, percentOwe, percentTotalHalf;
    
    float countPeopleOwe, countPeopleHalf, countPeoplePaid;
    
    NSInteger status;
    
    // Data Containers
    NSMutableDictionary *yearTermDict; //Key is yearNumber and data are array of YearTermModel
    
    NSInteger connectCounter;
    
    NSString *term;
    
    AlertDialog *alertDialog;
    
}

@property (nonatomic, strong) SelectYearAndTermDialog *filterDialog;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;

@end

@implementation ReportFeeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    //Set Color
    greenColor = [UIColor colorWithRed:0.13 green:0.76 blue:0.47 alpha:1.0];
    yellowColor = [UIColor colorWithRed:0.99 green:0.65 blue:0.15 alpha:1.0];
    redColor = [UIColor colorWithRed:1.00 green:0.39 blue:0.39 alpha:1.0];
    
    NSString *alertTitle = @"แจ้งเตือน";
    NSString *alertMessage = @"ขออภัย ยังไม่เปิดให้ใช้บริการในขณะนี้";
    [self showFinishAlertDialogWithTitle:alertTitle message:alertMessage];
    
    //AutoFont
    screenSizeWidth = [UIScreen mainScreen].bounds.size.width;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (screenSizeWidth > 320) {
            [self.headUpdateLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:20.0]];
            [self.dateNowLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:20.0]];
            [self.timeNowLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:20.0]];
        }
        else{
            [self.headUpdateLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:18.0]];
            [self.dateNowLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:18.0]];
            [self.timeNowLabel setFont:[UIFont fontWithName:@"THSarabunNew-Bold" size:18.0]];
        }
    }
    
    //Set Date Time
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.dateNowLabel.text = [self.dateFormatter stringFromDate:[NSDate date]];
    
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    self.timeNowLabel.text = [self.timeFormatter stringFromDate:[NSDate date]];
    
    //Set Pie
    [self.pieChart setBackgroundColor:[UIColor clearColor]];

    
    xData = [[NSMutableArray alloc] init];
    yData = [[NSMutableArray alloc] init];
    
    self.yearLabel.text = @"2561";
    self.termLabel.text = @"2";

    [self setupGraphData];
    [self setupPieChartView];
    
    term = @"TS0000007";

    [self getReporFeeWithTermID:term];
    
    connectCounter = 0;
    
    [self callAPIGetYearAndTerm];
    
    
}

- (void)setupPieChartView {
    
    self.pieChart.delegate = self;
    self.pieChart.usePercentValuesEnabled = YES;
    self.pieChart.drawSlicesUnderHoleEnabled = NO;
    //self.pieChart.drawSliceTextEnabled = NO;
    self.pieChart.holeRadiusPercent = 0.8;
    self.pieChart.transparentCircleRadiusPercent = 0.15;
    self.pieChart.chartDescription.enabled = NO;
    [self.pieChart setExtraOffsetsWithLeft:5.f top:5.f right:5.f bottom:5.f];
    
    self.pieChart.drawCenterTextEnabled = NO;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    self.pieChart.drawHoleEnabled = YES;
    self.pieChart.rotationAngle = 0.0;
    self.pieChart.rotationEnabled = YES;
    self.pieChart.highlightPerTapEnabled = YES;
    
    self.pieChart.entryLabelColor = [UIColor whiteColor];
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        self.pieChart.entryLabelFont = [UIFont fontWithName:@"THSarabunNew-Bold" size:32.0f];
//    }
//    else{
//        self.pieChart.entryLabelFont = [UIFont fontWithName:@"THSarabunNew-Bold" size:20.0f];
//    }
    
    
    ChartLegend *l = self.pieChart.legend;
    [l setEnabled:NO];
    
}

- (void)setupGraphData {
    
    if(yData.count == 0) {
        [self.pieChart setAlpha:0.0];
        [self.totalStack setAlpha:0.0];
        
        return;
    }
    else {
        [self.pieChart setAlpha:1.0];
        [self.totalStack setAlpha:1.0];
    }
    
    NSMutableArray *values = [[NSMutableArray alloc] init];
    
    // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
    for (int i = 0; i < yData.count; i++)
    {
        [values addObject:[[PieChartDataEntry alloc] initWithValue:([[yData objectAtIndex:i] integerValue]) label:[xData objectAtIndex:i]]];
    }
    
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithEntries:values label:@""];
    
    dataSet.sliceSpace = 0.0;
    
    // add a lot of colors
    
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    [colors addObject:redColor];
    [colors addObject:yellowColor];
    [colors addObject:greenColor];
    [colors addObjectsFromArray:ChartColorTemplates.vordiplom];
    [colors addObjectsFromArray:ChartColorTemplates.joyful];
    [colors addObjectsFromArray:ChartColorTemplates.colorful];
    [colors addObjectsFromArray:ChartColorTemplates.liberty];
    [colors addObjectsFromArray:ChartColorTemplates.pastel];
    [colors addObject:[UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f]];
    
    dataSet.colors = colors;
    
    PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];

    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
    [data setValueTextColor:UIColor.clearColor];
    self.pieChart.data = data;
    
    [self.pieChart highlightValue:nil];
    
    //    [self.pieChart animateWithXAxisDuration:2.0 easingOption:ChartEasingOptionEaseOutBack];
    
    [self.pieChart animateWithYAxisDuration:2.0 easingOption:ChartEasingOptionEaseOutCirc];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    if(self.pieChart != nil) {
        [self.pieChart animateWithYAxisDuration:2.0 easingOption:ChartEasingOptionEaseOutCirc];
    }
    
    
}

- (void)chartValueSelected:(ChartViewBase *)chartView entry:(ChartDataEntry *)entry highlight:(ChartHighlight *)highlight{
    
    NSInteger selectedIndex = highlight.x;
    
    if (selectedIndex == 0) {
        
        status = 0;
        
        ReportFeeSelectLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportFeeSelectLevelStoryboard"];
        viewController.status = status;
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else if (selectedIndex == 1){
        status = 1;
        
        ReportFeeSelectLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportFeeSelectLevelStoryboard"];
        viewController.status = status;
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    else{
        status = 2;
        
        ReportFeeSelectLevelViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportFeeSelectLevelStoryboard"];
        viewController.status = status;
        
//        [self.revealViewController pushFrontViewController:viewController animated:YES];
        
        [self.slideMenuController changeMainViewController:viewController close:YES];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - GetAPIData

-(void)callAPIGetYearAndTerm {
    [self getYearAndTermData];
}

-(void)getYearAndTermData {
    NSString *URLString = [APIURL getYearAndTermURL];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self callAPIGetYearAndTerm];
            }
            else {
                connectCounter = 0;
            }
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetYearAndTerm];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self callAPIGetYearAndTerm];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                yearTermDict = [[NSMutableDictionary alloc] init];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSInteger yearID = [[dataDict objectForKey:@"Yearid"] integerValue];
                    NSInteger yearNumber = [[dataDict objectForKey:@"YearNumber"] integerValue];
                    NSMutableString *termID = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Termid"]];
                    NSMutableString *termName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"TermName"]];
                    NSMutableString *startDateStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dStart"]];
                    NSMutableString *endDateStr = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dEnd"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) termID);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) termName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) startDateStr);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) endDateStr);
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSDate *lStartDate = [formatter dateFromString:startDateStr];
                    NSDate *lEndDate = [formatter dateFromString:endDateStr];
                    
                    YearTermModel *model = [[YearTermModel alloc] init];
                    [model setYearID:[[NSNumber alloc] initWithInteger:yearID]];
                    [model setYearNumber:[[NSNumber alloc] initWithInteger:yearNumber]];
                    [model setTermID:termID];
                    [model setTermName:termName];
                    [model setTermBegin:lStartDate];
                    [model setTermEnd:lEndDate];
                    
                    //                    NSLog(@"%@", [NSString stringWithFormat:@"Term Start : %@", [Utils dateInStringFormat:lStartDate format:@"yyyy-MM-dd"]]);
                    //                    NSLog(@"%@", [NSString stringWithFormat:@"Term End : %@", [Utils dateInStringFormat:lEndDate format:@"yyyy-MM-dd"]]);
                    
                    if([yearTermDict objectForKey:model.yearNumber] != nil) {
                        NSMutableArray *termArr = [yearTermDict objectForKey:model.yearNumber];
                        [termArr addObject:model];
                        [yearTermDict setObject:termArr forKey:model.yearNumber];
                    }
                    else {
                        NSMutableArray *termArr = [[NSMutableArray alloc] init];
                        [termArr addObject:model];
                        [yearTermDict setObject:termArr forKey:model.yearNumber];
                    }
                }
                
                // Initialize filter dialog
                if(self.filterDialog != nil && [self.filterDialog isDialogShowing]) {
                    [self.filterDialog dismissDialog];
                    self.filterDialog = nil;
                }
                
                self.filterDialog = [[SelectYearAndTermDialog alloc] initWithYearTermDictionary:yearTermDict];
                self.filterDialog.delegate = self;
                
            }
        }
        
    }];
    
}

- (void)applyFilter:(NSNumber *)schoolYear semester:(NSString *)semester termID:(NSString *)termID{
    
    ReportFeeFilter *filter = [[ReportFeeFilter alloc] init];
    filter.schoolyearID = schoolYear;
    filter.schooltermName = semester;
    
    filter.schooltermID = termID;
    
    term = termID;
    
    self.yearLabel.text = [[NSString alloc] initWithFormat:@"%@", filter.schoolyearID];
    self.termLabel.text = filter.schooltermName;
    
    [self getReporFeeWithTermID:filter.schooltermID];
    
    NSLog(@"%@", @"Apply Filter");
}

- (void)getReporFeeWithTermID:(NSString *)termId{
    
    long long userId = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getReportFeeWithUserID:userId termID:termId schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode] != 200)) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getReporFeeWithTermID:termId];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReporFeeWithTermID:termId];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getReporFeeWithTermID:termId];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else {
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                reportFeeArray = [[NSMutableArray alloc] init];
                
                NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
                [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                [numFormatter setUsesSignificantDigits:NO];
                [numFormatter setUsesGroupingSeparator:YES];
                [numFormatter setGroupingSeparator:@","];
                [numFormatter setDecimalSeparator:@"."];
                [numFormatter setGroupingSize:3];
                
                NSNumberFormatter *numFormatter1 = [[NSNumberFormatter alloc] init];
                [numFormatter1 setNumberStyle:NSNumberFormatterDecimalStyle];
                [numFormatter1 setUsesSignificantDigits:NO];
                [numFormatter1 setMaximumFractionDigits:2];
                [numFormatter1 setMinimumFractionDigits:2];
                [numFormatter1 setUsesGroupingSeparator:YES];
                [numFormatter1 setGroupingSeparator:@","];
                [numFormatter1 setDecimalSeparator:@"."];
                [numFormatter1 setGroupingSize:3];
                
                NSNumberFormatter *numFormatter2 = [[NSNumberFormatter alloc] init];
                [numFormatter2 setNumberStyle:NSNumberFormatterDecimalStyle];
                [numFormatter2 setUsesSignificantDigits:NO];
                [numFormatter2 setMaximumFractionDigits:1];
                [numFormatter2 setMinimumFractionDigits:1];
                [numFormatter2 setUsesGroupingSeparator:YES];
                [numFormatter2 setGroupingSeparator:@","];
                [numFormatter2 setDecimalSeparator:@"."];
                [numFormatter2 setGroupingSize:3];
                
                for(int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    int status = [[dataDict objectForKey:@"payment_status"] intValue];
                    
                    float amountPrice = [[dataDict objectForKey:@"amount"] floatValue];
                    float amountPercent = [[dataDict objectForKey:@"amount_percent"] floatValue];
                    float paymentPrice = [[dataDict objectForKey:@"payment"] floatValue];
                    float paymentPercent = [[dataDict objectForKey:@"payment_percent"] floatValue];
                    float countNumber = [[dataDict objectForKey:@"student"] floatValue];
                    
                    
                    ReportFeeModel *model = [[ReportFeeModel alloc] init];
                    [model setStatus:status];
                    [model setAmountPrice:amountPrice];
                    [model setAmountPercent:amountPercent];
                    [model setPaymentPrice:paymentPrice];
                    [model setPaymentPercent:paymentPercent];
                    [model setCountStudent:countNumber];
                    
                    [reportFeeArray addObject:model];
                    
                }
                
                [xData removeAllObjects];
                [yData removeAllObjects];
                
                total = 0;
                totalHalf = 0;
                statusOwe = 0;
                statusHalfOwe = 0;
                statusHalfPaid = 0;
                statusPaid = 0;
                
                totalPerson = 0;
                countPeopleOwe = 0;
                countPeopleHalf = 0;
                countPeoplePaid = 0;
                
                percentOwe = 0;
                percentHalfOwe = 0;
                percentHalfPaid = 0;
                percentPaid = 0;
                percentTotalHalf = 0;
                
                
                
                for (ReportFeeModel *model in reportFeeArray) {
                    if (model.status == 0) {
                        statusOwe = model.amountPrice;
                        NSNumber *number1 = [NSNumber numberWithFloat:statusOwe];
                        NSString *stringNum1 = [numFormatter1 stringFromNumber:number1];
                        NSLog(@"%@", stringNum1);
                        self.oweLabel.text = [NSString stringWithFormat:@"%@ บ.", stringNum1];
                        
                        if (percentHalfOwe == 0 || percentHalfOwe == 100) {
                            percentOwe = 100;
                        }
                        else{
                            percentOwe = model.amountPercent;
                        }
                        
                        self.owePercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)", percentOwe];
                        
                        countPeopleOwe = model.countStudent;
                        NSNumber *countOwe = [NSNumber numberWithFloat:countPeopleOwe];
                        NSString *stringcountOwe = [numFormatter stringFromNumber:countOwe];
                        NSLog(@"%@", stringcountOwe);
                        self.countOweLabel.text = [NSString stringWithFormat:@"%@ คน", stringcountOwe];
 
                        
                    }
                    else if (model.status == 1) {
                       
                        statusHalfOwe = model.amountPrice;
                        NSNumber *number2 = [NSNumber numberWithFloat:statusHalfOwe];
                        NSString *stringNum2 = [numFormatter1 stringFromNumber:number2];
                        NSLog(@"%@", stringNum2);
                        
                        statusHalfPaid = model.paymentPrice;
                        NSNumber *number3 = [NSNumber numberWithFloat:statusHalfPaid];
                        NSString *stringNum3 = [numFormatter1 stringFromNumber:number3];
                        NSLog(@"%@", stringNum3);
                        
                        totalHalf = statusHalfOwe + statusHalfPaid;
                        NSNumber *number4 = [NSNumber numberWithFloat:totalHalf];
                        NSString *stringNum4 = [numFormatter1 stringFromNumber:number4];
                        NSLog(@"%@", stringNum4);
                        
                        percentHalfOwe = model.amountPercent;
                        percentHalfPaid = model.paymentPercent;
                        
                        percentTotalHalf = percentHalfOwe + percentHalfPaid;
                        
                        countPeopleHalf = model.countStudent;
                        NSNumber *countHalf = [NSNumber numberWithFloat:countPeopleHalf];
                        NSString *stringcountHalf = [numFormatter stringFromNumber:countHalf];
                        NSLog(@"%@", stringcountHalf);
                        
                        self.oweHalfLabel.text = [NSString stringWithFormat:@"%@ บ.", stringNum2];
                        self.oweHalfPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)", percentHalfOwe];
                        
                        self.paidHalfLabel.text = [NSString stringWithFormat:@"%@ บ.", stringNum3];
                        self.paidHalfPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)", percentHalfPaid];
                        
                        self.countHalfLabel.text = [NSString stringWithFormat:@"%@ คน", stringcountHalf];
                        
                    }
                    else if (model.status == 2) {
                        statusPaid = model.paymentPrice;
                        NSNumber *number5 = [NSNumber numberWithFloat:statusPaid];
                        NSString *stringNum5 = [numFormatter1 stringFromNumber:number5];
                        NSLog(@"%@", stringNum5);
                        self.paidLabel.text = [NSString stringWithFormat:@"%@ บ.", stringNum5];
                        
                        percentPaid = model.paymentPercent;
                        self.paidPercentLabel.text = [NSString stringWithFormat:@"(%.1f%%)", percentPaid];
                        
                        countPeoplePaid = model.countStudent;
                        NSNumber *countPaid = [NSNumber numberWithFloat:countPeoplePaid];
                        NSString *stringcountPaid = [numFormatter stringFromNumber:countPaid];
                        NSLog(@"%@", stringNum5);
                        self.countPaidLabel.text = [NSString stringWithFormat:@"%@ คน", stringcountPaid];
                    }
                }
            
                total = statusOwe + totalHalf + statusPaid;
                totalPerson = countPeopleOwe + countPeopleHalf + countPeoplePaid;
                
                NSNumber *number6 = [NSNumber numberWithFloat:total];
                NSString *stringNum6 = [numFormatter1 stringFromNumber:number6];
                NSLog(@"%@", stringNum6);
                
                //Count Student
                NSNumber *number7 = [NSNumber numberWithFloat:totalPerson];
                NSString *stringNum7 = [numFormatter stringFromNumber:number7];
                NSLog(@"%@", stringNum7);
                
                self.totalPriceLabel.text = [NSString stringWithFormat:@"%@", stringNum6];
                self.countLabel.text = [NSString stringWithFormat:@"%@", stringNum7];
                
                [xData addObject:@""];
                [xData addObject:@""];
                [xData addObject:@""];
        
                [yData addObject:[[NSNumber alloc] initWithFloat:percentOwe]];
                [yData addObject:[[NSNumber alloc] initWithFloat:percentTotalHalf]];
                [yData addObject:[[NSNumber alloc] initWithFloat:percentPaid]];
                
                [self setupGraphData];
            }
        }
        
    }];
    
    
    
}


- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
//    [self.revealViewController revealToggle:self.revealViewController];
    
}

- (IBAction)moveBack:(id)sender {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}

- (IBAction)openDialog:(id)sender {
    
    if(self.filterDialog != nil) {
        [self.filterDialog showDialogInView:self.view title:@"Filter"];
    }
    
}


- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialog alloc] init];
    }
    else if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)showFinishAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    else{
        
        //        alertDialog = [[AlertDialog alloc] initWithNibName:@"AlertDialog" bundle:nil];
        
        alertDialog = [[AlertDialog alloc] init];
        alertDialog.delegate = self;
    }
    
    [alertDialog showDialogInView:self.view title:title message:message];
    
}

- (void)onAlertDialogClose {
    
    ReportListTeacherViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportListTeacherStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
}


@end
