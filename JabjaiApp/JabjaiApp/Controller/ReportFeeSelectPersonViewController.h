//
//  ReportFeeSelectPersonViewController.h
//  JabjaiApp
//
//  Created by mac on 5/4/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"

@interface ReportFeeSelectPersonViewController : UIViewController <SlideMenuControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)moveBack:(id)sender;

@end
