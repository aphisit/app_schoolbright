//
//  ReportFeeViewController.h
//  JabjaiApp
//
//  Created by mac on 3/20/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JabjaiApp-Swift.h>
#import <Charts/Charts.h>

#import <MKDropdownMenu/MKDropdownMenu.h>

#import "SlideMenuController.h"

#import "SWRevealViewController.h"

#import "SelectYearAndTermDialog.h"

@interface ReportFeeViewController : UIViewController <ChartViewDelegate, SlideMenuControllerDelegate, SelectYearAndTermDialogDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet PieChartView *pieChart;

@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *termLabel;

@property (weak, nonatomic) IBOutlet UILabel *headUpdateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateNowLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeNowLabel;

@property (weak, nonatomic) IBOutlet UIStackView *totalStack;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *paidLabel;
@property (weak, nonatomic) IBOutlet UILabel *paidHalfLabel;
@property (weak, nonatomic) IBOutlet UILabel *oweHalfLabel;
@property (weak, nonatomic) IBOutlet UILabel *oweLabel;

@property (weak, nonatomic) IBOutlet UILabel *paidPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *paidHalfPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *oweHalfPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *owePercentLabel;

@property (weak, nonatomic) IBOutlet UILabel *countPaidLabel;
@property (weak, nonatomic) IBOutlet UILabel *countHalfLabel;
@property (weak, nonatomic) IBOutlet UILabel *countOweLabel;


@property (weak, nonatomic) IBOutlet UILabel *countLabel;

- (IBAction)openDrawer:(id)sender;
- (IBAction)moveBack:(id)sender;

- (IBAction)openDialog:(id)sender;


@end
