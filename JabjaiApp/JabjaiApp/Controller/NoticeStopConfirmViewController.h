//
//  NoticeStopConfirmViewController.h
//  JabjaiApp
//
//  Created by mac on 12/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeStopConfirmViewController : UIViewController

@property (nonatomic) NSString* noticeDetail;
@property (nonatomic) NSString* setnoticeDetail;
@property (nonatomic) NSMutableArray* imageArray;
@property (nonatomic) NSMutableArray* imageNotice;
@property (nonatomic) NSString* leaveCause;
@property (nonatomic) NSString* setleaveCause;

@property (nonatomic) NSString* leavingFirstDate;
@property (nonatomic) NSString* leavingLastDate;

@property (nonatomic) NSString* leaveFirstDate;
@property (nonatomic) NSString* setleaveFirstDate;
@property (nonatomic) NSString* leaveLastDate;
@property (nonatomic) NSString* setleaveLastDate;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *leaveName;
@property (weak, nonatomic) IBOutlet UILabel *position;
@property (weak, nonatomic) IBOutlet UILabel *leaveCauseitem;
@property (weak, nonatomic) IBOutlet UILabel *firstLeaveDate;
@property (weak, nonatomic) IBOutlet UILabel *lastLeaveDate;
@property (weak, nonatomic) IBOutlet UILabel *confirmDetail;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)moveback:(id)sender;
- (IBAction)confirmPage:(id)sender;



@end
