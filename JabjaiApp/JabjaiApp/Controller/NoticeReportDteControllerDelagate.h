//
//  NoticeReportDteControllerDelagate.h
//  JabjaiApp
//
//  Created by mac on 1/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

//#import <UIKit/UIKit.h>
//
//@interface NoticeReportDteControllerDelagate : UIViewController

#import <Foundation/Foundation.h>

@protocol NoticeReportDteControllerDelagate <NSObject>

//@optional
//- (void)onSelectSubject:(long long)subjectID subjectName:(NSString *)subjectName date:(NSDate *)date;

@optional
- (void)onSelectMessage:(long long)letterID;

@end
