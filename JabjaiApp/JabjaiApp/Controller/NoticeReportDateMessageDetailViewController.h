//
//  NoticeReportDateMessageDetailViewController.h
//  JabjaiApp
//
//  Created by mac on 1/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

#import "SlideMenuController.h"

@interface NoticeReportDateMessageDetailViewController : UIViewController <SlideMenuControllerDelegate>

@property (assign, nonatomic) long long messageID;
@property (assign, nonatomic) long long letterID;

@property (nonatomic) NSInteger page;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *leaveNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *causeLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;

@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContentConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMenuConstraint;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCollectionViewConstraint;

@property (weak, nonatomic) IBOutlet UILabel *headAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (weak, nonatomic) IBOutlet UILabel *headChangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *approveLabel;

@property (weak, nonatomic) IBOutlet UIButton *moreButton;


- (IBAction)moveBack:(id)sender;

@end
