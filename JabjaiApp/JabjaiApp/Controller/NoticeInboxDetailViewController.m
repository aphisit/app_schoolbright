//
//  NoticeInboxDetailViewController.m
//  JabjaiApp
//
//  Created by mac on 12/14/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NoticeInboxDetailViewController.h"
#import "NoticeInboxViewController.h"
#import "NoticeInboxDetailModel.h"
#import "APIURL.h"
#import "Utils.h"
#import "UserData.h"

#import "UserInfoViewController.h"

#import "MessageInboxScrollableTextDialogCollectionViewCell.h"

#import "AlertDialog.h"
#import "AlertDialogConfirm.h"

#import <QBImagePickerController/QBImagePickerController.h>
#import "AppDelegate.h"
#import "ZMImageSliderViewController.h"

#import "ERProgressHud.h"

@interface NoticeInboxDetailViewController (){
    
    NoticeInboxDetailModel *noticeInboxDetailModel;
    
    NSMutableArray<NoticeInboxDetailModel *> *noticeLeaveArray;
    
    NSInteger connectCounter;
    
    NSInteger approve;
    
    AlertDialogConfirm *alertDialog;
    
    NSString *address, *tumbon, *aumphur, *province, *phone;
    NSString *headtumbon, *headaumphur, *headprovince, *headphone;
    
    NSArray *returnedArray;
    
    NSMutableArray *imageArray;
    
    NSString *urlFile;
    
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;

@end

@implementation NoticeInboxDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
    
    self.leaveInboxDetail.lineBreakMode = NSLineBreakByWordWrapping;
    self.leaveInboxDetail.numberOfLines = 0;
    
    self.leaveInboxDetailAddress.lineBreakMode = NSLineBreakByWordWrapping;
    self.leaveInboxDetailAddress.numberOfLines = 0;
    
    self.dateFormatter = [Utils getDateFormatter];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    self.timeFormatter = [Utils getDateFormatter];
    [self.timeFormatter setDateFormat:@"HH:mm 'น.'"];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.heightMenuConstraint.constant = 120;
        self.acceptButtonConstraint.constant = 100;
        self.declineButtonConstraint.constant = 100;
        self.heightContentConstraint.constant = 1000;
    }
    else{
        self.heightMenuConstraint.constant = 70;
        self.acceptButtonConstraint.constant = 56;
        self.declineButtonConstraint.constant = 56;
        self.heightContentConstraint.constant = 800;
    }
    
    imageArray = [[NSMutableArray alloc] init];
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageInboxScrollableTextDialogCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    
    [self clearDisplay];
    [self getLetterConfirmDetailWithLetterID:self.letterID];
    
//    [self getImageNewsWithMessageID:self.letterID];
    
}

- (void)viewDidLayoutSubviews{
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getLetterConfirmDetailWithLetterID:(long long)letterID{

    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getLetterLeaveDetailWithLetterID:letterID userID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [[ERProgressHud sharedInstance] showDarkBackgroundViewWithTitle:@"กรุณารอสักครู่...."];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getLetterConfirmDetailWithLetterID:letterID];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[ERProgressHud sharedInstance] hide];
            });
            
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getLetterConfirmDetailWithLetterID:letterID];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getLetterConfirmDetailWithLetterID:letterID];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                if (noticeLeaveArray != nil) {
                    [noticeLeaveArray removeAllObjects];
                    noticeLeaveArray = nil;
                }
                
                noticeLeaveArray = [[NSMutableArray alloc] init];
                
                NSDateFormatter *formatter = [Utils getDateFormatter];
                [formatter setDateFormat:[Utils getXMLDateFormat]];
                
                NSDateFormatter *formatter2 = [Utils getDateFormatter];
                [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    NSMutableString *leaveName, *leavePosition, *leaveCause, *leaveStart, *leaveEnd, *leaveDetail, *leaveRequest;
                    
                    //leave name
                    if (![[dataDict objectForKey:@"senderName"] isKindOfClass:[NSNull class]]) {
                        leaveName = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderName"]];
                    }
                    else{
                        leaveName = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave position
                    if (![[dataDict objectForKey:@"senderJob"] isKindOfClass:[NSNull class]]) {
                        leavePosition = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"senderJob"]];
                    }
                    else{
                        leavePosition = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave cause
                    if (![[dataDict objectForKey:@"letterType"] isKindOfClass:[NSNull class]]) {
                        leaveCause = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"letterType"]];
                    }
                    else{
                        leaveCause = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave start
                    if (![[dataDict objectForKey:@"leaveStart"] isKindOfClass:[NSNull class]]) {
                        leaveStart = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"leaveStart"]];
                    }
                    else{
                        leaveStart = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave end
                    if (![[dataDict objectForKey:@"leaveEnd"] isKindOfClass:[NSNull class]]) {
                        leaveEnd = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"leaveEnd"]];
                    }
                    else{
                        leaveEnd = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave request
                    if (![[dataDict objectForKey:@"sendRequestDate"] isKindOfClass:[NSNull class]]) {
                        leaveRequest = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"sendRequestDate"]];
                    }
                    else{
                        leaveRequest = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    //leave detail
                    if (![[dataDict objectForKey:@"letterDescription"] isKindOfClass:[NSNull class]]) {
                        leaveDetail = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"letterDescription"]];
                    }
                    else{
                        leaveDetail = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    int status = [[dataDict objectForKey:@"status"] intValue];
                    
                    NSMutableString *season;
                    
                    if (![[dataDict objectForKey:@"Season"] isKindOfClass:[NSNull class]]) {
                        season = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Season"]];
                    }
                    else{
                        season = [[NSMutableString alloc] initWithString:@""];
                    }
                    
//                    int season = [[dataDict objectForKey:@"Season"] intValue];
                    
                    NSMutableString *address, *tumbon, *aumphur, *province, *phone;
                    
                    if (![[dataDict objectForKey:@"Aumpher"] isKindOfClass:[NSNull class]]) {
                        aumphur = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Aumpher"]];
                    }
                    else{
                        aumphur = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Tumbon"] isKindOfClass:[NSNull class]]) {
                        tumbon = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Tumbon"]];
                    }
                    else{
                        tumbon = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"HomeNumber"] isKindOfClass:[NSNull class]]) {
                        address = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"HomeNumber"]];
                    }
                    else{
                        address = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Province"] isKindOfClass:[NSNull class]]) {
                        province = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Province"]];
                    }
                    else{
                        province = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"Phone"] isKindOfClass:[NSNull class]]) {
                        phone = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Phone"]];
                    }
                    else{
                        phone = [[NSMutableString alloc] initWithString:@""];
                    }
                    
//                    NSMutableArray *fileImage;
//                    fileImage = [dataDict objectForKey:@"file"];
//                    NSLog(@"%@", fileImage);
//
//                    if (fileImage.count == 0) {
//                        self.heightCollectionViewConstraint.constant = 0;
//                        self.heightContentConstraint.constant = 650;
//                    }
//
//                    for (i=0; i < fileImage.count; i++) {
//                        NSString *picture = fileImage[i];
//
//                        NSLog(@"%@", picture);
//
//                        urlFile = picture;
//
//                        //                        urlFile = picture;
//                    }
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveName);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leavePosition);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveCause);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveStart);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveEnd);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveDetail);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveRequest);
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) address);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) tumbon);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) aumphur);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) province);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) phone);
                    
                    NSDate *leaveStartDate;
                    leaveStartDate = [formatter2 dateFromString:leaveStart];
                    
                    NSDate *leaveEndDate;
                    leaveEndDate = [formatter2 dateFromString:leaveEnd];
                    
                    NSRange dotRange = [leaveRequest rangeOfString:@"."];
                    
                    NSDate *leaveRequestDate;
                    
                    if(dotRange.length != 0) {
                        leaveRequestDate = [formatter dateFromString:leaveRequest];
                    }
                    else {
                        leaveRequestDate = [formatter2 dateFromString:leaveRequest];
                    }
                    
                    NoticeInboxDetailModel *model = [[NoticeInboxDetailModel alloc] init];
                    
                    model.leaveName = leaveName;
                    model.leavePosition = leavePosition;
                    model.leaveCause = leaveCause;
                    model.leaveStart = leaveStartDate;
                    model.leaveEnd = leaveEndDate;
                    model.leaveDetail = leaveDetail;
                    model.leaveDateRequest = leaveRequestDate;
                    
                    model.address = address;
                    model.tumbon = tumbon;
                    model.aumphur = aumphur;
                    model.province = province;
                    model.phone = phone;
                    
//                    model.file = fileImage;
                    
                    model.status = status;
                    
                    model.season = season;
                    
                    noticeInboxDetailModel = model;
                    [self performData];
                    
                    [noticeLeaveArray addObject:model];
                    
                    [self.collectionView reloadData];
                    
                }
                
            }
        }
        
    }];
    
}

- (void)performData{
    
    if (noticeInboxDetailModel != nil) {
        
        self.leaveInboxDetailName.text = noticeInboxDetailModel.leaveName;
        self.leaveInboxDetailPosiion.text = noticeInboxDetailModel.leavePosition;
        self.leaveInboxDetailCause.text = noticeInboxDetailModel.leaveCause;
//        self.leaveInboxDetailFirstDate.text = [self.dateFormatter stringFromDate:noticeInboxDetailModel.leaveStart];
//        self.leaveInboxDetailLastDate.text = [self.dateFormatter stringFromDate:noticeInboxDetailModel.leaveEnd];
        
        self.leaveInboxDetailFirstDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveStart];
    
        self.leaveInboxDetailRequestDate.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveDateRequest] , [self.timeFormatter stringFromDate:noticeInboxDetailModel.leaveDateRequest]];
        
        self.leaveInboxDetail.text = noticeInboxDetailModel.leaveDetail;
        
        if (noticeInboxDetailModel.status == 2) {
            self.leaveInboxAccept.enabled = FALSE;
            self.declineButtonConstraint.constant = 0;
        }
        else if (noticeInboxDetailModel.status == 3){
            self.leaveInboxDecline.enabled = FALSE;
            self.acceptButtonConstraint.constant = 0;
            self.leaveInboxAccept.hidden = YES;
        }
        
        
        address = noticeInboxDetailModel.address;
        tumbon = noticeInboxDetailModel.tumbon;
        aumphur = noticeInboxDetailModel.aumphur;
        province = noticeInboxDetailModel.province;
        phone = noticeInboxDetailModel.phone;
        
//        if ([self.leaveInboxDetailAddress.text length] != 0) {
        
        if ([address isEqual:@""] || [tumbon isEqual:@""] || [aumphur isEqual:@""] || [province isEqual:@""] || [phone isEqual:@""]) {
            
            self.headLeaveInboxAddressDetail.text = @"";
            
            headtumbon = @"";
            headaumphur = @"";
            headprovince = @"";
            headphone = @"";
            
            self.leaveInboxDetailAddress.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@" , address, headtumbon, tumbon, headaumphur, aumphur, headprovince, province, headphone, phone];
        
            self.heightContentConstraint.constant = 500;
            
        }
        else{
            
            headtumbon = @"ตำบล";
            headaumphur = @"อำเภอ";
            headprovince = @"จังหวัด";
            headphone = @"เบอร์โทรศัพท์";
            
            self.leaveInboxDetailAddress.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@" , address, headtumbon, tumbon, headaumphur, aumphur, headprovince, province, headphone, phone];
            
        }
        
        if ([noticeInboxDetailModel.season isEqual:@""]) {
            self.headChangeInboxDetail.text = @"ลาถึงวันที่";
            self.leaveInboxDetailLastDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveEnd];
        }
        else{
            if ([noticeInboxDetailModel.season isEqual:@"0"]) {
                self.headChangeInboxDetail.text = @"ช่วงเวลา";
                self.leaveInboxDetailLastDate.text = @"ลาครึ่งวันเช้า";
            }
            else if ([noticeInboxDetailModel.season isEqual:@"1"]) {
                self.headChangeInboxDetail.text = @"ช่วงเวลา";
                self.leaveInboxDetailLastDate.text = @"ลาครึ่งวันบ่าย";
            }
            else if ([noticeInboxDetailModel.season isEqual:@"-1"]) {
                self.headChangeInboxDetail.text = @"ลาถึงวันที่";
                self.leaveInboxDetailLastDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveEnd];
            }
        }
        
//        if (noticeInboxDetailModel.season == 0) {
//            self.headChangeInboxDetail.text = @"ช่วงเวลา";
//            self.leaveInboxDetailLastDate.text = @"ลาครึ่งวันเช้า";
//        }
//        else if (noticeInboxDetailModel.season == 1) {
//            self.headChangeInboxDetail.text = @"ช่วงเวลา";
//            self.leaveInboxDetailLastDate.text = @"ลาครึ่งวันบ่าย";
//        }
//        else{
//            self.headChangeInboxDetail.text = @"ลาถึงวันที่";
//            self.leaveInboxDetailLastDate.text = [Utils getThaiDateAcronymFormatWithDate:noticeInboxDetailModel.leaveEnd];
//        }
        
//        imageArray = noticeInboxDetailModel.file;
        
    }
    else{
        
        [self clearDisplay];
    }
    
}

- (void)clearDisplay {
    
}

- (IBAction)accept:(id)sender {
    
    approve = 1;
    
    [self getRequestLetterWithLetterID:self.letterID approve:approve];
    
    //    [self validateData];
}

- (IBAction)decline:(id)sender {
    
    approve = 0;
    
    [self getRequestLetterWithLetterID:self.letterID approve:approve];
    
//    [self validateData];
}

- (void)getRequestLetterWithLetterID:(long long)letterID approve:(NSInteger)approve{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getRequestLetterWithLetterID:letterID userID:userID approve:approve schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
//    [self showIndicator];
    
    [[ERProgressHud sharedInstance] showDarkBackgroundViewWithTitle:@"กรุณารอสักครู่...."];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
//            [self stopIndicator];
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getRequestLetterWithLetterID:letterID approve:approve];
            }
            else {
                connectCounter = 0;
            }
            
        }
        
        else{
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[ERProgressHud sharedInstance] hide];
            });
            
            [self validateData];
        }
    }];
    
    
    
}

- (void)validateData{
    
    NSString *alertMessage = @"ทำรายการสำเร็จ";
    [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
    
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)onAlertDialogClose{
    
    UserInfoViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

- (IBAction)moveBack:(id)sender {
    
    NoticeInboxViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeInboxStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}

#pragma mark - Indicator
- (void)showIndicator {
    // Show the indicator
    if(![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndicator {
    [self.indicator stopAnimating];
}

- (void)getImageNewsWithMessageID:(long long)messageID{
    
    //add image to array
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getImageNews:userID idMessage:messageID schoolid:schoolid];
    NSURL *URL = [NSURL URLWithString:URLString];
    imageArray = [[NSMutableArray alloc] init];
    
    [Utils downloadDataFromURL:URL withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        returnedArray = returnedData;
        if(returnedArray.count == 0){
            returnedArray = NULL;
            
            self.heightCollectionViewConstraint.constant = 0;
            
            [self.collectionView reloadData];
        }
        
        if (returnedArray != nil || returnedArray != NULL) {
            for (int i=0; i<returnedArray.count; i++) {
                [imageArray addObject:[[returnedArray objectAtIndex:i]objectForKey:@"filename"] ];
            }
            [self.collectionView reloadData];
        }
        
    }];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MessageInboxScrollableTextDialogCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSData *imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [imageArray objectAtIndex:indexPath.row]]];
    cell.imageNews.tag = indexPath.row;
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageWithData:imageData];
    
    [cell.imageNews setBackgroundImage:imageView.image forState:UIControlStateNormal];
    [cell.imageNews addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:   (UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(60, 100);
}

- (void)okButtonTapped:(UIButton *)sender {
    
    NSLog(@"index = %d",sender.tag);
    ZMImageSliderViewController*controller = [[ZMImageSliderViewController alloc] initWithOptions:sender.tag imageUrls:imageArray];
    [self presentViewController:controller animated:YES completion:nil];
}

@end
