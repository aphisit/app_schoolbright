//
//  NoticeReportDateMessageViewController.m
//  JabjaiApp
//
//  Created by mac on 1/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "NoticeReportDateMessageViewController.h"
#import "NoticeReportDateMessageDetailViewController.h"
#import "NoticeReportDateMessageTableViewCell.h"
#import "ProgressTableViewCell.h"

#import "SWRevealViewController.h"

#import "NoticeReportDateViewController.h"

#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"

@interface NoticeReportDateMessageViewController (){
    
    NoticeReportDateMessageModel *noticeReportDateMessageModel;
    
    NSMutableArray<NoticeReportDateMessageModel *> *leaveArray;
    
    NSInteger connectCounter;
    
    UIColor *acceptLeave;
    UIColor *declineLeave;
    UIColor *waitLeave;
    
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end


@implementation NoticeReportDateMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NoticeReportDateMessageTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    acceptLeave = [UIColor colorWithRed:0.13 green:0.86 blue:0.55 alpha:1.0];
    declineLeave = [UIColor colorWithRed:1.00 green:0.32 blue:0.32 alpha:1.0];
    waitLeave = [UIColor colorWithRed:0.26 green:0.78 blue:0.96 alpha:1.0];
    
//    SWRevealViewController *revealViewController = self.revealViewController;
//    if(revealViewController) {
//        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
//    }

    [self getUserLeaveDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(leaveArray == nil) {
        return 0;
    }
    else {
        return leaveArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NoticeReportDateMessageModel *model = [leaveArray objectAtIndex:indexPath.row];
    
    NoticeReportDateMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.leaveDateLabel.text = [self.dateFormatter stringFromDate:model.date];
    cell.leaveCauseLabel.text = model.leaveType;
    
    if (model.status == 0) {
        cell.statusColor.backgroundColor = declineLeave;
    }
    else if (model.status == 1){
        cell.statusColor.backgroundColor = acceptLeave;
    }
    else{
        cell.statusColor.backgroundColor = waitLeave;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NoticeReportDateMessageModel *model = [leaveArray objectAtIndex:indexPath.row];
    
    NoticeReportDateMessageDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeReportDateMessageDetailStoryboard"];
    
    viewController.letterID = model.letterID;
    viewController.page = 2;
    
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void)getUserLeaveDetail{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getUserLeaveDetailWithUserID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getUserLeaveDetail];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserLeaveDetail];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getUserLeaveDetail];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
//                if (leaveArray != nil) {
//                    [leaveArray removeAllObjects];
//                    leaveArray = nil;
//                }
                
                leaveArray = [[NSMutableArray alloc] init];
                
                
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long letterID = [[dataDict objectForKey:@"letterId"]longLongValue];
                    
                    int status = [[dataDict objectForKey:@"status"] intValue];
                    
                    NSMutableString *countDate, *leaveType;
                    
                    if (![[dataDict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                        leaveType = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"type"]];
                    }
                    else{
                        leaveType = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"totalLeaveDay"] isKindOfClass:[NSNull class]]) {
                        countDate = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"totalLeaveDay"]];
                    }
                    
                    else{
                        countDate = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getXMLDateFormat]];
                    
                    NSDateFormatter *formatter2 = [Utils getDateFormatter];
                    [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
                    
                    NSMutableString *leaveDate = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"Date"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveType);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) leaveDate);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) countDate);
                    
                    NSRange dotRange = [leaveDate rangeOfString:@"."];
                    
                    NSDate *messageDate;
                    
                    if(dotRange.length != 0) {
                        messageDate = [formatter dateFromString:leaveDate];
                    }
                    else {
                        messageDate = [formatter2 dateFromString:leaveDate];
                    }
                    
                    NoticeReportDateMessageModel *model = [[NoticeReportDateMessageModel alloc] init];
                    
                    model.letterID = letterID;
                    model.leaveType = leaveType;
                    model.date = messageDate;
                    model.countLeave = countDate;
                    model.status = status;
                    
                    noticeReportDateMessageModel = model;
                    
                    [self performData];
                    
                    [leaveArray addObject:model];
                    
                }
                // reverse date descending
//                NSArray *reverse =  [[leaveArray reverseObjectEnumerator] allObjects];
//                leaveArray = [[NSMutableArray alloc] initWithArray:reverse];
                
                [self.tableView reloadData];
            }
        }
        
    }];
    
}

- (void)performData{
    
    if (noticeReportDateMessageModel != nil) {
        self.dateCount.text = noticeReportDateMessageModel.countLeave;
    }
    else{
        
        [self clearDisplay];
    }
    
}

- (void)clearDisplay {
        self.dateCount.text = @"0";
    }

- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];
    
//    [self.revealViewController revealToggle:self.revealViewController];
}

- (IBAction)openCalendar:(id)sender {
    
    NoticeReportDateViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeReportStoryboard"];
    
    [self.slideMenuController changeMainViewController:viewController close:YES];
//    [self.revealViewController pushFrontViewController:viewController animated:YES];
}



@end
