//
//  NoticeInboxViewController.h
//  JabjaiApp
//
//  Created by mac on 12/7/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideMenuController.h"
#import "LIDetailLeaveViewControllerXIB.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface NoticeInboxViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate,LIDetailLeaveViewControllerXIBDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *sortDataBtn;

@property (weak, nonatomic) IBOutlet UILabel *showNoShopLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMenuConstraint;
- (IBAction)openDrawer:(id)sender;

@end
