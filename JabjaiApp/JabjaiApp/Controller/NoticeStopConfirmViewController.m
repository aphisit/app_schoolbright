//
//  NoticeStopConfirmViewController.m
//  JabjaiApp
//
//  Created by mac on 12/25/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import "NoticeStopConfirmViewController.h"
#import "NoticeStopDetailViewController.h"
#import "NoticeStopViewController.h"
#import "UserInfoViewController.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "AlertDialogConfirm.h"
#import "NoticeStopConfirmTeacherModel.h"
#import "Utils.h"
#import "APIURL.h"
#import <AFNetworking/AFNetworking.h>
#import <QBImagePickerController/QBImagePickerController.h>

@interface NoticeStopConfirmViewController (){
    
    NSString *jsonString;
    AlertDialogConfirm *alertDialog;
    PHImageRequestOptions *requestOptions;
    UIImage *imager;
    NSInteger *num;
    NSData *dataImage;
    
    NSInteger leaveid;
    
    NSInteger connectCounter;
    NSBundle *myLangBundle;
}

@end

static NSString *cellIdentifier = @"Cell";

@implementation NoticeStopConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    myLangBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[UserData getChangLanguage]lowercaseString] ofType:@"lproj"]];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    self.leaveName.text = [NSString stringWithFormat:@"%@ %@", [UserData getFirstName] , [UserData getLastName]];
    
    if ([UserData getUserType] != STUDENT) {
        self.position.text = @"อาจารย์";
    }
    else{
        self.position.text = @"นักเรียน";
    }
    
    self.leaveCauseitem.text = self.leaveCause;
    self.firstLeaveDate.text = self.leaveFirstDate;
    self.lastLeaveDate.text = self.leaveLastDate;
    
    self.confirmDetail.text = self.noticeDetail;
    self.confirmDetail.lineBreakMode = NSLineBreakByWordWrapping;
    self.confirmDetail.numberOfLines = 0;
    
    [self stopIndiCator];
    
}

-(void)setLanguage{
 
//    _usernameTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LOGIN_ID_STUDEN",nil,myLangBundle,nil);
//    _passwordTextField.placeholder = NSLocalizedStringFromTableInBundle(@"PLACEHOLDER_LOGIN_BIRTHDAY",nil,myLangBundle,nil);
//    [_loginBtn setTitle:NSLocalizedStringFromTableInBundle(@"BUTTON_LOGIN",nil,myLangBundle,nil) forState:UIControlStateNormal];
//    [_signupBtn setTitle:NSLocalizedStringFromTableInBundle(@"BUTTON_SINGUP",nil,myLangBundle,nil) forState:UIControlStateNormal];
}

- (void)viewDidLayoutSubviews{
    
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_imageNotice count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //    static NSString *cellIdentifier = @"Cell";
    NoticeStopConfirm2CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.imageNotice.tag = 200;
    cell.imageNotice.image = [self.imageNotice objectAtIndex:indexPath.row];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:   (UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(50, 50);
}

- (void)validateData{
    
    [self stopIndiCator];
    NSString *alertMessage = NSLocalizedStringFromTableInBundle(@"DIALOG_LABEL_LEAVE_SUCCESS",nil,myLangBundle,nil);
    [self showAlertDialogWithTitle:@"แจ้งเตือน" message:alertMessage];
    
}

#pragma mark - Dialog
- (void)showAlertDialogWithTitle:(NSString *)title message:(NSString *)message {
    
    if(alertDialog == nil) {
        alertDialog = [[AlertDialogConfirm alloc] init];
        alertDialog.delegate = self;
    }
    if(alertDialog != nil && [alertDialog isDialogShowing]) {
        [alertDialog dismissDialog];
    }
    [alertDialog showDialogInView:self.view title:title message:message];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)onAlertDialogClose{
    
    NoticeStopViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeStopStoryboard"];
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}

- (void)showIndicator{
    
    if (![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
}

- (void)stopIndiCator{
    
    [self.indicator stopAnimating];
}

- (IBAction)moveback:(id)sender {
    
    NoticeStopDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NoticeStopDetailStoryboard"];
    
    viewController.leaveCause = self.leaveCause;
    viewController.leaveFirstDate = self.leaveFirstDate;
    viewController.leaveLastDate = self.leaveLastDate;
    viewController.noticeDetail = self.setnoticeDetail;
    
    [self.revealViewController pushFrontViewController:viewController animated:YES];
    
}

- (IBAction)confirmPage:(id)sender {
    
    [self showIndicator];
    
    [self validateData];
    
}

@end
