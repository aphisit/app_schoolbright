//
//  NoticeReportDateMessageViewController.h
//  JabjaiApp
//
//  Created by mac on 1/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "NoticeReportDateMessageModel.h"

#import "CFSCalendar.h"

#import "SlideMenuController.h"

@interface NoticeReportDateMessageViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate>

@property (nonatomic) long long letterID;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *dateCount;




- (IBAction)openDrawer:(id)sender;
- (IBAction)openCalendar:(id)sender;



@end
