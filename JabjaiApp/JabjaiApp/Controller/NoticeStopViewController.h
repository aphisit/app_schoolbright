//
//  NoticeStopViewController.h
//  JabjaiApp
//
//  Created by mac on 11/20/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfoDBHelper.h"
#import "SWRevealViewController.h"
#import "CalendarDialog.h"
#import "TableListDialog.h"
#import "SlideMenuController.h"
#import "UnAuthorizeMenuDialog.h"
#import "CallGetMenuListAPI.h"
#import "LoginViewController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface NoticeStopViewController : UIViewController <UITextFieldDelegate , TableListDialogDelegate ,CalendarDialogDelegate, SlideMenuControllerDelegate,UnAuthorizeMenuDialogDelegate,CallGetMenuListAPIDelegate,CallCRGetStatusClosedForRenovationAPIDelegate>

@property (nonatomic) NSString *leaveCause;
@property (nonatomic) NSString *leaveFirstDate;
@property (nonatomic) NSString *leaveLastDate;

@property (nonatomic) NSDate *leaveStartdate;
@property (nonatomic) NSDate *leaveEndDate;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLeaveLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerReasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDatefromLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEndleaveLabel;

@property (weak, nonatomic) IBOutlet UITextField *cause;
@property (weak, nonatomic) IBOutlet UITextField *firstdate;
@property (weak, nonatomic) IBOutlet UITextField *lastdate;

@property (weak, nonatomic) IBOutlet UIButton *headerNextbtn;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIView *section2;
@property (weak, nonatomic) IBOutlet UIView *section3;

- (IBAction)selectCauseAction:(id)sender;
- (IBAction)selectFirstdateAction:(id)sender;


- (IBAction)selectLastdateAction:(id)sender;


- (IBAction)openDrawer:(id)sender;
- (IBAction)nextPage:(id)sender;


@end
