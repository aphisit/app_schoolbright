//
//  SchoolFeeListViewController.h
//  JabjaiApp
//
//  Created by mac on 3/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"
#import "SWRevealViewController.h"
#import "TFTuitionDetailViewController.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
@interface SchoolFeeListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SlideMenuControllerDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMenuConstraint;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPaymentLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)openDrawer:(id)sender;

@end
