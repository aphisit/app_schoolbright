//
//  SchoolFeeListViewController.m
//  JabjaiApp
//
//  Created by mac on 3/12/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "SchoolFeeListViewController.h"
#import "SchoolFeeListModel.h"
#import "SchoolFeeListTableViewCell.h"
#import "Utils.h"
#import "APIURL.h"
#import "Constant.h"
#import "UserData.h"

@interface SchoolFeeListViewController (){
    
    NSMutableArray <SchoolFeeListModel *> *feeArray;
    
    NSInteger connectCounter;
    
    UIColor *waitColor;
    UIColor *paidColor;
    UIColor *oweColor;
    
    UIColor *grayColor;
    
    UIView *view;
    
    NSDate *now , *due , *issue;
    
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) CallCRGetStatusClosedForRenovationAPI *callCRGetStatusClosedForRenovationAPI;
@property (nonatomic, strong) CRClosedForRenovationDialog *cRClosedForRenovationDialog;
@end

@implementation SchoolFeeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callGetStatusServer];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SchoolFeeListTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.dateFormatter = [Utils getDateFormatter];
    self.dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    grayColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    
    waitColor = [UIColor colorWithRed:0.95 green:0.43 blue:0.00 alpha:1.0]; // orange
    paidColor = [UIColor colorWithRed:0.27 green:0.71 blue:0.26 alpha:1.0]; // green
    oweColor = [UIColor colorWithRed:0.93 green:0.23 blue:0.23 alpha:1.0]; // red
    
//    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = grayColor;
    SWRevealViewController *revealController = self.revealViewController;
    
    if (revealController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width - 30;
    }
    
    [self getInvoiceList];
    [self setLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    [self doDesignLayout];
}

//////////////////////////////////////////Check Status Server///////////////////////////////////////////////////
#pragma mark - CallCRGetStatusClosedForRenovationAPI
- (void)callGetStatusServer{
    if(self.callCRGetStatusClosedForRenovationAPI != nil) {
        self.callCRGetStatusClosedForRenovationAPI = nil;
    }
    self.callCRGetStatusClosedForRenovationAPI = [[CallCRGetStatusClosedForRenovationAPI alloc] init];
    self.callCRGetStatusClosedForRenovationAPI.delegate = self;
    [self.callCRGetStatusClosedForRenovationAPI callStatusServer];
}

- (void)callCRGetStatusClosedForRenovationAPI:(CallCRGetStatusClosedForRenovationAPI *)classObj statusCode:(NSInteger)statusCode message:(NSString *)message success:(BOOL)success{
    if (success) {
        if (statusCode != 200) {
            [self showClosedForRenovationDialog:message];
            NSLog(@"Server not work");
        }
    }
}

#pragma mark - CRClosedForRenovationDialog
- (void)showClosedForRenovationDialog:(NSString*)message{
    if(self.cRClosedForRenovationDialog != nil) {
        self.cRClosedForRenovationDialog = nil;
    }
    self.cRClosedForRenovationDialog = [[CRClosedForRenovationDialog alloc] init];
    [self.cRClosedForRenovationDialog showDialogInView:self.view title:@"แจ้งเตือน" message:message];
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) doDesignLayout{
    //set color header
    [self.headerView layoutIfNeeded];
    CAGradientLayer  *gradient = [Utils getGradientColorHeader];
    gradient.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradient atIndex:0];
}

- (void)setLanguage{
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE",nil,[Utils getLanguage],nil);
    self.headerPaymentLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_ALERT_PAYMENT",nil,[Utils getLanguage],nil);
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(feeArray == nil) {
        return 0;
    }
    else {
        return feeArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SchoolFeeListModel *model = [feeArray objectAtIndex:indexPath.row];
    
    SchoolFeeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.issueDateLabel.text = [self.dateFormatter stringFromDate:model.dateIssue] ;
    cell.termLabel.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_SEMESTER",nil,[Utils getLanguage],nil), model.termName];
    cell.yearLabel.text = [NSString stringWithFormat:@"%@ : %@", NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_YEAR",nil,[Utils getLanguage],nil),model.yearName];
    
    NSNumber *num = [NSNumber numberWithFloat:model.totalPrice];
    
    NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numFormatter setUsesSignificantDigits:NO];
    [numFormatter setMaximumFractionDigits:2];
    [numFormatter setMinimumFractionDigits:2];
    [numFormatter setUsesGroupingSeparator:YES];
    [numFormatter setGroupingSeparator:@","];
    [numFormatter setDecimalSeparator:@"."];
    [numFormatter setGroupingSize:3];
    NSString *stringNum = [numFormatter stringFromNumber:num];
    NSLog(@"%@", stringNum);
    
    cell.totalPriceLabel.text = [[NSString alloc] initWithFormat:@"%@ %@ %@",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_INFROM_PAYMENT",nil,[Utils getLanguage],nil), stringNum,NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_BAHT",nil,[Utils getLanguage],nil)];
    
    if (model.status == 0) {
        due = model.dateDue;
        issue = model.dateIssue;
        now = [NSDate date];
        // Compare Date
        if ([now compare:due] == NSOrderedAscending) {
            NSLog(@"xx");
            cell.dueDateLabel.hidden = NO;
            cell.dueDateLabel.text = [NSString stringWithFormat:@"%@ ( %@ )",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_INFROM_DUE_DATE",nil,[Utils getLanguage],nil),[Utils dateInStringFormat:model.dateDue format:@"dd/MM/yyyy"]] ;
            cell.statusColor.backgroundColor = waitColor;
            //cell.statusFeeLabel.textColor = oweColor;
            cell.statusFeeLabel.text = @" ";
        }else if ([now compare:due] == NSOrderedDescending){
            NSLog(@"xx");
            cell.dueDateLabel.hidden = NO;
            cell.statusColor.backgroundColor = oweColor;
            cell.dueDateLabel.text = [NSString stringWithFormat:@"%@ ( %@ )",NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_OVERDUE_SINCE",nil,[Utils getLanguage],nil),[Utils dateInStringFormat:model.dateDue format:@"dd/MM/yyyy"]] ;
            cell.statusFeeLabel.textColor = oweColor;
            cell.statusFeeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_OVERDUE",nil,[Utils getLanguage],nil);
        }
//        cell.statusFeeLabel.textColor = oweColor;
//        cell.statusFeeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_OVERDUE",nil,[Utils getLanguage],nil);
    }
    else if (model.status == 1){
        cell.statusColor.backgroundColor = paidColor;
        cell.statusFeeLabel.textColor = paidColor;
        cell.dueDateLabel.hidden = YES;
        cell.statusFeeLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_TUITION_FEE_PAID",nil,[Utils getLanguage],nil);
    }
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SchoolFeeListModel *model = [feeArray objectAtIndex:indexPath.row];
    TFTuitionDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TFTuitionDetailStoryboard"];
    viewController.invoiceID = model.invoiceID;
    viewController.studentID = [UserData getUserID];
    viewController.schoolID = [UserData getSchoolId];
    [self.slideMenuController changeMainViewController:viewController close:YES];
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    SchoolFeeListModel *model = [feeArray objectAtIndex:indexPath.row];
    NSInteger sizeHeight = 0;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
     {
         if (model.status == 0) {
             due = model.dateDue;
             issue = model.dateIssue;
             now = [NSDate date];
             if ([now compare:due] == NSOrderedAscending) {
                 NSLog(@"xx");
                 sizeHeight = 170;
             }else if ([now compare:due] == NSOrderedDescending){
                 NSLog(@"xx");
                 sizeHeight = 150;
             }
         }else{
             sizeHeight = 150;
         }
        // return 150;
     }else{
         if (model.status == 0) {
             due = model.dateDue;
             issue = model.dateIssue;
             now = [NSDate date];
             if ([now compare:due] == NSOrderedAscending) {
                 NSLog(@"xx");
                 sizeHeight = 250;
             }else if ([now compare:due] == NSOrderedDescending){
                 NSLog(@"xx");
                 sizeHeight = 200;
             }

         }else{
             sizeHeight = 200;
         }
         
         //return 200;
     }
    
    return sizeHeight;
}

- (void)getInvoiceList{
    
    long long userID = [UserData getUserID];
    long long schoolid = [UserData getSchoolId];
    NSString *URLString = [APIURL getSchoolFeeListWithUserID:userID schoolid:schoolid];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [Utils downloadDataFromURL:url withCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil || data == nil || ([(NSHTTPURLResponse *)response statusCode]) != 200) {
            
            if(error != nil) {
                NSLog(@"An error occured : %@" , [error localizedDescription]);
            }
            else if(data == nil) {
                NSLog(@"Data is null");
            }
            else {
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
                NSLog(@"HTTP status code = %d", (int)HTTPStatusCode);
            }
            
            if(connectCounter < TRY_CONNECT_MAX) {
                connectCounter++;
                
                NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                [self getInvoiceList];
            }
            else {
                connectCounter = 0;
            }
            
        }
        else {
            id returnedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(error != nil) {
                NSLog(@"An error occured : %@", [error localizedDescription]);
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getInvoiceList];
                }
                else {
                    connectCounter = 0;
                }
            }
            else if(![returnedData isKindOfClass:[NSArray class]]) {
                
                if(connectCounter < TRY_CONNECT_MAX) {
                    connectCounter++;
                    
                    NSLog(@"Error try to connect #%@", [@(connectCounter) stringValue]);
                    [self getInvoiceList];
                }
                else {
                    connectCounter = 0;
                }
                
            }
            else{
                NSArray *returnedArray = returnedData;
                connectCounter = 0;
                
                //                if (leaveArray != nil) {
                //                    [leaveArray removeAllObjects];
                //                    leaveArray = nil;
                //                }
                
                feeArray = [[NSMutableArray alloc] init];
                
                
                for (int i=0; i<returnedArray.count; i++) {
                    NSDictionary *dataDict = [returnedArray objectAtIndex:i];
                    
                    long long invoiceID = [[dataDict objectForKey:@"invoices_Id"]longLongValue];
                    
                    int status = [[dataDict objectForKey:@"status"] intValue];
                    
                    float total = [[dataDict objectForKey:@"totalprice"] floatValue];
                    
                    NSMutableString *term, *year;
                    
//                    NSMutableString *status;
                    
                    if (![[dataDict objectForKey:@"trem"] isKindOfClass:[NSNull class]]) {
                        term = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"trem"]];
                    }
                    else{
                        term = [[NSMutableString alloc] initWithString:@""];
                    }
                    
                    if (![[dataDict objectForKey:@"yesr"] isKindOfClass:[NSNull class]]) {
                        year = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"yesr"]];
                    }
                    else{
                        year = [[NSMutableString alloc] initWithString:@""];
                    }

                    NSDateFormatter *formatter = [Utils getDateFormatter];
                    [formatter setDateFormat:[Utils getXMLDateFormat]];
                    NSDateFormatter *formatter2 = [Utils getDateFormatter];
                    [formatter2 setDateFormat:[Utils getSeverDateTimeFormat]];
 
                    NSMutableString *dueDate = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"dueDate"]];
                    NSMutableString *issueDate = [[NSMutableString alloc] initWithFormat:@"%@", [dataDict objectForKey:@"issuedDate"]];
                    
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) term);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) year);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) dueDate);
                    CFStringTrimWhitespace((__bridge CFMutableStringRef) issueDate);
                    
                    NSRange dotRange = [dueDate rangeOfString:@"."];
                    NSRange dotRange2 = [issueDate rangeOfString:@"."];
                    NSDate *dueDay, *issueDay;
                    
                    if(dotRange.length != 0) {
                        dueDay = [formatter dateFromString:dueDate];
                    }
                    else {
                        dueDay = [formatter2 dateFromString:dueDate];
                    }
                    if(dotRange2.length != 0) {
                        issueDay = [formatter dateFromString:issueDate];
                    }
                    else {
                        issueDay = [formatter2 dateFromString:issueDate];
                    }
                    
                    SchoolFeeListModel *model = [[SchoolFeeListModel alloc] init];
                    model.invoiceID = invoiceID;
                    model.yearName = year;
                    model.termName = term;
                    model.status = status;
                    model.totalPrice = total;
                    model.dateDue = dueDay;
                    model.dateIssue = issueDay;
                    [feeArray addObject:model];
                    
                }

                [self.tableView reloadData];
            }
        }
        
    }];
    
}

- (IBAction)openDrawer:(id)sender {
    
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController toggleLeft];

}
@end
