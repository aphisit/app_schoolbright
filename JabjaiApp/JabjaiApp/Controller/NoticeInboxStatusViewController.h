//
//  NoticeInboxStatusViewController.h
//  JabjaiApp
//
//  Created by mac on 5/14/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideMenuController.h"

@interface NoticeInboxStatusViewController : UIViewController <SlideMenuControllerDelegate>

@property (assign, nonatomic) long long letterID;
@property (nonatomic) NSInteger page;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassroomLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPositionLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerReasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerStarDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerEndDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSubmitDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerAttactImageLabel;

@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailName;
@property (weak, nonatomic) IBOutlet UILabel *leaveInClassRoomLabel;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailPosiion;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailCause;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailFirstDate;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailLastDate;
@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailRequestDate;
@property (weak, nonatomic) IBOutlet UITextView *leaveDetailTextView;
@property (weak, nonatomic) IBOutlet UILabel *coronClassroomLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconClassroomImg;

//@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetail;

@property (weak, nonatomic) IBOutlet UIView *headMenuBar;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContentConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pointClassroomConstraint;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMenuConstraint;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *leaveInboxDetailAddress;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCollectionViewConstraint;
@property (weak, nonatomic) IBOutlet UIView *declineBackGround;
@property (weak, nonatomic) IBOutlet UIView *acceptBackGround;
@property (weak, nonatomic) IBOutlet UIView *statusBackGround;
@property (weak, nonatomic) IBOutlet UILabel *statusLeaveLabel;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIButton *declineButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelLeaveButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightButtonMenuConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightStatusConstraint;

@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIView *statusContent;

- (IBAction)moveback:(id)sender;

@end
