//
//  ReportBehaviorAllDataSelectViewController.h
//  JabjaiApp
//
//  Created by mac on 4/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TableListDialog.h"
#import "CallGetSchoolClassLevelAPI.h"
#import "CallGetSchoolClassroomAPI.h"
#import "CallCRGetStatusClosedForRenovationAPI.h"
#import "CRClosedForRenovationDialog.h"
#import "SchoolLevelModel.h"
#import "SchoolClassroomModel.h"
#import "BSStatusModel.h"
#import "SlideMenuController.h"

@interface ReportBehaviorAllDataSelectViewController : UIViewController <UITextFieldDelegate, TableListDialogDelegate, CallGetSchoolClassLevelAPIDelegate, CallGetSchoolClassroomAPIDelegate, SlideMenuControllerDelegate, CallCRGetStatusClosedForRenovationAPIDelegate>

@property (strong, nonatomic) NSArray<SchoolLevelModel *> *classLevelArray;
@property (strong, nonatomic) NSArray<SchoolClassroomModel *> *classroomArray;
@property (strong, nonatomic) NSArray<BSStatusModel *> *statusArray;
@property (nonatomic) NSInteger selectedClassLevelIndex;
@property (nonatomic) NSInteger selectedClassroomIndex;
@property (nonatomic) NSInteger selectedStatusIndex;
@property (nonatomic) long long classLevelId;
@property (nonatomic) long long classroomId;
@property (nonatomic) NSInteger statusId;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerClassLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerStatusLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITextField *classLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *classroomTextField;
@property (weak, nonatomic) IBOutlet UITextField *statusTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *section1;
@property (weak, nonatomic) IBOutlet UIView *section2;
@property (weak, nonatomic) IBOutlet UIView *section3;


- (IBAction)moveBack:(id)sender;
- (IBAction)actionNext:(id)sender;

@end
