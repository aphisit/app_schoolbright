//
//  ACLocationModel.h
//  JabjaiApp
//
//  Created by Mac on 11/11/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ACLocationModel : NSObject
@property (nonatomic,assign) double latitude;
@property (nonatomic,assign) double longitude;
@property (nonatomic,assign) int statusAccept;
@property (nonatomic,assign) int statusLacation;
@property (nonatomic,assign) int statusIsActive;

@end

NS_ASSUME_NONNULL_END
