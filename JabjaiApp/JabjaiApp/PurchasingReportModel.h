//
//  PurchasingReportModel.h
//  JabjaiApp
//
//  Created by mac on 12/26/2560 BE.
//  Copyright © 2560 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PurchasingReportModel : NSObject

@property (strong ,nonatomic) NSDate *purchasingList;

@end
