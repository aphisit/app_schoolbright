//
//  ARDetailModel.h
//  JabjaiApp
//
//  Created by toffee on 19/3/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ARDetailModel : NSObject
@property (nonatomic) NSString *term;
@property (nonatomic) NSString *gradeAverage;
@property (nonatomic) NSInteger year;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *studentId;
@property (nonatomic) NSString *branch;
@property (nonatomic) NSString *levelName;
@property (nonatomic) NSString *subLevelName;
@property (nonatomic) NSString *schoolNameTH;
@property (nonatomic) NSString *schoolNameEN;
@property (nonatomic) NSString *schoolNameImg;
@property (nonatomic) NSArray *basicCoursesArray;
@property (nonatomic) NSArray *additionalCoursesArray;
@property (nonatomic) NSArray *eventCoursesArray;
@property (nonatomic) NSArray *noCountCreditArray;

-(void) setTerm:(NSString*)term;
-(void) setGradeAverage:(NSString*)gradeAverage;
-(void) setYear:(NSInteger)year;
-(void) setName:(NSString * _Nonnull)name;
-(void) setStudentId:(NSString * _Nonnull)studentId;
-(void) setBranch:(NSString * _Nonnull)branch;
-(void) setLevelName:(NSString * _Nonnull)levelName;
-(void) setSubLevelName:(NSString * _Nonnull)subLevelName;
-(void) setSchoolNameTH:(NSString * _Nonnull)schoolNameTH;
-(void) setSchoolNameEN:(NSString * _Nonnull)schoolNameEN;
-(void) setSchoolNameImg:(NSString * _Nonnull)schoolNameImg;
-(void) setBasicCoursesArray:(NSArray * _Nonnull)basicCoursesArray;
-(void) setAdditionalCoursesArray:(NSArray * _Nonnull)additionalCoursesArray;
-(void) setEventCoursesArray:(NSArray * _Nonnull)eventCoursesArray;
-(void) setNoCountCreditArray:(NSArray * _Nonnull)noCountCreditArray;

-(NSString*)getTerm;
-(NSString*)getGradeAverage;
-(NSInteger)getYear;
-(NSString*)getName;
-(NSString*)getStudentId;
-(NSString*)getBranch;
-(NSString*)getLevelName;
-(NSString*)getSubLevelName;
-(NSString*)getSchoolNameTH;
-(NSString*)getSchoolNameEN;
-(NSString*)getSchoolImg;
-(NSArray*)getBasicCoursesArray;
-(NSArray*)getAdditionalCoursesArray;
-(NSArray*)getEventCoursesArray;
-(NSArray*)getNoCountCreditArray;

@end

NS_ASSUME_NONNULL_END
