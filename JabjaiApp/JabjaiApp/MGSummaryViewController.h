//
//  MGSummaryViewController.h
//  JabjaiApp
//
//  Created by toffee on 4/20/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SWRevealViewController.h"
#import "MGListMenuViewController.h"
#import "MGListMargetViewController.h"
#import "CallUpdateMargetPOSTAPI.h"
#import "MGSuccessAlertDialog.h"
#import "SlideMenuController.h"


@interface MGSummaryViewController : UIViewController <CallUpdateMargetPOSTAPIDelegate, MGSuccessAlertDialogDelegate, SlideMenuControllerDelegate>
@property (nonatomic) NSMutableArray* orderArray;
@property (nonatomic) NSMutableArray* amountOrderArray;
@property (nonatomic) NSMutableArray* totalPriceOrderArray;
@property (nonatomic) NSInteger totalPrice;
@property (nonatomic) NSString* orderStringJson;
@property (nonatomic) long long shopId;
@property (nonatomic) NSString* nameShop;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *numberOrder;
@property (weak, nonatomic) IBOutlet UILabel *menuOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;

- (IBAction)moveBack:(id)sender;
- (IBAction)actionSubmit:(id)sender;

@end
