//
//  MessageInboxDialog.h
//  JabjaiApp
//
//  Created by mac on 11/24/2559 BE.
//  Copyright © 2559 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MessageInboxDialogDelegate <NSObject>

@optional
-(void)messageInboxDialogClose;

@end

@interface MessageInboxDialog : UIViewController

@property (nonatomic, retain) id<MessageInboxDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

- (IBAction)closeDialog:(id)sender;

- (void)showDialogInView:(UIView *)targetView title:(NSString *)title message:(NSString *)message messageDate:(NSDate *)messageDate;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
