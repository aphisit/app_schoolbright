//
//  TFTuitionDetailViewController.h
//  JabjaiApp
//
//  Created by Mac on 2/10/2563 BE.
//  Copyright © 2563 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallTFGetTuitionDetailAPI.h"
#import "TFInvoicesModel.h"
#import "SchoolFeePriceListTableViewCell.h"
#import "TFInvoicesTableViewCell.h"
#import "SchoolFeeListViewController.h"
#import "TFPaymentViewController.h"
#import "AlertDialog.h"
NS_ASSUME_NONNULL_BEGIN

@interface TFTuitionDetailViewController : UIViewController <CallTFGetTuitionDetailAPIDelegate, UITableViewDelegate, UITableViewDataSource>

@property (assign, nonatomic) long long invoiceID;
@property (assign, nonatomic) long long studentID;
@property (assign, nonatomic) long long schoolID;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerInstallmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerRemainingBalanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPaymentLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPrompayLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerCardLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerPayBtn;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *schoolNameTHLabel;
@property (weak, nonatomic) IBOutlet UILabel *schoolNameENLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *termLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *classLabel;
@property (weak, nonatomic) IBOutlet UILabel *classroomLabel;
@property (weak, nonatomic) IBOutlet UITableView *orderTableView;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UITableView *installmentTableView;
@property (weak, nonatomic) IBOutlet UILabel *remainingBalanceLabel;


@property (weak, nonatomic) IBOutlet UILabel *statusPaymentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *schoolImage;
@property (weak, nonatomic) IBOutlet UIView *orderLineView;

//payment view
@property (weak, nonatomic) IBOutlet UIButton *radioPromptpayBtn;
@property (weak, nonatomic) IBOutlet UIButton *radioCreditBtn;
@property (weak, nonatomic) IBOutlet UIButton *paymentBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paymentHighViewConstraint;
@property (weak, nonatomic) IBOutlet UIView *paymentView;
- (IBAction)selectPaymentTypeAction:(id)sender;

- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderTableViewHighConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *installmentTableViewHighConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *closeInstallmentConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)paymentAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
