//
//  JHSelectRoomTableListViewCell.h
//  JabjaiApp
//
//  Created by toffee on 1/22/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@class JHSelectRoomTableListViewCell;

@protocol JHSelectRoomRadioTableViewCellDelegate <NSObject>

- (void)onPressRadioButton:(JHSelectRoomTableListViewCell *)tableViewCell atIndex:(NSInteger)index;

@end
@interface JHSelectRoomTableListViewCell : UITableViewCell

@property (weak, nonatomic) id<JHSelectRoomRadioTableViewCellDelegate> delegate;
@property (nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UILabel *label;
//@property (weak, nonatomic) IBOutlet UIButton *radioButton;
@property (weak, nonatomic) IBOutlet UIImageView *radioButton;

- (void)setRadioButtonSelected;
- (void)setRadioButtonSelectedWithType:(int) type;
- (void)setRadioButtonClearSelected;

//- (IBAction)actionPressRadioButton:(id)sender;

@end
