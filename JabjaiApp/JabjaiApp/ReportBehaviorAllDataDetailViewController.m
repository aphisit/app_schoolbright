//
//  ReportBehaviorAllDataDetailViewController.m
//  JabjaiApp
//
//  Created by mac on 4/23/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import "ReportBehaviorAllDataDetailViewController.h"
#import "ReportBehaviorAllDataCalendarViewController.h"
#import "Utils.h"
#import "UserData.h"
@interface ReportBehaviorAllDataDetailViewController () {
    UIColor *greenColor;
    UIColor *redColor;
    NSDateFormatter *timeFormatter;
}


@end

@implementation ReportBehaviorAllDataDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
   
    
    greenColor = [UIColor colorWithRed:12/255.0 green:190/255.0 blue:64/255.0 alpha:1.0];
    redColor = [UIColor colorWithRed:255/255.0 green:57/255.0 blue:56/255.0 alpha:1.0];
    timeFormatter = [Utils getDateFormatter];
    if ([[UserData getChangLanguage] isEqual:@"th"]) {
        [timeFormatter setDateFormat:@"HH:mm 'น.'"];
    }else{
        [timeFormatter setDateFormat:@"HH:mm"];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    
    CGFloat max = MAX(self.scoreLabel.frame.size.width, self.scoreLabel.frame.size.height);
    self.scoreLabel.layer.cornerRadius = max / 2.0;
    self.scoreLabel.layer.masksToBounds = YES;
    [self doDesignLayout];
    [self updateUI];
}
- (void) doDesignLayout{
    CAGradientLayer *gradientHeader;
    //set color header
    [self.headerView layoutIfNeeded];
    gradientHeader = [Utils getGradientColorHeader];
    gradientHeader.frame = self.headerView.bounds;
    [self.headerView.layer insertSublayer:gradientHeader atIndex:0];
    
    self.headerTitleLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_BEHAVIORSCORE_REPORT",nil,[Utils getLanguage],nil);
    self.headerSummaryLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_SUMMARY",nil,[Utils getLanguage],nil);
    self.headerStudentLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_STUDENT",nil,[Utils getLanguage],nil);
    self.headerScoreLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_SCORE",nil,[Utils getLanguage],nil);
    self.headerRemarkLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_SAVE",nil,[Utils getLanguage],nil);
    self.headerRecorderLabel.text = NSLocalizedStringFromTableInBundle(@"LABEL_BEHAVIOR_REP_MEAN",nil,[Utils getLanguage],nil);
}

- (void)updateUI {
    
    if(self.selectedHistoryModel != nil) {
        self.behaviorNameLabel.text = [self.selectedHistoryModel getBehaviorName];
        self.datetimeLabel.text = [NSString stringWithFormat:@"%@ %@", [Utils getThaiDateFormatWithDate:[self.selectedHistoryModel getDateTime]], [timeFormatter stringFromDate:[self.selectedHistoryModel getDateTime]]];
        self.studentNameLabel.text = [self.selectedHistoryModel getStudentName];
        
        if([self.selectedHistoryModel getBehaviorType] == 0) { // type add
            self.scoreLabel.text = [NSString stringWithFormat:@"+%@",[NSNumber numberWithDouble:[self.selectedHistoryModel getBehaviorScore]] ];
            self.scoreLabel.backgroundColor = greenColor;
        }
        else { // type reduce
            self.scoreLabel.text = [NSString stringWithFormat:@"-%@", [NSNumber numberWithDouble:[self.selectedHistoryModel getBehaviorScore]]];
            self.scoreLabel.backgroundColor = redColor;
        }
        
        if([self.selectedHistoryModel getRecorder] != nil && [[self.selectedHistoryModel getRecorder] length] > 0) {
            self.recorderLabel.text = [self.selectedHistoryModel getRecorder];
        }
        else {
            self.recorderLabel.text = @"-";
        }
        
        if([self.selectedHistoryModel getRemark] != nil && [[self.selectedHistoryModel getRemark] length] > 0) {
            self.remarkLabel.text = [self.selectedHistoryModel getRemark];
        }
        else {
            self.remarkLabel.text = @"-";
        }
        
        
    }
    else {
        self.behaviorNameLabel.text = nil;
        self.datetimeLabel.text = nil;
        self.studentNameLabel.text = @"-";
        self.scoreLabel.text = @"+0";
        self.scoreLabel.backgroundColor = greenColor;
        self.recorderLabel.text = @"-";
        self.remarkLabel.text = @"-";
    }
}

- (IBAction)moveBack:(id)sender {
    
    ReportBehaviorAllDataCalendarViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportBehaviorAllDataCalendarStoryboard"];
    
    viewController.classLevelArray = _classLevelArray;
    viewController.classroomArray = _classroomArray;
    viewController.statusArray = _statusArray;
    viewController.selectedClassLevelIndex = _selectedClassLevelIndex;
    viewController.selectedClassroomIndex = _selectedClassroomIndex;
    viewController.selectedStatusIndex = _selectedStatusIndex;
    viewController.classLevelId = _classLevelId;
    viewController.classroomId = _classroomId;
    viewController.statusId = _statusId;
    [self.slideMenuController changeMainViewController:viewController close:YES];
}


@end
