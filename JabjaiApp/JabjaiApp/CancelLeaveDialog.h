//
//  CancelLeaveDialog.h
//  JabjaiApp
//
//  Created by mac on 5/18/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CancelLeaveDialog;

@protocol CancelLeaveDialogDelegate <NSObject>
@optional

- (void)onCancelConfirmDialog:(CancelLeaveDialog *)cancelconfirmDialog confirm:(BOOL)confirm;

@end

@interface CancelLeaveDialog : UIViewController

@property (weak, nonatomic) id<CancelLeaveDialogDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIView *declineBackGround;
@property (weak, nonatomic) IBOutlet UIView *acceptBackGround;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;

- (IBAction)actionConfirm:(id)sender;
- (IBAction)actionCancel:(id)sender;

- (void)showDialogInView:(UIView *)targetView message:(NSString *)message;
- (void)dismissDialog;
- (BOOL)isDialogShowing;

@end
