//
//  ReportCheckSubjectDateTimeOfStudentViewController.h
//  JabjaiApp
//
//  Created by toffee on 23/1/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportCheckSubjectHistoryStudentAllViewController.h"
#import "ReportCheckFlagStatusCollectionViewCell.h"
#import "CallReportCheckSubjectDateTimeStudentAPI.h"
#import "ReportCheckNameSubjectDateTimeTableViewCell.h"
#import "RPCheckNameSubjectDateTimeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportCheckSubjectDateTimeOfStudentViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource,CallReportCheckSubjectDateTimeStudentAPIDelegate>

@property (nonatomic) NSArray *studentStatusAbsenArray;
@property (nonatomic) NSArray *studentStatusOnTimeArray;
@property (nonatomic) NSArray *studentStatusLateArray;
@property (nonatomic) NSArray *studentStatusPersonalArray;
@property (nonatomic) NSArray *studentStatusSickArray;
@property (nonatomic) NSArray *studentStatusEventArray;
@property (nonatomic) NSArray *studentStatusAllArray;
@property (nonatomic) NSArray *studentStatusUndefinedArray;
@property (nonatomic) NSArray *studentAllArray;
@property (nonatomic) NSString *nameClassRoom;
@property (nonatomic) NSString *nameSubject;
@property (nonatomic) long long selectedClassroomId;
@property (nonatomic) NSString *selectedSubjectId;
@property (nonatomic) NSDate* consideredDate;
@property (nonatomic) NSString *studentId;
@property (nonatomic) NSString *studentName;
@property (nonatomic) NSString *studentPic;

//date
@property (nonatomic) NSArray *dateTimeAllArray;
@property (nonatomic) NSArray *dateTimeOnTimeArray;
@property (nonatomic) NSArray *dateTimeLateArray;
@property (nonatomic) NSArray *dateTimeAbsenArray;
@property (nonatomic) NSArray *dateTimeSickArray;
@property (nonatomic) NSArray *dateTimePersonalArray;
@property (nonatomic) NSArray *dateTimeEventArray;
@property (nonatomic) NSArray *dateTimeUndefinedArray;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *studentImage;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *showNoStudent;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;

- (IBAction)moveBack:(id)sender;



@end

NS_ASSUME_NONNULL_END
