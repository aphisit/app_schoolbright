//
//  ReportSendNewsListTableViewCell.h
//  JabjaiApp
//
//  Created by mac on 4/5/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportSendNewsListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageIconSendNews;
@property (weak, nonatomic) IBOutlet UILabel *dateTitle;
@property (weak, nonatomic) IBOutlet UILabel *headTitle;
@property (weak, nonatomic) IBOutlet UILabel *detailText;
@property (weak, nonatomic) IBOutlet UIView *sectionView;

@end
