//
//  RFSpecifyNumberMoneyViewController.m
//  JabjaiApp
//
//  Created by toffee on 17/4/2562 BE.
//  Copyright © 2562 jabjai. All rights reserved.
//

#import "RFSpecifyNumberMoneyViewController.h"
#import "Utils.h"
@interface RFSpecifyNumberMoneyViewController (){
    BOOL isShowing;
    NSInteger money;
    BOOL limit;
}

@end

@implementation RFSpecifyNumberMoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isShowing = NO;
    self.amountMoneyTextField.delegate = self;
    
    self.showUnNumberLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_PLEASE_SPECIFY_AMOUNT",nil,[Utils getLanguage],nil);
    self.specifyAmountLable.text = NSLocalizedStringFromTableInBundle(@"LABEL_TOPUP_PLEASE_SPECIFY_AMOUNT",nil,[Utils getLanguage],nil);
    [self.confirmButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_TOPUP_SUBMIT",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedStringFromTableInBundle(@"BTN_TOPUP_CANCEL",nil,[Utils getLanguage],nil) forState:UIControlStateNormal];
    
}

- (void) doDesignLayout{
    CAGradientLayer *gradientConfirm,*gradientCancel;
    //set color header
    [self.confirmButton layoutIfNeeded];
    gradientConfirm = [Utils getGradientColorStatus:@"green"];
    gradientConfirm.frame = self.confirmButton.bounds;
    [self.confirmButton.layer insertSublayer:gradientConfirm atIndex:0];
    
    //set color button next
    [self.cancelButton layoutIfNeeded];
    gradientCancel = [Utils getGradientColorStatus:@"red"];
    gradientCancel.frame = self.cancelButton.bounds;
    [self.cancelButton.layer insertSublayer:gradientCancel atIndex:0];
}

- (IBAction)confirmAction:(id)sender {
    
    money = [[self.amountMoneyTextField text] integerValue];
    if (money > 0) {
        self.showUnNumberLable.hidden = YES;
        if(self.delegate && [self.delegate respondsToSelector:@selector(confirmNumberMoney:amountMoney:)]) {
            [self.delegate confirmNumberMoney:self amountMoney:money];
        }
        [self dismissDialog];
    }else{
        self.showUnNumberLable.hidden = NO;
    }
    
   
}

- (IBAction)cancelAction:(id)sender {
    [self removeDialogFromView];
}

- (void) showDialogInView:(UIView *)targetView{
    
    [self.view setFrame:CGRectMake(targetView.frame.origin.x, targetView.frame.origin.y, targetView.frame.size.width, targetView.frame.size.height)];
    [self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.5]];
    [targetView addSubview:self.view];
    
    // Initialize dialog view hide it
    [self.contentview setAlpha:0.0];
    
    // Animate the display of the dialog view
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [self.contentview setAlpha:1.0];
    [self.amountMoneyTextField becomeFirstResponder];
    self.showUnNumberLable.hidden = YES;
    [UIView commitAnimations];
    [self doDesignLayout];
    isShowing = YES;
}

- (void) dismissDialog{
    [self removeDialogFromView];
  
}
-(void)removeDialogFromView {
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.contentview setAlpha:0.0];
    
    [UIView commitAnimations];
    
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    
    isShowing = NO;
}



- (BOOL) isDialogShowing{
    
    return isShowing;
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(!CGRectContainsPoint(self.contentview.frame, touchLocation)) {
        [self resignFirstResponder];
        [self dismissDialog];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.contentview endEditing:YES];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(nonnull NSString *)string{
    
    if(textField == self.amountMoneyTextField){
        
        if ([string isEqualToString:@"0"] && range.location == 0) {
            limit = NO;
        }else{
            if (textField.text.length < 5 || string.length == 0){
                limit = YES;
            }
            else{
                limit = NO;
            }
        }
    }
    
    return limit;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
   // [self.contentview layoutIfNeeded];
    self.contentViewCenterY.constant = -60;
    //[self.contentview setFrame:CGRectMake(self.contentview.frame.origin.x, self.contentview.frame.origin.y - 80.0, self.contentview.frame.size.width, self.contentview.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.

}

-(void)keyboardDidHide:(NSNotification *)notification
{
    self.contentViewCenterY.constant = 0;
   // [self.contentview layoutIfNeeded];
   // [self.contentview setFrame:CGRectMake(self.contentview.frame.origin.x, self.contentview.frame.origin.y, self.contentview.frame.size.width, self.contentview.frame.size.height)];
}





@end
