//
//  ExtensionDelegate.h
//  test Extension
//
//  Created by toffee on 2/20/2561 BE.
//  Copyright © 2561 jabjai. All rights reserved.
//

#import <WatchKit/WatchKit.h>

@interface ExtensionDelegate : NSObject <WKExtensionDelegate>

@end
